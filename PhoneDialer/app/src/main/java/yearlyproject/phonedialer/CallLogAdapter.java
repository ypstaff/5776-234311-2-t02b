package yearlyproject.phonedialer;

import android.content.Context;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.provider.CallLog.Calls;
import android.text.TextUtils;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import yearlyproject.phonedialer.AsyncContactImageLoader.ImageCallback;

/**
 * @author Lior Volin
 * Based on an open source project.
 */
public class CallLogAdapter extends CursorAdapter {

    public static final String[] PROJECTION = new String[]{
            Calls._ID,
            Calls.NUMBER,
            Calls.CACHED_NAME,
            Calls.DATE,
            Calls.DURATION,
            Calls.NEW,
            Calls.TYPE
    };
    public static final int COLUMN_NUMBER = 1;
    public static final int COLUMN_CACHED_NAME = 2;
    public static final int COLUMN_DATE = 3;
    public static final int COLUMN_TYPE = 6;

    private class ViewCache {
        public final TextView contactName;
        public final TextView contactLastDialed;
        public final TextView contactInformation;
        public final TextView contactCallCount;
        public final ImageView contactImage;
        public final ImageView contactOverlayImage;

        public ViewCache(View base) {
            contactName = (TextView) base.findViewById(R.id.ContactName);
            contactLastDialed = (TextView) base.findViewById(R.id.ContactLastDialed);
            contactInformation = (TextView) base.findViewById(R.id.ContactInformation);
            contactCallCount = (TextView) base.findViewById(R.id.ContactCallCount);
            contactImage = (ImageView) base.findViewById(R.id.ContactImage);
            contactOverlayImage = (ImageView) base.findViewById(R.id.ContactOverlayImage);
        }

    }

    private Resources mResources;
    private AsyncContactImageLoader mAsyncContactImageLoader;
    private final Drawable mIncomingDrawable;
    private final Drawable mOutgoingDrawable;
    private final Drawable mMissedDrawable;

    public CallLogAdapter(Context context, Cursor cursor, AsyncContactImageLoader asyncContactImageLoader) {
        super(context, cursor);
        mResources = context.getResources();
        mAsyncContactImageLoader = asyncContactImageLoader;
        mIncomingDrawable = mResources.getDrawable(R.drawable.overlay_incoming);
        mOutgoingDrawable = mResources.getDrawable(R.drawable.overlay_outgoing);
        mMissedDrawable = mResources.getDrawable(R.drawable.overlay_missed);
    }

    @Override
    public View newView(Context c, Cursor cursor, ViewGroup parent) {

        View $ = LayoutInflater.from(c).inflate(R.layout.contact_item, null);
        ViewCache viewCache = new ViewCache($);
        $.setTag(viewCache);
        return $;
    }

    @Override
    public void bindView(View v, Context c, Cursor cursor) {

        final ViewCache viewCache = (ViewCache) v.getTag();
        // empty view, shouldn't bind it
        if (viewCache == null)
            return;

        String cachedName = cursor.getString(COLUMN_CACHED_NAME);
        viewCache.contactName.setText(!TextUtils.isEmpty(cachedName) ? cachedName : mResources.getString(R.string.unknown));
        long date = cursor.getLong(COLUMN_DATE);
        viewCache.contactLastDialed.setText(date != 0 ? DateUtils.getRelativeTimeSpanString(date) : mResources.getString(R.string.not_contacted));

        // number of -1 does not have a number
        String number = cursor.getString(COLUMN_NUMBER);
        viewCache.contactInformation.setText(TextUtils.equals(number, "-1") || TextUtils.isEmpty(number) ? "" : number);
        viewCache.contactCallCount.setVisibility(View.GONE);
        int type = cursor.getInt(COLUMN_TYPE);

        switch (type) {
            case Calls.INCOMING_TYPE:
                viewCache.contactOverlayImage.setImageDrawable(mIncomingDrawable);
                break;
            case Calls.OUTGOING_TYPE:
                viewCache.contactOverlayImage.setImageDrawable(mOutgoingDrawable);
                break;
            case Calls.MISSED_TYPE:
                viewCache.contactOverlayImage.setImageDrawable(mMissedDrawable);
                break;
        }
        viewCache.contactOverlayImage.setVisibility(View.VISIBLE);

        // set a tag for the callback to be able to check, so we don't set the contact image of a reused view
        viewCache.contactImage.setTag(number);
        viewCache.contactImage
                .setImageDrawable(mAsyncContactImageLoader.loadDrawableForNumber(number, new ImageCallback() {
                    @Override
                    public void imageLoaded(Drawable imageDrawable, String phoneNumber) {
                        if (TextUtils.equals(phoneNumber, (String) viewCache.contactImage.getTag()))
                            viewCache.contactImage.setImageDrawable(imageDrawable);
                    }
                }));
    }
}
