package yearlyproject.phonedialer;

import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.AsyncQueryHandler;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.provider.CallLog.Calls;
import android.provider.ContactsContract;
import android.provider.ContactsContract.CommonDataKinds.Email;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.provider.ContactsContract.Contacts;
import android.provider.ContactsContract.Data;
import android.provider.ContactsContract.PhoneLookup;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.res.ResourcesCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.GestureDetector;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.EditText;
import android.widget.FilterQueryProvider;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import yearlyproject.navigationlibrary.EmergencyCallActivity;
import yearlyproject.navigationlibrary.IterableGroup;
import yearlyproject.navigationlibrary.IterableListView;
import yearlyproject.navigationlibrary.IterableSingleView;
import yearlyproject.navigationlibrary.IterableViewGroup;
import yearlyproject.navigationlibrary.IterableViewGroups;

/**
 * @author Lior Volin
 * Based on an open source project.
 */
public class DialerActivity extends EmergencyCallActivity implements OnClickListener, OnItemClickListener, OnItemLongClickListener, OnSharedPreferenceChangeListener, FilterQueryProvider {
    public static final String PREFERENCE_VIBRATION_LENGTH = "vibration_length";
    public static final String PREFERENCE_SPEED_DIAL_SLOT = "speedDialSlot";
    public static final String PREFERENCE_T9_MATCH_START_OF_NAMES_ONLY = "t9_match_start_of_names_only";
    public static final String PREFERENCE_T9_SORT_BY_TIMES_CONTACTED = "t9_sort_by_times_contacted";
    public static final String PREFERENCE_T9_MATCH_PHONE_NUMBERS = "t9_match_phone_numbers";
    public static final String PREFERENCE_FAVORITE_CONTACTS_FIRST = "favorite_contacts_first";
    public static final String PREFERENCE_SHOW_CALL_COUNTER = "show_call_counter";

    public static final String DEFAULT_VIBRATION_LENGTH = "25";

    public static final int ACTIVITY_CONTACT_VIEW_DIALOG = 1;
    public static final int ACTIVITY_SELECT_NUMBER_DIALOG = 2;
    public static final int ACTIVITY_SELECT_SPEED_DIAL_SLOT_DIALOG = 3;
    public static final int PICK_CONTACT_REQUEST = 4;

    public static final int Gingerbread = 9;
    public static final int TIME_TO_SLEEP = 1500;

    private QueryHandler mQueryHandler;
    private ViewHolder mViewHolder;
    private yearlyproject.phonedialer.CallLogAdapter mCallLogAdapter;
    private Cursor mContactsCursor;
    private ContactsAdapter mContactsAdapter;
    private boolean mInCallLogMode;
    private boolean mDialerEnabled;
    private int mVibrationLength;
    private GestureDetector mGestureDetector;
    private boolean mT9MatchStartOfNamesOnly;
    private boolean mT9OrderByTimesContacted;
    private boolean mFavoriteContactsFirst;
    private boolean mT9MatchPhoneNumbers;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        initializeClassFields();

        prepareListeners();

        mViewHolder.phoneNumber.setInputType(0);
        mViewHolder.phoneNumber.requestFocus();
        mViewHolder.contactList.setAdapter(mCallLogAdapter);

        mQueryHandler.getCallLog();

        parseIntent(getIntent());

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        preferences.registerOnSharedPreferenceChangeListener(this);
        applySettings(preferences);

        checkMissedCalls();

        setThreeButtonsNavigation();
    }

    private void checkMissedCalls() {
        if (mInCallLogMode)
            new Thread() {
                public void run() {
                    clearMissedCalls();
                    if (Build.VERSION.SDK_INT != Gingerbread)
                        clearMissedCallsNotification();
                    try {
                        updateMissedCalls();
                    } catch (Exception e) {
                    } catch (Error err) {
                    } catch (Throwable t) {
                    }
                }
            }.start();
    }

    private void initializeClassFields() {
        mQueryHandler = new QueryHandler(getContentResolver());
        mViewHolder = new ViewHolder();
        mContactsCursor = null;
        AsyncContactImageLoader mAsyncContactImageLoader = new AsyncContactImageLoader(getApplicationContext(), getResources().getDrawable(R.drawable.default_icon));
        mCallLogAdapter = new CallLogAdapter(getApplicationContext(), null, mAsyncContactImageLoader);
        mContactsAdapter = new ContactsAdapter(getApplicationContext(), mContactsCursor, mAsyncContactImageLoader);
        mContactsAdapter.setFilterQueryProvider(this);
        mDialerEnabled = mInCallLogMode = true;

        mGestureDetector = new GestureDetector(getApplicationContext(), new GestureDetector.SimpleOnGestureListener() {
            @Override
            public boolean onFling(MotionEvent event1, MotionEvent event2, float velocityX, float velocityY) {
                if (event2.getY() < event1.getY() &&
                        mDialerEnabled &&
                        getResources().getConfiguration().orientation != Configuration.ORIENTATION_LANDSCAPE)
                    disableDialer();
                return false;
            }
        });

        mT9OrderByTimesContacted = mT9MatchStartOfNamesOnly = false;
    }

    private void setThreeButtonsNavigation() {
        //Setting the 3 buttons navigation system for the activity
        fab_left = (FloatingActionButton) findViewById(R.id.fab_left);
        fab_right = (FloatingActionButton) findViewById(R.id.fab_right);
        fab_middle = (FloatingActionButton) findViewById(R.id.fab_middle);

        //Setting the action buttons to be semi transparent
        fab_left.setAlpha((float) 0.5);
        fab_middle.setAlpha((float) 0.5);
        fab_right.setAlpha((float) 0.5);

        //Setting the listeners for the navigation buttons
        fab_left.setOnClickListener(this);
        fab_right.setOnClickListener(this);
        fab_middle.setOnClickListener(this);

        fab_left.setOnLongClickListener(this);
        fab_right.setOnLongClickListener(this);
        fab_middle.setOnLongClickListener(this);

        //Creating the navigation group for the dialpad for when the app starts
        group = createPadGroup();
        group.start();
    }

    //Creates the navigation group for when the dialpad is disabled
    private IterableGroup createFullCallLogGroup() {
        Drawable black_border = ResourcesCompat.getDrawable(
                this.getResources(), R.drawable.black_border, null);

        IterableGroup call_history_group = new IterableListView(
                (ListView) findViewById(R.id.ContactListView), this, black_border, black_border, Color.BLUE);

        IterableGroup expand_group = new IterableSingleView(
                findViewById(R.id.DialerExpandButton), this, black_border, black_border, Color.BLUE);

        List<IterableGroup> full_call_history_list = new ArrayList<>();
        full_call_history_list.add(call_history_group);
        full_call_history_list.add(expand_group);

        return new IterableViewGroups(
                full_call_history_list, findViewById(R.id.rl_full_call_history), this);
    }

    //Creates the navigation group for when the dialerpad is enabled
    private IterableGroup createPadGroup() {

        IterableGroup row1_group = createOneRowGroup(mViewHolder.button1, mViewHolder.button2, mViewHolder.button3, R.id.Row1);
        IterableGroup row2_group = createOneRowGroup(mViewHolder.button4, mViewHolder.button5, mViewHolder.button6, R.id.Row2);
        IterableGroup row3_group = createOneRowGroup(mViewHolder.button7, mViewHolder.button8, mViewHolder.button9, R.id.Row3);
        IterableGroup row4_group = createOneRowGroup(findViewById(R.id.ButtonStar), mViewHolder.button0, findViewById(R.id.ButtonHash), R.id.Row4);
        IterableGroup row5_group = createOneRowGroup(findViewById(R.id.ButtonPlus), findViewById(R.id.ButtonCall), findViewById(R.id.ButtonDelete), R.id.Row5);
        IterableGroup row6_group = createOneRowGroup(findViewById(R.id.ButtonContract), findViewById(R.id.ButtonPhoneBook), findViewById(R.id.ButtonEmergency), R.id.Row6);

        List<IterableGroup> pad_list = new ArrayList<>();
        pad_list.add(row1_group);
        pad_list.add(row2_group);
        pad_list.add(row3_group);
        pad_list.add(row4_group);
        pad_list.add(row5_group);
        pad_list.add(row6_group);

        return new IterableViewGroups(
                pad_list, findViewById(R.id.rl_dial_pad), this);
    }

    IterableGroup createOneRowGroup(View view1, View view2, View view3, int row_id) {
        Drawable black_border = ResourcesCompat.getDrawable(
                this.getResources(), R.drawable.black_border, null);

        List<View> row_list = new ArrayList<>();
        row_list.add(view1);
        row_list.add(view2);
        row_list.add(view3);

        return new IterableViewGroup(row_list, findViewById(row_id), this,
                black_border, black_border, Color.BLUE);
    }

    // parse the incoming intent and take specific action if needed
    private void parseIntent(Intent i) {
        if (!Intent.ACTION_VIEW.equals(i.getAction()) && !Intent.ACTION_DIAL.equals(i.getAction()))
            return;
        Uri data = i.getData();
        if (data == null)
            return;
        String scheme = data.getScheme();
        if (scheme == null || !scheme.equals("tel"))
            return;
        String number = data.getSchemeSpecificPart();
        if (number != null)
            dialNumber(number);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu m) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, m);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu m) {
        m.findItem(R.id.menu_item_show_hide)
                .setTitle(mDialerEnabled ? R.string.hide_dialer : R.string.show_dialer);
        m.findItem(R.id.menu_item_contact_list).setVisible(mInCallLogMode).setEnabled(mInCallLogMode);
        m.findItem(R.id.menu_item_call_log).setVisible(!mInCallLogMode).setEnabled(!mInCallLogMode);
        m.findItem(R.id.menu_item_clear_call_log)
                .setVisible(mInCallLogMode)
                .setEnabled(mInCallLogMode);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem i) {
        switch (i.getItemId()) {
            case R.id.menu_item_show_hide:
                toggleDialer();
                break;
            case R.id.menu_item_contact_list:
                mContactsAdapter.getFilter().filter(mViewHolder.phoneNumber.getText());
                switchToContactListMode();
                break;
            case R.id.menu_item_call_log:
                switchToCallLogMode();
                break;
            case R.id.menu_item_clear_call_log:
                askToClearCallLog();
                break;
            case R.id.menu_item_settings:
                Intent intent = new Intent(getApplicationContext(), DialerPreferenceActivity.class);
                startActivity(intent);
                break;
        }
        return true;
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent e) {
        if (keyCode != KeyEvent.KEYCODE_CALL && keyCode != KeyEvent.KEYCODE_ENTER || e.getRepeatCount() != 0
                || !mDialerEnabled)
            return super.onKeyDown(keyCode, e);
        callNumberAndFinish(mViewHolder.phoneNumber.getText());
        return true;
    }

    private void prepareListeners() {
        mViewHolder.button0.setOnLongClickListener(this);
        mViewHolder.button1.setOnLongClickListener(this);
        mViewHolder.button2.setOnLongClickListener(this);
        mViewHolder.button3.setOnLongClickListener(this);
        mViewHolder.button4.setOnLongClickListener(this);
        mViewHolder.button5.setOnLongClickListener(this);
        mViewHolder.button6.setOnLongClickListener(this);
        mViewHolder.button7.setOnLongClickListener(this);
        mViewHolder.button8.setOnLongClickListener(this);
        mViewHolder.button9.setOnLongClickListener(this);
        mViewHolder.buttonDelete.setOnLongClickListener(this);
        mViewHolder.contactList.setOnItemClickListener(this);
        mViewHolder.contactList.setOnItemLongClickListener(this);
        mViewHolder.dialerView.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent e) {
                return true;
            }
        });
        mViewHolder.contactList.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent e) {
                return mGestureDetector.onTouchEvent(e);
            }
        });
        mViewHolder.phoneNumber.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        //For the navigation's buttons functionality
        super.onClick(v);

        final Vibrator vibrator = (Vibrator) getSystemService(VIBRATOR_SERVICE);
        new Thread() {
            @Override
            public void run() {
                vibrator.vibrate(mVibrationLength);
            }
        }.start();

        switch (v.getId()) {
            case R.id.Button0:
                onCharacterPressed('0');
                break;
            case R.id.Button1:
                onCharacterPressed('1');
                break;
            case R.id.Button2:
                onCharacterPressed('2');
                break;
            case R.id.Button3:
                onCharacterPressed('3');
                break;
            case R.id.Button4:
                onCharacterPressed('4');
                break;
            case R.id.Button5:
                onCharacterPressed('5');
                break;
            case R.id.Button6:
                onCharacterPressed('6');
                break;
            case R.id.Button7:
                onCharacterPressed('7');
                break;
            case R.id.Button8:
                onCharacterPressed('8');
                break;
            case R.id.Button9:
                onCharacterPressed('9');
                break;
            case R.id.ButtonStar:
                onCharacterPressed('*');
                break;
            case R.id.ButtonHash:
                onCharacterPressed('#');
                break;
            case R.id.ButtonDelete:
                onDeletePressed();
                break;
            case R.id.ButtonCall:
                onCallPressed();
                break;
            case R.id.ButtonPlus:
                onPlusPressed();
                break;
            case R.id.DialerExpandButton:
                onDialerExpandPressed();
                break;
            case R.id.ButtonContract:
                onContractPressed();
                break;
            case R.id.ButtonEmergency:
                onEmergencyPressed();
                break;
            case R.id.ButtonPhoneBook:
                onPhoneBookPressed();
                break;
            case R.id.EditTextPhoneNumber:
                mViewHolder.phoneNumber.setCursorVisible(true);
                break;
        }
    }

    private void onPhoneBookPressed() {
        //Starting the contacts activity from the messages app for result - the phone
        // number of the chosen contact
        try {
            Intent intent = new Intent();
            intent.setClassName("yearlyproject.messages", "yearlyproject.messages.ContactsActivity");
            startActivityForResult(intent, PICK_CONTACT_REQUEST);
        } catch (ActivityNotFoundException anfe) {
            Toast.makeText(this, "You need to install the Messages app in order to use that functionality", Toast.LENGTH_LONG).show();
        } catch (Exception e) {
            Toast.makeText(this, "You can't use that functionality", Toast.LENGTH_LONG).show();
        }
    }

    private void onEmergencyPressed() {
        startActivity((new Intent(this, EmergencyActivity.class)));
    }

    private void onContractPressed() {
        //Disabling the dialer, canceling the marks of the previous navigation group, creating
        // a new navigation group for when the dialer is disabled and starting it
        disableDialer();
        group.stop();
        group = createFullCallLogGroup();
        group.start();
    }

    private void onDialerExpandPressed() {
        //Enabling the dialer, canceling the marks of the previous navigation group, creating
        // a new navigation group for when the dialer is enabled and starting it
        enableDialer();
        group.stop();
        group = createPadGroup();
        group.start();
    }

    private void onPlusPressed() {
        CharSequence cur = mViewHolder.phoneNumber.getText();
        int start = mViewHolder.phoneNumber.getSelectionStart();
        int end = mViewHolder.phoneNumber.getSelectionEnd();
        mViewHolder.phoneNumber.setText(cur.subSequence(0, start) + "+" + cur.subSequence(end, cur.length()));
        mViewHolder.phoneNumber.setSelection(start + 1);
    }

    private void onCallPressed() {
        if (mViewHolder.phoneNumber.getText().length() != 0)
            callNumberAndFinish(mViewHolder.phoneNumber.getText());
        else
            Toast.makeText(this, "You haven't entered a number to call", Toast.LENGTH_LONG).show();
    }

    private void onDeletePressed() {
        CharSequence cur = mViewHolder.phoneNumber.getText();
        int start = mViewHolder.phoneNumber.getSelectionStart();
        int end = mViewHolder.phoneNumber.getSelectionEnd();
        if (start != end) {
            cur = cur.subSequence(0, start).toString() + cur.subSequence(end, cur.length()).toString();
            mViewHolder.phoneNumber.setText(cur);
            mViewHolder.phoneNumber.setSelection(end - (end - start));
            if (cur.length() == 0)
                mViewHolder.phoneNumber.setCursorVisible(false);
        } else if (start != 0) {
            cur = cur.subSequence(0, start - 1).toString() + cur.subSequence(start, cur.length()).toString();
            mViewHolder.phoneNumber.setText(cur);
            mViewHolder.phoneNumber.setSelection(start - 1);
            if (cur.length() == 0)
                mViewHolder.phoneNumber.setCursorVisible(false);
        }
    }

    private void onCharacterPressed(char digit) {
        CharSequence cur = mViewHolder.phoneNumber.getText();

        int start = mViewHolder.phoneNumber.getSelectionStart();
        int end = mViewHolder.phoneNumber.getSelectionEnd();
        int len = cur.length();


        if (cur.length() == 0)
            mViewHolder.phoneNumber.setCursorVisible(false);

        cur = cur.subSequence(0, start).toString() + digit + cur.subSequence(end, len).toString();
        mViewHolder.phoneNumber.setText(cur);
        mViewHolder.phoneNumber.setSelection(start + 1);
    }

    private void applySettings(SharedPreferences p) {
        mVibrationLength = Integer.parseInt(p.getString(PREFERENCE_VIBRATION_LENGTH, DEFAULT_VIBRATION_LENGTH));

        mT9MatchStartOfNamesOnly = p.getBoolean(PREFERENCE_T9_MATCH_START_OF_NAMES_ONLY, true);
        mT9OrderByTimesContacted = p.getBoolean(PREFERENCE_T9_SORT_BY_TIMES_CONTACTED, true);
        mContactsAdapter.setShowCallCounter(p.getBoolean(PREFERENCE_SHOW_CALL_COUNTER, false));
        mFavoriteContactsFirst = p.getBoolean(PREFERENCE_FAVORITE_CONTACTS_FIRST, false);
        mT9MatchPhoneNumbers = p.getBoolean(PREFERENCE_T9_MATCH_PHONE_NUMBERS, false);

        CharSequence s = mViewHolder.phoneNumber.getText();
        if (mInCallLogMode && s.length() <= 0)
            return;
        mContactsAdapter.getFilter().filter(s);
        switchToContactListMode();
    }

    private void toggleDialer() {
        if (mDialerEnabled)
            disableDialer();
        else
            enableDialer();
    }

    private void disableDialer() {
        mDialerEnabled = false;
        mViewHolder.dialerView.setVisibility(View.GONE);
        mViewHolder.dialerExpandMenu.setVisibility(View.VISIBLE);
        mViewHolder.phoneNumber.setSelected(false);
        mViewHolder.phoneNumber.setCursorVisible(false);
    }

    private void enableDialer() {
        mDialerEnabled = true;
        mViewHolder.dialerView.setVisibility(View.VISIBLE);
        mViewHolder.dialerExpandMenu.setVisibility(View.GONE);
        mViewHolder.phoneNumber.requestFocus();
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View v, int position, long id) {
        if (!mInCallLogMode)
            mQueryHandler.showContactForLookupKey(
                    ((Cursor) mContactsAdapter.getItem(position)).getString(ContactsAdapter.COLUMN_LOOKUP_KEY));
        else {
            Cursor c = (Cursor) mCallLogAdapter.getItem(position);
            String phoneNumber = c.getString(CallLogAdapter.COLUMN_NUMBER);
            if (!TextUtils.isEmpty(phoneNumber) && !TextUtils.equals("-1", phoneNumber))
                mQueryHandler.showContactForNumber(phoneNumber);
        }
        return true;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case ACTIVITY_CONTACT_VIEW_DIALOG:
                onContactViewDialogResult(resultCode, data);
                break;

            case ACTIVITY_SELECT_NUMBER_DIALOG:
                onSelectNumberDialogResult(resultCode, data);
                break;

            case ACTIVITY_SELECT_SPEED_DIAL_SLOT_DIALOG:
                onSelectSpeedDialSlotDialogResult(resultCode, data);
                break;

            //When returning from picking a contact (from the contacts activity)
            case PICK_CONTACT_REQUEST:
                onPickContactRequestResult(resultCode, data);
                break;

            default:
                break;
        }
    }

    private void onSelectSpeedDialSlotDialogResult(int resultCode, Intent data) {
        if (resultCode != SelectSpeedDialSlotDialog.RESULT_OK)
            return;
        String phoneNumber = data.getStringExtra(SelectSpeedDialSlotDialog.PHONE_NUMBER);
        int slot = data.getIntExtra(SelectSpeedDialSlotDialog.SPEED_DIAL_SLOT, -1);
        if (slot != -1)
            setSpeedDial(phoneNumber, slot);
    }

    private void onSelectNumberDialogResult(int resultCode, Intent data) {
        if (resultCode == SelectNumberDialog.RESULT_OK)
            selectSpeedDialSlot(data.getStringExtra(SelectNumberDialog.PHONE_NUMBER));
    }

    private void onContactViewDialogResult(int resultCode, Intent data) {
        String phoneNumber;

        switch (resultCode) {

            case ContactViewDialog.RESULT_VIEW_CONTACT:
                String lookupKey = data.getStringExtra(ContactViewDialog.LOOKUP_KEY);
                viewContact(lookupKey);
                break;

            case ContactViewDialog.RESULT_CALL_NUMBER:
                phoneNumber = data.getStringExtra(ContactViewDialog.PHONE_NUMBER);
                callNumberAndFinish(phoneNumber);
                break;

            case ContactViewDialog.RESULT_SEND_SMS:
                phoneNumber = data.getStringExtra(ContactViewDialog.PHONE_NUMBER);
                sendSms(phoneNumber);
                break;

            case ContactViewDialog.RESULT_SEND_EMAIL:
                String emailAdress = data.getStringExtra(ContactViewDialog.EMAIL_ADDRESS);
                sendEmail(emailAdress);
                break;

            case ContactViewDialog.RESULT_SET_SPEED_DIAL:
                ArrayList<String> phoneNumbers = data.getStringArrayListExtra(ContactViewDialog.PHONE_NUMBERS);
                ArrayList<String> phoneTypes = data.getStringArrayListExtra(ContactViewDialog.PHONE_TYPES);
                selectSpeedDialNumber(phoneNumbers, phoneTypes);
                break;

            case ContactViewDialog.RESULT_DIAL_NUMBER:
                phoneNumber = data.getStringExtra(ContactViewDialog.PHONE_NUMBER);
                dialNumber(phoneNumber);
                break;

            default:
                break;
        }
    }

    //When returning from the contacts activity
    private void onPickContactRequestResult(int resultCode, Intent data) {
        //Creating the backgrounds and borders again
        group.stop();
        group = createPadGroup();
        group.start();
        //Dialing (and not calling yet) the returned phone number
        if (resultCode == RESULT_OK)
            dialNumber(data.getStringExtra("phone number"));
    }

    private void dialNumber(String phoneNumber) {
        enableDialer();
        mViewHolder.phoneNumber.setText(phoneNumber);
        mViewHolder.phoneNumber.setSelection(mViewHolder.phoneNumber.getText().length());
        mViewHolder.phoneNumber.setSelected(false);
    }

    private void addPhoneNumber(String phoneNumber) {
        startActivity((new Intent(ContactsContract.Intents.SHOW_OR_CREATE_CONTACT, Uri.parse("tel:" + phoneNumber))));
    }

    private void selectSpeedDialNumber(ArrayList<String> phoneNumbers, ArrayList<String> phoneTypes) {
        if (phoneNumbers.size() == 1)
            selectSpeedDialSlot(phoneNumbers.get(0));
        else {
            Intent intent = new Intent(getApplicationContext(), SelectNumberDialog.class);
            intent.putExtra(SelectNumberDialog.PHONE_NUMBERS, phoneNumbers);
            intent.putExtra(SelectNumberDialog.PHONE_TYPES, phoneTypes);
            startActivityForResult(intent, ACTIVITY_SELECT_NUMBER_DIALOG);
        }
    }

    private void selectSpeedDialSlot(String phoneNumber) {
        Intent intent = new Intent(getApplicationContext(), SelectSpeedDialSlotDialog.class);
        intent.putExtra(SelectSpeedDialSlotDialog.PHONE_NUMBER, phoneNumber);
        startActivityForResult(intent, ACTIVITY_SELECT_SPEED_DIAL_SLOT_DIALOG);
    }

    private void setSpeedDial(String phoneNumber, int slot) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        Editor editor = preferences.edit();
        editor.putString(PREFERENCE_SPEED_DIAL_SLOT + slot, phoneNumber);
        editor.apply();
    }

    private void sendEmail(String emailAdress) {
        startActivity(new Intent(Intent.ACTION_SENDTO, Uri.parse("mailto:" + emailAdress)));
    }

    private void sendSms(String phoneNumber) {
        startActivity(new Intent(Intent.ACTION_SENDTO, Uri.parse("smsto:" + phoneNumber)));
    }

    private void viewContact(final String lookupKey) {
        // can't start an activity on the lookup uri itself on android 2.1 phones
        new Thread() {
            @Override
            public void run() {
                startActivity(new Intent(Intent.ACTION_VIEW, Contacts.lookupContact(getContentResolver(),
                        Uri.withAppendedPath(Contacts.CONTENT_LOOKUP_URI, lookupKey))));
            }
        }.start();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
        if (!mInCallLogMode)
            callContactAndFinish(
                    ((Cursor) mContactsAdapter.getItem(position)).getString(ContactsAdapter.COLUMN_LOOKUP_KEY));
        else {
            Cursor c = (Cursor) mCallLogAdapter.getItem(position);
            String phoneNumber = c.getString(CallLogAdapter.COLUMN_NUMBER);
            if (!TextUtils.isEmpty(phoneNumber) && !TextUtils.equals("-1", phoneNumber))
                callNumberAndFinish(c.getString(c.getColumnIndex(Calls.NUMBER)));
        }
    }

    private void callNumberAndFinish(CharSequence number) {
        if (number == null || number.length() == 0) {
            Toast.makeText(this, R.string.contact_has_no_phone_number, Toast.LENGTH_SHORT).show();
            return;
        }

        Uri uri = Uri.parse("tel:" + Uri.encode(number.toString()));
        Intent intent = new Intent(Intent.ACTION_CALL, uri);
        startActivity(intent);
        finish();
    }

    private void callContactAndFinish(String lookupKey) {
        mQueryHandler.callContactAndFinish(lookupKey);
    }

    private void switchToCallLogMode() {
        if (mInCallLogMode)
            return;
        mInCallLogMode = true;
        mViewHolder.contactList.setAdapter(mCallLogAdapter);
    }

    private void switchToContactListMode() {
        if (!mInCallLogMode)
            return;
        mInCallLogMode = false;
        mViewHolder.contactList.setAdapter(mContactsAdapter);
    }

    private void askToClearCallLog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.clear_call_log)
                .setMessage(R.string.are_you_sure_you_want_to_clear_the_call_log)
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface i, int which) {
                        clearCallLog();
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface i, int which) {
                        i.cancel();
                    }
                })
                .setIcon(R.drawable.ic_dialog_alert);
        builder.create().show();
    }

    private void clearCallLog() {
        getContentResolver().delete(Calls.CONTENT_URI, null, null);
    }

    private void clearMissedCalls() {
        // Clear the new missed calls (the actual missed calls, doesn't clear the notification)
        ContentValues cv = new ContentValues(1);
        cv.put(Calls.NEW, 0);
        getContentResolver().update(Calls.CONTENT_URI, cv, Calls.TYPE + "=" + Calls.MISSED_TYPE + " AND " + Calls.NEW + "=1", null);
    }

    private void updateMissedCalls() {
        // Start with sleeping for a while, maybe the user wants to do something else, give him time to
        try {
            Thread.sleep(TIME_TO_SLEEP);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        ContentValues cv = new ContentValues(3);
        ContentResolver cr = getContentResolver();
        final String[] phoneProjection = new String[]{PhoneLookup.DISPLAY_NAME, PhoneLookup.TYPE, PhoneLookup.LABEL};

        Cursor c = cr.query(Calls.CONTENT_URI,
                new String[]{Calls.CACHED_NAME, Calls.NUMBER}, null, null, null);
        if (c != null && !c.moveToFirst()) {
            c.close();
            return;
        }
        HashSet<String> processedNumbers = c == null ? null : new HashSet<String>(c.getCount());
        if (c == null)
            return;
        do {
            String cachedName = c.getString(0);
            String number = c.getString(1);
            if (processedNumbers.contains(number))
                continue;
            processedNumbers.add(number);
            Cursor c2 = cr.query(Uri.withAppendedPath(PhoneLookup.CONTENT_FILTER_URI, Uri.encode(number)),
                    phoneProjection, null, null, null);
            if (c2 != null) {
                if (!c2.moveToFirst()) {
                    if (cachedName != null) {
                        cv.clear();
                        cv.putNull(Calls.CACHED_NAME);
                        cv.putNull(Calls.CACHED_NUMBER_TYPE);
                        cv.putNull(Calls.CACHED_NUMBER_LABEL);
                        cr.update(Calls.CONTENT_URI, cv, Calls.NUMBER + "=?", new String[]{number});
                    }
                } else if (!TextUtils.equals(c2.getString(0), cachedName)) {
                    cv.clear();
                    cv.put(Calls.CACHED_NAME, c2.getString(0));
                    cv.put(Calls.CACHED_NUMBER_TYPE, c2.getInt(1));
                    cv.put(Calls.CACHED_NUMBER_LABEL, c2.getString(2));
                    cr.update(Calls.CONTENT_URI, cv, Calls.NUMBER + "=?", new String[]{number});
                }
            }
            if (c2 != null)
                c2.close();
        } while (c.moveToNext());
        c.close();
    }

    private void clearMissedCallsNotification() {
        /*This actually uses non-public API's and reflection to access them
         * Code was taken from http://stackoverflow.com/questions/2720967/update-missed-calls-notification-on-android
    	 * only clears the notification */
        String LOG_TAG = "DialerActivity";
        try {
            Class<?> serviceManagerClass = Class.forName("android.os.ServiceManager");
            Method getServiceMethod = serviceManagerClass.getMethod("getService", String.class);
            Object phoneService = getServiceMethod.invoke(null, "phone");
            Class<?> ITelephonyClass = Class.forName("com.android.internal.telephony.ITelephony");
            Class<?> ITelephonyStubClass = null;
            for (Class<?> clazz : ITelephonyClass.getDeclaredClasses())
                if (clazz.getSimpleName().equals("Stub")) {
                    ITelephonyStubClass = clazz;
                    break;
                }
            if (ITelephonyStubClass == null)
                Log.d(LOG_TAG, "Unable to locate ITelephony.Stub class!");
            else {
                Class<?> IBinderClass = Class.forName("android.os.IBinder");
                Method asInterfaceMethod = ITelephonyStubClass.getDeclaredMethod("asInterface", IBinderClass);
                Object iTelephony = asInterfaceMethod.invoke(null, phoneService);
                if (iTelephony != null)
                    iTelephony.getClass().getMethod("cancelMissedCallsNotification").invoke(iTelephony);
                else
                    Log.w(LOG_TAG, "Telephony service is null, can't call " + "cancelMissedCallsNotification");
            }
        } catch (ClassNotFoundException ex) {
            Log.e(LOG_TAG,
                    "Failed to clear missed calls notification due to ClassNotFoundException!", ex);
        } catch (InvocationTargetException ex) {
            Log.e(LOG_TAG,
                    "Failed to clear missed calls notification due to InvocationTargetException!",
                    ex);
        } catch (NoSuchMethodException ex) {
            Log.e(LOG_TAG,
                    "Failed to clear missed calls notification due to NoSuchMethodException!", ex);
        } catch (Throwable ex) {
            Log.e(LOG_TAG, "Failed to clear missed calls notification due to Throwable!", ex);
        }
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences p, String key) {
        applySettings(p);
    }

    @Override
    public Cursor runQuery(CharSequence constraint) {
        try {
            stopManagingCursor(mContactsCursor);
            if (constraint == null || constraint.length() == 0)
                return mContactsCursor = managedQuery(Contacts.CONTENT_URI, ContactsAdapter.PROJECTION,
                        Contacts.HAS_PHONE_NUMBER + "=1", null, !mFavoriteContactsFirst ? Contacts.DISPLAY_NAME
                                : Contacts.STARRED + " DESC, " + Contacts.DISPLAY_NAME);
            String[] t9Lookup = getResources().getStringArray(R.array.t9lookup);
            StringBuilder builder = new StringBuilder();
            String constraintString = constraint.toString();
            for (int i = 0, constraintLength = constraintString.length(); i < constraintLength; ++i) {
                char c = constraintString.charAt(i);

                if (c >= '0' && c <= '9')
                    builder.append(t9Lookup[c - '0']);
                else if (c == '+')
                    builder.append(c);
                else
                    builder.append("[").append(Character.toLowerCase(c)).append(Character.toUpperCase(c)).append("]");
            }

            Uri contentUri = mT9MatchPhoneNumbers ? Phone.CONTENT_URI : Contacts.CONTENT_URI;

            String prefix = mT9MatchStartOfNamesOnly ? "" : "*";
            String whereStmt = mT9MatchPhoneNumbers ? "(" + Phone.DISPLAY_NAME + " GLOB ?) OR (" + Phone.NUMBER + " LIKE ?)" : "(" + Contacts.HAS_PHONE_NUMBER + " = 1) AND " + Contacts.DISPLAY_NAME + " GLOB ?";
            String[] whereArgs = !mT9MatchPhoneNumbers ? new String[]{prefix + builder.toString() + "*"}
                    : new String[]{prefix + builder.toString() + "*", constraintString + "%"};
            StringBuilder orderByBuilder = new StringBuilder();
            if (mFavoriteContactsFirst) {
                orderByBuilder.append(Contacts.STARRED);
                orderByBuilder.append(" DESC,");
            }
            if (mT9OrderByTimesContacted) {
                orderByBuilder.append(Contacts.TIMES_CONTACTED);
                orderByBuilder.append(" DESC,");
                orderByBuilder.append(Contacts.LAST_TIME_CONTACTED);
                orderByBuilder.append(" DESC, ");
            }
            orderByBuilder.append(Phone.DISPLAY_NAME);
            return mContactsCursor = managedQuery(contentUri, ContactsAdapter.PROJECTION, whereStmt, whereArgs,
                    orderByBuilder.toString());
        } catch (Exception e) {
            return null;
        } catch (Throwable t) {
            return null;
        }
    }

    private class QueryHandler extends AsyncQueryHandler {
        private static final int TOKEN_GET_CALL_LOG = 1;
        private static final int TOKEN_CALL_CONTACT = 2;
        private static final int TOKEN_SHOW_CONTACT_FOR_NUMBER = 3;
        private static final int TOKEN_SHOW_CONTACT_FOR_LOOKUP_KEY = 4;

        public QueryHandler(ContentResolver cr) {
            super(cr);
        }

        @Override
        protected void onQueryComplete(int token, Object cookie, Cursor c) {
            switch (token) {

                case TOKEN_GET_CALL_LOG:
                    onTokenGetCallLog(c);
                    break;

                case TOKEN_CALL_CONTACT:
                    onTokenCallContact(c);
                    break;

                case TOKEN_SHOW_CONTACT_FOR_NUMBER:
                    onTokenShowContactForNumber((String) cookie, c);
                    break;

                case TOKEN_SHOW_CONTACT_FOR_LOOKUP_KEY:
                    onTokenShowContactForLookupKey((String) cookie, c);
                    break;
            }
        }

        private void onTokenShowContactForLookupKey(String cookie, Cursor c) {
            if (!c.moveToFirst()) {
                // no data for this lookup key, should never happen
                c.close();
                return;
            }
            ArrayList<String> phoneNumbers = new ArrayList<>(c.getCount());
            ArrayList<Integer> phoneTypes = new ArrayList<>(c.getCount());
            ArrayList<String> emailAddresses = new ArrayList<>(c.getCount());
            String displayName = c.getString(0);

            do
                if (!Phone.CONTENT_ITEM_TYPE.equals(c.getString(1)))
                    emailAddresses.add(c.getString(2));
                else {
                    phoneNumbers.add(c.getString(2));
                    phoneTypes.add(c.getInt(3));
                }
            while (c.moveToNext());
            c.close();

            Intent intent = new Intent(getApplicationContext(), ContactViewDialog.class);
            intent.putExtra(ContactViewDialog.LOOKUP_KEY, cookie);
            intent.putExtra(ContactViewDialog.DISPLAY_NAME, displayName);
            intent.putExtra(ContactViewDialog.PHONE_NUMBERS, phoneNumbers);
            intent.putExtra(ContactViewDialog.PHONE_TYPES, phoneTypes);
            intent.putExtra(ContactViewDialog.EMAIL_ADDRESSES, emailAddresses);
            startActivityForResult(intent, ACTIVITY_CONTACT_VIEW_DIALOG);
        }

        private void onTokenShowContactForNumber(String cookie, Cursor c) {
            if (!c.moveToFirst()) {
                addPhoneNumber(cookie);
                c.close();
                return;
            }
            String lookupKey = c.getString(0);
            c.close();
            showContactForLookupKey(lookupKey);
        }

        private void onTokenCallContact(Cursor c) {
            if (!c.moveToFirst())
                Toast.makeText(DialerActivity.this, R.string.contact_has_no_phone_number, Toast.LENGTH_SHORT).show();
            else {
                String numberToCall = c.getString(c.getColumnIndex(Phone.NUMBER));
                do
                    if (c.getInt(c.getColumnIndex(Phone.IS_SUPER_PRIMARY)) == 1) {
                        numberToCall = c.getString(c.getColumnIndex(Phone.NUMBER));
                        break;
                    }
                while (c.moveToNext());
                c.close();
                Uri uri = Uri.parse("tel:" + Uri.encode(numberToCall));
                Intent intent = new Intent(Intent.ACTION_CALL, uri);
                startActivity(intent);
                finish();
            }
        }

        private void onTokenGetCallLog(Cursor c) {
            startManagingCursor(c);
            mCallLogAdapter.changeCursor(c);
        }

        public void getCallLog() {
            try {
                startQuery(TOKEN_GET_CALL_LOG, null, Calls.CONTENT_URI, CallLogAdapter.PROJECTION, null, null, Calls.DEFAULT_SORT_ORDER);
            } catch (Throwable t) {
            }
        }

        public void callContactAndFinish(String lookupKey) {
            try {
                startQuery(TOKEN_CALL_CONTACT, null, Phone.CONTENT_URI, null, Phone.LOOKUP_KEY + "=?", new String[]{lookupKey}, null);
            } catch (Throwable t) {
            }
        }

        public void showContactForNumber(String phoneNumber) {
            try {
                startQuery(TOKEN_SHOW_CONTACT_FOR_NUMBER, phoneNumber,
                        Uri.withAppendedPath(PhoneLookup.CONTENT_FILTER_URI, Uri.encode(phoneNumber)),
                        new String[]{PhoneLookup.LOOKUP_KEY}, null, null, null);
            } catch (Throwable t) {
            }
        }

        public void showContactForLookupKey(String lookupKey) {
            try {
                startQuery(TOKEN_SHOW_CONTACT_FOR_LOOKUP_KEY, lookupKey, Data.CONTENT_URI,
                        new String[]{Data.DISPLAY_NAME, Data.MIMETYPE, Data.DATA1, Data.DATA2},
                        Data.LOOKUP_KEY + "=? AND (" + Data.MIMETYPE + "=? OR " + Data.MIMETYPE + "=?)",
                        new String[]{lookupKey, Phone.CONTENT_ITEM_TYPE, Email.CONTENT_ITEM_TYPE},
                        Data.MIMETYPE + " DESC");
            } catch (Throwable t) {
            }
        }
    }

    private class ViewHolder {
        public final ListView contactList;
        public final EditText phoneNumber;
        public final ImageButton button0;
        public final ImageButton button1;
        public final ImageButton button2;
        public final ImageButton button3;
        public final ImageButton button4;
        public final ImageButton button5;
        public final ImageButton button6;
        public final ImageButton button7;
        public final ImageButton button8;
        public final ImageButton button9;
        public final ImageButton buttonDelete;
        public final View dialerView;
        public final View dialerExpandMenu;

        public ViewHolder() {
            contactList = (ListView) findViewById(R.id.ContactListView);
            phoneNumber = (EditText) findViewById(R.id.EditTextPhoneNumber);
            button0 = (ImageButton) findViewById(R.id.Button0);
            button1 = (ImageButton) findViewById(R.id.Button1);
            button2 = (ImageButton) findViewById(R.id.Button2);
            button3 = (ImageButton) findViewById(R.id.Button3);
            button4 = (ImageButton) findViewById(R.id.Button4);
            button5 = (ImageButton) findViewById(R.id.Button5);
            button6 = (ImageButton) findViewById(R.id.Button6);
            button7 = (ImageButton) findViewById(R.id.Button7);
            button8 = (ImageButton) findViewById(R.id.Button8);
            button9 = (ImageButton) findViewById(R.id.Button9);
            buttonDelete = (ImageButton) findViewById(R.id.ButtonDelete);
            dialerView = findViewById(R.id.DialerView);
            dialerExpandMenu = findViewById(R.id.DialerExpandMenu);

        }
    }
}