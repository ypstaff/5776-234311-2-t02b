package yearlyproject.phonedialer;

import java.io.InputStream;
import java.lang.ref.SoftReference;
import java.util.HashMap;

import android.content.Context;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.provider.ContactsContract.Contacts;
import android.provider.ContactsContract.PhoneLookup;

/**
 * @author Lior Volin
 * Based on an open source project.
 */
class AsyncContactImageLoader {

    public interface ImageCallback {
        void imageLoaded(Drawable imageDrawable, String phoneNumber);
    }

    private class BackgroundImageLoader extends Thread {
        public Handler mHandler;

        public BackgroundImageLoader() {
        }

        @Override
        public void run() {
            Looper.prepare();
            mHandler = new Handler();
            Looper.loop();
        }
    }

    private static final int IMAGECACHE_INITIAL_CAPACITY = 256;

    private final Context mContext;
    private final Drawable mDefaultDrawable;
    private final HashMap<String, SoftReference<Drawable>> mImageCache;
    private final Handler mHandler;
    private final BackgroundImageLoader mBackgroundImageLoader;

    public AsyncContactImageLoader(Context context, Drawable defaultDrawable) {
        mContext = context;
        mDefaultDrawable = defaultDrawable;
        mImageCache = new HashMap<>(IMAGECACHE_INITIAL_CAPACITY);
        mHandler = new Handler();
        mBackgroundImageLoader = new BackgroundImageLoader();
        mBackgroundImageLoader.start();
    }

    Drawable loadImageForContact(String lookupKey) {
        Uri contactUri = Contacts.lookupContact(mContext.getContentResolver(), Uri.withAppendedPath(Contacts.CONTENT_LOOKUP_URI, lookupKey));

        if (contactUri == null)
            return mDefaultDrawable;

        InputStream contactImageStream = Contacts.openContactPhotoInputStream(mContext.getContentResolver(), contactUri);
        return contactImageStream == null ? mDefaultDrawable : Drawable.createFromStream(contactImageStream, "contact_image");
    }

    Drawable loadImageForNumber(String number) {
        try {
            Uri uri = Uri.withAppendedPath(PhoneLookup.CONTENT_FILTER_URI, Uri.encode(number));
            Cursor cursor = mContext.getContentResolver().query(uri, new String[]{PhoneLookup.LOOKUP_KEY}, null, null, null);
            if (cursor == null || !cursor.moveToFirst())
                return mDefaultDrawable;
            String lookupKey = cursor.getString(0);
            cursor.close();
            return loadImageForContact(lookupKey);
        } catch (Exception e) {
            return null;
        }
    }

    public Drawable loadDrawableForContact(final String lookupKey, final ImageCallback c) {
        SoftReference<Drawable> softReference = mImageCache.get(lookupKey);
        Drawable $ = softReference == null ? null : softReference.get();
        if ($ != null)
            return $;
        // Run in the background thread
        mBackgroundImageLoader.mHandler.postAtFrontOfQueue(new Runnable() {

            @Override
            public void run() {
                final Drawable d = loadImageForContact(lookupKey);
                // Run in the UI-thread
                AsyncContactImageLoader.this.mHandler.post(new Runnable() {

                    @Override
                    public void run() {
                        mImageCache.put(lookupKey, new SoftReference<>(d));
                        c.imageLoaded(d, lookupKey);
                    }
                });
            }
        });
        return mDefaultDrawable;
    }

    public Drawable loadDrawableForNumber(final String number, final ImageCallback c) {
        SoftReference<Drawable> softReference = mImageCache.get(number);
        Drawable $ = softReference == null ? null : softReference.get();
        if ($ != null)
            return $;
        // Run in the background thread
        mBackgroundImageLoader.mHandler.postAtFrontOfQueue(new Runnable() {

            @Override
            public void run() {
                final Drawable d = loadImageForNumber(number);
                // Run in the UI-thread
                AsyncContactImageLoader.this.mHandler.post(new Runnable() {

                    @Override
                    public void run() {
                        mImageCache.put(number, new SoftReference<>(d));
                        c.imageLoaded(d, number);
                    }
                });
            }
        });
        return mDefaultDrawable;
    }
}