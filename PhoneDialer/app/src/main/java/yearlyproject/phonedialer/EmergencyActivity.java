package yearlyproject.phonedialer;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.res.ResourcesCompat;
import android.telephony.PhoneNumberUtils;
import android.telephony.SmsManager;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import yearlyproject.navigationlibrary.EmergencyCallActivity;
import yearlyproject.navigationlibrary.IterableGroup;
import yearlyproject.navigationlibrary.IterableSingleView;
import yearlyproject.navigationlibrary.IterableViewGroup;
import yearlyproject.navigationlibrary.IterableViewGroups;

/**
 * @author Lior Volin
 */
public class EmergencyActivity extends EmergencyCallActivity implements TextView.OnEditorActionListener {

    public static final int PICK_EMERGENCY_CALL_NUMBER = 1;
    public static final int PICK_EMERGENCY_MESSAGE_NUMBER_1 = 2;
    public static final int PICK_EMERGENCY_MESSAGE_NUMBER_2 = 3;
    public static final int PICK_EMERGENCY_MESSAGE_NUMBER_3 = 4;

    EditText et_emergency_call;
    ImageButton ib_emergency_call;
    EditText et_emergency_message_content;
    EditText et_emergency_message_number1;
    ImageButton ib_emergency_message_number1;
    EditText et_emergency_message_number2;
    ImageButton ib_emergency_message_number2;
    EditText et_emergency_message_number3;
    ImageButton ib_emergency_message_number3;
    Button b_emergency_save;

    String emergencyCallNumber;
    String emergencyMessageContent;
    String emergencyMessageNumber1;
    String emergencyMessageNumber2;
    String emergencyMessageNumber3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_emergency);

        loadEmergencyNumbers();

        parseIntent(getIntent());

        findViews();

        setListeners();

        setThreeButtonsNavigation();

        showSavesEmergencyNumbers();
    }

    private void showSavesEmergencyNumbers() {
        et_emergency_call.setText(emergencyCallNumber);
        et_emergency_message_content.setText(emergencyMessageContent);
        et_emergency_message_number1.setText(emergencyMessageNumber1);
        et_emergency_message_number2.setText(emergencyMessageNumber2);
        et_emergency_message_number3.setText(emergencyMessageNumber3);
    }

    private void setThreeButtonsNavigation() {
        //Setting the 3 buttons navigation system for the activity
        fab_left = (FloatingActionButton) findViewById(R.id.fab_left);
        fab_right = (FloatingActionButton) findViewById(R.id.fab_right);
        fab_middle = (FloatingActionButton) findViewById(R.id.fab_middle);

        //Setting the action buttons to be semi transparent
        fab_left.setAlpha((float) 0.5);
        fab_middle.setAlpha((float) 0.5);
        fab_right.setAlpha((float) 0.5);

        //Setting the listeners for the navigation buttons
        fab_left.setOnClickListener(this);
        fab_right.setOnClickListener(this);
        fab_middle.setOnClickListener(this);

        fab_left.setOnLongClickListener(this);
        fab_right.setOnLongClickListener(this);
        fab_middle.setOnLongClickListener(this);

        //Creating the navigation group for the dialpad for when the app starts
        group = createNavigationGroup();
        group.start();
    }

    private void setListeners() {
        et_emergency_call.setOnClickListener(this);
        et_emergency_call.setOnEditorActionListener(this);
        ib_emergency_call.setOnClickListener(this);
        et_emergency_message_content.setOnClickListener(this);
        et_emergency_message_content.setOnEditorActionListener(this);
        et_emergency_message_number1.setOnClickListener(this);
        et_emergency_message_number1.setOnEditorActionListener(this);
        ib_emergency_message_number1.setOnClickListener(this);
        et_emergency_message_number2.setOnClickListener(this);
        et_emergency_message_number2.setOnEditorActionListener(this);
        ib_emergency_message_number2.setOnClickListener(this);
        et_emergency_message_number3.setOnClickListener(this);
        et_emergency_message_number3.setOnEditorActionListener(this);
        ib_emergency_message_number3.setOnClickListener(this);
        b_emergency_save.setOnClickListener(this);
    }

    private void findViews() {
        et_emergency_call = (EditText) findViewById(R.id.et_emergency_call);
        ib_emergency_call = (ImageButton) findViewById(R.id.ib_emergency_call);
        et_emergency_message_content = (EditText) findViewById(R.id.et_emergency_message_content);
        et_emergency_message_number1 = (EditText) findViewById(R.id.et_emergency_message_number1);
        ib_emergency_message_number1 = (ImageButton) findViewById(R.id.ib_emergency_message_number1);
        et_emergency_message_number2 = (EditText) findViewById(R.id.et_emergency_message_number2);
        ib_emergency_message_number2 = (ImageButton) findViewById(R.id.ib_emergency_message_number2);
        et_emergency_message_number3 = (EditText) findViewById(R.id.et_emergency_message_number3);
        ib_emergency_message_number3 = (ImageButton) findViewById(R.id.ib_emergency_message_number3);
        b_emergency_save = (Button) findViewById(R.id.b_emergency_save);
    }

    private void loadEmergencyNumbers() {
        SharedPreferences sharedPref = getPreferences(Context.MODE_PRIVATE);
        emergencyCallNumber = sharedPref.getString(getString(R.string.saved_emergency_call_number), "");
        emergencyMessageContent = sharedPref.getString(getString(R.string.saved_emergency_message_content), "");
        emergencyMessageNumber1 = sharedPref.getString(getString(R.string.saved_emergency_message_number_1), "");
        emergencyMessageNumber2 = sharedPref.getString(getString(R.string.saved_emergency_message_number_2), "");
        emergencyMessageNumber3 = sharedPref.getString(getString(R.string.saved_emergency_message_number_3), "");
    }

    // parse the incoming intent and take specific action if needed
    private void parseIntent(Intent i) {
        if (!"yearlyproject.phonedialer.ACTION_EMERGENCY".equals(i.getAction()))
            return;
        if (emergencyCallNumber.length() != 0)
            callNumberAndFinish(emergencyCallNumber);
        else
            Toast.makeText(this, "Emergency call number was not set yet", Toast.LENGTH_LONG).show();
        if (emergencyMessageNumber1.isEmpty() && emergencyMessageNumber2.isEmpty() && emergencyMessageNumber3.isEmpty())
            Toast.makeText(this, "Emergency message number was not set yet", Toast.LENGTH_LONG).show();
        else if (emergencyMessageContent.isEmpty())
            Toast.makeText(this, "Emergency message content was not set yet", Toast.LENGTH_LONG).show();
        else {
            SmsManager smsManager = SmsManager.getDefault();
            if (!emergencyMessageNumber1.isEmpty())
                smsManager.sendTextMessage(emergencyMessageNumber1, null, emergencyMessageContent, null, null);
            if (!emergencyMessageNumber2.isEmpty())
                smsManager.sendTextMessage(emergencyMessageNumber2, null, emergencyMessageContent, null, null);
            if (!emergencyMessageNumber3.isEmpty())
                smsManager.sendTextMessage(emergencyMessageNumber3, null, emergencyMessageContent, null, null);
        }
        finish();
    }

    private void callNumberAndFinish(CharSequence number) {
        if (number == null || number.length() == 0) {
            Toast.makeText(this, R.string.contact_has_no_phone_number, Toast.LENGTH_SHORT).show();
            return;
        }

        Uri uri = Uri.parse("tel:" + Uri.encode(number.toString()));
        Intent intent = new Intent(Intent.ACTION_CALL, uri);
        startActivity(intent);
        finish();
    }

    //Creates the navigation group
    private IterableGroup createNavigationGroup() {
        Drawable black_border = ResourcesCompat.getDrawable(
                this.getResources(), R.drawable.black_border, null);

        IterableGroup row1_group = createOneRowGroup(et_emergency_call, ib_emergency_call, R.id.emergency_row1);

        IterableGroup row2_group = createOneRowGroup(et_emergency_message_content);
        IterableGroup row3_group = createOneRowGroup(et_emergency_message_number1, ib_emergency_message_number1, R.id.emergency_row3);
        IterableGroup row4_group = createOneRowGroup(et_emergency_message_number2, ib_emergency_message_number2, R.id.emergency_row4);
        IterableGroup row5_group = createOneRowGroup(et_emergency_message_number3, ib_emergency_message_number3, R.id.emergency_row5);

        List<IterableGroup> middle_group_list = new ArrayList<>();
        middle_group_list.add(row2_group);
        middle_group_list.add(row3_group);
        middle_group_list.add(row4_group);
        middle_group_list.add(row5_group);
        IterableGroup middle_group = new IterableViewGroups(
                middle_group_list, findViewById(R.id.emergency_middle_group), this,
                black_border, black_border, Color.BLUE);

        IterableGroup row6_group = createOneRowGroup(b_emergency_save);

        List<IterableGroup> emergency_list = new ArrayList<>();
        emergency_list.add(row1_group);
        emergency_list.add(middle_group);
        emergency_list.add(row6_group);
        return new IterableViewGroups(
                emergency_list, findViewById(R.id.rl_emergency), this,
                black_border, black_border, Color.BLUE);
    }

    IterableGroup createOneRowGroup(View v) {
        Drawable black_border = ResourcesCompat.getDrawable(
                this.getResources(), R.drawable.black_border, null);

        return new IterableSingleView(v, this,
                black_border, black_border, Color.BLUE);
    }

    IterableGroup createOneRowGroup(View view1, View view2, int row_id) {
        Drawable black_border = ResourcesCompat.getDrawable(
                this.getResources(), R.drawable.black_border, null);

        List<View> row_list = new ArrayList<>();
        row_list.add(view1);
        row_list.add(view2);

        return new IterableViewGroup(row_list, findViewById(row_id), this,
                black_border, black_border, Color.BLUE);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);

        switch (v.getId()) {

            case R.id.et_emergency_call:
                onEditTextPressed(et_emergency_call);
                break;

            case R.id.ib_emergency_call:
                onImageButtonPhoneBookPressed(PICK_EMERGENCY_CALL_NUMBER);
                break;

            case R.id.et_emergency_message_content:
                onEditTextPressed(et_emergency_message_content);
                break;

            case R.id.et_emergency_message_number1:
                onEditTextPressed(et_emergency_message_number1);
                break;

            case R.id.ib_emergency_message_number1:
                onImageButtonPhoneBookPressed(PICK_EMERGENCY_MESSAGE_NUMBER_1);
                break;

            case R.id.et_emergency_message_number2:
                onEditTextPressed(et_emergency_message_number2);
                break;

            case R.id.ib_emergency_message_number2:
                onImageButtonPhoneBookPressed(PICK_EMERGENCY_MESSAGE_NUMBER_2);
                break;

            case R.id.et_emergency_message_number3:
                onEditTextPressed(et_emergency_message_number3);
                break;

            case R.id.ib_emergency_message_number3:
                onImageButtonPhoneBookPressed(PICK_EMERGENCY_MESSAGE_NUMBER_3);
                break;

            case R.id.b_emergency_save:
                setEmergencyNumbers();
                finish();
                break;
        }
    }

    private void onEditTextPressed(EditText t) {
        t.setFocusableInTouchMode(true);
        t.requestFocus();
        showSoftKey(t);
        t.setSelection(t.getText().length());
    }

    private void onImageButtonPhoneBookPressed(int pickCode) {
        //Starting the contacts activity from the messages app for result - the phone
        // number of the chosen contact
        try {
            Intent intent = new Intent();
            intent.setClassName("yearlyproject.messages", "yearlyproject.messages.ContactsActivity");
            startActivityForResult(intent, pickCode);
        } catch (ActivityNotFoundException anfe) {
            Toast.makeText(this, "You need to install the Messages app in order to use that functionality", Toast.LENGTH_LONG).show();
        } catch (Exception e) {
            Toast.makeText(this, "You can't use that functionality", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        switch (requestCode) {

            case PICK_EMERGENCY_CALL_NUMBER:
                onPickContactRequestResult(resultCode, data, et_emergency_call);
                break;

            case PICK_EMERGENCY_MESSAGE_NUMBER_1:
                onPickContactRequestResult(resultCode, data, et_emergency_message_number1);
                break;

            case PICK_EMERGENCY_MESSAGE_NUMBER_2:
                onPickContactRequestResult(resultCode, data, et_emergency_message_number2);
                break;

            case PICK_EMERGENCY_MESSAGE_NUMBER_3:
                onPickContactRequestResult(resultCode, data, et_emergency_message_number3);
                break;

            default:
                break;
        }
    }

    //When returning from the contacts activity
    private void onPickContactRequestResult(int resultCode, Intent data, EditText editText) {
        if (resultCode == RESULT_OK) {
            String phoneNumber = data.getStringExtra("phone number");
            editText.setText(phoneNumber);
        }
    }

    //Setting the emergency numbers and saving them
    private void setEmergencyNumbers() {
        emergencyCallNumber = updateEmergencyNumber(et_emergency_call, "emergency call number", R.string.saved_emergency_call_number, emergencyCallNumber);
        updateEmergencyMessageContent();
        emergencyMessageNumber1 = updateEmergencyNumber(et_emergency_message_number1, "emergency message number 1", R.string.saved_emergency_message_number_1, emergencyMessageNumber1);
        emergencyMessageNumber2 = updateEmergencyNumber(et_emergency_message_number2, "emergency message number 2", R.string.saved_emergency_message_number_2, emergencyMessageNumber2);
        emergencyMessageNumber3 = updateEmergencyNumber(et_emergency_message_number3, "emergency message number 3", R.string.saved_emergency_message_number_3, emergencyMessageNumber3);
    }

    String updateEmergencyNumber(EditText etEmergencyNumber, String descriptionToPrint, int stringResource, String currentEmergencyNumber) {
        String $ = etEmergencyNumber.getText().toString();
        if (!PhoneNumberUtils.isGlobalPhoneNumber($) && !$.isEmpty()) {
            Toast.makeText(this, "Illegal " + descriptionToPrint + ". The number has not been changed",
                    Toast.LENGTH_LONG).show();
            return currentEmergencyNumber;
        }
        SharedPreferences sharedPref = getPreferences(Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(getString(stringResource), $);
        editor.apply();
        if ($.isEmpty() && !currentEmergencyNumber.isEmpty())
            Toast.makeText(this, descriptionToPrint + " has been deleted", Toast.LENGTH_LONG).show();
        else if (!$.equals(currentEmergencyNumber))
            Toast.makeText(this, descriptionToPrint + " has been set to " + $, Toast.LENGTH_LONG).show();
        return $;
    }

    //Saving the new emergency message content
    void updateEmergencyMessageContent() {
        String newEmergencyMessageContent = et_emergency_message_content.getText().toString();
        SharedPreferences sharedPref = getPreferences(Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(getString(R.string.saved_emergency_message_content), newEmergencyMessageContent);
        editor.apply();
        if (newEmergencyMessageContent.isEmpty() && !emergencyMessageContent.isEmpty())
            Toast.makeText(this, "Emergency message content has been deleted", Toast.LENGTH_LONG).show();
        else if (!newEmergencyMessageContent.equals(emergencyMessageContent))
            Toast.makeText(this, "Emergency message content has been set to " + newEmergencyMessageContent,
                    Toast.LENGTH_LONG).show();
        emergencyMessageContent = newEmergencyMessageContent;
    }

    //For the keyboard bluetooth integration - unregisters the keyboard from the bluetooth service
    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        if (actionId == EditorInfo.IME_ACTION_SEND || actionId == EditorInfo.IME_ACTION_NEXT
                || actionId == EditorInfo.IME_ACTION_DONE)
            hideSoftKey();
        return false;
    }
}
