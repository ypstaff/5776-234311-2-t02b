package yearlyproject.phonedialer;

import android.os.Bundle;
import android.preference.PreferenceActivity;

/**
 * @author Lior Volin
 * Based on an open source project.
 */
public class DialerPreferenceActivity extends PreferenceActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences);
    }
}
