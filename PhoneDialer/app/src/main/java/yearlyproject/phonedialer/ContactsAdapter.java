package yearlyproject.phonedialer;

import android.content.Context;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import yearlyproject.phonedialer.AsyncContactImageLoader.ImageCallback;

/**
 * @author Lior Volin
 * Based on an open source project.
 */
public class ContactsAdapter extends CursorAdapter {

    public static final String[] PROJECTION = new String[]{
            Phone._ID,
            Phone.LOOKUP_KEY,
            Phone.DISPLAY_NAME,
            Phone.CONTACT_STATUS,
            Phone.TIMES_CONTACTED,
            Phone.LAST_TIME_CONTACTED,
            Phone.STARRED
    };
    public static final int COLUMN_LOOKUP_KEY = 1;
    public static final int COLUMN_DISPLAY_NAME = 2;
    public static final int COLUMN_CONTACT_STATUS = 3;
    public static final int COLUMN_TIMES_CONTACTED = 4;
    public static final int COLUMN_LAST_TIME_CONTACTED = 5;
    public static final int COLUMN_STARRED = 6;

    private class ViewCache {
        public final TextView contactName;
        public final TextView contactLastDialed;
        public final TextView contactInformation;
        public final TextView contactCallCount;
        public final ImageView contactImage;
        public final ImageView contactStarred;

        public ViewCache(View base) {
            contactName = (TextView) base.findViewById(R.id.ContactName);
            contactLastDialed = (TextView) base.findViewById(R.id.ContactLastDialed);
            contactInformation = (TextView) base.findViewById(R.id.ContactInformation);
            contactCallCount = (TextView) base.findViewById(R.id.ContactCallCount);
            contactImage = (ImageView) base.findViewById(R.id.ContactImage);
            contactStarred = (ImageView) base.findViewById(R.id.ContactStarred);
        }

    }

    private Resources mResources;
    private AsyncContactImageLoader mAsyncContactImageLoader;
    private boolean mShowCallCounter;

    public ContactsAdapter(Context context, Cursor cursor, AsyncContactImageLoader asyncContactImageLoader) {
        super(context, cursor);
        mResources = context.getResources();
        mAsyncContactImageLoader = asyncContactImageLoader;
        mShowCallCounter = false;
    }

    @Override
    public View newView(Context c, Cursor cursor, ViewGroup parent) {

        View $ = LayoutInflater.from(c).inflate(R.layout.contact_item, null);
        ViewCache viewCache = new ViewCache($);
        $.setTag(viewCache);

        return $;
    }

    @Override
    public void bindView(View v, Context c, Cursor cursor) {
        final ViewCache viewCache = (ViewCache) v.getTag();
        // empty view has no viewcache, and we do nothing with it
        if (viewCache == null)
            return;
        String displayName = cursor.getString(COLUMN_DISPLAY_NAME) == null ? "" : cursor.getString(COLUMN_DISPLAY_NAME);
        viewCache.contactName.setText(displayName);

        long lastTimeContacted = cursor.getLong(COLUMN_LAST_TIME_CONTACTED);
        viewCache.contactLastDialed.setText(lastTimeContacted == 0 ? mResources.getString(R.string.not_contacted)
                : DateUtils.getRelativeTimeSpanString(lastTimeContacted));

        String status = cursor.getString(COLUMN_CONTACT_STATUS) == null ? "" : cursor.getString(COLUMN_CONTACT_STATUS);
        if (status.length() > 137)
            status = status.substring(0, 137) + "...";
        status = status.replace('\n', ' ');
        viewCache.contactInformation.setText(status);

        int timesContacted = cursor.getInt(COLUMN_TIMES_CONTACTED);
        viewCache.contactCallCount.setText(!mShowCallCounter ? "" : "(" + timesContacted + ")");

        int starred = cursor.getInt(COLUMN_STARRED);
        viewCache.contactStarred.setVisibility(starred != 1 ? View.GONE : View.VISIBLE);

        String lookupKey = cursor.getString(COLUMN_LOOKUP_KEY) == null ? "" : cursor.getString(COLUMN_LOOKUP_KEY);
        // set a tag for the callback to be able to check, so we don't set the contact image of a reused view
        viewCache.contactImage.setTag(lookupKey);
        viewCache.contactImage
                .setImageDrawable(mAsyncContactImageLoader.loadDrawableForContact(lookupKey, new ImageCallback() {
                    @Override
                    public void imageLoaded(Drawable imageDrawable, String lookupKey) {
                        if (lookupKey.equals(viewCache.contactImage.getTag()))
                            viewCache.contactImage.setImageDrawable(imageDrawable);
                    }
                }));
    }

    public void setShowCallCounter(boolean showCallCounter) {
        mShowCallCounter = showCallCounter;
    }
}