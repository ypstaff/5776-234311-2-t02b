package yearlyproject.bluetoothApp;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.IntentService;
import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

/**
 * Created by lmaman on 09-Dec-15.
 */
public class BluetoothMasterHandler extends Service {
	private static final String TAG = BluetoothMasterHandler.class
			.getSimpleName();

	private final BroadcastReceiver discoverChangeReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context c, Intent i) {
			if (!BluetoothAdapter.ACTION_SCAN_MODE_CHANGED
					.equals(i.getAction()))
				return;
			int previousState = i.getIntExtra(
					BluetoothAdapter.EXTRA_PREVIOUS_SCAN_MODE,
					BluetoothAdapter.SCAN_MODE_NONE);
			int currentState = i.getIntExtra(BluetoothAdapter.EXTRA_SCAN_MODE,
					BluetoothAdapter.SCAN_MODE_NONE);
			if (currentState != BluetoothAdapter.SCAN_MODE_NONE
					&& currentState != BluetoothAdapter.STATE_OFF)
				return;
			toastToActivity("Bluetooth state is off, killing server.");
			stopSelf();
		}
	};

	private BluetoothServerSocket mmServerSocket;
	private BluetoothSocket mBluetoothSocket;
	private Thread connectedThread;
	private Thread connectingThread;

	public void run() {
		Log.d(TAG, "BluetoothMasterHandler.run() Waiting for connection");
		BluetoothSocket socket;
		// Keep listening until exception occurs or a socket is returned
		while (!Thread.interrupted()) {
			try {
				socket = mmServerSocket.accept();
			} catch (IOException e) {
				break;
			}
			// If a connection was accepted
			if (socket != null) {
				// Do work to manage the connection (in a separate thread)
				// Cancel discovery because it will slow down the connection
				BluetoothAdapter.getDefaultAdapter().cancelDiscovery();
				manageConnectedSocket(socket);
				try {
					mmServerSocket.close();
				} catch (IOException e) {
					e.printStackTrace();
					toastToActivity("Could not connect.");
				}
				break;
			}
		}
	}

	private void manageConnectedSocket(BluetoothSocket s) {
		// Toast.makeText(this, "Connected to a client device.",
		// Toast.LENGTH_SHORT).show();
		Log.d(TAG, "manageConnectedSocket() Managing connection,server side");
		mBluetoothSocket = s;
		sendBroadcast(new Intent(BluetoothConnector.BT_CONNECTED));
		connectedThread = new Thread(new Runnable() {
			@Override
			public void run() {
				Log.d(TAG, "Listening thread started ");
				while (!Thread.interrupted()) {
					Log.d(TAG, "trying to get message");
					byte[] buffer = new byte[1024];
					int resVal = 0;
					try {
						resVal = mBluetoothSocket.getInputStream().read(buffer); // blocking
																					// call
					} catch (IOException e) {
						e.printStackTrace();
					}
					String msg = null;
					try {
						msg = new String(buffer, "UTF-8");
					} catch (UnsupportedEncodingException e) {
						e.printStackTrace();
					}
					if (Thread.interrupted() || resVal == 0) {
						stopSelf();
						break;
					}
					Log.d(TAG, "got message");
					Intent intent = new Intent(
							BluetoothConnector.BT_MESSAGE_RECEIVED);
					intent.putExtra("msg", msg);

					BluetoothMasterHandler.this.sendBroadcast(intent);

				}
				cancel();
			}
		});
		connectedThread.start();
	}

	/**
	 * Will cancel the listening socket, and cause the thread to finish
	 */
	private void cancel() {
		try {
			mmServerSocket.close();
		} catch (IOException e) {
		}
		Log.d(TAG, "master.cancel() Killing master");
		stopSelf();
	}

	@Override
	public void onDestroy() {
		Toast.makeText(this,
				"Service died. Bluetooth connection is terminated.",
				Toast.LENGTH_LONG).show();
		if (mBluetoothSocket != null)
			try {
				mBluetoothSocket.getInputStream().close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		if (connectedThread != null)
			connectedThread.interrupt();
		else if (connectingThread != null)
			connectingThread.interrupt();
		sendBroadcast(new Intent(BluetoothConnector.BT_DISCONNECTED));
		unregisterReceiver(discoverChangeReceiver);
	}

	@Nullable
	// no need to ever bind
	@Override
	public IBinder onBind(Intent i) {
		return null;
	}

	@Override
	public int onStartCommand(Intent i, int flags, int startId) {
		try {
			mmServerSocket = BluetoothAdapter.getDefaultAdapter()
					.listenUsingRfcommWithServiceRecord(
							BluetoothConnector.serverName,
							BluetoothConnector.uuid);
		} catch (IOException e) {
		}

		IntentFilter filter = new IntentFilter(
				BluetoothAdapter.ACTION_SCAN_MODE_CHANGED);
		registerReceiver(discoverChangeReceiver, filter);

		toastToActivity("Connected as a server.");

		connectingThread = new Thread(new Runnable() {
			@Override
			public void run() {
				BluetoothMasterHandler.this.run();
			}
		});
		connectingThread.start();
		return START_STICKY;
	}

	public synchronized void toastToActivity(final String message) {
		Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
	}

}
