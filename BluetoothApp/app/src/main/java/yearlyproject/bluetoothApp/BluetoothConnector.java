package yearlyproject.bluetoothApp;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

import java.io.IOException;
//import java.io.Serializable;
import java.util.UUID;

//import org.apache.commons.lang.SerializationUtils;

/**
 * Created by lmaman on 08-Dec-15.
 */
public class BluetoothConnector {

	private static final String TAG = BluetoothConnector.class.getSimpleName();
	static final int REQUEST_ENABLE_BT = 1;
	static final String BT_MESSAGE_SEND = "BluetoothConnector.BT_MESSAGE_SEND";
	static final String BT_MESSAGE_RECEIVED = "BluetoothConnector.BT_MESSAGE_RECEIVED";
	static final UUID uuid = UUID
			.fromString("00001101-0000-1000-8000-00805F9B34FB");
	static final String serverName = "3-Buttons";
	// special BT connection flags
	public static final String BT_CONNECTED = "BluetoothConnector.BT_CONNECTED";
	public static final String BT_DISCONNECTED = "BluetoothConnector.BT_DISCONNECTED";

	// Parent app related variables
	private Context activityContext;

	private BluetoothAdapter mBluetoothAdapter = BluetoothAdapter
			.getDefaultAdapter();
	private BluetoothSlaveHandler bluetoothSlaveHandler;

	public void initiate(Context context_) {
		this.activityContext = context_;

		if (mBluetoothAdapter == null)
			toastToActivity("Running on emulator, no Bluetooth capabilities.");

		if (mBluetoothAdapter.isEnabled())
			((BluetoothAbleActivity) activityContext).BluetoothOn();
		else
			((Activity) activityContext)
					.startActivityForResult((new Intent(
							BluetoothAdapter.ACTION_REQUEST_ENABLE)),
							REQUEST_ENABLE_BT);

	}

	public void discoverDevices() {

		if (mBluetoothAdapter.isDiscovering())
			mBluetoothAdapter.cancelDiscovery();

		if (mBluetoothAdapter.startDiscovery())
			toastToActivity("Started searching for nearby devices.");
	}

	public void runServer() {
		// Run server on a different thread as service
		activityContext.startService(new Intent(activityContext,
				BluetoothMasterHandler.class));
	}

	public void connectAsServer(int duration) {
		setDiscoverable(duration);
		// Wait for an onActivityResult to runServer
	}

	public void connectAsClient(BluetoothDevice mBluetoothDevice)
			throws IOException {
		bluetoothSlaveHandler = new BluetoothSlaveHandler(this,
				mBluetoothDevice);
		bluetoothSlaveHandler.start();
	}

	public void setDiscoverable(int duration) {
		// Set device as discoverable
		Intent discoverableIntent = new Intent(
				BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
		discoverableIntent.putExtra(
				BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, duration);
		((Activity) activityContext).startActivityForResult(discoverableIntent,
				duration);
	}

	private void killClient() {
		if (bluetoothSlaveHandler != null)
			bluetoothSlaveHandler.interrupt();
		bluetoothSlaveHandler = null;
	}

	public void onDestroy() {
		cancelDiscovery();
		killClient();
	}

	public void toastToActivity(final String message) {
		if (activityContext == null)
			return;
		((Activity) activityContext).runOnUiThread(new Runnable() {
			@Override
			public void run() {
				((BluetoothAbleActivity) activityContext)
						.toastToActivity(message);
			}
		});
	}

	public void cancelDiscovery() {
		if (mBluetoothAdapter.isDiscovering())
			mBluetoothAdapter.cancelDiscovery();
	}

	public void send(String msg) {
		// send msg, intent
		Intent intent = new Intent(BluetoothConnector.BT_MESSAGE_SEND);
		intent.putExtra("msg", msg);
		activityContext.sendBroadcast(intent);
	}

	// Getters
	public Context getActivityContext() {
		return activityContext;
	}

}
