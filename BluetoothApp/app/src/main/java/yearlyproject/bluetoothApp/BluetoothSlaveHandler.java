package yearlyproject.bluetoothApp;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.util.Log;

import java.io.IOException;

/**
 * Created by lmaman on 09-Dec-15.
 */
public class BluetoothSlaveHandler extends Thread {
	private static final String TAG = BluetoothSlaveHandler.class
			.getSimpleName();
	BluetoothConnector mBluetoothConnector;
	final BluetoothSocket mBluetoothSocket;

	private final BroadcastReceiver btMessageSender = new BroadcastReceiver() {
		@Override
		public void onReceive(Context c, Intent i) {
			if (!BluetoothConnector.BT_MESSAGE_SEND.equals(i.getAction()))
				return;
			String msg = i.getStringExtra("msg");
			Log.d(TAG, "Trying to send message " + msg);
			try {
				mBluetoothSocket.getOutputStream().write(msg.getBytes("UTF-8"));
				Log.d(TAG, "sent message " + msg);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	};

	private final BroadcastReceiver btKilled = new BroadcastReceiver() {
		@Override
		public void onReceive(Context c, Intent i) {
			if (!BluetoothAdapter.ACTION_CONNECTION_STATE_CHANGED.equals(i
					.getAction()))
				return;
			int currentState = i.getIntExtra(BluetoothAdapter.EXTRA_SCAN_MODE,
					BluetoothAdapter.SCAN_MODE_NONE);
			if (currentState != BluetoothAdapter.SCAN_MODE_NONE
					&& currentState != BluetoothAdapter.STATE_OFF)
				return;
			BluetoothSlaveHandler.this.mBluetoothConnector
					.toastToActivity("Connection ended.");
			BluetoothSlaveHandler.this.interrupt();
		}
	};

	public BluetoothSlaveHandler(BluetoothConnector bluetoothConnector,
			BluetoothDevice mBluetoothDevice) {
		mBluetoothConnector = bluetoothConnector;
		BluetoothSocket tmp = null;
		try {
			tmp = mBluetoothDevice
					.createRfcommSocketToServiceRecord(BluetoothConnector.uuid);
		} catch (IOException e) {
		}
		mBluetoothSocket = tmp;

		mBluetoothConnector.getActivityContext().registerReceiver(
				btKilled,
				(new IntentFilter(
						BluetoothAdapter.ACTION_CONNECTION_STATE_CHANGED)));
	}

	@Override
	public void run() {
		// Cancel discovery because it will slow down the connection
		mBluetoothConnector.cancelDiscovery();

		try {
			// Connect the device through the socket. This will block
			// until it succeeds or throws an exception
			mBluetoothSocket.connect();
		} catch (IOException connectException) {
			// Unable to connect; close the socket and get out
			try {
				mBluetoothSocket.close();
			} catch (IOException closeException) {
				closeException.printStackTrace();
			}
			mBluetoothConnector
					.toastToActivity("Could not connect to device. Try again.");
			return;
		}

		// Do work to manage the connection (in a separate thread)
		manageConnectedSocket();
	}

	private void manageConnectedSocket() {
		mBluetoothConnector.toastToActivity("Connected to a server device.");
		Log.d(TAG, "manageConnectedSocket() Managing connection, client side.");
		IntentFilter filter = new IntentFilter(
				BluetoothConnector.BT_MESSAGE_SEND);
		mBluetoothConnector.getActivityContext().registerReceiver(
				btMessageSender, filter);
		((BluetoothAbleActivity) mBluetoothConnector.getActivityContext())
				.BluetoothConnected();

	}

	/** Will cancel an in-progress connection, and close the socket */
	private void cancel() {
		try {
			mBluetoothSocket.close();
		} catch (IOException e) {
		}
		try {
			(mBluetoothConnector.getActivityContext())
					.unregisterReceiver(btKilled);
			(mBluetoothConnector.getActivityContext())
					.unregisterReceiver(btMessageSender);
		} catch (IllegalArgumentException e) {
		}
		Log.d(TAG, "slave.cancel() Killing slave");
		mBluetoothConnector = null;
	}

	@Override
	public void interrupt() {
		if (mBluetoothConnector == null)
			return;
		try {
			mBluetoothSocket.getOutputStream().close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		cancel();
		super.interrupt();
	}

}
