package yearlyproject.bluetoothApp;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Switch;
import android.widget.Toast;

/**
 * Created by lmaman on 07-Dec-15.
 */

public class MainActivity extends AppCompatActivity implements
		BluetoothAbleActivity {
	private static final String TAG = MainActivity.class.getSimpleName();
	static int discoverableDuration = 300; // seconds

	// special BT connection receivers
	private final BroadcastReceiver btConnectedReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context c, Intent i) {
			if (BluetoothConnector.BT_CONNECTED.equals(i.getAction()))
				BluetoothConnected();
		}
	};

	private final BroadcastReceiver btDisconnectedReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context c, Intent i) {
			if (BluetoothConnector.BT_DISCONNECTED.equals(i.getAction()))
				connectionDisconnected();
		}
	};

	// general BT message receiver

	private final BroadcastReceiver btMessageReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context c, Intent i) {
			if (!BluetoothConnector.BT_MESSAGE_RECEIVED.equals(i.getAction()))
				return;
			performAction(i.getStringExtra("msg").split("\\u0000")[0]);
		}
	};

	BluetoothConnector bluetoothConnector = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// android.os.Debug.waitForDebugger();
		setContentView(R.layout.activity_main);

		registerReceivers();

		Button connectAsServerButton = (Button) findViewById(R.id.buttonServer);
		connectAsServerButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				bluetoothConnector = new BluetoothConnector();
				bluetoothConnector.initiate(MainActivity.this);
				findViewById(R.id.loadingPanel).setVisibility(View.VISIBLE);

			}
		});

		Button connectAsClientButton = (Button) findViewById(R.id.buttonClient);
		connectAsClientButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				stopService(new Intent(MainActivity.this,
						BluetoothMasterHandler.class));
				MainActivity.this.startActivity((new Intent(MainActivity.this,
						SlaveActivity.class)));
			}
		});

		findViewById(R.id.stopService).setOnClickListener(
				new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						connectionDisconnected();
					}
				});

		setDefaultSetup();
	}

	private void setDefaultSetup() {
		findViewById(R.id.loadingPanel).setVisibility(View.INVISIBLE);
		findViewById(R.id.buttonClient).setVisibility(View.VISIBLE);
		findViewById(R.id.buttonServer).setVisibility(View.VISIBLE);
	}

	private void registerReceivers() {
		registerReceiver(btConnectedReceiver, new IntentFilter(
				BluetoothConnector.BT_CONNECTED));
		registerReceiver(btDisconnectedReceiver, new IntentFilter(
				BluetoothConnector.BT_DISCONNECTED));
		registerReceiver(btMessageReceiver, new IntentFilter(
				BluetoothConnector.BT_MESSAGE_RECEIVED));
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == BluetoothConnector.REQUEST_ENABLE_BT) {
			if (resultCode == RESULT_OK)
				BluetoothOn();
			else {
				Toast.makeText(
						this,
						"Bluetooth was not allowed to turn on, no Bluetooth capabilities.",
						Toast.LENGTH_SHORT).show();
				Log.d(TAG,
						"onActivityResult(REQUEST_ENABLE_BT) got RESULT_CANCEL. Bluetooth was not allowed to turn on, no Bluetooth capabilities");
				connectionDisconnected();
			}
		}

		// Callback from setting on discoverablity
		if (requestCode == discoverableDuration) {
			if (resultCode != RESULT_CANCELED)
				bluetoothConnector.runServer();
			else {
				Toast.makeText(
						this,
						"Bluetooth was not allowed to make the device discoverable, can't open a Bluetooth server.",
						Toast.LENGTH_LONG).show();
				Log.d(TAG,
						"onActivityResult(SetDiscover) got RESULT_CANCEL. Bluetooth was not allowed to make the device discoverable, can't open a Bluetooth server.");
				connectionDisconnected();
			}

		}
	}

	public void BluetoothOn() {
		Toast.makeText(this, "Bluetooth turned on and ready.",
				Toast.LENGTH_SHORT).show();
		bluetoothConnector.connectAsServer(discoverableDuration);
	}

	@Override
	public void toastToActivity(final String message) {
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				Toast.makeText(MainActivity.this, message, Toast.LENGTH_SHORT)
						.show();
			}
		});

	}

	@Override
	public void BluetoothConnected() {
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				findViewById(R.id.buttonClient).setVisibility(View.INVISIBLE);
				findViewById(R.id.buttonServer).setVisibility(View.INVISIBLE);
				findViewById(R.id.loadingPanel).setVisibility(View.INVISIBLE);
			}
		});
		if (!((Switch) findViewById(R.id.switch1)).isChecked())
			finish();

	}

	@Override
	public void performAction(final String s) {
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				switch(s) {
					case SlaveActivity.NEXT:
						toastToActivity("Next");
						break;

					case SlaveActivity.SELECT:
						toastToActivity("Select");
						break;

					case SlaveActivity.BACK:
						toastToActivity("Back");
						break;

					case SlaveActivity.LONG_NEXT:
						toastToActivity("Long Next");
						break;

					case SlaveActivity.LONG_SELECT:
						toastToActivity("Long Select");
						break;

					case SlaveActivity.LONG_BACK:
						toastToActivity("Long Back");
						break;
				}
			}
		});
	}

	@Override
	public void connectionDisconnected() {
		stopService(new Intent(MainActivity.this, BluetoothMasterHandler.class));
		bluetoothConnector = null;
		setDefaultSetup();
	}

	@Override
	public void onPause() {
		unregisterReceiver(btMessageReceiver);
		super.onPause();
	}

	@Override
	public void onResume() {
		super.onResume();
		registerReceiver(btMessageReceiver, new IntentFilter(
				BluetoothConnector.BT_MESSAGE_RECEIVED));
	}

	@Override
	public void onDestroy() {
		if (bluetoothConnector != null)
			bluetoothConnector.onDestroy();
		super.onDestroy();
		unregisterReceivers();
	}

	private void unregisterReceivers() {
		unregisterReceiver(btConnectedReceiver);
		unregisterReceiver(btDisconnectedReceiver);
	}

}
