package yearlyproject.bluetoothApp;

import android.app.ListActivity;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class SlaveActivity extends ListActivity implements BluetoothAbleActivity{
    private static final String TAG = SlaveActivity.class.getSimpleName();
    protected static final String NEXT = "1";
    protected static final String SELECT = "2";
    protected static final String BACK = "3";
    protected static final String LONG_NEXT = "4";
    protected static final String LONG_SELECT = "5";
    protected static final String LONG_BACK = "6";

    ArrayAdapter<String> adapter;
    BluetoothConnector bluetoothConnector = new BluetoothConnector();
    final List<String> mArrayAdapter= new ArrayList<>();
    final List<BluetoothDevice> devices = new ArrayList<>();

    // Create a BroadcastReceiver for ACTION_FOUND
    final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        public void onReceive(Context c, Intent i) {
            if (!BluetoothDevice.ACTION_FOUND.equals(i.getAction()))
				return;
			BluetoothDevice device = i
					.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
			if (devices.contains(device))
				return;
			devices.add(device);
			mArrayAdapter.add(device.getName() + "\n" + device.getAddress());
			adapter.notifyDataSetChanged();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //android.os.Debug.waitForDebugger();
        setContentView(R.layout.activity_slave);

        setupDefault();

    }

    private void setupDefault() {
        adapter=new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1,
                mArrayAdapter);
        setListAdapter(adapter);

        bluetoothConnector.initiate(this);

        this.getListView().setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, final View v,
                                    int position, long id) {
                final String item = (String) parent.getItemAtPosition(position);
                Toast.makeText(SlaveActivity.this, "Selected: " + item, Toast.LENGTH_LONG).show();
                try {
                    bluetoothConnector.connectAsClient(devices.get(position));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

        findViewById(R.id.select).setVisibility(View.INVISIBLE);
        findViewById(R.id.next).setVisibility(View.INVISIBLE);
        findViewById(R.id.back).setVisibility(View.INVISIBLE);
        findViewById(R.id.linearLayout).setVisibility(View.VISIBLE);
    }

    @Override
    protected void onActivityResult (int requestCode, int resultCode, Intent data){
        if (requestCode == BluetoothConnector.REQUEST_ENABLE_BT) {
            if (resultCode == RESULT_OK)
				BluetoothOn();
			else {
                Toast.makeText(this, "Bluetooth was not allowed to turn on, no Bluetooth capabilities.", Toast.LENGTH_SHORT).show();
                Log.d(TAG, "onActivityResult(REQUEST_ENABLE_BT) got RESULT_CANCEL. Bluetooth was not allowed to turn on, no Bluetooth capabilities");
                finish();
            }
        }

    }

    public void enableButtons(){
        Button selectButton = (Button) findViewById(R.id.select);
        selectButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bluetoothConnector.send(SELECT);
            }
        });
        selectButton.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                bluetoothConnector.send(LONG_SELECT);
                return true;
            }
        });

        Button nextButton = (Button) findViewById(R.id.next);
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bluetoothConnector.send(NEXT);
            }
        });
        nextButton.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                bluetoothConnector.send(LONG_NEXT);
                return true;
            }
        });

        Button backButton = (Button) findViewById(R.id.back);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bluetoothConnector.send(BACK);
            }
        });

        backButton.setOnLongClickListener(new View.OnLongClickListener(){
        @Override
        public boolean onLongClick(View v) {
            bluetoothConnector.send(LONG_BACK);
            return true;
        }
        });

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                findViewById(R.id.select).setVisibility(View.VISIBLE);
                findViewById(R.id.next).setVisibility(View.VISIBLE);
                findViewById(R.id.back).setVisibility(View.VISIBLE);
                findViewById(R.id.linearLayout).setVisibility(View.INVISIBLE);
            }
        });
    }

    @Override
    public void BluetoothOn(){
        Toast.makeText(this, "Bluetooth turned on and ready.", Toast.LENGTH_SHORT).show();
        // Register the BroadcastReceiver
        IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
        registerReceiver(mReceiver, filter); // Don't forget to unregister during onDestroy
        bluetoothConnector.discoverDevices();
    }

    @Override
    protected void onDestroy(){
        try{
            unregisterReceiver(mReceiver);
        }catch(IllegalArgumentException e){
        }
        bluetoothConnector.onDestroy();

        super.onDestroy();
    }

    @Override
    public void toastToActivity(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void BluetoothConnected() {
        enableButtons();
    }

    @Override
    public void performAction(String s) {
        toastToActivity(s);
    }

    @Override
    public void connectionDisconnected() {
        bluetoothConnector = new BluetoothConnector();
        setupDefault();
    }

}
