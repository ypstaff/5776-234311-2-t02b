package yearlyproject.messages;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.Telephony;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.widget.ResourceCursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

/**
 * This fragment contains the list view of the conversations
 * to show in the conversationsList activity
 *
 * @author Lior voiln
 */
public class ConversationsListFragment extends Fragment implements
        LoaderManager.LoaderCallbacks<Cursor>,
        AdapterView.OnItemClickListener {

    //Define a ListView object
    ListView mConversationsList;

    //An adapter that binds the result Cursor to the ListView
    private ResourceCursorAdapter mCursorAdapter;

    //Defines the columns wanted in the query
    @SuppressLint("InlinedApi")
    private static final String[] PROJECTION =
            {
                    Telephony.Sms.Conversations.THREAD_ID + " as _ID",
                    Telephony.Sms.Conversations.SNIPPET,
                    Telephony.Sms.Conversations.MESSAGE_COUNT
            };

    //Empty public constructor, required by the system
    public ConversationsListFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater i, ViewGroup container,
                             Bundle savedInstanceState) {
        //Inflates the fragment layout
        View $ = i.inflate(R.layout.list_view, container, false);

        //Gets the ListView from the View list of the parent activity
        mConversationsList = (ListView) $.findViewById(R.id.list);

        //Gets a CursorAdapter
        mCursorAdapter = new ConversationsListCursorAdapter(
                getActivity(),
                R.layout.conversations_list_item,
                null,
                0);

        //Sets the adapter for the ListView
        mConversationsList.setAdapter(mCursorAdapter);

        //Sets the item click listener to be the current fragment.
        mConversationsList.setOnItemClickListener(this);

        return $;
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        //Initializes the loader
        getLoaderManager().initLoader(0, null, this);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int loaderId, Bundle args) {
        //Starts the query
        return new CursorLoader(
                getActivity(),
                Telephony.Sms.Conversations.CONTENT_URI,
                PROJECTION,
                null,
                null,
                Telephony.Sms.Conversations.DEFAULT_SORT_ORDER
        );
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor c) {
        //Puts the result Cursor in the adapter for the ListView
        mCursorAdapter.swapCursor(c);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        //Deletes the reference to the existing Cursor
        mCursorAdapter.swapCursor(null);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View item, int position, long rowID) {
        //Gets the Cursor and moves to the selected conversation
        Cursor cursor = mCursorAdapter.getCursor();
        cursor.moveToPosition(position);

        //Gets the _ID value of the selected conversation
        String thread_id = cursor.getString(cursor.getColumnIndex("_ID"));

        //Gets the address of the other side of the selected conversation
        Cursor tempCursor = getActivity().getContentResolver().query(
                Telephony.Sms.CONTENT_URI,
                new String[]{Telephony.Sms.ADDRESS},
                "thread_id = ?",
                new String[]{thread_id},
                null);
        String address = "";
        if (tempCursor != null) {
            tempCursor.moveToFirst();
            address = tempCursor.getString(0);
            tempCursor.close();
        }

        //Gets the display name of the other side of the selected conversation
        Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(address));
        tempCursor = getActivity().getContentResolver().query(
                uri,
                new String[]{ContactsContract.PhoneLookup.DISPLAY_NAME},
                null,
                null,
                null);

        openSelectedConversation(thread_id, tempCursor, address);
    }

    //Starts conversation activity and sends it the data of the selected conversation
    private void openSelectedConversation(String thread_id, Cursor tempCursor, String address) {
        Intent intent = new Intent(getActivity(), ConversationActivity.class);
        intent.putExtra("EXTRA_IS_COMING_FROM_NEW_MESSAGE", false);
        intent.putExtra("EXTRA_THREAD_ID", thread_id);
        intent.putExtra("EXTRA_ADDRESS", address);
        if (tempCursor != null) {
            if (tempCursor.getCount() <= 0)
                intent.putExtra("EXTRA_DISPLAY_NAME", address);
            else {
                tempCursor.moveToFirst();
                intent.putExtra("EXTRA_DISPLAY_NAME", tempCursor.getString(0));
            }
            tempCursor.close();
        }
        startActivity(intent);
    }

    public ListView getConversationsList() {
        return mConversationsList;
    }
}
