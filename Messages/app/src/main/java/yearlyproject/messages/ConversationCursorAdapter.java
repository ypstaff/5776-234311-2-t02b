package yearlyproject.messages;

import android.content.Context;
import android.database.Cursor;
import android.provider.Telephony;
import android.support.v4.widget.ResourceCursorAdapter;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * Customized cursor adapter for the list view of a conversation
 *
 * @author Lior voiln
 */
public class ConversationCursorAdapter extends ResourceCursorAdapter {

    public static final float PARAMS_WEIGHT = 1.0f;
    public static final int IN_LINE_LAYOUT_LEFT = 30;
    public static final int IN_LINE_LAYOUT_TOP = 12;
    public static final int IN_LINE_LAYOUT_RIGHT = 20;
    public static final int IN_LINE_LAYOUT_BOTTOM = 12;
    public static final int IN_LINE_LAYOUT_OUT_LEFT = 3;
    public static final int IN_LINE_LAYOUT_OUT_TOP = 3;
    public static final int IN_LINE_LAYOUT_OUT_RIGHT = 50;
    public static final int IN_LINE_LAYOUT_OUT_BOTTOM = 3;
    public static final int OUT_LINE_LAYOUT_LEFT = 20;
    public static final int OUT_LINE_LAYOUT_TOP = 12;
    public static final int OUT_LINE_LAYOUT_RIGHT = 30;
    public static final int OUT_LINE_LAYOUT_BOTTOM = 12;
    public static final int OUT_LINE_LAYOUT_OUT_LEFT = 50;
    public static final int OUT_LINE_LAYOUT_OUT_TOP = 3;
    public static final int OUT_LINE_LAYOUT_OUT_RIGHT = 3;
    public static final int OUT_LINE_LAYOUT_OUT_BOTTOM = 3;

    //Constructor
    public ConversationCursorAdapter(Context context, int layout, Cursor c, int flags) {
        super(context, layout, c, flags);
    }

    @Override
    public void bindView(View v, Context c, Cursor cursor) {
        TextView tvMessageBody = (TextView) v.findViewById(R.id.tv_message_body);
        TextView tvMessageDate1 = (TextView) v.findViewById(R.id.tv_date1);
        TextView tvMessageDate2 = (TextView) v.findViewById(R.id.tv_date2);
        LinearLayout Line_layout = (LinearLayout) v.findViewById(R.id.Line_layout);
        LinearLayout Line_layout_out = (LinearLayout) v.findViewById(R.id.Line_layout_out);

        settingTextViews(cursor, tvMessageBody, tvMessageDate1, tvMessageDate2);

        LinearLayout.LayoutParams params =
                new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        params.weight = PARAMS_WEIGHT;

        separateIncomingFromOutgoing(cursor, Line_layout, Line_layout_out, params);

        Line_layout.setLayoutParams(params);
        tvMessageBody.setLayoutParams(params);
        tvMessageDate1.setLayoutParams(params);
        tvMessageDate2.setLayoutParams(params);
    }

    private void separateIncomingFromOutgoing(Cursor c, LinearLayout line_layout, LinearLayout line_layout_out, LinearLayout.LayoutParams p) {
        if (Integer.parseInt(c.getString(c.getColumnIndex(Telephony.Sms.TYPE))) == Telephony.Sms.MESSAGE_TYPE_INBOX) {
            p.gravity = Gravity.START;
            line_layout.setBackgroundResource(R.drawable.balloon_incoming_normal);
            line_layout.setPadding(IN_LINE_LAYOUT_LEFT, IN_LINE_LAYOUT_TOP, IN_LINE_LAYOUT_RIGHT,
                    IN_LINE_LAYOUT_BOTTOM);
            line_layout_out.setPadding(IN_LINE_LAYOUT_OUT_LEFT, IN_LINE_LAYOUT_OUT_TOP, IN_LINE_LAYOUT_OUT_RIGHT,
                    IN_LINE_LAYOUT_OUT_BOTTOM);
        } else {
            p.gravity = Gravity.END;
            line_layout.setBackgroundResource(R.drawable.balloon_outgoing_normal);
            line_layout.setPadding(OUT_LINE_LAYOUT_LEFT, OUT_LINE_LAYOUT_TOP, OUT_LINE_LAYOUT_RIGHT,
                    OUT_LINE_LAYOUT_BOTTOM);
            line_layout_out.setPadding(OUT_LINE_LAYOUT_OUT_LEFT, OUT_LINE_LAYOUT_OUT_TOP, OUT_LINE_LAYOUT_OUT_RIGHT,
                    OUT_LINE_LAYOUT_OUT_BOTTOM);
        }
    }

    private void settingTextViews(Cursor c, TextView tvMessageBody, TextView tvMessageDate1, TextView tvMessageDate2) {
        //Setting the text of the message body in the text view
        tvMessageBody.setText(c.getString(c.getColumnIndex(Telephony.Sms.BODY)));

        //Setting the date and time of the message in the text views
        Long dateInMillis = c.getLong(c.getColumnIndex(Telephony.Sms.DATE));
        tvMessageDate1.setText(android.text.format.DateFormat.format("d/MM/yyyy", dateInMillis));
        tvMessageDate2.setText(android.text.format.DateFormat.format("k:m", dateInMillis));
    }
}
