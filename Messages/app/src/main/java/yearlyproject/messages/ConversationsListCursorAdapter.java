package yearlyproject.messages;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.provider.Telephony;
import android.support.v4.widget.ResourceCursorAdapter;
import android.view.View;
import android.widget.TextView;

import java.util.Calendar;

/**
 * Customized cursor adapter for the list view of the conversations list
 *
 * @author Lior voiln
 */
public class ConversationsListCursorAdapter extends ResourceCursorAdapter {

    private static final int SNIPPET_LENGTH = 45;

    String address;
    String formattedDate;

    //Constructor
    public ConversationsListCursorAdapter(Context context, int layout, Cursor c, int flags) {
        super(context, layout, c, flags);
    }

    @Override
    public void bindView(View v, Context c, Cursor cursor) {
        TextView contact = (TextView) v.findViewById(R.id.tv_cl_contact_name);
        TextView snippet = (TextView) v.findViewById(R.id.tv_cl_message_body);
        TextView messagesNum = (TextView) v.findViewById(R.id.tv_cl_message_count);
        TextView date = (TextView) v.findViewById(R.id.tv_cl_date);

        getAddressAndDate(c, cursor);

        //Sets the date of the last message in the conversation in the text view
        date.setText(formattedDate);

        setContact(c, contact);
        setSnippet(cursor, snippet);

        messagesNum.setText(
                (cursor.getString(cursor.getColumnIndex(Telephony.Sms.Conversations.MESSAGE_COUNT)) + " messages"));
    }

    //Sets the snippet of the message body in the text view
    private void setSnippet(Cursor c, TextView snippet) {
        String message = c.getString(c.getColumnIndex(Telephony.Sms.Conversations.SNIPPET));
        snippet.setText(
                message.length() <= SNIPPET_LENGTH + 1 ? message : message.substring(0, SNIPPET_LENGTH) + "...");
    }

    //Sets the display name of the other side of the conversation in the text view
    private void setContact(Context c, TextView contact) {
        Cursor tempCursor;
        Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(address));
        tempCursor = c.getContentResolver().query(
                uri,
                new String[]{ContactsContract.PhoneLookup.DISPLAY_NAME},
                null,
                null,
                null);
        if (tempCursor == null)
            return;
        if (tempCursor.getCount() <= 0)
            contact.setText(address);
        else {
            tempCursor.moveToFirst();
            contact.setText(tempCursor.getString(0));
        }
        tempCursor.close();
    }

    //Gets the address of the other side of the conversation and
    //the date of the last message in the conversation
    private void getAddressAndDate(Context c, Cursor cursor) {
        //Gets the _ID value of the conversation
        String thread_id = cursor.getString(cursor.getColumnIndex(Telephony.Sms.Conversations._ID));

        Cursor tempCursor = c.getContentResolver().query(
                Telephony.Sms.CONTENT_URI,
                new String[]{Telephony.Sms.ADDRESS, Telephony.Sms.DATE},
                "thread_id = ?",
                new String[]{thread_id},
                Telephony.Sms.DEFAULT_SORT_ORDER);
        formattedDate = address = "";
        if (tempCursor == null)
            return;
        tempCursor.moveToFirst();
        address = tempCursor.getString(0);
        Long dateInMillis = tempCursor.getLong(1);
        formattedDate = android.text.format.DateFormat.format("d/MM/yyyy", dateInMillis).toString();
        String currentTime = android.text.format.DateFormat.format("d/MM/yyyy", Calendar.getInstance().getTime())
                .toString();
        if (currentTime.equals(formattedDate))
            formattedDate = android.text.format.DateFormat.format("k:m", dateInMillis).toString();
        tempCursor.close();
    }
}
