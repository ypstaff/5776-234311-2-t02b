package yearlyproject.messages;

import android.annotation.SuppressLint;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.Telephony;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

/**
 * This fragment contains the list view of the messages to show in the conversation activity
 *
 * @author Lior voiln
 */
public class ConversationFragment extends Fragment implements
        LoaderManager.LoaderCallbacks<Cursor>,
        AdapterView.OnItemClickListener {

    // Define a ListView object
    ListView mConversation;

    // An adapter that binds the result Cursor to the ListView
    private ConversationCursorAdapter mCursorAdapter;

    // Defines the columns wanted in the query
    @SuppressLint("InlinedApi")
    private static final String[] PROJECTION =
            {
                    Telephony.Sms._ID,
                    Telephony.Sms.THREAD_ID,
                    Telephony.Sms.BODY,
                    Telephony.Sms.DATE,
                    Telephony.Sms.TYPE
            };

    //Defines the text expression for the selection in the query
    @SuppressLint("InlinedApi")
    private static final String SELECTION =
            "thread_id = ?";

    //Defines a variable for the wanted conversation
    private String mWantedThreadId;

    // Defines the array to hold values that replace the ?
    private String[] mSelectionArgs = {mWantedThreadId};

    // Empty public constructor, required by the system
    public ConversationFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater i, ViewGroup container,
                             Bundle savedInstanceState) {
        //Inflates the fragment layout
        View $ = i.inflate(R.layout.list_view, container, false);

        //Gets the ListView from the View list of the parent activity
        mConversation = (ListView) $.findViewById(R.id.list);

        //Gets a CursorAdapter
        mCursorAdapter = new ConversationCursorAdapter(
                getActivity(),
                R.layout.conversation_item,
                null,
                0);

        //Sets the adapter for the ListView
        mConversation.setAdapter(mCursorAdapter);

        //Sets the item click listener to be the current fragment.
        mConversation.setOnItemClickListener(this);

        //Deletes the lines between the items in the list view
        mConversation.setDivider(null);

        return $;
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        //Setting the activity title to be the display name of the other side of the conversation
        getActivity().setTitle(getActivity().getIntent().getExtras().getString("EXTRA_DISPLAY_NAME"));

        // Initializes the loader
        getLoaderManager().initLoader(0, null, this);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int loaderId, Bundle args) {
        //Gets the ID of the conversation to show and stores it in the selection array
        mWantedThreadId = getActivity().getIntent().getExtras().getString("EXTRA_THREAD_ID");
        mSelectionArgs[0] = mWantedThreadId;

        // Starts the query
        return new CursorLoader(
                getActivity(),
                Telephony.Sms.CONTENT_URI,
                PROJECTION,
                SELECTION,
                mSelectionArgs,
                Telephony.Sms.DEFAULT_SORT_ORDER
        );
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor c) {
        //Puts the result Cursor in the adapter for the ListView
        mCursorAdapter.swapCursor(c);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        //Deletes the reference to the existing Cursor
        mCursorAdapter.swapCursor(null);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
    }

    public ListView getConversationList() {
        return mConversation;
    }
}