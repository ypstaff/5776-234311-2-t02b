package yearlyproject.messages;

import android.graphics.Color;
import android.graphics.PorterDuff;
import android.support.design.widget.FloatingActionButton;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import yearlyproject.navigationlibrary.*;

/**
 * This activity is responsible for the choose contact screen
 *
 * @author Lior voiln
 */
public class ContactsActivity extends EmergencyCallActivity implements View.OnClickListener, TextView.OnEditorActionListener {

    public static final int COLOR = 0xe0f47521;

    EditText et_search;

    // array for green button click effect Akiva
    final ArrayList<View> l1 = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contacts);

        et_search = (EditText) findViewById(R.id.et_search);

        setListeners();

        //add green effect- akiva
        buttonEffect(findViewById(R.id.fab_middle));

        setThreeButtonsNavigation();
    }

    private void setThreeButtonsNavigation() {
        //Setting the 3 buttons navigation system for the activity
        fab_left = (FloatingActionButton) findViewById(R.id.fab_left);
        fab_right = (FloatingActionButton) findViewById(R.id.fab_right);
        fab_middle = (FloatingActionButton) findViewById(R.id.fab_middle);

        //setting the action buttons semi transparent
        fab_left.setAlpha((float) 0.5);
        fab_middle.setAlpha((float) 0.5);
        fab_right.setAlpha((float) 0.5);

        fab_left.setOnClickListener(this);
        fab_right.setOnClickListener(this);
        fab_middle.setOnClickListener(this);

        fab_left.setOnLongClickListener(this);
        fab_right.setOnLongClickListener(this);
        fab_middle.setOnLongClickListener(this);

        group = createNavigationGroup();
        group.start();
    }

    IterableViewGroups createNavigationGroup() {
        ContactsFragment contactsFragment =
                (ContactsFragment) getSupportFragmentManager().findFragmentById(R.id.f_contacts);

        List<IterableGroup> list = new ArrayList<>();

        list.add(new IterableSingleView(et_search, this));
        list.add(new IterableListView(contactsFragment.getContactsList(), this));

        return new IterableViewGroups(list, findViewById(R.id.ll_contacts), this);
    }

    private void setListeners() {
        et_search.setOnClickListener(this);
        et_search.setOnEditorActionListener(this);

        //When the edit text changes update the search string
        et_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                ((ContactsFragment) getSupportFragmentManager().findFragmentById(R.id.f_contacts))
                        .setSearchString(et_search.getText().toString());
            }

            @Override
            public void afterTextChanged(Editable e) {

            }
        });
    }

    //five methods for button effect- Akiva
    public void buttonEffectClear() {
        l1.clear();
    }

    public void buttonEffectAdd(List<View> vs) {
        for (View button : vs)
            if (button != null)
                l1.add(button);
    }

    public void buttonEffectDo(ArrayList<View> l2) {
        for (View vAll : l2)
            if (vAll != null) {
                if (vAll.getBackground() == null) {
                    vAll.setBackgroundColor(Color.WHITE);
                    vAll.invalidate();
                }
                vAll.invalidate();
                vAll.getBackground().setColorFilter(COLOR, PorterDuff.Mode.SRC_ATOP);
            }
    }

    public void buttonEffectStop(ArrayList<View> l2) {
        for (View vAll : l2)
            if (vAll != null && vAll.getBackground() != null) {
                vAll.getBackground().clearColorFilter();
                vAll.invalidate();
            }
    }

    public void buttonEffect(View button) {
        //  buttonEffectAdd(button);
        button.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent e) {

                switch (e.getAction()) {
                    case MotionEvent.ACTION_DOWN: {
                        buttonEffectClear();
                        buttonEffectAdd(group.getSelectedViews());
                        buttonEffectDo(l1);
                        break;
                    }
                    case MotionEvent.ACTION_UP: {
                        buttonEffectStop(l1);
                        break;
                    }
                }
                return false;
            }
        });
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);

        switch (v.getId()) {
            //When clicking on the edit text
            case R.id.et_search:
                showSoftKey(et_search);
                break;
        }
    }

    //For the keyboard bluetooth integration - unregisters the keyboard from the bluetooth service
    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent e) {
        if (actionId == EditorInfo.IME_ACTION_SEND || actionId == EditorInfo.IME_ACTION_NEXT
                || actionId == EditorInfo.IME_ACTION_DONE)
            hideSoftKey();
        return false;
    }
}
