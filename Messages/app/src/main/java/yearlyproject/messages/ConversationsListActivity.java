package yearlyproject.messages;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.os.Bundle;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;

import yearlyproject.navigationlibrary.*;

import java.util.ArrayList;
import java.util.List;

/**
 * This activity is responsible for showing all the available conversations
 *
 * @author Lior voiln
 */
public class ConversationsListActivity extends EmergencyCallActivity
        implements View.OnClickListener, View.OnLongClickListener {

    public static final int COLOR = 0xe0f47521;

    Button b_new_message;

    // array for green button click effect Akiva
    final ArrayList<View> l1 = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_conversations_list);

        b_new_message = (Button) findViewById(R.id.b_new_message);

        b_new_message.setOnClickListener(this);

        //add green effect- akiva
        buttonEffect(findViewById(R.id.fab_middle));

        setThreeButtonsNavigation();
    }

    private void setThreeButtonsNavigation() {
        //Setting the 3 buttons navigation system for the activity
        fab_left = (FloatingActionButton) findViewById(R.id.fab_left);
        fab_right = (FloatingActionButton) findViewById(R.id.fab_right);
        fab_middle = (FloatingActionButton) findViewById(R.id.fab_middle);

        //setting the action buttons semi transparent
        fab_left.setAlpha((float) 0.5);
        fab_middle.setAlpha((float) 0.5);
        fab_right.setAlpha((float) 0.5);

        fab_left.setOnClickListener(this);
        fab_right.setOnClickListener(this);
        fab_middle.setOnClickListener(this);

        fab_left.setOnLongClickListener(this);
        fab_right.setOnLongClickListener(this);
        fab_middle.setOnLongClickListener(this);

        group = createNavigationGroup();
        group.start();
    }

    IterableViewGroups createNavigationGroup() {
        ConversationsListFragment fragment = (ConversationsListFragment)
                getSupportFragmentManager().findFragmentById(R.id.f_conversations_list);

        List<IterableGroup> list = new ArrayList<>();
        list.add(new IterableSingleView(b_new_message, this));
        list.add(new IterableListView(fragment.getConversationsList(), this));

        return new IterableViewGroups(list, findViewById(R.id.ll_conversations), this);
    }

    //five methods for button effect- Akiva
    public void buttonEffectClear() {
        l1.clear();
    }

    public void buttonEffectAdd(List<View> vs) {
        for (View button : vs)
            if (button != null)
                l1.add(button);
    }

    public void buttonEffectDo(ArrayList<View> l2) {
        for (View vAll : l2)
            if (vAll != null) {
                if (vAll.getBackground() == null) {
                    vAll.setBackgroundColor(Color.WHITE);
                    vAll.invalidate();
                }
                vAll.invalidate();
                vAll.getBackground().setColorFilter(COLOR, PorterDuff.Mode.SRC_ATOP);
            }
    }

    public void buttonEffectStop(ArrayList<View> l2) {
        for (View vAll : l2)
            if (vAll != null && vAll.getBackground() != null && vAll.getBackground() != null) {
                vAll.getBackground().clearColorFilter();
                vAll.invalidate();
            }
    }

    public void buttonEffect(View button) {
        //  buttonEffectAdd(button);
        button.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent e) {
                switch (e.getAction()) {
                    case MotionEvent.ACTION_DOWN: {
                        buttonEffectClear();
                        buttonEffectAdd(group.getSelectedViews());
                        buttonEffectDo(l1);
                        break;
                    }
                    case MotionEvent.ACTION_UP: {
                        buttonEffectStop(l1);
                        break;
                    }
                }
                return false;
            }
        });
    }

    //since conversation index could change in the list and mess up the current selected row,
    //we reset the screen on each resume to avoid wierd behaviour in lists
    @Override
    protected void onResume() {
        super.onResume();
        group.start();
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);

        switch (v.getId()) {

            case R.id.b_new_message:
                Intent intent = new Intent(this, NewMessageActivity.class);
                startActivity(intent);
                break;
        }
    }
}
