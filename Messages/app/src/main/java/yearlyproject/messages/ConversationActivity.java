package yearlyproject.messages;

import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.support.design.widget.FloatingActionButton;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import yearlyproject.navigationlibrary.*;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * This activity is responsible for showing all the messages in a conversation
 *
 * @author Lior voiln
 */
public class ConversationActivity extends EmergencyCallActivity implements
        View.OnClickListener, TextView.OnEditorActionListener {

    private static final int FROM_NEW_MESSAGE_MILISECS_LIMIT = 10000;
    private static final int SENDING_MILISECS_LIMIT = 5000;
    public static final int COLOR = 0xe0f47521;

    EditText etNewMessageBody;
    Button bSend;
    ProgressBar pbNewMessage;
    ConversationFragment fragment;

    // array for green button click effect Akiva
    final ArrayList<View> l1 = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_conversation);

        findViewsAndFragments();

        setListeners();

        boolean isComingFromNewMessage =
                getIntent().getExtras().getBoolean("EXTRA_IS_COMING_FROM_NEW_MESSAGE");
        if (isComingFromNewMessage)
            onComingFromNewMessage();
        else
            pbNewMessage.setVisibility(View.GONE);

        //add green effect- akiva
        buttonEffect(findViewById(R.id.fab_middle));

        setThreeButtonsNavigation();
    }

    private void setThreeButtonsNavigation() {
        //Setting the 3 buttons navigation system for the activity
        fab_left = (FloatingActionButton) findViewById(R.id.fab_left);
        fab_right = (FloatingActionButton) findViewById(R.id.fab_right);
        fab_middle = (FloatingActionButton) findViewById(R.id.fab_middle);

        //setting the action buttons semi transparent
        fab_left.setAlpha((float) 0.5);
        fab_middle.setAlpha((float) 0.5);
        fab_right.setAlpha((float) 0.5);

        fab_left.setOnClickListener(this);
        fab_right.setOnClickListener(this);
        fab_middle.setOnClickListener(this);

        fab_left.setOnLongClickListener(this);
        fab_right.setOnLongClickListener(this);
        fab_middle.setOnLongClickListener(this);

        group = createNavigationGroup();
        group.start();
    }

    IterableViewGroups createNavigationGroup() {
        List<View> groupList = new ArrayList<>();
        groupList.add(etNewMessageBody);
        groupList.add(bSend);

        List<IterableGroup> groupsList = new ArrayList<>();
        groupsList.add(new IterableViewGroup(groupList, findViewById(R.id.ll_send), this));
        groupsList.add(new IterableListView(fragment.getConversationList(), this));

        return new IterableViewGroups(groupsList, findViewById(R.id.ll_conversation), this);
    }

    private void setListeners() {
        bSend.setOnClickListener(this);
        etNewMessageBody.setOnClickListener(this);
        etNewMessageBody.setOnEditorActionListener(this);
    }

    private void onComingFromNewMessage() {
        //If we've come here from the new message activity, the progress bar
        //should stay visible until the new message appears in the list view
        final int messageCountBeforeSending =
                getIntent().getExtras().getInt("EXTRA_MESSAGE_COUNT_BEFORE_SENDING");

        final Context thisContext = this;

        new Thread(new Runnable() {
            @Override
            public void run() {
                Long startTime = Calendar.getInstance().getTimeInMillis();
                while (true) {
                    if (fragment.getConversationList().getCount() > messageCountBeforeSending)
                        break;
                    if (Calendar.getInstance().getTimeInMillis() - startTime > FROM_NEW_MESSAGE_MILISECS_LIMIT) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(thisContext, "Can't send the message right now", Toast.LENGTH_LONG).show();
                            }
                        });
                        finish();
                        return;
                    }
                }
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        pbNewMessage.setVisibility(View.GONE);
                    }
                });
            }
        }).start();
    }

    private void findViewsAndFragments() {
        etNewMessageBody = (EditText) findViewById(R.id.et_new_message);
        bSend = (Button) findViewById(R.id.b_send_new_message);
        pbNewMessage = (ProgressBar) findViewById(R.id.pb_new_message);

        fragment = (ConversationFragment)
                getSupportFragmentManager().findFragmentById(R.id.f_conversation);
    }

    //five methods for button effect- Akiva
    public void buttonEffectClear() {
        l1.clear();
    }

    public void buttonEffectAdd(List<View> vs) {
        for (View button : vs)
            if (button != null)
                l1.add(button);
    }

    public void buttonEffectDo(ArrayList<View> l2) {
        for (View vAll : l2)
            if (vAll != null) {
                if (vAll.getBackground() == null) {
                    vAll.setBackgroundColor(Color.WHITE);
                    vAll.invalidate();
                }
                vAll.invalidate();
                vAll.getBackground().setColorFilter(COLOR, PorterDuff.Mode.SRC_ATOP);
            }
    }

    public void buttonEffectStop(ArrayList<View> l2) {
        for (View vAll : l2)
            if (vAll != null && vAll.getBackground() != null && vAll.getBackground() != null) {
                vAll.getBackground().clearColorFilter();
                vAll.invalidate();
            }
    }

    public void buttonEffect(View button) {
        //  buttonEffectAdd(button);
        button.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent e) {

                switch (e.getAction()) {
                    case MotionEvent.ACTION_DOWN: {
                        buttonEffectClear();
                        buttonEffectAdd(group.getSelectedViews());
                        buttonEffectDo(l1);
                        break;
                    }
                    case MotionEvent.ACTION_UP: {
                        buttonEffectStop(l1);
                        break;
                    }
                }
                return false;
            }
        });
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);

        switch (v.getId()) {
            case R.id.b_send_new_message:
                onSendNewMessagePressed();
                break;

            case R.id.et_new_message:
                showSoftKey(etNewMessageBody);
                break;
        }
    }

    private void onSendNewMessagePressed() {
        //Getting the message body from the edit text
        String messageBody = etNewMessageBody.getText().toString();

        //If the message body is empty then toast error message
        if ("".equals(messageBody)) {
            Toast.makeText(this, "Enter message content", Toast.LENGTH_SHORT).show();
            return;
        }

        //Getting the address from the intent
        String address = getIntent().getExtras().getString("EXTRA_ADDRESS");

        //Sending the message
        SmsManager smsManager = SmsManager.getDefault();
        smsManager.sendTextMessage(address, null, messageBody, null, null);

        //After sending the message erasing its text from the edit text
        etNewMessageBody.setText("");

        manageProgressBar();
    }

    private void manageProgressBar() {
        //The progress bar should be visible until the new message appears in the list view
        pbNewMessage.setVisibility(View.VISIBLE);

        //Getting the number of data items in the list view before sending the message
        final int countBeforeSending = fragment.getConversationList().getCount();

        final Context thisContext = this;

        new Thread(new Runnable() {
            @Override
            public void run() {
                Long startTime = Calendar.getInstance().getTimeInMillis();
                while (true) {
                    if (fragment.getConversationList().getCount() > countBeforeSending)
                        break;
                    if (Calendar.getInstance().getTimeInMillis() - startTime > SENDING_MILISECS_LIMIT) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(thisContext, "Can't send the message right now", Toast.LENGTH_LONG).show();
                            }
                        });
                        finish();
                        return;
                    }
                }
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        pbNewMessage.setVisibility(View.GONE);
                    }
                });
            }
        }).start();
    }

    //For the keyboard bluetooth integration - unregisters the keyboard from the bluetooth service
    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent e) {
        if (actionId == EditorInfo.IME_ACTION_SEND || actionId == EditorInfo.IME_ACTION_NEXT
                || actionId == EditorInfo.IME_ACTION_DONE)
            hideSoftKey();
        return false;
    }
}
