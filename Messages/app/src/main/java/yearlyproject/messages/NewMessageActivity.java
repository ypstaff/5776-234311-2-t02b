package yearlyproject.messages;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.provider.ContactsContract;
import android.provider.Telephony;
import android.support.design.widget.FloatingActionButton;
import android.os.Bundle;
import android.telephony.PhoneNumberUtils;
import android.telephony.SmsManager;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import yearlyproject.navigationlibrary.*;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * An activity for sending a new message
 *
 * @author Roy Svinik
 * contributor Lior Volin
 */

public class NewMessageActivity extends EmergencyCallActivity implements View.OnClickListener, TextView.OnEditorActionListener {

    private static final int SENDING_MILISECS_LIMIT = 5000;
    private static final int COLOR = 0xe0f47521;
    private static final int contactCode = 1;

    Button b_send;
    ImageButton ib_contacts;
    String message;
    EditText et_message;
    String phoneNumber = "";
    String contactName = "";
    EditText et_contact_name;
    ProgressBar pb_new_conversation;

    // array for green button click effect Akiva
    final ArrayList<View> l1 = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_message);

        findViews();

        setListeners();

        pb_new_conversation.setVisibility(View.GONE);

        //add green effect- akiva
        buttonEffect(findViewById(R.id.fab_middle));

        setThreeButtonsNavigation();
    }

    private void setListeners() {
        et_contact_name.setOnClickListener(this);
        et_contact_name.setOnEditorActionListener(this);
        b_send.setOnClickListener(this);
        et_message.setOnClickListener(this);
        et_message.setOnEditorActionListener(this);
        ib_contacts.setOnClickListener(this);
    }

    private void setThreeButtonsNavigation() {
        fab_left = (FloatingActionButton) findViewById(R.id.fab_left);
        fab_right = (FloatingActionButton) findViewById(R.id.fab_right);
        fab_middle = (FloatingActionButton) findViewById(R.id.fab_middle);

        fab_left.setOnClickListener(this);
        fab_right.setOnClickListener(this);
        fab_middle.setOnClickListener(this);

        fab_left.setOnLongClickListener(this);
        fab_right.setOnLongClickListener(this);
        fab_middle.setOnLongClickListener(this);

        group = createNavigationGroup();
        group.start();
    }

    IterableViewGroups createNavigationGroup() {
        List<View> row_list = new ArrayList<>();
        row_list.add(et_contact_name);
        row_list.add(ib_contacts);

        List<IterableGroup> group_list = new ArrayList<>();
        group_list.add(new IterableViewGroup(row_list, findViewById(R.id.ll_contact), this));
        group_list.add(new IterableSingleView(et_message, this));
        group_list.add(new IterableSingleView(b_send, this));

        return new IterableViewGroups(group_list, findViewById(R.id.ll_new_message), this);
    }

    private void findViews() {
        b_send = (Button) findViewById(R.id.b_send);
        ib_contacts = (ImageButton) findViewById(R.id.ib_contacts);
        et_message = (EditText) findViewById(R.id.et_write_message);
        et_contact_name = (EditText) findViewById(R.id.et_contact_name);
        pb_new_conversation = (ProgressBar) findViewById(R.id.pb_new_conversation);
    }

    //five methods for button effect- Akiva
    public void buttonEffectClear() {
        l1.clear();
    }

    public void buttonEffectAdd(List<View> vs) {
        for (View button : vs)
            if (button != null)
                l1.add(button);
    }

    public void buttonEffectDo(ArrayList<View> l2) {
        for (View vAll : l2)
            if (vAll != null) {
                if (vAll.getBackground() == null) {
                    vAll.setBackgroundColor(Color.WHITE);
                    vAll.invalidate();
                }
                vAll.invalidate();
                vAll.getBackground().setColorFilter(COLOR,
                        PorterDuff.Mode.SRC_ATOP);
            }
    }

    public void buttonEffectStop(ArrayList<View> l2) {
        for (View vAll : l2)
            if (vAll != null && vAll.getBackground() != null) {
                vAll.getBackground().clearColorFilter();
                vAll.invalidate();
            }
    }

    public void buttonEffect(View button) {
        //  buttonEffectAdd(button);
        button.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent e) {

                switch (e.getAction()) {
                    case MotionEvent.ACTION_DOWN: {
                        buttonEffectClear();
                        buttonEffectAdd(group.getSelectedViews());
                        buttonEffectDo(l1);
                        break;
                    }
                    case MotionEvent.ACTION_UP: {
                        buttonEffectStop(l1);
                        break;
                    }
                }
                return false;
            }
        });
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);

        switch (v.getId()) {

            case R.id.et_contact_name:
                onEditTextPressed(et_contact_name);
                break;

            case R.id.ib_contacts:
                Intent intent = new Intent(this, ContactsActivity.class);
                startActivityForResult(intent, contactCode);
                break;

            case R.id.b_send:
                onSendPressed();
                break;

            case R.id.et_write_message:
                onEditTextPressed(et_message);
                break;
        }
    }

    private void onSendPressed() {
        message = et_message.getText().toString();

        if ("".equals(message)) {
            Toast.makeText(this, "Enter message content", Toast.LENGTH_SHORT).show();
            return;
        }

        if (!et_contact_name.getText().toString().equals(contactName)) {
            phoneNumber = et_contact_name.getText().toString();
            contactName = phoneNumber;
        }

        if (!PhoneNumberUtils.isGlobalPhoneNumber(phoneNumber)) {
            Toast.makeText(this, "Illegal recipient", Toast.LENGTH_SHORT).show();
            return;
        }

        final int messageCountBeforeSending = getCountBeforeSending();

        //Sending the new message
        SmsManager smsManager = SmsManager.getDefault();
        smsManager.sendTextMessage(phoneNumber, null, message, null, null);

        openConversationActivity(messageCountBeforeSending);
    }

    private void openConversationActivity(final int messageCountBeforeSending) {
        //Setting the progress bar to visible until the thread_id is found (it takes
        //time if the new message that was just sent is the first message in the thread)
        //and then we can move to the conversation activity of the thread
        pb_new_conversation.setVisibility(View.VISIBLE);

        final Context thisContext = this;

        new Thread(new Runnable() {
            @Override
            public void run() {
                Long startTime = Calendar.getInstance().getTimeInMillis();
                Cursor tempCursor;
                while (true) {
                    tempCursor = getContentResolver().query(Telephony.Sms.CONTENT_URI,
                            new String[]{Telephony.Sms.THREAD_ID}, "address = ?", new String[]{phoneNumber},
                            null);
                    if (tempCursor != null && tempCursor.getCount() != 0)
                        break;
                    if (Calendar.getInstance().getTimeInMillis() - startTime > SENDING_MILISECS_LIMIT) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(thisContext, "Can't find the new conversation", Toast.LENGTH_LONG).show();
                            }
                        });
                        finish();
                        return;
                    }
                }
                tempCursor.moveToFirst();
                String thread_id = tempCursor.getString(0);
                tempCursor.close();
                openThreadIdConversationActivity(thread_id, thisContext, messageCountBeforeSending);
            }
        }).start();
    }

    //Moving to the conversation activity of thread_id
    private void openThreadIdConversationActivity(String thread_id, Context thisContext, int messageCountBeforeSending) {
        Cursor tempCursor;
        Intent intent2 = new Intent(thisContext, ConversationActivity.class);
        intent2.putExtra("EXTRA_IS_COMING_FROM_NEW_MESSAGE", true);
        intent2.putExtra("EXTRA_MESSAGE_COUNT_BEFORE_SENDING", messageCountBeforeSending);
        intent2.putExtra("EXTRA_THREAD_ID", thread_id);
        intent2.putExtra("EXTRA_ADDRESS", phoneNumber);
        Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(phoneNumber));
        tempCursor = getContentResolver().query(
                uri,
                new String[]{ContactsContract.PhoneLookup.DISPLAY_NAME},
                null,
                null,
                null);
        if (tempCursor == null || tempCursor.getCount() <= 0)
            intent2.putExtra("EXTRA_DISPLAY_NAME", phoneNumber);
        else {
            tempCursor.moveToFirst();
            intent2.putExtra("EXTRA_DISPLAY_NAME",
                    tempCursor.getString(0));
        }
        if (tempCursor != null)
            tempCursor.close();

        startActivity(intent2);
        finish();
    }

    //Getting the message count in the thread before sending the new message
    private int getCountBeforeSending() {
        return getTempCursorCountBeforeSending(getContentResolver().query(Telephony.Sms.CONTENT_URI,
                new String[]{Telephony.Sms.THREAD_ID}, "address = ?", new String[]{phoneNumber}, null));
    }

    private int getTempCursorCountBeforeSending(Cursor tempCursor) {
        int $ = 0;

        //Getting the message count in the thread - thread_id
        if (tempCursor != null) {
            if (tempCursor.getCount() == 0)
                tempCursor.close();
            else {
                tempCursor.moveToFirst();
                String thread_id = tempCursor.getString(0);
                tempCursor.close();
                tempCursor = getContentResolver()
                        .query(Telephony.Sms.Conversations.CONTENT_URI,
                                new String[]{Telephony.Sms.Conversations.MESSAGE_COUNT},
                                "thread_id = ?",
                                new String[]{thread_id}, null);
                if (tempCursor != null) {
                    if (tempCursor.getCount() != 0) {
                        tempCursor.moveToFirst();
                        $ = tempCursor.getInt(0);
                    }
                    tempCursor.close();
                }
            }
        }
        return $;
    }

    private void onEditTextPressed(EditText t) {
        t.setFocusableInTouchMode(true);
        t.requestFocus();
        showSoftKey(t);
        t.setSelection(t.getText().length());
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode != contactCode || resultCode != Activity.RESULT_OK)
            return;
        contactName = data.getStringExtra("contact name");
        phoneNumber = data.getStringExtra("phone number");
        et_contact_name.setText(contactName);
    }

    //For the keyboard bluetooth integration - unregisters the keyboard from the bluetooth service
    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent e) {
        if (actionId == EditorInfo.IME_ACTION_SEND || actionId == EditorInfo.IME_ACTION_NEXT
                || actionId == EditorInfo.IME_ACTION_DONE)
            hideSoftKey();
        return false;
    }
}
