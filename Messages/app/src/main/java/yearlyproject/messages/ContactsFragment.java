package yearlyproject.messages;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.util.Pair;
import android.support.v4.widget.SimpleCursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.HashSet;
import java.util.Set;

/**
 * This fragment contains the list view of contacts to show in the choose contact activity
 *
 * @author Lior voiln
 */
public class ContactsFragment extends Fragment implements
        LoaderManager.LoaderCallbacks<Cursor>,
        AdapterView.OnItemClickListener {

    //Defines a ListView object
    ListView mContactsList;

    //An adapter that binds the result Cursor to the ListView
    private SimpleCursorAdapter mCursorAdapter;

    //Defines the columns wanted in the contacts query
    @SuppressLint("InlinedApi")
    private static final String[] PROJECTION =
            {
                    ContactsContract.CommonDataKinds.Phone._ID,
                    Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB
                            ? ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME
                            : ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME_PRIMARY,
                    ContactsContract.CommonDataKinds.Phone.PHOTO_THUMBNAIL_URI,
                    ContactsContract.CommonDataKinds.Phone.NUMBER,
                    ContactsContract.CommonDataKinds.Phone.NORMALIZED_NUMBER

            };

    //The column index for the CONTACT_NAME column
    private static final int CONTACT_NAME_INDEX = 1;

    //Defines the text expression for the selection in the contacts query
    //We want only rows with phone number.
    @SuppressLint("InlinedApi")
    private static final String SELECTION =
            (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB ? ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME
                    : ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME_PRIMARY) + " LIKE ? AND "
                    + ContactsContract.CommonDataKinds.Phone.HAS_PHONE_NUMBER + " = 1";

    // Defines a variable for the search string
    private String mSearchString;

    // Defines the array to hold values that replace the ?
    private String[] mSelectionArgs = {mSearchString};

    //Defines an array that contains column names to move from the Cursor to the ListView.
    @SuppressLint("InlinedApi")
    private final static String[] FROM_COLUMNS = {
            "display_name",
            "photo_thumbnail",
            "number"
    };

    //Defines an array that contains resource ids for the layout views
    //that get the Cursor column contents.
    private final static int[] TO_IDS = {
            R.id.text1,
            R.id.image1,
            R.id.text2
    };

    //Empty public constructor, required by the system
    public ContactsFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater i, ViewGroup container,
                             Bundle savedInstanceState) {
        //Inflate the fragment layout
        View $ = i.inflate(R.layout.list_view, container, false);

        //Gets the ListView from the View list of the parent activity
        mContactsList = (ListView) $.findViewById(R.id.list);

        //Gets a CursorAdapter
        mCursorAdapter = new SimpleCursorAdapter(
                getActivity(),
                R.layout.contacts_list_item,
                null,
                FROM_COLUMNS,
                TO_IDS,
                0);

        //Sets the adapter for the ListView
        mContactsList.setAdapter(mCursorAdapter);

        //Sets the item click listener to be the current fragment.
        mContactsList.setOnItemClickListener(this);

        //In the beginning all the contacts are shown
        mSearchString = "_";

        return $;
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        //Initializes the loader
        getLoaderManager().initLoader(0, null, this);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int loaderId, Bundle args) {
        //Makes search string into pattern and stores it in the selection array
        mSelectionArgs[0] = "%" + mSearchString + "%";

        //Starts the query
        return new CursorLoader(
                getActivity(),
                ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                PROJECTION,
                SELECTION,
                mSelectionArgs,
                "2 ASC"
        );
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor c) {

        MatrixCursor withoutDuplicationsCursor = new MatrixCursor(new String[]{"_id", "display_name", "number", "photo_thumbnail"});

        Set<Pair> contactNumbersSet = new HashSet<>();

        //Adding rows to the new cursor without duplications in the pair (contact name, contact number)
        if (!c.isClosed()) {
            c.moveToFirst();
            while (!c.isAfterLast()) {
                //Taking the values of all the columns in a row from the cursor with the duplications
                String contact_id = c.getString(c.getColumnIndex(ContactsContract.CommonDataKinds.Phone._ID));
                String contactName = c.getString(c.getColumnIndex(
                        Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB
                                ? ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME
                                : ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME_PRIMARY));
                String contactNumber = c.getString(c.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                String contactPhotoThumbnail = c.getString(c.getColumnIndex(ContactsContract.CommonDataKinds.Phone.PHOTO_THUMBNAIL_URI));
                String normalizedContactNumber = c.getString(c.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NORMALIZED_NUMBER));

                //Adding the row to the new cursor if the pair (contact name, contact number)
                // of that row does not already present in the new cursor
                int prevCount = contactNumbersSet.size();
                contactNumbersSet.add(new Pair<>(contactName, normalizedContactNumber));
                if (contactNumbersSet.size() > prevCount)
                    withoutDuplicationsCursor
                            .addRow(new Object[]{contact_id, contactName, contactNumber, contactPhotoThumbnail});
                c.moveToNext();
            }
            c.close();
        }

        //Puts the new without duplications cursor in the adapter for the ListView
        mCursorAdapter.swapCursor(withoutDuplicationsCursor);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        //Deletes the reference to the existing Cursor
        mCursorAdapter.swapCursor(null);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View item, int position, long rowID) {
        //Gets the Cursor and moves to the selected contact
        Cursor cursor = mCursorAdapter.getCursor();
        cursor.moveToPosition(position);

        //Gets the phone number and the name of the selected contact
        String phoneNumber = cursor.getString(cursor.getColumnIndex("number"));
        String contactName = cursor.getString(CONTACT_NAME_INDEX);

        //Returns the data of the selected contact and finishes the activity
        Intent returnIntent = new Intent();
        returnIntent.putExtra("phone number", phoneNumber);
        returnIntent.putExtra("contact name", contactName);
        getActivity().setResult(Activity.RESULT_OK, returnIntent);
        getActivity().finish();
    }

    public void setSearchString(String searchString) {
        //Sets the search string
        mSearchString = searchString;

        //Making the query again to get new contacts to show in the list
        getLoaderManager().restartLoader(0, null, this);
    }

    public ListView getContactsList() {
        return mContactsList;
    }
}