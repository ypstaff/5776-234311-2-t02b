package yearlyproject.snake;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.view.View;
import android.view.Window;
import android.widget.Button;

import java.util.ArrayList;
import java.util.List;

import yearlyproject.navigationlibrary.EmergencyCallActivity;
import yearlyproject.navigationlibrary.IterableGroup;
import yearlyproject.navigationlibrary.IterableSingleView;
import yearlyproject.navigationlibrary.IterableViewGroups;

/**
 * Created by Akiva
 * Title Screen activity
 */

public class TitleScreen extends EmergencyCallActivity {

    SharedPreferences settings;
    SharedPreferences.Editor SettingEditor;
    Button button1;
    Button button2;
    Button button3;
    Button button4;

    @Override
    public void onCreate(Bundle savedInstanceState) {

        // Set Theme According to Settings
        settings = getSharedPreferences(getString(R.string.settings_file), Constants.MODE_PRIVATE_SHARED_PREFRENCES);
        if (settings.getInt(getString(R.string.theme_settings), Constants.DEFAULT_VALUE_THEME) == Constants.THEME_HOLO)
			setTheme(android.R.style.Theme_Holo);
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.title_screen);

        initiateViews();
        initTouchListeners();
        prepareIterableGroups();
    }

    private void initiateViews() {
        //setting buttons to be used by this activity
        button1 = (Button) findViewById(R.id.button1);
        button2 = (Button) findViewById(R.id.button2);
        button3 = (Button) findViewById(R.id.button3);
        button4 = (Button) findViewById(R.id.button4);


        //Setting the 3 buttons navigation system for the activity
        fab_left = (FloatingActionButton) findViewById(R.id.fab_left);
        fab_right = (FloatingActionButton) findViewById(R.id.fab_right);
        fab_middle = (FloatingActionButton) findViewById(R.id.fab_middle);

        //setting the action buttons semi transparent
        fab_left.setAlpha((float) 0.5);
        fab_middle.setAlpha((float) 0.5);
        fab_right.setAlpha((float) 0.5);
    }

    protected void initTouchListeners() {
        //setting listeners
        fab_left.setOnClickListener(this);
        fab_right.setOnClickListener(this);
        fab_middle.setOnClickListener(this);
        fab_left.setOnLongClickListener(this);
        fab_right.setOnLongClickListener(this);
        fab_middle.setOnLongClickListener(this);
    }

    private void prepareIterableGroups() {
        //creating list to iterate over with navigation module
        List<IterableGroup> list = new ArrayList<>();
        list.add(new IterableSingleView(button1, this));
        list.add(new IterableSingleView(button2, this));
        list.add(new IterableSingleView(button3, this));
        list.add(new IterableSingleView(button4, this));

        group = new IterableViewGroups(list, findViewById(R.id.incompassingLayoutTitle), this);
        group.start();
    }

    // On "Start" Button Press, Start Game
    public void startGame(View v) {
        Intent intent = new Intent(this, GameScreen.class);
        startActivity(intent);
    }

    // On "Game Settings Options" Button Press, Go to Game Settings Options
    public void options(View v) {
        Intent intent = new Intent(this, OptionsScreen.class);
        startActivity(intent);
        this.finish();
    }

    // On "Game Play Options" Button Press, Go to Game Settings Play
    public void gamePlayOptions(View v) {
        SettingEditor = settings.edit();
        SettingEditor.putInt(getString(R.string.HighScoreSettings), Constants.PLAY_OPTIONS_SCREEN);
        SettingEditor.commit();
        Intent intent = new Intent(this, PlayOptionsScrean.class);
        startActivity(intent);
        this.finish();
    }

    // On "High Scores" Button Press, Go to High Score Settings
    public void showHighScores(View v) {
        SettingEditor = settings.edit();
        SettingEditor.putInt(getString(R.string.HighScoreSettings), Constants.HIGH_SCORE_SCREEN);
        SettingEditor.commit();
        Intent intent = new Intent(this, PlayOptionsScrean.class);
        startActivity(intent);
    }
}