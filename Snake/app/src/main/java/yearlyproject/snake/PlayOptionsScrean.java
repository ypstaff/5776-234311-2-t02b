package yearlyproject.snake;



import android.app.backup.BackupManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import yearlyproject.navigationlibrary.EmergencyCallActivity;
import yearlyproject.navigationlibrary.IterableGroup;
import yearlyproject.navigationlibrary.IterableSingleView;
import yearlyproject.navigationlibrary.IterableViewGroup;
import yearlyproject.navigationlibrary.IterableViewGroups;

/**
 * Created by Akiva
 * Play and High Score Options activity.
 * Allows various choices on how to play the game.
 */

public class PlayOptionsScrean extends EmergencyCallActivity implements View.OnClickListener {

    SharedPreferences userPreferences, speedSetting;
    SharedPreferences.Editor speedSettingEditor, SettingEditor;
    Button buttonSpeed0, buttonSpeed1, buttonSpeed2, buttonSpeed3, buttonPlayMode0, buttonPlayMode1, buttonSurroundWalls0, buttonSurroundWalls1, back;
    EditText levelToPlay;
    TextView levelToPlayTextView;
    int theme, speed, playMode, gameLevel, surroundWalls, screenType;
    boolean justCreated;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getSharedPreferencesInfo();

        // Set Theme According to Settings
        if (theme == Constants.THEME_HOLO)
			setTheme(android.R.style.Theme_Holo);
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.play_options);

        initiateViews();
        //lets the function to create iterable groups know that we are in onCreate function
        justCreated = true;
        setButtons();

        //set player's current chosen level, and levels available

        initTouchListeners();
    }

    private void getSharedPreferencesInfo() {
        // Grab Existing Settings
        userPreferences = getSharedPreferences(getString(R.string.settings_file), Constants.MODE_PRIVATE_SHARED_PREFRENCES);
        theme = userPreferences.getInt(getString(R.string.theme_settings), Constants.DEFAULT_VALUE_THEME);
        speedSetting = getSharedPreferences(getString(R.string.speed_file), Constants.MODE_PRIVATE_SHARED_PREFRENCES);
        speed = speedSetting.getInt(getString(R.string.speed_settings), Constants.DEFAULT_VALUE_SPEED);
        playMode = speedSetting.getInt(getString(R.string.play_settings), Constants.DEFAULT_VALUE_PLAY);
        gameLevel = speedSetting.getInt(getString(R.string.level_settings), Constants.DEFAULT_VALUE_LEVEL);
        surroundWalls = speedSetting.getInt(getString(R.string.walls_settings), Constants.SURROUND_WALLS);
        screenType = userPreferences.getInt(getString(R.string.HighScoreSettings), Constants.PLAY_OPTIONS_SCREEN);
    }


    private void initiateViews() {
        //setting Items to be used by this activity
        buttonSpeed0 = (Button) findViewById(R.id.buttonSpeed0);
        buttonSpeed1 = (Button) findViewById(R.id.buttonSpeed1);
        buttonSpeed2 = (Button) findViewById(R.id.buttonSpeed2);
        buttonSpeed3 = (Button) findViewById(R.id.buttonSpeed3);
        buttonPlayMode0 = (Button) findViewById(R.id.buttonPlayMode0);
        buttonPlayMode1 = (Button) findViewById(R.id.buttonPlayMode1);
        buttonSurroundWalls0 = (Button) findViewById(R.id.buttonSurroundWalls0);
        buttonSurroundWalls1 = (Button) findViewById(R.id.buttonSurroundWalls1);
        back = (Button) findViewById(R.id.back);
        levelToPlayTextView = (TextView) findViewById(R.id.levelToPlayTextView);
        levelToPlay = (EditText) findViewById(R.id.levelToPlay);

        if (screenType == Constants.HIGH_SCORE_SCREEN)
			back.setText(R.string.HighScoreBackButton);

        levelToPlayTextView.setText(getString(R.string.oneToTen) + Integer.toString(Constants.LEVELS_BUILT));
        levelToPlay.setText(Integer.toString(gameLevel));

        //Setting the 3 buttons navigation system for the activity
        fab_left = (FloatingActionButton) findViewById(R.id.fab_left);
        fab_right = (FloatingActionButton) findViewById(R.id.fab_right);
        fab_middle = (FloatingActionButton) findViewById(R.id.fab_middle);

        //setting the action buttons semi transparent
        fab_left.setAlpha((float) 0.5);
        fab_middle.setAlpha((float) 0.5);
        fab_right.setAlpha((float) 0.5);
    }

    protected void initTouchListeners() {
        //setting listeners
        fab_left.setOnClickListener(this);
        fab_right.setOnClickListener(this);
        fab_middle.setOnClickListener(this);
        fab_left.setOnLongClickListener(this);
        fab_right.setOnLongClickListener(this);
        fab_middle.setOnLongClickListener(this);
        levelToPlay.setOnClickListener(this);

        levelToPlay.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent e) {
                boolean $ = false;
                if (actionId == EditorInfo.IME_ACTION_SEND || actionId == EditorInfo.IME_ACTION_NEXT
                        || actionId == EditorInfo.IME_ACTION_DONE) {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                    hideSoftKey();
                }
                return $;
            }
        });
    }

    private void setButtons() {
        if (playMode != Constants.FREE_STYLE_MODE)
			playModeLevels(null);
		else
			playModeFreeStyle(null);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);

        //setting special keyboard needs because of navigation integration
        switch (v.getId()) {
            case R.id.levelToPlay:
                levelToPlay.setFocusableInTouchMode(true);
                levelToPlay.requestFocus();
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.showSoftInput(levelToPlay, InputMethodManager.SHOW_IMPLICIT);
                levelToPlay.setSelection(levelToPlay.getText().length());
        }

        // overrides background (fix weird bug)
        setButtons();
    }

    // Back Button pressed (or Show High Score Pressed)
    public void back(View v) {
        if (screenType != Constants.HIGH_SCORE_SCREEN)
			onBackPressed();
		else {
			boolean freeStyle = playMode == Constants.FREE_STYLE_MODE,
					surroundWallsbool = surroundWalls == Constants.SURROUND_WALLS;
			if (levelToPlay.getText() == null || levelToPlay.getText().toString().length() == 0) {
				Toast.makeText(this, R.string.level_dosent_exist_message, Toast.LENGTH_SHORT).show();
				return;
			}
			gameLevel = Integer.parseInt(levelToPlay.getText().toString());
			if ((gameLevel < 1 || gameLevel > Constants.LEVELS_BUILT) && playMode == Constants.LEVELS_MODE) {
				Toast.makeText(this, R.string.level_dosent_exist_message, Toast.LENGTH_SHORT).show();
				return;
			}
			SettingEditor = userPreferences.edit();
			SettingEditor.putInt(getString(R.string.highScoreType),
					HighScoreSnake.HighScoreType(freeStyle, surroundWallsbool, speed, gameLevel));
			SettingEditor.commit();
			Intent intent = new Intent(this, HighScoresListShow.class);
			startActivity(intent);
			this.finish();
		}
    }

    // Go Back to Title Screen
    @Override
    public void onBackPressed() {
        //insure player has a legal level if one is required
        if (levelToPlay.getText() == null || levelToPlay.getText().toString().length() == 0) {
            Toast.makeText(this, R.string.level_dosent_exist_message, Toast.LENGTH_SHORT).show();
            return;
        }
        gameLevel = Integer.parseInt(levelToPlay.getText().toString());
        if ((gameLevel < 1 || gameLevel > Constants.LEVELS_BUILT) && playMode == Constants.LEVELS_MODE) {
            Toast.makeText(this, R.string.level_dosent_exist_message, Toast.LENGTH_SHORT).show();
            return;
        }
        if (screenType != Constants.HIGH_SCORE_SCREEN) {
            // Save in speed Settings SharedPreferences
            speedSettingEditor = speedSetting.edit();
            speedSettingEditor.putInt(getString(R.string.play_settings), playMode);
            speedSettingEditor.putInt(getString(R.string.speed_settings), speed);
            speedSettingEditor.putInt(getString(R.string.level_settings), gameLevel);
            speedSettingEditor.putInt(getString(R.string.walls_settings), surroundWalls);
            speedSettingEditor.commit();

            (new BackupManager(this)).dataChanged();
        }
        // Go Home & Close Options Screen
        Intent intent = new Intent(this, TitleScreen.class);
        startActivity(intent);
        this.finish();
    }

    public void speedSlowYes() {
        buttonSpeed0.setEnabled(true);
        buttonSpeed0.getBackground().setColorFilter(0xe0f47521, PorterDuff.Mode.SRC_ATOP);
        buttonSpeed0.invalidate();
    }

    public void speedSlowNo() {
        buttonSpeed0.setEnabled(true);
        buttonSpeed0.getBackground().clearColorFilter();
        buttonSpeed0.invalidate();
    }

    public void speedNormalYes() {
        buttonSpeed1.setEnabled(true);
        buttonSpeed1.getBackground().setColorFilter(0xe0f47521, PorterDuff.Mode.SRC_ATOP);
        buttonSpeed1.invalidate();
    }

    public void speedNormalNo() {
        buttonSpeed1.setEnabled(true);
        buttonSpeed1.getBackground().clearColorFilter();
        buttonSpeed1.invalidate();
    }

    public void speedFastYes() {
        buttonSpeed2.setEnabled(true);
        buttonSpeed2.getBackground().setColorFilter(0xe0f47521, PorterDuff.Mode.SRC_ATOP);
        buttonSpeed2.invalidate();
    }

    public void speedFastNo() {
        buttonSpeed2.setEnabled(true);
        buttonSpeed2.getBackground().clearColorFilter();
        buttonSpeed2.invalidate();
    }

    public void speedExtremeYes() {
        buttonSpeed3.setEnabled(true);
        buttonSpeed3.getBackground().setColorFilter(0xe0f47521, PorterDuff.Mode.SRC_ATOP);
        buttonSpeed3.invalidate();
    }

    public void speedExtremeNo() {
        buttonSpeed3.setEnabled(true);
        buttonSpeed3.getBackground().clearColorFilter();
        buttonSpeed3.invalidate();
    }

    //if speed slow is/was pressed
    public void speedSlow(View view) {
        speedSlowYes();
        speedNormalNo();
        speedFastNo();
        speedExtremeNo();

        speed = Constants.SLOW_SPEED;
    }

    //if speed normal is/was pressed
    public void speedNormal(View view) {
        speedNormalYes();
        speedSlowNo();
        speedFastNo();
        speedExtremeNo();

        speed = Constants.NORMAL_SPEED;
    }

    //if speed fast is/was pressed
    public void speedFast(View view) {
        speedFastYes();
        speedNormalNo();
        speedSlowNo();
        speedExtremeNo();

        speed = Constants.FAST_SPEED;
    }

    //if speed extreme is/was pressed
    public void speedExtreme(View view) {
        speedExtremeYes();
        speedNormalNo();
        speedFastNo();
        speedSlowNo();

        speed = Constants.EXTREME_SPEED;
    }


    //if with Surround Walls is/was pressed
    public void withSurroundWalls(View view) {
        buttonSurroundWalls0.setEnabled(true);
        buttonSurroundWalls0.getBackground().setColorFilter(0xe0f47521, PorterDuff.Mode.SRC_ATOP);
        buttonSurroundWalls0.invalidate();

        buttonSurroundWalls1.setEnabled(true);
        buttonSurroundWalls1.getBackground().clearColorFilter();
        buttonSurroundWalls1.invalidate();

        surroundWalls = Constants.SURROUND_WALLS;
    }

    //if no Surround Walls is/was pressed
    public void withOutSurroundWalls(View view) {
        buttonSurroundWalls1.setEnabled(true);
        buttonSurroundWalls1.getBackground().setColorFilter(0xe0f47521, PorterDuff.Mode.SRC_ATOP);
        buttonSurroundWalls1.invalidate();

        buttonSurroundWalls0.setEnabled(true);
        buttonSurroundWalls0.getBackground().clearColorFilter();
        buttonSurroundWalls0.invalidate();

        surroundWalls = Constants.NO_SURROUND_WALLS;
    }


    //if play mode free style is/was pressed
    public void playModeFreeStyle(View view) {
        buttonPlayMode0.getBackground().setColorFilter(0xe0f47521, PorterDuff.Mode.SRC_ATOP);
        buttonPlayMode0.invalidate();

        buttonPlayMode1.getBackground().clearColorFilter();
        buttonPlayMode1.invalidate();

        levelToPlay = (EditText) findViewById(R.id.levelToPlay);
        levelToPlay.setEnabled(false);

        setButtonsFreeStyle();
        prepareIterableGroupsFreeStyle();

        playMode = Constants.FREE_STYLE_MODE;
        justCreated = false;
    }

    private void prepareIterableGroupsFreeStyle() {
        if (playMode != Constants.FREE_STYLE_MODE || justCreated) {
            if (group != null)
				group.stop();

            //creating lists to iterate over with navigation module, taking into account enabled things only
            List<View> speedList = new ArrayList<>();
            speedList.add(buttonSpeed0);
            speedList.add(buttonSpeed1);
            speedList.add(buttonSpeed2);
            speedList.add(buttonSpeed3);

            List<View> playModeList = new ArrayList<>();
            playModeList.add(buttonPlayMode0);
            playModeList.add(buttonPlayMode1);

            List<View> wallsList = new ArrayList<>();
            wallsList.add(buttonSurroundWalls0);
            wallsList.add(buttonSurroundWalls1);

            List<IterableGroup> list = new ArrayList<>();
            list.add(new IterableViewGroup(playModeList, findViewById(R.id.LayoutPlayMode), this));
            list.add(new IterableViewGroup(wallsList, findViewById(R.id.LayoutSurroundWalls), this));
            list.add(new IterableViewGroup(speedList, findViewById(R.id.LayoutSpeed), this));
            list.add(new IterableSingleView(back, this));

            group = new IterableViewGroups(list, findViewById(R.id.incompassingLayoutPlayOptions), this);
            group.start();
        }
    }

    public void setButtonsFreeStyle() {
        // Speed set button
        switch (speed) {
            case Constants.SLOW_SPEED:
                speedSlow(null);
                break;
            case Constants.NORMAL_SPEED:
                speedNormal(null);
                break;
            case Constants.FAST_SPEED:
                speedFast(null);
                break;
            case Constants.EXTREME_SPEED:
                speedExtreme(null);
                break;
        }

        // Walls set button
        if (surroundWalls == Constants.SURROUND_WALLS)
			withSurroundWalls(null);
		else
			withOutSurroundWalls(null);
    }

    //if play mode levels is/was pressed
    public void playModeLevels(View view) {
        buttonPlayMode1.getBackground().setColorFilter(0xe0f47521, PorterDuff.Mode.SRC_ATOP);
        buttonPlayMode1.invalidate();

        buttonPlayMode0.getBackground().clearColorFilter();
        buttonPlayMode0.invalidate();

        levelToPlay.setEnabled(true);

        disableButtonsLevels();
        prepareIterableGroupsLevels();

        playMode = Constants.LEVELS_MODE;
        justCreated = false;
    }

    private void prepareIterableGroupsLevels() {
        if (playMode != Constants.LEVELS_MODE || justCreated) {
            if (group != null)
				group.stop();

            //creating lists to iterate over with navigation module, taking into account enabled things only
            List<View> playModeList = new ArrayList<>();
            playModeList.add(buttonPlayMode0);
            playModeList.add(buttonPlayMode1);

            List<IterableGroup> list = new ArrayList<>();
            list.add(new IterableViewGroup(playModeList, findViewById(R.id.LayoutPlayMode), this));
            list.add(new IterableSingleView(levelToPlay, this));
            list.add(new IterableSingleView(back, this));

            group = new IterableViewGroups(list, findViewById(R.id.incompassingLayoutPlayOptions), this);
            group.start();
        }
    }

    private void disableButtonsLevels() {
        //disable and clear background of all buttons not connected to levels
        buttonSpeed0.setEnabled(false);
        buttonSpeed0.getBackground().clearColorFilter();
        buttonSpeed0.invalidate();

        buttonSpeed1.setEnabled(false);
        buttonSpeed1.getBackground().clearColorFilter();
        buttonSpeed1.invalidate();

        buttonSpeed2.setEnabled(false);
        buttonSpeed2.getBackground().clearColorFilter();
        buttonSpeed2.invalidate();

        buttonSpeed3.setEnabled(false);
        buttonSpeed3.getBackground().clearColorFilter();
        buttonSpeed3.invalidate();

        buttonSurroundWalls0.setEnabled(false);
        buttonSurroundWalls0.getBackground().clearColorFilter();
        buttonSurroundWalls0.invalidate();

        buttonSurroundWalls1.setEnabled(false);
        buttonSurroundWalls1.getBackground().clearColorFilter();
        buttonSurroundWalls1.invalidate();
    }

}