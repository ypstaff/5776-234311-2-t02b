package yearlyproject.snake;


import android.app.backup.BackupManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import yearlyproject.navigationlibrary.EmergencyCallActivity;
import yearlyproject.navigationlibrary.IterableGroup;
import yearlyproject.navigationlibrary.IterableSingleView;
import yearlyproject.navigationlibrary.IterableViewGroup;
import yearlyproject.navigationlibrary.IterableViewGroups;

/**
 * Created by Akiva
 * Options activity.
 * Allows various choices on the settings of the game.
 */

public class OptionsScreen extends EmergencyCallActivity implements View.OnClickListener {

    SharedPreferences userPreferences;
    SharedPreferences.Editor userPreferencesEditor;
    Button buttonTheme0, buttonTheme1, buttonControls1, buttonControls2, buttonView0, buttonView1, back;
    EditText playerName;
    int theme, controls, viewMode;
    String name;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        getSharedPreferencesInfo();
        // Set Theme According to Settings
        if (theme == Constants.THEME_HOLO)
			setTheme(android.R.style.Theme_Holo);
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.options);

        initiateViews();
        initTouchListeners();
        setButtons();
        prepareIterableGroups();
    }

    private void getSharedPreferencesInfo() {
        // Grab Existing Settings
        userPreferences = getSharedPreferences(getString(R.string.settings_file), Constants.MODE_PRIVATE_SHARED_PREFRENCES);
        theme = userPreferences.getInt(getString(R.string.theme_settings), Constants.DEFAULT_VALUE_THEME);
        controls = userPreferences.getInt(getString(R.string.controls_settings), Constants.DEFAULT_VALUE_CONTROL);
        viewMode = userPreferences.getInt(getString(R.string.view_settings), Constants.DEFAULT_VALUE_VIEW);
        name = userPreferences.getString(getString(R.string.name_settings), getString(R.string.default_player_name));
    }

    private void initiateViews() {
        //set player's current name
        playerName = (EditText) findViewById(R.id.playerName);
        playerName.setText(name);

        //setting Buttons to be used by this activity
        buttonTheme0 = (Button) findViewById(R.id.buttonTheme0);
        buttonTheme1 = (Button) findViewById(R.id.buttonTheme1);
        buttonControls1 = (Button) findViewById(R.id.buttonControls1);
        buttonControls2 = (Button) findViewById(R.id.buttonControls2);
        buttonView0 = (Button) findViewById(R.id.buttonView0);
        buttonView1 = (Button) findViewById(R.id.buttonView1);
        back = (Button) findViewById(R.id.back);

        //Setting the 3 buttons navigation system for the activity
        fab_left = (FloatingActionButton) findViewById(R.id.fab_left);
        fab_right = (FloatingActionButton) findViewById(R.id.fab_right);
        fab_middle = (FloatingActionButton) findViewById(R.id.fab_middle);

        //setting the action buttons semi transparent
        fab_left.setAlpha((float) 0.5);
        fab_middle.setAlpha((float) 0.5);
        fab_right.setAlpha((float) 0.5);
    }

    protected void initTouchListeners() {
        //setting listeners
        fab_left.setOnClickListener(this);
        fab_right.setOnClickListener(this);
        fab_middle.setOnClickListener(this);
        fab_left.setOnLongClickListener(this);
        fab_right.setOnLongClickListener(this);
        fab_middle.setOnLongClickListener(this);
        playerName.setOnClickListener(this);

        playerName.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent e) {
                boolean $ = false;
                if (actionId == EditorInfo.IME_ACTION_SEND || actionId == EditorInfo.IME_ACTION_NEXT
                        || actionId == EditorInfo.IME_ACTION_DONE) {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                    hideSoftKey();
                }
                return $;
            }
        });
    }

    private void setButtons() {
        if (theme != Constants.LIGHT_THEME)
			DarkTheme(null);
		else
			LightTheme(null);

        // controls set button
        if (controls == Constants.TWO_BUTTONS_CONTROLS_SNAKE_POV)
			TwoButtons(null);
		else
			TwoButtonsBoard(null);

        // Mode set button
        if (viewMode == Constants.MODERN_MODE)
			ViewModern(null);
		else
			ViewClassic(null);
    }

    private void prepareIterableGroups() {
        //creating lists to iterate over with navigation module
        List<View> themesList = new ArrayList<>();
        themesList.add(buttonTheme0);
        themesList.add(buttonTheme1);

        List<View> controlsList = new ArrayList<>();
        controlsList.add(buttonControls1);
        controlsList.add(buttonControls2);

        List<View> viewsList = new ArrayList<>();
        viewsList.add(buttonView0);
        viewsList.add(buttonView1);

        List<IterableGroup> list = new ArrayList<>();
        list.add(new IterableSingleView(playerName, this));
        list.add(new IterableViewGroup(themesList, findViewById(R.id.LayoutTheme), this));
        list.add(new IterableViewGroup(controlsList, findViewById(R.id.LayoutControls), this));
        list.add(new IterableViewGroup(viewsList, findViewById(R.id.LayoutView), this));
        list.add(new IterableSingleView(back, this));

        group = new IterableViewGroups(list, findViewById(R.id.incompassingLayoutOptions), this);
        group.start();
    }

    public void onClick(View v) {
        super.onClick(v);

        //setting special keyboard needs because of navigation integration
        switch (v.getId()) {
            case R.id.playerName:
                playerName.setFocusableInTouchMode(true);
                playerName.requestFocus();
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.showSoftInput(playerName, InputMethodManager.SHOW_IMPLICIT);
                playerName.setSelection(playerName.getText().length());
        }

        // overrides background (fix weird bug)
        setButtons();
    }

    // Back Button pressed
    public void back(View v) {
        onBackPressed();
    }

    // Go Back to Title Screen
    @Override
    public void onBackPressed() {
        //insure player has a name
        if (playerName.getText() == null || playerName.getText().toString().length() == Constants.NO_STRING_NUM) {
            Toast.makeText(this, R.string.no_name_toast, Toast.LENGTH_SHORT).show();
            return;
        }

        // Save in Settings SharedPreferences
        name = playerName.getText().toString();
        userPreferencesEditor = userPreferences.edit();
        userPreferencesEditor.putInt(getString(R.string.theme_settings), theme);
        userPreferencesEditor.putInt(getString(R.string.controls_settings), controls);
        userPreferencesEditor.putInt(getString(R.string.view_settings), viewMode);
        userPreferencesEditor.putString(getString(R.string.name_settings), name);
        userPreferencesEditor.commit();

        // Call for Backup
        BackupManager backupManager = new BackupManager(this);
        backupManager.dataChanged();

        // Go to Title Screen & Close Options Screen
        Intent intent = new Intent(this, TitleScreen.class);
        startActivity(intent);
        this.finish();
    }

    //if light theme is/was pressed
    public void LightTheme(View v) {
        buttonTheme0.getBackground().setColorFilter(0xe0f47521, PorterDuff.Mode.SRC_ATOP);
        buttonTheme0.invalidate();

        buttonTheme1.getBackground().clearColorFilter();
        buttonTheme1.invalidate();

        theme = Constants.LIGHT_THEME;
    }

    //if Dark theme is/was pressed
    public void DarkTheme(View v) {
        buttonTheme1.getBackground().setColorFilter(0xe0f47521, PorterDuff.Mode.SRC_ATOP);
        buttonTheme1.invalidate();

        buttonTheme0.getBackground().clearColorFilter();
        buttonTheme0.invalidate();

        theme = Constants.DARK_THEME;
    }

    //if Two Buttons snake P.O.V is/was pressed
    public void TwoButtons(View v) {
        buttonControls1.getBackground().setColorFilter(0xe0f47521, PorterDuff.Mode.SRC_ATOP);
        buttonControls1.invalidate();

        buttonControls2.getBackground().clearColorFilter();
        buttonControls2.invalidate();

        controls = Constants.TWO_BUTTONS_CONTROLS_SNAKE_POV;
    }

    //if Two Buttons Board P.O.V is/was pressed
    public void TwoButtonsBoard(View v) {
        buttonControls2.getBackground().setColorFilter(0xe0f47521, PorterDuff.Mode.SRC_ATOP);
        buttonControls2.invalidate();

        buttonControls1.getBackground().clearColorFilter();
        buttonControls1.invalidate();

        controls = Constants.TWO_BUTTONS_CONTROLS_BOARD_POV;
    }

    //if Modern view is/was pressed
    public void ViewModern(View v) {
        buttonView0.getBackground().setColorFilter(0xe0f47521, PorterDuff.Mode.SRC_ATOP);
        buttonView0.invalidate();

        buttonView1.getBackground().clearColorFilter();
        buttonView1.invalidate();

        viewMode = Constants.MODERN_MODE;
    }

    //if Classic view is/was pressed
    public void ViewClassic(View v) {
        buttonView1.getBackground().setColorFilter(0xe0f47521, PorterDuff.Mode.SRC_ATOP);
        buttonView1.invalidate();

        buttonView0.getBackground().clearColorFilter();
        buttonView0.invalidate();

        viewMode = Constants.CLASSIC_MODE;
    }

}
