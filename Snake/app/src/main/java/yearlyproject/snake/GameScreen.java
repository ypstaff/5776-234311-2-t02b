package yearlyproject.snake;


import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.TextView;

import java.util.List;

import yearlyproject.navigationlibrary.EmergencyCallActivity;

/**
 * Created by Akiva
 * Game Screen activity.
 * Handel's the interaction with user playing the game.
 */

public class GameScreen extends EmergencyCallActivity {

    private Game game;
    private Activity mActivity;
    SharedPreferences userPreferences, speedSetting;
    SharedPreferences.Editor SettingEditor;
    private boolean darkTheme = false, classicMode = false, realyWantBack = false;
    private int controls, playMode, surroundWalls, speed, gameLevel;
    String name;

    // Initialize Game Screen
    @Override
    public void onCreate(Bundle savedInstanceState) {
        // Set Theme, Controls Mode, View Mode and more according to to SharedPreferences Files
        userPreferences = getSharedPreferences(getString(R.string.settings_file), Constants.MODE_PRIVATE_SHARED_PREFRENCES);
        speedSetting = getSharedPreferences(getString(R.string.speed_file), Constants.MODE_PRIVATE_SHARED_PREFRENCES);
        if (userPreferences.getInt(getString(R.string.theme_settings), Constants.DEFAULT_VALUE_THEME) == Constants.THEME_HOLO) {
            setTheme(android.R.style.Theme_Holo);
            darkTheme = true;
        }
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        getSharedPreferencesInfo();

        super.onCreate(savedInstanceState);
        setContentView(R.layout.game_2arrow);
        mActivity = this;
        TextView score = (TextView) findViewById(R.id.score);

        // Create Game View, this will be the class that does all the work of the game
        game = new Game(this, this, score, darkTheme, classicMode, controls, speed, playMode, gameLevel, surroundWalls);

        //setting the game to be played in the game frame
        FrameLayout frameView = (FrameLayout) findViewById(R.id.gameFrame);
        frameView.addView(game);

        initiateViews();
        initTouchListeners();
        setImagesToControls();
    }

    private void initiateViews() {
        //Setting the 3 buttons navigation system for the activity
        fab_left = (FloatingActionButton) findViewById(R.id.fab_left);
        fab_right = (FloatingActionButton) findViewById(R.id.fab_right);
        fab_middle = (FloatingActionButton) findViewById(R.id.fab_middle);

        //setting the action buttons semi transparent
        fab_left.setAlpha((float) 0.5);
        fab_middle.setAlpha((float) 0.5);
        fab_right.setAlpha((float) 0.5);
    }

    protected void initTouchListeners() {
        //setting listeners
        fab_left.setOnClickListener(this);
        fab_right.setOnClickListener(this);
        fab_middle.setOnClickListener(this);
        fab_left.setOnLongClickListener(this);
        fab_right.setOnLongClickListener(this);
        fab_middle.setOnLongClickListener(this);
    }

    private void getSharedPreferencesInfo() {
        if (userPreferences.getInt(getString(R.string.view_settings), Constants.DEFAULT_VALUE_VIEW) == Constants.CLASSIC_MODE)
			classicMode = true;
        controls = userPreferences.getInt(getString(R.string.controls_settings), Constants.DEFAULT_VALUE_CONTROL);
        name = userPreferences.getString(getString(R.string.name_settings), getString(R.string.default_player_name));
        speed = speedSetting.getInt(getString(R.string.speed_settings), Constants.DEFAULT_VALUE_SPEED);
        playMode = speedSetting.getInt(getString(R.string.play_settings), Constants.DEFAULT_VALUE_PLAY);
        gameLevel = speedSetting.getInt(getString(R.string.level_settings), Constants.DEFAULT_VALUE_LEVEL);
        surroundWalls = speedSetting.getInt(getString(R.string.walls_settings), Constants.DEFAULT_VALUE_LEVEL);
    }

    private void setImagesToControls() {
        //set the proper images to control buttons base on control type
        if (controls == Constants.TWO_BUTTONS_CONTROLS_SNAKE_POV) {
            fab_left.setImageResource(R.drawable.leftarrow);
            fab_right.setImageResource(R.drawable.rightarrow);
        } else {
            fab_left.setImageResource(R.drawable.down_left_arrow);
            fab_right.setImageResource(R.drawable.up_right_arrow);
        }
        fab_middle.setImageResource(R.drawable.back);
    }

    // Back Button pressed
    @Override
    public void onBackPressed() {
        // Back Button really pressed, needed because of complications of navigation module integration
        if (realyWantBack)
			this.finish();
    }

    @Override
    public void onClick(View v) {
        try {
            super.onClick(v);
        } catch (Exception e) {
            //  needed because of complications of navigation module integration
        }
        //choose which direction to go based on control mode
        if (controls == Constants.TWO_BUTTONS_CONTROLS_SNAKE_POV)
			switch (v.getId()) {
			case R.id.fab_left:
				leftClick(null);
				break;
			case R.id.fab_right:
				rightClick(null);
				break;
			case R.id.fab_middle:
				realyWantBack = true;
				onBackPressed();
				break;
			}
		else
			switch (v.getId()) {
			case R.id.fab_left:
				if (this.game.snake.direction == Constants.DIRECTION_RIGHT
						|| this.game.snake.direction == Constants.DIRECTION_LEFT)
					downClick(null);
				else
					leftClick(null);
				break;
			case R.id.fab_right:
				if (this.game.snake.direction == Constants.DIRECTION_RIGHT
						|| this.game.snake.direction == Constants.DIRECTION_LEFT)
					upClick(null);
				else
					rightClick(null);
				break;
			case R.id.fab_middle:
				realyWantBack = true;
				onBackPressed();
				break;
			}
    }

    // On Left Arrow Click, Snake Turns Left
    public void leftClick(View v) {
        game.snake.turnLeft();
    }

    // On Right Arrow Click, Snake Turns Right
    public void rightClick(View v) {
        game.snake.turnRight();
    }

    // On Down Arrow Click, Snake Turns Down
    public void downClick(View v) {
        game.snake.turnDown();
    }

    // On Up Arrow Click, Snake Turns Up
    public void upClick(View v) {
        game.snake.turnUp();
    }

    /**
     * On Game Over go to Game Over activity
     * Called from Game Object
     */
    public void gameOver() {
        //create new high score for this game
        boolean freeStyle = playMode == Constants.FREE_STYLE_MODE, surroundWallsbool = surroundWalls == Constants.SURROUND_WALLS;
        int gameType = HighScoreSnake.HighScoreType(freeStyle, surroundWallsbool, speed, gameLevel);
        HighScoreSnake highScoreSnake = new HighScoreSnake(name, this.game.score, gameType);
        highScoreSnake.save();
        //get all high scores saved
        List<HighScoreSnake> listScoresGet = HighScoreSnake.listAll(HighScoreSnake.class);
        //insure a nice amount of high scores remains over time
        if (listScoresGet.size() < Constants.MIN_HIGH_SCORES)
			for (int j = 0; j < Constants.LEVELS_BUILT + Constants.GAME_TYPE_NOT_LEVEL; ++j)
				for (int i = 0; i < Constants.NUM_OF_HIGH_SCORES_TAKEN; ++i)
					listScoresGet.add(new HighScoreSnake(getString(R.string.taunt), i, j));
        HighScoreSnake.deleteAll(HighScoreSnake.class);
        HighScoreSnake.saveInTx(listScoresGet);

        //save game type
        SettingEditor = userPreferences.edit();
        SettingEditor.putInt(getString(R.string.highScoreType), gameType);
        SettingEditor.commit();

        //Go To Game Over activity
        Intent intent = new Intent(this, GameOverScreen.class);
        startActivity(intent);
        this.finish();
    }

    //what to do when game resumes
    public void resumeGame() {
        if (game == null || game.snake == null)
			return;
		game.snake.stopped = false;
		game.invalidate();
    }

    // On Game Pause
    public void pauseGame() {
        // Do nothing if game is over
        if (game.gameOver)
			return;
        //pause game
        game.snake.stopped = true;
    }

    // Hardware Button Presses
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent e) {
        // On Menu or Back Press, end game
        if ((keyCode == KeyEvent.KEYCODE_MENU || keyCode == KeyEvent.KEYCODE_BACK) && e.getRepeatCount() == Constants.NO_REPEAT)
            mActivity.finish();

        // On Left D-Pad Button, Snake Turns Left
        if ((keyCode == KeyEvent.KEYCODE_DPAD_LEFT) && e.getRepeatCount() == Constants.NO_REPEAT)
            game.snake.turnLeft();

        // On Right D-Pad Button, Snake Turns Right
        if ((keyCode == KeyEvent.KEYCODE_DPAD_RIGHT) && e.getRepeatCount() == Constants.NO_REPEAT)
            game.snake.turnRight();

        return true;
    }

    // Pause Game when Activity is paused
    @Override
    public void onPause() {
        super.onPause();
        pauseGame();
    }

    // Resume Game when Activity is resumed
    @Override
    public void onResume() {
        super.onResume();
        resumeGame();
    }

}
