package yearlyproject.snake;

/**
 * Created by Akiva on 21/12/2015.
 * holds Constants for snake app
 */
public class Constants {
    public static final int NO_STRING_NUM = 0;
    public static final int MODE_PRIVATE_SHARED_PREFRENCES = 0;
    public static final int DEFAULT_VALUE_THEME = 0;
    public static final int DEFAULT_VALUE_CONTROL = 0;
    public static final int DEFAULT_VALUE_SPEED = 0;
    public static final int DEFAULT_VALUE_VIEW = 0;
    public static final int DEFAULT_VALUE_PLAY = 0;
    public static final int DEFAULT_VALUE_LEVEL = 1;
    public static final int THEME_HOLO = 1;
    public static final int FRAME_RATE_MULT = 5;
    public static final int FRAME_RATE_ADD = 1;
    public static final int FRAME_RATE_BASE_GAME_RUNNING = 1000;
    public static final int FRAME_RATE_BASE_GAME_STOPPED = 500;
    public static final double INWIDTH_MULT = 10.0;
    public static final double INHEIGHT_MULT = 10.0;
    public static final int MIN_SQUARE_NUM = 15;
    public static final int PADDING_DEV = 2;
    public static final int CLASSIC_MODE_DEV = 20;
    public static final int SCORE_SETUP = -1;
    public static final int DEFAULT_HIGH_SCORE = 0;
    public static final int GRAPH_START_NUM = 0;
    public static final int BLACK_COLOR_WALL = 0;
    public static final int BLUE_COLOR_SNAKE = 1;
    public static final int GREY_COLOR_FOOD = 2;
    public static final int RED_COLOR_HEAD = 3;
    public static final int SNAKE_LENGTH_START = 3;
    public static final int DIRECTION_RIGHT = 0;
    public static final int DIRECTION_LEFT = 2;
    public static final int DIRECTION_UP = 3;
    public static final int DIRECTION_DOWN = 1;
    public static final int NUM_OF_DIRECTION = 4;
    public static final int TURN_LEFT = -1;
    public static final int TURN_RIGHT = 1;
    public static final int LIGHT_THEME = 0;
    public static final int DARK_THEME = 1;
    public static final int TWO_BUTTONS_CONTROLS_SNAKE_POV = 1;
    public static final int TWO_BUTTONS_CONTROLS_BOARD_POV = 2;
    public static final int MODERN_MODE = 0;
    public static final int CLASSIC_MODE = 1;
    public static final int FREE_STYLE_MODE = 0;
    public static final int LEVELS_MODE = 1;
    public static final int SLOW_SPEED = 0;
    public static final int NORMAL_SPEED = 1;
    public static final int FAST_SPEED = 2;
    public static final int EXTREME_SPEED = 3;
    public static final int SURROUND_WALLS = 0;
    public static final int NO_SURROUND_WALLS = 1;
    public static final int LEVELS_BUILT = 10;
    public static final int NUM_OF_HIGH_SCORES_TAKEN = 20;
    public static final int NO_REPEAT = 0;
    public static final int DIV_WALLS_QUARTER = 4;
    public static final int MUL_WALLS_THREE_QUARTER = 3;
    public static final int DIV_TO_HALF_SCREEN = 2;
    public static final int DIRECTION_REMOVED_ONE = 1;
    public static final int DIRECTION_REMOVED_TWO = 2;

    public static final int HIGH_SCORE_SCREEN = 1;
    public static final int PLAY_OPTIONS_SCREEN = 0;
    public static final int WITH_WALLS_SLOW = 2;
    public static final int WITH_WALLS_NORMAL = 3;
    public static final int WITH_WALLS_FAST = 4;
    public static final int WITH_WALLS_EXTREME = 5;
    public static final int WITHOUT_WALLS_SLOW = 6;
    public static final int WITHOUT_WALLS_NORMAL = 7;
    public static final int WITHOUT_WALLS_FAST = 8;
    public static final int WITHOUT_WALLS_EXTREME = 9;

    public static final int LEVEL_1 = 11;
    public static final int LEVEL_2 = 12;
    public static final int LEVEL_3 = 13;
    public static final int LEVEL_4 = 14;
    public static final int LEVEL_5 = 15;
    public static final int LEVEL_6 = 16;
    public static final int LEVEL_7 = 17;
    public static final int LEVEL_8 = 18;
    public static final int LEVEL_9 = 19;
    public static final int LEVEL_10 = 10;


    public static final int MIN_HIGH_SCORES = 30;
    public static final int GAME_TYPE_NOT_LEVEL = 10;


}
