package yearlyproject.snake;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import yearlyproject.navigationlibrary.EmergencyCallActivity;
import yearlyproject.navigationlibrary.IterableGroup;
import yearlyproject.navigationlibrary.IterableListView;
import yearlyproject.navigationlibrary.IterableSingleView;
import yearlyproject.navigationlibrary.IterableViewGroups;

/**
 * Created by Akiva
 * Activity to display High Scores
 */

public class HighScoresListShow extends EmergencyCallActivity {

    SharedPreferences settings;
    Button button2;
    TextView textViewTitle;
    ListView lv_myList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        // Set Theme According to Settings
        settings = getSharedPreferences(getString(R.string.settings_file), Constants.MODE_PRIVATE_SHARED_PREFRENCES);
        if (settings.getInt(getString(R.string.theme_settings), Constants.DEFAULT_VALUE_THEME) == Constants.THEME_HOLO)
			setTheme(android.R.style.Theme_Holo);
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_high_scores);

        initiateViews();
        initTouchListeners();
        prepareIterableGroups();

        //set high score title
        int highScoreType = settings.getInt(getString(R.string.highScoreType), Constants.LEVEL_1);
        textViewTitle.setText(HighScoreSnake.typeToTitle(highScoreType));

        //Getting High Scores from SQLite DB, and taking the right type of high scores
        List<HighScoreSnake> listScores = HighScoreSnake.listAll(HighScoreSnake.class), listScoresRightType = new LinkedList<>();
        for (HighScoreSnake score : listScores)
			if (score.getHighScoreType() == highScoreType)
				listScoresRightType.add(score);

        //take the 20 best high scores in order
        Collections.sort(listScoresRightType);
        Collections.reverse(listScoresRightType);
        List<HighScoreSnake> subList = new ArrayList<>(listScoresRightType.subList(0, Constants.NUM_OF_HIGH_SCORES_TAKEN));

        lv_myList.setAdapter((new MyAdapter(subList)));
    }

    private void initiateViews() {
        //setting items to be used by this activity
        button2 = (Button) findViewById(R.id.button2);
        textViewTitle = (TextView) findViewById(R.id.textViewTitle);
        lv_myList = (ListView) findViewById(R.id.lv_myList);

        //Setting the 3 buttons navigation system for the activity
        fab_left = (FloatingActionButton) findViewById(R.id.fab_left);
        fab_right = (FloatingActionButton) findViewById(R.id.fab_right);
        fab_middle = (FloatingActionButton) findViewById(R.id.fab_middle);

        //setting the action buttons semi transparent
        fab_left.setAlpha((float) 0.5);
        fab_middle.setAlpha((float) 0.5);
        fab_right.setAlpha((float) 0.5);
    }

    protected void initTouchListeners() {
        //setting listeners
        fab_left.setOnClickListener(this);
        fab_right.setOnClickListener(this);
        fab_middle.setOnClickListener(this);
        fab_left.setOnLongClickListener(this);
        fab_right.setOnLongClickListener(this);
        fab_middle.setOnLongClickListener(this);
    }

    private void prepareIterableGroups() {
        //creating list to iterate over with navigation module
        List<IterableGroup> list = new ArrayList<>();
        list.add(new IterableListView(lv_myList, this));
        list.add(new IterableSingleView(button2, this));

        group = new IterableViewGroups(list, findViewById(R.id.incompassingLayoutHighScore), this);
        group.start();
    }

    private class MyAdapter extends BaseAdapter {

        private List<HighScoreSnake> myList1;

        public MyAdapter(List<HighScoreSnake> aList1) {
            //putting High Scores in order to display them
            Collections.sort(aList1);
            Collections.reverse(aList1);
            myList1 = aList1;
        }

        @Override
        public int getCount() {
            return myList1.size();
        }

        @Override
        public Object getItem(int position) {
            return myList1.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view;
            ViewHolder viewHolder;

            if (convertView != null) {
				view = convertView;
				viewHolder = (ViewHolder) view.getTag();
			} else {
				LayoutInflater li = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				view = li.inflate(R.layout.list_row_layout, null);
				viewHolder = new ViewHolder();
				viewHolder.myText1 = (TextView) view.findViewById(R.id.tv_text1);
				viewHolder.myText2 = (TextView) view.findViewById(R.id.tv_text2);
				view.setTag(viewHolder);
			}

            // Put the content in the view
            viewHolder.myText1.setText(myList1.get(position).getName());
            viewHolder.myText2.setText(Integer.toString(myList1.get(position).getScore()));
            return view;
        }

        private class ViewHolder {
            TextView myText1;
            TextView myText2;
        }
    }

    // On "Back" Button Press, Go to Title Screen (Menu)
    public void goToMenu(View v) {
        this.finish();
    }

}
