package yearlyproject.snake;


import android.annotation.TargetApi;
import android.app.backup.BackupAgentHelper;
import android.app.backup.SharedPreferencesBackupHelper;
import android.os.Build;

/**
 * Created by Akiva
 * Backing up game settings
 */

@TargetApi(Build.VERSION_CODES.FROYO)
public class SnakeBackup extends BackupAgentHelper {
    // The name of the SharedPreferences file
    static final String PREFS = "settings";

    // A key to uniquely identify the set of backup data
    static final String PREFS_BACKUP_KEY = "settings";

    // Allocate a helper and add it to the backup agent
    @Override
    public void onCreate() {
        addHelper(PREFS_BACKUP_KEY, (new SharedPreferencesBackupHelper(this, PREFS)));
    }
}