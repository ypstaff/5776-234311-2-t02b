package yearlyproject.snake;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.view.View;
import android.view.Window;
import android.widget.Button;

import java.util.ArrayList;
import java.util.List;

import yearlyproject.navigationlibrary.EmergencyCallActivity;
import yearlyproject.navigationlibrary.IterableGroup;
import yearlyproject.navigationlibrary.IterableSingleView;
import yearlyproject.navigationlibrary.IterableViewGroups;

/**
 * Created by Akiva
 * Game Over activity
 */

public class GameOverScreen extends EmergencyCallActivity {

    SharedPreferences settings;
    Button button1;
    Button button2;
    Button button3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        // Set Theme According to Settings
        settings = getSharedPreferences(getString(R.string.settings_file), Constants.MODE_PRIVATE_SHARED_PREFRENCES);
        if (settings.getInt(getString(R.string.theme_settings), Constants.DEFAULT_VALUE_THEME) == Constants.THEME_HOLO)
			setTheme(android.R.style.Theme_Holo);
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.game_over);

        initiateViews();
        initTouchListeners();
        prepareIterableGroups();
    }

    private void initiateViews() {
        //setting buttons to be used by this activity
        button1 = (Button) findViewById(R.id.button1);
        button2 = (Button) findViewById(R.id.button2);
        button3 = (Button) findViewById(R.id.button3);

        //Setting the 3 buttons navigation system for the activity
        fab_left = (FloatingActionButton) findViewById(R.id.fab_left);
        fab_right = (FloatingActionButton) findViewById(R.id.fab_right);
        fab_middle = (FloatingActionButton) findViewById(R.id.fab_middle);

        //setting the action buttons semi transparent
        fab_left.setAlpha((float) 0.5);
        fab_middle.setAlpha((float) 0.5);
        fab_right.setAlpha((float) 0.5);
    }

    protected void initTouchListeners() {
        //setting listeners
        fab_left.setOnClickListener(this);
        fab_right.setOnClickListener(this);
        fab_middle.setOnClickListener(this);
        fab_left.setOnLongClickListener(this);
        fab_right.setOnLongClickListener(this);
        fab_middle.setOnLongClickListener(this);
    }

    private void prepareIterableGroups() {
        //creating list to iterate over with navigation module
        List<IterableGroup> list = new ArrayList<>();
        list.add(new IterableSingleView(button1, this));
        list.add(new IterableSingleView(button2, this));
        list.add(new IterableSingleView(button3, this));

        group = new IterableViewGroups(list, findViewById(R.id.incompassingLayoutGameOver), this);
        group.start();
    }


    // On "Play Again" Button Press, Start Game again
    public void playAgain(View v) {
        Intent intent = new Intent(this, GameScreen.class);
        startActivity(intent);
        this.finish();
    }

    // On "Go To Menu" Button Press, Go to Menu
    public void goToMenu(View v) {
        this.finish();
    }

    // On "Go To High Scores" Button Press, Go to High Scores
    public void goToHighScore(View v) {
        Intent intent = new Intent(this, HighScoresListShow.class);
        startActivity(intent);
        this.finish();
    }
}
