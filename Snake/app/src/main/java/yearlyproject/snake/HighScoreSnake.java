package yearlyproject.snake;


import com.orm.SugarRecord;

/**
 * Created by Akiva
 * Class to save High Scores
 * Using amazing tools made by SugarRecord that make using SQLite really easy
 */

public class HighScoreSnake extends SugarRecord implements Comparable<HighScoreSnake> {
    String name;
    int score, HighScoreTypeNum;

    //default constructor used by SugarRecord
    public HighScoreSnake() {
        this.name = "default name";
        this.score = Constants.DEFAULT_HIGH_SCORE;
        this.HighScoreTypeNum = Constants.LEVEL_1;
    }

    public HighScoreSnake(String name, int score, int HighScoreTypeNum) {
        this.name = name;
        this.score = score;
        this.HighScoreTypeNum = HighScoreTypeNum;
    }

    public String getName() {
        return name;
    }

    public int getScore() {
        return score;
    }

    public int getHighScoreType() {
        return HighScoreTypeNum;
    }

    // Comparing method for sorting high scores
    @Override
    public int compareTo(HighScoreSnake s) {
		return this.getScore() < s.getScore() ? -1 : this.getScore() > s.getScore() ? 1 : 0;
	}

    /**
     * Translates the type of high score
     * to a the title of the high score shown to user
     */
    public static String typeToTitle(int type) {
        String end = ".";
        switch (type) {
            case Constants.LEVEL_1:
                end = "Level 1" + end;
                break;
            case Constants.LEVEL_2:
                end = "Level 2" + end;
                break;
            case Constants.LEVEL_3:
                end = "Level 3" + end;
                break;
            case Constants.LEVEL_4:
                end = "Level 4" + end;
                break;
            case Constants.LEVEL_5:
                end = "Level 5" + end;
                break;
            case Constants.LEVEL_6:
                end = "Level 6" + end;
                break;
            case Constants.LEVEL_7:
                end = "Level 7" + end;
                break;
            case Constants.LEVEL_8:
                end = "Level 8" + end;
                break;
            case Constants.LEVEL_9:
                end = "Level 9" + end;
                break;
            case Constants.LEVEL_10:
                end = "Level 10" + end;
                break;
            case Constants.WITH_WALLS_SLOW:
                end = "With Walls Slow speed" + end;
                break;
            case Constants.WITH_WALLS_NORMAL:
                end = "With Walls Normal speed" + end;
                break;
            case Constants.WITH_WALLS_FAST:
                end = "With Walls Fast speed" + end;
                break;
            case Constants.WITH_WALLS_EXTREME:
                end = "With Walls Extreme speed" + end;
                break;
            case Constants.WITHOUT_WALLS_SLOW:
                end = "Without Walls Slow speed" + end;
                break;
            case Constants.WITHOUT_WALLS_NORMAL:
                end = "Without Walls Normal speed" + end;
                break;
            case Constants.WITHOUT_WALLS_FAST:
                end = "Without Walls Fast speed" + end;
                break;
            case Constants.WITHOUT_WALLS_EXTREME:
                end = "Without Walls Extreme speed" + end;
                break;
        }
        return "High Score " + end;
    }

    /**
     * Get high score type
     * based on game mode, if there are surround walls,
     * what speed game is being played and game level
     */
    public static int HighScoreType(boolean freeStyle, boolean surroundWalls, int speed, int level) {
        if (!freeStyle)
			switch (level) {
			case 1:
				return Constants.LEVEL_1;
			case 2:
				return Constants.LEVEL_2;
			case 3:
				return Constants.LEVEL_3;
			case 4:
				return Constants.LEVEL_4;
			case 5:
				return Constants.LEVEL_5;
			case 6:
				return Constants.LEVEL_6;
			case 7:
				return Constants.LEVEL_7;
			case 8:
				return Constants.LEVEL_8;
			case 9:
				return Constants.LEVEL_9;
			case 10:
				return Constants.LEVEL_10;
			}
        if (surroundWalls)
			switch (speed) {
			case Constants.SLOW_SPEED:
				return Constants.WITH_WALLS_SLOW;
			case Constants.NORMAL_SPEED:
				return Constants.WITH_WALLS_NORMAL;
			case Constants.FAST_SPEED:
				return Constants.WITH_WALLS_FAST;
			case Constants.EXTREME_SPEED:
				return Constants.WITH_WALLS_EXTREME;
			}
        switch (speed) {
            case Constants.SLOW_SPEED:
                return Constants.WITHOUT_WALLS_SLOW;
            case Constants.NORMAL_SPEED:
                return Constants.WITHOUT_WALLS_NORMAL;
            case Constants.FAST_SPEED:
                return Constants.WITHOUT_WALLS_FAST;
            case Constants.EXTREME_SPEED:
                return Constants.WITHOUT_WALLS_EXTREME;
        }
        return Constants.LEVEL_1;
    }
}
