package yearlyproject.snake;

/**
 * Created by Akiva
 * Game Class.
 * All the logic of the snake game is here.
 */

import java.util.ArrayList;
import java.util.Random;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RectShape;
import android.view.View;
import android.widget.TextView;

public class Game extends View {
    //set in setup
    private boolean setupComplete = false;
    private int pxSquare, squaresWidth, squaresHeight, sqBorder = 0, padding = 0;
    private ArrayList<Block> walls;
    public Snake snake;
    private Food food;
    public int score, controls;
    // set in Game creation
    private Random random;
    private TextView scoreView;
    private GameScreen mActivity;
    public boolean gameOver = false, alreadyMovedThisTurn = false;
    private int frameRate, gameLevel, playMode, surroundWalls;
    private boolean darkTheme, classicMode;

    public Game(Context context, GameScreen activity, TextView scoreView, boolean darkTheme, boolean classicMode, int controls, int speed, int playMode, int gameLevel, int surroundWalls) {
        super(context);
        //setting up game fields for for use during snake game
        mActivity = activity;
        random = new Random();
        this.scoreView = scoreView;
        this.darkTheme = darkTheme;
        this.classicMode = classicMode;
        this.controls = controls;
        this.frameRate = Constants.FRAME_RATE_MULT * (speed + Constants.FRAME_RATE_ADD);
        this.playMode = playMode;
        this.gameLevel = gameLevel;
        this.surroundWalls = surroundWalls;
    }

    // If User Scores
    private void score() {
        ++score;
        scoreView.setText(Integer.toString(this.score));
    }

    // Draw the game, responsible for game moving along
    @SuppressLint("DrawAllocation")
    protected void onDraw(Canvas canvas) {
        //check if setup done yet. if not do it now
        if (!setupComplete) {
            setup();
            //draw again now that setup is done
            this.invalidate();
            return;
        }
        //Draw Walls
        for (Block block : walls) {
            block.draw(canvas);
        }
        //Move & Draw Snake
        snake.draw(canvas);
        //Draw Food
        food.draw(canvas);

        //Invalidate View After Timer, Using New Thread to prevent Blocking UI Thread
        //If Snake is Stopped, Wait and then call game over
        final View parent = this;
        if (!snake.stopped) {
            new Thread(new Runnable() {
                public void run() {
                    parent.postDelayed(new Runnable() {
                        public void run() {
                            parent.invalidate();
                        }
                    }, Constants.FRAME_RATE_BASE_GAME_RUNNING / frameRate);
                }
            }).start();
        } else if (gameOver) {
            new Thread(new Runnable() {
                public void run() {
                    parent.postDelayed(new Runnable() {
                        public void run() {
                            mActivity.gameOver();
                        }
                    }, Constants.FRAME_RATE_BASE_GAME_STOPPED);
                }
            }).start();
        }
    }

    // Setup the game
    public void setup() {
        //Reset Score
        score = Constants.SCORE_SETUP;
        this.score();
        gameOver = false;


        calculateGameBoard();
        setupWalls();
        finishSetupDetails();
    }

    public void calculateGameBoard(){
        //Calculate Width of View in Inches
        int pxWidth = getWidth();
        int pxHeight = getHeight();
        int dpi = getResources().getDisplayMetrics().densityDpi;
        float inWidth = ((float) pxWidth) / dpi;
        float inHeight = ((float) pxHeight) / dpi;

        //Calculate Number of Squares Based on View Size (Minimum 15 x 15)
        squaresWidth = (int) (inWidth * Constants.INWIDTH_MULT);
        squaresHeight = (int) (inHeight * Constants.INHEIGHT_MULT);
        if (squaresHeight < Constants.MIN_SQUARE_NUM)
			squaresHeight = Constants.MIN_SQUARE_NUM;
        if (squaresWidth < Constants.MIN_SQUARE_NUM)
			squaresWidth = Constants.MIN_SQUARE_NUM;
        //Calculate Size of Squares
        int pxSquareWidth = pxWidth / squaresWidth;
        int pxSquareHeight = pxHeight / squaresHeight;
        pxSquare = pxSquareWidth > pxSquareHeight ? pxSquareHeight : pxSquareWidth;

        //Calculate Padding Around & Between Squares
        padding = (pxWidth - squaresWidth * pxSquare) / Constants.PADDING_DEV;
        if (classicMode)
			sqBorder = pxSquare / Constants.CLASSIC_MODE_DEV;
    }

    public void setupWalls() {
        //Build List of Wall Objects
        walls = new ArrayList<>();
        // if we are playing free style with no walls, dont setup walls the walls
        if (surroundWalls != Constants.NO_SURROUND_WALLS || playMode != Constants.FREE_STYLE_MODE) {
            for (int j = 0; j < squaresWidth; ++j) {
                walls.add(new Block(j, Constants.GRAPH_START_NUM, Constants.BLACK_COLOR_WALL));  //Top Walls
                walls.add(new Block(j, squaresHeight - 1, Constants.BLACK_COLOR_WALL));  //Bottom Walls
            }
            for (int j = 1; j < (squaresHeight - 1); ++j) { //Left Walls
                walls.add(new Block(Constants.GRAPH_START_NUM, j, Constants.BLACK_COLOR_WALL));  //Left Walls
                walls.add(new Block(squaresWidth - 1, j, Constants.BLACK_COLOR_WALL)); //Right Walls
            }
        }
    }

    public void finishSetupDetails(){
        //Create Snake
        int direction = random.nextInt(Constants.NUM_OF_DIRECTION);
        snake = new Snake(direction);

        //if we are playing with levels, activate use the proper level
        if (playMode == Constants.LEVELS_MODE)
			activateLevel(gameLevel);

        //Create Food
        food = new Food(snake, walls);
        setupComplete = true;
    }




    //removes all walls from game
    public void removeWalls() {
        walls = new ArrayList<>();
    }

    //activates the proper level if we are on levels mode
    public void activateLevel(int num) {
        switch (num) {
            case 1:
                level1();
                break;
            case 2:
                level2();
                break;
            case 3:
                level3();
                break;
            case 4:
                level4();
                break;
            case 5:
                level5();
                break;
            case 6:
                level6();
                break;
            case 7:
                level7();
                break;
            case 8:
                level8();
                break;
            case 9:
                level9();
                break;
            default:
                level10();
                break;
        }
    }

    public void level1() {
        //speed ror this level
        this.frameRate = Constants.FRAME_RATE_MULT * (Constants.SLOW_SPEED + Constants.FRAME_RATE_ADD);
        //added walls specifically for this level
        for (int j = squaresHeight / Constants.DIV_WALLS_QUARTER; j < ((squaresHeight * Constants.MUL_WALLS_THREE_QUARTER) / Constants.DIV_WALLS_QUARTER); ++j) {
            this.walls.add(new Block(squaresWidth / Constants.DIV_WALLS_QUARTER, j, Constants.BLACK_COLOR_WALL));
            this.walls.add(new Block(((squaresWidth * Constants.MUL_WALLS_THREE_QUARTER) / Constants.DIV_WALLS_QUARTER), j, Constants.BLACK_COLOR_WALL));
        }
        //start direction for this level (new walls make it necessary too choose a start direction)
        this.snake = new Snake(Constants.DIRECTION_UP);
    }

    public void level2() {
        //speed ror this level
        this.frameRate = Constants.FRAME_RATE_MULT * (Constants.NORMAL_SPEED + Constants.FRAME_RATE_ADD);
        //added walls specifically for this level
        for (int j = squaresWidth / Constants.DIV_WALLS_QUARTER; j < ((squaresWidth * Constants.MUL_WALLS_THREE_QUARTER) / Constants.DIV_WALLS_QUARTER); ++j) {
            this.walls.add(new Block(j, squaresHeight / Constants.DIV_WALLS_QUARTER, Constants.BLACK_COLOR_WALL));
            this.walls.add(new Block(j, ((squaresHeight * Constants.MUL_WALLS_THREE_QUARTER) / Constants.DIV_WALLS_QUARTER), Constants.BLACK_COLOR_WALL));
        }
        //start direction for this level (new walls make it necessary too choose a start direction)
        this.snake = new Snake(Constants.DIRECTION_LEFT);
    }

    public void level3() {
        //speed ror this level
        this.frameRate = Constants.FRAME_RATE_MULT * (Constants.FAST_SPEED + Constants.FRAME_RATE_ADD);
        //added walls specifically for this level
        this.walls.add(new Block(squaresWidth / Constants.DIV_WALLS_QUARTER, squaresHeight / Constants.DIV_WALLS_QUARTER, Constants.BLACK_COLOR_WALL));  //Left Up Wall
        this.walls.add(new Block(((squaresWidth * Constants.MUL_WALLS_THREE_QUARTER) / Constants.DIV_WALLS_QUARTER), squaresHeight / Constants.DIV_WALLS_QUARTER, Constants.BLACK_COLOR_WALL)); //Right Up Wall
        this.walls.add(new Block(squaresWidth / Constants.DIV_WALLS_QUARTER, ((squaresHeight * Constants.MUL_WALLS_THREE_QUARTER) / Constants.DIV_WALLS_QUARTER), Constants.BLACK_COLOR_WALL));  //Left Down Wall
        this.walls.add(new Block(((squaresWidth * Constants.MUL_WALLS_THREE_QUARTER) / Constants.DIV_WALLS_QUARTER), ((squaresHeight * Constants.MUL_WALLS_THREE_QUARTER) / Constants.DIV_WALLS_QUARTER), Constants.BLACK_COLOR_WALL)); //Right Down Wall
    }

    public void level4() {
        //speed ror this level
        this.frameRate = Constants.FRAME_RATE_MULT * (Constants.SLOW_SPEED + Constants.FRAME_RATE_ADD);
        //remove surround walls for this level
        removeWalls();
        //added walls specifically for this level
        for (int j = 0; j < squaresWidth; ++j) {
            walls.add(new Block(j, Constants.GRAPH_START_NUM, Constants.BLACK_COLOR_WALL));  //Top Walls
            walls.add(new Block(j, squaresHeight - 1, Constants.BLACK_COLOR_WALL));  //Bottom Walls
        }
    }

    public void level5() {
        //speed ror this level
        this.frameRate = Constants.FRAME_RATE_MULT * (Constants.SLOW_SPEED + Constants.FRAME_RATE_ADD);
        //remove surround walls for this level
        removeWalls();
        //added walls specifically for this level
        for (int j = 0; j < (squaresHeight - 1); ++j) { //Left Walls
            walls.add(new Block(Constants.GRAPH_START_NUM, j, Constants.BLACK_COLOR_WALL));  //Left Walls
            walls.add(new Block(squaresWidth - 1, j, Constants.BLACK_COLOR_WALL)); //Right Walls
        }
    }

    public void level6() {
        //speed ror this level
        this.frameRate = Constants.FRAME_RATE_MULT * (Constants.NORMAL_SPEED + Constants.FRAME_RATE_ADD);
        //remove surround walls for this level
        removeWalls();
        //added walls specifically for this level
        for (int j = 0; j < squaresWidth; ++j) {
            walls.add(new Block(j, Constants.GRAPH_START_NUM, Constants.BLACK_COLOR_WALL));  //Top Walls
            walls.add(new Block(j, squaresHeight - 1, Constants.BLACK_COLOR_WALL));  //Bottom Walls
        }
    }

    public void level7() {
        //speed ror this level
        this.frameRate = Constants.FRAME_RATE_MULT * (Constants.NORMAL_SPEED + Constants.FRAME_RATE_ADD);
        //remove surround walls for this level
        removeWalls();
        //added walls specifically for this level
        for (int j = 0; j < (squaresHeight - 1); ++j) { //Left Walls
            walls.add(new Block(Constants.GRAPH_START_NUM, j, Constants.BLACK_COLOR_WALL));  //Left Walls
            walls.add(new Block(squaresWidth - 1, j, Constants.BLACK_COLOR_WALL)); //Right Walls
        }
    }

    public void level8() {
        //speed ror this level
        this.frameRate = Constants.FRAME_RATE_MULT * (Constants.NORMAL_SPEED + Constants.FRAME_RATE_ADD);
        //remove surround walls for this level
        removeWalls();
        //added walls specifically for this level
        for (int j = 0; j < squaresWidth; ++j) {
            walls.add(new Block(j, Constants.GRAPH_START_NUM, Constants.BLACK_COLOR_WALL));  //Top Walls
            walls.add(new Block(j, squaresHeight - 1, Constants.BLACK_COLOR_WALL));  //Bottom Walls
        }
        for (int j = 0; j < squaresWidth; j = j + 2) {
            this.walls.add(new Block(j, squaresHeight / Constants.DIV_WALLS_QUARTER, Constants.BLACK_COLOR_WALL));
            this.walls.add(new Block(j, ((squaresHeight * Constants.MUL_WALLS_THREE_QUARTER) / Constants.DIV_WALLS_QUARTER), Constants.BLACK_COLOR_WALL));
        }
        //start direction for this level (new walls make it necessary too choose a start direction)
        this.snake = new Snake(Constants.DIRECTION_LEFT);
    }

    public void level9() {
        //speed ror this level
        this.frameRate = Constants.FRAME_RATE_MULT * (Constants.SLOW_SPEED + Constants.FRAME_RATE_ADD);
        //remove surround walls for this level
        removeWalls();
        //added walls specifically for this level
        for (int j = 0; j < (squaresHeight - 1); ++j) {
            walls.add(new Block(Constants.GRAPH_START_NUM, j, Constants.BLACK_COLOR_WALL));  //Left Walls
            walls.add(new Block(squaresWidth - 1, j, Constants.BLACK_COLOR_WALL)); //Right Walls
        }
        for (int j = 0; j < (squaresHeight - 1); j = j + 2) { //Left Walls
            this.walls.add(new Block(squaresWidth / Constants.DIV_WALLS_QUARTER, j, Constants.BLACK_COLOR_WALL));
            this.walls.add(new Block(((squaresWidth * Constants.MUL_WALLS_THREE_QUARTER) / Constants.DIV_WALLS_QUARTER), j, Constants.BLACK_COLOR_WALL));
        }
        //start direction for this level (new walls make it necessary too choose a start direction)
        this.snake = new Snake(Constants.DIRECTION_UP);
    }

    public void level10() {
        //speed ror this level
        this.frameRate = Constants.FRAME_RATE_MULT * (Constants.SLOW_SPEED + Constants.FRAME_RATE_ADD);
        //added walls specifically for this level
        for (int j = squaresHeight / Constants.DIV_WALLS_QUARTER; j < ((squaresHeight * Constants.MUL_WALLS_THREE_QUARTER) / Constants.DIV_WALLS_QUARTER); ++j)
			this.walls.add(new Block(squaresWidth / Constants.DIV_WALLS_QUARTER, j, Constants.BLACK_COLOR_WALL));
        for (int j = squaresWidth / Constants.DIV_WALLS_QUARTER; j < ((squaresWidth * Constants.MUL_WALLS_THREE_QUARTER) / Constants.DIV_WALLS_QUARTER); ++j) {
            this.walls.add(new Block(j, squaresHeight / Constants.DIV_WALLS_QUARTER, Constants.BLACK_COLOR_WALL));
            this.walls.add(new Block(j, ((squaresHeight * Constants.MUL_WALLS_THREE_QUARTER) / Constants.DIV_WALLS_QUARTER), Constants.BLACK_COLOR_WALL));
        }
        //start direction for this level (new walls make it necessary too choose a start direction)
        this.snake = new Snake(Constants.DIRECTION_RIGHT);
    }


    /**
     * Snake Object contains a list of blocks,
     * and knows if it is moving and in which direction it is moving
     **/
    public class Snake {
        public ArrayList<Block> blocks;
        public int direction, length;
        public boolean stopped = false;

        // Create Snake with 3 Blocks
        public Snake(int directionGiven) {

            // Create Leading Block
            blocks = new ArrayList<>();
            blocks.add(new Block(squaresWidth / Constants.DIV_TO_HALF_SCREEN, squaresHeight / Constants.DIV_TO_HALF_SCREEN, Constants.RED_COLOR_HEAD));
            length = Constants.SNAKE_LENGTH_START;

            // Take Random Initial Direction and Add 2 Remaining Blocks
            direction = directionGiven;
            switch (direction) {
                case Constants.DIRECTION_RIGHT: //Going Right
                    blocks.add(new Block(squaresWidth / Constants.DIV_TO_HALF_SCREEN - Constants.DIRECTION_REMOVED_ONE, squaresHeight / Constants.DIV_TO_HALF_SCREEN, Constants.BLUE_COLOR_SNAKE));
                    blocks.add(new Block(squaresWidth / Constants.DIV_TO_HALF_SCREEN - Constants.DIRECTION_REMOVED_TWO, squaresHeight / Constants.DIV_TO_HALF_SCREEN, Constants.BLUE_COLOR_SNAKE));
                    break;
                case Constants.DIRECTION_DOWN: //Going Down
                    blocks.add(new Block(squaresWidth / Constants.DIV_TO_HALF_SCREEN, squaresHeight / Constants.DIV_TO_HALF_SCREEN - Constants.DIRECTION_REMOVED_ONE, Constants.BLUE_COLOR_SNAKE));
                    blocks.add(new Block(squaresWidth / Constants.DIV_TO_HALF_SCREEN, squaresHeight / Constants.DIV_TO_HALF_SCREEN - Constants.DIRECTION_REMOVED_TWO, Constants.BLUE_COLOR_SNAKE));
                    break;
                case Constants.DIRECTION_LEFT: //Going Left
                    blocks.add(new Block(squaresWidth / Constants.DIV_TO_HALF_SCREEN + Constants.DIRECTION_REMOVED_ONE, squaresHeight / Constants.DIV_TO_HALF_SCREEN, Constants.BLUE_COLOR_SNAKE));
                    blocks.add(new Block(squaresWidth / Constants.DIV_TO_HALF_SCREEN + Constants.DIRECTION_REMOVED_TWO, squaresHeight / Constants.DIV_TO_HALF_SCREEN, Constants.BLUE_COLOR_SNAKE));
                    break;
                case Constants.DIRECTION_UP: //Going Up
                    blocks.add(new Block(squaresWidth / Constants.DIV_TO_HALF_SCREEN, squaresHeight / Constants.DIV_TO_HALF_SCREEN + Constants.DIRECTION_REMOVED_ONE, Constants.BLUE_COLOR_SNAKE));
                    blocks.add(new Block(squaresWidth / Constants.DIV_TO_HALF_SCREEN, squaresHeight / Constants.DIV_TO_HALF_SCREEN + Constants.DIRECTION_REMOVED_TWO, Constants.BLUE_COLOR_SNAKE));
                    break;
            }
        }

        // Move & Draw Snake
        public void draw(Canvas canvas) {
            if (!stopped) {
                move();
            }
            for (Block block : blocks) {
                block.draw(canvas);
            }
        }

        // Turn One Direction Left from Current Orientation (Snake Oriented)
        // If Not Going Left or Right, Go Left (BOARD Oriented)

        public void hasMoved(int beforeDirection) {
            if (beforeDirection != this.direction)
				alreadyMovedThisTurn = true;
        }

        public void turnLeft() {
            if (alreadyMovedThisTurn)
                return;
            int beforeDirection = this.direction;
            if (controls == Constants.TWO_BUTTONS_CONTROLS_SNAKE_POV) {
                this.direction = this.direction + Constants.TURN_LEFT;
                if (this.direction < Constants.DIRECTION_RIGHT)
                    this.direction = Constants.DIRECTION_UP;
            } else if (this.direction != Constants.DIRECTION_RIGHT && this.direction != Constants.DIRECTION_LEFT)
                this.direction = Constants.DIRECTION_LEFT;
            hasMoved(beforeDirection);
        }

        // Turn One Direction Right from Current Orientation (Snake Oriented)
        // If Not Going Left or Right, Go Right (BOARD Oriented)
        public void turnRight() {
            if (alreadyMovedThisTurn)
                return;
            int beforeDirection = this.direction;
            if (controls == Constants.TWO_BUTTONS_CONTROLS_SNAKE_POV) {
                this.direction = this.direction + Constants.TURN_RIGHT;
                if (this.direction > Constants.DIRECTION_UP)
                    this.direction = Constants.DIRECTION_RIGHT;
            } else if (this.direction != Constants.DIRECTION_RIGHT && this.direction != Constants.DIRECTION_LEFT)
                this.direction = Constants.DIRECTION_RIGHT;
            hasMoved(beforeDirection);
        }

        // If Not Going Down or Up, Go Down (BOARD Oriented Only)
        public void turnDown() {
            if (alreadyMovedThisTurn)
                return;
            int beforeDirection = this.direction;
            if (this.direction != Constants.DIRECTION_DOWN && this.direction != Constants.DIRECTION_UP)
                this.direction = Constants.DIRECTION_DOWN;
            hasMoved(beforeDirection);
        }

        // If Not Going Down or Up, Go Up (BOARD Oriented Only)
        public void turnUp() {
            if (alreadyMovedThisTurn)
                return;
            int beforeDirection = this.direction;
            if (this.direction != Constants.DIRECTION_DOWN && this.direction != Constants.DIRECTION_UP)
                this.direction = Constants.DIRECTION_UP;
            hasMoved(beforeDirection);
        }

        // Move Snake 1 Space in Current Direction
        public void move() {
            // Grab Current Front Block
            Block frontBlock = blocks.get(0);

            // Create New Block at Front of Snake
            Block newBlock = newBlockLocation(frontBlock);

            //set the rest of the blocks the color blue
            for (Block block : blocks)
				block.setType(Constants.BLUE_COLOR_SNAKE);

            //check if the game has ended (snake crashed into a wall or itself
            if (this.collides(newBlock) || newBlock.collides(walls)) {
                stopped = true;
                for (Block block : blocks)
					block.setType(Constants.RED_COLOR_HEAD);
                newBlock.setType(Constants.BLACK_COLOR_WALL);
                gameOver = true;
            } else {
                // Add New Block to the Front
                blocks.add(0, newBlock);

                if (!this.collides(food))
					blocks.remove(length);
				else {
					food.move(this, walls);
					++length;
					score();
				}
            }
            alreadyMovedThisTurn = false;
        }

        //location of the new block in front of snake
        public Block newBlockLocation(Block frontBlock) {
            Block newBlock;
            switch (direction) {
                case Constants.DIRECTION_RIGHT: //Going Right
                    if ((frontBlock.x + Constants.DIRECTION_REMOVED_ONE) != squaresWidth)
						newBlock = new Block(frontBlock.x + Constants.DIRECTION_REMOVED_ONE, frontBlock.y,
								Constants.RED_COLOR_HEAD);
					else
						newBlock = new Block(0, frontBlock.y, Constants.RED_COLOR_HEAD);
                    break;
                case Constants.DIRECTION_DOWN: //Going Down
                    if ((frontBlock.y + Constants.DIRECTION_REMOVED_ONE) != squaresHeight)
						newBlock = new Block(frontBlock.x, frontBlock.y + Constants.DIRECTION_REMOVED_ONE,
								Constants.RED_COLOR_HEAD);
					else
						newBlock = new Block(frontBlock.x, 0, Constants.RED_COLOR_HEAD);
                    break;
                case Constants.DIRECTION_LEFT: //Going Left
                    if ((frontBlock.x) != 0)
						newBlock = new Block(frontBlock.x - Constants.DIRECTION_REMOVED_ONE, frontBlock.y,
								Constants.RED_COLOR_HEAD);
					else
						newBlock = new Block(squaresWidth - Constants.DIRECTION_REMOVED_ONE, frontBlock.y,
								Constants.RED_COLOR_HEAD);
                    break;
                default:  //Going Up
                    if ((frontBlock.y) != 0)
						newBlock = new Block(frontBlock.x, frontBlock.y - Constants.DIRECTION_REMOVED_ONE,
								Constants.RED_COLOR_HEAD);
					else
						newBlock = new Block(frontBlock.x, squaresHeight - Constants.DIRECTION_REMOVED_ONE,
								Constants.RED_COLOR_HEAD);
                    break;
            }
            return newBlock;
        }

        // Check for Collisions with a Block
        public boolean collides(Block b) {
            for (Block oneBlock : this.blocks)
                if (b.collides(oneBlock))
					return true;
            return false;
        }
    }

    /**
     * Blocks are what everything in the game is made out of
     **/
    public class Block {
        public int x = Constants.GRAPH_START_NUM, y = Constants.GRAPH_START_NUM;
        ShapeDrawable shape;

        public Block() {
        }

        public Block(int x, int y, int type) {
            this.x = x;
            this.y = y;
            shape = new ShapeDrawable(new RectShape());
            shape.setBounds(padding + x * pxSquare + sqBorder, padding + y * pxSquare + sqBorder, padding + pxSquare * (x + 1) - sqBorder, padding + pxSquare * (y + 1) - sqBorder);
            this.setType(type);
        }

        public void draw(Canvas canvas) {
            shape.draw(canvas);
        }

        //check if a block collides with this block
        public boolean collides(Block b) {
            return b.x == this.x && b.y == this.y;
        }

        //checks if a list of blocks collides with this block
        public boolean collides(ArrayList<Block> blocks) {
            for (Block block : blocks)
				if (this.collides(block))
					return true;
            return false;
        }

        //sets the color of the block
        public void setType(int type) {
            switch (type) {
                case Constants.BLACK_COLOR_WALL: //If Wall, Paint Black
				shape.getPaint().setColor(!darkTheme ? Color.BLACK : Color.parseColor("#fff3f3f3"));
                    break;
                case Constants.BLUE_COLOR_SNAKE: //If Snake, Paint Blue
                    shape.getPaint().setColor(Color.parseColor("#ff33b5e5"));
                    break;
                case Constants.GREY_COLOR_FOOD: //If Food, Paint Grey
                    shape.getPaint().setColor(Color.GRAY);
                    break;
                case Constants.RED_COLOR_HEAD: //If Head of snake or Collision, Paint Red
                    shape.getPaint().setColor(Color.RED);
                    break;
            }
        }

    }

    /**
     * If the snake meets food,
     * snake length grows by one,
     * the score goes up by one
     * and the food moves to another unoccupied location.
     **/
    class Food extends Block {

        public Food(Snake snake, ArrayList<Block> blocks) {
            shape = new ShapeDrawable(new RectShape());
            this.setType(Constants.GREY_COLOR_FOOD);
            this.move(snake, blocks);
        }

        //moves the food to another unoccupied location.
        public void move(Snake s, ArrayList<Block> blocks) {
            while (true) {
                this.x = random.nextInt(squaresWidth - 3) + 1;
                this.y = random.nextInt(squaresHeight - 3) + 1;
                if (!s.collides(this) && !this.collides(blocks)) break;
            }
            shape.setBounds(padding + x * pxSquare + sqBorder, padding + y * pxSquare + sqBorder, padding + (x + 1) * pxSquare - sqBorder, padding + (y + 1) * pxSquare - sqBorder);
        }

    }
}
