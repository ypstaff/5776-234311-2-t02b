package utils;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import utils.iterators.BlockIterator;
import utils.iterators.BlockIteratorFactory;
import utils.iterators.NestedBlockIterator;
import utils.iterators.SmartBlockIterator;

import com.google.inject.Binder;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Module;

public class NestedBlockIteratorTest {
	
	private NestedBlockIterator<Integer> $;
	private MockBlockIteratorFactory mockFactory;
	
	@Before
	public void setup() {
		$ = new NestedBlockIterator<Integer>(2);
		
		Injector injector = Guice.createInjector(new Module() {

			@Override
			public void configure(Binder binder) {
				mockFactory = new MockBlockIteratorFactory();
				binder.bind(BlockIteratorFactory.class).toInstance(mockFactory);
			}
			
		});
		
		injector.injectMembers($);
	}
	
	private static class MockBlockIteratorFactory implements BlockIteratorFactory {

		public List<SmartBlockIterator> iterators;
		
		public MockBlockIteratorFactory() {
			iterators = new ArrayList<SmartBlockIterator>();
		}
		
		@Override
		public BlockIterator build(int maxIndex) {
			SmartBlockIterator $ = Mockito.mock(SmartBlockIterator.class);
			iterators.add($);
			return $;
		}
		
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void throwsIllegalArgumentExceptionOnNegativeBlockSize() {
		$ = new NestedBlockIterator<Integer>(-5);
	}
	
	@Test (expected = NullPointerException.class)
	public void throwsNullPointerExceptionWhenAddingNullList() {
		$.addNestingLevel(null);
	}
	
	@Test
	public void doNothingWhenPoppingAndHaveNoIterators() {
		for(int i=0;i<10;i++)
			$.removeNestingLevel();
	}
	
	@Test
	public void returnCorrectNestingLevel() {
		assertThat($.getNestingLevel(),is(0));
	}
	
	@Test
	public void iterateOnTopIteratorOnly() {
		$.addNestingLevel(new ArrayList<Integer>(Arrays.asList(1,2,3,4,5)));
		$.addNestingLevel(new ArrayList<Integer>(Arrays.asList(1,2,3,4,5,6)));
		
		$.iterate();
		Mockito.verify(mockFactory.iterators.get(1),Mockito.times(1)).iterate();
		Mockito.verify(mockFactory.iterators.get(0),Mockito.times(0)).iterate();
	}
	
	@Test
	public void nestingLevelStatsAtZero() {
		assertThat($.getNestingLevel(),is(0));
	}
	
	@Test
	public void returnCorrectNestingLevelAfterAddingALevel() {
		$.addNestingLevel(new ArrayList<Integer>(Arrays.asList(1,2,3,4,5)));
		assertThat($.getNestingLevel(),is(1));
	}
	
	@Test
	public void returnCorrectNestingLevelAfterRemovingLevel() {
		$.addNestingLevel(new ArrayList<Integer>(Arrays.asList(1,2,3,4,5)));
		$.addNestingLevel(new ArrayList<Integer>(Arrays.asList(1,2,3)));
		$.removeNestingLevel();
		assertThat($.getNestingLevel(),is(1));
		$.removeNestingLevel();
		assertThat($.getNestingLevel(),is(0));
	}
	
	@Test
	public void increaseGranularityOfTopIteratorOnly() {
		$.addNestingLevel(new ArrayList<Integer>(Arrays.asList(1,2,3,4)));
		$.addNestingLevel(new ArrayList<Integer>(Arrays.asList(1,2,3,4)));
		$.increaseGranularity();
		
		Mockito.verify(mockFactory.iterators.get(1),Mockito.times(1)).increaseGranularity();
		Mockito.verify(mockFactory.iterators.get(0),Mockito.times(0)).increaseGranularity();
	}
	
	@Test
	public void decreaseGranularityOfTopiteratorOnly() {
		$.addNestingLevel(new ArrayList<Integer>(Arrays.asList(1,2,3,4)));
		$.addNestingLevel(new ArrayList<Integer>(Arrays.asList(1,2,3,4)));
		$.decreaseGranularity();
		Mockito.verify(mockFactory.iterators.get(1),Mockito.times(1)).decreaseGranularity();
		Mockito.verify(mockFactory.iterators.get(0),Mockito.times(0)).decreaseGranularity();
	}
	
	@Test
	public void getGranularityFromTopIterator() {
		$.addNestingLevel(new ArrayList<Integer>(Arrays.asList(1,2,3,4)));
		$.addNestingLevel(new ArrayList<Integer>(Arrays.asList(1,2,3,4)));
		$.getCurrentGranularity();
		Mockito.verify(mockFactory.iterators.get(1),Mockito.times(1)).getGranularity();
		Mockito.verify(mockFactory.iterators.get(0),Mockito.times(0)).getGranularity();
	}
	
	@Test
	public void DoesNotThrowIllegalArgumentExceptionWhenAddingListSmallerThanBlockSize() {
		$.addNestingLevel(new ArrayList<Integer>(Arrays.asList(1)));
	}
	
	
	
	
}
