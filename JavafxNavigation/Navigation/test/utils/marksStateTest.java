package utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javafx.scene.Node;
import javafx.scene.layout.Pane;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import utils.marking.MarksState;
import utils.marking.OverlayMarker;

public class marksStateTest {
	
	private MarksState $;
	private OverlayMarker marker;
	
	@Before
	public void setup() {
		marker = Mockito.mock(OverlayMarker.class);
		$ = new MarksState(marker);
	}
	
	@Test (expected = NullPointerException.class)
	public void throwNullPointerExceptionOnNullMarker() {
		new MarksState(null);
	}
	
	@Test
	public void markerMarksBordersAndOverlayOfNode() {
		Pane argPane = new Pane();
		$.mark(argPane);
		Mockito.verify(marker,Mockito.times(1)).markBorders(argPane);
		Mockito.verify(marker,Mockito.times(1)).createMark(argPane);
	}
	
	@Test
	public void markerMarksBorderAndOverlayOfEveryNodeInList() {
		List<Node> argPanes = new ArrayList<Node>(Arrays.asList(
				new Pane(),
				new Pane(),
				new Pane()));
		$.markAll(argPanes);
		for(int i=0;i<argPanes.size();i++) {
			Mockito.verify(marker,Mockito.times(1)).markBorders(argPanes.get(i));
			Mockito.verify(marker,Mockito.times(1)).createMark(argPanes.get(i));
		}
	}
	
	@Test
	public void markerMarksBordersOnlyOnMarkBorders() {
		Pane argPane = new Pane();
		$.markBorders(argPane);
		Mockito.verify(marker,Mockito.times(1)).markBorders(argPane);
		Mockito.verify(marker,Mockito.times(0)).createMark(argPane);
	}
	
	
}
