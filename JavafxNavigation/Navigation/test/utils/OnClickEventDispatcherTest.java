package utils;

import javafx.geometry.Bounds;
import javafx.scene.Node;
import junit.framework.TestCase;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

@RunWith(PowerMockRunner.class)
@PrepareForTest(Bounds.class)
public class OnClickEventDispatcherTest extends TestCase {
	
	private OnClickEventDispatcher $;
	private Node testNode;
	
	@Before
	public void setup() {
		testNode = Mockito.mock(Node.class);
		$ = new OnClickEventDispatcher();
	}
	
	
	@Test (expected = NullPointerException.class)
	public void throwNullPointerExceptionOnNullNode() {
		$.dispatchOnClickEvent(null);
	}
	
	//powermocking node causes a bug in the jvm, and there is no way mock
	//wit mockito since the methods i'm calling are final.
	//so this test is going to be ignored.
	@Test
	@Ignore
	public void dispatchingEventToNode() {
		
		Bounds mockBounds = PowerMockito.mock(Bounds.class);
		PowerMockito.when(mockBounds.getMinX()).thenReturn(0.0);
		PowerMockito.when(mockBounds.getMinY()).thenReturn(0.0);
		PowerMockito.when(testNode.getBoundsInLocal()).thenReturn(mockBounds);
		$.dispatchOnClickEvent(testNode);
		Mockito.verify(testNode,Mockito.times(1)).fireEvent(Mockito.any());
	}
}
