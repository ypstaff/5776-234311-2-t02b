package utils;

import static org.junit.Assert.assertEquals;

import java.util.function.Predicate;

import org.junit.Before;
import org.junit.Test;

import utils.iterators.BlockIterator;

public class BlockIteratorTest {
	
	private BlockIterator $;
	private int maxIndex;
	private int blockSize;
	private Granularity startingGranularity;
	private int startingIndex;
	
	@Before
	public void setup() {
		maxIndex=100;
		blockSize=5;
		startingGranularity=Granularity.BLOCK;
		startingIndex=1;
		$ = new BlockIterator(this.maxIndex, this.blockSize, this.startingGranularity,startingIndex);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void throwsIllegalArgumentExceptionOnNegativeMaxIndex() {
		$ = new BlockIterator(-1, -5, Granularity.BLOCK);
	}
	
	@Test
	public void DoesNotthrowIllegalArgumentExceptionOnMaxIndexLowerThanBlockSize() {
		$ = new BlockIterator(1,100,Granularity.BLOCK);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void throwsIllegalArgumentExceptionOnNegativeStartIndex() {
		$ = new BlockIterator(10,2,Granularity.BLOCK,-10);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void throwsIllegalArgumentExceptionOnNegativeBlockSize() {
		$ = new BlockIterator(10,-5,Granularity.ALL,0);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void thorwsIllegalArgumentExceptionOnMaxIndexLowerThanStartingIndex() {
		$ = new BlockIterator(10,-5,Granularity.ALL,20);
	}
	
	@Test
	public void startsAtSpecifiedGranularity() {
		assertEquals(startingGranularity,$.getGranularity());
	}
	
	@Test
	public void startsAtSpecifiedStartingIndex() {
		assertEquals(startingIndex,$.getStartIndex());
	}
	
	@Test
	public void advanceByBlockSizeWhileInBlockGranularity() {
		$.iterate();
		assertEquals(startingIndex+blockSize,$.getLower());
	}
	
	@Test
	public void advanceByOneWhileInSingleGranularity() {
		$.decreaseGranularity();
		$.iterate();
		assertEquals(startingIndex+1,$.getLower());
	}
	
	@Test
	public void upperBoundIsBlockSizeAheadWhenInBlockGranularity() {
		assertEquals(startingIndex+blockSize,$.getUpper());
		$.iterate();
		assertEquals(startingIndex+2*blockSize,$.getUpper());
	}
	
	@Test
	public void upperBoundIsOneIndexAheadWhenInSingleGranularity() {
		$.decreaseGranularity();
		assertEquals(startingIndex+1,$.getUpper());
		$.iterate();
		assertEquals(startingIndex+2,$.getUpper());
	}
	
	@Test
	public void increaseGranularityFromSingleToBlock() {
		$.decreaseGranularity();
		$.increaseGranularity();
		assertEquals(Granularity.BLOCK,$.getGranularity());
	}
	
	@Test
	public void increaseGranularityFromBlockToAll() {
		$.increaseGranularity();
		assertEquals(Granularity.ALL,$.getGranularity());
	}
	
	@Test
	public void GranularityDoesNotChangeWhenIncreasingInAllGranularity() {
		$.increaseGranularity();
		$.increaseGranularity();
		assertEquals(Granularity.ALL,$.getGranularity());
	}
	
	@Test
	public void GranularityDoesNotChangeWhenDecreasingInSingleGranularity() {
		$.decreaseGranularity();
		$.decreaseGranularity();
		assertEquals(Granularity.SINGLE,$.getGranularity());
	}
	
	@Test
	public void DecreaseGranularityFromBlockToSingle() {
		$.decreaseGranularity();
		assertEquals(Granularity.SINGLE,$.getGranularity());
	}
	
/*	@Test
	public void dimensionBoundsForCurrentIsFromLowerToUpper() {
		List<Integer> bounds = $.getDimensionBoundsForCurrent();
		assertEquals(2,bounds.size());
		assertEquals(bounds.get(0).intValue(), $.getLower());
	}
	
	@Test
	public void dimnesionBoundsForAllReturnsCorrectValuesInBlockGranularity() {
		List<Integer> expected = new ArrayList<Integer>();
		for(int i=startingIndex;i<maxIndex;i+=blockSize) {
			expected.add(i);
		}
		expected.add(maxIndex);
		List<Integer> bounds = $.getDimensionBoundsForAll();
		assertThat(bounds,is(expected));
	}
	
	@Test
	public void dimensionBoundsForAllReturnsCorrectValuesInSingleGranularty() {
		List<Integer> expected = new ArrayList<Integer>();
		int maxIterationIndex = Math.min($.getSelectedIndex()+blockSize, maxIndex);
		for(int i=$.getLower();i<maxIterationIndex;i++) {
			expected.add(i);
		}
		expected.add(maxIterationIndex);
		$.decreaseGranularity();
		List<Integer> bounds = $.getDimensionBoundsForAll();
		
		assertThat(bounds,is(expected));
	}
	
	@Test
	public void dimensionBoundsForAllReturnsEntireRangeOnAllGranularity() {
		List<Integer> expected = new ArrayList<Integer>(Arrays.asList(startingIndex,maxIndex-1));
		$.increaseGranularity();
		List<Integer> bounds = $.getDimensionBoundsForAll();
		
		assertThat(bounds,is(expected));
	}
	*/
	@Test
	public void returnPredicateWhichMatchesIndexesFromLowerInclusiveToUpperNonExclusive() {
		$.iterate();
		$.iterate();
		Predicate<Integer> resultPredicate = $.getPredicate();
		int lowerIndex = $.getLower();
		int upperIndex = $.getUpper();
		for(int i=startingIndex;i<maxIndex;i++) {
			assertEquals(resultPredicate.test(i),(i>=lowerIndex && upperIndex>i));
		}
	}
}
