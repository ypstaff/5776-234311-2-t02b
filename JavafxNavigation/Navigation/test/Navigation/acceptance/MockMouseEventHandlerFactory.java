package Navigation.acceptance;

import java.util.List;

import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;

import org.mockito.Mockito;

public class MockMouseEventHandlerFactory  {

	private List<EventHandler<MouseEvent>> handlers;
	
	public MockMouseEventHandlerFactory(List<EventHandler<MouseEvent>> targetList) {
		handlers = targetList;
	}
	
	@SuppressWarnings("unchecked")
	public EventHandler<MouseEvent> build() {
		handlers.add(Mockito.mock(EventHandler.class));
		return handlers.get(handlers.size()-1);
	}

}
