package Navigation.acceptance;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.function.Consumer;
import java.util.function.Supplier;

import javafx.event.EventHandler;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mockito;

import Navigation.IterableNodeGroup;
import Navigation.IterableNodeGroups;

public class IterableNodeGroupsAcceptanceTest {

	private static IterableNodeGroups root;
	private static List<EventHandler<MouseEvent>> handlers;
	private static MockMouseEventHandlerFactory factory;

	private static List<IterableNodeGroup> groups;
	private static HBox topRow;
	private static HBox buttomRow;
	
	private static TestApplicationMediator mediator;

	@BeforeClass
	public static void beforeAll() throws InterruptedException {
		Consumer<Void> beforeTestAction = new Consumer<Void>() {

			@Override
			public void accept(Void t) {
				root = mediator.getRoot();
			}
		};

		Supplier<Pane> paneSupplier = new Supplier<Pane>() {

			@Override
			public Pane get() {
				return new BorderPane();
			}	
		};

		Consumer<Pane> interfaceInitializer= new Consumer<Pane>() {

			@Override
			public void accept(Pane t) {
				BorderPane $ = (BorderPane)t;
				handlers =  new ArrayList<EventHandler<MouseEvent>>();
				factory =  new MockMouseEventHandlerFactory(handlers);
				groups = new ArrayList<IterableNodeGroup>();
				$.setTop(initTop());
			}

			private Pane initTop() {
				VBox $ = new VBox();
				topRow = new HBox();
				buttomRow = new HBox();

				for(int i=0;i<10;i++) {
					topRow.getChildren().add(createLabelChild("Top "+i));
				}
				for(int i=0;i<10;i++) {
					buttomRow.getChildren().add(createLabelChild("Buttom "+i));
				}

				groups.add(mediator.getRoot().addChildGroup(topRow.getChildren()));
				groups.add(mediator.getRoot().addChildGroup(buttomRow.getChildren(),4));
				return $;
			}

			private Label createLabelChild(String text) {
				Label $ = new Label(text);
				$.setOnMouseClicked(factory.build());
				return $;
			}
		};

		mediator = new TestApplicationMediator(
				beforeTestAction, paneSupplier, interfaceInitializer);
		mediator.launch();
	}

	private static void wrapAndExecute(Consumer<Void> testToRun) throws Throwable {
		mediator.wrapAndExecute(testToRun);
	}

	@AfterClass
	public static void afterAll() throws Throwable {
		mediator.close();
	}

	@Test
	public void iterateByOneWhenNoBlockSizeGiven() throws Throwable {
		wrapAndExecute(new Consumer<Void>() {
			@Override
			public void accept(Void t) {
				root.select();
				root.select();
				root.next();
				root.select();
				root.next();
				verifyOneClickOnMockHandlerAtIndex(0);
				verifyOneClickOnMockHandlerAtIndex(1);
				for(int i=2;i<handlers.size();i++)
					verifyNoClickOnMockHandlerAtIndex(i);
			}
		});

	}

	@Test
	public void neverGoOutOfBlockBounds() throws Throwable {
		wrapAndExecute(new Consumer<Void>() {

			@Override
			public void accept(Void t) {
				root.next();
				root.select();
				root.select();
				for(int i=0;i<20;i++) {
					root.select();
					root.next();
				}
				for(int i=14;i<handlers.size();i++) {
					verifyNoClickOnMockHandlerAtIndex(i);
				}
				for(int i=10;i<14;i++) {
					verifyMultipleClicksOnMockHandlerAtIndex(i);
				}
			}
		});
	}

	@Test
	public void arrivingAtFirstElementAfterIteratingFromLast() throws Throwable {
		wrapAndExecute(new Consumer<Void>() {
			@Override
			public void accept(Void arg0) {
				root.select();
				for(int i=0;i<9;i++) root.next();
				root.select();
				verifyOneClickOnMockHandlerAtIndex(9);
				root.next();
				root.select();
				verifyOneClickOnMockHandlerAtIndex(0);
			}
		});
	}

	@Test
	public void arrivingAtFirstBlockAfterIteratingFromLast() throws Throwable {
		wrapAndExecute(new Consumer<Void>() {

			@Override
			public void accept(Void t) {
				root.next();
				root.select();

				for(int i=0;i<2;i++) 
					root.next();
				root.select();
				root.select();
				verifyOneClickOnMockHandlerAtIndex(18);
				root.unselect();
				root.next();
				root.select();
				root.select();
				verifyOneClickOnMockHandlerAtIndex(10);
			}
		});
	}

	@Test
	public void alwaysEnterFirstElementInSelectedBlock() throws Throwable {
		wrapAndExecute(new Consumer<Void>() {

			@Override
			public void accept(Void t) {
				root.next();
				root.select();

				root.select();
				root.select();
				verifyOneClickOnMockHandlerAtIndex(10);
				root.unselect();
				root.next();
				root.select();
				root.select();
				verifyOneClickOnMockHandlerAtIndex(14);
				root.unselect();
				root.next();
				root.select();
				root.select();
				verifyOneClickOnMockHandlerAtIndex(18);
			}
		});
	}

	@Test
	public void enterFirstElementOnReturnToBlockAfterIteratingInBlock() throws Throwable {
		wrapAndExecute(new Consumer<Void>() {

			@Override
			public void accept(Void t) {
				root.next();
				root.select();

				root.select();
				root.next();
				root.next();
				root.unselect();
				for(int i=0;i<3;i++)
					root.next();
				verifyNoClickOnMockHandlerAtIndex(10);
				root.select();
				root.select();
				verifyOneClickOnMockHandlerAtIndex(10);
			}
		});
	}

	@Test
	public void ReenterToFirstElementOfGroup() throws Throwable {
		wrapAndExecute(new Consumer<Void>() {

			@Override
			public void accept(Void t) {
				root.select();

				for(int i=0;i<5;i++) {
					root.next();
				}
				root.unselect();
				root.select();
				root.select();
				verifyOneClickOnMockHandlerAtIndex(0);
				verifyNoClickOnMockHandlerAtIndex(5);

			}
		});
	}
	
	@Test
	public void jumpToCorrectSpecificGroup() throws Throwable {
		wrapAndExecute(new Consumer<Void>() {

			@Override
			public void accept(Void t) {
				root.selectSpecific(groups.get(1));
				
				root.select();
				root.select();
				root.select();
				verifyOneClickOnMockHandlerAtIndex(10);
				
				root.selectSpecific(groups.get(0));
				root.select();
				root.select();
				verifyOneClickOnMockHandlerAtIndex(0);
			}
		});
	}
	
	@Test
	public void jumpToFirstGroupIfArgumentGroupNotFound() throws Throwable {
		wrapAndExecute(new Consumer<Void>() {

			@Override
			public void accept(Void t) {
				root.next();
				root.selectSpecific(Mockito.mock(IterableNodeGroup.class));
				root.select();
				root.select();
				verifyOneClickOnMockHandlerAtIndex(0);
			}
		});
	}
	
	@Test
	public void skipDisabledGroupInIteration() throws Throwable {
		wrapAndExecute(new Consumer<Void>() {
			@Override
			public void accept(Void t) {
				root.setEnabledMode(groups.get(1), false);
				root.next();
				root.select();
				root.select();
				verifyOneClickOnMockHandlerAtIndex(0);
			}
		});
	}
	
	@Test
	public void skipDisabledNodeInIteration() throws Throwable {
		wrapAndExecute(new Consumer<Void>() {

			@Override
			public void accept(Void t) {
				root.setEnabledMode(topRow.getChildren().get(1), false);
				root.select();
				root.next();
				root.select();
				verifyNoClickOnMockHandlerAtIndex(1);
				verifyOneClickOnMockHandlerAtIndex(2);
			}
		});
	}
	
	@Test
	public void notSkippingNodesAfterTheyAreReenabled() throws Throwable {
		wrapAndExecute(new Consumer<Void>() {

			@Override
			public void accept(Void t) {
				root.setEnabledMode(topRow.getChildren().get(1), false);
				root.setEnabledMode(topRow.getChildren().get(1), true);
				root.select();
				root.next();
				root.select();
				verifyOneClickOnMockHandlerAtIndex(1);
			}
			
		});
	}
	
	@Test
	public void notSkippingGroupsAfterTheyAreReenabled() throws Throwable {
		wrapAndExecute(new Consumer<Void>() {

			@Override
			public void accept(Void t) {
				root.setEnabledMode(groups.get(1), false);
				root.setEnabledMode(groups.get(1), true);
				root.next();
				root.select();
				root.select();
				root.select();
				verifyOneClickOnMockHandlerAtIndex(10);
			}
		});
	}

	@Test
	public void sendEventOnEverySingleElementSelect() throws Throwable {
		wrapAndExecute(new Consumer<Void>() {
			
			@Override
			public void accept(Void t) {
				root.select();
				for(int i=0;i<50;i++) {
					root.select();
				}
				Mockito.verify(handlers.get(0),Mockito.times(50)).handle(Mockito.any());
			}
		});
	}
	
	@Test
	public void sendEventToClickedElementAndOnlyToIt() throws Throwable {
		wrapAndExecute(new Consumer<Void>() {

			@Override
			public void accept(Void t) {
				root.select();
				int randomIndex = new Random().nextInt(10);
				for(int i=0;i<randomIndex;i++) 
					root.next();
				
				for(int i=0;i<handlers.size();i++) {
					verifyNoClickOnMockHandlerAtIndex(i);
				}
				root.select();
				for(int i=0;i<handlers.size();i++) {
					if(i!=randomIndex)
						verifyNoClickOnMockHandlerAtIndex(i);
					else
						verifyOneClickOnMockHandlerAtIndex(i);
				}
			}
		});
	}


	private void verifyNoClickOnMockHandlerAtIndex(int index) {
		Mockito.verify(handlers.get(index),Mockito.never()).handle(Mockito.any());
	}

	private void verifyOneClickOnMockHandlerAtIndex(int index) {
		Mockito.verify(handlers.get(index),Mockito.times(1)).handle(Mockito.any());
	}

	private void verifyMultipleClicksOnMockHandlerAtIndex(int index) {
		Mockito.verify(handlers.get(index),Mockito.atLeastOnce()).handle(Mockito.any());
	}
	
	


}
