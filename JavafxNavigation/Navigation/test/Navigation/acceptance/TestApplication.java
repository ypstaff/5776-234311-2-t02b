package Navigation.acceptance;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.function.Consumer;
import java.util.function.Supplier;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import Navigation.IterableNodeGroups;

public class TestApplication extends Application {

	protected static Pane mainPane;
	protected static IterableNodeGroups root;
	protected static Scene mainScene;
	protected static Consumer<Void> testToRun;
	
	protected static BlockingQueue<Consumer<Void>> tests = new ArrayBlockingQueue<>(10);
	protected static BlockingQueue<Throwable> throwables = new ArrayBlockingQueue<>(10);
	protected static boolean moreTests = true;
	
	protected static Consumer<Void> beforeTestAction;
	protected static Supplier<Pane> paneSupplier;
	protected static Consumer<Pane> interfaceInitializer;
	
	@Override
	public void start(Stage mainStage) throws Exception {
		while(moreTests) {
			mainPane = paneSupplier.get();
			root = new IterableNodeGroups(mainPane);
			mainScene = new Scene(root.getParentPane());
			
			interfaceInitializer.accept(mainPane);
			
			mainStage.setScene(mainScene);
			mainStage.show();
			
			root.start();
			testToRun = tests.take();
			try {
				beforeTestAction.accept((Void)null);
				testToRun.accept((Void)null);
				throwables.put(new FakeException());
			} catch(Throwable ex) {
				throwables.put(ex);
				while(throwables.size()>0) {}
			}
		}
		
		mainStage.close();
	}
	
	
	
	

}
