package Navigation.acceptance;

import java.util.function.Consumer;
import java.util.function.Supplier;

import javafx.scene.layout.Pane;
import Navigation.IterableNodeGroups;

public class TestApplicationMediator {
	
	private Thread applicationThread;
	
	public TestApplicationMediator(Consumer<Void> beforeTestAction,
	Supplier<Pane> paneSupplier,
	Consumer<Pane> interfaceInitializer) {
		applicationThread = null;
		TestApplication.beforeTestAction = beforeTestAction;
		TestApplication.paneSupplier = paneSupplier;
		TestApplication.interfaceInitializer = interfaceInitializer;
	}
	
	public void launch() {
		if(applicationThread!=null) {
			return;
		}
		applicationThread = new Thread(()->{TestApplication.launch(
				TestApplication.class,(String[])null);});
		applicationThread.start();
	}
	
	public void wrapAndExecute(Consumer<Void> testToRun) throws Throwable {
		TestApplication.tests.put(testToRun);
		while(TestApplication.tests.size()>0) {}
		Throwable t = TestApplication.throwables.take();
		if(!(t instanceof FakeException))
			throw t;
	}
	
	public void close() throws Throwable {
		wrapAndExecute(new Consumer<Void>() {
			@Override
			public void accept(Void t) {
				TestApplication.moreTests=false;
			}
		});
	}
	
	public IterableNodeGroups getRoot() {
		return TestApplication.root;
	}
}
