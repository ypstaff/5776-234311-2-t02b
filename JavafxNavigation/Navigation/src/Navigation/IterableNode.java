package Navigation;

import javafx.scene.Node;

/**
 * defines an interface which every element in an IterableNodeGroup must implement.
 * @author Sergey
 *
 */
public interface IterableNode {
	public void select();
	public boolean unselect();
	public Node getNode();
}
