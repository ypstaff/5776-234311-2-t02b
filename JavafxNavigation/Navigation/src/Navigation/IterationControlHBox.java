package Navigation;



import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;

import Navigation.IterableNodeGroups.Action;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
/**
 * A pane with 3 buttons used to control a given IterableNodeGroups object.
 * the functionality of the buttons can be extended by setting their OnLongClickConsumers.
 * @author Sergey
 *
 */
public class IterationControlHBox extends HBox{
	
	public Button next;
	public Button select;
	public Button back;
	public Button options;
	
	private Map<Action,Consumer<Void>> onClickConsumers;
	private Map<Action,Consumer<Void>> onLongClickConsumers;
	
	
	
	private long mouseDownTimestamp;
	
	public IterationControlHBox(IterableNodeGroups root) {
		onClickConsumers = new HashMap<Action,Consumer<Void>>();
		onLongClickConsumers = new HashMap<Action, Consumer<Void>>();
		
		onClickConsumers.put(Action.SELECT,(v)->root.select());
		onClickConsumers.put(Action.NEXT,(v)->root.next());
		onClickConsumers.put(Action.BACK, (v)->root.unselect());
		onClickConsumers.put(Action.OPTIONS, getEmptyConsumer());
		
		onLongClickConsumers.put(Action.SELECT, getEmptyConsumer());
		onLongClickConsumers.put(Action.NEXT, getEmptyConsumer());
		onLongClickConsumers.put(Action.BACK, getEmptyConsumer());
		onLongClickConsumers.put(Action.OPTIONS,getEmptyConsumer());
		
		next=setupButton(Action.NEXT);
		select=setupButton(Action.SELECT);
		back=setupButton(Action.BACK);
		options = setupButton(Action.OPTIONS);
	}
	
	public void setOptionsOnClickHandler(EventHandler<MouseEvent> handler) {
		options.setOnMouseClicked(handler);
		options.setVisible(true);
	}
	
	private Consumer<Void> getEmptyConsumer() {
		return new Consumer<Void>() {

			@Override
			public void accept(Void t) {
				
			}
			
		};
	}
	
	private Button createButton(Action action, Consumer<Void> onClickConsumer,Consumer<Void> onLongClickConsumer) {
		Button $ = new Button(action.toString());
		
		$.setOnMousePressed(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent arg0) {
				mouseDownTimestamp = System.nanoTime();
			}
			
		});
		
		$.setOnMouseReleased(makeOnMouseReleasedListener(onClickConsumer,onLongClickConsumer));
		return $;
	}
	
	private Button setupButton(Action action) {
		Button $ = createButton(action, onClickConsumers.get(action), onLongClickConsumers.get(action));
		this.getChildren().add($);
		return $;
	}
	
	private EventHandler<MouseEvent> makeOnMouseReleasedListener(
			Consumer<Void> onClickConsumer,Consumer<Void> onLongClickConsumer) {
		return new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent arg0) {
				if((double)(System.nanoTime() - mouseDownTimestamp)/1000000000.0 > 0.7) {
					onLongClickConsumer.accept((Void)null);
				} else {
					onClickConsumer.accept((Void)null);
				}
			}
		};
	}


	public void setOnSelectLongClick(Consumer<Void> onSelectLongClick) {
		select.setOnMouseReleased(makeOnMouseReleasedListener(onClickConsumers.get(Action.SELECT),onSelectLongClick));
	}



	public void setOnNextLongClick(Consumer<Void> onNextLongClick) {
		next.setOnMouseReleased(makeOnMouseReleasedListener(onClickConsumers.get(Action.NEXT),onNextLongClick));
	}



	public void setOnBackLongClick(Consumer<Void> onBackLongClick) {
		back.setOnMouseReleased(makeOnMouseReleasedListener(onClickConsumers.get(Action.BACK),onBackLongClick));
	}
	
	public void setOnOptionsClick(Consumer<Void> onOptionsClick) {
		options.setOnMouseClicked(makeOnMouseReleasedListener(onOptionsClick, getEmptyConsumer()));
	}

}
