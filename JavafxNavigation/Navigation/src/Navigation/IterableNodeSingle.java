package Navigation;

import utils.OnClickEventDispatcher;
import utils.marking.MarksState;
import utils.marking.OverlayMarker;
import javafx.scene.Node;

/**
 * a container for a single node, which is used by IterableNodeGroup.
 * @author Sergey
 *
 */
public class IterableNodeSingle implements IterableNode,MarkingNode {

	private Node node;
	private MarksState marksState;
	private OnClickEventDispatcher eventDispatcher;
	
	public IterableNodeSingle(Node n) {
		this.node = n;
		eventDispatcher = new OnClickEventDispatcher();
	}
	
	@Override
	public void select() {
		eventDispatcher.dispatchOnClickEvent(node);
		
	}

	@Override
	public boolean unselect() {
		marksState.removeAllMarks();
		return false;
	}

	@Override
	public void setMarker(OverlayMarker m) {
		marksState = new MarksState(m);
		
	}
	
	public Node getNode() {
		return this.node;
	}

}
