package Navigation;

import java.util.List;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

/**
 * A container for an IterableNodeGroups object, for easier use and initialization.
 * @author Sergey
 *
 */
public class NavigableApplication extends Application implements EventHandler<KeyEvent>,RootedObject {

	private IterableNodeGroups root;
	private Scene s;
	
	@Override
	public void start(Stage arg0) throws Exception {
		//root.start();
		
	}
	
	private Scene initScene() {
		s = new Scene(root.getParentPane());
		s.setOnKeyPressed(this);

		
		return s;
	}
	
	public Scene init(Pane mainPane, boolean markAllGroupsBorders) {
		root =new IterableNodeGroups(mainPane,markAllGroupsBorders);
		return initScene();
	}

	public Scene init(Pane mainPane) {
		root =new IterableNodeGroups(mainPane);
		return initScene();
	}
	
	public Scene init(Pane mainPane, Color selectionOverlayColor,
			Color selectionBorderColor) {
		root = new IterableNodeGroups(mainPane,selectionOverlayColor,selectionBorderColor);
		return initScene();
	}
	
	public void next() {
		root.next();
	} 
	
	public void select() {
		root.select();
	}
	
	public void back() {
		root.unselect();
	}
	
	public void start() {
		root.start();
	}
	
	public IterableNodeGroup addIterableGroup(Node node) {
		return root.addChildGroup(node);
	}
	
	public IterableNodeGroup addIterableGroup(List<Node> groupNodes) {
		return root.addChildGroup(groupNodes);
	}
	
	public IterableNodeGroup addIterableGroup(List<Node> groupNodes, int blockSize) {
		return root.addChildGroup(groupNodes, blockSize);
	}
	
	public boolean selectSpecific(Node node) {
		return root.selectSpecific(node);
	}
	
	public boolean selectSpecific(IterableNodeGroup group) {
		return root.selectSpecific(group);
	}
	
	public boolean setEnabledMode(Node node, boolean mode) {
		return root.setEnabledMode(node, mode);
	}
	
	public boolean setEnabledMode(IterableNodeGroup group, boolean mode) {
		return root.setEnabledMode(group, mode);
	}
	
	public void clearGroups() {
		root.clearGroups();
	}

	@Override
	public void handle(KeyEvent e) {
		String keyValue = e.getCode().toString().toUpperCase();
		if(keyValue.equals("D")) {
			root.next();
		} else if(keyValue.equals("S")) {
			root.select();
		} else if(keyValue.equals("A")) {
			root.unselect();
		}
		
	}
	
	public IterationControlHBox getIterationControlHBox() {
		return new IterationControlHBox(root);
	}

	@Override
	public IterableNodeGroups getRoot() {
		return this.root;
	}
	
	
	
	

}
