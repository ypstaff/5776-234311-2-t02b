package Navigation;

import java.util.List;

import javafx.scene.Node;

/**
 * A Node which is in an IterableNodeGroup, and contains other IterableNodes.
 * @author Sergey
 *
 */
public interface IterableNodeCollection extends IterableNode {

	public void next();
	public List<Node> childrenAsNodes();
}
