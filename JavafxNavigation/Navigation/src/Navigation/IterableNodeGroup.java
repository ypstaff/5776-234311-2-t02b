package Navigation;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import com.google.inject.Inject;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Bounds;
import javafx.scene.Node;
import utils.Granularity;
import utils.NodesGroupLimits1D;
import utils.OnClickEventDispatcher;
import utils.iterators.GranularIterator;
import utils.iterators.SingleCellIterator;
import utils.iterators.SmartBlockIterator;
import utils.marking.MarksState;
import utils.marking.OverlayMarker;

/**
 * Used to iterate over a group of nodes, which can contain groups of nodes as well.
 * @author Sergey
 *
 */
public class IterableNodeGroup implements IterableNodeCollection {
	private List<IterableNode> children;
	private OnClickEventDispatcher eventDispatcher;
	@Inject private MarksState marksState;
	private OverlayMarker marker;
	private GranularIterator iterator;
	private HashSet<Integer> disabledIndexes;
	private boolean isChildSelected;
	private boolean isSelected;

	public IterableNodeGroup(List<Node> children,OverlayMarker marker) {
		this(children,marker,new OnClickEventDispatcher(), 
				new SingleCellIterator(0,children.size()));
	}

	public IterableNodeGroup(List<Node> children,OverlayMarker marker,
			OnClickEventDispatcher eventDispatcher,GranularIterator iterator) {
		this.children = new ArrayList<IterableNode>();
		for(Node n : children) {
			if(n instanceof IterableNode)
				this.children.add((IterableNode)n);
			else
				this.children.add(new IterableNodeSingle(n));
		}
		for(Node n : children)
			if (n instanceof MarkingNode)
				((MarkingNode) n).setMarker(marker);
		
		for(Node n : children) {
			n.boundsInLocalProperty().addListener(new ChangeListener<Bounds>() {
				
				@Override
				public void changed(ObservableValue<? extends Bounds> arg0,
						Bounds arg1, Bounds arg2) {
					refresh();
				}
			});
		}

		this.eventDispatcher=eventDispatcher;
		this.marker=marker;                   //need this to add children in the future.
		this.marksState=new MarksState(marker);
		this.iterator = iterator;
		disabledIndexes = new HashSet<Integer>();
	}

	public IterableNodeGroup(List<Node> children, OverlayMarker marker,
			int blockSize) {
		this(children,marker,new OnClickEventDispatcher(), 
				new SmartBlockIterator(children.size(), blockSize, Granularity.ALL,0));
	}

	public void addIterableNodeGroup(IterableNodeGroup g) {
		children.add(g);
		iterator.navigateToIndex(0);
		iterator.setMaxIndex(children.size());
		marksState.removeAllMarks();
		markAllMatchingBorders();
		markAllMatchingSelections();
	}

	/**
	 * Marks the parent pane of an iterable node group.
	 */
	public void selectAll() {
		marksState.markRegion(this.childrenAsNodes());
	}

	/**
	 * Removes the mark from the parent pane of the an iterable node group.
	 */
	public void unselectAll() {
		marksState.removeAllMarks();
	}

	/**
	 * Selects a single element in this pane and marks it (if the node is iterable by itself, we pass on the select call to it).
	 */
	@Override
	public void select() {
		isSelected = true;
		marksState.removeAllMarks();
		boolean toSendEvent = false;
		while(!canStopAtElement())
			iterator.iterate();
		if(iterator.getGranularity()==Granularity.SINGLE) {
			IterableNode current = currentNode();
			if(current instanceof IterableNodeCollection) {
				((IterableNodeCollection)current).select();
				isChildSelected=true;
				return;
			}
			toSendEvent = true;
		} else {
			iterator.decreaseGranularity();
			if(this.children.size()==iterator.getBlockSize()) {
				this.select();
				return;
			}
		}
		markAllMatchingSelections();
		markAllMatchingBorders();
		if(toSendEvent) eventDispatcher.dispatchOnClickEvent(currentNode().getNode());
	}

	/**
	 * Unmarks the child node if possible.
	 * @return
	 */
	@Override
	public boolean unselect() {
		marksState.removeAllMarks(); 
		if(iterator.getGranularity()==Granularity.SINGLE) {
			IterableNode current = currentNode();
			if(current instanceof IterableNodeCollection) {
				if(!((IterableNodeCollection)current).unselect()) {
					return false;
				}
				isChildSelected=false;
			}
		}
		iterator.increaseGranularity();
		if(!iterator.isAtTopGranularity()) {
			markAllMatchingBorders();
			markAllMatchingSelections();
		} else {
			iterator.navigateToIndex(0);
			isSelected = false;
		}
		return iterator.isAtTopGranularity();
	}

	/**
	 * Unmarks the current node if possible, and moves the marking to the next node.
	 */
	@Override
	public void next() {
		marksState.removeAllSelectionMarks();
		if(iterator.getGranularity()==Granularity.SINGLE && isChildSelected) {
			IterableNode current = currentNode();
			if(current instanceof IterableNodeCollection) {
				((IterableNodeCollection)current).next();
				return;
			}
		}
		do {
			iterator.iterate();
		} while(!canStopAtElement());
		markAllMatchingSelections();
	}

	private boolean canStopAtElement() {
		for(int i=iterator.getLower();i<iterator.getUpper();i++) {
			if(!disabledIndexes.contains(i)) return true;
		}
		return false;
	}

	private void markAllMatchingSelections() {
		List<Integer> selectedIndexes = iterator.getSelectedIndexes();
		if(selectedIndexes.size()==0) return;
		marksState.markRegion(childrenToNodes().subList(selectedIndexes.get(0),
				selectedIndexes.get(selectedIndexes.size()-1)+1));
	}

	private void markAllMatchingBorders() {
		List<NodesGroupLimits1D> limits = iterator.getSiblingGroups();
		if(limits.size()==0) return;
		for(NodesGroupLimits1D limit : limits)
			marksState.markRegionBorders(childrenToNodes().subList(limit.getLeft(), limit.getRight()+1));
	}

	private IterableNode currentNode() {
		return children.get(iterator.getSelectedIndex());
	}

	/**
	 * disables or enables a node, if it is present in a group.
	 * we will skip over disabled nodes while iterating.
	 * @param n
	 * @param mode - 'true' to enable the node, and 'false' to disable
	 * @return true if the node was enabled,false otherwise
	 */
	public boolean setEnabledMode(Node n, boolean mode) {
		
		int index = indexOfChild(n);
		if(index!=-1) {
			if(mode) {
				disabledIndexes.remove(index);
				return true;
			} else {
				disabledIndexes.add(index);
				return false;
			}
		}
		return false;
	}
	
	private int indexOfChild(Node n) {
		for(int i=0;i<children.size();i++) {
			if(children.get(i).getNode().equals(n))
				return i;
		}
		return -1;
	}

	/**
	 * jumps to requested node if it is present in the group.
	 * @param n
	 * @return true on success, and false on failure.
	 */
	public boolean selectSpecific(Node n) {
		marksState.removeAllMarks();
		int index = children.indexOf(n);
		if(index==-1) return false;
		iterator.navigateToIndex(index);
		markAllMatchingSelections();
		markAllMatchingBorders();
		return true;
	}

	public void addChild(Node n) {
		children.add(new IterableNodeSingle(n));
		iterator.setMaxIndex(children.size()-1);
		if(n instanceof MarkingNode) {
			((MarkingNode)n).setMarker(marker);
		}
	}

	@Override
	public List<Node> childrenAsNodes() {
		List<Node> $ = new ArrayList<Node>();
		for(IterableNode n : children) {
			if(n instanceof IterableNodeCollection)
				$.addAll(((IterableNodeCollection)n).childrenAsNodes());
			else
				$.add(n.getNode());
		}
		return $;
	}

	protected List<Node> childrenToNodes() {
		List<Node> $ = new ArrayList<Node>();
		for(IterableNode n : children) {
			$.add(n.getNode());
		}
		return $;
	}


	@Override
	public Node getNode() {
		return children.get(0).getNode().getParent();
	}

	/**
	 * redraws the marks of the selected elements, and the borders of elements/blocks we can iterate over.
	 */
	public void refresh() {
		if(!isSelected || isChildSelected) return;
		if(isSelected && !isChildSelected) {
			marksState.removeAllMarks();
			markAllMatchingBorders();
			markAllMatchingSelections();
		} else {
			marksState.removeAllMarks();
			marksState.markRegion(this.childrenAsNodes());
		}
	}
	
	public boolean isSelected() {
		return isSelected;
	}
}
