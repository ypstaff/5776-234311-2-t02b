package Navigation;

import java.util.ArrayList;
import java.util.List;

import utils.Granularity;
import utils.MenuEventDispatcher;
import utils.MenuEventDispatcher.EventCode;
import utils.iterators.NestedBlockIterator;
import utils.marking.MenuItemMarker;
import javafx.collections.ListChangeListener;
import javafx.scene.Node;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;

/**
 * A menubar whose children menus (and their submenus) can be iterated over.
 * Note : because a menu is a window, which repaints only on showing, this class supports
 * only one level of nesting in the menus, and each MenuItem inserted into a menu must be
 * of type CustomMenuItem and contain a label. (this is enough for most uses, and looks better
 * than a window which flashes while you iterate).
 * @author Sergey
 *
 */
public class IterableMenuBar extends MenuBar implements IterableNodeCollection,ListChangeListener<Menu> {

	private List<MenuItem> mainMenus;
	private boolean isSelected;
	private MenuItemMarker marker;
	private NestedBlockIterator<MenuItem> nestedIterator;

	public IterableMenuBar(int blockSize) {
		mainMenus=new ArrayList<MenuItem>();
		isSelected=false;
		marker=new MenuItemMarker();
		nestedIterator=new NestedBlockIterator<MenuItem>(blockSize);
		super.getMenus().addListener(this);
	}

	
	/**
	 * selects the marked elements, if a single element is marked, a matching
	 * event will be fired, and if a group of elements is marked, we will enter this group
	 * and will be able to iterate over them.
	 */
	@Override
	public void select() {
		if(!isSelected) {
			isSelected=true;
			nestedIterator.addNestingLevel(mainMenus);
			markAllMatching();
		} else {
			List<MenuItem> selectedItems = nestedIterator.getSelectedElements();
			if(selectedItems.size()>1) {
				unmarkAllMatching();
				nestedIterator.decreaseGranularity();
				markAllMatching();
			} else{
				MenuEventDispatcher d = new MenuEventDispatcher();
				MenuItem m = selectedItems.get(0);
				EventCode c=d.getNextEventCode(m);
				unmarkAllMatching();
				if(EventCode.HIDE==c) {
					nestedIterator.removeNestingLevel();
				} else if (EventCode.SHOW==c && ((Menu)m).getItems().size()>0) {
					nestedIterator.addNestingLevel(((Menu)m).getItems());
				} else if(EventCode.FIRE==c && nestedIterator.getNestingLevel()>1) {
					nestedIterator.removeNestingLevel();
				}
				markAllMatching();
				d.dispatchEvent(m);
			}
		}
	}

	/**
	 * selects the next element in the marked group if possible, if not - stays in place.
	 */
	@Override
	public void next() {
		unmarkAllMatching();
		nestedIterator.iterate();
		markAllMatching();


	}

	/**
	 * goes out of the currently selected group. when we are iterating over groups of items in
	 * a menu, and this function is called, we will exit to the menubar.
	 */
	@Override
	public boolean unselect() {
		unmarkAllMatching();
		if(nestedIterator.getCurrentGranularity()==Granularity.SINGLE) {
			nestedIterator.increaseGranularity();
			if(nestedIterator.getCurrentGranularity()==Granularity.ALL && (nestedIterator.getNestingLevel()>1)) 
				nestedIterator.getSelectedElements().get(0).getParentMenu().hide();
		}  else {
			if(nestedIterator.getNestingLevel()>1) {
				nestedIterator.getSelectedElements().get(0).getParentMenu().hide();
			}
			nestedIterator.removeNestingLevel(); 
		}
		if(nestedIterator.getNestingLevel()==0) 
			isSelected=false;
		else
			markAllMatching();
		return nestedIterator.getNestingLevel()==0;
	}

	@Override
	public void onChanged(
			javafx.collections.ListChangeListener.Change<? extends Menu> c) {
		while(c.next())
			if(c.wasAdded()) {
				mainMenus.addAll(c.getAddedSubList());
			} else if(c.wasRemoved()) {
				mainMenus.removeAll(c.getRemoved());
			}
	}

	
	/**
	 * changes the style of selected element.
	 */
	private void markAllMatching() {
		for(MenuItem s : nestedIterator.getSelectedElements()) {
			marker.createMark(s);
		}

	}

	/**
	 * removes the changes made to the style of the previously selected elements.
	 */
	private void unmarkAllMatching() {
		for(MenuItem s : nestedIterator.getSelectedElements()) {
			marker.removeMark(s);
		}
	}

	/**
	 * returns the MenuBar of this instance.
	 */
	@Override
	public Node getNode() {
		return this;
	}

	/**
	 * return all children and self as Nodes.
	 */
	@Override
	public List<Node> childrenAsNodes() {
		List<Node> $ = new ArrayList<Node>();
		$.add(this);
		$.addAll(this.getChildrenUnmodifiable());
		return $;
	}


}
