package Navigation;

import utils.marking.OverlayMarker;

/**
 * A node which uses an OverlayMarker to create selection and border marks, and must receive it from its parent.
 * @author Sergey
 *
 */
public interface MarkingNode {
	public void setMarker(OverlayMarker m);
}
