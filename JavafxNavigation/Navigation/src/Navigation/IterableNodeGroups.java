package Navigation;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.function.Consumer;

import utils.iterators.GranularIterator;
import utils.iterators.SingleCellIterator;
import utils.marking.MarksState;
import utils.marking.OverlayMarker;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Bounds;
import javafx.scene.Node;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;

/***
 * Used to iterate over groups of Nodes, while each group is iterable by itself.
 * @author Sergey
 *
 */
public class IterableNodeGroups {
	private List<IterableNodeGroup> children;
	private Pane encapsulatingPane;
	private Pane glassPane;
	private OverlayMarker marker;
	private MarksState marksState;
	private boolean isChildSelected;
	private Consumer<Void> onBackActionConsumer;
	private HashSet<Integer> disabledIndexes;
	private GranularIterator iterator;
	private boolean markChildrenBorders;
	public enum Action {NEXT,SELECT,BACK,OPTIONS};

	private IterationControlHBox controlPane;

	public IterableNodeGroups(Pane mainPane) {
		this(mainPane,false);
	}	
	
	public IterableNodeGroups(Pane mainPane, boolean markChildBorders) {
		this.encapsulatingPane=new StackPane();
		encapsulatingPane.getChildren().add(mainPane);
		glassPane=new Pane();
		glassPane.setMouseTransparent(true);
		encapsulatingPane.getChildren().add(glassPane);
		children=new ArrayList<IterableNodeGroup>();
		marker=new OverlayMarker(glassPane);
		onBackActionConsumer = null;
		iterator = new SingleCellIterator(0, 1);
		disabledIndexes = new HashSet<Integer>();
		controlPane = new IterationControlHBox(this);
		marksState = new MarksState(marker);
		this.markChildrenBorders = markChildBorders;
	}

	public IterableNodeGroups(Pane mainPane,
			Color selectionOverlayColor,
			Color selectionBorderColor) {
		this(mainPane);
		marker = new OverlayMarker(glassPane,selectionOverlayColor,selectionBorderColor);
	}

	/**
	 * returns a StackPane which is build from laying a glass pane over the user's mainPane.
	 * @return
	 */
	public Pane getParentPane() {
		return encapsulatingPane;
	}

	private void bindOnBoundsChangeListener(Node n) {
		n.boundsInParentProperty().addListener(new ChangeListener<Bounds>() {

			@Override
			public void changed(ObservableValue<? extends Bounds> arg0,
					Bounds arg1, Bounds arg2) {
				refresh();
			}
			
		});
	}
	
	/**
	 * creates an IterableNodeGroup with a single child and returns it.
	 */
	public IterableNodeGroup addChildGroup(Node child) {
		IterableNodeGroup $ =  addGroup(new IterableNodeGroup(Arrays.asList(child),marker));
		bindOnBoundsChangeListener(child);
		return $;
	}

	/**
	 * creates an IterableNodeGroup with a list of Nodes and returns it.
	 * @param groupChildren
	 * @return
	 */
	public IterableNodeGroup addChildGroup(List<Node> groupChildren) {
		IterableNodeGroup $ =  addGroup(new IterableNodeGroup(groupChildren, marker));
		for(Node n : groupChildren) {
			bindOnBoundsChangeListener(n);
		}
		return $;
	}
	
	/**
	 * creates an IterableNodeGroup with a list of Nodes returns it.
	 * @param groupChildren
	 * @param blockSize - how many elements we will iterate over at once. after calling select(), we will iterate over
	 * 1 element at a time, but after calling unselect() we will resume iterating over <i>blockSize</i> elements
	 * @return
	 */
	public IterableNodeGroup addChildGroup(List<Node> groupChildren,int blockSize) {
		IterableNodeGroup $ =  addGroup(new IterableNodeGroup(groupChildren, marker, blockSize));
		for(Node n : groupChildren) {
			bindOnBoundsChangeListener(n);
		}
		return $;
	}
	
	public IterableNodeGroup addChildGroup(IterableNodeGroup group) {
		return addGroup(group);
	}
	
	private IterableNodeGroup addGroup(IterableNodeGroup group) {
		children.add(group);
		iterator.setMaxIndex(children.size());
		return children.get(children.size()-1);
	}
	
	/**
	 * clears all the created marks, and clears all the IterableNodeGroups this object contains.
	 */
	public void clearGroups() {
		if(isChildSelected)
			while(!getGroupAtCurrent().unselect()) {}
		this.children.clear();
		this.glassPane.getChildren().clear();
		this.iterator = new SingleCellIterator(0, 1);
		this.isChildSelected=false;
	}

	/***
	 * Select the currently marked group.
	 */
	public void select() {
		isChildSelected=true;
		marksState.removeAllBorderMarks();
		getGroupAtCurrent().select();
	}

	/**
	 * Moves selection from the current child group if possible, and marks its parent pane.
	 */
	public void unselect() {
		if (!isChildSelected)
			return;
		if(getGroupAtCurrent().unselect()) {
			isChildSelected = false;
			if(onBackActionConsumer!=null)
				onBackActionConsumer.accept(null);
			if(disabledIndexes.contains(iterator.getSelectedIndex())) {
				return;
			}
			getGroupAtCurrent().selectAll(); 
			if(markChildrenBorders)
				refresh();
		}
	}

	public void setOnLastBackConsumer(Consumer<Void> consumer) {
		this.onBackActionConsumer = consumer;
	}

	/***
	 * Selects the next child group if no group is selected, otherwise
	 * we mark the next element in the selected group.
	 */
	public void next() {
		if(isChildSelected) {
			getGroupAtCurrent().next();
		} else {
			getGroupAtCurrent().unselectAll();
			do {
				iterator.iterate();
			} while(!canStopAtElement());
			getGroupAtCurrent().selectAll();
		}
	}

	private boolean canStopAtElement() {
		for(int i=iterator.getLower();i<iterator.getUpper();i++) {
			if(!disabledIndexes.contains(i)) return true;
		}
		return false;
	}

	/**
	 * marks the first IterableNodeGroup
	 */
	public void start() {
		children.get(0).unselectAll();
		children.get(0).selectAll();
	}

	/**
	 * selects a specific node, and marks it, if it exists in one of the IterableNodeGroups.
	 * if this call is successful we will resume iterating from the selected node.
	 * @param n
	 * @return true on success, false otherwise.
	 */
	public boolean selectSpecific(Node n) {
		exitSelectedGroup();
		for(IterableNodeGroup child : children) {
			if(child.selectSpecific(n)) {
				getGroupAtCurrent().unselectAll();
				iterator.navigateToIndex(children.indexOf(child));
				isChildSelected=true;
				return true;
			}
		}
		jumpToIndex(0);
		return false;
	}

	/**
	 * selects a specific IterableNodeGroup which this instance holds. iteration over groups will
	 * resume from the selected group
	 * @param group
	 * @return true on success, false otherwise.
	 */
	public boolean selectSpecific(IterableNodeGroup group) {
		exitSelectedGroup();
		isChildSelected=false;
		for(IterableNodeGroup child : children) {
			if(child.equals(group)) {
				jumpToIndex(children.indexOf(child));
				return true;
			}
		}
		jumpToIndex(0);
		return false;
	}
	
	
	private void exitSelectedGroup() {
		IterableNodeGroup toUnselect = getGroupAtCurrent();
		while(toUnselect.unselect()==false) {}
	}
	
	private void jumpToIndex(int n) {
		getGroupAtCurrent().unselectAll();
		iterator.navigateToIndex(n);
		getGroupAtCurrent().selectAll();
	}

	@Deprecated
	public void doAction(Action action) {
		switch(action) {
		case NEXT : next();break;
		case SELECT : select();break;
		case BACK : unselect();break;
		default:
			break;
		}
	}
	
	/**
	 *
	 * @return a pane with buttons which are used to call the functions of this instance, and can 
	 * recieve additional functionality by setting their OnLongClickConsumers.
	 */
	public IterationControlHBox getIterationControlHBoxControlHBox() {
		return controlPane;
	}

	/**
	 * disables or enables a node, if it exists in any of the children groups. a disabled node will be skipped in iterations.
	 * @param n
	 * @param mode
	 * @return
	 */
	public boolean setEnabledMode(Node n, boolean mode) {
		for(IterableNodeGroup $ : children) {
			if($.setEnabledMode(n, mode)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * disables or enables a child group. a disabled group will be skipped in iterations.
	 * @param group
	 * @param mode
	 * @return
	 */
	public boolean setEnabledMode(IterableNodeGroup group, boolean mode) {
		int index = children.indexOf(group);
		if(index!=-1) {
			if(mode) {
				disabledIndexes.remove(index);
				return true;
			} else {
				disabledIndexes.add(index);
				return false;
			}
		}
		return false;
	}
	
	/**
	 * returns the overlay marker which is used by this instance. need this in order to create
	 * IterableNodeGroups not through this instance.
	 * @return
	 */
	public OverlayMarker getOverlayMarker() {
		return this.marker;
	}

	private IterableNodeGroup getGroupAtCurrent() {
		return children.get(iterator.getSelectedIndex());
	}
	
	/**
	 * redraws the current selection marks.
	 */
	public void refresh() {
		marksState.removeAllBorderMarks();
		for(IterableNodeGroup group : children) {
			if(markChildrenBorders)
				marksState.markRegionBorders(group.childrenAsNodes());
			if(group.isSelected()) {
				group.refresh();
			}
		}
		if(isChildSelected)
			return;
		getGroupAtCurrent().unselectAll();
		getGroupAtCurrent().selectAll();
	}
	
	
}
