package Navigation;

/**
 * defines an iterface for an object which contains a root IterableNodeGroups object and can return it.
 * @author Sergey
 *
 */
public interface RootedObject  {
	IterableNodeGroups getRoot();
}
