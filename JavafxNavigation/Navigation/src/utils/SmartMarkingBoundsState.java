package utils;

/**
 * Same as MarkingBoundsState, but can skip redundant states in selection/iteration.
 * @author Sergey
 *
 */
public class SmartMarkingBoundsState extends MarkingBoundsState {

	public SmartMarkingBoundsState(IterationType iterationType, int columnBlockSize,
			int rowBlockSize, int rows, int columns) {
		super(iterationType, columnBlockSize, rowBlockSize, rows, columns);
	}
	
	public SmartMarkingBoundsState(IterationType type, int columnBlockSize,
			int rowBlockSize, int rowsCount, int columnsCount, int rowsStart,
			int columnsStart) {
		super(type,columnBlockSize,rowBlockSize,rowsCount,columnsCount,rowsStart,columnsStart);
	}

	public boolean isInRedundantState() {
		return (granularity==IterationGranularity.MAIN_SINGLE &&
				mainIterator.getMaxIndex()-mainIterator.getSelectedIndex()==1) 
				||(granularity==IterationGranularity.MAIN_SINGLE && mainIterator.getBlockSize()==1)
				|| (granularity==IterationGranularity.SECONDARY_BLOCK && 
				secondaryIterator.getMaxIndex()-secondaryIterator.getStartIndex()==secondaryIterator.getBlockSize())
				|| (granularity==IterationGranularity.MAIN_BLOCK &&
					mainIterator.getMaxIndex()-mainIterator.getStartIndex()<=mainIterator.getBlockSize());
	}
	
	@Override
	public void decreaseGranularity() {
		super.decreaseGranularity();
		while(isInRedundantState())
			super.decreaseGranularity();
	}
	
	@Override
	public void increaseGranularity() {
		super.increaseGranularity();
		for(int i=0; i<4 && isInRedundantState();i++)
			super.increaseGranularity();
	}
	

}
