package utils;

/**
 * Used to create bounds (of indexes) of groups of nodes, since we can usually set one dimension
 * as constant and only update the other.
 * @author Sergey
 *
 */
public class NodesGroupLimitsFactory {
	public int top,left,buttom,right;

	public int getTop() {
		return top;
	}

	public NodesGroupLimitsFactory setTop(int top) {
		this.top = top;
		return this;
	}

	public int getLeft() {
		return left;
	}

	public NodesGroupLimitsFactory setLeft(int left) {
		this.left = left;
		return this;
	}

	public int getButtom() {
		return buttom;
	}

	public NodesGroupLimitsFactory setButtom(int buttom) {
		this.buttom = buttom;
		return this;
	}

	public int getRight() {
		return right;
	}

	public NodesGroupLimitsFactory setRight(int right) {
		this.right = right;
		return this;
	}
	
	public NodesGroupLimits build() {
		return new NodesGroupLimits(top, left, buttom, right);
	}
}
