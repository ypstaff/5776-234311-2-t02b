package utils;

import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;

/**
 * Used to dispatch ActionEvent to MenuItems, and find out what the is the next correct event to dispatch.
 * @author Sergey
 *
 */
public class MenuEventDispatcher {
	public enum EventCode {HIDE,SHOW,FIRE};
	public EventCode getNextEventCode(MenuItem m) {
		if(m instanceof Menu) {
			Menu menu = (Menu)m;
			if(menu.getItems().size()==0) {
				return EventCode.FIRE;
			} else if(menu.isShowing()) {
				return EventCode.HIDE;
			} else {
				return EventCode.SHOW;
			}
		} else {
			return EventCode.FIRE;
		}
	}
	
	public void dispatchEvent(MenuItem m) {
		if(m instanceof Menu) {
			Menu menu = (Menu)m;
			if(menu.getItems().size()==0) {
				menu.fire();
			} else if(menu.isShowing()) {
				menu.hide();
			} else {
				menu.show();
			}
		} else {
			m.fire();
		}
	}
}
