package utils;

/**
 * Contains bounds (of indexes) of a group of nodes in 1D
 * @author Sergey
 *
 */
public class NodesGroupLimits1D {
	private int left,right;
	
	public NodesGroupLimits1D(int left,int right) {
		this.left=left;
		this.right=right;
	}

	public int getLeft() {
		return left;
	}

	public void setLeft(int left) {
		this.left = left;
	}

	public int getRight() {
		return right;
	}

	public void setRight(int right) {
		this.right = right;
	}
}
