package utils;

/**
 * Contains bounds (of indexes) of a group of node in 2D.
 * @author Sergey
 *
 */
public class NodesGroupLimits {
	private NodesGroupLimits1D vertical,horizontal;
	 
	public NodesGroupLimits(int top,int left,int buttom,int right) {
		vertical = new NodesGroupLimits1D(top, buttom);
		horizontal = new NodesGroupLimits1D(left, right);
	}

	public int getTop() {
		return vertical.getLeft();
	}

	public int getLeft() {
		return horizontal.getLeft();
	}

	public int getButtom() {
		return vertical.getRight();
	}

	public int getRight() {
		return horizontal.getRight();
	}
	
	public void rotate() {
		NodesGroupLimits1D tmp = vertical;
		vertical = horizontal;
		horizontal = tmp;
	}
	
}
