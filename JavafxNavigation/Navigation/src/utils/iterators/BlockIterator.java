package utils.iterators;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;

import utils.Granularity;
import utils.NodesGroupLimits1D;

public class BlockIterator implements GranularIterator {
	protected int selectedIndex,offset,maxIndex,blockSize;
	protected int lower,upper;
	protected int startIndex;
	
	protected Granularity granularity;
	
	public BlockIterator(int maxIndex,int blockSize,Granularity startingGranularity) {
		this(maxIndex,blockSize,startingGranularity,1);
	}
	
	public BlockIterator(int maxIndex,int blockSize,Granularity startingGranularity,int startIndex) {
		if(maxIndex<0  || startIndex<0 || maxIndex<startIndex || blockSize<0) {
			throw new IllegalArgumentException();
		}
		this.startIndex=startIndex;
		this.lower=startIndex;
		this.upper=(startingGranularity==Granularity.ALL) ?maxIndex : lower+blockSize;
		this.maxIndex=maxIndex;
		this.selectedIndex=1;
		this.offset=0;
		this.blockSize=blockSize;
		this.granularity=startingGranularity;
	}
	
	@Override
	public void iterate() {
		if(granularity==Granularity.BLOCK) {
			if(lower+blockSize>=maxIndex) lower=startIndex;
			else lower=lower+blockSize;
			upper = (Math.min(blockSize+lower,maxIndex));
		} else {
			offset = (offset+1)%Math.min(blockSize, maxIndex-selectedIndex);
			lower = selectedIndex+(offset)%(Math.min(blockSize, maxIndex-selectedIndex));
			upper = lower+1;
		}
			
	}
	
	@Override
	public void decreaseGranularity() {
		if (granularity == Granularity.SINGLE)
			return;
		if(granularity==Granularity.BLOCK) {
			offset = 0;
			upper = lower + 1;
			selectedIndex = lower;
		} else if(granularity == Granularity.ALL) {
			upper=lower+blockSize;
		}
		granularity=granularity.decrease();
	}
	
	@Override
	public void increaseGranularity() {
		if(granularity == Granularity.ALL)
			return;
		if(granularity == Granularity.BLOCK) {
			lower=startIndex;
			upper=maxIndex;
		} else if(granularity == Granularity.SINGLE) {
			lower=selectedIndex;
			upper=Math.min(lower+blockSize,maxIndex);
			offset=0;
		}
		granularity=granularity.increase();
	}
	
	@Override
	public Predicate<Integer> getPredicate() {
		return (x)->{return x>=lower && x<upper;};
	}
	
	public void setMaxIndex(int maxIndex) {
		this.maxIndex=maxIndex;
	}
	
	public NodesGroupLimits1D getDimensionBoundsForCurrent() {
		return new NodesGroupLimits1D(lower,upper-1);
	}
	
	
	
	@Override
	public Granularity getGranularity() {
		return this.granularity;
	}
	
	public int getStartIndex() {
		return this.startIndex;
	}
	
	public int getMaxIndex() {
		return this.maxIndex;
	}
	
	public int getLower() {
		return this.lower;
	}
	
	public int getUpper() {
		return this.upper;
	}
	
	public int getSelectedIndex() {
		return this.selectedIndex+this.offset;
	}
	
	public int getBlockSize() {
		return this.blockSize;
	}
	
	public List<Integer> getSelectedIndexes() {
		List<Integer> $ = new ArrayList<Integer>();
		for(int i=lower;i<upper;i++) {
			$.add(i);
		}
		return $;
	}

	@Override
	public boolean isAtTopGranularity() {
		return this.granularity==Granularity.ALL;
	}
	
	public List<NodesGroupLimits1D> getSiblingGroups() {
		List<NodesGroupLimits1D> $= new ArrayList<NodesGroupLimits1D>();
		List<Integer> bounds = getDimensionBoundsForAll();
		for(int i=0;i<bounds.size()-1;i++) {
			$.add(new NodesGroupLimits1D(bounds.get(i), bounds.get(i+1)-1));
		}
		return $;
	}
	
	private List<Integer> getDimensionBoundsForAll() {
		if(Granularity.ALL==this.granularity) {
			return Arrays.asList(1,maxIndex-1);
		}
		List<Integer> $ = new ArrayList<Integer>();
		int index,delta,max;
		if(this.granularity==Granularity.BLOCK) {
			index=startIndex;
			delta=blockSize;
			max=maxIndex;
		} else {
			index=selectedIndex;
			delta = 1;
			max = Math.min(selectedIndex+blockSize,maxIndex);
		}
		while(index<max) {
			$.add(index);
			index+=delta;
		}
		$.add(max);
		return $;
	}

	@Override
	public void navigateToIndex(int index) {
		if(index<1 || index>this.maxIndex) return;
		if(this.granularity==Granularity.SINGLE)
			this.increaseGranularity();
		else if(this.granularity==Granularity.ALL)
			this.decreaseGranularity();
		while(index<lower || index>upper) {
			this.iterate();
		}
		this.decreaseGranularity();
		while(this.selectedIndex+offset!=index) {
			this.iterate();
		}
	}
	
	public boolean canSendEvent() {
		return upper-lower==1;
	}
}
