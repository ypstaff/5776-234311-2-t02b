package utils.iterators;

import utils.Granularity;


public class SmartBlockIterator extends BlockIterator {

	
	public SmartBlockIterator(int maxIndex, int blockSize,
			Granularity startingGranularity) {
		super(maxIndex, blockSize, startingGranularity);
		
		if(isInRedundantState())
			super.decreaseGranularity();
	}
	
	public SmartBlockIterator(int maxIndex,int blockSize,
			Granularity startingGranularity, int startIndex) {
		super(maxIndex, blockSize, startingGranularity, startIndex);
		
		if(isInRedundantState()) {
			super.decreaseGranularity();
		}
	}
	
	private boolean isInRedundantState() {
		return ((this.granularity==Granularity.BLOCK && this.maxIndex-this.startIndex==this.blockSize) || 
				(this.granularity==Granularity.BLOCK && this.maxIndex-this.selectedIndex==1));
	}

	@Override
	public void increaseGranularity() {
		super.increaseGranularity();
		if(isInRedundantState())
			super.increaseGranularity();
	}
	
	
	

}
