package utils.iterators;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;

import utils.Granularity;
import utils.NodesGroupLimits1D;

public class SingleCellIterator implements GranularIterator{
	private int startIndex,maxIndex;
	private int current;
	private Granularity granularity;
	
	public SingleCellIterator(int startIndex,int maxIndex) {
		this.startIndex=startIndex;
		this.maxIndex=maxIndex;
		this.current=startIndex;
		this.granularity=Granularity.BLOCK;
	}
	
	@Override
	public void iterate() {
		current = (current+1)%maxIndex+startIndex;
	}
	
	@Override
	public void increaseGranularity() {
		if(granularity==Granularity.BLOCK) return;
		granularity=granularity.increase();
		return;
	}
	
	@Override
	public void decreaseGranularity() {
		granularity=granularity.decrease();
		return;
	}
	
	@Override
	public Predicate<Integer> getPredicate() {
		return x->{return x==current;};
	}
	@Override
	public Granularity getGranularity() {
		return granularity;
	}
	@Override
	public int getLower() {
		return current;
	}
	@Override
	public int getUpper() {
		return current+1;
	}
	@Override
	public int getSelectedIndex() {
		return current;
	}
	@Override
	public int getBlockSize() {
		return 1;
	}
	@Override
	public int getMaxIndex() {
		return this.maxIndex;
	}
	@Override
	public int getStartIndex() {
		return this.startIndex;
	}
	
	public List<Integer> getSelectedIndexes() {
		return new ArrayList<Integer>(Arrays.asList(this.current));
	}

	@Override
	public boolean isAtTopGranularity() {
		return this.granularity==Granularity.BLOCK;
	}
	
	public List<NodesGroupLimits1D> getSiblingGroups() {
		List<NodesGroupLimits1D> $= new ArrayList<NodesGroupLimits1D>();
		for(int i=startIndex;i<maxIndex;i++) {
			$.add(new NodesGroupLimits1D(i, i));
		}
		return $;
	}

	@Override
	public void navigateToIndex(int index) {
		if(index<startIndex || index>this.maxIndex) return;
		while(current!=index) {
			this.iterate();
		}
	}

	@Override
	public void setMaxIndex(int maxIndex) {
		this.maxIndex=maxIndex;
		
	}
}
