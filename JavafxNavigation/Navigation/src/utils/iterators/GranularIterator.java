package utils.iterators;

import java.util.List;
import java.util.function.Predicate;

import utils.Granularity;
import utils.NodesGroupLimits1D;

public interface GranularIterator {
	public void iterate();
	public void increaseGranularity();
	public void decreaseGranularity();
	public Predicate<Integer> getPredicate();
	public Granularity getGranularity();
	public int getLower();
	public int getUpper();
	public int getSelectedIndex();
	public int getBlockSize();
	public int getMaxIndex();
	public int getStartIndex();
	public void setMaxIndex(int maxIndex);
	public List<Integer> getSelectedIndexes();
	public boolean isAtTopGranularity();
	public List<NodesGroupLimits1D> getSiblingGroups();
	public void navigateToIndex(int index);
}

