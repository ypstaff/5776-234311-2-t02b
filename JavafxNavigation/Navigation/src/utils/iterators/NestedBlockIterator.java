package utils.iterators;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;
import java.util.function.Predicate;

import utils.Granularity;

import com.google.inject.Inject;

public class NestedBlockIterator<T> {
	private Stack<List<T>> lists;
	private Stack<BlockIterator> iterators;
	@Inject private BlockIteratorFactory initialIteratorFactory;
	
	public NestedBlockIterator(int blockSize) {
		if(blockSize<0) throw new IllegalArgumentException();
		this.initialIteratorFactory = new BlockIteratorFactory() {
			
			@Override
			public BlockIterator build(int maxIndex) {
				return new SmartBlockIterator(maxIndex,blockSize,Granularity.BLOCK,0);
			}
		};
		lists=new Stack<List<T>>();
		iterators=new Stack<BlockIterator>();
	}
	
	public int getNestingLevel() {
		return lists.size();
	}
	
	public void addNestingLevel(List<T> list) {
		lists.push(list);
		iterators.push(initialIteratorFactory.build(list.size()));
	}
	
	public void removeNestingLevel() {
		if(lists.size()==0) return;
		lists.pop();
		iterators.pop();
	}
	
	public void iterate() {
		lastIterator().iterate();
	}
	
	public void increaseGranularity() {
		lastIterator().increaseGranularity();
	}
	
	public void decreaseGranularity() {
		lastIterator().decreaseGranularity();
	}
	
	public List<T> getSelectedElements() {
		List<T> res = new ArrayList<T>();
		List<T> lastList=lists.peek();
		Predicate<Integer> p = lastIterator().getPredicate();
		for(int i=0;i<lastList.size();i++) {
			if(p.test(i)) res.add(lastList.get(i));
		}
		return res;
	}
	
	public Granularity getCurrentGranularity() {
		return lastIterator().getGranularity();
	}
	
	private BlockIterator lastIterator() {
		return iterators.peek();
	}
}
