package utils.iterators;

public interface BlockIteratorFactory {
	public BlockIterator build(int maxIndex);
}
