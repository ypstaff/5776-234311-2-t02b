package utils.marking;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

import javafx.geometry.Bounds;
import javafx.scene.Node;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;

public class MarksState {
		
	private List<Rectangle> currentSelectionMarks;
	private List<Line> currentBorderMarks;
	private OverlayMarker marker;
	
	
	public MarksState(OverlayMarker marker) {
		if(marker == null) throw new NullPointerException();
		this.marker=marker;
		this.currentBorderMarks=new ArrayList<Line>();
		this.currentSelectionMarks=new ArrayList<Rectangle>();
	}
	
	public void mark(Node n) {
		markSelection(n);
		markBorders(n);
	}
	
	public void markRegion(List<Node> region) {
		Double[] dimensions = getRegionDimensions(region);
		currentBorderMarks.addAll(marker.markBorders(
				dimensions[0], dimensions[1],dimensions[2], dimensions[3]));
		currentSelectionMarks.add(marker.createMark(
				dimensions[0], dimensions[1],dimensions[2], dimensions[3]));
	}
	
	public void markRegionBorders(List<Node> region) {
		Double[] dimensions = getRegionDimensions(region);
		currentBorderMarks.addAll(marker.markBorders(
				dimensions[0], dimensions[1],dimensions[2], dimensions[3]));
	}
	
	private Double[] getRegionDimensions(List<Node> region) {
		Double[] dimensions = new Double[4];
		List<Bounds> bounds = region.stream()
				.map(new Function<Node, Bounds>() {
					@Override
					public Bounds apply(Node t) {
						return t.localToScene(t.getBoundsInLocal());
					}
				}).collect(Collectors.toList());
		Comparator<Double> doubleComparator = new Comparator<Double>() {
			@Override
			public int compare(Double o1, Double o2) {
				if(o1<o2) return -1;
				else if(o1>o2) return 1;
				return 0;
 			}
		};
		dimensions[0] = bounds.stream().map(Bounds::getMinX).min(doubleComparator).get().doubleValue();
		dimensions[1] = bounds.stream().map(Bounds::getMinY).min(doubleComparator).get().doubleValue();
		dimensions[2] = bounds.stream().map(Bounds::getMaxX).max(doubleComparator).get().doubleValue();
		dimensions[3] = bounds.stream().map(Bounds::getMaxY).max(doubleComparator).get().doubleValue();
		return dimensions;
	}
	
	public void markSelection(Node n) {
		currentSelectionMarks.add(marker.createMark(n));
	}
	
	public void markBorders(Node n) {
		currentBorderMarks.addAll(marker.markBorders(n));
	}
	
	public void markBorders(Node tl,Node br) {
		currentBorderMarks.addAll(marker.markBorders(tl,br));
	}
	
	
	public void markSelectionAll(List<Node> elements) {
		elements.forEach(element->markSelection(element));
	}
	
	public void markBordersAll(List<Node> elements) {
		elements.forEach(element->markBorders(element));
	}
	
	public void markAll(List<Node> elements) {
		elements.forEach(element->mark(element));
	}
	
	public void removeAllMarks() {
		removeAllSelectionMarks();
		removeAllBorderMarks();
	}
	
	public void removeAllSelectionMarks() {
		marker.removeMarks(currentSelectionMarks);
		currentSelectionMarks.clear();
	}
	
	public void removeAllBorderMarks() {
		marker.removeBorderMarks(currentBorderMarks);
		currentBorderMarks.clear();
	}

}
