package utils.marking;

import java.util.HashMap;

import javafx.css.Styleable;
import javafx.scene.Node;
import javafx.scene.control.CustomMenuItem;
import javafx.scene.control.MenuItem;
public class MenuItemMarker {

	private static String BORDER_STYLE="-fx-border-color : #000000;";
	private static String BACKGROUND_STYLE="-fx-background-color : rgba(250,0,0,0.5);";
	
	private HashMap<Styleable,String> originalStyles;
	
	public MenuItemMarker() {
		originalStyles=new HashMap<Styleable,String>();
		
	}
	
	public void createMark(MenuItem s) {
		createMark(s,BACKGROUND_STYLE);
	}
	
	public void createBorderMark(MenuItem s) {
		createMark(s,BORDER_STYLE);
	}
	
	private void createMark(MenuItem s , String mark) {
		if(s instanceof CustomMenuItem) {
			CustomMenuItem cmi = (CustomMenuItem)s;
			changeStyle(cmi.getContent(),mark);
		} else {
			changeStyle(s,mark);
		}
	}

	private void changeStyle(Node s,String mark) {
		String newStyle=mark;
		String originalStyle=s.getStyle();
		if(s.getStyle()!=null) {
			newStyle=mark+originalStyle;
		}
		s.setStyle(newStyle);
		originalStyles.put(s, originalStyle);
	}
	
	private void changeStyle(MenuItem s,String mark) {
		String newStyle=mark;
		String originalStyle=s.getStyle();
		if(s.getStyle()!=null) {
			newStyle=mark+originalStyle;
		}
		s.setStyle(newStyle);
		originalStyles.put(s, originalStyle);

	}
	
	public void removeMark(MenuItem s) {
		removeMark(s,BACKGROUND_STYLE);
	}
	
	public void removeBorderMark(MenuItem s) {
		removeMark(s,BORDER_STYLE);
	}
	
	private void removeMark(MenuItem s,String mark) {
		if(s instanceof CustomMenuItem) {
			CustomMenuItem cmi = (CustomMenuItem)s;
			undoStyleChange(cmi.getContent(), mark);
		} else
			undoStyleChange(s,mark);
	}
	
	private void undoStyleChange(MenuItem s,String mark) {
		s.setStyle(s.getStyle().replace(mark, ""));
	}
	
	private void undoStyleChange(Node s,String mark) {
		s.setStyle(s.getStyle().replace(mark, ""));
	}
}
