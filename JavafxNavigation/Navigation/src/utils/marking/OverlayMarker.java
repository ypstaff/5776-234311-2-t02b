package utils.marking;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javafx.geometry.Bounds;
import javafx.scene.Node;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;

public class OverlayMarker {
	
	private Pane glassPane;
	private Color overlayColor;
	private Color borderLineColor;
	
	public OverlayMarker(Pane glassPane) {
		this(glassPane,Color.RED,Color.BLACK);
	}
	
	public OverlayMarker(Pane glassPane, Color overlayColor, Color borderLineColor) {
		this.glassPane=glassPane;
		this.overlayColor=overlayColor;
		this.borderLineColor=borderLineColor;
	}
 	
	/**
	 * Marks a given element by drawing a transparent rectangle over it on the marker's glass pane.
	 * @param element - node to be marked.
	 * @return Rectangle which represents the created mark. 
	 */
	public Rectangle createMark(Node element) {
		Bounds b = element.localToScene(element.getBoundsInLocal());
		return createMark(b.getMinX(),b.getMinY(),b.getMaxX(),b.getMaxY());
	}
	
	public Rectangle createMark(double topX,double topY,double buttomX,double buttomY) {
		Rectangle $ = new Rectangle();
		$.setX(topX);
		$.setY(topY);
		$.setWidth(buttomX-topX);
		$.setHeight(buttomY-topY);
		$.setFill(overlayColor);
		$.setOpacity(0.5);
		$.setMouseTransparent(true);
		glassPane.getChildren().add($);
		return $;
	}
	
	/**
	 * Removes a mark which was made by this instance.
	 * @param r - mark to remove.
	 */
	public void removeMark(Rectangle mark) {
		glassPane.getChildren().remove(mark);
	}
	
	public void removeMarks(List<Rectangle> marks) {
		for(Rectangle mark : marks) {
			removeMark(mark);
		}
	}
	
	public List<Line> markBorders(Node element) {
		Bounds b = element.localToScene(element.getBoundsInLocal());
		double tl_x=b.getMinX();
		double tl_y=b.getMinY();
		double br_x=b.getMaxX();
		double br_y=b.getMaxY();
		return markBorders(tl_x,tl_y,br_x,br_y);
	}
	
	public List<Line> markBorders(Node topLeft,Node buttomRight) {
		Bounds b1 = topLeft.localToScene(topLeft.getBoundsInLocal());
		Bounds b2 = buttomRight.localToScene(buttomRight.getBoundsInLocal());
		double tl_x=b1.getMinX();
		double tl_y=b1.getMinY();
		double br_x=b2.getMaxX();
		double br_y=b2.getMaxY();
		return markBorders(tl_x,tl_y,br_x,br_y);
	}
	
	public List<Line> markBorders(double topX,double topY,double buttomX,double buttomY) {
		List<Line> lines = new ArrayList<Line>();
		Line top = new Line(topX,topY,buttomX,topY);
		Line left = new Line(topX,topY,topX,buttomY);
		Line right = new Line(buttomX,topY,buttomX,buttomY);
		Line bottom = new Line(topX,buttomY,buttomX,buttomY);
		lines.addAll(Arrays.asList(top,left,right,bottom));
		for(Line l : lines) {
			l.setStroke(borderLineColor);
			glassPane.getChildren().add(l);
		}
		return lines;
	}
	
	public void removeBorderMarks(List<Line> lines) {
		for(Line l : lines) {
			glassPane.getChildren().remove(l);
		}
	}
}
