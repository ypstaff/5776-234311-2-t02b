package utils;

import javafx.scene.Node;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import lombok.NonNull;

/**
 * Dispatches an MouseEvent of the type MOUSE_CLICKED to the given node.
 * @author Sergey
 *
 */
public class OnClickEventDispatcher {
	
	public void dispatchOnClickEvent(@NonNull Node node) {
		double xEventCoord=node.getBoundsInLocal().getMinX();
		double yEventCoord = node.getBoundsInLocal().getMinY();

		node.fireEvent((new MouseEvent(MouseEvent.MOUSE_CLICKED,
				xEventCoord, yEventCoord, xEventCoord, yEventCoord,
				MouseButton.PRIMARY, 1, false, false, false, false, true,
				false, false, true, false, false, null)));
	}
	
}
