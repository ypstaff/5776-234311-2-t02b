package utils;

import java.util.ArrayList;
import java.util.List;
import java.util.function.BiPredicate;

import utils.iterators.BlockIterator;

/**
 * Auxiliary class for iterating over IterableGridPane. Since we support iteration view rows/columns and in
 * different modes, this class was needed to encapsulate the iteration logic.
 * @author Sergey
 *
 */
public class MarkingBoundsState {


	public enum IterationType {ROWS,COLLUMNS};
	public enum IterationGranularity {
		ALL,MAIN_SINGLE ,MAIN_BLOCK ,SECONDARY_SINGLE,SECONDARY_BLOCK;

		public IterationGranularity increase() {
			switch(this) {
			case ALL: return ALL;
			case MAIN_BLOCK: return ALL;
			case MAIN_SINGLE: return MAIN_BLOCK;
			case SECONDARY_SINGLE: return SECONDARY_BLOCK;
			case SECONDARY_BLOCK: return MAIN_SINGLE;
			}
			return MAIN_BLOCK;
		}
		public IterationGranularity decrease() {
			switch(this) {
			case ALL: return MAIN_BLOCK;
			case MAIN_BLOCK: return MAIN_SINGLE;
			case MAIN_SINGLE: return SECONDARY_BLOCK;
			case SECONDARY_SINGLE: return SECONDARY_SINGLE;
			case SECONDARY_BLOCK: return SECONDARY_SINGLE;
			}
			return SECONDARY_SINGLE;
		}
	};
	protected IterationGranularity granularity;

	protected BlockIterator rowsIterator,columnsIterator;
	protected BlockIterator mainIterator,secondaryIterator;

	public MarkingBoundsState(IterationType iterationType,int columnBlockSize ,
			int rowBlockSize,int rows,int columns) {
		this(iterationType,columnBlockSize,rowBlockSize,rows,columns,1,1);
		
	}

	public MarkingBoundsState(IterationType iterationType, int columnBlockSize,
			int rowBlockSize, int rows, int columns, int rowsStart,
			int columnsStart) {
		granularity=IterationGranularity.MAIN_BLOCK;
		Granularity columnsGranularity=Granularity.BLOCK,rowsGranularity=Granularity.ALL;
		if(IterationType.ROWS==iterationType) {
			columnsGranularity=Granularity.ALL;
			rowsGranularity=Granularity.BLOCK;
		}
		columnsIterator=new BlockIterator(columns, columnBlockSize,columnsGranularity,columnsStart);
		rowsIterator=new BlockIterator(rows, rowBlockSize,rowsGranularity,rowsStart);
		
		
		mainIterator=columnsIterator;
		secondaryIterator=rowsIterator;
		if (IterationType.ROWS != iterationType)
			return;
		mainIterator = rowsIterator;
		secondaryIterator = columnsIterator;
	}

	public void increaseGranularity() {
		if(granularity==IterationGranularity.SECONDARY_BLOCK ||
				granularity==IterationGranularity.SECONDARY_SINGLE)
			secondaryIterator.increaseGranularity();
		else if(granularity==IterationGranularity.MAIN_SINGLE)
			mainIterator.increaseGranularity();
		this.granularity=this.granularity.increase();
	}

	public void decreaseGranularity() {
		if(granularity==IterationGranularity.SECONDARY_BLOCK ||
				granularity==IterationGranularity.MAIN_SINGLE) 
			secondaryIterator.decreaseGranularity();
		else if(granularity==IterationGranularity.MAIN_BLOCK)
			mainIterator.decreaseGranularity();
		this.granularity=this.granularity.decrease();
	}

	public void iterate() {
		if(granularity!=IterationGranularity.SECONDARY_SINGLE &&
				granularity!=IterationGranularity.SECONDARY_BLOCK) 
			mainIterator.iterate();
		else
			secondaryIterator.iterate();
	}


	public BiPredicate<Integer,Integer> getPredicate() {
		return new BiPredicate<Integer, Integer>() {

			@Override
			public boolean test(Integer row, Integer col) {
				return rowsIterator.getPredicate().test(row) && columnsIterator.getPredicate().test(col);
			}
		};
	}

	public void setRowsCount(int rows) {
		this.rowsIterator.setMaxIndex(rows);
	}

	public void setColumnsCount(int columns) {
		this.columnsIterator.setMaxIndex(columns);
	}

	public int getColumn() {
		return columnsIterator.getLower();
	}

	public int getRow() {
		return rowsIterator.getLower();
	}

	public boolean isAtTopGranularity() {
		return this.granularity==IterationGranularity.ALL;
	}

	public boolean canSendEvent() {
		return this.mainIterator.canSendEvent() && this.secondaryIterator.canSendEvent();
	}

	public List<NodesGroupLimits> getActiveGroupsLimits() {
		if(IterationGranularity.SECONDARY_BLOCK==this.granularity || IterationGranularity.SECONDARY_SINGLE==this.granularity) {
			return getActiveGroupsLimits(secondaryIterator, mainIterator);

		} else {
			return getActiveGroupsLimits(mainIterator, secondaryIterator);
		}
	}
	
	private List<NodesGroupLimits> getActiveGroupsLimits(BlockIterator mainIndexesProducer,
			BlockIterator secondaryIndexesProducer) {
		List<NodesGroupLimits1D> mainIndexes = mainIndexesProducer.getSiblingGroups();
		NodesGroupLimits1D secondaryIndexes = secondaryIndexesProducer.getDimensionBoundsForCurrent();
		if(secondaryIndexesProducer!=rowsIterator) 
			return createLimitsWithColumnsAsMainDimention(mainIndexes,secondaryIndexes);
		else 
			return createLimitsWithRowsAsMainDimention(mainIndexes,secondaryIndexes);
		
	}
	
	private List<NodesGroupLimits> createLimitsWithColumnsAsMainDimention(List<NodesGroupLimits1D> mainIndexes,
			NodesGroupLimits1D secondaryIndexes) {
		List<NodesGroupLimits> $ = new ArrayList<NodesGroupLimits>();
		NodesGroupLimitsFactory factory = new NodesGroupLimitsFactory();
		factory.setLeft(secondaryIndexes.getLeft());
		factory.setRight(secondaryIndexes.getRight());
		for(NodesGroupLimits1D limit : mainIndexes) {
			factory.setTop(limit.getLeft());
			factory.setButtom(limit.getRight());
			$.add(factory.build());
		}
		return $;
	}
	
	private List<NodesGroupLimits> createLimitsWithRowsAsMainDimention(List<NodesGroupLimits1D> mainIndexes,
			NodesGroupLimits1D secondaryIndexes) {
		List<NodesGroupLimits> $ = createLimitsWithColumnsAsMainDimention(mainIndexes,secondaryIndexes);
		for(NodesGroupLimits limit : $)
			limit.rotate();
		return $;
	}

}
