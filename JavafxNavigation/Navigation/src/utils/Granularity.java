package utils;


/**
 * represents the selection granularity by a GranularIterator of elements in a given group.
 * @author Sergey
 *
 */
public enum Granularity {ALL,BLOCK,SINGLE;
public Granularity increase() {
	switch(this) {
	case ALL : return ALL;
	case BLOCK : return ALL;
	case SINGLE : return BLOCK;
	}
	return null;
}

public Granularity decrease() {
	switch(this) {
	case ALL : return BLOCK;
	case BLOCK : return SINGLE;
	case SINGLE : return SINGLE;
	}
	return null;
}

}