package CompositGUIPanes;

import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import MozartWaves.Project.Project;
import Navigation.IterableGridPane;
import Pattern.Pattern;
import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;
import utils.MarkingBoundsState.IterationType;

public class ProjectDisplayGridPane extends IterableGridPane {

	private Project m_project;
	private Consumer<Pattern> callbackMethod;
	
	
	public ProjectDisplayGridPane(IterationType iterationType, int blockSize,
			Consumer<Pattern> callback,Project project) {
		super(iterationType, blockSize);
		this.callbackMethod = callback;
		m_project = project;
		initData();
	}
	
	private void initData() {
		List<Pattern> patterns = m_project.getPatterns().stream()
				.sorted(Pattern.makePatternComparator()).collect(Collectors.toList());
		for(int i=0;i<patterns.size();i++) {
			PatternDisplayPane p = new PatternDisplayPane(patterns.get(i));
			super.add(p,1, i+1);
			p.setOnMouseClicked(new EventHandler<MouseEvent>() {
				@Override
				public void handle(MouseEvent arg0) {
					callbackMethod.accept(p.getPattern());				
				}
			});
		}
	}
	
	public void updateView(Project project) {
		super.getChildren().clear();
		m_project = project;
		initData();
	}
	

}
