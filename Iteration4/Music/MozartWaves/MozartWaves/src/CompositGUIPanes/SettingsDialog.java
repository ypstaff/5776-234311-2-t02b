package CompositGUIPanes;

import java.util.List;
import java.util.function.Consumer;

import MozartWaves.ApplicationSettings;
import MozartWaves.GuiUtils;
import MozartWaves.MarkableColor;
import Navigation.IterableNodeGroups;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.TilePane;
import javafx.scene.layout.VBox;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;


/**
 * Displays a window where the user can change the settings of the application
 * and receive explanation about non-trivial settings.
 * @author Sergey
 *
 */
public class SettingsDialog extends Stage {
	private ApplicationSettings settings;
	private IterableNodeGroups root;
	private BorderPane mainPane;
	private Pane explanationPane;
	private VBox settingsListWrapper;
	
	public SettingsDialog(ApplicationSettings currentSettings) {
		this.settings = currentSettings;
		this.explanationPane = new HBox();
		mainPane = new BorderPane();
		root = new IterableNodeGroups(mainPane);
		mainPane.setCenter(buildSettingsListPane());
		mainPane.setBottom(root.getIterationControlHBoxControlHBox());
		this.setWidth(800);
		this.setHeight(600);
		Scene s = new Scene(root.getParentPane());
		this.setScene(s);
		root.start();
		GuiUtils.setRoot(root);
	}
	
	/**
	 * builds the pain which is in the center of the main pane, and links it to Navigation.
	 * @return the main content pane of this dialog
	 */
	private Pane buildSettingsListPane() {
		VBox contentAndControls = new VBox();
		HBox centerContent = new HBox();
		VBox settingsListPane = new VBox();
		Label header = new Label("Current Settings");
		settingsListWrapper = new VBox();
		settingsListPane.getChildren().add(header);
		settingsListPane.getChildren().add(settingsListWrapper);
		
		settingsListWrapper.getChildren().add(
				inititateSettingWrapperAndAddToGroups("Group marking color",
						ApplicationSettings.getPossibleMarkingColors(),
						settings.getUsedColor(),
						(v)->buildMarkingColorExplanationPane()));
		
		settingsListWrapper.getChildren().add(
				inititateSettingWrapperAndAddToGroups("Quick pattern reload",
						ApplicationSettings.getBooleanList(),
						settings.isQuickReloadEnabled(),
						(v)->buildQuickReloadExplanationPane()));
		
		settingsListWrapper.getChildren().add(
				inititateSettingWrapperAndAddToGroups("Full screen enabled",
						ApplicationSettings.getBooleanList(),
						settings.isInFullScreenMode(),null));
		
		centerContent.getChildren().add(settingsListPane);
		centerContent.getChildren().add(explanationPane);
		
		contentAndControls.getChildren().add(centerContent);
		TilePane controlButtons = new TilePane();
		Button confirm = new Button("Confirm");
		confirm.setOnMouseClicked(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				settings = makeSettingsObject();
				close();
			}
			
		});
		
		Button cancel = new Button("Cancel");
		cancel.setOnMouseClicked(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				close();
			}
		});
		
		controlButtons.getChildren().add(confirm);
		controlButtons.getChildren().add(cancel);
		contentAndControls.getChildren().add(controlButtons);
		
		root.addChildGroup(controlButtons.getChildren());
		
		return contentAndControls;
	}
	
	/**
	 * Creates a wrapper pane for a setting.
	 * @param settingName 
	 * @param possibleValues 
	 * @param initialValue
	 * @param callback - the method which will be called once the 'explanation' button in this pane is clicked
	 * @return a pane which will enable changing the value of the selected setting and getting the final value
	 */
	private <T> SettingWrapperPane<T> inititateSettingWrapperAndAddToGroups(String settingName,
			List<T> possibleValues, T initialValue, Consumer<Void> callback) {
		SettingWrapperPane<T> $=null;
		if(callback!=null)
			$ = new SettingWrapperPane<T>(
				settingName,possibleValues,initialValue,callback);
		else
			$ = new SettingWrapperPane<T>(
					settingName,possibleValues,initialValue);
		root.addChildGroup($.getControlButtons());
		return $;
	}
	
	/**
	 * builds an explanation pane for the 'quick reload' setting.
	 */
	private void buildQuickReloadExplanationPane() {
		Pane $ = new VBox();
		Label explanationArea = new Label();
		explanationArea.setText("While this option is enabled the application will\n"
				+ "use up more volume on the hard drive while working\n"
				+ "in order to make the loading times of the patterns\n"
				+ "shorter.");
		$.getChildren().add(explanationArea);
		explanationArea.setPadding(new Insets(10));
		mainPane.setRight($);
	}
	
	/**
	 * builds and example pane for the 'marking color' setting
	 */
	private void buildMarkingColorExplanationPane() {
		StackPane $ = new StackPane();
		Pane glass = new Pane();
		VBox wrapper = new VBox();
		
		$.getChildren().add(wrapper);
		$.getChildren().add(glass);
		
		Label header = new Label("Marking color samples");
		wrapper.getChildren().add(header);
		
		HBox samplesBox = new HBox();
		Rectangle markSample;
		for(int i=0;i<MarkableColor.values().length;i++) {
			markSample = new Rectangle(50,50,
					MarkableColor.values()[i].toJavaFXColor());
			markSample.setOpacity(0.5);
			samplesBox.getChildren().add(markSample);
		}
		wrapper.getChildren().add(samplesBox);
		
		
		mainPane.setRight($);
	}
	
	/**
	 * 
	 * @return an ApplicationSettings object with the setting set in this dialog.
	 */
	private ApplicationSettings makeSettingsObject() {
		ApplicationSettings $ = new ApplicationSettings();
		$.setUsedColor(extractSetting(0));
		$.setQuickReloadEnabled(extractSetting(1));
		$.setFullScreenMode(extractSetting(2));
		return $;
	}
	
	/**
	 * 
	 * @param index - index of the setting wrapper pane we want to query.
	 * @return the selected value of the setting.
	 */
	@SuppressWarnings("unchecked")
	private <T> T extractSetting(int index) {
		return ((SettingWrapperPane<T>)settingsListWrapper.getChildren().get(index)).getCurrentSetting();
	}
	
	/**
	 * 
	 * @return the current settings object we hold.
	 */
	public ApplicationSettings getNewSettings() {
		return this.settings;
	}
	
}
