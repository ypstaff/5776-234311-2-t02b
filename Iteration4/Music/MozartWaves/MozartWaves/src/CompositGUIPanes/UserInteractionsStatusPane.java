package CompositGUIPanes;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import MozartWaves.Project.Project;
import Pattern.Pattern;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;

public class UserInteractionsStatusPane extends VBox {

	private Label projectNameLabel;
	private Label patternNameLabel;
	private Label userActionMessageLabel;
	private Label timestampLabel;
	
	public final static String PROJECT_OPEN = "Successfully opened a project";
	public final static String PATTERN_CREATION = "Successfully created a pattern";
	public final static String COMPOSITE_PATTERN_SAVED = "Saved main composition";
	public final static String BASIC_PATTERN_SAVED = "Saved current basic pattern";
	public final static String PATTERN_CLEARED = "Cleared current pattern";
	public final static String PROJECT_CREATED = "Created new project";
	public final static String PROJECT_SAVED = "Saved project";
	public final static String EXPORTED_SUCCESSFULLY = "Exported successfully";
	public final static String PATTERN_LOAD = "Successfully loaded a pattern";
	public final static String PATTERN_SAVE = "Successfully saved a pattern";
	public static final String SAVE_BEFORE_UNDO_REDO = "Save the current pattern before\n"
			+"performig this action";
	
	
	public UserInteractionsStatusPane() {
		projectNameLabel = new Label();
		patternNameLabel = new Label();
		userActionMessageLabel = new Label();
		timestampLabel = new Label();
		this.getChildren().addAll(projectNameLabel,patternNameLabel,userActionMessageLabel,timestampLabel);
	}
	
	public void setProjectName(String name) {
		projectNameLabel.setText("Current Project : "+name);
	}
	
	public void setPatternName(String name) {
		patternNameLabel.setText("Current Pattern : "+name);
	}
	
	public void setUserActionLabel(String message) {
		userActionMessageLabel.setText(message);
	}
	
	public void updateTimeStamp() {
		timestampLabel.setText("Action performed at " +
				new SimpleDateFormat("HH:mm:ss").format(Calendar.getInstance().getTime()));
	}
	
	public void updateStatus(Project proj, Pattern pattern, String message) {
		setProjectName(proj.getName());
		setPatternName(pattern.getPatternName());
		setUserActionLabel(message);
		updateTimeStamp();
	}
	
	
	
	
	
}
