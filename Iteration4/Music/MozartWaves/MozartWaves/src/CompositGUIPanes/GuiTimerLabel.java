/**
 * 
 */
package CompositGUIPanes;

import java.util.Timer;
import java.util.TimerTask;

import javafx.application.Platform;
import javafx.scene.control.Label;

/**
 * @author RomanM
 * A runnable/stoppable label which shows seconds
 * Updated every 0.1 seconds
 */

public class GuiTimerLabel extends Label{
	
	Timer timer;
	LabelTimerTask timerTask;
	
	/**
	 * initializer
	 */
	public GuiTimerLabel(){
		timer = new Timer();
		
	}
	
	
	/**
	 * Start counting(from 0)
	 */
	public void startCounting(){
		timer = new Timer();
		timerTask=new LabelTimerTask(this);
	    timer.scheduleAtFixedRate(timerTask, 0, 100);
	}
	
	/**
	 * stops counting and resets to 0
	 */
	public void stopCounting(){
		timerTask.cancel();
		 timer.cancel();
         Platform.runLater(new LabelNuller(this));
         
	}

	/**
	 * @author RomanM
	 *	the task that runs and updates the label
	 */
	private class LabelTimerTask extends TimerTask{
		
		private Label label; 
		private float currentSecond= 0f;
		public LabelTimerTask(Label label){
			this.label = label;
		}
		
		@Override
        public void run() {
    	Platform.runLater(new Runnable() {
            @Override
            public void run() {
            	label.setText(String.format("%.1f", currentSecond)+ "s");
            	currentSecond = (float) (currentSecond +  0.1);
            }
        });
		
		}
	}
	
	/**
	 * @author RomanM
	 * Label nuller - sets the label to ""
	 */
	private class LabelNuller implements Runnable{
		private Label label;
		/**
		 * @param label
		 * Gets the label to null
		 */
		public LabelNuller(Label label){
			this.label = label;
		}
		@Override
		public void run() {
			// TODO Auto-generated method stub
			label.setText("");
		};
	}
	
}



