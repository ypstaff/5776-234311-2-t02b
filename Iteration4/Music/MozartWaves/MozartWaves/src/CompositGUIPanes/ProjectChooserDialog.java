package CompositGUIPanes;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import MozartWaves.GuiUtils;
import MozartWaves.Project.Project;
import Navigation.IterableGridPane;
import Navigation.IterableNodeGroups;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import utils.MarkingBoundsState.IterationType;


/***
 * Enables the user to switch between different projects, while also giving
 * a description of the patterns used in them.
 * @author Sergey
 *
 */
public class ProjectChooserDialog extends Stage{

	private IterableNodeGroups root;
	private int currentChosen;
	private List<String> projectFilePaths;
	private ListView<PatternDisplayPane> patternsInProject;
	private ObservableList<PatternDisplayPane> patternDisplayItems;
	private Label projectNameLabel;
	private File[] result;
	


	public ProjectChooserDialog(File[] result) {
		currentChosen=-1;
		this.result = result;
		BorderPane mainPane = new BorderPane();
		root = new IterableNodeGroups(mainPane,true);
		mainPane.setBottom(root.getIterationControlHBoxControlHBox());

		patternDisplayItems = FXCollections.observableArrayList();
		mainPane.setRight(buildProjectInfoDisplayPane());
		mainPane.setCenter(buildProjectSelectionPane());


		Scene s = new Scene(root.getParentPane());
		this.setScene(s);
		this.initModality(Modality.APPLICATION_MODAL);

		root.start();
		GuiUtils.setRoot(root);
	}

	/**
	 * building the pane which will display the pattern descriptions
	 * @return
	 */
	private Pane buildProjectInfoDisplayPane() {
		VBox projectInfoPane = new VBox();
		patternsInProject = new ListView<PatternDisplayPane>();
		patternsInProject.setItems(patternDisplayItems);
		projectNameLabel = new Label("Please select a project");
		projectInfoPane.getChildren().addAll(
				Arrays.asList(projectNameLabel,patternsInProject));
		return projectInfoPane;
	}

	/**
	 * builds the pane which will enable the user to select a project and later confirm 
	 * it's selection in order to switch to it.
	 * furthermore, the behavior of loading the pattern descriptions is defined here.
	 * @return
	 */
	private Pane buildProjectSelectionPane() {
		projectFilePaths = Project.allProjectPaths();
		List<Node> projectNames =  projectFilePaths.stream()
				.map((str)->{return new Label(str);}).collect(Collectors.toList());
		IterableGridPane projectNamesWrapper = new IterableGridPane(IterationType.ROWS,1,1);
		for(int i=0;i<projectNames.size();i++)
			projectNamesWrapper.add(projectNames.get(i),1, i+1);

		for(Node projectNameWrapper : projectNames) {
			projectNameWrapper.setOnMouseClicked(new EventHandler<MouseEvent>() {

				@Override
				public void handle(MouseEvent arg0) {
					Label eventTarget = (Label)arg0.getTarget();
					currentChosen = projectNamesWrapper.getChildren().indexOf(eventTarget);
					String projectName = eventTarget.textProperty().get();
					projectNameLabel.textProperty().set(projectName);
					try {
						Project selected = Project.buildFromName(projectName);
						patternDisplayItems.clear();
						List<PatternDisplayPane> displayPanes = 
								selected.getPatterns().stream()
								.map((pattern)->new PatternDisplayPane(pattern))
								.collect(Collectors.toList());
						patternDisplayItems.addAll(displayPanes);
					} catch (ClassNotFoundException | IOException e) {
						e.printStackTrace();
					}
				}
			});
		}

		root.addChildGroup(projectNames);

		HBox controlButtons = new HBox();
		Button confirm = new Button("Confirm");
		Button cancel = new Button("Cancel");
		controlButtons.getChildren().add(confirm);
		controlButtons.getChildren().add(cancel);

		root.addChildGroup(controlButtons.getChildren());


		cancel.setOnMouseClicked(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent arg0) {
				setAnswerAndClose(null);
			}
		});

		confirm.setOnMouseClicked(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent arg0) {
				if(currentChosen!=-1)
					setAnswerAndClose(new File(projectFilePaths.get(currentChosen)));
				else
					setAnswerAndClose(null);
			}
		});

		VBox centerContent = new VBox();
		centerContent.getChildren().add(projectNamesWrapper);
		centerContent.getChildren().add(controlButtons);
		return centerContent;
	}

	private void setAnswerAndClose(File f) {
		result[0] = f;
		close();
	}

}
