package CompositGUIPanes;

import java.awt.Color;
import java.awt.Dimension;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.net.URL;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.Arrays;

import javax.swing.JPanel;

import com.sun.pdfview.PDFFile;
import com.sun.pdfview.PagePanel;

import MozartWaves.GuiUtils;
import MozartWaves.Main;
import Navigation.IterableNodeGroups;
import javafx.embed.swing.SwingNode;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

public class UserManualDisplayDialog extends Stage {
	private PagePanel pagePanel;
	private PDFFile file;
	private BorderPane mainPane;
	private IterableNodeGroups root;
	private int currentPage;
	
	public UserManualDisplayDialog() {
		currentPage=1;
		mainPane = new BorderPane();
		root = new IterableNodeGroups(mainPane,true);
		
		initiateControlPanel();
		try {
			initPDFDisplayPane();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		Scene s = new Scene(root.getParentPane());
		this.setScene(s);
		this.setHeight(800);
		this.setWidth(700);
		this.setOnShown(new EventHandler<WindowEvent>() {

			@Override
			public void handle(WindowEvent arg0) {
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				pagePanel.showPage(file.getPage(currentPage));
			}
			
		});
		GuiUtils.setRoot(root);
	}
	
	private void initiateControlPanel() {
		Button nextPage = new Button("Next Page");
		nextPage.setOnMouseClicked(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				currentPage=currentPage+1;
				if(currentPage>file.getNumPages()) currentPage = 1;
				pagePanel.showPage(file.getPage(currentPage));
			}
			
		});
		Button prevPage = new Button("Previous Page");
		prevPage.setOnMouseClicked(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				currentPage=currentPage-1;
				if(currentPage<=0) currentPage = file.getNumPages();
				pagePanel.showPage(file.getPage(currentPage));
			}
			
		});
		Button close= new Button("Close");
		close.setOnMouseClicked(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				close();
			}
			
		});
		VBox allButtons  = new VBox();
		HBox buttonsPane = new HBox();
		buttonsPane.getChildren().addAll(Arrays.asList(nextPage,prevPage,close));
		allButtons.getChildren().add(buttonsPane);
		allButtons.getChildren().add(root.getIterationControlHBoxControlHBox());
		mainPane.setLeft(allButtons);
		root.addChildGroup(Arrays.asList(nextPage,prevPage));
		root.addChildGroup(close);
		
		
	}
	
	private void initPDFDisplayPane() throws IOException {
		URL userManualResourcePath = Main.class.getResource("/Docs/UserGuide.pdf");
		File pdfFile = new File(userManualResourcePath.getPath());
		RandomAccessFile raf = new RandomAccessFile(pdfFile, "r");
		FileChannel channel = raf.getChannel();
		ByteBuffer buff = channel.map(FileChannel.MapMode.READ_ONLY, 0, channel.size());
		file = new PDFFile(buff);
		pagePanel = new PagePanel();
		pagePanel.setBackground(Color.WHITE);
		pagePanel.setPreferredSize(new Dimension(600, 700));
		
		raf.close();
		JPanel swingPane = new JPanel();
		swingPane.add(pagePanel);	
		SwingNode swn = new SwingNode();
		swn.setContent(swingPane);
		mainPane.setCenter(swn);
	}
	
}
