package CompositGUIPanes;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;

import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.TilePane;

/**
 * 
 * @author Sergey
 *
 * @param <T> type of the values of the setting.
 * enables selection of a setting from a list of given values by 'next' and 'previous' buttons, 
 * and loading an explanation pane for this setting when 'explanation' button is clicked.
 * 
 * 
 */
public class SettingWrapperPane<T> extends Pane {
	
	private String settingName;
	private List<T> possibleValues;
	private Consumer<Void> explanationPaneLoaderCallback;
	private int currentIndex;
	private Label currentOptionLabel;
	
	private List<Node> controlButtons;
	
	public SettingWrapperPane(String settingName, List<T> values,T initialValue) {
			this(settingName,values, initialValue ,new Consumer<Void>() {

				@Override
				public void accept(Void t) {
				}
			},true);
	}
	
	public SettingWrapperPane(String settingName, List<T> values, T initialValue,
			Consumer<Void> explanationPaneSupplier) {
		this(settingName,values, initialValue ,explanationPaneSupplier ,false);
	
	}
	
	private SettingWrapperPane(String settingName, List<T> values, T initialValue,
			Consumer<Void> explanationPaneSupplier, boolean disableExplanationButton) {
		this.settingName = settingName;
		this.possibleValues = values;
		this.explanationPaneLoaderCallback = explanationPaneSupplier;
		this.currentIndex = values.indexOf(initialValue);
		controlButtons = new ArrayList<Node>();
		initiatePaneStructure(disableExplanationButton);
	}
	
	/**
	 * initiate the layout of this pane and the functionality of its elements.
	 */
	private void initiatePaneStructure(boolean disableExplanationButton) {
		currentOptionLabel = new Label(settingName+": "+possibleValues.get(currentIndex).toString());
		TilePane tilePane = new TilePane();
		tilePane.getChildren().add(currentOptionLabel); 
		
		HBox controlHBox = new HBox();
		tilePane.getChildren().add(controlHBox);
		List<Node> hboxChildren = controlHBox.getChildren();
		List<List<Node>> listsToUpdate = Arrays.asList(hboxChildren,controlButtons);
		
		Button next = initAndAddControlButton("Next", listsToUpdate);
		next.setOnMouseClicked(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent arg0) {
				moveOptionSelectionByDelta(1);
			}
			
		});
		Button previous = initAndAddControlButton("Previous", listsToUpdate);
		previous.setOnMouseClicked(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent arg0) {
				moveOptionSelectionByDelta(-1);
			}
		});
		Button explanation = initAndAddControlButton("Explanation",listsToUpdate);
		explanation.setOnMouseClicked(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent arg0) {
				explanationPaneLoaderCallback.accept((Void)null);
			}
		});
		explanation.setDisable(disableExplanationButton);
		this.getChildren().add(tilePane);
	}
	
	/**
	 * creates a button with the passed String as text, and adds it to the passed lists. 
	 * @param text
	 * @param targetLists
	 * @return
	 */
	private Button initAndAddControlButton(String text,List<List<Node>> targetLists) {
		Button $ = new Button(text);
		for(List<Node> list : targetLists) {
			list.add($);
		}
		return $;
	}
	
	/**
	 * returns the list of the 'next','previous','explanation' buttons.
	 * @return
	 */
	public List<Node> getControlButtons() {
		return this.controlButtons;
	}
	
	/**
	 * 
	 * @return current selected value for the setting which this wrapper pane holds.
	 */
	public T getCurrentSetting() {
		return possibleValues.get(currentIndex);
	}
	
	/**
	 * moves to the <i>'(current index)+delta'</i> value of the setting.
	 * @param delta - by how much do we want to change the current index in the list.
	 */
	private void moveOptionSelectionByDelta(int delta) {
		currentIndex=(currentIndex+delta);
		if(currentIndex >= possibleValues.size()) currentIndex=0;
		else if(currentIndex <0) currentIndex = possibleValues.size()-1;
		currentOptionLabel.setText(settingName+": "+possibleValues.get(currentIndex).toString());
	}
}
