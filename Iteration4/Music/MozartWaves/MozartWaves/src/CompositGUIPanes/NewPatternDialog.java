/**
 * 
 */
package CompositGUIPanes;

import MozartWaves.GuiUtils;
import MozartWaves.Pojos.Instrument;
import Navigation.IterableGridPane;
import Navigation.IterableNodeGroups;
import Navigation.IterationControlHBox;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import utils.MarkingBoundsState.IterationType;

/**
 * @author RomanM
 *	an extension of stage to display the new pattern dialog
 */

public class NewPatternDialog extends Stage {

	Instrument chosenInstrument;
	
	/**
	 * initializer
	 */
	public NewPatternDialog(){
		Stage stage=this;
		this.setWidth(300);
		this.setHeight(150);
		this.initStyle(StageStyle.DECORATED);
		BorderPane mainBp = new BorderPane();
		IterableNodeGroups root = new IterableNodeGroups(mainBp);
		Button okButton = new Button("Ok");
		Button cancelButton = new Button("Cancel");
		HBox buttonsBox= new HBox();
		buttonsBox.getChildren().add(okButton);
		buttonsBox.getChildren().add(cancelButton);
		mainBp.setRight(buttonsBox);
		okButton.setOnMouseClicked(new EventHandler<Event>() {
			@Override
			public void handle(Event event) {
				stage.close();
			}
		});
		cancelButton.setOnMouseClicked(new EventHandler<Event>() {
			@Override
			public void handle(Event event) {
				chosenInstrument=null;
				stage.close();
			}
		});
		GridPane instrumentsGp = new IterableGridPane(IterationType.ROWS,1);
		for(int i=0 ; i< Instrument.values().length; i++){
			Button button = new Button(Instrument.values()[i].toString());
			final int j=i;
			if(i==0){
				chosenInstrument=Instrument.values()[i];
				button.setStyle(GuiUtils.getColorString());
			}
			button.setOnMouseClicked(new EventHandler<Event>() {
				@Override
				public void handle(Event event) {
					for(Node button2: instrumentsGp.getChildren()){
						if(!button2.equals(button)){
							((Button)button2).setStyle(GuiUtils.getUncoloredString());
						}else{
							button2.setStyle(GuiUtils.getColorString());
						}
					}
					chosenInstrument=Instrument.values()[j];
				}
			});
			instrumentsGp.add(button, 1, i+1);
		}
		mainBp.setLeft(instrumentsGp);
		root.addChildGroup(instrumentsGp.getChildren());
		root.addChildGroup(buttonsBox.getChildren());
		mainBp.setBottom(new IterationControlHBox(root));
		Scene scene = new Scene(root.getParentPane());
		this.setScene(scene);
		this.setTitle("New Pattern");
		root.start();
		this.initModality(Modality.APPLICATION_MODAL);
		GuiUtils.setRoot(root);
	}
	
	/**
	 * @return the instrument chosen by the dialog (Null if cancel)
	 */
	public Instrument getChosenInstrument(){
		return chosenInstrument;
	}
	
	
}
