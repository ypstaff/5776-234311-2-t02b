package CompositGUIPanes;

import MozartWaves.Pojos.Instrument;
import Pattern.Pattern;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;

public class PatternDisplayPane extends VBox {
	
	private Pattern pattern;
	
	private static final String NAME = "Pattern name: ";
	private static final String INSTRUMENT_USED = "Instrument used: ";
	private static final String STYLE = "-fx-border-color : black; -fx-background-color : white;";
	
	public PatternDisplayPane(Pattern pattern) {
		super.setStyle(super.getStyle()+STYLE);
		this.pattern=pattern;
		super.getChildren().add(new Label(NAME+pattern.getPatternName()));
		//super.getChildren().add(new Label(new Long(pattern.getTimeLength()).toString()));
		Instrument usedInstrument = pattern.getInstrument();
		String instrumentsDetails = INSTRUMENT_USED+usedInstrument;
		super.getChildren().add(new Label(instrumentsDetails));
		
	}
	
	public Pattern getPattern() {
		return this.pattern;
	}
}
