/**
 * 
 */
package CompositGUIPanes;



import javax.sound.sampled.LineEvent;
import javax.sound.sampled.LineListener;
import javax.sound.sampled.UnsupportedAudioFileException;

import MozartWaves.GuiUtils;
import Navigation.IterableNodeGroups;
import Playable.Playable;
import javafx.application.Platform;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 * @author RomanM
 *
 */
public class ClipPlayingDialog extends Stage{


		private IterableNodeGroups root;

		public ClipPlayingDialog() {
			GuiTimerLabel timerLabel = new GuiTimerLabel();
			GuiUtils.setTimerLabel(timerLabel);
			BorderPane mainPane = new BorderPane();
			root = new IterableNodeGroups(mainPane);
			mainPane.setBottom(root.getIterationControlHBoxControlHBox());
			Button stopButton = new Button("Stop");
			stopButton.setOnMouseClicked(new EventHandler<Event>() {
				@Override
				public void handle(Event event) {
					try {
						Playable.stopClip();
					} catch (UnsupportedAudioFileException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			});
			mainPane.setRight(stopButton);
			mainPane.setLeft(timerLabel);
			root.addChildGroup(stopButton);
			Scene s = new Scene(root.getParentPane());
			this.setScene(s);
			this.initModality(Modality.APPLICATION_MODAL);
			root.start();
			Playable.addLineListener(new CloseListener(this));
			GuiUtils.setRoot(root);
		}
		
		@Override
		public void showAndWait(){
			super.showAndWait();
		}
		
		private class CloseListener implements LineListener{

			
			private Stage stage;

			CloseListener(Stage stage){
				this.stage =stage;
			}
			
			@Override
			public void update(LineEvent event) {
				if(event.getType()==LineEvent.Type.CLOSE)
					Platform.runLater(new Runnable() {
						@Override
						public void run() {
							stage.close();
						}
					});
		}
		
		}
}



