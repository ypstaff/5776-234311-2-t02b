package CompositGUIPanes;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.BiPredicate;
import java.util.stream.Collectors;

import javax.sound.sampled.UnsupportedAudioFileException;

import EventHandlers.MusicalButtonHandler;
import EventHandlers.PatternGridButtonHandler;
import MozartWaves.GuiUtils;
import MozartWaves.Main;
import MozartWaves.Pojos.IntWrapper;
import Navigation.IterableGridPane;
import Pattern.CompositePatternHolder;
import Pattern.Pattern;
import Playable.InstrumentSound;
import Playable.Playable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import utils.MarkingBoundsState.IterationType;


public class PlayableGridPane extends IterableGridPane {

	public PlayableGridPane(IterationType iterationType, int columnBlockSize,
			int rowBlockSize) {
		super(iterationType, columnBlockSize, rowBlockSize,1,2);
	}

	public PlayableGridPane(IterationType iterationType, int blockSize) {
		super(iterationType,blockSize,blockSize,1,2);
	}
	
	public PlayableGridPane fillGridWithSounds(List<InstrumentSound> sounds,Pattern currentPattern,IntWrapper soundTimeLength) {
		BiConsumer<Playable,Integer> consumerAdd= new BiConsumer<Playable,Integer>(){
			@Override
			public void accept(Playable playable, Integer u) {
				currentPattern.addPlayable((InstrumentSound)playable, u);
			}
		};
		BiConsumer<Playable,Integer> consumerRemove= new BiConsumer<Playable,Integer>(){
			@Override
			public void accept(Playable playable, Integer u) {
				currentPattern.removePlayable((InstrumentSound)playable, u);
			}
		};
		BiPredicate<Playable,Integer> predicateExisted= new BiPredicate<Playable,Integer>(){
			@Override
			public boolean test(Playable t, Integer u) {
				return currentPattern.contains((InstrumentSound)t, u);
			}
		};
		fillGrid(sounds, Main.patternTimeLength , consumerAdd, consumerRemove,
				predicateExisted, soundTimeLength, false);
		return this;
	}
	
	public PlayableGridPane fillGridWithPatterns(List<Pattern> patterns,IntWrapper soundTimeLength,
			CompositePatternHolder composite,int offset) {
		List<Pattern> sortedPatterns = patterns.stream().
				sorted(Pattern.makePatternComparator()).collect(Collectors.toList());
		BiConsumer<Playable,Integer> consumerAdd= new BiConsumer<Playable,Integer>(){
			@Override
			public void accept(Playable playable, Integer u) {
				try {
					composite.addPlayable((Pattern)playable, u);
				} catch (IOException | UnsupportedAudioFileException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		};
		BiConsumer<Playable,Integer> consumerRemove= new BiConsumer<Playable,Integer>(){
			@Override
			public void accept(Playable playable, Integer u) {
				composite.removePlayable((Pattern)playable, u);
			}
		};
		BiPredicate<Playable,Integer> predicateExisted= new BiPredicate<Playable,Integer>(){
			@Override
			public boolean test(Playable t, Integer u) {
				return composite.exists((Pattern)t, u);
			}
		};
		fillGrid(sortedPatterns, 4000*16,consumerAdd, consumerRemove, 
				predicateExisted, soundTimeLength, true,offset);
		return this;
	}

	public void fillGrid(List<? extends Playable> playables, int gridTimeLength,
			BiConsumer<Playable, Integer> biConsumerAdd,
			BiConsumer<Playable, Integer> biConsumerRemove,
			BiPredicate<Playable,Integer> biPredicateExisted,
			IntWrapper soundTimeLength, boolean projectGridView) {
		fillGridFromOffset(playables,gridTimeLength,biConsumerAdd,
				biConsumerRemove,biPredicateExisted,soundTimeLength,projectGridView,0);
	}
	
	public void fillGrid(List<? extends Playable> playables, int gridTimeLength,
			BiConsumer<Playable, Integer> biConsumerAdd,
			BiConsumer<Playable, Integer> biConsumerRemove,
			BiPredicate<Playable,Integer> biPredicateExisted,
			IntWrapper soundTimeLength, boolean projectGridView,int offset) {
		fillGridFromOffset(playables,gridTimeLength,biConsumerAdd,
				biConsumerRemove,biPredicateExisted,soundTimeLength,projectGridView,offset);
		
	}
	
	private void fillGridFromOffset(List<? extends Playable> playables, int gridTimeLength,
			BiConsumer<Playable, Integer> biConsumerAdd,
			BiConsumer<Playable, Integer> biConsumerRemove,
			BiPredicate<Playable,Integer> biPredicateExisted,
			IntWrapper soundTimeLength, boolean projectGridView,int offset) {
		super.getChildren().clear();
		ArrayList<Button> plButtons = new ArrayList<Button>();
		for(int i=0;i<playables.size();i++) {
			Button $ = new Button(playables.get(i).toString());
			$.setPrefSize(150,40);
			plButtons.add($);
			Playable clonedPlayable = playables.get(i);
			if(!projectGridView){ //So that cloneable doesnt change
				clonedPlayable = playables.get(i).clone();
				clonedPlayable.setTimeLength(250);
			}
			$.setOnMouseClicked(new MusicalButtonHandler(clonedPlayable));
			this.add($, 1, i+2);
		}
		int granularity= projectGridView ? 4000 : 250;
		
		for(int i=2; i<=gridTimeLength/granularity+1; i++){
			Double timeIdex = (double)((double)(i-2+offset)*(double)((double)granularity/(double)1000));
			Label label = new Label("    "+  Double.toString( timeIdex)+"s");
			label.setStyle("-fx-text-fill: Green;"+label.getStyle());
			this.add(label, i, 1);
		}
		
		GuiUtils.setPlayButtons(plButtons);
		for(int i=playables.size()-1;i>=0;i--) {
			for(int j=gridTimeLength/granularity; j>=1; j--){
				MainGridButton button = new MainGridButton();
				button.setPrefSize(50,40);
				List<MainGridButton> nextButtonsToUpdate = new ArrayList<MainGridButton>();
				nextButtonsToUpdate.add(button);
				if(!projectGridView){
					for(int k=j+2; k<=gridTimeLength/granularity+1; k++)
						nextButtonsToUpdate.add(((MainGridButton)GuiUtils.getNodeByRowColumnIndex(i+2, k,this)));
				}
				Playable clonedPlayable = playables.get(i).clone();
				if(projectGridView){ //So that cloneable doesnt change
					clonedPlayable=playables.get(i);
				}
				button.setOnMouseClicked(new PatternGridButtonHandler(clonedPlayable, j-1+offset, 
						biPredicateExisted.test(clonedPlayable, j-1+offset),biConsumerAdd,
						biConsumerRemove,soundTimeLength, 
						nextButtonsToUpdate, projectGridView));
				this.add(button, j+1, i+2);
			}
		}
	}

}
