/**
 * 
 */
package CompositGUIPanes;
import javafx.scene.control.Button;
/**
 * @author RomanM
 *
 */
public class MainGridButton extends Button{
	
	private int numberOfTimesTaken;
	
	
	
	public MainGridButton(String name) {
		super(name);
		numberOfTimesTaken=0;
	}
	
	public MainGridButton() {
		super();
		numberOfTimesTaken=0;
	}

	public void increaseTaken(){
		numberOfTimesTaken++;
	}
	
	public void decreaseTaken(){
		numberOfTimesTaken--;
	}
	
	public int getTaken(){
		return numberOfTimesTaken;
		
	}

}
