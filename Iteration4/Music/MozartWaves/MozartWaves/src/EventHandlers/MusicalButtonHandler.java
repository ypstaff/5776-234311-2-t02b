package EventHandlers;

import javax.sound.sampled.UnsupportedAudioFileException;

import Playable.Playable;
import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;

/**
 * @author RomanM
 * A handler Playable buttons(on press the "sound" of the button will play)
 */
public class MusicalButtonHandler implements EventHandler<MouseEvent> {

	private Playable playable;
	
	/**
	 * @param playable - to play when the mouse event is fired
	 */
	public MusicalButtonHandler(Playable playable) {
		this.playable = playable;
	}
	
	@Override
	public void handle(MouseEvent event) {
	     try {
	    	 playable.play();
		} catch (UnsupportedAudioFileException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
