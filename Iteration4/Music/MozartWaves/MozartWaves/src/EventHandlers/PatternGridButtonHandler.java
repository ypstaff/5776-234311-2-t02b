/**
 * 
 */
package EventHandlers;

import java.util.List;
import java.util.function.BiConsumer;

import CompositGUIPanes.MainGridButton;
import MozartWaves.GuiUtils;
import MozartWaves.Pojos.IntWrapper;
import Playable.Playable;
import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;

/**
 * @author RomanM
 *	Main Grid button handler - for inserting/removing playables from current pattern
 */


public class PatternGridButtonHandler  implements EventHandler<MouseEvent> {


	int timeIndex;
	IntWrapper soundTimeLengthWrapper;
	boolean taken=false;
	private Playable playable;
	private BiConsumer<Playable, Integer> biConsumerRemove;
	private BiConsumer<Playable, Integer> biConsumerAdd;
	private boolean projectGridView;
	private List<MainGridButton> buttons;
	private long prevTimeLength;
	
	/**
	 * @param pl - the Playable assigned to the button
	 * @param i - the index in which to play the sound
	 * @param existed - true if the button need to be initialized as set
	 * @param biConsumerAdd - function for adding the playable
	 * @param biConsumerRemove - function for removing the playable
	 * @param soundTimeLength - the length wrapper(changes in the main GUI)
	 * @param buttons -the buttons in the same row, to update if needed(for coloring)
	 * @param projectGridView - true if in composition project view context
	 */
	public PatternGridButtonHandler(Playable pl, int i, boolean existed, 
			BiConsumer<Playable, Integer> biConsumerAdd,
			BiConsumer<Playable, Integer> biConsumerRemove, IntWrapper soundTimeLength, 
			List<MainGridButton> buttons,
			boolean projectGridView) {
		this.buttons=buttons;
		this.playable=pl;
		this.timeIndex=i;
		this.biConsumerAdd=biConsumerAdd;
		this.biConsumerRemove=biConsumerRemove;
		this.soundTimeLengthWrapper=soundTimeLength;
		this.projectGridView=projectGridView;
		if(existed)
			setButtonClicked(playable.getTimeLength());
	}


	@Override
	public void handle(MouseEvent event) {
		   //TODO: don't do the next!~!!!!!!
		
			 
		   if(!taken){
			   long timeLength=playable.getTimeLength();
			   if(!projectGridView){
				   timeLength = soundTimeLengthWrapper.value;
				  (playable).setTimeLength(timeLength);
				  
			   }
			   prevTimeLength=timeLength;
			   setButtonClicked(timeLength);
			   biConsumerAdd.accept(playable, timeIndex);
		   }else{
			   for(int i=0; i<Math.min(prevTimeLength/250, buttons.size()); i++){
				   buttons.get(i).decreaseTaken();
				   if(buttons.get(i).getTaken()==0){
					   buttons.get(i).setStyle(GuiUtils.getUncoloredString());
					   buttons.get(i).setTextFill(Color.TRANSPARENT);
				   }
				}
			   buttons.get(0).setTextFill(Color.TRANSPARENT);
			   playable.setTimeLength(prevTimeLength);
			   biConsumerRemove.accept(playable, timeIndex);
			   taken= false;
		   }
	}
	
	/**
	 * @param timeLength
	 * set the button of the handler and after it to the user-chosen  color id needed
	 */
	private void setButtonClicked(long timeLength){
		prevTimeLength=timeLength;
		for(int i=0; i<Math.min(timeLength/250, buttons.size()); i++){
			taken=true;
			buttons.get(i).setStyle(GuiUtils.getColorString());
			
			buttons.get(i).increaseTaken();
		}
		buttons.get(0).setTextFill(Color.BLACK);
		String buttonText="";
		if(projectGridView){
			buttonText="4s";
		}else{
			buttonText=(double)((double)timeLength/(double)1000) + "s";
		}
		buttons.get(0).setText(buttonText);
	}
	
}
