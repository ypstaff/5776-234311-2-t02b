/**
 * 
 */
package EventHandlers;

import MozartWaves.Main;
import MozartWaves.Pojos.IntWrapper;
import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;

/**
 * @author RomanM
 * Handler for octave next/prev buttons
 */
public class ChangeOctaveButtonHandler implements EventHandler<MouseEvent> {

	
	IntWrapper pianoOctave;
	int changeTo;
	Main mainProgram;
	
	public ChangeOctaveButtonHandler(Main mainProgram, int changeTo, IntWrapper pianoOctave){
		this.pianoOctave=pianoOctave;
		this.changeTo=changeTo;
		this.mainProgram=mainProgram;
	}
	
	
	@Override
	public void handle(MouseEvent arg0) {
		pianoOctave.value+=changeTo;
		if(pianoOctave.value<1)
			pianoOctave.value=1;
		else if(pianoOctave.value>6)
			pianoOctave.value=6;
		else
			mainProgram.refreshGrid();
	}
}
