package EventHandlers;


import java.util.List;
import java.util.function.Consumer;

import MozartWaves.GuiUtils;
import MozartWaves.Pojos.IntWrapper;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;

/**
 * @author RomanM
 *	sound time button handler
 */
public class SoundTimeButtonHandler  implements EventHandler<MouseEvent> {

	int index;
	IntWrapper soundTimeWrapper;
	private List<Button> buttons;
	private Consumer<Void> consumer;
	/**
	 * 
	 * @param buttons - all of the sound time buttons to update when clicked
	 * @param myIndex - current handler index in buttons
	 * @param soundTimeLength the length sound wrapper to set when clicked
	 * @param consumer 
	 */
	public SoundTimeButtonHandler(List<Button> buttons,int myIndex,IntWrapper soundTimeLength,
			Consumer<Void> consumer){
		this.index=myIndex;
		this.buttons = buttons;
		this.soundTimeWrapper = soundTimeLength;
		this.consumer=consumer;
	}
	
	@Override
	public void handle(MouseEvent event) {
		consumer.accept(null);
			for(Button b : buttons)
				b.setStyle(GuiUtils.getUncoloredString());
			buttons.get(index).setStyle(GuiUtils.getColorString());
			soundTimeWrapper.value=(int)(Double.parseDouble(
					buttons.get(index).getText().replace("s", ""))*1000); // To milli
	}
}
