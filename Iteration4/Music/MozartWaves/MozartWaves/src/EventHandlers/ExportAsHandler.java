/**
 * 
 */
package EventHandlers;

import java.io.IOException;
import java.util.function.Supplier;

import javax.sound.sampled.UnsupportedAudioFileException;

import AISManaging.AISManager;
import MozartWaves.Pojos.SoundType;
import MozartWaves.Project.Project;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

/**
 * @author RomanM
 *
 */
public class ExportAsHandler implements EventHandler<ActionEvent> {
	
	private SoundType type;
	private Supplier<Project> projectSupplier;


	public ExportAsHandler(SoundType type, Supplier<Project> projectSupplier){
		this.type=type;
		this.projectSupplier=projectSupplier;
	}
	
	
	/* (non-Javadoc)
	 * @see javafx.event.EventHandler#handle(javafx.event.Event)
	 */
	@Override
	public void handle(ActionEvent arg0) {
		Project currentProject =projectSupplier.get();
		String name ="";
		switch(type){
		case WAV:
			name="Composition.wav";
		}
		try {
			AISManager.saveAudioWav(currentProject.getCompositePattern().getAudioInputStream(),
					"/Compositions/", currentProject.getName()+"_"+name);
		} catch (IOException | UnsupportedAudioFileException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
