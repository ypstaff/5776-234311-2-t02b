/**
 * 
 */
package EventHandlers;

import java.util.function.Consumer;

import CompositGUIPanes.NewPatternDialog;
import MozartWaves.GuiUtils;
import MozartWaves.Pojos.Instrument;
import Navigation.IterableNodeGroups;
import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;

/**
 * @author RomanM
 *	New pattern button handler
 */
public class NewPatternButtonHandler implements EventHandler<MouseEvent> {

	Consumer<Instrument> consumer;
	private IterableNodeGroups root;

	public NewPatternButtonHandler(Consumer<Instrument> cons, IterableNodeGroups root){
		this.consumer = cons;
		this.root=root;
	}
	
	@Override
	public void handle(MouseEvent event) {
		NewPatternDialog newPatDialog = new NewPatternDialog();
		newPatDialog.showAndWait();
		Instrument tmp =newPatDialog.getChosenInstrument();
		GuiUtils.setRoot(root);
		if(tmp!=null){
			consumer.accept(tmp);
		}
	}

}
