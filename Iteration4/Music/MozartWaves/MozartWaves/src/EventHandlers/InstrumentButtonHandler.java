/**
 * 
 */
package EventHandlers;

import MozartWaves.GuiUtils;
import MozartWaves.Pojos.Instrument;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;

/**
 * @author RomanM
 *  Instrument choosing button for new pattern dialog
 */
public class InstrumentButtonHandler implements EventHandler<MouseEvent> {

	Instrument instrument;
	GridPane gpToUpdate;
	Button eventButton;
	
	/**
	 * @param ins - Instrument chosen by this button
	 * @param gpToUpdate - Grid Pane to update 
	 * @param button - to update in the GUI
	 */
	public InstrumentButtonHandler( Instrument ins, GridPane gpToUpdate, Button button){
		this.instrument =ins;
		this.gpToUpdate=gpToUpdate;
		this.eventButton=button;
	}
	
	@Override
	public void handle(MouseEvent arg0) {
		for(Node button: gpToUpdate.getChildren()){
			if(!button.equals(eventButton)){
				((Button)button).setStyle(GuiUtils.getUncoloredString());
			}else{
				button.setStyle(GuiUtils.getColorString());
			}
		}
	}

	

}
