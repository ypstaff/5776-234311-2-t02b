package Pattern;

import java.io.IOException;
import java.io.ObjectStreamException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.UnsupportedAudioFileException;

import AISManaging.AISManager;
import AISManaging.AisGC;
import MozartWaves.Pojos.Instrument;
import Playable.Playable;
import Playable.Sound;



enum PatternLength{SIXTEEN, EIGHT, FOUR};

/**
 * @author RomanM
 *
 */
public class Pattern extends Playable implements Serializable{

	private static final long serialVersionUID = 3414409082635983664L;
	private static final String PATTERN_TMP_FOLDER = "/PatternTmp/";
	private int PatternId;
	private SoundSet[] m_soundSetArray;
	private AudioInputStream[] m_AudioInputStreamArray;
	private Set<Long> columnIndicesToUpdate;
	private boolean mainWavNeedsUpdate = true, isComposite = false,changedSinceLastSave;
	private AudioInputStream m_currentAudioInputStream;
	private int granularity;
	private Instrument m_instrument;
	
	/**
	 * @param l - length in Milliseconds
	 * @param id 
	 * @param granularity - length in Milliseconds of each colmun
	 * @param   ins - Instrument
	 * @return new Pattern
	 */
	public Pattern(long l, int id, int granularity, Instrument ins){
		this(l,id, granularity);
		m_instrument=ins;
	}
	
	/**
	 * @param l - length in Milliseconds
	 * @param id 
	 * @param granularity - length in Milliseconds of each colmun
	 * @return
	 */
	public Pattern(long l, int id, int granularity){
		this.granularity=granularity;
		m_AudioInputStreamArray= new AudioInputStream[(int)(l/granularity)];
		columnIndicesToUpdate=new HashSet<Long>();
		PatternId = id ;
		timeLength = l; //MilliSecs
		changedSinceLastSave = false;
		initSoundListArray();
	}
	
	/**
	 * @param plSound -Sound
	 * @param l - sound column index
	 * adds the sound to the pattern at the index l
	 */
	public void addPlayable(Sound plSound, long l){
		changedSinceLastSave = true;
		mainWavNeedsUpdate = true;
		m_soundSetArray[(int)l].getSoundSet().add(plSound);
		columnIndicesToUpdate.add(l);
	}
	
	/**
	 * @param plSound -Sound
	 * @param l - sound column index
	 * removes the sound to the pattern at the index l
	 */
	public void removePlayable(Sound plSound, long l){
		changedSinceLastSave = true;
		mainWavNeedsUpdate= true;
		m_soundSetArray[(int)l].getSoundSet().remove(plSound);
		columnIndicesToUpdate.add(l);
	}
	
	/**
	* @param plSound -Sound
	 * @param index - sound column index
	 * @return true if plSound exists at "index"
	 */
	public boolean contains(Sound plSound, long index){
		SoundSet[] soundList = getPlayableSoundSet();
		if(index>=soundList.length){
			return false;
		}
		for(long t=granularity; t<=4000; t*=2){
			plSound.setTimeLength(t);
			if(soundList[(int)index].getSoundSet().contains(plSound))
				return true;
		}
		return false;
	}
	
	/**
	 * @return the audioInputStream of the pattern
	 */
	@Override
	public AudioInputStream getAudioInputStream() throws IOException, UnsupportedAudioFileException {
		updatePatternAndSave();
		closeAllStreams(null);
		return loadPatternFromWav();
	}
	
	/**
	 * @return the pattern Name
	 */
	public String getPatternName() {
		return "Pattern_" + getID();
	}
	
	/**
	 * @return pattern path relative to the main program
	 */
	public String getPatternRelativePath(){
		return PATTERN_TMP_FOLDER+ getPatternName() + ".wav";
	}
	
	/**
	 * @return the Playable sound set of the pattern
	 */
	public SoundSet[] getPlayableSoundSet(){
		return m_soundSetArray;
	}
	
	/**
	 * @return the instrument of the pattern
	 */
	public Instrument getInstrument(){
		return m_instrument;
	}

	/** Updates column Streams and saves the columnStreams and main stream on disk
	 * @throws IOException
	 * @throws UnsupportedAudioFileException
	 */
	public void updatePatternAndSave() throws IOException, UnsupportedAudioFileException{
		if(mainWavNeedsUpdate){
			updateAudioInputStreamList();
			mainWavNeedsUpdate =! mainWavNeedsUpdate;
			List<AudioInputStream> aisList= getAllColumnStreams();
		  	if(isComposite)
				m_currentAudioInputStream = AISManager.concatinateAIS(aisList);
			else
				m_currentAudioInputStream = AISManager.mergeAIS(aisList);
			if(isEmpty()){
				m_currentAudioInputStream =  AISManager.getSilence(timeLength);
			}
			savePatternToWav();
			closeAllStreams(aisList);	
		}
	}
	
	/**
	 * Sets that all Streams need an update on disk(Column and main )
	 */
	public void setAllStreamsNeedsUpdate(){
		mainWavNeedsUpdate= true;
		setColumnStreamsNeedUpdate();
	}
	
	/**
	 *  Sets that all column streams need an update on disk
	 */
	public void setColumnStreamsNeedUpdate(){
		for(long i=0; i <timeLength/granularity; i++)
			columnIndicesToUpdate.add(i);
	}
	

	/**
	 * @param l - length in Milliseconds
	 * Changes length of the pattern
	 */
	public void changeLength(long l) {
		this.timeLength=l;
		m_AudioInputStreamArray= new AudioInputStream[(int)(l/granularity)];
		SoundSet[] tmp = this.m_soundSetArray;
		initSoundListArray();
		for(int i=0; i< Math.min(m_soundSetArray.length, tmp.length); i++ ){
			m_soundSetArray[i]= tmp[i];
		}
		setAllStreamsNeedsUpdate();
	}
	
	/** @return true - if pattern modified since last save*/
	public boolean isChanged() {
		return changedSinceLastSave;
	}

	/**indicate that the pattern was not changed since it was saved.*/
	public void setUpdated() {
		changedSinceLastSave=false;
	}
	
	
	//----------------------- Overridings--------------------------------
	@Override
	public int getID() {
		return this.PatternId;
	}
	
	@Override
	public Playable clone() {
		Pattern $ = new Pattern(this.timeLength, this.PatternId, granularity);
		for(Long col : columnIndicesToUpdate) {
			$.columnIndicesToUpdate.add(col);
		}
		$.granularity = this.granularity;
		$.m_AudioInputStreamArray = this.m_AudioInputStreamArray.clone();
		$.mainWavNeedsUpdate = this.mainWavNeedsUpdate;
		$.m_soundSetArray = this.m_soundSetArray.clone();
		$.m_currentAudioInputStream = m_currentAudioInputStream; //TODO:change this to actually clone?
		return $;
	}
	
	@Override
	public String toString() {
		return this.getPatternName()+"\n"+this.getInstrument();
	}
	
	
	/**
	 * @param comp
	 * sets if the pattern is a composite pattern
	 */
	public void setComposite(boolean comp){
		this.isComposite=comp;
	}
	
	/**
	 * @return if the pattern is empty from sounds
	 */
	private boolean isEmpty(){
		for(SoundSet $: m_soundSetArray){
			if(!$.getSoundSet().isEmpty())
				return false;
		}
		return true;
	}
	
	/** inits the soundListArray */
	private void initSoundListArray(){
		m_soundSetArray = new SoundSet[(int)timeLength/granularity];
		for(int i=0; i<m_soundSetArray.length; i++){
			m_soundSetArray[i]= new SoundSet();
		}
		
	}
	
	//------------------------ Streams Managing -------------------------------------------------------
	

	/**Clear column streams from disk*/
	public void clearColumnStreamsData(){
		setColumnStreamsNeedUpdate();
		for(int i=0; i<m_AudioInputStreamArray.length; i++){
			if(m_AudioInputStreamArray[i]!=null){
				AISManager.deleteSounds(PATTERN_TMP_FOLDER+ getPatternName() +"_Stream_"+i+".wav");
			}
		}
	}
	
	/** Clear main stream from disk */
	public void clearMainStreamData(){
		mainWavNeedsUpdate= true;
		if(m_currentAudioInputStream!=null)
			AISManager.deleteSounds(PATTERN_TMP_FOLDER+ getPatternName() +".wav");
	}
	
	/** Clear All streams from disk(Main and columns) */
	public void clearAllSreamsData(){
		clearColumnStreamsData();
		clearMainStreamData();
	}
	
	/**
	 * @throws IOException
	 * @throws UnsupportedAudioFileException
	 */
	private void updateAudioInputStreamList() throws IOException, UnsupportedAudioFileException{
		for(int i=0; i<timeLength/granularity; i++){
			if(!columnIndicesToUpdate.contains((long)i)){
				continue;
			}
			
			if( m_soundSetArray[i].getSoundSet().isEmpty()){
				if(m_AudioInputStreamArray[i]!=null){
					m_AudioInputStreamArray[i].close();
				}
				m_AudioInputStreamArray[i]=null;
			}
			
			List<AudioInputStream> $= new ArrayList<AudioInputStream>();
			if(isComposite){
				for(Sound sound: m_soundSetArray[i].getSoundSet())
					 $.add(sound.getAudioInputStream());
			}else{
				for(Sound sound: m_soundSetArray[i].getSoundSet()){
					AudioInputStream concatinatedStream;
					AudioInputStream prefixAIS =  AISManager.getSilence(i*granularity);
					AudioInputStream soundAIS = sound.getAudioInputStream();
					AisGC.addStreamToFlush(prefixAIS);
					AisGC.addStreamToFlush(soundAIS);
					if(sound.getTimeLength() +i*granularity>=timeLength){
						long tmp= sound.getTimeLength();
						sound.setTimeLength(timeLength-i*granularity);
						soundAIS = sound.getAudioInputStream();
						AisGC.addStreamToFlush(soundAIS);
						concatinatedStream = AISManager.concatinateAIS(prefixAIS, soundAIS);
						AisGC.addStreamToFlush(concatinatedStream);
						sound.setTimeLength(tmp);
						$.add(concatinatedStream);
						continue;
					}
					AudioInputStream suffixAIS =  AISManager.getSilence(timeLength-i*granularity-sound.getTimeLength());
					concatinatedStream = AISManager.
			        		concatinateAIS(prefixAIS, soundAIS);
					AisGC.addStreamToFlush(concatinatedStream);
			        concatinatedStream = AISManager.
			        		concatinateAIS(concatinatedStream, suffixAIS);
			        AisGC.addStreamToFlush(concatinatedStream);
			        AisGC.addStreamToFlush(suffixAIS);
			        $.add(concatinatedStream);
				}
			}
			m_AudioInputStreamArray[i]=AISManager.mergeAIS($);
			$.clear();
			saveStream(i);
		}
		columnIndicesToUpdate=new HashSet<Long>();
	}
	
	/**
	 * @return
	 * @throws IOException
	 * @throws UnsupportedAudioFileException
	 */
	private List<AudioInputStream> getAllColumnStreams() throws IOException, UnsupportedAudioFileException{
		 List<AudioInputStream> aisList = new ArrayList<AudioInputStream>();
		 for(int i=0; i<m_soundSetArray.length; i++){
			if(!m_soundSetArray[i].getSoundSet().isEmpty())
				aisList.add(loadStream(i));
			else if(isComposite)
				 aisList.add(AISManager.getSilence(granularity));
		 }
		return aisList;
	}
	
	/**
	 * @param i
	 * @throws IOException
	 */
	private void saveStream(int i) throws IOException{
		if(m_AudioInputStreamArray[i]==null){
			return;
		}
		AISManager.saveAudioWav(m_AudioInputStreamArray[i], PATTERN_TMP_FOLDER, getPatternName() +"_Stream_"+i+".wav");
		m_AudioInputStreamArray[i].close();
	}
	
	/**
	 * @param i
	 * @return
	 * @throws IOException
	 * @throws UnsupportedAudioFileException
	 */
	private AudioInputStream loadStream(int i) throws IOException, UnsupportedAudioFileException{
				return AISManager.loadAudio( PATTERN_TMP_FOLDER+ getPatternName() +"_Stream_"+i+".wav", -999);
	}
	
	/**
	 * @return
	 * @throws IOException
	 * @throws UnsupportedAudioFileException
	 */
	private AudioInputStream loadPatternFromWav() throws IOException, UnsupportedAudioFileException{
		return AISManager.loadAudio(getPatternRelativePath(), timeLength);
	}
	
	/**
	 * @throws IOException
	 * @throws UnsupportedAudioFileException
	 */
	private void savePatternToWav() throws IOException, UnsupportedAudioFileException{
		AISManager.saveAudioWav(m_currentAudioInputStream, PATTERN_TMP_FOLDER, getPatternName() + ".wav");
	}
	
	/**
	 * @param aisList
	 * @throws IOException
	 */
	private void closeAllStreams(List<AudioInputStream> aisList) throws IOException{
		if(m_currentAudioInputStream!=null){
			m_currentAudioInputStream.close();
		}
		for(int i=0; i<m_AudioInputStreamArray.length; i++ ){
			if(m_AudioInputStreamArray[i]!=null){
				m_AudioInputStreamArray[i].close();
			}
		}
		if(aisList!=null){
			for(AudioInputStream stream: aisList){
				stream.close();
			}
			aisList.clear();
		}
	}
	
	//-------------------- Serialize Managment -------------------------------------------------
	
	/**
	 * @param out
	 * @throws IOException
	 */
	private void writeObject(java.io.ObjectOutputStream out)
			throws IOException {
		out.writeObject(m_soundSetArray);
		out.writeInt(PatternId);
		out.writeLong(timeLength);
		out.writeInt(granularity);
		out.writeObject(m_instrument);
		out.writeBoolean(isComposite);
	}
	
	/**
	 * @param in
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	private void readObject(java.io.ObjectInputStream in)
			throws IOException, ClassNotFoundException {
		m_soundSetArray = (SoundSet[]) in.readObject();
		PatternId = in.readInt();
		timeLength = in.readLong();
		granularity = in.readInt();
		m_instrument = (Instrument)in.readObject();
		isComposite = in.readBoolean();
		columnIndicesToUpdate = new HashSet<>();
		for(int i=0;i<m_soundSetArray.length;i++) {
			columnIndicesToUpdate.add((long)i);
		}
		m_AudioInputStreamArray= new AudioInputStream[(int)timeLength/granularity];
		mainWavNeedsUpdate = true;
		m_currentAudioInputStream = null;
		changedSinceLastSave=false;
		
	}
	
	/**
	 * @throws ObjectStreamException
	 */
	@SuppressWarnings("unused")
	private void readObjectNoData()
			throws ObjectStreamException {
		m_AudioInputStreamArray= new AudioInputStream[(int)timeLength/granularity];
		m_soundSetArray = new SoundSet[(int)timeLength/granularity];
		PatternId = -1;
		columnIndicesToUpdate = new HashSet<>();
	}

	public void addPlayable(Pattern m_currentPattern) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public boolean equals(Object pat){
		if(pat instanceof Pattern){
			return ((Pattern)pat).getID()==(getID());
		}
		return false;
	}
	
	public static Comparator<Pattern> makePatternComparator() {
		return new Comparator<Pattern>() {

			@Override
			public int compare(Pattern arg0, Pattern arg1) {
				if(arg0.getID()>arg1.getID()) return 1;
				else if(arg0.getID()<arg1.getID()) return -1;
				return 0;
			}
			
		};
	}
}



