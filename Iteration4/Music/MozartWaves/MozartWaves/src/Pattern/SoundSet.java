/**
 * 
 */
package Pattern;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import Playable.Sound;

/**
 * @author RomanM
 * SoundSet - contains a Set of Sounds - for template purposes 
 */
public class SoundSet implements Serializable {
	
	private static final long serialVersionUID = 1217252829050647222L;
	Set<Sound> plSoundList;
	
	/**
	 *  initialize
	 */
	SoundSet(){
		this.plSoundList=new HashSet<Sound>();
	}
	
	/**
	 * @return the Set
	 */
	public Set<Sound> getSoundSet(){
		return plSoundList;
	}
	
	@Override
	public SoundSet clone() {
		SoundSet $ = new SoundSet();
		for(Sound sound : plSoundList) {
			$.plSoundList.add(sound.clone());
		}
		return $;
	}
}
