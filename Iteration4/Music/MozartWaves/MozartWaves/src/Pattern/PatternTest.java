/**
 * 
 */
package Pattern;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import MozartWaves.Pojos.Instrument;
import Playable.Sound;

/**
 * @author RomanM
 *
 */
public class PatternTest {

	
	
	private Pattern pattern1;
	private static int granularity;
	private static long length;
	private static int pattern1Id;
	private static Sound  sound1;
	private static String sound1Path;
	
	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		sound1Path="blabla";
		granularity=250;
		length=40000;
		pattern1Id=5;
		sound1=new Sound(sound1Path, granularity);
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		
		
		
	}

	@Test
	public void testThatInitializingPatternWDoesntCrash() {
		pattern1 = new Pattern(length,pattern1Id,granularity);
		assertTrue(true);
	}
	
	@Test
	public void testThatISoundIsInsertedWithoutCrashing() {
		pattern1 = new Pattern(length,pattern1Id,granularity);
		pattern1.addPlayable(sound1,5);
		assertTrue(true);
	}
	
	@Test
	public void testThatAfterSoundAddedContainsReturnsTrue() {
		pattern1 = new Pattern(length,pattern1Id,granularity);
		pattern1.addPlayable(sound1, 5);
		assertTrue(pattern1.contains(sound1, 5));
	}
	
	@Test
	public void testThatAfterSoundIsAddedRemovesGracefully() {
		pattern1 = new Pattern(length,pattern1Id,granularity);
		pattern1.addPlayable(sound1, 5);
		pattern1.removePlayable(sound1,5);
		assertTrue(true);
	}
	
	@Test
	public void testThatAfterSoundIsAddedAndRemovedContainsReturnsFalse() {
		pattern1 = new Pattern(length,pattern1Id,granularity);
		pattern1.addPlayable(sound1, 5);
		pattern1.removePlayable(sound1,5);
		assertFalse(pattern1.contains(sound1, 5));
	}
	
	@Test
	public void testThatGetInstrumentIsCorrect() {
		pattern1 = new Pattern(length,pattern1Id,granularity);
		assertNull(pattern1.getInstrument());
	}
	
	@Test
	public void testThatGetInstrumentIsCorrect2() {
		pattern1 = new Pattern(length,pattern1Id,granularity, Instrument.PIANO);
		assertEquals(Instrument.PIANO, pattern1.getInstrument());
	}
	
	@Test
	public void testThatChangeLengthChangesLength() {
		pattern1 = new Pattern(length,pattern1Id,granularity, Instrument.PIANO);
		pattern1.changeLength(600);
		assertEquals(600, pattern1.getTimeLength());
	}
	
}
