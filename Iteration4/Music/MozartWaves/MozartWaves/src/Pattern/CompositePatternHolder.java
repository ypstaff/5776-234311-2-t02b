package Pattern;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.UnsupportedAudioFileException;

import Playable.Playable;
import Playable.Sound;

/**
 * @author RomanM
 * Extends pattern. accepts pattern as columns
 */
public class CompositePatternHolder extends Pattern{

	private static final long serialVersionUID = -9006936397340026413L;
	private static int compositePatternId= 100;
	@SuppressWarnings("unused")
	private long timeSegnmentLength;
	private List<Pattern> existingPatterns;
	
	/**
	 * @param timeSegnmentLength - in Milliseconds
	 * initializer
	 */
	public CompositePatternHolder(long timeSegnmentLength) {
		super(timeSegnmentLength, compositePatternId, 4000);
		this.timeSegnmentLength=timeSegnmentLength;
		setComposite(true);
		existingPatterns= new ArrayList<Pattern>();
	}
	
	/**
	 * @param pattern
	 * @param col - column index
	 * adds the pattern at the "index" column
	 * @throws IOException
	 * @throws UnsupportedAudioFileException
	 */
	public void addPlayable(Pattern pattern,int col) throws IOException, UnsupportedAudioFileException {
		if(col>=getPlayableSoundSet().length){
			changeLength((col+1)*4000);
		}
		existingPatterns.add(pattern);
		pattern.updatePatternAndSave();
		Sound plSound = new Sound(pattern.getPatternRelativePath(),
				pattern.getTimeLength());
		super.addPlayable(plSound, col);
	}
	
	/**
	 * @param pattern
	 * @param col - column index
	 * removes the pattern at the "index" column
	 */
	public void removePlayable(Pattern pattern, int col) {
		try {
			existingPatterns.remove(pattern);
			pattern.updatePatternAndSave();
		} catch (IOException | UnsupportedAudioFileException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Sound plSound = new Sound(pattern.getPatternRelativePath(),
				pattern.getTimeLength());
		super.removePlayable(plSound, col);
	}

	/**
	 * @param pattern
	 * @param col
	 * @return true- if pattern exists at coumn "col"
	 */
	public boolean exists(Pattern pattern, int col) {
		Sound plSound = new Sound(pattern.getPatternRelativePath(),
				pattern.getTimeLength());
		return contains(plSound, col);
	}
	
	
	
	//--------------------------- Overridings -------------------------------------------
	
	@Override
	public void updatePatternAndSave() throws IOException, UnsupportedAudioFileException{
		for(Pattern pat: existingPatterns){
			pat.updatePatternAndSave();
		}
		super.updatePatternAndSave();
	}
	
	@Override
	public AudioInputStream getAudioInputStream() throws IOException, UnsupportedAudioFileException{
		changeLength((getMaxColumn()+1)*4000);
		return super.getAudioInputStream();
	}
	
	
	private int getMaxColumn(){
		int maxCol=0;
		SoundSet[] set = getPlayableSoundSet();
		for(int i=0; i< set.length; i++)
			if(!set[i].getSoundSet().isEmpty()){
				maxCol=i;
		}
		return maxCol;
	}
	
	@Override
	public Playable clone() {
		return this;
	}

	@Override
	public int getID() {
		return compositePatternId;
	}
	


}
