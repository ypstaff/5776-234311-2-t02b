
package Pattern;

import java.io.IOException;

import javax.sound.sampled.UnsupportedAudioFileException;

import Playable.Sound;

/**
 * @author RomanM
 *
 */
public final class PatternManager {

	
	
	/**
	 * @param pattern1,pattern2
	 * @param id
	 * @return Pattern -  A mix of the two patterns
	 * @throws IOException
	 * @throws UnsupportedAudioFileException
	 */
	public static Pattern mergePatterns(Pattern pattern1, Pattern pattern2, int id) throws IOException, UnsupportedAudioFileException{
		//TODO: if very slow, can do some improvements here
		Pattern mergedPattern= new Pattern(Math.max(pattern1.getTimeLength(),pattern2.getTimeLength()), id, 250);
		Sound plSound1 = new Sound(pattern1.getPatternRelativePath(), pattern1.getTimeLength());
		Sound plSound2 = new Sound(pattern2.getPatternRelativePath(), pattern2.getTimeLength());
		mergedPattern.addPlayable(plSound1, 0);
		mergedPattern.addPlayable(plSound2, 0);
		mergedPattern.setComposite(true);
		return mergedPattern;
	}
	
	/**
	 * 
	 * @param pattern1
	 * @param pattern2
	 * @param id
	 * @return Pattern - A concat of the two patterns
	 * @throws IOException
	 * @throws UnsupportedAudioFileException
	 */
	public static Pattern concatPatterns(Pattern pattern1, Pattern pattern2, int id) throws IOException, UnsupportedAudioFileException{
		//TODO: if very slow, can do some improvements here
		Pattern concatinatedPattern= new Pattern(pattern1.getTimeLength()+pattern2.getTimeLength(), id,250);
		pattern1.updatePatternAndSave();
		pattern2.updatePatternAndSave();
		Sound plSound1 = new Sound(pattern1.getPatternRelativePath(), pattern1.getTimeLength());
		Sound plSound2 = new Sound(pattern2.getPatternRelativePath(), pattern2.getTimeLength());
		concatinatedPattern.addPlayable(plSound1, 0);
		concatinatedPattern.addPlayable(plSound2, pattern1.getTimeLength()/250);
		concatinatedPattern.setComposite(true);
		return concatinatedPattern;
	}
	
}
