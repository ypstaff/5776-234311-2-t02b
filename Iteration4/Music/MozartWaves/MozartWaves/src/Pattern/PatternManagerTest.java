
package Pattern;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.UnsupportedAudioFileException;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import AISManaging.AISManager;
import MozartWaves.Pojos.Chord;
import MozartWaves.Pojos.Instrument;
import MozartWaves.Pojos.MusicalNote;
import Playable.InstrumentSound;

/**
 * @author RomanM
 *
 */
public class PatternManagerTest {

	
	Pattern pattern1;
	Pattern pattern2, pattern3, pattern4;
	long pattern1Time = 1000, pattern2Time = 6000;
	long pattern3Time = 6000, pattern4Time = 4000;
	int granularity = 250;
	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		pattern1 = new Pattern(pattern1Time,-1,granularity);
		InstrumentSound sound =  new InstrumentSound(Instrument.PIANO, MusicalNote.C, 3, 500);
		InstrumentSound sound2 =  new InstrumentSound(Instrument.PIANO, MusicalNote.E, 3, 500);
		InstrumentSound sound3 =  new InstrumentSound(Instrument.PIANO, MusicalNote.G, 3, 500);
		pattern1.addPlayable(sound, 0);
		pattern1.addPlayable(sound2, 0);
		pattern1.addPlayable(sound3, 0);
		pattern2 = new Pattern(pattern2Time,2,granularity);
		sound =  new InstrumentSound(Instrument.PIANO, MusicalNote.C, 4, 2000);
		sound2 =  new InstrumentSound(Instrument.PIANO, MusicalNote.E, 4, 2000);
		sound3 =  new InstrumentSound(Instrument.PIANO, MusicalNote.G, 4, 2000);
		pattern2.addPlayable(sound, 0);
		pattern2.addPlayable(sound2, 0);
		pattern2.addPlayable(sound3, 0);
		
		pattern3 = new Pattern(pattern3Time,1,granularity);
		sound =  new InstrumentSound(Instrument.ELECTRIC_GUITAR_CHORDS, Chord.A,0, 4000);
		pattern3.addPlayable(sound, 0);
		pattern4 = new Pattern(pattern4Time,2,granularity);
		sound =  new InstrumentSound(Instrument.ELECTRIC_GUITAR_CHORDS, Chord.Am,0, 2000);
		pattern4.addPlayable(sound, 5);

	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testThatLengthOfMergesIsCorrect() throws IOException, UnsupportedAudioFileException {
		Pattern mergedPattern = PatternManager.mergePatterns(pattern1, pattern2, 3);
		assertEquals(pattern2Time,mergedPattern.getTimeLength());
	}
	
	@Test
	public void testThatMergedPatternSavesCorrectly() throws IOException, UnsupportedAudioFileException {
		Pattern mergedPattern = PatternManager.mergePatterns(pattern1, pattern2, 3);
		AISManager.saveAudioWav(mergedPattern.getAudioInputStream(), "/Tests/", "mergedPatternsCEG3_CEG4.wav");
	}


	@Test
	public void testThatLengthOfConcatenatedIsCorrect() throws IOException, UnsupportedAudioFileException {
		Pattern concatenatedPattern = PatternManager.concatPatterns(pattern1, pattern2, 3);
		assertEquals(pattern2Time+pattern1Time,concatenatedPattern.getTimeLength());
	}
	
	
	@Test
	public void testConcatPatternToItselfAcoupleOfTimes() throws IOException, UnsupportedAudioFileException {
		Pattern concatenatedPattern = PatternManager.concatPatterns(pattern1, pattern1, 0);
		for(int i=1; i<10; i++){
			concatenatedPattern = PatternManager.concatPatterns(concatenatedPattern, pattern1, i);
		}
		AISManager.saveAudioWav(concatenatedPattern.getAudioInputStream(), "/Tests/", "CEG3_concatenated5Times.wav");
		AISManager.saveAudioWav(concatenatedPattern.getAudioInputStream(), "/Tests/", "CEG3_concatenated5Times.wav");
		AISManager.saveAudioWav(concatenatedPattern.getAudioInputStream(), "/Tests/", "CEG3_concatenated5Times.wav");
		AISManager.saveAudioWav(concatenatedPattern.getAudioInputStream(), "/Tests/", "CEG3_concatenated5Times.wav");
	}
	

	@Test
	public void testThatElectricMergedPatternSavesCorrectly() throws IOException, UnsupportedAudioFileException {
		Pattern mergedPattern = PatternManager.mergePatterns(pattern3, pattern4, 3);
		//AudioInputStream ais = pattern3.getAudioInputStream();
		@SuppressWarnings("unused")
		AudioInputStream tmpAIS= AISManager.loadAudio("/Sounds/ClassicGuitarChords/A.wav", -999);
		AISManager.saveAudioWav(pattern3.getAudioInputStream(), "/Tests/", "mergedPatternsA.wav");
		AISManager.saveAudioWav(mergedPattern.getAudioInputStream(), "/Tests/", "mergedPatternsA_Am.wav");
	
	}
	
}
