package AISManaging;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import java.io.File;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import org.junit.Test;
import MozartWaves.Main;
import MozartWaves.Pojos.Instrument;
import MozartWaves.Pojos.MusicalNote;
import Playable.InstrumentSound;

/**
 * @author RomanM
 * Most of those tests have to be done by listening
 */
public class AISManagerTest {

	@Test
	public void testThatTrimmingWorksCorrectly() throws LineUnavailableException, IOException {
		InstrumentSound sound =  new InstrumentSound(Instrument.PIANO, MusicalNote.B, 3, 500); 
		assertEquals( 500 , sound.getTimeLength() );
	}
	
	@Test
	public void testThatLengthIsHalfSecondWhenAssigned() throws LineUnavailableException, IOException {
		InstrumentSound sound =  new InstrumentSound(Instrument.PIANO, MusicalNote.C, 3, 500); 
		assertEquals( 500 , sound.getTimeLength());
	}
	
	@Test
	public void testThatAudioInputStreamLoadsCorrectly() throws LineUnavailableException, IOException, UnsupportedAudioFileException {
		InstrumentSound sound =  new InstrumentSound(Instrument.PIANO, MusicalNote.C, 3, 500); 
		AudioInputStream ais = AISManager.loadAudio(sound.getPath(), sound.getTimeLength());
		assertNotNull(ais);
	}
	
	@Test
	public void testThatAudioInputStreamSavesCorectly() throws LineUnavailableException, IOException, UnsupportedAudioFileException {
		InstrumentSound sound =  new InstrumentSound(Instrument.PIANO, MusicalNote.C, 3, 4000);
		AudioInputStream ais = AISManager.loadAudio(sound.getPath(), sound.getTimeLength());
		AISManager.saveAudioWav(ais, "/Tests/","C3.wav");
		//if we're here than we've not crushed - also check the path
	}
	
	@Test
	public void testThatMixingMixesWithoutCrashing() throws LineUnavailableException, IOException, UnsupportedAudioFileException {
		InstrumentSound sound =  new InstrumentSound(Instrument.PIANO, MusicalNote.C, 3, 4000);
		InstrumentSound sound2 =  new InstrumentSound(Instrument.PIANO, MusicalNote.E, 3, 4000);
		@SuppressWarnings("unused")
		AudioInputStream ais = AISManager.mergeAIS(sound.getAudioInputStream(), 
				sound2.getAudioInputStream());
	}
	
	@Test
	public void testThatMixingReturnsNotNull() throws LineUnavailableException, IOException, UnsupportedAudioFileException {
		InstrumentSound sound =  new InstrumentSound(Instrument.PIANO, MusicalNote.C, 3, 4000);
		InstrumentSound sound2 =  new InstrumentSound(Instrument.PIANO, MusicalNote.E, 3, 4000);
		AudioInputStream ais = AISManager.mergeAIS(sound.getAudioInputStream(), 
				sound2.getAudioInputStream());
		assertNotNull(ais);
	}
	
	@Test
	public void testThatMixingAndSaveWorksTogether() throws LineUnavailableException, IOException, UnsupportedAudioFileException {
		InstrumentSound sound =  new InstrumentSound(Instrument.PIANO, MusicalNote.C, 3, 4000);
		InstrumentSound sound2 =  new InstrumentSound(Instrument.PIANO, MusicalNote.E, 3, 4000);
		AudioInputStream ais = AISManager.mergeAIS(sound.getAudioInputStream(), 
				sound2.getAudioInputStream());
		AISManager.saveAudioWav(ais, "/Tests/","C3_E3_Merged.wav");
	}
	
	@Test
	public void testThatMixingMergesThreeSounds() throws LineUnavailableException, IOException, UnsupportedAudioFileException {
		InstrumentSound sound =  new InstrumentSound(Instrument.PIANO, MusicalNote.C, 3, 4000);
		InstrumentSound sound2 =  new InstrumentSound(Instrument.PIANO, MusicalNote.E, 3, 2000);
		InstrumentSound sound3 =  new InstrumentSound(Instrument.PIANO, MusicalNote.G, 3, 1000);
		AudioInputStream ais = AISManager.mergeAIS(sound.getAudioInputStream(), 
				sound2.getAudioInputStream());
		AudioInputStream ais2 = AISManager.mergeAIS(sound3.getAudioInputStream(), ais);
		AISManager.saveAudioWav(ais2, "/Tests/","C3_E3_G3_Merged.wav");
	}
	
	@Test
	public void testThatConcatinatingDoesntCrashAndReturnsNotNull() throws LineUnavailableException, IOException, UnsupportedAudioFileException {
		InstrumentSound sound =  new InstrumentSound(Instrument.PIANO, MusicalNote.C, 3, 4000);
		InstrumentSound sound2 =  new InstrumentSound(Instrument.PIANO, MusicalNote.E, 3, 4000);
		AudioInputStream ais = AISManager.concatinateAIS(sound.getAudioInputStream(), 
				sound2.getAudioInputStream());
		assertNotNull(ais);
	}
	
	@Test
	public void testThatConcatinatingCreatesADesiredSound() throws LineUnavailableException, IOException, UnsupportedAudioFileException {
		InstrumentSound sound =  new InstrumentSound(Instrument.PIANO, MusicalNote.C, 3, 4000);
		InstrumentSound sound2 =  new InstrumentSound(Instrument.PIANO, MusicalNote.E, 3, 4000);
		AudioInputStream ais = AISManager.concatinateAIS(sound.getAudioInputStream(), 
				sound2.getAudioInputStream());
		AISManager.saveAudioWav(ais, "/Tests/","C3_E3_Concatenated.wav");
	}
	
	@Test
	public void testThatConcatinatingAndMergingTwoChordsWorks() throws LineUnavailableException, IOException, UnsupportedAudioFileException {
		InstrumentSound sound =  new InstrumentSound(Instrument.PIANO, MusicalNote.C, 3, 4000);
		InstrumentSound sound2 =  new InstrumentSound(Instrument.PIANO, MusicalNote.E, 3, 3000);
		InstrumentSound sound3 =  new InstrumentSound(Instrument.PIANO, MusicalNote.G, 3, 2000);
		AudioInputStream ais = AISManager.mergeAIS(sound.getAudioInputStream(), 
				sound2.getAudioInputStream());
		AudioInputStream ais2 = AISManager.mergeAIS(sound3.getAudioInputStream(), ais);
		
		InstrumentSound sound4 =  new InstrumentSound(Instrument.PIANO, MusicalNote.C, 4, 2000);
		InstrumentSound sound5 =  new InstrumentSound(Instrument.PIANO, MusicalNote.E, 4, 2000);
		InstrumentSound sound6 =  new InstrumentSound(Instrument.PIANO, MusicalNote.G, 4, 2000);
		 ais = AISManager.mergeAIS(sound4.getAudioInputStream(), 
				sound5.getAudioInputStream());
		AudioInputStream ais3 = AISManager.mergeAIS(sound6.getAudioInputStream(), ais);
		AudioInputStream aisFinal=AISManager.concatinateAIS(ais2, ais3);
		assertNotNull(aisFinal);
		AISManager.saveAudioWav(aisFinal, "/Tests/", "CEG3_CEG4_concatinate.wav");
	}
	
	@Test
	public void testThatMergingOf8SoundsWorks() throws LineUnavailableException, IOException, UnsupportedAudioFileException {
		List<AudioInputStream> AISList = new ArrayList<AudioInputStream>();
		List<InstrumentSound> soundList = new ArrayList<InstrumentSound>();
		soundList.add(new InstrumentSound(Instrument.PIANO, MusicalNote.C, 3, 2000));
		soundList.add(new InstrumentSound(Instrument.PIANO, MusicalNote.E, 3, 2000));
		soundList.add(new InstrumentSound(Instrument.PIANO, MusicalNote.G, 3, 2000));
		soundList.add(new InstrumentSound(Instrument.PIANO, MusicalNote.F, 3, 2000));
		soundList.add(new InstrumentSound(Instrument.PIANO, MusicalNote.C, 4, 2000));
		soundList.add(new InstrumentSound(Instrument.PIANO, MusicalNote.E, 4, 2000));
		soundList.add(new InstrumentSound(Instrument.PIANO, MusicalNote.G, 4, 2000));
		soundList.add(new InstrumentSound(Instrument.PIANO, MusicalNote.F, 4, 2000));
		for(InstrumentSound sound: soundList){
			AISList.add(sound.getAudioInputStream());
		}
		
		AudioInputStream aisFinal = AISManager.mergeAIS(AISList);
		
		AISManager.saveAudioWav(aisFinal, "/Tests/", "superMerged.wav");
		AISList = new ArrayList<AudioInputStream>();
		soundList = new ArrayList<InstrumentSound>();
		soundList.add(new InstrumentSound(Instrument.PIANO, MusicalNote.C, 5, 2000));
		soundList.add(new InstrumentSound(Instrument.PIANO, MusicalNote.E, 5, 2000));
		soundList.add(new InstrumentSound(Instrument.PIANO, MusicalNote.G, 5, 2000));
		soundList.add(new InstrumentSound(Instrument.PIANO, MusicalNote.F, 5, 2000));
		for(InstrumentSound sound: soundList){
			AISList.add(sound.getAudioInputStream());
		}
		aisFinal = AISManager.mergeAIS(AISList);
		AISManager.saveAudioWav(aisFinal, "/Tests/", "superMerged2.wav");
		//AISList.add(SoundManager.loadAudio("/Tests/superMerged.wav", 2000));
		aisFinal = AISManager.mergeAIS(AISManager.loadAudio("/Tests/superMerged.wav", 2000),
				AISManager.loadAudio("/Tests/superMerged2.wav", 2000));
		AISManager.saveAudioWav(aisFinal, "/Tests/", "superMergedEXTRALARGE.wav");
	}
	
	@Test
	public void testThatConcatinatingAListWorks() throws LineUnavailableException, IOException, UnsupportedAudioFileException {
		InstrumentSound sound =  new InstrumentSound(Instrument.PIANO, MusicalNote.C, 3, 2000);
		InstrumentSound sound2 =  new InstrumentSound(Instrument.PIANO, MusicalNote.E, 3, 2000);
		InstrumentSound sound3 =  new InstrumentSound(Instrument.PIANO, MusicalNote.G, 3, 2000);
		List<AudioInputStream> AISList= new ArrayList<AudioInputStream>();
		AISList.add(sound.getAudioInputStream());
		AISList.add(sound2.getAudioInputStream());
		AISList.add(sound3.getAudioInputStream());
		AudioInputStream aisFinal=AISManager.concatinateAIS(AISList);
		assertNotNull(aisFinal);
		AISManager.saveAudioWav(aisFinal, "/Tests/", "CEGe_ConcatinatedByListCon.wav");
	}
	
	@Test
	public void script() throws LineUnavailableException, IOException, UnsupportedAudioFileException {
		for(int i=1; i<=6; i++){
			for(MusicalNote note: MusicalNote.values()){
				InstrumentSound sound =  new InstrumentSound(Instrument.PIANO, note, i, 250);
				AISManager.saveAudioWav(sound.getAudioInputStream(), "/Sounds/Piano/", "Piano.ff." +note+i+  "_250.wav");
			}
		}
	}
	
	
	@Test
	public void testThatSavesCorrectly() throws LineUnavailableException, IOException, UnsupportedAudioFileException {
		InstrumentSound sound =  new InstrumentSound(Instrument.PIANO, MusicalNote.C, 3, 2000);
		AISManager.saveAudioWav(sound.getAudioInputStream(), "/Tests/", "soundToSave.wav");
		File f = new File( (Main.class.getResource("/Tests/soundToSave.wav")).getPath());
		assertTrue(f.exists() );
	}
	
	public static void gc() {
	     Object obj = new Object();
	     @SuppressWarnings({ "rawtypes" })
		WeakReference ref = new WeakReference<Object>(obj);
	     obj = null;
	     while(ref.get() != null) {
	       System.gc();
	     }
	   }
	
}
