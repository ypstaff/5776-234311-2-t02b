/**
 * 
 */
package AISManaging;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * @author RomanM
 * Audio Input Stream garbage collector 
 */

public final class AisGC {

	static List<InputStream> currentStreamsToDelete = new ArrayList<InputStream>();

	
	/**
	 * @param in
	 * add "in" to the streams needed to flush/close later
	 */
	public static void addStreamToFlush(InputStream in){
		currentStreamsToDelete.add(in);
	}
	
	
	/**
	 * @throws IOException
	 * flush all added streams and call System gc
	 */
	public static void flushStreams() throws IOException{
		for(InputStream stream: currentStreamsToDelete){
			stream.close();
		}
		currentStreamsToDelete.clear();
		System.gc();
	}
	
}
