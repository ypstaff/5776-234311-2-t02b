package AISManaging;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.SequenceInputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.sound.sampled.AudioFileFormat;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioFormat.Encoding;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.UnsupportedAudioFileException;

import MozartWaves.Main;

/**
 * @author RomanM
 * Works in AudioInputStream level:
 * Merging\Concatinating\trimming streams
 * Saving\Loading streams by Path;
 */


public final class AISManager {
	
	public static AudioFormat g_audioFormat= new AudioFormat(Encoding.PCM_SIGNED,44100,16,2,4,44100,false);
	private static SilentAudioInputStream silentAudio; 
	private static  final int numberofSamplesForSilence = 200000000;
	
	 static {
		 silentAudio = new  SilentAudioInputStream(g_audioFormat, numberofSamplesForSilence);
	 }
	
	/**
	 * @param audioInputStream
	 * @param startMilli
	 * @param endMilli
	 * @return new AIS with that ends in endMilli miliseconds
	 * @throws IOException
	 */
	public static AudioInputStream trimAIS(AudioInputStream audioInputStream,long startMilli,float endMilli) throws IOException{
		AudioFormat audioFormat= audioInputStream.getFormat();
		float bytesPerSecond = audioFormat.getFrameSize() * (int)audioFormat.getFrameRate();
	  //  long  startByte=bytesPerSecond*(int)(2); //TODO: check how to trim beginning
	    long  endByte=(long) (bytesPerSecond*(endMilli/4000));
	    //audioInputStream.skip(startByte); 
	    AudioInputStream shortenedStream = new AudioInputStream(audioInputStream, audioInputStream.getFormat(), endByte);
	    AisGC.addStreamToFlush(shortenedStream);
	    return shortenedStream;
	}
	
	/**
	 * @param ais1
	 * @param ais2
	 * @return a merge od the two AISs
	 */
	public static AudioInputStream mergeAIS(AudioInputStream ais1, AudioInputStream ais2){
		ArrayList<AudioInputStream> aisList = new ArrayList<AudioInputStream>();
		aisList.add(ais1);
		aisList.add(ais2);
		MixingFloatAudioInputStream mais = new MixingFloatAudioInputStream(ais1.getFormat(),aisList);
		AisGC.addStreamToFlush(mais);
		AudioInputStream ais= new AudioInputStream(mais,mais.getFormat(),mais.getFrameLength());
		AisGC.addStreamToFlush(ais);
		return ais;
	}
	
	/**
	 * @param audioInputStreamLst
	 * @return a merge of all the AISs in list
	 */
	public static AudioInputStream mergeAIS(List<AudioInputStream> audioInputStreamLst){
		if(audioInputStreamLst.size()==0){
			return null;
		}
		MixingFloatAudioInputStream mais = new MixingFloatAudioInputStream(g_audioFormat,audioInputStreamLst);
		AisGC.addStreamToFlush(mais);
		AudioInputStream ais= new AudioInputStream(mais,mais.getFormat(),mais.getFrameLength());
		AisGC.addStreamToFlush(ais);
		try {
			mais.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return ais;
	}
	
	/**
	 * @param ais
	 * @param relativePathName - relative path to the main program
	 * @param fileName
	 * @throws IOException
	 * saves the ais stream as relativePathName/filename 
	 */
	public static void saveAudioWav(AudioInputStream ais, String relativePathName,String fileName) throws IOException{
		String testPath = Main.class.getResource(relativePathName).getPath();
		File fileOut = new File(testPath+"/"+fileName);
		AudioSystem.write(ais, AudioFileFormat.Type.WAVE, fileOut);
	}
	
	/**
	 * @param fullPathRelative- relative path to the main program
	 * @param l
	 * @return stream from fullPathRelative with length of "l" milliseconds
	 * @throws IOException
	 * @throws UnsupportedAudioFileException
	 */
	public static AudioInputStream loadAudio(String fullPathRelative, long l) throws IOException, UnsupportedAudioFileException{
		InputStream in = Main.class.getResourceAsStream(fullPathRelative);
		InputStream buf = new BufferedInputStream(in);
		AisGC.addStreamToFlush(in);
		AisGC.addStreamToFlush(buf);
		AudioInputStream ais = AudioSystem.getAudioInputStream(buf);
		AisGC.addStreamToFlush(ais);
		if(l!=-999){
			ais= AISManager.trimAIS(ais, 0, l);
		}
		AisGC.addStreamToFlush(ais);
		return ais;
	}
	
	/**
	 * @param ais1
	 * @param ais2
	 * @return a concatenation of ais1+ais2
	 */
	public static AudioInputStream concatinateAIS(AudioInputStream ais1, AudioInputStream ais2){
		   AudioInputStream ais = new AudioInputStream(
                       new SequenceInputStream(ais1, ais2),     
                       ais2.getFormat(), 
                       ais1.getFrameLength() + ais2.getFrameLength());
		   AisGC.addStreamToFlush(ais);
		   return ais;
	}
	
	/**
	 * @param audioInputStreamLst
	 * @return a concatenation of all the streams in the list(in the order they appear in list)
	 */
	public static AudioInputStream concatinateAIS(List<AudioInputStream> audioInputStreamLst){
		AudioInputStream outputAIS = audioInputStreamLst.get(0);
		AisGC.addStreamToFlush(outputAIS);
		for(AudioInputStream ais: audioInputStreamLst){
			if(!ais.equals(audioInputStreamLst.get(0))){
				outputAIS=concatinateAIS(outputAIS, ais);
				AisGC.addStreamToFlush(outputAIS);
			}
		}
		return outputAIS;
	}

	/**
	 * @param fullPathRelative- relative path to the main program
	 * deletes the sound in the path
	 */
	public static void deleteSounds(String fullPathRelative){
		URL url = Main.class.getResource(fullPathRelative);
		if(url==null){
			return;
		}
		String fullPath = url.getPath();
		File fileToDelete = new File(fullPath);
		fileToDelete.delete();
	}
	
	/**
	 * @param timeLength
	 * @return an AIS of silence with the specified length
	 * @throws IOException
	 */
	public static AudioInputStream getSilence(long timeLength) throws IOException{
		silentAudio.reset();
		return trimAIS(silentAudio, 0, timeLength);
	}
	
}
