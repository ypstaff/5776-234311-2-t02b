package Playable;

import java.io.IOException;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.UnsupportedAudioFileException;

import AISManaging.AISManager;
import MozartWaves.Pojos.AbstractMusicalValue;
import MozartWaves.Pojos.Instrument;


/**
 * @author RomanM
 * Holds the sound component
 */

public class InstrumentSound extends Sound{
	/**
	 * 
	 */
	private static final long serialVersionUID = -95144549009730542L;
	Instrument ins;
	AbstractMusicalValue value;
	int octave; //(ranges from 1 to 7)
	
	

	/**
	 * @param ins - instrument
	 * @param value - Musical value(Note in this case)
	 * @param octave - octave(for piano)
	 * @param length - sound length
	 */
	public InstrumentSound(Instrument ins, AbstractMusicalValue value, int octave, int length) {
		this.ins=ins;
		this.timeLength = length;
		this.value = value;
		this.octave = octave;
	}
	
	
	/**
	 * @param sound 
	 * Copy constructor
	 */
	public InstrumentSound(InstrumentSound sound) {
		this.ins=sound.ins;
		this.value=sound.value;
		this.octave=sound.octave;
		timeLength = sound.timeLength; //Milli Seconds=1000;
	}
	
	
	
	
	@Override
	public AudioInputStream getAudioInputStream() throws IOException, UnsupportedAudioFileException {
		return AISManager.loadAudio(getPath(), timeLength);
	}

	/**
	 * @return the path, constructed from ths Instrument Sound dields
	 */
	public String getPath() {
		String path="";
		switch(ins){
			case PIANO:
				path = "/Sounds/Piano/"+ ins + ".ff."+value.toString()+ Integer.toString(octave)+ ".wav";
			break;
		
			case ELECTRIC_GUITAR_CHORDS:
				path = "/Sounds/ElectricGuitarChords/"+  value.toString()+".wav";
			break;
				
			case CLASSIC_GUITAR_CHORDS:
				path = "/Sounds/ClassicGuitarChords/"+  value.toString()+".wav";
			break;
		default:
			break;
		
		}
		return path;
	}

	/**
	 * @return the Instrument
	 */
	public Instrument getIns() {
		return ins;
	}
	
	

	@Override
	public boolean equals(Object other) {
		if(!(other instanceof InstrumentSound)){
			return false;
		}
		InstrumentSound sound = (InstrumentSound) other;
		if(getPath().equals(sound.getPath()) && timeLength==sound.getTimeLength()){
			return true;
		}
		return false;
	}
	
	@Override
	public int hashCode() {
		return (int) (getPath().hashCode() ^ timeLength);
	}
	
	@Override
	public String toString() {
		if(Instrument.PIANO == this.ins) {
			return this.value.toString()+this.octave;
		} else
			return this.value.toString();
		
	}
	
	@Override
	public InstrumentSound clone() {
		return new InstrumentSound(this);
	}


	
}
