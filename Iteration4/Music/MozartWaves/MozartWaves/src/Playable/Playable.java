package Playable;


import java.io.IOException;
import java.io.Serializable;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineEvent;
import javax.sound.sampled.LineListener;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

import AISManaging.AISManager;
import AISManaging.AisGC;


/**
 * @author RomanM
 *	Playable - classes implementing this one are serializable and can be played/stopped using AIS
 */
public abstract class Playable implements Serializable{
	
	private static final long serialVersionUID = -2183685233218873113L;
	protected long timeLength; // in MilliSeconds
	private static Clip s_clip ;
	private static AudioInputStream  s_AIS;
	

	/**
	 * @returns the ID of the Playable
	 */
	public int getID() {
		return -9999;
	}
	
	/**
	 * plays the Playable via clip
	 * @throws UnsupportedAudioFileException
	 */
	public void play() throws UnsupportedAudioFileException {
		try {
			//Flushing
			if(s_AIS!=null){
				s_AIS.close();
			}
			s_clip.drain();
			s_clip.flush();
			
			//Opening
			s_AIS=getAudioInputStream();
			AisGC.addStreamToFlush(s_AIS);
			s_clip.open(s_AIS);
			s_clip.start();
		} catch (LineUnavailableException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * @return the length of the Playable in Millisecs
	 */
	public long getTimeLength() {
		return timeLength;
	}

	/**
	 * @param t
	 * sets the timeLength to t (Millisecs)
	 */
	public void setTimeLength(long t) {
		this.timeLength =t;
	}
	
	/**
	 * @param listener
	 * inits and adds listener to the clip(for events like stop/play/etc.)
	 */
	public static void initClip(LineListener listener){
		DataLine.Info info = new DataLine.Info(Clip.class, AISManager.g_audioFormat);
		try {
			s_clip = (Clip) AudioSystem.getLine(info);
		} catch (LineUnavailableException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		s_clip.addLineListener(new LineListener()
        {
            @Override
            public void update(LineEvent event)
            {
                if (event.getType() == LineEvent.Type.STOP){
                	s_clip.close();
                	try {
						s_AIS.close();
						s_AIS=null;
						AisGC.flushStreams();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
                }
            }

        });
		s_clip.addLineListener(listener);
	}
	
	/**
	 * @param listener
	 * adds listener to the clip(for events like stop/play/etc.)
	 */
	public static void addLineListener(LineListener listener){
		s_clip.addLineListener(listener);
	}
	
	/**
	 * @throws UnsupportedAudioFileException
	 * stops the clip if its playing (if not , nothing happnes)
	 */
	public static void stopClip() throws UnsupportedAudioFileException {
		if(s_clip.isRunning())
			s_clip.stop();
	}
	
	/**
	 * @throws UnsupportedAudioFileException,IOException
	 * returns the AudioInputStream
	 */
	public abstract AudioInputStream getAudioInputStream() throws IOException, UnsupportedAudioFileException ;
	
	/**
	 * marks all implementing classes cloneable
	 */
	public abstract Playable clone();
	
	
}
