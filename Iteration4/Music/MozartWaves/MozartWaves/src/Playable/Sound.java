package Playable;

import java.io.IOException;
import java.io.Serializable;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.UnsupportedAudioFileException;

import AISManaging.AISManager;

/**
 * @author RomanM
 * simple Sound with a path(to get the AIS from) and length - to trim the AIS
 */
public class Sound extends  Playable implements Serializable{
	
	private static final long serialVersionUID = 1296402851772794943L;
	protected String path;
	
	/**
	 * emptyInitializer
	 */
	public Sound(){}
	
	/**
	 * @param path
	 * @param timeLength - in MilliSeconds
	 * sets a new Sound with path to get the AIS from and the length = timeLength(Milliseconds)
	 */
	public Sound(String path,long timeLength){
		this.timeLength=timeLength;
		this.path=path;
	}

	@Override
	public AudioInputStream getAudioInputStream() throws IOException, UnsupportedAudioFileException {
		if(path==null){
			return null;
		}
		return AISManager.loadAudio(path, -999);
	}

	@Override
	public Sound clone() {
		Sound plSound = new Sound(path, timeLength);
		return plSound;
	}
	
	@Override
	public int hashCode() {
		return (int) (path.hashCode() ^ timeLength);
	}
	
	@Override
	public boolean equals(Object other) {
		if(!(other instanceof Sound)){
			return false;
		}
		Sound sound = (Sound) other;
		if(path.equals(sound.path) && timeLength==sound.getTimeLength()){
			return true;
		}
		return false;
	}
	
}
