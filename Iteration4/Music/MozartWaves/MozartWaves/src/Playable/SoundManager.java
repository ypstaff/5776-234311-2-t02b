/**
 * 
 */
package Playable;

import java.util.ArrayList;
import java.util.List;

import MozartWaves.Pojos.Chord;
import MozartWaves.Pojos.Instrument;
import MozartWaves.Pojos.MusicalNote;

/**
 * @author RomanM
 * Sound Managing -getting sounds by instruments
 */
public final class SoundManager {


	/**
	 * @return a list of All ClassicGuitarSounds
	 */
	public static List<InstrumentSound> classicGuitarSounds() {
		List<InstrumentSound> sounds = new ArrayList<InstrumentSound>();
		for(int i=0;i<Chord.values().length-9;i++) {
			sounds.add(new InstrumentSound(Instrument.CLASSIC_GUITAR_CHORDS,Chord.values()[i],0,250));
		}
		return sounds;
	}
	
	/**
	 * @param fromOctave
	 * @param length
	 * @return A list of piano sounds of the octave "fromOctave", each with length(Millisecs) of "length"
	 */
	public static List<InstrumentSound> pianoSounds(int fromOctave,int length){
		List<InstrumentSound> sounds = new ArrayList<InstrumentSound>();
		for(int i=0;i<MusicalNote.values().length;i++) {
			sounds.add(new InstrumentSound(Instrument.PIANO, MusicalNote.values()[i], 
					fromOctave, length));
		}
		return sounds;
	}
	
	/**
	 * @return a list of All electricGuitarSounds
	 */
	public static List<InstrumentSound> electricGuitarSounds(){
		//sound =  new Sound(instrument, (Chord)value, 250);
		List<InstrumentSound> sounds = new ArrayList<InstrumentSound>();
		for(int i=2;i<Chord.values().length;i++) {
			sounds.add(new InstrumentSound(Instrument.ELECTRIC_GUITAR_CHORDS, Chord.values()[i],0, 250));
		}
		return sounds;
	}
	
}
