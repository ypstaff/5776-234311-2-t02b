package MozartWaves;

import javafx.scene.paint.Color;

/**
 * Holds the colors which can be used by Navigation in this application.
 * this enum is needed since we want to let the user change the color at will,
 * and we need to display the name of the selected color. since Color of javafx doesn't have a method
 * to get the color's name (which makes sense - since not every color has a name), we need
 * an auxiliary class to get the name of the colors we support. 
 * @author Sergey
 *
 */
public enum MarkableColor {Red {
	@Override
	public Color toJavaFXColor() {
		return Color.RED;
	}
},Blue {
	@Override
	public Color toJavaFXColor() {
		return Color.BLUE;
	}
},Green {
	@Override
	public Color toJavaFXColor() {
		return Color.GREEN;
	}
},Purple {
	@Override
	public Color toJavaFXColor() {
		return Color.PURPLE;
	}
},Yellow {
	@Override
	public Color toJavaFXColor() {
		return Color.YELLOW;
	}
};

/**
 * 
 * @return matching javafx Color
 */
public abstract Color toJavaFXColor();

}