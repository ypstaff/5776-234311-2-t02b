/**
 * 
 */
package MozartWaves;


import java.util.ArrayList;

import CompositGUIPanes.GuiTimerLabel;
import Navigation.IterableNodeGroups;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;
/**
 * @author RomanM
 *
 */
import yearlyproject.pcBluetoothApp.SPPBluetoothServer;
public final class GuiUtils {
	
	public static ArrayList<Button> playButtons;
	public static boolean playButtonsEnabled = true;
	public static Button playButton;
	private static GuiTimerLabel timerLabel;
	private static String coloredButtonStyle;
	private static String uncoloredButtonStyle;
	private static BluetoothInterfaceController bluetoothInterface;
	
	
	static {
		uncoloredButtonStyle= new Button().getStyle();
		coloredButtonStyle= "-fx-background-color : Yellow;" +uncoloredButtonStyle;
		bluetoothInterface = new BluetoothInterfaceController(null);
		SPPBluetoothServer sppBluetoothServer= new SPPBluetoothServer(bluetoothInterface);
		sppBluetoothServer.start();
		
    }
	
	 @SuppressWarnings("static-access")
	public static Node getNodeByRowColumnIndex(final int row,final int column,GridPane gridPane) {
	        Node result = null;
	        ObservableList<Node> childrens = gridPane.getChildren();
	        for(Node node : childrens) {
	            if(gridPane.getRowIndex(node) == row && gridPane.getColumnIndex(node) == column) {
	                result = node;
	                break;
	            }
	        }
	        return result;
	  }
	 
	 
	 public static void setPlayButton(Button but){
		 playButton= but;
	 }
	 
	 public static void setTimerLabel(GuiTimerLabel timer){
		 timerLabel= timer;
	 }
	 
	 public static void setPlayButtons(ArrayList<Button> buttons){
			 playButtons= buttons;
			 playButtons.add(playButton);
	 }
	 
	 public static String getColorString(){
		 return coloredButtonStyle;
	 }
	 
	 public static void setColor(String color){
		 coloredButtonStyle= "-fx-background-color : "+color+";" +uncoloredButtonStyle ;
	 }
	 
	 public static void togglePlayButton(){
			playButtonsEnabled=!playButtonsEnabled;
			for(Button $: playButtons){
				$.setDisable(!playButtonsEnabled);
			}
		 	if(timerLabel.getText().equals("")){
		 		timerLabel.startCounting();
		 	}
		 	else{
		 		timerLabel.stopCounting();
		 	}
		
		}


	public static String getUncoloredString() {
		// TODO Auto-generated method stub
		return uncoloredButtonStyle;
	}
	
	/** Initiatess the bluetooth connection with Brainiac bluetooth API*/
	public static void setRoot(IterableNodeGroups root) {
		bluetoothInterface.setRoot(root);
	}
}
