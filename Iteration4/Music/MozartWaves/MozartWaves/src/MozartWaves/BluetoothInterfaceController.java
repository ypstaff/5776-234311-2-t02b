/**
 * 
 */
package MozartWaves;

import Navigation.IterableNodeGroups;
import yearlyproject.pcBluetoothApp.Controller;

/**
 * @author RomanM
 *
 */
public class BluetoothInterfaceController implements Controller{

	
	IterableNodeGroups root;
	
	public BluetoothInterfaceController(IterableNodeGroups root){
		this.root=root;
	}
	
	@Override
	public void back() {
		root.unselect();
		System.out.println("root.unselect();");
	}

	@Override
	public void longBack() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void longNext() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void longSelect() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void next() {
		root.next();
		System.out.println("root.next();");
	}

	@Override
	public void select() {
		root.select();
		System.out.println("root.select();");
	}
	
	public void setRoot(IterableNodeGroups newRoot) {
		this.root = newRoot;
	}
	

}
