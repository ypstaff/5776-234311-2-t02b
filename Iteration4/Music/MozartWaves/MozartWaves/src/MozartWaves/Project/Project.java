package MozartWaves.Project;

import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.ObjectStreamException;
import java.io.Serializable;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import MozartWaves.Main;
import Pattern.CompositePatternHolder;
import Pattern.Pattern;


public class Project implements Serializable {

	private static final long serialVersionUID = 2218001660692158003L;
	private List<Pattern> patterns; 
	private CompositePatternHolder composite;
	private String projectName;
	private int nextPatternID;
	

	private static int NextProjectID;
	private static final  URL projectDirPath = Main.class.getResource("/Projects");
	private static final  URL metadataDirPath = Main.class.getResource("/Metadata");
	
	static {
		loadID();
		Runtime.getRuntime().addShutdownHook(new Thread(()->saveID()));
	}

	public static List<String> allProjectPaths() {
		File projectsDir = new File(projectDirPath.getPath());
		File[] projects = projectsDir.listFiles();
		List<String> $ = new ArrayList<String>();
		String path;
		for(int i=0;i<projects.length;i++) {
			path = projects[i].getPath();
			$.add(projects[i].getPath().substring(path.lastIndexOf("\\")+1, path.length()));
		}
		return $;
	}

	public static Project buildFromName(String name) throws ClassNotFoundException, IOException {
		String filepath = projectDirPath+"/"+name+"/description.txt";
		return buildFromFile(filepath.replace("file:", ""));
	}

	public CompositePatternHolder getCompositePattern() {
		return this.composite;
	}

	public void setCompositePattern(CompositePatternHolder composite) {
		this.composite = composite;
	}

	public static void loadID() {
		FileInputStream fis = null;
		ObjectInputStream ois = null;
		try {
			String filepath = metadataDirPath.getPath()+"/"+"Project";
			File f = new File(filepath);
			if(!f.exists()) {
				f.createNewFile();
				NextProjectID = 1;
			} else {
				fis = new FileInputStream(f);
				ois = new ObjectInputStream(fis);
				NextProjectID = ois.readInt();
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			closeStreamsList(Arrays.asList(fis,ois));
		}
	}

	public static void saveID() {
		FileOutputStream fos = null;
		ObjectOutputStream oos = null;
		try {
			String filepath = metadataDirPath.getPath()+"/"+"Project";
			File f = new File(filepath);
			if(!f.exists()) {
				f.createNewFile();
			}
			fos = new FileOutputStream(f);
			oos = new ObjectOutputStream(fos);
			oos.writeInt(NextProjectID);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			closeStreamsList(Arrays.asList(oos));
		}

	}
	
	private static void closeStreamsList(List<Closeable> streams) {
		for(Closeable $ : streams) {
			if($!=null) {
				try{
					$.close();
				} catch(IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public static Project buildFromFile(String path) throws IOException, ClassNotFoundException {
		validateFilepath(path);
		FileInputStream fis = new FileInputStream(path);
		ObjectInputStream in = new ObjectInputStream(fis);
		Project $ = (Project)in.readObject();
		in.close();
		return $;
	}

	public static void saveToFile(Project project) throws IOException {
		String path = Main.class.getResource("/Projects").getPath() + "/" + project.getName();
		File targetDirectory = new File(path);
		if(!targetDirectory.exists()) {
			targetDirectory.mkdirs();
			NextProjectID++;
		}
		File targetFile = new File(targetDirectory.getPath()+"/description.txt");
		targetFile.createNewFile();

		FileOutputStream fos = new FileOutputStream(targetFile);
		ObjectOutputStream out = new ObjectOutputStream(fos);
		out.writeObject(project);
		out.close();
	}

	private static boolean validateFilepath(String path) throws FileNotFoundException {
		if(path == null) {
			throw new NullPointerException();
		}
		return (new File(path).exists());
	}

	public Project() {
		patterns = new ArrayList<Pattern>();
		composite = new CompositePatternHolder(16000*4);
		projectName="Project_"+NextProjectID;
		nextPatternID = 0;
	}

	public int getCurrentPatternID() {
		return nextPatternID;
	}

	public void clearAllUsedStreams() {
		for(Pattern p : patterns)
			p.clearAllSreamsData();
		composite.clearAllSreamsData();
	}

	public List<Pattern> getPatterns() {
		return this.patterns;
	}

	public String getName() {
		return this.projectName;
	}
	
	public void addPattern(Pattern p) {
		if(patterns.indexOf(p)!=-1)
			patterns.remove(p);
		else 
			nextPatternID++;
		patterns.add(p);
	}


	public List<Pattern> getPatternsInCurrentRange(int compositionStartingPatternIndex) {
		List<Pattern> patterns = getPatterns();
		return patterns.subList(compositionStartingPatternIndex, Math.min(patterns.size(), compositionStartingPatternIndex+8));
	}

	public List<Pattern> getPatternsInNextRange(int compositionStartingPatternIndex) {
		compositionStartingPatternIndex = compositionStartingPatternIndex+8;
		if(compositionStartingPatternIndex>getPatterns().size())
			compositionStartingPatternIndex=0;
		return getPatternsInCurrentRange(compositionStartingPatternIndex);
	}

	public List<Pattern> getPatternsInPreviousRange(int compositionStartingPatternIndex) {
		compositionStartingPatternIndex = compositionStartingPatternIndex-8;
		if(compositionStartingPatternIndex<getPatterns().size())
			compositionStartingPatternIndex=0;
		return getPatternsInCurrentRange(compositionStartingPatternIndex);
	}



	private void writeObject(java.io.ObjectOutputStream out)
			throws IOException {
		out.writeObject(patterns);
		out.writeUTF(projectName);
		out.writeObject(composite);
		out.writeInt(nextPatternID);
	}

	@SuppressWarnings("unchecked")
	private void readObject(java.io.ObjectInputStream in)
			throws IOException, ClassNotFoundException {
		patterns = (List<Pattern>) in.readObject();
		projectName = in.readUTF();
		composite = (CompositePatternHolder) in.readObject();
		nextPatternID = in.readInt();
	}

	@SuppressWarnings("unused")
	private void readObjectNoData()
			throws ObjectStreamException {
		patterns = new ArrayList<Pattern>();
		composite = new CompositePatternHolder(16000*4);
		projectName = "_";
	}
}
