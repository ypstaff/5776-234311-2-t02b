package MozartWaves;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Supplier;

import javax.sound.sampled.LineEvent;
import javax.sound.sampled.LineListener;
import javax.sound.sampled.UnsupportedAudioFileException;

import CompositGUIPanes.ClipPlayingDialog;
import CompositGUIPanes.GuiTimerLabel;
import CompositGUIPanes.PlayableGridPane;
import CompositGUIPanes.ProjectChooserDialog;
import CompositGUIPanes.ProjectDisplayGridPane;
import CompositGUIPanes.SettingsDialog;
import CompositGUIPanes.UserInteractionsStatusPane;
import CompositGUIPanes.UserManualDisplayDialog;
import EventHandlers.ChangeOctaveButtonHandler;
import EventHandlers.ExportAsHandler;
import EventHandlers.NewPatternButtonHandler;
import EventHandlers.SoundTimeButtonHandler;
import MozartWaves.Pojos.Instrument;
import MozartWaves.Pojos.IntWrapper;
import MozartWaves.Pojos.SoundType;
import MozartWaves.Project.Project;
import MozartWaves.undoRedo.UndoRedoMechanism;
import Navigation.IterableMenuBar;
import Navigation.IterableNodeGroup;
import Navigation.IterableNodeGroups;
import Navigation.NavigableApplication;
import Pattern.CompositePatternHolder;
import Pattern.Pattern;
import Playable.InstrumentSound;
import Playable.Playable;
import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.geometry.Rectangle2D;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CustomMenuItem;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Screen;
import javafx.stage.Stage;
import utils.MarkingBoundsState.IterationType;
import yearlyproject.pcBluetoothApp.SPPBluetoothServer;

/**
 * @author RomanM (with sergey)
 * Main application, handles GUI and logic responses
 */

@SuppressWarnings({ "unused"})
public class Main extends NavigableApplication{
	final String taken=" T ", available="";
	final String[] soundLengthValues = new String[]{ "0.25s", "0.5s", "1s", "2s", "4s"};
	private ApplicationSettings settings;
	private UndoRedoMechanism undoRedoMechanism;
	IntWrapper soundTimeLength,currentPianoPatternOctaveView;
	private int compositionStartingPatternIndex,currentTimeSegmentOffset;
	//GUI
	private PlayableGridPane patternGridPane;
	private Stage m_primaryStage;
	private Scene m_scene;
	private MenuBar mBar;
	private BorderPane mainLayout;
	private VBox patternListDisplay;
	private HBox soundTimeLengthOptions,patternControlPane,patternCreationTab;
	private GuiTimerLabel timerLabel;
	private BorderPane patternBorderPane,patternLayout;
	private ProjectDisplayGridPane projectDisplayGridPane;
	private Button newCompositPattern, nextButton, prevButton;
	private UserInteractionsStatusPane statusPane;
	//Pattern
	boolean currenPatternSaved=false;
	private Pattern m_currentPattern;
	private Project currentProject;
	private Instrument m_currentInstrument;
	public static final int patternTimeLength = 4000;
	private boolean projectGridMode=false;
	private ClipPlayingDialog clipDialog;
	private IterableNodeGroup patternWrapperPanes;
	private IterableNodeGroups root;

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		m_primaryStage=primaryStage;
		Playable.initClip(new LineListener()  {
			@Override
			public void update(LineEvent arg0) {
				if (arg0.getType() == LineEvent.Type.STOP || arg0.getType() == LineEvent.Type.START){
					GuiUtils.togglePlayButton();
				}
			}
		});
		statusPane = new UserInteractionsStatusPane();
		clipDialog = new ClipPlayingDialog();
		compositionStartingPatternIndex = currentTimeSegmentOffset=0;
		undoRedoMechanism = new UndoRedoMechanism();
		settings = new ApplicationSettings();  //TODO : load this from file
		m_currentInstrument=Instrument.PIANO;
		currentPianoPatternOctaveView = new IntWrapper();
		currentPianoPatternOctaveView.value=3;
		currentProject = new Project();
		soundTimeLength= new IntWrapper();
		soundTimeLength.value=250;
		Screen screen = Screen.getPrimary();
		Rectangle2D bounds = screen.getVisualBounds();
		m_primaryStage.setWidth(bounds.getWidth());
		m_primaryStage.setHeight(bounds.getHeight());
		mainLayout = new BorderPane();
		m_primaryStage.setScene(this.init(mainLayout));
		root=getRoot();
		initMainPane();
		initiateGroups();
		createNewPattern(m_currentInstrument);
		m_primaryStage.setResizable(false);
		m_primaryStage.show();
		this.start();
		m_primaryStage.setTitle("Music Waves");
		GuiUtils.setRoot(root);
		m_primaryStage.setOnCloseRequest(e ->{ currentProject.clearAllUsedStreams();Platform.exit(); System.exit(0);});
	}
	
	/** @param ins - the chosen Instrument
	 creates and set the GUI with a new basic pattern*/
	public void createNewPattern(Instrument ins){
		m_currentInstrument=ins;
		if(m_currentPattern!= null ){
			if(!currenPatternSaved){
				m_currentPattern.clearAllSreamsData();
			}else if( !settings.isQuickReloadEnabled()){
				m_currentPattern.clearColumnStreamsData();
			}
		}
		projectGridMode = false;
		m_currentPattern = new Pattern(patternTimeLength, currentProject.getCurrentPatternID(), 250, m_currentInstrument);
		statusPane.updateStatus(currentProject, m_currentPattern, UserInteractionsStatusPane.PATTERN_CREATION);
		refreshGrid();
	}

	/** loads the pattern to the grid(via parsing logic)
	 * @param ins - the chosen instrument
	 * @param pat - the chosen pattern
	 */
	public void loadPattern(Instrument ins, Pattern pat){
		m_currentPattern=pat;
		projectGridMode = false;
		statusPane.updateStatus(currentProject, m_currentPattern, UserInteractionsStatusPane.PATTERN_LOAD);
		refreshGrid();
	}

	/**
	 * Refreshes the grid (For events like changing color, loading/creating patterns
	 */
	public void refreshGrid(){
		if(!projectGridMode)
			getGridPaneByInstrument(m_currentInstrument);
		else
			getCompositPatternGridPane(currentProject.getPatternsInCurrentRange(compositionStartingPatternIndex), currentTimeSegmentOffset);
		patternLayout.setCenter(patternGridPane);
		initiateGroups();
	}

	/**
	 * the following functions return the panes to initiate the grid with
	 * @return the main pane of the GUI stage
	 */
	private Pane initMainPane() {
		
		patternLayout = new BorderPane();
		patternLayout.setTop(initPatternTab());
		mainLayout.setCenter(patternLayout);
		patternGridPane = new PlayableGridPane(IterationType.ROWS, 1,4);
		mBar = initMainMenuBar();
		mainLayout.setTop(mBar);
		patternLayout.setCenter(patternGridPane);
		patternListDisplay = (VBox) initiateProjectDisplayGridPane(currentProject);
		mainLayout.setLeft(patternListDisplay);
		mainLayout.setRight(statusPane);
		mainLayout.setBottom(getIterationControlHBox());
		return mainLayout;
	}

	private void initiateGroups() {
		this.clearGroups();
		this.addIterableGroup(patternGridPane);
		if(!projectGridMode)
				this.addIterableGroup(soundTimeLengthOptions.getChildren());
		this.addIterableGroup(initPatternNavigaionButtons().getChildren());
		this.addIterableGroup(patternCreationTab.getChildren());
		this.addIterableGroup(mBar);
		patternWrapperPanes = this.addIterableGroup(patternListDisplay.getChildren().get(1));
		if(currentProject.getPatterns().size()==0) 
			setEnabledMode(patternWrapperPanes, false);
		else
			setEnabledMode(patternWrapperPanes, true);
		this.start();
	}

	private MenuBar initMainMenuBar(){
		MenuBar menuBar = new IterableMenuBar(2);
		menuBar.getMenus().addAll(initFileMenu() ,initSettingsMenu(),initExportAsMenu(),initHelpMenu());
		return menuBar;
	}

	private Menu initFileMenu(){
		Menu menuFile= new Menu("File");
		MenuItem newProj = new CustomMenuItem(new Label("New Project"));
		newProj.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent t) {
				newCompositPattern.setDisable(true);
				currentProject.clearAllUsedStreams();
				currentProject = new Project();
				m_currentInstrument = Instrument.PIANO;
				projectDisplayGridPane.updateView(currentProject);
				createNewPattern(m_currentInstrument);
				undoRedoMechanism = new UndoRedoMechanism();
				statusPane.updateStatus(currentProject, m_currentPattern,
						UserInteractionsStatusPane.PROJECT_CREATED);
			}
		});
		MenuItem openProj = new CustomMenuItem(new Label("Open Project"));
		openProj.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent t) {
				File[] result = new File[1];
				ProjectChooserDialog dialog = new ProjectChooserDialog(result);
				dialog.showAndWait();
				GuiUtils.setRoot(root);
				if(result[0]==null) return;
				try {
					currentProject.clearAllUsedStreams();
					currentProject = Project.buildFromName(result[0].getPath());
					if(currentProject.getPatterns().size()!=0) {
						newCompositPattern.setDisable(false);
						m_currentPattern = currentProject.getPatterns().get(0);
						m_currentInstrument = m_currentPattern.getInstrument();
					} else {
						newCompositPattern.setDisable(true);
						m_currentPattern = new Pattern(4000,currentProject.getCurrentPatternID() ,250,Instrument.PIANO);
						m_currentInstrument = Instrument.PIANO;
					}
					refreshGrid();
					projectDisplayGridPane.updateView(currentProject);
					undoRedoMechanism = new UndoRedoMechanism();
					statusPane.updateStatus(currentProject, m_currentPattern,
							UserInteractionsStatusPane.PROJECT_OPEN);
				} catch (ClassNotFoundException | IOException e) {
					e.printStackTrace();
				}
			}
		});
		MenuItem saveProj = new CustomMenuItem(new Label("Save Project"));
		saveProj.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent t) {
				try {
					currentProject.clearAllUsedStreams();
					Project.saveToFile(currentProject);
					for(Pattern p : currentProject.getPatterns()) {
						undoRedoMechanism.CommitChange(p);
					}
					undoRedoMechanism.CommitChange(m_currentPattern);
					statusPane.updateStatus(currentProject, m_currentPattern,
							UserInteractionsStatusPane.PROJECT_SAVED);
				} catch (IOException e) {
					e.printStackTrace();
				}	
			}
		});
		menuFile.getItems().addAll(newProj, openProj, saveProj);
		return menuFile;
	}

	private Menu initSettingsMenu(){
		Menu menuSettings= new Menu("Settings");
		MenuItem openSettingsDialog = new CustomMenuItem(new Label("Settings dialog"));
		openSettingsDialog.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent arg0) {
				SettingsDialog settingsDialog = new SettingsDialog(new ApplicationSettings());
				settingsDialog.showAndWait();
				GuiUtils.setRoot(getRoot());
				settings = settingsDialog.getNewSettings();
				if(settings!=null){
					GuiUtils.setColor(settings.getUsedColor().toString());
				}
				refreshColoredButtons();
				initiateGroups();
				m_primaryStage.setFullScreen(settings.isInFullScreenMode());
			}

		});
		menuSettings.getItems().add(openSettingsDialog);
		return menuSettings;
	}

	private Menu initExportAsMenu(){
		Menu exportAs= new Menu("Export As");
		Supplier<Project> suppliear= new Supplier<Project>(){
			@Override
			public Project get() {
				return currentProject;
			}
		};
		MenuItem openSettingsDialog = new CustomMenuItem(new Label("WAV"));
		openSettingsDialog.setOnAction(new ExportAsHandler(SoundType.WAV,suppliear));
		openSettingsDialog.addEventHandler(ActionEvent.ACTION, (e)->
			statusPane.updateStatus(currentProject, m_currentPattern, UserInteractionsStatusPane.EXPORTED_SUCCESSFULLY));
		exportAs.getItems().add(openSettingsDialog);
		return exportAs;
	}
	
	private Menu initHelpMenu() {
		Menu help = new Menu("Help");
		help.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				UserManualDisplayDialog umdd = new UserManualDisplayDialog();
				umdd.showAndWait();
				GuiUtils.setRoot(getRoot());
			}
		});
		return help;
	}
	
	private BorderPane initPatternTab(){
		patternBorderPane = new BorderPane();
		patternBorderPane.setLeft(initNewPatternTab());
		patternBorderPane.setCenter(initSoundTimeLengthPane());
		patternBorderPane.setBottom(initPatternNavigaionButtons());
		return patternBorderPane;
	}

	private HBox initNewPatternTab() {
		patternCreationTab = new HBox();
		Button newBasicPattern = new Button("New Basic Pattern");
		newBasicPattern.setOnMouseClicked(new NewPatternButtonHandler(new Consumer<Instrument>() {
			@Override
			public void accept(Instrument t) {
				createNewPattern(t);
			}
		}, getRoot()));
		newCompositPattern = new Button("Composition");
		newCompositPattern.setDisable(true);
		newCompositPattern.setOnMouseClicked(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent arg0) {
				if(projectGridMode)
					return;
				if(!currentProject.getPatterns().isEmpty()){
					saveBasicPattern();
					m_currentPattern=currentProject.getCompositePattern();
					m_currentPattern.setAllStreamsNeedsUpdate();
					getCompositPatternGridPane(currentProject.getPatternsInCurrentRange(compositionStartingPatternIndex),currentTimeSegmentOffset);
					statusPane.setPatternName(m_currentPattern.getPatternName());
				}
			}
		});
		Button clearButton = new Button ("Clear Pattern");
		clearButton.setOnMouseClicked(new EventHandler<MouseEvent>(){
			@Override
			public void handle(MouseEvent event) {
				if(projectGridMode){
					currentProject.setCompositePattern(new CompositePatternHolder(16000*4));
					m_currentPattern = currentProject.getCompositePattern();
					getCompositPatternGridPane(currentProject.getPatternsInCurrentRange(compositionStartingPatternIndex),currentTimeSegmentOffset);
				}else{
					m_currentPattern= new Pattern(patternTimeLength, m_currentPattern.getID(), 250,m_currentInstrument);
					loadPattern(m_currentInstrument,m_currentPattern);
				}
				m_currentPattern.clearAllSreamsData();
				statusPane.updateStatus(currentProject, m_currentPattern,
						UserInteractionsStatusPane.PATTERN_CLEARED);
			}
		});
		patternCreationTab.getChildren().addAll(Arrays.asList(newBasicPattern,newCompositPattern,clearButton));
		return patternCreationTab;
	}

	private Pane initiateProjectDisplayGridPane(Project currentProject) {
		Consumer<Pattern> loaderCallback = new Consumer<Pattern>() {
			@Override
			public void accept(Pattern pattern) {
				loadPattern(pattern.getInstrument(),pattern);
				m_currentInstrument = pattern.getInstrument();
				getGridPaneByInstrument(m_currentInstrument);
			}
		};
		VBox wrapper = new VBox();
		projectDisplayGridPane =  new ProjectDisplayGridPane(IterationType.ROWS, 1,
				loaderCallback,currentProject);
		wrapper.getChildren().add(new Label("Available Patterns"));
		wrapper.getChildren().add(projectDisplayGridPane);
		return wrapper;
	}
	
	private Pane initPatternNavigaionButtons() {
		if(patternControlPane==null) {
			patternControlPane = new HBox();
		}
		patternControlPane.getChildren().clear();
		ObservableList<Node> children = patternControlPane.getChildren();
		children.add(initPlayButton());
		if(projectGridMode) {
			children.addAll(getMainCompositionMovementPane());
		} else {
			children.addAll(getBasicPatternMovementPane());
		}
		Button saveButton = new Button ("Save Pattern");
		children.add(saveButton);
		saveButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent arg0) {
				if(!projectGridMode)
					saveBasicPattern();
				else
					saveCompositePattern();
				statusPane.updateStatus(currentProject, m_currentPattern, UserInteractionsStatusPane.PATTERN_SAVE);
			}

		});
		children.addAll(initUndoRedoButtons());
		return patternControlPane;
	}
	
	/** saves the composition in the project*/
	private void saveCompositePattern() {
		CompositePatternHolder $ = currentProject.getCompositePattern();
		if($.isChanged()) {
			m_currentPattern.setUpdated();
			undoRedoMechanism.CommitChange($); 
		}
		m_currentPattern.setUpdated();
	}
	
	/** saves the current pattern the user working on in the project
	 * (and displays it on the GUI)*/
	private void saveBasicPattern() {
		currentProject.addPattern(m_currentPattern);
		((ProjectDisplayGridPane)patternListDisplay.getChildren().get(1)).updateView(currentProject);
		if(m_currentPattern.isChanged())
			undoRedoMechanism.CommitChange(m_currentPattern);
		currenPatternSaved=true;
		setEnabledMode(patternWrapperPanes, true);
		newCompositPattern.setDisable(false);
		m_currentPattern.setUpdated();
	}

	private List<Node> getMainCompositionMovementPane() {
		List<Node> $ = new ArrayList<Node>();
		Button nextPatterns = new Button("Next patterns");
		nextPatterns.setOnMouseClicked(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent arg0) {
				getCompositPatternGridPane(currentProject.getPatternsInNextRange(compositionStartingPatternIndex),currentTimeSegmentOffset);
			}
		});
		Button prevPatterns = new Button("Previous patterns");
		prevPatterns.setOnMouseClicked(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				getCompositPatternGridPane(currentProject.getPatternsInPreviousRange(compositionStartingPatternIndex),currentTimeSegmentOffset);
			}

		});
		Button nextTimeSegment = new Button("Next time segment");
		nextTimeSegment.setOnMouseClicked(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				currentTimeSegmentOffset += 16;
				getCompositPatternGridPane(currentProject.getPatternsInCurrentRange(compositionStartingPatternIndex),currentTimeSegmentOffset);
			}

		});
		Button prevTimeSegment = new Button("Previous time segment");
		prevTimeSegment.setOnMouseClicked(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				if(currentTimeSegmentOffset==0) return;
				currentTimeSegmentOffset-=16;
				getCompositPatternGridPane(currentProject.getPatternsInCurrentRange(compositionStartingPatternIndex),currentTimeSegmentOffset);
			}

		});
		$.addAll(Arrays.asList(nextPatterns,prevPatterns,nextTimeSegment,prevTimeSegment));
		return $;
	}

	private List<Node> getBasicPatternMovementPane() {
		List<Node> $ = new ArrayList<Node>();
		nextButton = new Button("Next Octaves");
		nextButton.setOnMouseClicked(new ChangeOctaveButtonHandler(this, 1,currentPianoPatternOctaveView));
		prevButton = new Button("Previous Octaves");
		prevButton.setOnMouseClicked(new ChangeOctaveButtonHandler(this, -1,currentPianoPatternOctaveView));
		$.addAll(Arrays.asList(nextButton,prevButton));
		return $;
	}

	private HBox initSoundTimeLengthPane(){
		Label label= new Label("   Choose Sound Length:  ");
		soundTimeLengthOptions = new HBox();
		HBox soundTimeLengthOptionsWrapper = new HBox();
		soundTimeLengthOptionsWrapper.getChildren().add(label);
		List<Button> buttons = new ArrayList<Button>();
		for(int i=1; i<6; i++){
			Button button = new Button(soundLengthValues[i-1]);
			buttons.add(button);
			button.setOnMouseClicked(new SoundTimeButtonHandler(buttons,i-1,soundTimeLength, new Consumer<Void>() {
				@Override
				public void accept(Void t) {
					initiateGroups();
				}
			}));
			soundTimeLengthOptions.getChildren().add(button);
			if(soundTimeLength.value==Math.pow(2, i)*125){
				button.setStyle(GuiUtils.getColorString());
			}
		}
		soundTimeLengthOptionsWrapper.getChildren().add(soundTimeLengthOptions);
		return soundTimeLengthOptionsWrapper;
	}

	private Button initPlayButton(){
		Button playButton = new Button("Play");
		playButton.setOnMouseClicked(new EventHandler<Event>() {
			@Override
			public void handle(Event event) {
				try {
					if(!projectGridMode){
							m_currentPattern.play();
					} else
							currentProject.getCompositePattern().play();
					}
				catch (UnsupportedAudioFileException e) {
						e.printStackTrace();
				}
				clipDialog.showAndWait();
				GuiUtils.setRoot(getRoot());
			}
		});
		GuiUtils.setPlayButton(playButton);
		return playButton;
	}

	private List<Node> initUndoRedoButtons() {
		List<Node> $ = new ArrayList<Node>();
		Button undo = new Button("Undo");
		undo.setOnMouseClicked(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				if(!canExecuteUndoOrRedo()) return;
				if(projectGridMode) {
					if(currentProject.getCompositePattern().isChanged());
						saveCompositePattern();
					currentProject.setCompositePattern((CompositePatternHolder)undoRedoMechanism.undoChangeIn(currentProject.getCompositePattern()));
				} else {
					if(m_currentPattern.isChanged())
						saveBasicPattern();
					currentProject.getPatterns().remove(m_currentPattern);
					m_currentPattern = (Pattern)undoRedoMechanism.undoChangeIn(m_currentPattern);
					currentProject.getPatterns().add(m_currentPattern);
				}
				refreshGrid();
			}
		});
		Button redo = new Button("Redo");
		redo.setOnMouseClicked(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				if(!canExecuteUndoOrRedo()) return;
				if(projectGridMode) {
					currentProject.setCompositePattern((CompositePatternHolder)undoRedoMechanism.redoChangeIn(
							currentProject.getCompositePattern()));
				} else {
					currentProject.getPatterns().remove(m_currentPattern);
					m_currentPattern  = (Pattern)undoRedoMechanism.redoChangeIn(m_currentPattern);
					currentProject.getPatterns().add(m_currentPattern);
				}
				refreshGrid();
			}

		});
		$.addAll(Arrays.asList(undo,redo));
		return $;
	}
	
	private boolean canExecuteUndoOrRedo() {
		if(!undoRedoMechanism.wasSaved(m_currentPattern)) {
			statusPane.updateStatus(currentProject, m_currentPattern, UserInteractionsStatusPane.SAVE_BEFORE_UNDO_REDO);
			return false;
		}
		return true;
	}

	private void refreshColoredButtons(){
		patternBorderPane.setCenter(initSoundTimeLengthPane());
		refreshGrid();
	}

	private void getCompositPatternGridPane(List<Pattern> patterns,int offset) {
		projectGridMode = true;
		fillGridAndInitGroups((v)->patternGridPane.fillGridWithPatterns(patterns,soundTimeLength ,
				currentProject.getCompositePattern(),offset));
	}

	private void getGridPaneByInstrument(Instrument ins){
		projectGridMode = false;
		setEnableNextPrev(ins.isPositionBound());
		List<InstrumentSound> sounds = ins.getSoundList(currentPianoPatternOctaveView.value, soundTimeLength.value);
		List<Playable> playables = new ArrayList<Playable>(sounds);
		fillGridAndInitGroups((v)->patternGridPane.fillGridWithSounds(sounds, m_currentPattern, soundTimeLength));
	}
	
	private void fillGridAndInitGroups(Consumer<Void> fillFunction) {
		patternGridPane = new PlayableGridPane(IterationType.ROWS, 1,4);
		fillFunction.accept((Void)null);
		patternLayout.setCenter(patternGridPane);
		initiateGroups();
	}

	private void setEnableNextPrev(boolean enable){
		nextButton.setDisable(!enable);
		prevButton.setDisable(!enable);
	}

}
