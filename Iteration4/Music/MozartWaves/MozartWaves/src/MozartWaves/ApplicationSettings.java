package MozartWaves;

import java.util.Arrays;
import java.util.List;

/**
 * Encapsulates the settings of the application which the user can control.
 * @author Sergey
 *
 */
public class ApplicationSettings {
	
	private static final List<Object> markingColors =
			Arrays.asList(MarkableColor.Red,MarkableColor.Blue,MarkableColor.Green,
					MarkableColor.Purple,MarkableColor.Yellow);
	
	private static final List<Object> enabledDisabled =
			Arrays.asList(true,false);
	
	private MarkableColor usedColor;
	private boolean quickReloadEnabled;
	private boolean isInFullScreen;
	
	public ApplicationSettings() {
		usedColor = MarkableColor.Red;
		quickReloadEnabled = false;
		isInFullScreen = false;
	}
	
	public static List<Object> getPossibleMarkingColors() {
		return markingColors;
	}
	
	public static List<Object> getBooleanList() {
		return enabledDisabled;
	}
	
	public ApplicationSettings setUsedColor(MarkableColor color) {
		this.usedColor = color;
		return this;
	}
	
	public ApplicationSettings setQuickReloadEnabled(boolean enabled) {
		this.quickReloadEnabled = enabled;
		return this;
	}
	
	public ApplicationSettings setFullScreenMode(boolean enabled) {
		this.isInFullScreen = enabled;
		return this;
	}
	
	public MarkableColor getUsedColor() {
		return usedColor;
	}

	public boolean isQuickReloadEnabled() {
		return quickReloadEnabled;
	}
	
	public boolean isInFullScreenMode() {
		return isInFullScreen;
	}
	
}
