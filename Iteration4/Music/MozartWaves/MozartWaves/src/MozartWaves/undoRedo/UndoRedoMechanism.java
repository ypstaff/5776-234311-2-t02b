package MozartWaves.undoRedo;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;
import java.util.Map;

import Playable.Playable;

/**
 * 
 * @author Sergey
 *
 */
public class UndoRedoMechanism {
	
	private Map<Integer,UndoRedoStack<byte[]>> stateMap;
	
	public UndoRedoMechanism() {
		stateMap = new HashMap<Integer,UndoRedoStack<byte[]>>();
	}
	
	
	
	public Playable undoChangeIn(Playable p ) {
		Playable $ = undoChangeIn(p.getID());
		return makeResult(p,$);
	}
	
	public Playable redoChangeIn(Playable p ) {
		Playable $ = redoChangeIn(p.getID());
		return makeResult(p,$);
	}
	
	private Playable makeResult(Playable oldVal, Playable newVal) {
		if(newVal!=null)
			return newVal;
		return oldVal;
	}
	
	/**
	 * returns the last committed state of <i>p</i>
	 * @param p - the playable which is used a key, and whose previous state we want.
	 * @return the previous state of <i>p</i> or null if no such state exists.
	 */
	private Playable undoChangeIn(int id) {
		if(stateMap.get(id)==null) {
			return null;
		}
		UndoRedoStack<byte[]> undoRedoStack = stateMap.get(id);
		byte[] byteState = undoRedoStack.undo();
		if(byteState == null) return null;
		Playable $ = playableFromByteArray(byteState);
		return $;
	}
	
	/**
	 * returns the next committed state of <i>p</i> (this makes sense if we made an undo action)
	 * @param p - the playable which is used a key, and whose previous state we want.
	 * @return the next committed state of <i>p</i>, or null if no such state exists.
	 */
	private Playable redoChangeIn(int id) {
		if(stateMap.get(id) == null) {
			return null;
		}
		UndoRedoStack<byte[]> undoRedoStack = stateMap.get(id);
		byte[] byteState = undoRedoStack.redo();
		if(byteState == null) return null;
		Playable $ = playableFromByteArray(byteState);
		return $;
	}
	
	/**
	 * commits the change in the playable p - this will invalidate
	 * all the commited states which are ahead of this point.
	 * @param p
	 */
	public void CommitChange(Playable p) {
		byte[] state = playableToByteArray(p);
		int id = p.getID();
		if(stateMap.get(id)==null) {
			UndoRedoStack<byte[]> stack = new UndoRedoStack<byte[]>();
			stack.commit(state);
			stateMap.put(id, stack);
		} else {
			stateMap.get(id).commit(state);
		}
	}
	
	/**
	 * 
	 * @param p - the playable which is used a key, and whose previous state we want.
	 * @return the last committed state of p, or null if no such state exists.
	 */
	public byte[] getCurrentCommitedState(Playable p) {
		if(stateMap.get(p)==null) return null;
		return stateMap.get(p).getCurrentCommitedState();
	}
	/**
	 * converts p to a byte array by serialization.
	 * @param p
	 * @return
	 */
	public static byte[] playableToByteArray(Playable p) {
		try {
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			ObjectOutputStream oos = new ObjectOutputStream(baos);
			oos.writeObject(p);
			baos.close();
			oos.close();
			return baos.toByteArray();
		} catch(IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * converts an array of bytes to a playable by deserialization.
	 * @param bytes
	 * @return
	 */
	public static Playable playableFromByteArray(byte[] bytes) {
		try {
			ObjectInputStream ois = new ObjectInputStream(new ByteArrayInputStream(bytes));
			Playable $ = (Playable)ois.readObject();
			ois.close();
			return $;
		} catch(IOException | ClassNotFoundException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public boolean wasSaved(Playable p) {
		return this.stateMap.get(p.getID())!=null;
	}
}
