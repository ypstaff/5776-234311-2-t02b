package MozartWaves.undoRedo;

import java.util.Stack;

/**
 * 
 * @author Sergey
 *
 * @param <T> type of elements inserted into the stack.
 * an auxiliary ds for the UndoRedoMechanism class.
 */
public class UndoRedoStack<T> {
	
	private Stack<T> undoStack;
	private Stack<T> redoStack;
	
	public UndoRedoStack() {
		undoStack = new Stack<T>();
		redoStack = new Stack<T>();
	}
	
	/**
	 * 
	 * @return previous saved state, or null if no such states exist.
	 */
	public T redo() {
		if(redoStack.size()==0)
			return null;
		doExchange(redoStack,undoStack);
		if(undoStack.size()==0) return null;
		return undoStack.peek();
	}
	
	/**
	 * 
	 * @return next saved state, or null if no such states exist.
	 */
	public T undo() {
		if(undoStack.size()==0) 
			return null;
		doExchange(undoStack,redoStack);
		if(undoStack.size()==0) return null;
		return undoStack.peek();
	}
	
	/**
	 * exchanges values between two stacks.
	 * @param popFrom
	 * @param pushTo
	 * @return
	 */
	private void doExchange(Stack<T> popFrom, Stack<T> pushTo) {
		T $ = popFrom.pop();
		pushTo.push($);
	}
	
	/**
	 * saves the given state as the top of the undo stack, and removes all states from redo stack.
	 * @param state
	 */
	public void commit(T state) {
		undoStack.push(state);
		redoStack.clear();
	}
	
	/**
	 * @return - the which was pushed into undoStack last.
	 */
	public T getCurrentCommitedState() {
		if(undoStack.isEmpty()) return null;
		return undoStack.peek();
	}

}
