package MozartWaves.Pojos;
/**
 * @author RomanM
 * The name speaks for itself
 */

public enum MusicalNote implements AbstractMusicalValue {
	C, Db, D, Eb, E, F, Gb, G, Ab, A, Bb, B
}
