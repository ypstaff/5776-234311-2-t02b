/**
 * 
 */
package MozartWaves.Pojos;

/**
 * @author RomanM
 * Chords of guitars/piano/etc.
 */
public enum Chord implements AbstractMusicalValue {
	Am7,Em7,A,A7,Am,C,D,D7,Dm,E,E7,Gm,G,B,B7,Bm,C7,Cm,F,F7,Fm,G7
}
