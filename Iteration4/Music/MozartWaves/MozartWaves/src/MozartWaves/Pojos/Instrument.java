/**
 * 
 */
package MozartWaves.Pojos;

import java.util.List;

import Playable.InstrumentSound;
import Playable.SoundManager;

/**
 * @author RomanM
 * An instrument Enum - can easily add instrument here
 */
public enum Instrument {
	PIANO("Piano") {
		@Override
		public boolean isPositionBound() {
			return false;
		}
	}, ELECTRIC_GUITAR_CHORDS("Electric Guitar Chords") {
		@Override
		public boolean isPositionBound() {
			return true;
		}
	}, CLASSIC_GUITAR_CHORDS("Classic Guitar Chords") {
		@Override
		public boolean isPositionBound() {
			return true;
		}
	};

	
	private String name; 
    private Instrument(String name) { 
        this.name = name; 
    } 

    @Override 
    public String toString(){ 
        return name; 
    } 
    
    public List<InstrumentSound> getSoundList(int from, int soundTimeLength) {
    	switch(this) {
		case CLASSIC_GUITAR_CHORDS:
			return SoundManager.classicGuitarSounds();
		case ELECTRIC_GUITAR_CHORDS:
			return SoundManager.electricGuitarSounds();
		case PIANO:
			return SoundManager.pianoSounds(from, soundTimeLength);
		default:
			return null;
  
    	}
    		
    }
    
    //TODO: think of a better name for this.
    public abstract boolean isPositionBound();
}
