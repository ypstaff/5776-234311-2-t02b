/**
 * 
 */
package MozartWaves.Pojos;

/**
 * @author RomanM
 * Music type (mp3/wav/etc.) easily modifiable
 */
public enum SoundType {
	WAV;
}
