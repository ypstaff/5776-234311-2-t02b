package yearlyproject.programming.keyboards;

import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

import yearlyproject.programming.keyboard.KeyBoard;

/**
 * Unit tests for the Numbers Keyboard
 * @author Akiva Herer
 * Tests all the numbers to make sure they are where they are supposed to be, 
 * and output what they should. Also makes sure that the navigation doesn't break. 
 *
 */

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@RunWith(JavaFxJUnit4ClassRunnerKeyBoardNumbers.class)
public class UnitTestsKeyboardNumbers {

		//row1
		@Test
		public void row1test1() {
			JavaFxJUnit4ApplicationKeyboardNumbers.root.select();
			JavaFxJUnit4ApplicationKeyboardNumbers.root.select();
			Assert.assertTrue(JavaFxJUnit4ApplicationKeyboardNumbers.k.received("0 "));
		}
		
		
		@Test
		public void row1test2() {
			JavaFxJUnit4ApplicationKeyboardNumbers.root.next();
			JavaFxJUnit4ApplicationKeyboardNumbers.root.select();
			Assert.assertTrue(JavaFxJUnit4ApplicationKeyboardNumbers.k.received("1 "));
		}
		
		@Test
		public void row1test3() {
			JavaFxJUnit4ApplicationKeyboardNumbers.root.next();
			JavaFxJUnit4ApplicationKeyboardNumbers.root.select();
			Assert.assertTrue(JavaFxJUnit4ApplicationKeyboardNumbers.k.received("2 "));
		}
		

		//row2
		@Test
		public void row2test1() {
			JavaFxJUnit4ApplicationKeyboardNumbers.root.unselect();
			JavaFxJUnit4ApplicationKeyboardNumbers.root.next();
			JavaFxJUnit4ApplicationKeyboardNumbers.root.select();
			JavaFxJUnit4ApplicationKeyboardNumbers.root.select();
			Assert.assertTrue(JavaFxJUnit4ApplicationKeyboardNumbers.k.received("3 "));
		}
		
		@Test
		public void row2test2() {
			JavaFxJUnit4ApplicationKeyboardNumbers.root.next();
			JavaFxJUnit4ApplicationKeyboardNumbers.root.select();
			Assert.assertTrue(JavaFxJUnit4ApplicationKeyboardNumbers.k.received("4 "));
		}
		
		@Test
		public void row2test3() {
			JavaFxJUnit4ApplicationKeyboardNumbers.root.next();
			JavaFxJUnit4ApplicationKeyboardNumbers.root.select();
			Assert.assertTrue(JavaFxJUnit4ApplicationKeyboardNumbers.k.received("5 "));
		}
	
	
		//row3
		@Test
		public void row3test1() {
			JavaFxJUnit4ApplicationKeyboardNumbers.root.unselect();
			JavaFxJUnit4ApplicationKeyboardNumbers.root.next();
			JavaFxJUnit4ApplicationKeyboardNumbers.root.select();
			JavaFxJUnit4ApplicationKeyboardNumbers.root.select();
			Assert.assertTrue(JavaFxJUnit4ApplicationKeyboardNumbers.k.received("6 "));
		}
		
		@Test
		public void row3test2() {
			JavaFxJUnit4ApplicationKeyboardNumbers.root.next();
			JavaFxJUnit4ApplicationKeyboardNumbers.root.select();
			Assert.assertTrue(JavaFxJUnit4ApplicationKeyboardNumbers.k.received("7 "));
		}
		
		@Test
		public void row3test3() {
			JavaFxJUnit4ApplicationKeyboardNumbers.root.next();
			JavaFxJUnit4ApplicationKeyboardNumbers.root.select();
			Assert.assertTrue(JavaFxJUnit4ApplicationKeyboardNumbers.k.received("8 "));
		}
		
	
		//row4
		@Test
		public void row4test1() {
			JavaFxJUnit4ApplicationKeyboardNumbers.root.unselect();
			JavaFxJUnit4ApplicationKeyboardNumbers.root.next();
			JavaFxJUnit4ApplicationKeyboardNumbers.root.select();
			JavaFxJUnit4ApplicationKeyboardNumbers.root.select();
			Assert.assertTrue(JavaFxJUnit4ApplicationKeyboardNumbers.k.received("9 "));
		}
				
		@Test
		public void row4test2() {
			JavaFxJUnit4ApplicationKeyboardNumbers.root.next();
			JavaFxJUnit4ApplicationKeyboardNumbers.root.select();
			Assert.assertTrue(JavaFxJUnit4ApplicationKeyboardNumbers.k.received(KeyBoard.ENTER+" "));
		}
		
		//row5
		@Test
		public void row5test1() {
			JavaFxJUnit4ApplicationKeyboardNumbers.root.unselect();
			JavaFxJUnit4ApplicationKeyboardNumbers.root.next();
			JavaFxJUnit4ApplicationKeyboardNumbers.root.select();
			Assert.assertTrue(JavaFxJUnit4ApplicationKeyboardNumbers.k.received(KeyBoard.BACKSPACE+" "));
		}
}
