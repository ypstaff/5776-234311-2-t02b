package yearlyproject.programming.keyboards;
import javafx.application.Application;
import javafx.embed.swing.JFXPanel;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.stage.Screen;
import javafx.stage.Stage;

import javax.swing.JFrame;

import org.junit.BeforeClass;
import org.junit.Test;

import yearlyproject.programming.common.EchoModule;
import yearlyproject.programming.common.EchoSender;
import yearlyproject.programming.keyboard.KeyBoard;
import Navigation.IterableNodeGroups;

import com.google.inject.Guice;

/**
 * A way to launch and test the Keyboard
 * @author Akiva Herer
 *
 */
public class LaunchKeyboard extends Application{
	
	@Override
	public void start(Stage primaryStage) throws Exception {
		JFrame frame = new JFrame("test");
		final JFXPanel fxPanel = new JFXPanel();
		KeyBoard eg = new KeyBoard(Guice.createInjector(new EchoModule()).getInstance(EchoSender.class), (x)->{}, (x)->{},null);

		
		IterableNodeGroups root = eg.getIterableRoot();
		fxPanel.setScene(new Scene(root.getParentPane()));
		
		frame.add(fxPanel);
		frame.setFocusableWindowState(false);
		frame.setVisible(true);
		
		//set frame size to fit half the screen
		Rectangle2D bounds = Screen.getPrimary().getVisualBounds();
		frame.setSize((int) bounds.getWidth(), (int) bounds.getHeight()/2);
		frame.setLocation(0, (int) bounds.getHeight()/2);
		
		root.start();
	}
	
	//launching the Keyboard
	@BeforeClass
	public static void launchGui() {
		launch();
	}
	
	@Test
	//junit runner need tests to run
	public void nothing(){
		
	}

}
