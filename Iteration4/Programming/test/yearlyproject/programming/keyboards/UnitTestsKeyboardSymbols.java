package yearlyproject.programming.keyboards;

import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

import yearlyproject.programming.keyboard.KeyBoard;

/**
 * Unit tests for the Symbol Keyboard
 * @author Akiva Herer
 * Tests all the symbols to make sure they are where they are supposed to be, 
 * and output what they should. Also makes sure that the navigation doesn't break. 
 *
 */

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@RunWith(JavaFxJUnit4ClassRunnerKeyBoardSymbols.class)
public class UnitTestsKeyboardSymbols {

	//row1
	@Test
	public void row1test1() {
		JavaFxJUnit4ApplicationKeyboardSymbols.root.select();
		JavaFxJUnit4ApplicationKeyboardSymbols.root.select();
		JavaFxJUnit4ApplicationKeyboardSymbols.root.select();
		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboardSymbols.k.received("( "));
	}
	
	
	@Test
	public void row1test2() {
		JavaFxJUnit4ApplicationKeyboardSymbols.root.next();
		JavaFxJUnit4ApplicationKeyboardSymbols.root.select();
		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboardSymbols.k.received(") "));
	}
	
	@Test
	public void row1test3() {
		JavaFxJUnit4ApplicationKeyboardSymbols.root.unselect();
		JavaFxJUnit4ApplicationKeyboardSymbols.root.next();
		JavaFxJUnit4ApplicationKeyboardSymbols.root.select();
		JavaFxJUnit4ApplicationKeyboardSymbols.root.select();
		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboardSymbols.k.received("\" "));
	}
	
	@Test
	public void row1test4() {
		JavaFxJUnit4ApplicationKeyboardSymbols.root.next();
		JavaFxJUnit4ApplicationKeyboardSymbols.root.select();
		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboardSymbols.k.received("! "));
	}
	
	@Test
	public void row1test5() {
		JavaFxJUnit4ApplicationKeyboardSymbols.root.unselect();
		JavaFxJUnit4ApplicationKeyboardSymbols.root.next();
		JavaFxJUnit4ApplicationKeyboardSymbols.root.select();
		JavaFxJUnit4ApplicationKeyboardSymbols.root.select();
		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboardSymbols.k.received("{ "));
	}
	
	@Test
	public void row1test6() {
		JavaFxJUnit4ApplicationKeyboardSymbols.root.next();
		JavaFxJUnit4ApplicationKeyboardSymbols.root.select();
		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboardSymbols.k.received("} "));
	}

	//row2
	@Test
	public void row2test1() {
		JavaFxJUnit4ApplicationKeyboardSymbols.root.unselect();
		JavaFxJUnit4ApplicationKeyboardSymbols.root.unselect();
		JavaFxJUnit4ApplicationKeyboardSymbols.root.next();
		JavaFxJUnit4ApplicationKeyboardSymbols.root.select();
		JavaFxJUnit4ApplicationKeyboardSymbols.root.select();
		JavaFxJUnit4ApplicationKeyboardSymbols.root.select();
		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboardSymbols.k.received("? "));
	}
	
	@Test
	public void row2test2() {
		JavaFxJUnit4ApplicationKeyboardSymbols.root.next();
		JavaFxJUnit4ApplicationKeyboardSymbols.root.select();
		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboardSymbols.k.received("& "));
	}
	
	@Test
	public void row2test3() {
		JavaFxJUnit4ApplicationKeyboardSymbols.root.unselect();
		JavaFxJUnit4ApplicationKeyboardSymbols.root.next();
		JavaFxJUnit4ApplicationKeyboardSymbols.root.select();
		JavaFxJUnit4ApplicationKeyboardSymbols.root.select();
		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboardSymbols.k.received("* "));
	}
	
	@Test
	public void row2test4() {
		JavaFxJUnit4ApplicationKeyboardSymbols.root.next();
		JavaFxJUnit4ApplicationKeyboardSymbols.root.select();
		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboardSymbols.k.received("+ "));
	}
	
	@Test
	public void row2test5() {
		JavaFxJUnit4ApplicationKeyboardSymbols.root.unselect();
		JavaFxJUnit4ApplicationKeyboardSymbols.root.next();
		JavaFxJUnit4ApplicationKeyboardSymbols.root.select();
		JavaFxJUnit4ApplicationKeyboardSymbols.root.select();
		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboardSymbols.k.received("@ "));
	}
	
	@Test
	public void row2test6() {
		JavaFxJUnit4ApplicationKeyboardSymbols.root.next();
		JavaFxJUnit4ApplicationKeyboardSymbols.root.select();
		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboardSymbols.k.received("^ "));
	}
	
	//row3
	@Test
	public void row3test1() {
		JavaFxJUnit4ApplicationKeyboardSymbols.root.unselect();
		JavaFxJUnit4ApplicationKeyboardSymbols.root.unselect();
		JavaFxJUnit4ApplicationKeyboardSymbols.root.next();
		JavaFxJUnit4ApplicationKeyboardSymbols.root.select();
		JavaFxJUnit4ApplicationKeyboardSymbols.root.select();
		JavaFxJUnit4ApplicationKeyboardSymbols.root.select();
		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboardSymbols.k.received("$ "));
	}
	
	@Test
	public void row3test2() {
		JavaFxJUnit4ApplicationKeyboardSymbols.root.next();
		JavaFxJUnit4ApplicationKeyboardSymbols.root.select();
		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboardSymbols.k.received("% "));
	}
	
	@Test
	public void row3test3() {
		JavaFxJUnit4ApplicationKeyboardSymbols.root.unselect();
		JavaFxJUnit4ApplicationKeyboardSymbols.root.next();
		JavaFxJUnit4ApplicationKeyboardSymbols.root.select();
		JavaFxJUnit4ApplicationKeyboardSymbols.root.select();
		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboardSymbols.k.received("_ "));
	}
	
	@Test
	public void row3test4() {
		JavaFxJUnit4ApplicationKeyboardSymbols.root.next();
		JavaFxJUnit4ApplicationKeyboardSymbols.root.select();
		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboardSymbols.k.received("~ "));
	}
	
	@Test
	public void row3test5() {
		JavaFxJUnit4ApplicationKeyboardSymbols.root.unselect();
		JavaFxJUnit4ApplicationKeyboardSymbols.root.next();
		JavaFxJUnit4ApplicationKeyboardSymbols.root.select();
		JavaFxJUnit4ApplicationKeyboardSymbols.root.select();
		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboardSymbols.k.received("| "));
	}
	
	@Test
	public void row3test6() {
		JavaFxJUnit4ApplicationKeyboardSymbols.root.next();
		JavaFxJUnit4ApplicationKeyboardSymbols.root.select();
		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboardSymbols.k.received(": "));
	}
	
	//row4
	@Test
	public void row4test1() {
		JavaFxJUnit4ApplicationKeyboardSymbols.root.unselect();
		JavaFxJUnit4ApplicationKeyboardSymbols.root.unselect();
		JavaFxJUnit4ApplicationKeyboardSymbols.root.next();
		JavaFxJUnit4ApplicationKeyboardSymbols.root.select();
		JavaFxJUnit4ApplicationKeyboardSymbols.root.select();
		JavaFxJUnit4ApplicationKeyboardSymbols.root.select();
		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboardSymbols.k.received("# "));
	}
	
	@Test
	public void row4test2() {
		JavaFxJUnit4ApplicationKeyboardSymbols.root.next();
		JavaFxJUnit4ApplicationKeyboardSymbols.root.select();
		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboardSymbols.k.received("< "));
	}
	
	@Test
	public void row4test3() {
		JavaFxJUnit4ApplicationKeyboardSymbols.root.unselect();
		JavaFxJUnit4ApplicationKeyboardSymbols.root.next();
		JavaFxJUnit4ApplicationKeyboardSymbols.root.select();
		JavaFxJUnit4ApplicationKeyboardSymbols.root.select();
		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboardSymbols.k.received("> "));
	}
	
	@Test
	public void row4test4() {
		JavaFxJUnit4ApplicationKeyboardSymbols.root.unselect();
		JavaFxJUnit4ApplicationKeyboardSymbols.root.next();
		JavaFxJUnit4ApplicationKeyboardSymbols.root.select();
		JavaFxJUnit4ApplicationKeyboardSymbols.root.select();
		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboardSymbols.k.received(KeyBoard.BACKSPACE+" "));
	}
}
