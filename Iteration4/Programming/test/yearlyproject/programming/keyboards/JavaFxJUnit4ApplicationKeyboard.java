package yearlyproject.programming.keyboards;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.ReentrantLock;

import javafx.application.Application;
import javafx.embed.swing.JFXPanel;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.stage.Screen;
import javafx.stage.Stage;

import javax.swing.JFrame;

import yearlyproject.programming.common.TestModule;
import yearlyproject.programming.common.TestSender;
import yearlyproject.programming.keyboard.KeyBoard;
import Navigation.IterableNodeGroups;

import com.google.inject.Guice;

/**
 * This is the application which starts JavaFx.  It is controlled through the startJavaFx() method.
 * @author Akiva Herer (code mostly taken from stack over flow)
 */

public class JavaFxJUnit4ApplicationKeyboard  extends Application
{

    /** The lock that guarantees that only one JavaFX thread will be started. */
    private static final ReentrantLock LOCK = new ReentrantLock();

    /** Started flag. */
    private static AtomicBoolean started = new AtomicBoolean();

  //Constants for use in Keyboard unit tests
    public static KeyBoard eg;
    public static IterableNodeGroups root;
    public static TestSender k;
    
    /**
     * Start JavaFx.
     */
    public static void startJavaFx()
    {
        try
        {
            // Lock or wait.  This gives another call to this method time to finish
            // and release the lock before another one has a go
            LOCK.lock();

            if (!started.get())
            {
                // start the JavaFX application
                final ExecutorService executor = Executors.newSingleThreadExecutor();
                executor.execute(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        JavaFxJUnit4ApplicationKeyboard.launch();
                    }
                });

                while (!started.get())
                {
                    Thread.yield();
                }
            }
        }
        finally
        {
            LOCK.unlock();
        }
    }

    /**
     * Launch.
     */
    protected static void launch()
    {
        Application.launch();
    }

    /**
     * An empty start method.
     *
     * @param stage The stage
     */
    @Override
    public void start(final Stage stage)
    {
        started.set(Boolean.TRUE);
        
		JFrame frame = new JFrame("test");
		final JFXPanel fxPanel = new JFXPanel();
		 k=Guice.createInjector(new TestModule()).getInstance(TestSender.class);
		 eg =  new KeyBoard(k, (x)->{}, (x)->{},null);
	
		root = eg.getIterableRoot();
		fxPanel.setScene(new Scene(root.getParentPane()));
		
		frame.add(fxPanel);
		frame.setFocusableWindowState(false);
		frame.setVisible(true);
		
		//set frame size to fit half the screen
		Rectangle2D bounds = Screen.getPrimary().getVisualBounds();
		frame.setSize((int) bounds.getWidth(), (int) bounds.getHeight()/2);
		frame.setLocation(0, (int) bounds.getHeight()/2);
		
		root.start();
        
        
    }
}