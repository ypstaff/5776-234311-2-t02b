package yearlyproject.programming.keyboards;

import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

import yearlyproject.programming.keyboard.KeyBoard;

/**
 * Unit tests for the Keyboard
 * @author Akiva Herer
 * Tests all the buttons to make sure they are where they are supposed to be, 
 * and output what they should. Also makes sure that the navigation doesn't break. 
 *
 */

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@RunWith(JavaFxJUnit4ClassRunnerKeyBoard.class)
public class UnitTestsKeyboard {

	//function for selecting next button in group
	public void nextInGroup(){
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.select();
	}

	//function for selecting next button in next group
	public void nextGroupFirst(){
		JavaFxJUnit4ApplicationKeyboard.root.unselect();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.select();
		JavaFxJUnit4ApplicationKeyboard.root.select();
	}

	//function for selecting next button in next row
	public void nextRowFirst(){
		JavaFxJUnit4ApplicationKeyboard.root.unselect();
		JavaFxJUnit4ApplicationKeyboard.root.unselect();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.select();
		JavaFxJUnit4ApplicationKeyboard.root.select();
		JavaFxJUnit4ApplicationKeyboard.root.select();
	}

	//function for selecting next button in next row of special buttons
	public void nextRowFirstSpecial(){
		JavaFxJUnit4ApplicationKeyboard.root.unselect();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.select();
		JavaFxJUnit4ApplicationKeyboard.root.select();
	}

	//function for selecting next button in next row, and skipping over the first button
	private void nextRowFirstPlus1() {
		JavaFxJUnit4ApplicationKeyboard.root.unselect();
		JavaFxJUnit4ApplicationKeyboard.root.unselect();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.select();
		JavaFxJUnit4ApplicationKeyboard.root.select();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.select();
	}

	/*
	 * Testing all the buttons, not using special case buttons such as shift.
	 */

	//row1
	@Test
	public void row1test1() {
		JavaFxJUnit4ApplicationKeyboard.root.unselect();
		JavaFxJUnit4ApplicationKeyboard.root.unselect();
		JavaFxJUnit4ApplicationKeyboard.root.select();
		JavaFxJUnit4ApplicationKeyboard.root.select();
		JavaFxJUnit4ApplicationKeyboard.root.select();
		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboard.k.received(KeyBoard.ARROW_UP+" "));
	}

	@Test
	public void row1test2() {
		nextInGroup();
		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboard.k.received(KeyBoard.ARROW_DOWN+" "));
	}

	@Test
	public void row1test3() {
		nextInGroup();
		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboard.k.received(KeyBoard.ARROW_LEFT+" "));
	}

	@Test
	public void row1test4() {
		nextInGroup();
		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboard.k.received(KeyBoard.ARROW_RIGHT+" "));
	}
	@Test
	public void row1test5() {
		nextGroupFirst();
		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboard.k.received(KeyBoard.SPACE+" "));
	}
	@Test
	public void row1test6() {
		nextInGroup();
		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboard.k.received(KeyBoard.ENTER+" "));
	}
	@Test
	public void row1test7() {
		nextInGroup();
		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboard.k.received(KeyBoard.ESCAPE+" "));
	}
	@Test
	public void row1test8() {
		nextInGroup();
		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboard.k.received(KeyBoard.DELETE+" "));
	}
	@Test
	public void row1test9() {
		nextGroupFirst();
		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboard.k.received(KeyBoard.BACKSPACE+" "));
	}

	//row2
	@Test
	public void row2part1test1() {
		nextRowFirst();
		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboard.k.received("` "));
	}

	@Test
	public void row2part1test2() {
		nextInGroup();
		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboard.k.received("1 "));
	}

	@Test
	public void row2part1test3() {
		nextInGroup();
		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboard.k.received("2 "));
	}

	@Test
	public void row2part1test4() {
		nextInGroup();
		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboard.k.received("3 "));
	}
	@Test
	public void row2part1test5() {
		nextGroupFirst();
		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboard.k.received("4 "));
	}
	@Test
	public void row2part1test6() {
		nextInGroup();
		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboard.k.received("5 "));
	}
	@Test
	public void row2part1test7() {
		nextInGroup();
		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboard.k.received("6 "));
	}
	@Test
	public void row2part1test8() {
		nextInGroup();
		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboard.k.received("7 "));
	}
	@Test
	public void row2part1test9() {
		nextGroupFirst();
		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboard.k.received("8 "));
	}
	@Test
	public void row2part2test1() {
		nextInGroup();
		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboard.k.received("9 "));
	}

	@Test
	public void row2part2test2() {
		nextInGroup();
		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboard.k.received("0 "));
	}

	@Test
	public void row2part2test3() {
		nextInGroup();
		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboard.k.received("- "));
	}

	@Test
	public void row2part2test4() {
		nextGroupFirst();
		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboard.k.received("= "));
	}

	//row3
	@Test
	public void row3part1test1() {
		nextRowFirst();
		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboard.k.received(KeyBoard.TAB+" "));
	}

	@Test
	public void row3part1test2() {
		nextInGroup();
		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboard.k.received("q "));
	}

	@Test
	public void row3part1test3() {
		nextInGroup();
		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboard.k.received("w "));
	}

	@Test
	public void row3part1test4() {
		nextInGroup();
		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboard.k.received("e "));
	}
	@Test
	public void row3part1test5() {
		nextGroupFirst();
		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboard.k.received("r "));
	}
	@Test
	public void row3part1test6() {
		nextInGroup();
		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboard.k.received("t "));
	}
	@Test
	public void row3part1test7() {
		nextInGroup();
		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboard.k.received("y "));
	}
	@Test
	public void row3part1test8() {
		nextInGroup();
		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboard.k.received("u "));
	}
	@Test
	public void row3part1test9() {
		nextGroupFirst();
		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboard.k.received("i "));
	}
	@Test
	public void row3part2test1() {
		nextInGroup();
		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboard.k.received("o "));
	}

	@Test
	public void row3part2test2() {
		nextInGroup();
		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboard.k.received("p "));
	}

	@Test
	public void row3part2test3() {
		nextInGroup();
		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboard.k.received("[ "));
	}

	@Test
	public void row3part2test4() {
		nextGroupFirst();
		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboard.k.received("] "));
	}
	@Test
	public void row3part2test5() {
		nextInGroup();
		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboard.k.received("\\ "));
	}

	//row4
	@Test
	public void row4part1test1() {
		nextRowFirstPlus1();
		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboard.k.received("a "));
	}


	@Test
	public void row4part1test2() {
		nextInGroup();
		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboard.k.received("s "));
	}

	@Test
	public void row4part1test3() {
		nextInGroup();
		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboard.k.received("d "));
	}

	@Test
	public void row4part1test4() {
		nextGroupFirst();
		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboard.k.received("f "));
	}
	@Test
	public void row4part1test5() {
		nextInGroup();
		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboard.k.received("g "));
	}
	@Test
	public void row4part1test6() {
		nextInGroup();
		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboard.k.received("h "));
	}
	@Test
	public void row4part1test7() {
		nextInGroup();
		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboard.k.received("j "));
	}
	@Test
	public void row4part1test8() {
		nextGroupFirst();
		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboard.k.received("k "));
	}
	@Test
	public void row4part1test9() {
		nextInGroup();
		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboard.k.received("l "));
	}
	@Test
	public void row4part2test1() {
		nextInGroup();
		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboard.k.received("; "));
	}

	@Test
	public void row4part2test2() {
		nextInGroup();
		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboard.k.received("' "));
	}

	//row5
	@Test
	public void row5part1test1() {
		nextRowFirstPlus1();
		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboard.k.received("z "));
	}


	@Test
	public void row5part1test2() {
		nextInGroup();
		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboard.k.received("x "));
	}

	@Test
	public void row5part1test3() {
		nextInGroup();
		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboard.k.received("c "));
	}

	@Test
	public void row5part1test4() {
		nextGroupFirst();
		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboard.k.received("v "));
	}
	@Test
	public void row5part1test5() {
		nextInGroup();
		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboard.k.received("b "));
	}
	@Test
	public void row5part1test6() {
		nextInGroup();
		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboard.k.received("n "));
	}
	@Test
	public void row5part1test7() {
		nextInGroup();
		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboard.k.received("m "));
	}
	@Test
	public void row5part1test8() {
		nextGroupFirst();
		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboard.k.received(", "));
	}
	@Test
	public void row5part1test9() {
		nextInGroup();
		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboard.k.received(". "));
	}
	@Test
	public void row5part2test1() {
		nextInGroup();
		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboard.k.received("/ "));
	}

	@Test
	public void row5part2test2() {
		nextInGroup();
		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboard.k.received(KeyBoard.PAGE_UP+" "));
	}

	@Test
	public void row5part2test3() {
		nextGroupFirst();
		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboard.k.received(KeyBoard.PAGE_DOWN+" "));
	}

	//row 6
	@Test
	public void row6test1() {
		JavaFxJUnit4ApplicationKeyboard.root.unselect();
		JavaFxJUnit4ApplicationKeyboard.root.unselect();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.select();
		JavaFxJUnit4ApplicationKeyboard.root.select();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.select();
		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboard.k.received(KeyBoard.SPACE+" "));
	}

	@Test
	public void row6test2() {
		nextInGroup();
		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboard.k.received(KeyBoard.PRINT_SCREEN+" "));
	}


	//row7
	@Test
	public void row7part1test1() {
		nextRowFirst();
		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboard.k.received("F1 "));
	}


	@Test
	public void row7part1test2() {
		nextInGroup();
		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboard.k.received("F2 "));
	}

	@Test
	public void row7part1test3() {
		nextInGroup();
		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboard.k.received("F3 "));
	}

	@Test
	public void row7part1test4() {
		nextInGroup();
		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboard.k.received("F4 "));
	}
	@Test
	public void row7part1test5() {
		nextGroupFirst();
		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboard.k.received("F5 "));
	}
	@Test
	public void row7part1test6() {
		nextInGroup();
		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboard.k.received("F6 "));
	}
	@Test
	public void row7part1test7() {
		nextInGroup();
		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboard.k.received("F7 "));
	}
	@Test
	public void row7part1test8() {
		nextInGroup();		
		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboard.k.received("F8 "));
	}
	@Test
	public void row7part1test9() {
		nextGroupFirst();
		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboard.k.received("F9 "));
	}
	@Test
	public void row7part2test1() {
		nextInGroup();
		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboard.k.received("F10 "));
	}

	@Test
	public void row7part2test2() {
		nextInGroup();
		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboard.k.received("F11 "));
	}

	@Test
	public void row7part2test3() {
		nextInGroup();
		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboard.k.received("F12 "));
	}

	@Test
	public void row7part2test4() {
		nextGroupFirst();
		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboard.k.received(KeyBoard.HOME+" "));
	}

	@Test
	public void row7part2test5() {
		nextInGroup();
		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboard.k.received(KeyBoard.END+" "));
	}

	@Test
	public void row7part2test6() {
		nextInGroup();
		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboard.k.received(KeyBoard.INSERT+" "));
	}

	/*
	 * Now we will test the special case buttons.
	 * We will press 1 non letter button, and one letter button for each of the 16 cases
	 */

	//8 cases with CapsLock
	@Test
	public void specialCaseYesCapsLockTest1() {
		//Ctrl = 0, Alt = 0, Shift = 0
		//the letter
		JavaFxJUnit4ApplicationKeyboard.root.unselect();
		JavaFxJUnit4ApplicationKeyboard.root.unselect();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.select();
		JavaFxJUnit4ApplicationKeyboard.root.select();
		JavaFxJUnit4ApplicationKeyboard.root.select();
		nextInGroup();
		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboard.k.received("A "));
		//the not letter
		JavaFxJUnit4ApplicationKeyboard.root.unselect();
		JavaFxJUnit4ApplicationKeyboard.root.unselect();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.select();
		JavaFxJUnit4ApplicationKeyboard.root.select();
		JavaFxJUnit4ApplicationKeyboard.root.select();
		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboard.k.received("F1 "));

	}

	@Test
	public void specialCaseYesCapsLockTest2() {
		//Ctrl = 0, Alt = 0, Shift = 1
		//the letter
		JavaFxJUnit4ApplicationKeyboard.root.unselect();
		JavaFxJUnit4ApplicationKeyboard.root.unselect();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.select();
		JavaFxJUnit4ApplicationKeyboard.root.select();
		JavaFxJUnit4ApplicationKeyboard.root.select();
		nextInGroup();
		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboard.k.received("z "));
		//the not letter
		JavaFxJUnit4ApplicationKeyboard.root.unselect();
		JavaFxJUnit4ApplicationKeyboard.root.unselect();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.select();
		JavaFxJUnit4ApplicationKeyboard.root.select();
		JavaFxJUnit4ApplicationKeyboard.root.select();
		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboard.k.received(KeyBoard.SHIFT+" "+"F1 "));
	}

	@Test
	public void specialCaseYesCapsLockTest3() {
		//Ctrl = 0, Alt = 1, Shift = 1
		//the Alt
		JavaFxJUnit4ApplicationKeyboard.root.unselect();
		JavaFxJUnit4ApplicationKeyboard.root.unselect();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.select();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.select();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.select();

		//the letter
		JavaFxJUnit4ApplicationKeyboard.root.unselect();
		JavaFxJUnit4ApplicationKeyboard.root.unselect();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.select();
		JavaFxJUnit4ApplicationKeyboard.root.select();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.select();

		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboard.k.received(KeyBoard.ALT+" "+KeyBoard.SHIFT+" "+"z "));
		//the not letter
		JavaFxJUnit4ApplicationKeyboard.root.unselect();
		JavaFxJUnit4ApplicationKeyboard.root.unselect();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.select();
		JavaFxJUnit4ApplicationKeyboard.root.select();
		JavaFxJUnit4ApplicationKeyboard.root.select();
		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboard.k.received(KeyBoard.ALT+" "+KeyBoard.SHIFT+" "+"F1 "));
	}

	@Test
	public void specialCaseYesCapsLockTest4() {
		//Ctrl = 1, Alt = 1, Shift = 1
		//the Ctrl
		JavaFxJUnit4ApplicationKeyboard.root.unselect();
		JavaFxJUnit4ApplicationKeyboard.root.unselect();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.select();
		JavaFxJUnit4ApplicationKeyboard.root.select();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.select();

		//the letter
		JavaFxJUnit4ApplicationKeyboard.root.unselect();
		JavaFxJUnit4ApplicationKeyboard.root.unselect();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.select();
		JavaFxJUnit4ApplicationKeyboard.root.select();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.select();

		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboard.k.received(KeyBoard.CTRL+" "+KeyBoard.ALT+" "+KeyBoard.SHIFT+" "+"z "));
		//the not letter
		JavaFxJUnit4ApplicationKeyboard.root.unselect();
		JavaFxJUnit4ApplicationKeyboard.root.unselect();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.select();
		JavaFxJUnit4ApplicationKeyboard.root.select();
		JavaFxJUnit4ApplicationKeyboard.root.select();
		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboard.k.received(KeyBoard.CTRL+" "+KeyBoard.ALT+" "+KeyBoard.SHIFT+" "+"F1 "));
	}

	@Test
	public void specialCaseYesCapsLockTest5() {
		//Ctrl = 1, Alt = 0, Shift = 1
		//the Alt
		JavaFxJUnit4ApplicationKeyboard.root.unselect();
		JavaFxJUnit4ApplicationKeyboard.root.unselect();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.select();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.select();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.select();

		//the letter
		JavaFxJUnit4ApplicationKeyboard.root.unselect();
		JavaFxJUnit4ApplicationKeyboard.root.unselect();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.select();
		JavaFxJUnit4ApplicationKeyboard.root.select();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.select();

		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboard.k.received(KeyBoard.CTRL+" "+KeyBoard.SHIFT+" "+"z "));
		//the not letter
		JavaFxJUnit4ApplicationKeyboard.root.unselect();
		JavaFxJUnit4ApplicationKeyboard.root.unselect();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.select();
		JavaFxJUnit4ApplicationKeyboard.root.select();
		JavaFxJUnit4ApplicationKeyboard.root.select();
		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboard.k.received(KeyBoard.CTRL+" "+KeyBoard.SHIFT+" "+"F1 "));
	}

	@Test
	public void specialCaseYesCapsLockTest6() {
		//Ctrl = 1, Alt = 0, Shift = 0
		//the Shift
		JavaFxJUnit4ApplicationKeyboard.root.unselect();
		JavaFxJUnit4ApplicationKeyboard.root.unselect();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.select();
		JavaFxJUnit4ApplicationKeyboard.root.select();
		JavaFxJUnit4ApplicationKeyboard.root.select();

		//the letter
		nextInGroup();

		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboard.k.received(KeyBoard.CTRL+" "+"z "));
		//the not letter
		JavaFxJUnit4ApplicationKeyboard.root.unselect();
		JavaFxJUnit4ApplicationKeyboard.root.unselect();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.select();
		JavaFxJUnit4ApplicationKeyboard.root.select();
		JavaFxJUnit4ApplicationKeyboard.root.select();
		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboard.k.received(KeyBoard.CTRL+" "+"F1 "));
	}

	@Test
	public void specialCaseYesCapsLockTest7() {
		//Ctrl = 1, Alt = 1, Shift = 0
		//the Alt
		JavaFxJUnit4ApplicationKeyboard.root.unselect();
		JavaFxJUnit4ApplicationKeyboard.root.unselect();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.select();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.select();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.select();

		//the letter
		JavaFxJUnit4ApplicationKeyboard.root.unselect();
		JavaFxJUnit4ApplicationKeyboard.root.unselect();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.select();
		JavaFxJUnit4ApplicationKeyboard.root.select();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.select();

		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboard.k.received(KeyBoard.CTRL+" "+KeyBoard.ALT+" "+"z "));
		//the not letter
		JavaFxJUnit4ApplicationKeyboard.root.unselect();
		JavaFxJUnit4ApplicationKeyboard.root.unselect();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.select();
		JavaFxJUnit4ApplicationKeyboard.root.select();
		JavaFxJUnit4ApplicationKeyboard.root.select();
		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboard.k.received(KeyBoard.CTRL+" "+KeyBoard.ALT+" "+"F1 "));
	}

	@Test
	public void specialCaseYesCapsLockTest8() {
		//Ctrl = 0, Alt = 1, Shift = 0
		//the Ctrl
		JavaFxJUnit4ApplicationKeyboard.root.unselect();
		JavaFxJUnit4ApplicationKeyboard.root.unselect();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.select();
		JavaFxJUnit4ApplicationKeyboard.root.select();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.select();

		//the letter
		JavaFxJUnit4ApplicationKeyboard.root.unselect();
		JavaFxJUnit4ApplicationKeyboard.root.unselect();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.select();
		JavaFxJUnit4ApplicationKeyboard.root.select();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.select();

		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboard.k.received(KeyBoard.ALT+" "+"z "));
		//the not letter
		JavaFxJUnit4ApplicationKeyboard.root.unselect();
		JavaFxJUnit4ApplicationKeyboard.root.unselect();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.select();
		JavaFxJUnit4ApplicationKeyboard.root.select();
		JavaFxJUnit4ApplicationKeyboard.root.select();
		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboard.k.received(KeyBoard.ALT+" "+"F1 "));
	}

	//8 cases without CapsLock
	@Test
	public void specialCaseZNoCapsLockTest1() {
		//Ctrl = 0, Alt = 0, Shift = 0

		//the Alt
		JavaFxJUnit4ApplicationKeyboard.root.unselect();
		JavaFxJUnit4ApplicationKeyboard.root.unselect();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.select();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.select();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.select();


		//the letter and CapsLock
		JavaFxJUnit4ApplicationKeyboard.root.unselect();
		JavaFxJUnit4ApplicationKeyboard.root.unselect();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.select();
		JavaFxJUnit4ApplicationKeyboard.root.select();
		JavaFxJUnit4ApplicationKeyboard.root.select();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.select();
		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboard.k.received("a "));
		//the not letter
		JavaFxJUnit4ApplicationKeyboard.root.unselect();
		JavaFxJUnit4ApplicationKeyboard.root.unselect();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.select();
		JavaFxJUnit4ApplicationKeyboard.root.select();
		JavaFxJUnit4ApplicationKeyboard.root.select();
		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboard.k.received("F1 "));

	}

	@Test
	public void specialCaseZNoCapsLockTest2() {
		//Ctrl = 0, Alt = 0, Shift = 1
		//the letter
		JavaFxJUnit4ApplicationKeyboard.root.unselect();
		JavaFxJUnit4ApplicationKeyboard.root.unselect();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.select();
		JavaFxJUnit4ApplicationKeyboard.root.select();
		JavaFxJUnit4ApplicationKeyboard.root.select();
		nextInGroup();
		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboard.k.received("Z "));
		//the not letter
		JavaFxJUnit4ApplicationKeyboard.root.unselect();
		JavaFxJUnit4ApplicationKeyboard.root.unselect();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.select();
		JavaFxJUnit4ApplicationKeyboard.root.select();
		JavaFxJUnit4ApplicationKeyboard.root.select();
		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboard.k.received(KeyBoard.SHIFT+" "+"F1 "));
	}

	@Test
	public void specialCaseZNoCapsLockTest3() {
		//Ctrl = 0, Alt = 1, Shift = 1
		//the Alt
		JavaFxJUnit4ApplicationKeyboard.root.unselect();
		JavaFxJUnit4ApplicationKeyboard.root.unselect();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.select();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.select();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.select();

		//the letter
		JavaFxJUnit4ApplicationKeyboard.root.unselect();
		JavaFxJUnit4ApplicationKeyboard.root.unselect();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.select();
		JavaFxJUnit4ApplicationKeyboard.root.select();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.select();

		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboard.k.received(KeyBoard.ALT+" "+KeyBoard.SHIFT+" "+"z "));
		//the not letter
		JavaFxJUnit4ApplicationKeyboard.root.unselect();
		JavaFxJUnit4ApplicationKeyboard.root.unselect();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.select();
		JavaFxJUnit4ApplicationKeyboard.root.select();
		JavaFxJUnit4ApplicationKeyboard.root.select();
		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboard.k.received(KeyBoard.ALT+" "+KeyBoard.SHIFT+" "+"F1 "));
	}

	@Test
	public void specialCaseZNoCapsLockTest4() {
		//Ctrl = 1, Alt = 1, Shift = 1
		//the Ctrl
		JavaFxJUnit4ApplicationKeyboard.root.unselect();
		JavaFxJUnit4ApplicationKeyboard.root.unselect();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.select();
		JavaFxJUnit4ApplicationKeyboard.root.select();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.select();

		//the letter
		JavaFxJUnit4ApplicationKeyboard.root.unselect();
		JavaFxJUnit4ApplicationKeyboard.root.unselect();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.select();
		JavaFxJUnit4ApplicationKeyboard.root.select();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.select();

		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboard.k.received(KeyBoard.CTRL+" "+KeyBoard.ALT+" "+KeyBoard.SHIFT+" "+"z "));
		//the not letter
		JavaFxJUnit4ApplicationKeyboard.root.unselect();
		JavaFxJUnit4ApplicationKeyboard.root.unselect();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.select();
		JavaFxJUnit4ApplicationKeyboard.root.select();
		JavaFxJUnit4ApplicationKeyboard.root.select();
		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboard.k.received(KeyBoard.CTRL+" "+KeyBoard.ALT+" "+KeyBoard.SHIFT+" "+"F1 "));
	}

	@Test
	public void specialCaseZNoCapsLockTest5() {
		//Ctrl = 1, Alt = 0, Shift = 1
		//the Alt
		JavaFxJUnit4ApplicationKeyboard.root.unselect();
		JavaFxJUnit4ApplicationKeyboard.root.unselect();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.select();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.select();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.select();

		//the letter
		JavaFxJUnit4ApplicationKeyboard.root.unselect();
		JavaFxJUnit4ApplicationKeyboard.root.unselect();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.select();
		JavaFxJUnit4ApplicationKeyboard.root.select();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.select();

		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboard.k.received(KeyBoard.CTRL+" "+KeyBoard.SHIFT+" "+"z "));
		//the not letter
		JavaFxJUnit4ApplicationKeyboard.root.unselect();
		JavaFxJUnit4ApplicationKeyboard.root.unselect();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.select();
		JavaFxJUnit4ApplicationKeyboard.root.select();
		JavaFxJUnit4ApplicationKeyboard.root.select();
		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboard.k.received(KeyBoard.CTRL+" "+KeyBoard.SHIFT+" "+"F1 "));
	}

	@Test
	public void specialCaseZNoCapsLockTest6() {
		//Ctrl = 1, Alt = 0, Shift = 0
		//the Shift
		JavaFxJUnit4ApplicationKeyboard.root.unselect();
		JavaFxJUnit4ApplicationKeyboard.root.unselect();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.select();
		JavaFxJUnit4ApplicationKeyboard.root.select();
		JavaFxJUnit4ApplicationKeyboard.root.select();

		//the letter
		nextInGroup();

		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboard.k.received(KeyBoard.CTRL+" "+"z "));
		//the not letter
		JavaFxJUnit4ApplicationKeyboard.root.unselect();
		JavaFxJUnit4ApplicationKeyboard.root.unselect();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.select();
		JavaFxJUnit4ApplicationKeyboard.root.select();
		JavaFxJUnit4ApplicationKeyboard.root.select();
		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboard.k.received(KeyBoard.CTRL+" "+"F1 "));
	}

	@Test
	public void specialCaseZNoCapsLockTest7() {
		//Ctrl = 1, Alt = 1, Shift = 0
		//the Alt
		JavaFxJUnit4ApplicationKeyboard.root.unselect();
		JavaFxJUnit4ApplicationKeyboard.root.unselect();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.select();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.select();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.select();

		//the letter
		JavaFxJUnit4ApplicationKeyboard.root.unselect();
		JavaFxJUnit4ApplicationKeyboard.root.unselect();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.select();
		JavaFxJUnit4ApplicationKeyboard.root.select();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.select();

		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboard.k.received(KeyBoard.CTRL+" "+KeyBoard.ALT+" "+"z "));
		//the not letter
		JavaFxJUnit4ApplicationKeyboard.root.unselect();
		JavaFxJUnit4ApplicationKeyboard.root.unselect();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.select();
		JavaFxJUnit4ApplicationKeyboard.root.select();
		JavaFxJUnit4ApplicationKeyboard.root.select();
		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboard.k.received(KeyBoard.CTRL+" "+KeyBoard.ALT+" "+"F1 "));
	}

	@Test
	public void specialCaseZNoCapsLockTest8() {
		//Ctrl = 0, Alt = 1, Shift = 0
		//the Ctrl
		JavaFxJUnit4ApplicationKeyboard.root.unselect();
		JavaFxJUnit4ApplicationKeyboard.root.unselect();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.select();
		JavaFxJUnit4ApplicationKeyboard.root.select();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.select();

		//the letter
		JavaFxJUnit4ApplicationKeyboard.root.unselect();
		JavaFxJUnit4ApplicationKeyboard.root.unselect();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.select();
		JavaFxJUnit4ApplicationKeyboard.root.select();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.select();
		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboard.k.received(KeyBoard.ALT+" "+"z "));

		//the not letter
		JavaFxJUnit4ApplicationKeyboard.root.unselect();
		JavaFxJUnit4ApplicationKeyboard.root.unselect();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.select();
		JavaFxJUnit4ApplicationKeyboard.root.select();
		JavaFxJUnit4ApplicationKeyboard.root.select();
		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboard.k.received(KeyBoard.ALT+" "+"F1 "));
	}

	/*
	 * Now we will test the special buttons.
	 * We will press all the special buttons.
	 */

	//row1
	@Test
	public void zSpecialButtonsrow1test1() {
		//open special buttons
		JavaFxJUnit4ApplicationKeyboard.root.unselect();
		JavaFxJUnit4ApplicationKeyboard.root.unselect();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.select();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.select();
		JavaFxJUnit4ApplicationKeyboard.root.select();

		// go to special buttons start
		JavaFxJUnit4ApplicationKeyboard.root.unselect();
		JavaFxJUnit4ApplicationKeyboard.root.unselect();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.next();
		JavaFxJUnit4ApplicationKeyboard.root.select();
		JavaFxJUnit4ApplicationKeyboard.root.select();
		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboard.k.received(KeyBoard.CTRL+" "+"z "));
	}

	@Test
	public void zSpecialButtonsrow1test2() {
		nextInGroup();
		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboard.k.received(KeyBoard.CTRL+" "+KeyBoard.SPACE+" "));
	}

	@Test
	public void zSpecialButtonsrow1test3() {
		nextInGroup();
		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboard.k.received(KeyBoard.CTRL+" "+"l "));
	}

	@Test
	public void zSpecialButtonsrow1test4() {
		nextInGroup();
		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboard.k.received(KeyBoard.CTRL+" "+"d "));
	}

	//row2
	@Test
	public void zSpecialButtonsrow2test1() {
		nextRowFirstSpecial();
		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboard.k.received(KeyBoard.CTRL+" "+"c "));
	}

	@Test
	public void zSpecialButtonsrow2test2() {
		nextInGroup();
		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboard.k.received(KeyBoard.CTRL+" "+"v "));
	}

	@Test
	public void zSpecialButtonsrow2test3() {
		nextInGroup();
		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboard.k.received(KeyBoard.CTRL+" "+"F11 "));
	}

	@Test
	public void zSpecialButtonsrow2test4() {
		nextInGroup();
		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboard.k.received(KeyBoard.CTRL+" "+"i "));
	}

	//row3
	@Test
	public void zSpecialButtonsrow3test1() {
		nextRowFirstSpecial();
		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboard.k.received(KeyBoard.CTRL+" "+"/ "));
	}

	@Test
	public void zSpecialButtonsrow3test2() {
		nextInGroup();
		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboard.k.received(KeyBoard.CTRL+" "+"h "));
	}

	@Test
	public void zSpecialButtonsrow3test3() {
		nextInGroup();
		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboard.k.received(KeyBoard.ALT+" "+KeyBoard.SHIFT+" "+"r "));
	}

	@Test
	public void zSpecialButtonsrow3test4() {
		nextInGroup();
		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboard.k.received(KeyBoard.CTRL+" "+KeyBoard.SHIFT+" "+"o "));
	}

	//row4
	@Test
	public void zSpecialButtonsrow4test1() {
		nextRowFirstSpecial();
		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboard.k.received(KeyBoard.CTRL+" "+"1 "));
	}

	@Test
	public void zSpecialButtonsrow4test2() {
		nextInGroup();
		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboard.k.received(KeyBoard.CTRL+" "+"j "));
	}

	@Test
	public void zSpecialButtonsrow4test3() {
		nextInGroup();
		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboard.k.received(KeyBoard.CTRL+" "+"o "));
	}

	@Test
	public void zSpecialButtonsrow4test4() {
		nextInGroup();
		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboard.k.received(KeyBoard.CTRL+" "+"t "));
	}

	//row5
	@Test
	public void zSpecialButtonsrow5test1() {
		nextRowFirstSpecial();
		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboard.k.received(KeyBoard.ALT+" "+KeyBoard.ARROW_UP+" "));
	}

	@Test
	public void zSpecialButtonsrow5test2() {
		nextInGroup();
		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboard.k.received(KeyBoard.ALT+" "+KeyBoard.ARROW_DOWN+" "));
	}

	@Test
	public void zSpecialButtonsrow5test3() {
		nextInGroup();
		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboard.k.received(KeyBoard.CTRL+" "+KeyBoard.SHIFT+" "+"r "));
	}

	@Test
	public void zSpecialButtonsrow5test4() {
		nextInGroup();
		Assert.assertTrue(JavaFxJUnit4ApplicationKeyboard.k.received(KeyBoard.CTRL+" "+KeyBoard.SHIFT+" "+"t "));
	}
		
}
