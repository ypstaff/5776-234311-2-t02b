package yearlyproject.programming.stackoverflow;

import static org.junit.Assert.*;

import org.junit.BeforeClass;
import org.junit.Test;

import javafx.geometry.Rectangle2D;
import javafx.util.Pair;
import yearlyproject.programming.common.ProgrammingController;

/**
 * Testing the Browser class
 * 
 * @author Lior Volin
 *
 */
public class BrowserTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		BrowserForTest.main(null);
	}

	@Test
	public void testResizeWithDefaultControllerSize() {
		makeTest(ProgrammingController.defaultControllerSize);
	}

	@Test
	public void testResizeWithEditControllerSize() {
		makeTest(ProgrammingController.mediumControllerSize);
	}

	@Test
	public void testResizeWithDebugControllerSize() {
		makeTest(ProgrammingController.mediumControllerSize);
	}

	private void makeTest(Pair<Integer, Integer> size) {
		Browser.resizeBrowser(size);
		Rectangle2D rec = getExpected(size);

		assertEquals("width browser resize error", (int) rec.getWidth(), (int) Browser.primaryStage.getWidth());
		assertEquals("Height browser resize error", (int) rec.getHeight(), (int) Browser.primaryStage.getHeight());
		assertEquals("X browser resize error", (int) rec.getMinX(), (int) Browser.primaryStage.getX());
		assertEquals("Y browser resize error", (int) rec.getMinY(), (int) Browser.primaryStage.getY());
	}

	private Rectangle2D getExpected(Pair<Integer, Integer> size) {
		int numOfAllSections = size.getKey();
		return ProgrammingController.getDimentsions(numOfAllSections, (numOfAllSections - size.getValue()), true);
	}

}
