package yearlyproject.programming.stackoverflow;

import javafx.application.Application;
import javafx.stage.Stage;

/**
 * A class that enables testing the Browser class by
 * starting the browser app and immediately closing it
 * 
 * @author Lior Volin
 *
 */
public class BrowserForTest extends Application {

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) {
		// Starting the browser app
		Application app = null;
		try {
			app = Browser.class.newInstance();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		Stage stage = new Stage();

		try {
			app.start(stage);
		} catch (Exception e) {
			e.printStackTrace();
		}

		// Closing the browser app
		Browser.primaryStage.close();
	}
}