package yearlyproject.programming.edit;
import javafx.application.Application;
import javafx.embed.swing.JFXPanel;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.stage.Screen;
import javafx.stage.Stage;

import javax.swing.JFrame;

import org.junit.BeforeClass;
import org.junit.Test;

import yearlyproject.programming.common.EchoModule;
import yearlyproject.programming.common.EchoSender;
import Navigation.IterableNodeGroups;

import com.google.inject.Guice;

/**
 * A way to launch and test the Edit Gui
 * @author Roy Svinik
 *
 */
public class LaunchEditGuiEcho extends Application{
	
	@Override
	public void start(Stage primaryStage) throws Exception {
		EditGui eg = new EditGui(Guice.createInjector(new EchoModule()).getInstance(EchoSender.class));
		JFrame frame = new JFrame("test");
		final JFXPanel fxPanel = new JFXPanel();
		
		IterableNodeGroups root = eg.getIterableRoot();
		fxPanel.setScene(new Scene(root.getParentPane()));
		
		frame.add(fxPanel);
		frame.setFocusableWindowState(false);
		frame.setVisible(true);
		
		//set frame size to fit half the screen
		Rectangle2D bounds = Screen.getPrimary().getVisualBounds();
		frame.setSize((int) bounds.getWidth(), (int) bounds.getHeight()/2);
		frame.setLocation(0, (int) bounds.getHeight()/2);
		
		root.start();
	}
	
	@BeforeClass
	public static void launchGui() {
		launch();
	}
	
	@Test
	//junit runner need tests to run
	public void nothing(){
		
	}

}
