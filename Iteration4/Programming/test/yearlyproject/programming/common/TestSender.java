package yearlyproject.programming.common;

import yearlyproject.programming.controlEngine.KeySender;


/**
 * 
 * @author Akiva Herer
 *
 */


public class TestSender implements KeySender  {
	String testString = "";
	@Override
	public void send(String... keysLabels) {
		for (String string : keysLabels)
			testString += string + " ";
	}
	
	public Boolean received(String s) {
	//	System.out.println(testString);
		if(testString.equals(s)){
			testString = "";
			return true;
		}
		testString = "";
		return false;
	}
	
	

	@Override
	public void openView(VIEW w) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void holdControl(CONTROL_KEY key) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void releaseControl(CONTROL_KEY key) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void openTemplateAssist() {
		// TODO Auto-generated method stub
		
	}

}
