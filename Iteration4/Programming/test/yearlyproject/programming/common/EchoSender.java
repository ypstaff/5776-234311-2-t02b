package yearlyproject.programming.common;

import java.util.function.Consumer;

import org.apache.log4j.Logger;

import yearlyproject.programming.controlEngine.WindowedKeySender;

/**
 * 
 * @author kfirlan
 *
 */
public class EchoSender implements WindowedKeySender {
	private static final Logger LOGGER = Logger.getLogger(EchoSender.class);

	@Override
	public void send(String... keysLabels) {
		String all = "";
		for (String string : keysLabels)
			all += string + " ";
		LOGGER.info("send simult "+all);
		System.out.println("send simult "+all);		
	}

	@Override
	public void openView(VIEW w) {
		LOGGER.info("open view "+w.toString());
		System.out.println("open view "+w.toString());
		
	}

	@Override
	public void holdControl(CONTROL_KEY key) {
		LOGGER.info("hold "+key.toString());
		System.out.println("hold "+key.toString());
	}

	@Override
	public void releaseControl(CONTROL_KEY key) {
		LOGGER.info("release "+key.toString());
		System.out.println("release "+key.toString());
	}

	@Override
	public void openTemplateAssist() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resize(int numOfAllSections, int numOfTakenSegments, boolean gravityBottom) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void checkTitleAndDo(String inTitle, Boolean contain, Consumer<Void> action) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void restart() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void changeToVertical() {
		System.out.println("vertical");
	}

	@Override
	public void backToHorozotial() {
		System.out.println("Horozotial");
	}

	@Override
	public void resizeController(Integer numOfAllSections, Integer numOfTakenSegments, boolean gravityBottom) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void changeToVertical(boolean thinMenusMode) {
		// TODO Auto-generated method stub
		
	}

}
