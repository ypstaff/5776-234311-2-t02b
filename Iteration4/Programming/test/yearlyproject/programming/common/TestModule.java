package yearlyproject.programming.common;
import yearlyproject.programming.controlEngine.KeySender;

import com.google.inject.AbstractModule;

/**
 * 
 * @author Akiva Herer
 *
 */
public class TestModule extends AbstractModule{

	
	protected void configure() {
		bind(KeySender.class).to(TestSender.class);	
	}

}

