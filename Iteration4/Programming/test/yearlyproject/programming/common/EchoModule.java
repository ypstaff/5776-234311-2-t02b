package yearlyproject.programming.common;
import yearlyproject.programming.controlEngine.WindowedKeySender;

import com.google.inject.AbstractModule;

/**
 * 
 * @author kfirlan
 *
 */
public class EchoModule extends AbstractModule{

	
	protected void configure() {
		bind(WindowedKeySender.class).to(EchoSender.class);	
	}

}
