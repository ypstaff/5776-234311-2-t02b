package yearlyproject.programming.view;
import java.awt.Dimension;

import javafx.application.Application;
import javafx.embed.swing.JFXPanel;
import javafx.scene.Scene;
import javafx.stage.Stage;

import javax.swing.JFrame;
import javax.swing.WindowConstants;

import org.junit.BeforeClass;
import org.junit.Test;

import yearlyproject.programming.common.EchoModule;
import yearlyproject.programming.common.EchoSender;
import yearlyproject.programming.keyboard.OpenKeyboards;

import com.google.inject.Guice;
/**
 * not realy a test but a method to run gui without defining main
 * i dont know why this test should be an application, 
 * but with out it i get ExceptionInInitializerError on new Button("some_name") 
 * @author kfirlan
 *
 */
public class LaunchViewGuiEcho extends Application{
	@Override
	public void start(Stage primaryStage) throws Exception {
		ViewGui vg = new ViewGui(Guice.createInjector(new EchoModule()).getInstance(EchoSender.class)
				,null,null,(x)->{});
		vg.setOpenKDB(new OpenKeyboards() {
			
			@Override
			public void symbols() {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void regular() {
				// TODO Auto-generated method stub
				System.out.println("open keyboard");
			}
			
			@Override
			public void numbers() {
				// TODO Auto-generated method stub
				
			}
		});
		JFrame frame = new JFrame("test");
		final JFXPanel fxPanel = new JFXPanel();
		fxPanel.setScene(new Scene(vg.getIterableRoot().getParentPane()));
		frame.add(fxPanel);
		frame.setFocusableWindowState(false);
		frame.setSize(new Dimension(1300, 400));
		frame.setVisible(true);
		vg.getIterableRoot().start();
		frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE );
	}	
	@BeforeClass
	public static void launchGui() {
		launch();//block thread
	}
	
	@Test
	//junit runner need tests to run
	public void nothing(){
		
	}
	

}
