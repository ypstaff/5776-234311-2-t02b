package yearlyproject.programming.view;

import java.awt.Dimension;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.ReentrantLock;

import javafx.application.Application;
import javafx.embed.swing.JFXPanel;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import javax.swing.JFrame;
import javax.swing.WindowConstants;

import yearlyproject.programming.common.EchoModule;
import yearlyproject.programming.common.EchoSender;
import yearlyproject.programming.keyboard.OpenKeyboards;
import Navigation.IterableNodeGroups;

import com.google.inject.Guice;
/**
 * based on akiva tests
 * @author kfirlan
 *
 */
public class ViewGuiTestApp extends Application {
	public static JFrame frame;
	public static VBox vbox;
	public static IterableNodeGroups root;
	public static JFXPanel fxPanel = new JFXPanel();
	@Override
	public void start(Stage primaryStage) throws Exception {
		started.set(Boolean.TRUE);
		ViewGui vg = buildViewGui();
		BorderPane tmp = (BorderPane) ((StackPane) (vg.getIterableRoot().getParentPane())).getChildrenUnmodifiable().get(0);
		vbox = (VBox) tmp.getLeft();
		frame = new JFrame("test");
		
		fxPanel.setScene(new Scene(vg.getIterableRoot().getParentPane()));
		frame.add(fxPanel);
		frame.setFocusableWindowState(false);
		frame.setVisible(true);
		frame.setSize(new Dimension(800, 400));
		frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE );
		root.start();
	}
	public static ViewGui buildViewGui() {
		ViewGui $ = new ViewGui(Guice.createInjector(new EchoModule()).getInstance(EchoSender.class));
		$.setOpenKDB(new OpenKeyboards() {
			
			@Override
			public void symbols() {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void regular() {
				// TODO Auto-generated method stub
				System.out.println("open keyboard");
			}
			
			@Override
			public void numbers() {
				// TODO Auto-generated method stub
				
			}
		});
		root = $.getIterableRoot();
		return $;
	}	
	  /** The lock that guarantees that only one JavaFX thread will be started. */
    private static final ReentrantLock LOCK = new ReentrantLock();

    /** Started flag. */
    private static AtomicBoolean started = new AtomicBoolean();
	/**
     * Start JavaFx.
     */
    public static void startJavaFx()
    {
        try
        {
            // Lock or wait.  This gives another call to this method time to finish
            // and release the lock before another one has a go
            LOCK.lock();

            if (!started.get())
            {
                // start the JavaFX application
                final ExecutorService executor = Executors.newSingleThreadExecutor();
                executor.execute(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        launch();
                    }
                });

                while (!started.get())
                {
                    Thread.yield();
                }
            }
        }
        finally
        {
            LOCK.unlock();
        }
    }
    protected static void launch()
    {
        Application.launch();
    }
}
