package yearlyproject.programming.view;
import java.awt.Dimension;

import javafx.application.Application;
import javafx.embed.swing.JFXPanel;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.stage.Screen;
import javafx.stage.Stage;

import javax.swing.JFrame;

import org.junit.BeforeClass;
import org.junit.Test;

import yearlyproject.programming.controlEngine.EclipseController;
/**
 * not realy a test but a method to run gui without defining main
 *  i dont know why this test should be an application, 
 * but with out it i get ExceptionInInitializerError on new Button("some_name") 
 * @author kfirlan
 *
 */
public class LaunchViewGui extends Application{
	@Override
	public void start(Stage primaryStage) throws Exception {
		setBounds(primaryStage);
		ViewGui vg = new ViewGui(new EclipseController(null));
		JFrame frame = new JFrame("test");
		final JFXPanel fxPanel = new JFXPanel();
		fxPanel.setScene(new Scene(vg.getIterableRoot().getParentPane()));
		frame.add(fxPanel);
		frame.setFocusableWindowState(false);
		frame.setVisible(true);
		frame.setSize(new Dimension(500, 500));
	}	
	@BeforeClass
	public static void launchGui() {
		launch();//block thread
	}
	
	@Test
	//junit runner need tests to run
	public void nothing(){
	}
	/**
	 * set bounds of the stage to half of the screen
	 */
	private void setBounds(Stage primaryStage) {
		Rectangle2D bounds = Screen.getPrimary().getVisualBounds();
		primaryStage.setY((bounds.getMinY()+bounds.getHeight())/2);
		primaryStage.setX(bounds.getMinX());
		primaryStage.setHeight(bounds.getHeight()/2);
		primaryStage.setWidth(bounds.getWidth());
	}
	

}
