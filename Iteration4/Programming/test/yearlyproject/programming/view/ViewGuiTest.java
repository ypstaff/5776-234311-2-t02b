package yearlyproject.programming.view;

import javafx.scene.Node;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
/**
 * 
 * @author kfirlan
 *
 */
@RunWith(ViewGuiTestAppRunner.class)
public class ViewGuiTest {
	
	@After
	//i should find a better way to do it
	public void reset(){
		for(int i=0;i<100;++i)
			ViewGuiTestApp.root.unselect();
	}
		
	@Test
	public void outlineVisibleAfterTouch() throws InterruptedException{
		Assert.assertNotNull(ViewGuiTestApp.vbox);
		ViewGuiTestApp.root.select();
		ViewGuiTestApp.root.select();
		Assert.assertTrue(ViewGuiTestApp.vbox.getChildrenUnmodifiable().get(1).isVisible());
	}
	
	@Test
	public void invisibleOnStart() throws InterruptedException{
		boolean first = true;
		Assert.assertNotNull(ViewGuiTestApp.vbox);
		for (Node n : ViewGuiTestApp.vbox.getChildrenUnmodifiable()) {
			if (first){
				first=false;
				Assert.assertTrue(n.isVisible());
				continue;
			}
			Assert.assertFalse(n.isVisible());
		}
	}
	
	@Test
	public void justOneVisibleAfterTouch(){
		Assert.assertNotNull(ViewGuiTestApp.vbox);
		ViewGuiTestApp.root.select();
		ViewGuiTestApp.root.select();
		for (int i=2;i<=6;++i)
			Assert.assertFalse(ViewGuiTestApp.vbox.getChildrenUnmodifiable().get(i).isVisible());
	}
	
	@Test
	public void menuBarVisibleAfterTouch(){
		Assert.assertNotNull(ViewGuiTestApp.vbox);
		ViewGuiTestApp.root.next();
		ViewGuiTestApp.root.select();
		for (int i=0;i<3;++i)
			ViewGuiTestApp.root.next();
		ViewGuiTestApp.root.select();
		Assert.assertTrue(ViewGuiTestApp.vbox.getChildrenUnmodifiable().get(6).isVisible());
	}
	
	
}
