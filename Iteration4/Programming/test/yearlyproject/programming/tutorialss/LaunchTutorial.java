package yearlyproject.programming.tutorialss;

import javafx.application.Application;
import javafx.embed.swing.JFXPanel;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.stage.Screen;
import javafx.stage.Stage;
import yearlyproject.programming.tutorial.Tutorial;

import javax.swing.JFrame;

import org.junit.BeforeClass;
import org.junit.Test;

import Navigation.IterableNodeGroups;


/**
 * A way to launch and test the Tutorial
 * @author Akiva Herer
 *
 */

public class LaunchTutorial extends Application{
	
	@Override
	public void start(Stage primaryStage) throws Exception {
		JFrame frame = new JFrame("test");
		final JFXPanel fxPanel = new JFXPanel();
		Tutorial eg = new Tutorial((x)->{});

		
		IterableNodeGroups root = eg.getIterableRoot();
		fxPanel.setScene(new Scene(root.getParentPane()));
		
		frame.add(fxPanel);
		frame.setFocusableWindowState(false);
		frame.setVisible(true);
		
		//set frame size to fit half the screen
		Rectangle2D bounds = Screen.getPrimary().getVisualBounds();
		frame.setSize((int) bounds.getWidth(), (int) bounds.getHeight()/2);
		frame.setLocation(0, (int) bounds.getHeight()/2);
		
		root.start();
	}
	
	//launching the Tutorial
	@BeforeClass
	public static void launchGui() {
		launch();
	}
	
	@Test
	//junit runner need tests to run
	public void nothing(){
		
	}

}
