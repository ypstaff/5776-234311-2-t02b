package yearlyproject.programming.debug;

import java.util.function.Consumer;

import yearlyproject.programming.common.GuiSegment;
import yearlyproject.programming.common.MenuButtons;
import yearlyproject.programming.controlEngine.KeySender;
import com.google.inject.Inject;

/**
 * The main GUI for the Debug mode.
 * @author lmaman
 *
 */
public class DebugGui extends GuiSegment {
	
	@Inject
	public DebugGui(final KeySender sender) {
		this(sender, null);
	}
	
	@Inject
	public DebugGui(final KeySender sender, Consumer<Void> onTut) {
		MenuButtons[] tmpMenuButtons = { 
				new Controls(sender),
				new SubMenus(sender),
				new CodeView(sender),
				new Variables(sender),
				new Breakpoints(sender)
		};
		menus = tmpMenuButtons;
		
		String[] tmpLabels = {
				"Controls",
				"Sub Segments",
				"Code View",
				"Varaibles",
				"Breakpoints"
		};
		labels = tmpLabels;
		
		winTitle = "Debug";
		
		applyStyles();
		
		setOnTutorial(onTut);
		
		buildToVbox();
		
		
		MenuButtons segmentsGui = findMenu(SubMenus.class);
		//From Breakpoint button in SubSegments, go to Breakpoints row.
		setTransition(segmentsGui.findButton(SubMenus.BREAKPOINTS_LABEL), Breakpoints.class);
		//From Variables button in SubSegments, go to Variables row.
		setTransition(segmentsGui.findButton(SubMenus.VARIABLES_LABEL), Variables.class);
		//From Code View button in SubSegments, go to Code View row.
		setTransition(segmentsGui.findButton(SubMenus.CODE_VIEW_LABEL), CodeView.class);
	}


}
