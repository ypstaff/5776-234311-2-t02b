package yearlyproject.programming.debug;

import javafx.scene.control.Button;
import yearlyproject.programming.common.MenuButtons;
import yearlyproject.programming.controlEngine.KeySender;

import com.google.inject.Inject;
/**
 * Gui for navigating Breakpoints.
 * @author lmaman
 *
 */
public class Breakpoints extends MenuButtons {

	public static final String DOWN_LABEL = "Down";
	public static final String UP_LABEL = "Up";
	public static final String TOGGLE_BREAKPOINT_LABEL = "Toggle Breakpoint";
	public static final String SKIP_ALL_BREAKPOINTS_LABEL = "Skip All Breakpoints";

	
	@Inject
	private final KeySender sender;
	
	public Breakpoints(final KeySender sender) {
		this.sender = sender;
		
		
		Button [] tmpMenu = {
			new Button(DOWN_LABEL),
			new Button(UP_LABEL),
			new Button(TOGGLE_BREAKPOINT_LABEL),
			new Button(SKIP_ALL_BREAKPOINTS_LABEL)
		};
		subMenu = tmpMenu; 
		
		setGuiSubMenuActions();

	}
	
	@Override
	public Button[] getSubMenu() {
		return subMenu;
	}
	
	private void setGuiSubMenuActions() {
		applyEventHandler(subMenu, DOWN_LABEL, e -> { sender.send("DOWN");});
		applyEventHandler(subMenu, UP_LABEL, e -> { sender.send("UP");});
		applyEventHandler(subMenu, TOGGLE_BREAKPOINT_LABEL, e -> { sender.send("SPACE");});
		applyEventHandler(subMenu, SKIP_ALL_BREAKPOINTS_LABEL, e -> { sender.send("CTRL", "ALT", "b");});

	}

}
