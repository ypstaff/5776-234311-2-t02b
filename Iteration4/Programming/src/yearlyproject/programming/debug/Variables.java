package yearlyproject.programming.debug;

import javafx.scene.control.Button;
import yearlyproject.programming.common.MenuButtons;
import yearlyproject.programming.controlEngine.KeySender;
import com.google.inject.Inject;

/**
 * Gui for navigating Variables.
 * @author lmaman
 *
 */
public class Variables extends MenuButtons {
	private static final String COLLAPSE_ALL_LABEL = "Collapse all";
	private static final String COLLAPSE_LABEL = "Collapse";
	private static final String EXPEND_LABEL = "Expand";
	private static final String UP_LABEL = "Up";
	private static final String DOWN_LABEL = "Down";
	
	@Inject
	private final KeySender sender;



	public Variables(final KeySender sender) {
		this.sender = sender;
		Button[] tmp = {
			new Button(DOWN_LABEL),
			new Button(UP_LABEL),
			new Button(EXPEND_LABEL),
			new Button(COLLAPSE_LABEL),
			new Button(COLLAPSE_ALL_LABEL)
		};
		subMenu = tmp;
		setGuiSubMenuActions();

	}
	
	@Override
	public Button[] getSubMenu() {
		return subMenu;
	}

	private void setGuiSubMenuActions() {
		applyEventHandler(subMenu, DOWN_LABEL, (e) -> sender.send(DOWN_LABEL));
		applyEventHandler(subMenu, UP_LABEL, (e) -> sender.send(UP_LABEL));
		applyEventHandler(subMenu, EXPEND_LABEL, (e) -> sender.send("shift", "right"));
		applyEventHandler(subMenu, COLLAPSE_LABEL, (e) -> sender.send("shift", "left"));
		applyEventHandler(subMenu, COLLAPSE_ALL_LABEL, (e) -> sender.send("shift", "ctrl", "NUMPADDIV"));
	}
	
}
