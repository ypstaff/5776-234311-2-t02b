package yearlyproject.programming.debug;

import javafx.scene.control.Button;
import yearlyproject.programming.common.MenuButtons;
import yearlyproject.programming.controlEngine.KeySender;

import com.google.inject.Inject;
/**
 * Gui for navigating Code.
 * @author lmaman
 *
 */
public class CodeView extends MenuButtons {

	public static final String DOWN_LABEL = "Down";
	public static final String UP_LABEL = "Up";
	public static final String GOTO_LINE_LABEL = "Go to Line";
	public static final String TOGGLE_BREAKPOINT_LABEL = "Toggle Breakpoint";


	@Inject
	private final KeySender sender;
	
	public CodeView(final KeySender sender) {
		this.sender = sender;
		
		Button [] tmpMenu = {
			new Button(DOWN_LABEL),
			new Button(UP_LABEL),
			new Button(GOTO_LINE_LABEL),
			new Button(TOGGLE_BREAKPOINT_LABEL)
		};
		subMenu = tmpMenu; 
		
		setGuiSubMenuActions();

	}
	
	@Override
	public Button[] getSubMenu() {
		return subMenu;
	}

	
	private void setGuiSubMenuActions() {
		applyEventHandler(subMenu, DOWN_LABEL, e -> { sender.send("DOWN");});
		applyEventHandler(subMenu, UP_LABEL, e -> { sender.send("UP");});
		applyEventHandler(subMenu, GOTO_LINE_LABEL, e -> { sender.send("CTRL", "l"); keyboards.numbers();/*TODO go to numpad*/});
		applyEventHandler(subMenu, TOGGLE_BREAKPOINT_LABEL, e -> { sender.send("CTRL", "SHIFT", "b");});

	}

}
