package yearlyproject.programming.debug;

import javafx.scene.control.Button;
import yearlyproject.programming.common.MenuButtons;
import yearlyproject.programming.controlEngine.KeySender;
import yearlyproject.programming.controlEngine.KeySender.VIEW;

import com.google.inject.Inject;
/**
 * Gui for Menu navigation.
 * @author lmaman
 *
 */
public class SubMenus extends MenuButtons {
	
	public static final String BREAKPOINTS_LABEL = "Breakpoins";
	public static final String VARIABLES_LABEL = "Variables";
	public static final String CODE_VIEW_LABEL = "Code View";


	@Inject
	private final KeySender sender;
	
	public SubMenus(final KeySender sender) {
		this.sender = sender;
		
		Button [] tmpMenu = {
			new Button(CODE_VIEW_LABEL),
			new Button(BREAKPOINTS_LABEL),
			new Button(VARIABLES_LABEL)
		};
		subMenu = tmpMenu;
		
		setGuiSubMenuActions();
	}
	
	@Override
	public Button[] getSubMenu() {
		return subMenu;
	}

	
	private void setGuiSubMenuActions() {
		applyEventHandler(subMenu, BREAKPOINTS_LABEL, e -> sender.openView(VIEW.BREAKPOINTS));
		applyEventHandler(subMenu, VARIABLES_LABEL, e -> sender.openView(VIEW.VARIABLES));
		applyEventHandler(subMenu, CODE_VIEW_LABEL, e -> { sender.openView(VIEW.EDITOR); sender.send("F12");});
	}

}
