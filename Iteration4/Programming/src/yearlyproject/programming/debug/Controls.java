package yearlyproject.programming.debug;

import javafx.scene.control.Button;
import yearlyproject.programming.common.MenuButtons;
import yearlyproject.programming.controlEngine.KeySender;

import com.google.inject.Inject;
/**
 * Gui for controlling the debugger.
 * @author lmaman
 *
 */
public class Controls extends MenuButtons{
	
	public static final String START_LABEL = "Start";
	public static final String RESUME_LABEL = "Resume";
	public static final String TERMINATE_LABEL = "Terminate";
	public static final String STEP_INTO_LABEL = "Step Into";
	public static final String STEP_OVER_LABEL = "Step Over";
	public static final String STEP_RETURN_LABEL = "Step Return";

	
	@Inject
	private final KeySender sender;

	public Controls(KeySender sender) {
		this.sender = sender;
		
		Button [] tmpMenu = {
			new Button(START_LABEL),
			new Button(RESUME_LABEL),
			new Button(TERMINATE_LABEL),
			new Button(STEP_INTO_LABEL),
			new Button(STEP_OVER_LABEL),
			new Button(STEP_RETURN_LABEL)
		};
		subMenu = tmpMenu; 
		
		setGuiSubMenuActions();
	}
	
	@Override
	public Button[] getSubMenu() {
		return subMenu;
	}

	
	private void setGuiSubMenuActions() {
		applyEventHandler(subMenu, START_LABEL, e -> { 
			//Terminate an open debug process.
			sender.send("CTRL", "F2");
			sender.send("F11");
		});
		applyEventHandler(subMenu, RESUME_LABEL, e -> { sender.send("F8");});
		applyEventHandler(subMenu, TERMINATE_LABEL, e -> { sender.send("CTRL", "F2");});
		applyEventHandler(subMenu, STEP_INTO_LABEL, e -> { sender.send("F5");});
		applyEventHandler(subMenu, STEP_OVER_LABEL, e -> { sender.send("F6");});
		applyEventHandler(subMenu, STEP_RETURN_LABEL, e -> { sender.send("F7");});

	}

}
