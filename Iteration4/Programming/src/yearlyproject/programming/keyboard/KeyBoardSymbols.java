package yearlyproject.programming.keyboard;


import java.util.function.Consumer;

import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import yearlyproject.programming.common.RootedObject;
import yearlyproject.programming.controlEngine.KeySender;
import Navigation.IterableNodeGroups;
import Navigation.IterationControlHBox;

/**
 *  Class for creating the Symbol Keyboard
 * @author Akiva Herer 
 *
 */


public class KeyBoardSymbols implements RootedObject {

	//the parameters of the symbol keyboard
	private BorderPane m_mainPane;
	IterationControlHBox m_iterControlPane;
	private HBox hboxes[];
	private KeySender sender;
	private IterableNodeGroups root;
	Consumer<Void> onFinish;
	Consumer<Void> onFinishTut;
	private OpenKeyboards keyboards;

	//the constants of the symbol keyboard
	public static final int NUM_ROWS = 4;
	public static final int ROW_ONE_SIZE =6;
	public static final int ROW_TWO_SIZE =6;
	public static final int ROW_THREE_SIZE =6;
	public static final int ROW_FOUR_SIZE =5;

	public static final int HBOX_GAP = 2;
	public static final int VBOX_GAP = 4;

	public static final String REGULAR ="Regular";
	public static final String BACKSPACE ="Backspace";


	public KeyBoardSymbols(KeySender sender,Consumer<Void> onFinish, Consumer<Void> onFinishTut, OpenKeyboards keyboards) {
		this.sender = sender;
		this.onFinish = onFinish;
		this.onFinishTut = onFinishTut;
		this.keyboards = keyboards;

		//building the symbol keyboard
		hboxes = new HBox [NUM_ROWS];
		buildRows();
		m_mainPane = new BorderPane();
		VBox center = new VBox(VBOX_GAP);	
		for(int i=0; i<NUM_ROWS;++i){
			hboxes[i].setAlignment(Pos.CENTER);
			center.getChildren().add(hboxes[i]);
		}
		center.setAlignment(Pos.CENTER);
		m_mainPane.setCenter(center);				

		//integrating the symbol keyboard with the navigation
		root = new IterableNodeGroups(m_mainPane,Color.RED,Color.RED);
		m_iterControlPane= root.getIterationControlHBoxControlHBox();
		m_mainPane.setBottom(m_iterControlPane);
		for (Node hbox : center.getChildrenUnmodifiable())
			root.addChildGroup(((HBox) hbox).getChildren(),2);
	}

	//building the rows of the symbol keyboard
	private void buildRows(){
		rowOneButtons();
		rowTwoButtons();
		rowThreeButtons();
		rowFourButtons();
	}

	//the four methods for adding symbols to the symbol keyboard
	private void rowOneButtons() {
		hboxes[0]=new HBox(HBOX_GAP);	

		standardSymbolButtonAdd("   (    ",hboxes[0]);
		standardSymbolButtonAdd("    )   ",hboxes[0]);
		standardSymbolButtonAdd("   \"   ",hboxes[0]);
		standardSymbolButtonAdd("    !    ",hboxes[0]);
		standardSymbolButtonAdd("   {    ",hboxes[0]);
		standardSymbolButtonAdd("    }   ",hboxes[0]);

	}

	private void rowTwoButtons() {
		hboxes[1]=new HBox(HBOX_GAP);	

		standardSymbolButtonAdd("   ?   ",hboxes[1]);
		standardSymbolButtonAdd("   &   ",hboxes[1]);
		standardSymbolButtonAdd("   *   ",hboxes[1]);
		standardSymbolButtonAdd("   +   ",hboxes[1]);
		standardSymbolButtonAdd("   @  ",hboxes[1]);
		standardSymbolButtonAdd("   ^   ",hboxes[1]);
	}

	private void rowThreeButtons() {
		hboxes[2]=new HBox(HBOX_GAP);	

		standardSymbolButtonAdd("   $   ",hboxes[2]);
		standardSymbolButtonAdd("   %   ",hboxes[2]);
		problamaticButtonAdd(" ___ ",hboxes[2]);
		standardSymbolButtonAdd("   ~   ",hboxes[2]);
		standardSymbolButtonAdd("    |    ",hboxes[2]);
		standardSymbolButtonAdd("    :    ",hboxes[2]);
	}

	private void rowFourButtons() {
		hboxes[3]=new HBox(HBOX_GAP);	

		standardSymbolButtonAdd("   #   ",hboxes[3]);
		standardSymbolButtonAdd("   <   ",hboxes[3]);
		standardSymbolButtonAdd("   >   ",hboxes[3]);
		regularButtonAdd(hboxes[3]);
		standardSymbolButtonAdd(BACKSPACE,hboxes[3]);
	}

	//creates the button for returning to regular keyboard
	private void regularButtonAdd(HBox h) {
		Button b=new Button(" "+REGULAR+" ");
		b.setStyle("-fx-background-color: Black; -fx-text-fill: White");
		b.setOnMouseClicked(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent arg0) {	
				switchToKBD();
			}
		});		
		h.getChildren().add(b);
	}

	//creates the buttons for symbol keyboard
	private void standardSymbolButtonAdd(String s, HBox h) {
		Button b=new Button(s);
		b.setStyle("-fx-background-color: Black; -fx-text-fill: White");
		b.setOnMouseClicked(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent arg0) {
				sender.send(b.getText().replace(" ", ""));
			}
		});		
		h.getChildren().add(b);
	}
	// _ didn't shown in button by itself, need ___ this is a work around 
	private void problamaticButtonAdd(String s, HBox h) {
		Button b=new Button(s);
		b.setStyle("-fx-background-color: Black; -fx-text-fill: White");
		b.setOnMouseClicked(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent arg0) {
				sender.send("_");
			}
		});		
		h.getChildren().add(b);
	}

	//implementation of interface for integration
	@Override
	public IterableNodeGroups getIterableRoot() {
		return root;
	}

	//opens up regular Keyboard
	private void switchToKBD() {
		keyboards.regular();
	}

}
