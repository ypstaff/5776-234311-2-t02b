package yearlyproject.programming.keyboard;

import java.util.function.Consumer;

import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import yearlyproject.programming.common.RootedObject;
import yearlyproject.programming.controlEngine.KeySender;
import yearlyproject.programming.tutorial.TutorialControlHBox;
import Navigation.IterableNodeGroups;
import Navigation.IterationControlHBox;

/**
 *  Class for creating the Numbers Keyboard
 * @author Akiva Herer 
 *
 */

public class KeyBoardNumbers  implements RootedObject {

	//the parameters of the symbol keyboard
	private BorderPane m_mainPane;
	IterationControlHBox m_iterControlPane;
	private HBox hboxes[];
	private KeySender sender;
	private IterableNodeGroups root;
	private Consumer<Void> onFinish;
	private TutorialControlHBox tutorialHBox;

	//the constants of the symbol keyboard
	public static final int NUM_ROWS = 5;
	public static final int ROW_ONE_SIZE =3;
	public static final int ROW_TWO_SIZE =3;
	public static final int ROW_THREE_SIZE =3;
	public static final int ROW_FOUR_SIZE =2;
	public static final int ROW_FIVE_SIZE =1;

	public static final int HBOX_GAP = 2;
	public static final int VBOX_GAP = 4;

	public static final String RETURN ="Finish";
	public static final String BACKSPACE ="Backspace";

	public KeyBoardNumbers(KeySender sender, Consumer<Void> onFinish, Consumer<Void> onTut) {
		this.sender = sender;
		this.onFinish = onFinish;

		//building the numbers keyboard
		hboxes = new HBox [NUM_ROWS];
		buildRows();
		m_mainPane = new BorderPane();
		VBox center = new VBox(VBOX_GAP);	
		for(int i=0; i<NUM_ROWS;++i){
			hboxes[i].setAlignment(Pos.CENTER);
			center.getChildren().add(hboxes[i]);
		}
		center.setAlignment(Pos.CENTER);
		m_mainPane.setCenter(center);	


		//integrating the numbers keyboard with the navigation
		root = new IterableNodeGroups(m_mainPane,Color.RED,Color.RED);
		m_iterControlPane= root.getIterationControlHBoxControlHBox();
		m_mainPane.setBottom(m_iterControlPane);
		for (Node hbox : center.getChildrenUnmodifiable())
			root.addChildGroup(((HBox) hbox).getChildren());

		//integrating the keyboard with the Tutorial
//		tutorialHBox = new TutorialControlHBox(root, onTut ,m_mainPane);
//		tutorialHBox.setTutorialButtons();
	}

	//building the rows of the numbers keyboard
	private void buildRows(){
		rowOneButtons();
		rowTwoButtons();
		rowThreeButtons();
		rowFourButtons();
		rowFiveButtons();
	}



	//the five methods for adding numbers to the numbers keyboard
	private void rowOneButtons() {
		hboxes[0]=new HBox(HBOX_GAP);	

		numberButtonAdd("   0   ",hboxes[0]);
		numberButtonAdd("   1   ",hboxes[0]);
		numberButtonAdd("   2   ",hboxes[0]);
	}



	private void rowTwoButtons() {
		hboxes[1]=new HBox(HBOX_GAP);	

		numberButtonAdd("   3   ",hboxes[1]);
		numberButtonAdd("   4   ",hboxes[1]);
		numberButtonAdd("   5   ",hboxes[1]);
	}

	private void rowThreeButtons() {
		hboxes[2]=new HBox(HBOX_GAP);	

		numberButtonAdd("   6   ",hboxes[2]);
		numberButtonAdd("   7   ",hboxes[2]);
		numberButtonAdd("   8   ",hboxes[2]);
	}

	private void rowFourButtons() {
		hboxes[3]=new HBox(HBOX_GAP);	

		numberButtonAdd("   9   ",hboxes[3]);
		returnButtonAdd(hboxes[3]); 
	}

	private void rowFiveButtons() {
		hboxes[4]=new HBox(HBOX_GAP);	
		numberButtonAdd("         "+BACKSPACE+"          ",hboxes[4]);
	}

	//creates the button for returning to what was being done
	private void returnButtonAdd(HBox h) {
		Button b=new Button("       "+RETURN+"      ");
		b.setStyle("-fx-background-color: Black; -fx-text-fill: White");
		b.setOnMouseClicked(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent arg0) {
				sender.send("Enter");
				onFinish.accept(null);
			}
		});		
		h.getChildren().add(b);

	}

	//creates the buttons for numbers keyboard
	private void numberButtonAdd(String s, HBox h) {
		Button b=new Button(s);
		b.setStyle("-fx-background-color: Black; -fx-text-fill: White");
		b.setOnMouseClicked(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent arg0) {
				sender.send(b.getText().replace(" ", ""));
			}
		});		
		h.getChildren().add(b);
	}

	//implementation of interface for integration
	@Override
	public IterableNodeGroups getIterableRoot() {
		return root;
	}

}
