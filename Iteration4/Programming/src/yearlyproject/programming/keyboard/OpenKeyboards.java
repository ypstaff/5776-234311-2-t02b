package yearlyproject.programming.keyboard;
/**
 * Interface for Keyboard handlers.
 * @author lmaman
 *
 */
public interface OpenKeyboards {
	public void numbers();
	public void regular();
	public void symbols();
}
