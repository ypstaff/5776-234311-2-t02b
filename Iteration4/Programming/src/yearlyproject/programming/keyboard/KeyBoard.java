package yearlyproject.programming.keyboard;

import java.util.Arrays;
import java.util.function.Consumer;

import com.google.inject.Inject;

import Navigation.IterableNodeGroup;
import Navigation.IterableNodeGroups;
import Navigation.IterationControlHBox;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import yearlyproject.programming.common.RootedObject;
import yearlyproject.programming.controlEngine.KeySender;
import yearlyproject.programming.tutorial.TutorialControlHBox;

/**
 *  Class for creating the Keyboard
 * @author Akiva Herer 
 *
 */

public class KeyBoard implements RootedObject {

	//the parameters of the keyboard
	private BorderPane m_mainPane;
	IterationControlHBox m_iterControlPane;
	private HBox hboxesNormal[];
	private HBox hboxesSpecial[];
	private Boolean buttonPressed[];
	private KeySender sender;
	private IterableNodeGroups root;
	Consumer<Void> onFinish;
	Consumer<Void> onTut;
	TutorialControlHBox tutorialHBox;
	private OpenKeyboards keyboards;
	private Boolean specialButtonOpen=false;
	private VBox specailButtons;
	private IterableNodeGroup nodesOfSpecialButtons[];


	//the constants of the keyboard
	//the order sent to send
	public static final int CAPS_LOCKED_PRESSED = 3;
	public static final int SHIFT_PRESSED = 2;
	public static final int ALT_PRESSED = 1;
	public static final int CTRL_PRESSED = 0;	


	public static final int NUM_ROWS = 7;
	public static final int ROW_ONE_SIZE =10;
	public static final int ROW_TWO_SIZE =14;
	public static final int ROW_THREE_SIZE =14;
	public static final int ROW_FOUR_SIZE =13;
	public static final int ROW_FIVE_SIZE =14;
	public static final int ROW_SIX_SIZE =11;
	public static final int ROW_SEVEN_SIZE =17;

	public static final int NUM_ROWS_SPECIAL = 5;
	public static final int ROW_ONE_SPECIAL_SIZE =4;
	public static final int ROW_TWO_SPECIAL_SIZE =4;
	public static final int ROW_THREE_SPECIAL_SIZE =4;
	public static final int ROW_FOUR_SPECIAL_SIZE =4;
	public static final int ROW_FIVE_SPECIAL_SIZE =4;

	public static final int HBOX_GAP = 2;
	public static final int VBOX_GAP = 4;

	public static final int SPECIAL_KEYS = 4;

	public static final String ESCAPE ="Esc";
	public static final String HOME ="Home";
	public static final String END ="End";
	public static final String INSERT ="Insert";
	public static final String DELETE ="Delete";
	public static final String BACKSPACE ="Backspace";
	public static final String TAB ="Tab";
	public static final String CAPSLOCK ="CapsLock";
	public static final String ENTER ="Enter";
	public static final String SHIFT ="Shift";
	public static final String CTRL ="Ctrl";
	public static final String ALT ="Alt";
	public static final String WINDOWS ="Windows";
	public static final String PRINT_SCREEN ="PrtSc";
	public static final String PAGE_UP ="PgUp";
	public static final String PAGE_DOWN ="PgDn";
	public static final String ARROW_UP ="Up"; 
	public static final String ARROW_DOWN ="Down";
	public static final String ARROW_LEFT ="Left";
	public static final String ARROW_RIGHT ="Right";
	public static final String SPACE ="Space";
	public static final String SYMBOL =" Symbol keyboard ";
	public static final String RETURN ="Return";
	public static final String OPEN_SPECIAL_BUTTONS = " Special Buttons ";


	@Inject
	public KeyBoard(final KeySender sender, Consumer<Void> onFinish, Consumer<Void> onTut, OpenKeyboards keyboards) {
		this.sender = sender;
		this.onFinish = onFinish;
		this.onTut = onTut;
		this.keyboards = keyboards;

		//building the keyboard
		hboxesNormal = new HBox [NUM_ROWS];
		buttonPressed = new Boolean[SPECIAL_KEYS];
		Arrays.fill(buttonPressed, Boolean.FALSE);
		buildRows();
		m_mainPane = new BorderPane();
		VBox right = new VBox(VBOX_GAP);
		for(int i=0; i<NUM_ROWS;i++){
			hboxesNormal[i].setAlignment(Pos.CENTER);			
			right.getChildren().add(hboxesNormal[i]);
		}
		right.setAlignment(Pos.CENTER);

		//creating special buttons
		hboxesSpecial = new HBox [NUM_ROWS_SPECIAL];
		nodesOfSpecialButtons= new IterableNodeGroup [NUM_ROWS_SPECIAL];
		specailButtons = new VBox(VBOX_GAP);
		buildRowsSpecial();
		for(int i=0; i<NUM_ROWS_SPECIAL;i++){
			hboxesSpecial[i].setAlignment(Pos.CENTER);			
			specailButtons.getChildren().add(hboxesSpecial[i]);
		}
		specailButtons.setAlignment(Pos.CENTER);

		//setting the keyboard in the pane	
		m_mainPane.setCenter(right);

		//integrating the keyboard with the navigation				
		root = new IterableNodeGroups(m_mainPane,Color.RED,Color.RED);		
		m_iterControlPane= root.getIterationControlHBoxControlHBox();
		m_mainPane.setBottom(m_iterControlPane);

		//integrating the keyboard with the Tutorial
//		tutorialHBox = new TutorialControlHBox(root, onTut ,m_mainPane);
//		tutorialHBox.setTutorialButtons();

		for (Node hbox : right.getChildrenUnmodifiable())
			root.addChildGroup(((HBox) hbox).getChildren(),4);
		int i=0;
		for (Node hbox : specailButtons.getChildrenUnmodifiable()){
			nodesOfSpecialButtons[i]=root.addChildGroup(((HBox) hbox).getChildren());
			++i;
		}
		for(int i1=0;i1<NUM_ROWS_SPECIAL;i1++)
			root.setEnabledMode(nodesOfSpecialButtons[i1], false);
	}


	//building the rows of the special buttons
	private void buildRowsSpecial(){
		rowOneSpecialButtons();
		rowTwoSpecialButtons();
		rowThreeSpecialButtons();
		rowFourSpecialButtons();
		rowFiveSpecialButtons();
	}

	//the five methods for creating special buttons rows
	private void rowOneSpecialButtons() {
		hboxesSpecial[0]=new HBox(HBOX_GAP);	
		Button fixText=specialButtonAddCtrl("  z     ",hboxesSpecial[0]);
		fixText.setText("  Undo  ");
		fixText= specialButtonAddCtrl(" "+SPACE,hboxesSpecial[0]);
		fixText.setText(" Content Assist ");
		fixText=specialButtonAddCtrl("  l     ",hboxesSpecial[0]);	
		fixText.setText(" Go To Line ");
		fixText=specialButtonAddCtrl("  d     ",hboxesSpecial[0]);	
		fixText.setText("  Delete Line  ");

	}

	private void rowTwoSpecialButtons() {
		hboxesSpecial[1]=new HBox(HBOX_GAP);	
		Button fixText = specialButtonAddCtrl("  c    ",hboxesSpecial[1]);	
		fixText.setText("    Copy    ");
		fixText=specialButtonAddCtrl("  v    ",hboxesSpecial[1]);
		fixText.setText("     Paste    ");
		fixText = specialButtonAddCtrl("  F11  ",hboxesSpecial[1]);
		fixText.setText("       Run       ");
		fixText=specialButtonAddCtrl("  i     ",hboxesSpecial[1]);
		fixText.setText("     Indent Fix    ");

	}

	private void rowThreeSpecialButtons() {
		hboxesSpecial[2]=new HBox(HBOX_GAP);	

		Button fixText=specialButtonAddCtrl(" /  ",hboxesSpecial[2]);
		fixText.setText(" Comment Row ");
		fixText=specialButtonAddCtrl("  h    ",hboxesSpecial[2]);	
		fixText.setText("   Search   ");
		fixText=specialButtonAddAltShift(" r",hboxesSpecial[2]);
		fixText.setText(" Rename ");
		fixText=specialButtonAddCtrlShift(" o",hboxesSpecial[2]);
		fixText.setText(" Add Import  ");


	}

	private void rowFourSpecialButtons() {
		hboxesSpecial[3]=new HBox(HBOX_GAP);
		
		Button fixText = specialButtonAddCtrl("  1     ",hboxesSpecial[3]);
		fixText.setText("Quick Fix ");
		fixText=specialButtonAddCtrl("  j     ",hboxesSpecial[3]);
		fixText.setText("Incremental Find");
		fixText=specialButtonAddCtrl("  o     ",hboxesSpecial[3]);
		fixText.setText("Quick Search");
		fixText=specialButtonAddCtrl("  t     ",hboxesSpecial[3]);
		fixText.setText("Type Tree");
	}

	private void rowFiveSpecialButtons() {
		hboxesSpecial[4]=new HBox(HBOX_GAP);
		
		Button fixText = specialButtonAddAlt(" "+ARROW_UP,hboxesSpecial[4]);
		fixText.setText(" Row Up ");
		fixText=specialButtonAddAlt(" "+ARROW_DOWN,hboxesSpecial[4]);
		fixText.setText("Row Down ");
		fixText=specialButtonAddCtrlShift(" r",hboxesSpecial[4]);
		fixText.setText("Open Resource  ");
		fixText=specialButtonAddCtrlShift(" t",hboxesSpecial[4]);
		fixText.setText("Open Type ");
	}


	//creating the special buttons in the keyBoard that have ctrl and then something
	private Button specialButtonAddCtrl(String s, HBox h) {
		//saving the original string
		String p=s;
		//Ctrl+p is the string that is sent in the end
		Button $=new Button(CTRL+s);
		$.setStyle("-fx-background-color: Black; -fx-text-fill: White");
		$.setOnMouseClicked(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent arg0) {
				sender.send(CTRL, p.replace(" ", ""));
			}
		});		
		h.getChildren().add($);
		return $;
	}

	//creating the special buttons in the keyBoard that have alt and then something
	private Button specialButtonAddAlt(String s, HBox h) {
		//saving the original string
		String p=s;
		//ALT+p is the string that is sent in the end
		Button $=new Button(ALT+s);
		$.setStyle("-fx-background-color: Black; -fx-text-fill: White");
		$.setOnMouseClicked(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent arg0) {
				sender.send(ALT, p.replace(" ", ""));
			}
		});		
		h.getChildren().add($);
		return $;
	}

	//creating the special buttons in the keyBoard that have Ctrl and Shift and then something
	private Button specialButtonAddCtrlShift(String s, HBox h) {
		//saving the original string
		String p=s;
		//Ctrl + Shift+p is the string that is sent in the end
		Button $=new Button(CTRL+" "+SHIFT+s);
		$.setStyle("-fx-background-color: Black; -fx-text-fill: White");
		$.setOnMouseClicked(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent arg0) {	
				sender.send(CTRL,SHIFT, p.replace(" ", ""));
			}
		});		
		h.getChildren().add($);
		return $;
	}

	//creating the special buttons in the keyBoard that have Alt and Shift and then something
	private Button specialButtonAddAltShift(String s, HBox h) {
		//saving the original string
		String p=s;
		//Alt + Shift+p is the string that is sent in the end
		Button $=new Button(ALT+" "+SHIFT+s);
		$.setStyle("-fx-background-color: Black; -fx-text-fill: White");
		$.setOnMouseClicked(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent arg0) {
				sender.send(ALT,SHIFT, p.replace(" ", ""));
			}
		});		
		h.getChildren().add($);
		return $;
	}

	//implementation of interface for integration
	@Override
	public IterableNodeGroups getIterableRoot() {
		return root;
	}

	//converting an integer n to a string of spaces of size n 
	private String intToString(int n){
		String $="";
		for(int i=0;i<n;++i)
			$ = $ + " ";
		return $;
	}

	//setting a button labels for shift use (yellow for when shift is pressed)
	private void setButtonYellow(Button b, String s1, String s2, int before, int middle, int after) {
		b.setText("");
		Label labelLeft = new Label(intToString(before)+s1+intToString(middle));
		labelLeft.setStyle("-fx-background-color: Black; -fx-text-fill: White ");
		Label labelRight = new Label(s2+intToString(after));
		labelRight.setStyle("-fx-background-color: Black; -fx-text-fill: Yellow");		
		HBox hBox = new HBox();
		hBox.getChildren().addAll( labelLeft, labelRight);
		b.setGraphic(hBox);
	}
	
	//building the rows of the keyboard
	private void buildRows(){
		rowOneButtons();
		rowTwoButtons();
		rowThreeButtons();
		rowFourButtons();
		rowFiveButtons();
		rowSixButtons();
		rowSevenButtons();
	}

	//the six methods for creating keyboard rows
	public void rowOneButtons(){
		hboxesNormal[0]=new HBox(HBOX_GAP);	
		standardNoChangeButtonAdd("    "+ARROW_UP+"    ",hboxesNormal[0]);
		standardNoChangeButtonAdd(" "+ARROW_DOWN+" ",hboxesNormal[0]);
		standardNoChangeButtonAdd("   "+ARROW_LEFT+"   ",hboxesNormal[0]);
		standardNoChangeButtonAdd("  "+ARROW_RIGHT+"  ",hboxesNormal[0]);
		standardNoChangeButtonAdd("   "+SPACE+"   ",hboxesNormal[0]);
		standardNoChangeButtonAdd("  "+ENTER+"  ",hboxesNormal[0]);
		standardNoChangeButtonAdd("     "+ESCAPE+"     ",hboxesNormal[0]);
		standardNoChangeButtonAdd("  "+DELETE+"  ",hboxesNormal[0]);	
		standardNoChangeButtonAdd("  "+BACKSPACE+" ",hboxesNormal[0]);
		returnButtonAdd(hboxesNormal[0]);
	}

	public void rowTwoButtons(){
		hboxesNormal[1]=new HBox(HBOX_GAP);	
		Button fixStyle = standardNoChangeButtonAdd("   `   ",hboxesNormal[1]);	
		setButtonYellow(fixStyle,"'","~",1,3,2);
		fixStyle=standardNoChangeButtonAdd("   1   ", hboxesNormal[1]);
		setButtonYellow(fixStyle,"1","!",1,3,2);
		fixStyle=standardNoChangeButtonAdd("   2   ", hboxesNormal[1]);
		setButtonYellow(fixStyle,"2","@",1,3,0);
		fixStyle=standardNoChangeButtonAdd("   3   ", hboxesNormal[1]);
		setButtonYellow(fixStyle,"3","#",1,3,1);
		fixStyle=standardNoChangeButtonAdd("   4   ", hboxesNormal[1]);
		setButtonYellow(fixStyle,"4","$",1,3,2);
		fixStyle=standardNoChangeButtonAdd("   5   ", hboxesNormal[1]);
		setButtonYellow(fixStyle,"5","%",1,3,1);
		fixStyle=standardNoChangeButtonAdd("   6   ", hboxesNormal[1]);
		setButtonYellow(fixStyle,"6","^",1,3,2);
		fixStyle=standardNoChangeButtonAdd("   7   ", hboxesNormal[1]);
		setButtonYellow(fixStyle,"7","&",1,3,0);
		fixStyle=standardNoChangeButtonAdd("   8   ", hboxesNormal[1]);
		setButtonYellow(fixStyle,"8","*",1,3,2);
		fixStyle=standardNoChangeButtonAdd("   9   ", hboxesNormal[1]);
		setButtonYellow(fixStyle,"9","(",1,3,2);
		fixStyle=standardNoChangeButtonAdd("   0   ",hboxesNormal[1]);
		setButtonYellow(fixStyle,"0",")",1,3,2);
		fixStyle=standardNoChangeButtonAdd("   -   ",hboxesNormal[1]);
		setButtonYellow(fixStyle,"-","_",1,3,2);
		fixStyle=standardNoChangeButtonAdd("   =   ",hboxesNormal[1]);
		setButtonYellow(fixStyle,"=","+",1,3,1);	
	}




	public void rowThreeButtons(){
		hboxesNormal[2]=new HBox(HBOX_GAP);	
		standardNoChangeButtonAdd(TAB+"       ",hboxesNormal[2]);
		letterButtonAdd("   q   ",hboxesNormal[2]);
		letterButtonAdd("   w   ",hboxesNormal[2]);
		letterButtonAdd("   e   ",hboxesNormal[2]);
		letterButtonAdd("   r   ",hboxesNormal[2]);
		letterButtonAdd("   t   ",hboxesNormal[2]);
		letterButtonAdd("   y   ",hboxesNormal[2]);
		letterButtonAdd("   u   ",hboxesNormal[2]);
		letterButtonAdd("   i   ",hboxesNormal[2]);
		letterButtonAdd("   o   ",hboxesNormal[2]);
		letterButtonAdd("   p   ",hboxesNormal[2]);
		Button fixStyle =standardNoChangeButtonAdd("   [   ",hboxesNormal[2]);
		setButtonYellow(fixStyle,"[","{",1,3,1);
		fixStyle =standardNoChangeButtonAdd("   ]   ",hboxesNormal[2]);
		setButtonYellow(fixStyle,"]","}",1,3,1);
		fixStyle =standardNoChangeButtonAdd("              \\",hboxesNormal[2]);
		setButtonYellow(fixStyle,"\\","|",6,4,1);
	}

	public void rowFourButtons(){
		hboxesNormal[3]=new HBox(HBOX_GAP);	
		specailButtonAdd(hboxesNormal[3],CAPSLOCK+"    ",CAPS_LOCKED_PRESSED);
		letterButtonAdd("    a    ",hboxesNormal[3]);
		letterButtonAdd("    s    ",hboxesNormal[3]);
		letterButtonAdd("    d    ",hboxesNormal[3]);
		letterButtonAdd("    f    ",hboxesNormal[3]);
		letterButtonAdd("    g    ",hboxesNormal[3]);
		letterButtonAdd("    h    ",hboxesNormal[3]);
		letterButtonAdd("    j    ",hboxesNormal[3]);
		letterButtonAdd("    k    ",hboxesNormal[3]);
		letterButtonAdd("    l     ",hboxesNormal[3]);
		Button fixStyle =standardNoChangeButtonAdd("   ;   ",hboxesNormal[3]);
		setButtonYellow(fixStyle,";",":",3,4,2);
		fixStyle =standardNoChangeButtonAdd("   '   ",hboxesNormal[3]);
		setButtonYellow(fixStyle,"'","\"",3,4,2);



	}

	public void rowFiveButtons(){
		hboxesNormal[4]=new HBox(HBOX_GAP);	
		Button fixStyle;
		specailButtonAdd(hboxesNormal[4],SHIFT+"                  ",SHIFT_PRESSED);
		letterButtonAdd("   z   ",hboxesNormal[4]);
		letterButtonAdd("   x   ",hboxesNormal[4]);
		letterButtonAdd("   c   ",hboxesNormal[4]);
		letterButtonAdd("   v   ",hboxesNormal[4]);
		letterButtonAdd("   b   ",hboxesNormal[4]);
		letterButtonAdd("   n   ",hboxesNormal[4]);
		letterButtonAdd("   m   ",hboxesNormal[4]);
		fixStyle =standardNoChangeButtonAdd("   ,   ",hboxesNormal[4]);
		setButtonYellow(fixStyle,",","<",1,3,1);
		fixStyle =standardNoChangeButtonAdd("   .   ",hboxesNormal[4]);
		setButtonYellow(fixStyle,".",">",1,3,1);
		fixStyle =standardNoChangeButtonAdd("   /   ",hboxesNormal[4]);
		setButtonYellow(fixStyle,"/","?",1,3,1);
		fixStyle = standardNoChangeButtonAdd(PAGE_UP,hboxesNormal[4]);
		fixStyle = standardNoChangeButtonAdd(PAGE_DOWN,hboxesNormal[4]);
	}

	public void rowSixButtons(){
		hboxesNormal[5]=new HBox(HBOX_GAP);	
		symbolButtonAdd(hboxesNormal[5]);
		specailButtonAdd(hboxesNormal[5],"     "+CTRL+"      ",CTRL_PRESSED);
		standardNoChangeButtonAdd("                         "+SPACE+"                          ",hboxesNormal[5]);
		standardNoChangeButtonAdd("   "+PRINT_SCREEN+"   ",hboxesNormal[5]);
		openSpecialButtonAdd(OPEN_SPECIAL_BUTTONS,hboxesNormal[5]);
		specailButtonAdd(hboxesNormal[5],"      "+ALT+"      ",ALT_PRESSED);
	}

	private void openSpecialButtonAdd(String s, HBox h) {
		Button b=new Button(s);
		b.setStyle("-fx-background-color: Black; -fx-text-fill: White");
		b.setOnMouseClicked(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent arg0) {
				if(specialButtonOpen){
					m_mainPane.setLeft(null);
					specialButtonOpen=false;
					for(int i=0;i<NUM_ROWS_SPECIAL;i++)
						root.setEnabledMode(nodesOfSpecialButtons[i], false);
					return;
				}
				m_mainPane.setLeft(specailButtons);
				specialButtonOpen=true;
				for(int i=0;i<NUM_ROWS_SPECIAL;i++)
					root.setEnabledMode(nodesOfSpecialButtons[i], true);
			}
		});		
		h.getChildren().add(b);


	}


	public void rowSevenButtons(){
		hboxesNormal[6]=new HBox(HBOX_GAP);	
		for(int i=1;i<=12;i++) {
			if(i<10)
				standardNoChangeButtonAdd("F" + i + "   ", hboxesNormal[6]);
			else
				standardNoChangeButtonAdd("F" + i+ " ", hboxesNormal[6]);
		}
		standardNoChangeButtonAdd(" "+HOME+" ",hboxesNormal[6]);
		standardNoChangeButtonAdd("  "+END+"   ",hboxesNormal[6]);
		standardNoChangeButtonAdd(""+INSERT+"",hboxesNormal[6]);
	}


	//creates the buttons that need special treatment- Ctrl, Shift, Alt, CapsLock
	private Button specailButtonAdd(HBox h,String name, int indexButtonPressed) {
		Button $=new Button(name);
		$.setStyle("-fx-background-color: Black; -fx-text-fill: White");
		$.setOnMouseClicked(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent arg0) {
				buttonPressed[indexButtonPressed] = !buttonPressed[indexButtonPressed];
				makeSureSameStyle();
			}

			private void styleNotPressed(Node b1) {
				b1.setStyle("-fx-background-color: Black; -fx-text-fill: White");
			}

			private void stylePressed(Node b1) {
				b1.setStyle("-fx-background-color: Yellow; -fx-text-fill: Black");
			}
			
			//makes sure the button is marked if it was pressed
			private void makeSureSameStyle() {
				for(Node n: h.getChildren())
					if ((((Button) n).getText()).replace(" ", "").equalsIgnoreCase(name.replace(" ", "")))
						if (buttonPressed[indexButtonPressed])
							stylePressed(n);
						else
							styleNotPressed(n);
			}
		});		
		h.getChildren().add($);
		return $;
	}

	//creates the non letter buttons
	public Button standardNoChangeButtonAdd(String s,HBox h){
		//saving the original string
		String p=s;
		Button $=new Button(s);
		$.setStyle("-fx-background-color: Black; -fx-text-fill: White");
		$.setOnMouseClicked(new EventHandler<MouseEvent>() {
			//send out string with proper additions 
			//(Shift, Ctrl, Alt, depending on if they are pressed)
			@Override
			public void handle(MouseEvent arg0) {
				if(!buttonPressed[CTRL_PRESSED]&&!buttonPressed[ALT_PRESSED]){
					if (!buttonPressed[SHIFT_PRESSED])
						sender.send(p.replace(" ", ""));
					else
						sender.send(SHIFT, p.replace(" ", ""));
					return;
				}
				if(buttonPressed[CTRL_PRESSED]&&!buttonPressed[ALT_PRESSED]){
					if (!buttonPressed[SHIFT_PRESSED])
						sender.send(CTRL, p.replace(" ", ""));
					else
						sender.send(CTRL, SHIFT, p.replace(" ", ""));
					return;
				}
				if(!buttonPressed[CTRL_PRESSED]&&buttonPressed[ALT_PRESSED]){
					if (!buttonPressed[SHIFT_PRESSED])
						sender.send(ALT, p.replace(" ", ""));
					else
						sender.send(ALT, SHIFT, p.replace(" ", ""));
					return;
				}
				if(buttonPressed[CTRL_PRESSED]&&buttonPressed[ALT_PRESSED]) {
					if (!buttonPressed[SHIFT_PRESSED])
						sender.send(CTRL, ALT, p.replace(" ", ""));
					else
						sender.send(CTRL, ALT, SHIFT, p.replace(" ", ""));
				}	
			}
		});		
		h.getChildren().add($);
		return $;
	}

	//creates the letter buttons
	public Button letterButtonAdd(String s,HBox h){
		Button $=new Button(s);
		$.setStyle("-fx-background-color: Black; -fx-text-fill: White");
		$.setOnMouseClicked(new EventHandler<MouseEvent>() {
			//send out string with proper additions 
			//(Shift, Ctrl, Alt, depending on if they are pressed)
			@Override
			public void handle(MouseEvent arg0) {//need 16 options to cover
				if(!buttonPressed[CTRL_PRESSED]&&!buttonPressed[ALT_PRESSED]){
					if (buttonPressed[SHIFT_PRESSED] && buttonPressed[CAPS_LOCKED_PRESSED]
							|| !buttonPressed[SHIFT_PRESSED] && !buttonPressed[CAPS_LOCKED_PRESSED])
						sender.send($.getText().replace(" ", ""));
					else
						sender.send($.getText().replace(" ", "").toUpperCase());
					return;
				}  
				if(buttonPressed[CTRL_PRESSED]&&!buttonPressed[ALT_PRESSED]){
					if (!buttonPressed[SHIFT_PRESSED])
						sender.send(CTRL, $.getText().replace(" ", ""));
					else
						sender.send(CTRL, SHIFT, $.getText().replace(" ", ""));
					return;
				}
				if(!buttonPressed[CTRL_PRESSED]&&buttonPressed[ALT_PRESSED]){
					if (!buttonPressed[SHIFT_PRESSED])
						sender.send(ALT, $.getText().replace(" ", ""));
					else
						sender.send(ALT, SHIFT, $.getText().replace(" ", ""));
					return;
				}
				if(buttonPressed[CTRL_PRESSED]&&buttonPressed[ALT_PRESSED]){
					if (!buttonPressed[SHIFT_PRESSED])
						sender.send(CTRL, ALT, $.getText().replace(" ", ""));
					else
						sender.send(CTRL, ALT, SHIFT, $.getText().replace(" ", ""));
				}

			}
		});		
		h.getChildren().add($);
		return $;
	}

	//creates the button for returning to what was being done
	private void returnButtonAdd(HBox h) {
		Button b=new Button("  "+RETURN+"  ");
		b.setStyle("-fx-background-color: Black; -fx-text-fill: White");
		b.setOnMouseClicked(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent arg0) {
				onFinish.accept(null);
			}
		});		
		h.getChildren().add(b);		
	}

	//creates the button to open symbol keyboard
	private Button symbolButtonAdd(HBox h) {
		Button $=new Button(SYMBOL);
		$.setStyle("-fx-background-color: Black; -fx-text-fill: White");
		$.setOnMouseClicked(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent arg0) {
				switchToSym();
			}
		});	
		h.getChildren().add($);
		return $;
	}

	//opens up Symbol Keyboard
	private void switchToSym() {
		keyboards.symbols();
	}



}