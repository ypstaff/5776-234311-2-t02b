package yearlyproject.programming.edit;

import javafx.scene.control.Button;
import yearlyproject.programming.common.ApplicationOpener;
import yearlyproject.programming.common.MenuButtons;
import yearlyproject.programming.controlEngine.KeySender;
import yearlyproject.programming.stackoverflow.Browser;
import yearlyproject.programming.tutorial.TutorialControlHBox;

import com.google.inject.Inject;

/**
 * Gui for grouping common use keys
 * @author Roy Svinik
 *
 */
public class CommonUseGui extends MenuButtons implements ApplicationOpener {

	public static final String SAVE_LABEL = "Save";
	public static final String RUN_LABEL = "Run";
	public static final String SEARCH_LABEL = "StackOverflow";
	public static final String TUTORIAL_LABAL = "Restart Tutorial";
	
	@Inject
	private final KeySender sender;
	
	public CommonUseGui(KeySender sender) {
		this.sender = sender;
		
		Button[] tmpMenu = {
			new Button(SAVE_LABEL),
			new Button(RUN_LABEL),
			new Button(SEARCH_LABEL),
			new Button(TUTORIAL_LABAL)
		};
		subMenu = tmpMenu;

		setGuiSubMenuActions();
	}
	
	@Override
	public Button[] getSubMenu() {
		return subMenu;
	}

	@Override
	public Button[] getMenuButtons() {
		return null;
	}
	
	/**
	 * sets the buttons handlers
	 */
	private void setGuiSubMenuActions() {
		applyEventHandler(subMenu, SAVE_LABEL, e -> sender.send("CTRL", "s"));
		applyEventHandler(subMenu, RUN_LABEL, e -> sender.send("CTRL", "F11"));
		
			applyEventHandler(subMenu, SEARCH_LABEL, e -> {
				try {
					runAnotherApp(Browser.class);
					browserConsumer.accept(null);
				} catch (Exception e1) {
					e1.printStackTrace();
				}
			});
		applyEventHandler(subMenu, TUTORIAL_LABAL, (e)->{
			TutorialControlHBox.restartTutorial();
		});
		
	}
	
}
