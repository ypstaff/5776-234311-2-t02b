package yearlyproject.programming.edit;

import java.util.function.Consumer;

import javafx.scene.control.Button;
import javafx.scene.layout.HBox;
import yearlyproject.programming.common.GuiSegment;
import yearlyproject.programming.common.MenuButtons;
import yearlyproject.programming.controlEngine.WindowedKeySender;

import com.google.inject.Inject;

/**
 * The main Gui if the Edit mode. Organizes all the sub menus
 * @author Roy Svinik
 *
 */
public class EditGui extends GuiSegment {
	
	@Inject
	public EditGui(final WindowedKeySender sender){
		this(sender, null);
	}
	
	@Inject
	public EditGui(final WindowedKeySender sender, Consumer<Void> onTut) {
		//a way around array initialization restrictions
		MenuButtons[] tmp = {
			new HotKeyGui(sender),
			new GenerateGui(sender),
			new EnterAndArrowsGui(sender),
			new HotKeyGui2(sender),
			new RefactorGui(sender),
			new CommonUseGui(sender)
		};
		menus = tmp;
		
		String[] tmpLabels = {
			"Hot Keys",
			"Code Templates",
			"Navigate",
			"More Hot Keys",
			"Refactor",
			"Common Use"
		};
		labels = tmpLabels;
		
		winTitle = "Edit";
		
		applyStyles();
		
		setOnTutorial(onTut);
		buildToVbox();
		
		//change buttons handlers to direct user to optimal next button
		
		//after clicking on generate move to arrows
		MenuButtons genGui = findMenu(GenerateGui.class);
		for(Button b : genGui.getSubMenu()) {
			setTransition(b, EnterAndArrowsGui.class);
		}
		
		MenuButtons refactorGui = findMenu(RefactorGui.class);
		setTransition(refactorGui.findButton(RefactorGui.MOVE_LABEL), EnterAndArrowsGui.class);
		setTransition(refactorGui.findButton(RefactorGui.CHANGE_METHOD_SIGNATURE_LABEL), EnterAndArrowsGui.class);
		setTransition(refactorGui.findButton(RefactorGui.INLINE_LABEL), EnterAndArrowsGui.class);
		setTransition(refactorGui.findButton(RefactorGui.OTHER_LABEL), EnterAndArrowsGui.class);
		
		MenuButtons hotkeyGui2 = findMenu(HotKeyGui2.class);
		setTransition(hotkeyGui2.findButton(HotKeyGui2.FIX_LABEL), EnterAndArrowsGui.class);
		
		// unpress shift when getting out of a row
		root.setOnLastBackConsumer(e ->  {
			EnterAndArrowsGui gui = (EnterAndArrowsGui) findMenu(EnterAndArrowsGui.class);
			if (gui.isShiftPressed) {
				gui.findButton(EnterAndArrowsGui.SHIFT_LABEL).getOnMouseClicked().handle(null);
			}
		});
	}
	
	/**
	 * Add the iteration groups differently
	 * controls the number of subgroups per row
	 */
	@Override
	protected void addGroup(int rowIdx) {
		int numOptionsPerBlock;
		switch (rowIdx) {
		case 0:
			numOptionsPerBlock = 4;
			break;
		case 1:
			numOptionsPerBlock = 1;
			break;
		case 2:
			numOptionsPerBlock = 5;
			break;
		case 3:
			numOptionsPerBlock = 3;
			break;
		case 4:
			numOptionsPerBlock = 4;
			break;
		case 5:
			numOptionsPerBlock = 1;
			break;
		default:
			numOptionsPerBlock = 1;
			break;
		}
		
		//if there is only one sub group make no subgroup
		if (numOptionsPerBlock == 1) 
			groups.add(root.addChildGroup(((HBox) vbox.getChildrenUnmodifiable().get(rowIdx)).getChildren()));
		else
			groups.add(root.addChildGroup(((HBox) vbox.getChildrenUnmodifiable().get(rowIdx)).getChildren(), numOptionsPerBlock));
	}
}
