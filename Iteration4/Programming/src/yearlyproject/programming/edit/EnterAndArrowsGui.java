package yearlyproject.programming.edit;

import javafx.scene.control.Button;
import yearlyproject.programming.common.MenuButtons;
import yearlyproject.programming.controlEngine.KeySender;

import com.google.inject.Inject;

/**
 * Gui for grouping code navigation buttons
 * @author Roy Svinik
 *
 */
public class EnterAndArrowsGui extends MenuButtons {

	public Boolean isShiftPressed = false;
	
	public static final String ENTER_LABEL = "Enter";
	public static final String DOWN_LABEL = "Down";
	public static final String UP_LABEL = "Up";
	public static final String RIGHT_LABEL = "Right";
	public static final String LEFT_LABEL = "Left";
	public static final String TAB_LABEL = "Tab";
	public static final String ESC_LABEL = "Esc";
	public static final String GOTO_LABEL = "GOTO Line";
	public static final String SHIFT_LABEL = "Shift";
	public static final String HOME_LABEL = "Home";
	public static final String END_LABEL = "End";
	public static final String PGDN_LABEL = "PgDn";
	public static final String PGUP_LABEL = "PgUp";
	
	@Inject
	private final KeySender sender;
	
	public EnterAndArrowsGui(KeySender sender) {
		this.sender = sender;
		
		Button[] tmpMenu = {
			new Button(DOWN_LABEL),
			new Button(UP_LABEL),
			new Button(ENTER_LABEL),
			new Button(RIGHT_LABEL),
			new Button(LEFT_LABEL),
			new Button(TAB_LABEL),
			new Button(ESC_LABEL),
			new Button(GOTO_LABEL),
			new Button(SHIFT_LABEL),
			new Button(HOME_LABEL),
			new Button(END_LABEL),
			new Button(PGDN_LABEL),
			new Button(PGUP_LABEL)
		};
		
		subMenu = tmpMenu; 
		
		setGuiSubMenuActions();
	}

	@Override
	public Button[] getSubMenu() {	
		return subMenu;
	}

	@Override
	public Button[] getMenuButtons() {
		return null;
	}
	
	/**
	 * sets the buttons handlers
	 */
	private void setGuiSubMenuActions() {
		applyEventHandler(subMenu, ENTER_LABEL, e -> sender.send("ENTER"));
		applyEventHandler(subMenu, DOWN_LABEL, e -> sender.send("DOWN"));
		applyEventHandler(subMenu, UP_LABEL, e -> sender.send("UP"));
		applyEventHandler(subMenu, RIGHT_LABEL, e -> sender.send("RIGHT"));
		applyEventHandler(subMenu, LEFT_LABEL, e -> sender.send("LEFT"));
		applyEventHandler(subMenu, TAB_LABEL, e -> sender.send("TAB"));
		applyEventHandler(subMenu, ESC_LABEL, e -> sender.send("ESC"));
		applyEventHandler(subMenu, GOTO_LABEL, e -> {sender.send("CTRL", "l"); keyboards.numbers();});
		applyEventHandler(subMenu, SHIFT_LABEL, e -> {
			if (isShiftPressed) {
				// return shift style to default
				subMenu[8].setStyle("-fx-background-color: black; -fx-text-fill: white");
				sender.releaseControl(KeySender.CONTROL_KEY.SHIFT);
			} else {
				// change shift style to appear pressed
				subMenu[8].setStyle("-fx-background-color: red; -fx-text-fill: white");
				sender.holdControl(KeySender.CONTROL_KEY.SHIFT);
			}
			isShiftPressed = !isShiftPressed;
		});
		applyEventHandler(subMenu, HOME_LABEL, e -> sender.send("HOME"));
		applyEventHandler(subMenu, END_LABEL, e -> sender.send("END"));
		applyEventHandler(subMenu, PGDN_LABEL, e -> sender.send("PgDn"));
		applyEventHandler(subMenu, PGUP_LABEL, e -> sender.send("PgUp"));
	}
	
}
