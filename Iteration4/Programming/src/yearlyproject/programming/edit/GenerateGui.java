package yearlyproject.programming.edit;

import javafx.scene.control.Button;
import yearlyproject.programming.common.MenuButtons;
import yearlyproject.programming.controlEngine.KeySender;

import com.google.inject.Inject;

/**
 * Gui for generating code templates
 * @author Roy Svinik
 *
 */
public class GenerateGui extends MenuButtons {

	public static final String LOOP_LABEL = "Loop";
	public static final String CTRL_LABEL = "Control Structure";
	public static final String VAR_LABEL = "Variable";
	public static final String METHOD_LABEL = "Method";
	
	
	@Inject
	private final KeySender sender;
	
	public GenerateGui(KeySender sender) {
		this.sender = sender;
		
		Button [] tmpMenu = {
			new Button(LOOP_LABEL),
			new Button(CTRL_LABEL),
			new Button(VAR_LABEL),
			new Button(METHOD_LABEL),
		};
		subMenu = tmpMenu; 
		
		setGuiSubMenuActions();
	}

	@Override
	public Button[] getSubMenu() {	
		return subMenu;
	}

	@Override
	public Button[] getMenuButtons() {
		return null;
	}
	
	/**
	 * sets the buttons handlers
	 */
	private void setGuiSubMenuActions() {
		applyEventHandler(subMenu, LOOP_LABEL, e -> { sender.send("l", "o", "o", "p"); 
			sender.openTemplateAssist();
		});
		applyEventHandler(subMenu, CTRL_LABEL, e -> { sender.send("c", "t", "r", "l"); 
			sender.openTemplateAssist();
		});
		applyEventHandler(subMenu, VAR_LABEL, e -> { sender.send("v", "a", "r"); 
			sender.openTemplateAssist();
		});
		applyEventHandler(subMenu, METHOD_LABEL, e -> { sender.send("m", "e", "t", "h", "o", "d"); 
			sender.openTemplateAssist();
		});
	}
	
	

}
