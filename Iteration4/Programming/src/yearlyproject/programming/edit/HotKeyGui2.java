package yearlyproject.programming.edit;

import javafx.scene.control.Button;
import yearlyproject.programming.common.MenuButtons;
import yearlyproject.programming.controlEngine.KeySender;

import com.google.inject.Inject;

/**
 * Gui for grouping more hotkeys
 * @author Roy Svinik
 *
 */
public class HotKeyGui2 extends MenuButtons {

	public static final String FIND_LABEL = "Find";
	public static final String FONT_UP_LABEL = "Font up";
	public static final String FONT_DOWN_LABEL = "Font down";
	public static final String FIX_LABEL = "Quick fix";
	public static final String GOTO_METHOD_LABEL = "GOTO def.";

	
	@Inject
	private final KeySender sender;
	
	public HotKeyGui2(KeySender sender) {
		this.sender = sender;
		
		Button [] tmpMenu = {
			new Button(FIND_LABEL),
			new Button(FIX_LABEL),
			new Button(GOTO_METHOD_LABEL),
			new Button(FONT_UP_LABEL),
			new Button(FONT_DOWN_LABEL)
		};
		subMenu = tmpMenu;
		
		setGuiSubMenuActions();
	}
	
	@Override
	public Button[] getSubMenu() {
		return subMenu;
	}

	@Override
	public Button[] getMenuButtons() {
		return null;
	}
	
	/**
	 * sets the buttons handlers
	 */
	private void setGuiSubMenuActions() {
		applyEventHandler(subMenu, FIND_LABEL, e -> { sender.send("CTRL", "f"); keyboards.regular();});
		applyEventHandler(subMenu, FONT_UP_LABEL, e -> { sender.send("CTRL", "SHIFT", "=");});
		applyEventHandler(subMenu, FONT_DOWN_LABEL, e -> { sender.send("CTRL", "-");});
		applyEventHandler(subMenu, FIX_LABEL, e -> sender.send("CTRL", "1"));
		applyEventHandler(subMenu, GOTO_METHOD_LABEL, e -> sender.send("F3"));
	}
	
}
