package yearlyproject.programming.edit;

import javafx.scene.control.Button;
import yearlyproject.programming.common.MenuButtons;
import yearlyproject.programming.controlEngine.WindowedKeySender;

import com.google.inject.Inject;

/**
 * Gui for grouping refactoring methods
 * @author Roy Svinik
 *
 */
public class RefactorGui extends MenuButtons {

	public static final String RENAME_LABEL = "Rename";
	public static final String MOVE_LABEL = "Move";
	public static final String CHANGE_METHOD_SIGNATURE_LABEL = "Change method signature";
	public static final String EXTRACT_METHOD_LABEL = "Extract method";
	public static final String EXTRACT_LOCAL_VARIABLE_LABEL = "Extract local variable";
	public static final String INLINE_LABEL = "Inline";
	public static final String OTHER_LABEL = "Other Refactoring";
	
	
	@Inject
	private final WindowedKeySender sender;
	
	public RefactorGui(WindowedKeySender sender) {
		this.sender = sender;
		
		Button [] tmpMenu = {
			new Button(RENAME_LABEL),
			new Button(MOVE_LABEL),
			new Button(CHANGE_METHOD_SIGNATURE_LABEL),
			new Button(EXTRACT_METHOD_LABEL),
			new Button(EXTRACT_LOCAL_VARIABLE_LABEL),
			new Button(INLINE_LABEL),
			new Button(OTHER_LABEL),
		};
		subMenu = tmpMenu; 
		
		
		setGuiSubMenuActions();
	}
	
	@Override
	public Button[] getSubMenu() {
		return subMenu;
	}

	@Override
	public Button[] getMenuButtons() {
		return null;
	}
	
	/**
	 * sets the buttons handlers
	 */
	private void setGuiSubMenuActions() {
		applyEventHandler(subMenu, RENAME_LABEL, e -> { sender.send("ALT", "SHIFT", "R"); keyboards.regular();});
		applyEventHandler(subMenu, MOVE_LABEL, e -> { sender.changeToVertical(); keyboards.regular(); sender.send("ALT", "SHIFT", "V");});
		applyEventHandler(subMenu, CHANGE_METHOD_SIGNATURE_LABEL, e -> { sender.changeToVertical(); keyboards.regular(); sender.send("ALT", "SHIFT", "C");});
		applyEventHandler(subMenu, EXTRACT_METHOD_LABEL, e -> { sender.changeToVertical(); keyboards.regular(); sender.send("ALT", "SHIFT", "M");});
		applyEventHandler(subMenu, EXTRACT_LOCAL_VARIABLE_LABEL, e -> { sender.send("ALT", "SHIFT", "L"); keyboards.regular();});
		applyEventHandler(subMenu, INLINE_LABEL, e -> { sender.send("ALT", "SHIFT", "I");});
		applyEventHandler(subMenu, OTHER_LABEL, e -> { sender.changeToVertical(); keyboards.regular(); sender.send("ALT", "SHIFT", "T");});
	}
	
}
