package yearlyproject.programming.edit;

import javafx.scene.control.Button;
import yearlyproject.programming.common.MenuButtons;
import yearlyproject.programming.controlEngine.KeySender;

import com.google.inject.Inject;

/**
 * Gui for grouping hotkeys
 * @author Roy Svinik
 *
 */
public class HotKeyGui extends MenuButtons {

	public static final String COPY_LABEL = "Copy";
	public static final String PASTE_LABEL = "Paste";
	public static final String UNDO_LABEL = "Undo";
	public static final String REDO_LABEL = "Redo";
	public static final String RM_LINE_LABEL = "Remove Line";
	public static final String KBD_LABEL = "Keyboard";
	private static final String COMMENT_LABEL = "Comment";
	
	
	@Inject
	private final KeySender sender;
	
	public HotKeyGui(KeySender sender) {
		this.sender = sender;
		
		Button [] tmpMenu = {
			new Button(KBD_LABEL),
			new Button(UNDO_LABEL),
			new Button(REDO_LABEL),
			new Button(COPY_LABEL),
			new Button(PASTE_LABEL),
			new Button(RM_LINE_LABEL),
			new Button(COMMENT_LABEL)
		};
		subMenu = tmpMenu; 
		
		setGuiSubMenuActions();
	}
	
	@Override
	public Button[] getSubMenu() {
		return subMenu;
	}

	@Override
	public Button[] getMenuButtons() {
		return null;
	}
	
	/**
	 * sets the buttons handlers
	 */
	private void setGuiSubMenuActions() {
		applyEventHandler(subMenu, COPY_LABEL, e -> sender.send("CTRL", "c"));
		applyEventHandler(subMenu, PASTE_LABEL, e -> sender.send("CTRL", "v"));
		applyEventHandler(subMenu, UNDO_LABEL, e -> sender.send("CTRL", "z"));
		applyEventHandler(subMenu, REDO_LABEL, e -> sender.send("CTRL", "y"));
		applyEventHandler(subMenu, RM_LINE_LABEL, e -> sender.send("CTRL", "d"));
		applyEventHandler(subMenu, KBD_LABEL, e -> keyboards.regular());
		applyEventHandler(subMenu, COMMENT_LABEL, e -> sender.send("CTRL", "/"));
	}
	
}
