package yearlyproject.programming.tutorial;

import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import yearlyproject.programming.common.GuiSegment;
import yearlyproject.programming.common.ProgrammingController;
import yearlyproject.programming.common.RootedObject;

import java.util.function.Consumer;

import com.google.inject.Inject;

import Navigation.IterableNodeGroups;
import Navigation.IterationControlHBox;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

/**
 *  Class for creating the Tutorial
 * @author Akiva Herer 
 *
 */


public class Tutorial implements RootedObject {
	
	//the parameters of the tutorial for each class
	private BorderPane m_mainPane;
	IterationControlHBox m_iterControlPane;
	private IterableNodeGroups root;
	Consumer<Void> onFinish;
	private HBox tutorialButtons;
	private Image image;	
	private ImageView imv;
	
	//the parameters of the tutorial for all classes
	public static int tutorialPage=1; 
	
	public static final int NUM_PAGES_TUTORIAL = 47;
	
	public static final int HBOX_GAP = 2;
	public static final int VBOX_GAP = 4;

	public static final String PREVIOUS ="Previous";
	public static final String NEXT ="Next";
	public static final String RETURN ="Return";
	public static final String winTitle = "Tutorial";
	
	
	@Inject
	public Tutorial(Consumer<Void> onFinish) {
		this.onFinish = onFinish;
		
		//building the tutorial
		m_mainPane = new BorderPane();
		buildTutorial();
		VBox top = new VBox(VBOX_GAP);
		tutorialButtons.setAlignment(Pos.CENTER);			
		top.getChildren().add(tutorialButtons);
		top.setAlignment(Pos.CENTER);
		m_mainPane.setTop(top);
		
		//integrating the tutorial with the navigation				
		root = new IterableNodeGroups(m_mainPane);		
		m_iterControlPane= root.getIterationControlHBoxControlHBox();
		m_mainPane.setBottom(m_iterControlPane);
		root.addChildGroup(tutorialButtons.getChildren());
		
		//setting label and bluetooth indicator
		VBox rightVbox = new VBox(3);
		Label winLabel = new Label(winTitle);
		winLabel.setStyle("-fx-font-size: 30.5;");
		rightVbox.getChildren().add(winLabel);
		
		if (ProgrammingController.BTConnected) {
			GuiSegment.setBTIndicator(rightVbox, true);
		}
		else{
			GuiSegment.setBTIndicator(rightVbox, false);
		}
		
		m_mainPane.setRight(rightVbox);
	}

	//builds the tutorials buttons
	private void buildTutorial() {
		tutorialButtons=new HBox(HBOX_GAP);	
		tutorialButtonAdd(PREVIOUS);
		tutorialButtonAdd(RETURN);
		tutorialButtonAdd(NEXT);
		displayCorrectImage();
	}
	
	//creates the buttons for the tutorial
	private void tutorialButtonAdd(String s) {
		Button b=new Button(s);
		b.setStyle("-fx-background-color: Black; -fx-text-fill: White");
		b.setOnMouseClicked(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent arg0) {
				if(b.getText().equals(PREVIOUS)){
					handlePrevious();
					return;
				}
				if(b.getText().equals(NEXT)){
					handleNext();
					return;
				}
				handleReturn();
			}

			private void handleReturn() {
				onFinish.accept(null);
			}
			
			//goes to next tutorial page
			private void handleNext() {
				if(tutorialPage == NUM_PAGES_TUTORIAL)
					return;
				++tutorialPage;
				displayCorrectImage();
			}

			//goes to previous tutorial page
			private void handlePrevious() {
				if(tutorialPage == 1)
					return;
				--tutorialPage;
				displayCorrectImage();
			}
		});		
		tutorialButtons.getChildren().add(b);
		
	}
	
	//Displays the correct tutorial Page
	private void displayCorrectImage() {
		imv = new ImageView();
        image = new Image(Tutorial.class.getResourceAsStream("Page"+tutorialPage+".png"));
        imv.setImage(image);
		m_mainPane.setCenter(imv);
	}

	//implementation of interface for integration
	@Override
	public IterableNodeGroups getIterableRoot() {
		return root;
	}
	

	
	
}
