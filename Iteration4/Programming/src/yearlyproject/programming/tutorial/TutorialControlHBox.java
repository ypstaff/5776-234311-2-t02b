package yearlyproject.programming.tutorial;

import java.util.function.Consumer;

import yearlyproject.programming.common.ProgrammingController;
import yearlyproject.programming.controlEngine.ConfigManager;
import Navigation.IterableNodeGroup;
import Navigation.IterableNodeGroups;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;

/**
 *  Class for creating the buttons to call Tutorial from other pages
 * @author Akiva Herer 
 *
 */


public class TutorialControlHBox extends HBox {
	
	//the parameters of the tutorial calling buttons for each class
	public Button goToTutorial;
	public Button doneWithTutorial;
	private Consumer<Void> onTut;
	private IterableNodeGroups root;
	private BorderPane thisPane;
	private IterableNodeGroup iter;
	
	//the parameters of the tutorial calling buttons for all classes
	public static final int HBOX_GAP = 2;
	public static boolean doneWithTutorialBool = false;

	
	public TutorialControlHBox(IterableNodeGroups root, Consumer<Void> onTut, BorderPane thisPane) {		
		this.onTut = onTut;
		this.root = root;
		this.thisPane = thisPane;

		//building the tutorial calling buttons
		//button to call the tutorial
		goToTutorial=new Button("Go To Tutorial");
		goToTutorial.setStyle("-fx-background-color: Black; -fx-text-fill: White");
		goToTutorial.setOnMouseClicked(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent arg0) {
				funcCallTutorial();				
			}
		});		
		this.getChildren().add(goToTutorial);
		
		//button to finish with the tutorial for the rest program, until restart tutorial
		doneWithTutorial = new Button("Done With Tutorial");
		doneWithTutorial.setStyle("-fx-background-color: Black; -fx-text-fill: White");
		doneWithTutorial.setOnMouseClicked(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent arg0) {
				doneWithTutorialBool=true;
				ConfigManager.registerTutorialDone(doneWithTutorialBool);
				root.setEnabledMode(iter, false);
				//Select segment's first row and hide Tutorial pane
				root.unselect();
				root.next();
				TutorialControlHBox.this.setVisible(false);
				ProgrammingController.refreshCurrentSegment(false);
			}
		});		
		this.getChildren().add(doneWithTutorial);
		this.setAlignment(Pos.CENTER);
	}
	
	//calls the tutorial
	protected void funcCallTutorial() {
		onTut.accept(null);
	}

	//sets the tutorial buttons on the top of the pane
	public void setTutorialButtons() {
		if(doneWithTutorialBool)
			return;
		this.setSpacing(HBOX_GAP);
		iter = root.addChildGroup(this.getChildren());
		thisPane.setTop(this);
	}
	
	//to restart the tutorial after closing it
	public static void restartTutorial(){
		doneWithTutorialBool = false;
		ConfigManager.registerTutorialDone(doneWithTutorialBool);
		ProgrammingController.refreshCurrentSegment(false);
	}
	
	//to initialize the tutorial when opening eclipse
	public static void initTutorial(){
		String tutorialDone = ConfigManager.getValue("tutorialDone");
		if (tutorialDone == null)
			doneWithTutorialBool = false;
		else{
			if(tutorialDone.equals("true"))
				doneWithTutorialBool = true;
			else
				doneWithTutorialBool = false;
		}
	}
		

}
