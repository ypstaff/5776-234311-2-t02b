package yearlyproject.programming.controlEngine;

import java.io.File;
import java.io.IOException;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;

import org.ini4j.InvalidFileFormatException;
import org.ini4j.Wini;

public class ConfigManager {
	private static Wini ini = null;
	/**
	 * Initiate config manager.
	 * @return ini file or null if a problem occurred.
	 */
	static void init() {
		if (ini != null)
			return;
		String appdataDir = System.getenv("appdata");
		System.out.println(appdataDir);
		File iniFile = new File(appdataDir + "/Brainiac/Brainiac.ini");
		if(!iniFile.exists()){
			try {
				iniFile.createNewFile();
			} catch (IOException e1) {
				return;
			}
		}
		
		
		try {
			ini = new Wini(iniFile);
		} catch (InvalidFileFormatException e) {
			return;
		} catch (IOException e) {
			return;
		}
	}
	
	/**
	 * Set use default property in to false  from outside Eclipse Finder.
	 */
	public static void registerUseDeafaultToFalse(){
		if (ini == null)
			return;
		registerVal("useDefault", "false");
	}
	
	/**
	 * Set use default property in to true  from outside Eclipse Finder.
	 */
	public static void registerUseDeafaultToTrue(){
		if (ini == null)
			return;
		
		registerVal("useDefault", "true");
	}
	
	/**
	 * Get the value associated with key, on null if doesn't exist.
	 * @param key the key
	 * @return value associated with key, on null if doesn't exist
	 */
	public static String getValue(String key){
		if (ini == null)
			return null;
		return ini.get("Settings", key);
	}
	
	/**
	 * Register value <b>val</b> to key <b>key</b>.
	 * @param key property key to register.
	 * @param val property value to register.
	 */
	private static void registerVal(String key, String val) {
		if (ini == null)
			return;
		System.out.println("registering: " + key + " " + val);

		ini.put("Settings", key , val);
		try {
			ini.store();
		} catch (IOException e) {
			if (e.getMessage().contains("(Access is denied)")){
				Alert alert = new Alert(AlertType.ERROR, "Changing settings requires Admin access. Please re-run as admin.");
				alert.showAndWait();
				System.exit(1);
			}
		}
	}
	
	/**
	 * Register Eclipse directory path
	 * @param eclipsePath
	 */
	public static void registerEclipsePath(String eclipsePath) {
		registerVal("eclipsePath", eclipsePath);
	}
	
	/**
	 * Register is tutorial done
	 */
	public static void registerTutorialDone(Boolean done) {
		if (done)
			registerVal("tutorialDone", "true");
		else
			registerVal("tutorialDone", "false");
	}

}
