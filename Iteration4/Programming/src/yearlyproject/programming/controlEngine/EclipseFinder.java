package yearlyproject.programming.controlEngine;

import java.io.File;
import java.util.Optional;

import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.DialogPane;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;
import javafx.util.Callback;
/**
 * Find Eclipse directory path, using default value or user choice.
 * @author lmaman
 *
 */
public class EclipseFinder {

	/**
	 * Creates an Eclipse finder.
	 * @param primaryStage stage to fill with Gui elements (alerts and Directory Chooser).
	 * @param y y coordinate for Gui elements
	 * @return Eclipse directory path
	 */
	public String findEclipsePath(Stage primaryStage){
		String eclipsePath = ConfigManager.getValue("eclipsePath");
		String useDefault = ConfigManager.getValue("useDefault");
		
		if(useDefault == null){
			ConfigManager.registerUseDeafaultToFalse();
			useDefault = "false";
		}
		System.out.println(eclipsePath);
		System.out.println(useDefault);
		
		if(eclipsePath == null || primaryStage == null)
		{
			String workDir = EclipseFinder.class.getProtectionDomain().getCodeSource().getLocation().getPath();
			if(workDir.contains("Iteration4/Programming")){
				eclipsePath = workDir.split("Iteration4/Programming")[0] + "Iteration4/Programming/eclipse";
				eclipsePath = eclipsePath.replace("/C:", "C:").replace("/", "\\");
			}
		}

		
		if (!useDefault.equals("true") || eclipsePath == null){
			System.out.println(eclipsePath);
			if (promotUserChangeVersion(eclipsePath)){
				eclipsePath = chooseEclipePath(primaryStage, eclipsePath);
			}
		}
		
		return eclipsePath;
	}
	
	/**
	 * Register use default value
	 * @param val
	 */
	public void registerUseDefault(String val) {
		if(val == "false")
			ConfigManager.registerUseDeafaultToFalse();
		else if(val == "true")
			ConfigManager.registerUseDeafaultToTrue();
	}
	
	/**
	 * Ask the user if he would like to change Eclipse version.
	 * @param eclipsePath currently used Eclipse version path
	 * @return user decision
	 */
	private boolean promotUserChangeVersion(String eclipsePath) {
		ButtonType buttonTypeYes = ButtonType.YES;
		ButtonType buttonTypeNo = ButtonType.NO;
		ButtonType buttonTypeCancel = ButtonType.CANCEL;
		
		Callback<Boolean, Void> optOutAction = param ->{
			registerUseDefault("true");
			return null;
		};
		Alert alert;
		
		if(eclipsePath == null){
			alert = createAlertWithOptOut(AlertType.CONFIRMATION,
					"Change Eclipse version",
					"Please choose Eclipse directory",
					"", "Never ask again",
					optOutAction, ButtonType.OK, buttonTypeCancel);
		}
		else{
			alert = createAlertWithOptOut(AlertType.CONFIRMATION,
					"Change Eclipse version",
					"Would you like to change your Eclipse version?",
					"Currently using: " + eclipsePath, "Never ask again",
					optOutAction, buttonTypeYes, buttonTypeNo, buttonTypeCancel);
					//Deactivate Defaultbehavior for yes-Button:
				    Button yesButton = (Button) alert.getDialogPane().lookupButton( ButtonType.YES );
				    yesButton.setDefaultButton( false );
		
				    //Activate Defaultbehavior for no-Button:
				    Button noButton = (Button) alert.getDialogPane().lookupButton( ButtonType.NO );
				    noButton.setDefaultButton( true );
		}
		
		
		Optional<ButtonType> result = alert.showAndWait();
		if (result.get() == buttonTypeYes || result.get() == ButtonType.OK){
		    // ... user chose "Yes"
			return true;
		} else if (result.get() == buttonTypeNo) {
		    // ... user chose "No"
			return false;
		} else {
		    // ... user chose CANCEL or closed the dialog
			System.exit(0);
		}
		return false;
	}

	/**
	 * Prompt the user a Directory Chooser to choose Eclipse path
	 * @param primaryStage stage to fill with Directory Chooser
	 * @param eclipsePath initial path
	 * @return chosen Eclipse path, or null if user canceled.
	 */
	private String chooseEclipePath(Stage primaryStage, String eclipsePath) {
		if (primaryStage == null)
			return eclipsePath;
		
		DirectoryChooser directoryChooser = new DirectoryChooser();
		if (eclipsePath != null){
			File initDir = new File(eclipsePath);
			directoryChooser.setInitialDirectory(initDir);
		}
		
		directoryChooser.setTitle("Choose Eclipse directory");
		File selectedDirectory = 
                directoryChooser.showDialog(primaryStage);
        String labelSelectedDirectory = null;
		if(selectedDirectory != null){
            labelSelectedDirectory = selectedDirectory.getPath();
        }
	
		
		System.out.println(labelSelectedDirectory);
		return labelSelectedDirectory;
	}

	/**
	 * Create an Alert with buttons and a check box.
	 * Taken from StackOverflow, link below.
	 * @see <a href="http://stackoverflow.com/questions/36949595/how-do-i-create-a-javafx-alert-with-a-check-box-for-do-not-ask-again">StackOverflow</a>
	 * @param type the Alert type.
	 * @param title the Alert title.
	 * @param headerText the header test.
	 * @param message the Alert message.
	 * @param optOutMessage the check box message.
	 * @param optOutAction the check box on check action.
	 * @param buttonTypes the buttons.
	 * @return the Alert created.
	 */
	private static Alert createAlertWithOptOut(AlertType type, String title,
			String headerText, String message, String optOutMessage,
			Callback<Boolean, Void> optOutAction, ButtonType... buttonTypes) {
		Alert alert = new Alert(type);
		// Need to force the alert to layout in order to grab the graphic,
		// as we are replacing the dialog pane with a custom pane
		alert.getDialogPane().applyCss();
		Node graphic = alert.getDialogPane().getGraphic();
		// Create a new dialog pane that has a checkbox instead of the hide/show
		// details button
		// Use the supplied callback for the action of the checkbox
		alert.setDialogPane(new DialogPane() {
			@Override
			protected Node createDetailsButton() {
				CheckBox optOut = new CheckBox();
				optOut.setText(optOutMessage);
				optOut.setOnAction(e -> optOutAction.call(optOut.isSelected()));
				return optOut;
			}
		});
		alert.getDialogPane().getButtonTypes().addAll(buttonTypes);
		alert.getDialogPane().setContentText(message);
		// Fool the dialog into thinking there is some expandable content
		// a Group won't take up any space if it has no children
		alert.getDialogPane().setExpandableContent(new Group());
		alert.getDialogPane().setExpanded(true);
		// Reset the dialog graphic using the default style
		alert.getDialogPane().setGraphic(graphic);
		alert.setTitle(title);
		alert.setHeaderText(headerText);
		return alert;
	}

	
}
