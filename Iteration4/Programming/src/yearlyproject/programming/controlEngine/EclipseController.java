package yearlyproject.programming.controlEngine;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.StandardOpenOption;
import java.util.Scanner;
import java.util.function.Consumer;

import yearlyproject.programming.common.ProgrammingController;
import yearlyproject.programming.stackoverflow.Browser;
import javafx.geometry.Rectangle2D;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Screen;
import javafx.stage.Stage;
import autoitx4java.AutoItX;

import com.google.common.io.Files;
import com.jacob.com.LibraryLoader;
/**
 * Controls Eclipse using Autoitx4java.
 * @author lmaman
 * @pre
 * 		Install or register dll of Autoit
 */

public class EclipseController implements WindowedKeySender {

	private static final double VERTICAL_REGULAR_PROPORTION = 0.4;
	private static final double VERTIVAL_THIN_MENUS_PROPORTION = 0.7;
	
	private static final String BRAINIAC_CONTROLLER = "Brainiac Controller";
	private static final String exeTitle = "eclipse";
	private static String Title = "3 Buttons Eclipse";
	private static boolean MEASURE_TIME = false;
	
	AutoItX autoItInstace;
	String eclipsePath;
	String currentlyDown = "";
	int numOfAllSections = 5, numOfTakenSegments = 3;
	private Stage primaryStage;
	private Rectangle2D currentDim;
	private boolean isInVertical = false;
	

	/**
	 * Creates a new 3 Button Eclipse controller.
	 * Will launch a new 3 Button Eclipse instance in case a 3 Button Eclipse is not open.
	 * 
	 * ( An eclipse instance is a 3 Buttons Eclipse if the string "3 Buttons Eclipse" is in it's title.
	 *  This can be achieved by changing the workspace name. See the README for more details.  )
	 *  
	 * @param eclipsePath - the path to the Eclipse executable directory.
	 */
	public EclipseController(Stage primaryStage) {
		super();
		
		File jacobFile = new File("lib", "jacob-1.18-x64.dll"); // path to the
																// jacob dll
		System.setProperty(LibraryLoader.JACOB_DLL_PATH,
				jacobFile.getAbsolutePath());

		ConfigManager.init();

		try{
			autoItInstace = new AutoItX();
		}catch(com.jacob.com.ComFailException e){
			Alert alert = new Alert(AlertType.ERROR, "No Autoit was found, please install Autoit or register the dll in Resources from the installation directory.");
			alert.showAndWait().ifPresent((x)->System.exit(1));
		}
		autoItInstace.setOption(AutoItX.OPT_WIN_TITLE_MATCH_MODE, "2");
		this.primaryStage = primaryStage;
		
		//Check if 3 buttons Eclipse is already running
		if (!autoItInstace.winExists(EclipseController.Title)) {
			findAndRunEclipse(primaryStage);
		}

		//autoItInstace.winSetOnTop(Title, "", true);
		setFocus();

		System.out.println("Eclipse open and ready");
	}

	@Override
	public void send(String... keysLabels) {
		long beginTime = 0;
		if (MEASURE_TIME) {
			beginTime = System.nanoTime();
		}
		
		//setFocus();
		sendNoFocus(keysLabels);

		if (MEASURE_TIME) {
			double seconds = (double) (System.nanoTime() - beginTime) / 1000000000.0;
			System.out.println("sending " + keysLabels + " took: " + seconds);
		}

	}
	
	@Override
	public void openView(VIEW w) {
			String viewChar = null;
			switch (w) {
			case BREAKPOINTS:
				viewChar = "b";
				break;
			case OUTLINE:
				viewChar = "o";
				break;
			case PACKAGES_EXPLORER:
				viewChar = "p";
				break;
			case PROBLEMS:
				viewChar = "x";
				break;
			case SEARCH:
				viewChar = "s";
				break;
			case CONSOLE:
				viewChar = "c";
				break;
			case VARIABLES:
				viewChar = "v";
				break;
				
				
			case EDITOR:
				send("ctrl", "q");
				return;
			default:
				break;
			}
			send("alt", "shift", "q");
			sendNoFocus(viewChar);
		}

	@Override
	public void holdControl(CONTROL_KEY key) {
		if (currentlyDown != ""){
			throw new RuntimeException("Can't hold more than one key at a time. Currently holding: " + currentlyDown);
		}
			
		currentlyDown = key.name();
		send(key.name()+"DOWN");
	}

	@Override
	public void releaseControl(CONTROL_KEY key) {
		if (key.name() != currentlyDown){
			throw new RuntimeException("Need to release " + currentlyDown + " but was requested to release " + key.name());
		}
		
		currentlyDown = "";
		send(key.name()+"UP");
	}
	
	@Override
	public void openTemplateAssist() {
		send("CTRL", "SPACE", "CTRL", "SPACE");
	}
	
	@Override
	public void resize(int numOfAllSections,int numOfTakenSegments, boolean gravityBottom){
		if (isInVertical == true)
			return;
		currentDim = ProgrammingController.getDimentsions(numOfAllSections, numOfTakenSegments, gravityBottom);
		autoItInstace.winMove(EclipseController.Title, "", (int)currentDim.getMinX(), (int)currentDim.getMinY(), (int)currentDim.getWidth(), (int)currentDim.getHeight());
	}

	@Override
	public void checkTitleAndDo(String inTitle, Boolean contain, Consumer<Void> action) {
		if(autoItInstace.winGetTitle("[ACTIVE]").contains(inTitle) != contain)
			action.accept(null);
	}
	
	@Override
	public void restart() {
		autoItInstace.winClose(EclipseController.Title);
		//Wait for window to be closed
		while (autoItInstace.winExists(EclipseController.Title))
			autoItInstace.sleep(500);
		
		autoItInstace.sleep(1000);
		findAndRunEclipse(primaryStage);
		autoItInstace.sleep(1000);
		setFocus();
		
	}

	@Override
	public void changeToVertical() {
		changeToVertical(false);
	}
	
	public void changeToVertical(boolean thinMenusMode) {
		if (isInVertical == true)
			return;
		isInVertical = true;
		System.out.println("Changaing to vertical");
		Rectangle2D bounds = Screen.getPrimary().getVisualBounds();
		int newHeight = (int) (bounds.getHeight());
		double proportion;
		if (thinMenusMode) {
			proportion = VERTIVAL_THIN_MENUS_PROPORTION;
		} else {
			proportion = VERTICAL_REGULAR_PROPORTION;
		}
        int newWidth = (int) (bounds.getWidth() * proportion);
        int newX = (int) (bounds.getWidth() * (1-proportion));
        int newY = 0;
        
        autoItInstace.winMove(EclipseController.Title, "", newX, newY, newWidth, newHeight);
        autoItInstace.winMove(BRAINIAC_CONTROLLER, "", 0, 0, newX, newHeight);
        Browser.setPrimaryStageSizes(new Rectangle2D(newX, newY, newWidth, newHeight));
	}

	@Override
	public void backToHorozotial() {
		System.out.println("Back from vertical");

		Rectangle2D bounds = Screen.getPrimary().getVisualBounds();

		autoItInstace.winMove(EclipseController.Title, "", (int)currentDim.getMinX(), (int)currentDim.getMinY(), (int)currentDim.getWidth(), (int)currentDim.getHeight());
		autoItInstace.winMove(BRAINIAC_CONTROLLER, "", 0, 0, (int) bounds.getWidth(), (int)(bounds.getHeight() - currentDim.getHeight()));
		isInVertical = false;
		
	}

	@Override
	public void resizeController(Integer numOfAllSections,
			Integer numOfTakenSegments, boolean gravityBottom) {
		if (isInVertical == true)
			return;
		Rectangle2D rec = ProgrammingController.getDimentsions(numOfAllSections, numOfTakenSegments, gravityBottom);
		autoItInstace.winMove(BRAINIAC_CONTROLLER, "", (int)rec.getMinX(), (int)rec.getMinY(), (int)rec.getWidth(), (int)rec.getHeight());
	}
	
	
	/**
	 * Find the path to Eclipse directory and run Eclipse.
	 * Looks in the configuration manager, and prompt to the user a request accordingly.
	 * @param primaryStage the stage to put gui on
	 */
	private void findAndRunEclipse(Stage primaryStage) {
		String exePath;
		EclipseFinder eFinder = new EclipseFinder();
		// If no stage was given, mainly for development.
		if (primaryStage == null) {
			eclipsePath = eFinder.findEclipsePath(null);
			// System.out.println(eclipsePath);
			exePath = this.eclipsePath + "\\" + exeTitle + ".exe";
			runEclipse(exePath);

		} else { 
			Boolean foundEclipse = false;
			// Keep asking the user until Eclipse is found
			while (!foundEclipse) {

				this.eclipsePath = eFinder.findEclipsePath(primaryStage);
				// No Eclipse folder was chosen, try again
				if (eclipsePath == null) {
					continue;
				}
				exePath = this.eclipsePath + "\\" + exeTitle + ".exe";

				Boolean succses = true;
				try {
					runEclipse(exePath);
				} catch (RuntimeException e) {
					Alert alert = new Alert(AlertType.ERROR, e.getMessage());
					alert.showAndWait();
					succses = false;
				}
				if (succses) {
					foundEclipse = true;
					ConfigManager.registerEclipsePath(eclipsePath);
				}
			}
		}
	}
	
	/**
	 * Run Eclipse.
	 * @param exePath path to Eclipse executable.
	 * @throws RuntimeException if executable does not exist.
	 */
	private void runEclipse(String exePath) {
		System.out.println(exePath);
		File f = new File(exePath);
		if (!f.exists()) {
			String error = "Error: Eclipe executable is not in the expected location: "
					+ exePath;
			throw new RuntimeException(error);
		}
		
		configWorkspace();
		

		autoItInstace.run("\""+ exePath +"\"", ".", AutoItX.SW_SHOWNORMAL);
		//Wait for Eclipse to launch
		autoItInstace.sleep(15000); 
	}

	private void configWorkspace() {
		File prefFile = new File(eclipsePath + "\\configuration\\.settings\\org.eclipse.ui.ide.prefs");
		
		if(!prefFile.exists()){
			try {
				prefFile.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();

			}
		}
		
		String fileContent="";
		try {
			fileContent= new Scanner(prefFile).useDelimiter("\\A").next();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		if(fileContent.contains("3ButtonsEclipseWorkspace"))
			return;
		
		String workspace = System.getenv("userprofile").replace("\\", "\\\\").replace("C:", "C\\:") + "\\\\3ButtonsEclipseWorkspace"+ "\\n";
		fileContent = fileContent.replace("MAX_RECENT_WORKSPACES=", "max=");
		
		fileContent = fileContent.replace("RECENT_WORKSPACES=", "RECENT_WORKSPACES=" + workspace );
		fileContent = fileContent.replace("SHOW_WORKSPACE_SELECTION_DIALOG=true", "SHOW_WORKSPACE_SELECTION_DIALOG=false");

		if(!fileContent.contains("RECENT_WORKSPACES=")){
			fileContent += "RECENT_WORKSPACES=" + workspace + "\n";
			fileContent += "SHOW_WORKSPACE_SELECTION_DIALOG=false\n";
		}
		
		fileContent = fileContent.replace("max=", "MAX_RECENT_WORKSPACES=");

		
		try {
			Files.write(fileContent.getBytes(), prefFile.getAbsoluteFile());
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println(fileContent);
	}
	
	
	/**
	 * Sends keystrokes to the active window.
	 * @param keysLabels
	 */
	private void sendNoFocus(String... keysLabels) {
		String toSend = "";
		
		for (String key : keysLabels) {
			
			if (!isKeyALetter(key)) {
				key = parse(key.toUpperCase());
			}
			
			toSend += key;
		}
		
		System.out.println(toSend);
		
		autoItInstace.send(toSend, false);
	}

	/**
	 * Parse special characters to Autoit style chars.
	 * @param str name of special character.
	 * @return Autoit style string
	 */
	private String parse(String str) {
		System.out.println("Before parse:" + str);
		switch (str) {
		case "SHIFT":
			return "+";
		case "ALT":
			return "!";
		case "CTRL":
			return "^";
		case "WINKEY":
			return "#";
		}
		return "{" + str + "}";
	}

	/**
	 * @param str the string to check
	 * @return if str is a letter
	 */
	private boolean isKeyALetter(String str) {
		return (str.length() == 1 && Character.isLetterOrDigit(str.charAt(0)));
	}
	
	/**
	 * Set window focus on the window with a title containing <b>EclipseController.Title</b> 
	 */
	private void setFocus() {
		long beginFocus = 0;
		if (MEASURE_TIME) {
			beginFocus = System.nanoTime();
		}
		String currTitle = EclipseController.Title;
		if (currTitle != "") {
			autoItInstace.winActivate(currTitle);
			autoItInstace.winWaitActive(currTitle);
			
		}

		if (MEASURE_TIME) {
			double seconds = (double) (System.nanoTime() - beginFocus) / 1000000000.0;
			System.out.println("focus took: " + seconds);
		}

	}
	
	
	
}
