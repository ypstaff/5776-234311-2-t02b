package yearlyproject.programming.controlEngine;

import java.util.function.Consumer;

/**
 * A key sender in a window.
 * Allows window operations.
 * @author lmaman
 *
 */
public interface WindowedKeySender extends KeySender {
	/**
	 * Resize Sender window. 
	 * It's size will be (numOfTakenSegments/numOfAllSections)*(screen size).
	 * @param numOfAllSections num of sections to divide the screen to
	 * @param numOfTakenSegments num of segments to give the sender
	 * @param gravityBottom set sender gravity to bottom
	 */
	public void resize(int numOfAllSections,int numOfTakenSegments, boolean gravityBottom);
	
	/**
	 * Resize Controller window. 
	 * It's size will be (numOfTakenSegments/numOfAllSections)*(screen size).
	 * @param numOfAllSections num of sections to divide the screen to
	 * @param numOfTakenSegments num of segments to give the sender
	 * @param gravityBottom set sender gravity to bottom
	 */
	public void resizeController(Integer numOfAllSections, Integer numOfTakenSegments, boolean gravityBottom);

	/**
	 * Check if a string is in or not in the window title, and do an <b>action</b> accordingly.
	 * 
	 * @param 	inTitle string to check
	 * @param 	contain condition to check.
	 * 			True - if <b>inTitle</b> not in window title, execute <b>action</b>.
	 * 			False - if <b>inTitle</b> in window title, execute <b>action</b>.
	 * @param action action to do if condition is not met
	 */
	public void checkTitleAndDo(String inTitle, Boolean contain, Consumer<Void> action);
	
	/**
	 * Restart Window controller
	 */
	public void restart();
	
	public void changeToVertical();
	
	/**
	 * Change orientation of the controller and sender to vertical.
	 * Used mostly for menus and wizards.
	 * @param thinMenusMode
	 */
	public void changeToVertical(boolean thinMenusMode);

	/**
	 * Change orientation of the controller and sender to back to horizontal.
	 */
	public void backToHorozotial();
	
	
}
