package yearlyproject.programming.controlEngine;

/**
 * Send key strokes ,for example send alt+shift+r, to eclipse.
 * 
 * @author lmaman
 *
 */
public interface KeySender {
	/**
	 * Send keystrokes at the same time, like the user pressed keys
	 * simultaneously.
	 * 
	 * @param keyLabels
	 *            keys to send.
	 */
	public void send(String... keysLabels);

	/**
	 * Views Enum.
	 */
	public enum VIEW {
		BREAKPOINTS, OUTLINE, PACKAGES_EXPLORER, PROBLEMS, SEARCH, EDITOR, CONSOLE, VARIABLES
	};

	/**
	 * Open a View.
	 * 
	 * @param w
	 *            View to open
	 */
	public void openView(VIEW w);

	/**
	 * Controls Enum.
	 * @author lmaman
	 *
	 */
	public enum CONTROL_KEY {
		CTRL, ALT, SHIFT
	}

	/**
	 * Hold a key until releaseControl is called. Only one key can be held at
	 * a time.
	 * 
	 * @param key
	 *            the key to hold, from CONTROL_KEY enum.
	 */
	public void holdControl(CONTROL_KEY key);

	/**
	 * Release a key previously held by holdControl.
	 * 
	 * @param key
	 *            the key to release, from CONTROL_KEY enum.
	 */
	public void releaseControl(CONTROL_KEY key);
	
	/**
	 * Opens template assist.
	 */
	public void openTemplateAssist();
}
