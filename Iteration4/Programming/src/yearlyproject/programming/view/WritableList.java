package yearlyproject.programming.view;

import javafx.scene.control.Button;

import org.apache.log4j.Logger;

import yearlyproject.programming.common.MenuButtons;
import yearlyproject.programming.controlEngine.KeySender;
import yearlyproject.programming.controlEngine.KeySender.VIEW;

import com.google.inject.Inject;

/**
 * handles problems mode both gui and actions
 * 
 * @author kfirlan
 *
 */
public class WritableList extends MenuButtons {
	private static final String ENTER_LABEL = "Enter";
	private static final String UP_LABEL = "Up";
	private static final String DOWN_LABEL = "Down";
	private static final String KEYBOARD_LABEL = "Keyboard";
	public static final String EDITOR_LIST = "Editors List";
	public static final String CONSOLE_MENU = "Console View";
	private static final Logger LOGGER = Logger.getLogger(ProblemGui.class);
	private final Button[] subMenu = {new Button(KEYBOARD_LABEL), new Button(DOWN_LABEL), new Button(UP_LABEL), 
			new Button(ENTER_LABEL)};
	private final Button[] mainButtons = { new Button(EDITOR_LIST),new Button(CONSOLE_MENU)};
	
	@Inject
	private final KeySender sender;

	public WritableList(KeySender sender) {
		this.sender = sender;
		setMainMenuActions();
		setOutlineSubMenu();
		applyStyle();
	}

	@Override
	public Button[] getSubMenu() {
		return subMenu;
	}

	@Override
	public Button[] getMenuButtons() {
		return mainButtons;
	}

	private void setMainMenuActions() {
		applyEventHandler(mainButtons, EDITOR_LIST, (e)->{
			sender.send("ctrl","e");
			LOGGER.debug("editors list opened");
			onMenuAction.accept(null);
		});
		applyEventHandler(mainButtons, CONSOLE_MENU, (e)->{
			sender.openView(VIEW.CONSOLE);
			LOGGER.debug("console opened");
			onMenuAction.accept(null);
		});
	}

	private void setOutlineSubMenu() {
		applyEventHandler(subMenu, DOWN_LABEL, (e) -> sender.send(DOWN_LABEL));
		applyEventHandler(subMenu, UP_LABEL, (e) -> sender.send(UP_LABEL));
		applyEventHandler(subMenu, ENTER_LABEL, (e) -> {sender.send(ENTER_LABEL);});
		applyEventHandler(subMenu, KEYBOARD_LABEL, (e)->keyboards.regular());
	}

	@Override
	protected void applyStyle() {
		String css_style = "-fx-border-color: purple; -fx-border-width: 3; "+ MenuButtons.FONT;
		for (Button button : subMenu)
			button.setStyle(css_style);
		for (Button button : mainButtons)
			button.setStyle(css_style);

	}

}
