package yearlyproject.programming.view;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Labeled;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

import org.apache.log4j.Logger;

import yearlyproject.programming.common.GuiSegment;
import yearlyproject.programming.common.LongLabelsHBox;
import yearlyproject.programming.common.MenuButtons;
import yearlyproject.programming.common.ProgrammingController;
import yearlyproject.programming.common.ProgrammingController.NAV_BUTTONS;
import yearlyproject.programming.common.RootedObject;
import yearlyproject.programming.controlEngine.WindowedKeySender;
import yearlyproject.programming.keyboard.OpenKeyboards;
import yearlyproject.programming.tutorial.TutorialControlHBox;
import Navigation.IterableNodeGroup;
import Navigation.IterableNodeGroups;
import Navigation.IterationControlHBox;

import com.google.inject.Inject;

/**
 * the main gui of the view mode organize all the menu and their sub menus
 * 
 * @author kfirlan
 *
 */
public class ViewGui implements RootedObject {
	private final MenuButtons[] menus;
	public static final int hbox_gap = 1;
	private final IterableNodeGroups iterableNodeGroups;
	private IterationControlHBox iterControl;
	private List<HBox> subMenuesHbox = new ArrayList<>();
	private final List<IterableNodeGroup> lines = new ArrayList<>();
	private Map<NAV_BUTTONS, Consumer<Void>> navHandlers;
	private EventHandler<MouseEvent> optionsHandler;
	private Consumer<Void> onTut;
	private WindowedKeySender sender;
	private LongLabelsHBox longLablesHBox;
	private static final Logger LOGGER = Logger.getLogger(ViewGui.class);
	
	@Inject
	public ViewGui(final WindowedKeySender sender){
		this(sender, null, null, null);
	}

	@Inject
	public ViewGui(final WindowedKeySender sender, Map<NAV_BUTTONS, Consumer<Void>> navHandlers, EventHandler<MouseEvent> optionsHandler, Consumer<Void> onTut) {
		MenuButtons[] tmp = { new OutlineGui(sender), new WritableList(sender), new PackageExplorerGui(sender),
				new WizardGui(sender), new ProblemGui(sender), new MenuBarGui(sender) };
		this.menus = tmp;
		this.navHandlers = navHandlers;
		this.optionsHandler = optionsHandler;
		this.onTut = onTut;
		this.sender = sender;
		VBox vbox = new VBox(3);
		List<HBox> hboxes = new ArrayList<>();
		List<Node> firstLineList = new ArrayList<>();
		HBox firstLine = new HBox(hbox_gap);
		hboxes.add(firstLine);
		for (MenuButtons menuButtons : menus) {
			firstLineList.addAll(Arrays.asList(menuButtons.getMenuButtons()));
			hboxes.add(createLine(menuButtons.getSubMenu()));
		}
		reOrder(firstLineList);
		firstLine.getChildren().addAll(firstLineList);
		subMenuesHbox = hboxes.subList(1, hboxes.size());
		setSubMenuesVisibility(false);
		vbox.getChildren().addAll(hboxes);
		iterableNodeGroups = buildViewRoot(vbox);
	}

	public void setSubMenuesVisibility(Boolean v) {
		for (HBox hBox : subMenuesHbox)
			hBox.setVisible(v);
	}

	private HBox createLine(final Button[] navigate) {
		HBox $ = new HBox(hbox_gap);
		$.getChildren().addAll(navigate);
		return $;
	}

	/**
	 * create the pane and iterable groups of the gui and return it
	 * @return root iterable groups of the view part
	 */
	private IterableNodeGroups buildViewRoot(final VBox b){
		LOGGER.debug("Build view root was called");
		BorderPane bp = new BorderPane();
		bp.setLeft(b);
		final IterableNodeGroups $ = new IterableNodeGroups(bp);
		iterControl = new IterationControlHBox($);
		if (navHandlers != null){
			//Set special navigation handlers
			iterControl.setOnBackLongClick(navHandlers.get(NAV_BUTTONS.LONG_BACK));
			iterControl.setOnSelectLongClick(navHandlers.get(NAV_BUTTONS.LONG_SELECT));
			iterControl.setOnNextLongClick(navHandlers.get(NAV_BUTTONS.LONG_NEXT));
		}
		if(optionsHandler != null){
			iterControl.setOptionsOnClickHandler(optionsHandler);
		}
		
		if(onTut != null){
			// integrating the keyboard with the Tutorial
			TutorialControlHBox tutorialHBox = new TutorialControlHBox($, onTut, bp);
			tutorialHBox.setTutorialButtons();
		}
		
		VBox controlBox = new VBox();
		longLablesHBox = new LongLabelsHBox(iterControl);
		controlBox.getChildren().addAll(longLablesHBox, iterControl);
		bp.setBottom(controlBox);
		
		VBox rightVbox = new VBox(3);
		Label winLabel = new Label("View");
		winLabel.setStyle("-fx-font-size: 28;");
		rightVbox.getChildren().add(winLabel);
		
		if (ProgrammingController.BTConnected) {
			GuiSegment.setBTIndicator(rightVbox, true);
		}
		else{
			GuiSegment.setBTIndicator(rightVbox, false);
		}
		
		rightVbox.setAlignment(Pos.TOP_RIGHT);
		bp.setCenter(rightVbox);
		
		boolean first = true;
		for (Node hbox : b.getChildrenUnmodifiable()){
			if(first){
				first = false;
				List<Node> list = ((HBox) hbox).getChildren();
				lines.add($.addChildGroup(list.subList(0, list.size()/2)));
				lines.add($.addChildGroup(list.subList(list.size()/2,list.size())));
				continue;
			}
			lines.add($.addChildGroup(((HBox) hbox).getChildren()));
		}
		for (int i=0; i< menus.length; ++i){
			$.setEnabledMode(lines.get(i+2), false);
			//these lines enter the selection of the iterable group to the button
			final int tmp = i;
			menus[i].setMenuSwitch((x)->focusSubMenu(tmp));
		}
		focusWizardInject();
		return $;
	}
	
	@Override
	public IterableNodeGroups getIterableRoot(){
		return iterableNodeGroups;
	}
	
	/**
	 * 
	 * @return iteration control HBox of this gui
	 */
	public IterationControlHBox getIterControl() {
		return iterControl;
	}
	//changing order of list elements by labels
	private void putAfter(List<Node> ns,String after,String label){
		int afterI=-1,labelI=-1;
		for (int k=0; k < ns.size() ; ++k) {
			if (after.equals(((Labeled) ns.get(k)).getText()))
				afterI=k;
			if (label.equals(((Labeled) ns.get(k)).getText()))
				labelI=k;
			if (afterI >= 0 && labelI>=0)
				break;
		}
		if (afterI < 0 || labelI < 0)
			return;
		Node node = ns.get(labelI);
		ns.remove(labelI);
		ns.add(afterI, node);
		
	}
	
	private void reOrder(List<Node> ns){
		putAfter(ns, ProblemGui.PROBLEMS_MENU, WritableList.CONSOLE_MENU);
	}
	/**
	 * shift the iterableNodeGroups focus to the wizard submenu
	 */
	public void focusWizard(){
		focusSubMenu(WizardGui.class);
		sender.changeToVertical();
	}

	/**
	 * shift the iterableNodeGroups focus to the down up enter submenu 
	 */
	public void focusUpDownEnter(){
		focusSubMenu(ProblemGui.class);
	}
	
	private void focusSubMenu(Class<?> c){
		int i=0;
		boolean flag = false;
		for (;i<menus.length;++i)
			if (c.isInstance(menus[i])){
				flag = true;
				break;
			}
		if (flag)
			focusSubMenu(i);
			
	}

	private void focusSubMenu(int i) {
		iterableNodeGroups.setOnLastBackConsumer((x)->setSubMenuesVisibility(false));
		iterableNodeGroups.unselect();
		iterableNodeGroups.selectSpecific(lines.get(i+2));
		iterableNodeGroups.select();
		iterableNodeGroups.setOnLastBackConsumer((x)->focusMainMenu());
		subMenuesHbox.get(i).setVisible(true);
	}
	
	private void focusMainMenu(){
		sender.send("esc");
		sender.send("esc"); //remove focus from whatever you were doing (2 times on purpose)
		sender.backToHorozotial();
		iterableNodeGroups.setOnLastBackConsumer((x)->{});
		iterableNodeGroups.next();
		lines.get(0).unselectAll();//remove redundant marking
		setSubMenuesVisibility(false);
	}
	
	public void setOpenKDB(OpenKeyboards k){
		for (MenuButtons menuButtons : menus)
			menuButtons.setOpenKeyboard(k);
	}

	public LongLabelsHBox getLongLablesHBox() {
		return longLablesHBox;
	}

	public void setLongLablesHBox(LongLabelsHBox longLablesHBox) {
		this.longLablesHBox = longLablesHBox;
	}
	
	private void focusWizardInject(){
		for (MenuButtons b : menus)
			if (b instanceof FocusWizrad)
				((FocusWizrad) b).focusWizard((x) -> {
					focusWizard();
				});
	}
	
}
