package yearlyproject.programming.view;

import javafx.scene.control.Button;

import org.apache.log4j.Logger;

import yearlyproject.programming.common.MenuButtons;
import yearlyproject.programming.controlEngine.WindowedKeySender;

import com.google.inject.Inject;

public class WizardGui extends MenuButtons {
	private static final String ENTER_LABEL = "Enter";
	private static final String UP_LABEL = "Up";
	public static final String WIZARD = "Wizard";
	private static final String KEYBOARD_LABEL = "Keyboard";
	private static final String SPACE_LABEL = "Space";
	private static final String DOWN_LABEL = "Down";
	private static final String SHIFT_TAB_LABEL = "Shift Tab";
	private static final String TAB_LABEL = "Tab";
	private static final String COLLAPSE_LABEL = "Collapse";
	private static final String EXPEND_LABEL = "Expend";
	private static final String PREFRENCES = "Prefrences";
	private static final Logger LOGGER = Logger.getLogger(WizardGui.class);
	private static final String PASTE_LABEL = "Paste";
	private final Button[] subMenu = { new Button(TAB_LABEL), 
			new Button(SHIFT_TAB_LABEL), new Button(DOWN_LABEL),new Button(ENTER_LABEL),new Button(EXPEND_LABEL), 
			new Button(UP_LABEL),new Button(SPACE_LABEL), 
			new Button(PASTE_LABEL),new Button(KEYBOARD_LABEL), new Button(COLLAPSE_LABEL)};
	private final Button[] mainButtons = { new Button(WIZARD),new Button(PREFRENCES) };

	@Inject
	private final WindowedKeySender sender;

	public WizardGui(WindowedKeySender sender) {
		this.sender = sender;
		setMainMenuActions();
		setOutlineSubMenu();
		applyStyle();
	}

	@Override
	public Button[] getSubMenu() {
		return subMenu;
	}

	@Override
	public Button[] getMenuButtons() {
		return mainButtons;
	}

	private void setMainMenuActions() {
		applyEventHandler(mainButtons, WIZARD, (e) -> {
			LOGGER.debug("wizard mode opened");
			sender.changeToVertical();
			onMenuAction.accept(null);
		});
		applyEventHandler(mainButtons, PREFRENCES, (e) -> {
			sender.changeToVertical();
			sender.send("alt","w");
			sender.send("up");
			sender.send("enter");
			LOGGER.debug("wizard mode opened");
			onMenuAction.accept(null);
		});
	}

	private void setOutlineSubMenu() {
		applyEventHandler(subMenu, DOWN_LABEL, (e) -> sender.send(DOWN_LABEL));
		applyEventHandler(subMenu, UP_LABEL, (e) -> sender.send(UP_LABEL));
		applyEventHandler(subMenu, SPACE_LABEL, (e) -> sender.send(SPACE_LABEL));
		applyEventHandler(subMenu, TAB_LABEL, (e) -> sender.send(TAB_LABEL));
		applyEventHandler(subMenu, SHIFT_TAB_LABEL, (e) -> sender.send("shift", TAB_LABEL));
		applyEventHandler(subMenu, COLLAPSE_LABEL, (e)->sender.send("shift","left"));
		applyEventHandler(subMenu, EXPEND_LABEL, (e)->sender.send("shift","right"));
		applyEventHandler(subMenu, KEYBOARD_LABEL, (e) ->keyboards.regular());
		applyEventHandler(subMenu, PASTE_LABEL, (e)->sender.send("ctrl","v"));
		applyEventHandler(subMenu, ENTER_LABEL, (e)->sender.send(ENTER_LABEL));
	}

	@Override
	protected void applyStyle() {
		String css_style = "-fx-border-color: green; -fx-border-width: 3 ;" + MenuButtons.FONT;
		for (Button button : subMenu)
			button.setStyle(css_style);
		for (Button button : mainButtons)
			button.setStyle(css_style);

	}

}
