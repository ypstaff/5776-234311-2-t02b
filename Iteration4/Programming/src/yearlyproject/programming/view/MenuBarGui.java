package yearlyproject.programming.view;

import javafx.scene.control.Button;

import java.util.function.Consumer;

import org.apache.log4j.Logger;

import yearlyproject.programming.common.MenuButtons;
import yearlyproject.programming.controlEngine.WindowedKeySender;

import com.google.inject.Inject;

/**
 * handles menubar mode
 * 
 * @author kfirlan
 *
 */
public class MenuBarGui extends MenuButtons implements FocusWizrad{
	private static final String MENU_BAR = "Menu Bar";
	private static final String LEFT_LABEL = "Left";
	private static final String RIGHT_LABEL = "Right";
	private static final String ENTER_LABEL = "Enter";
	private static final String UP_LABEL = "Up";
	private static final String DOWN_LABEL = "Down";
	private static final Logger LOGGER = Logger.getLogger(MenuBarGui.class);
	private final Button[] subMenu = { new Button(DOWN_LABEL), new Button(UP_LABEL), new Button(ENTER_LABEL)
			,new Button(RIGHT_LABEL),new Button(LEFT_LABEL) };
	private final Button[] mainButtons = {new Button(MENU_BAR)};
	
	@Inject
	private final WindowedKeySender sender;
	private Consumer<Void> focusWizard;
	public MenuBarGui(WindowedKeySender sender) {
		this.sender = sender;
		setMainMenuActions();
		setOutlineSubMenu();
		applyStyle();
	}

	@Override
	public Button[] getSubMenu() {
		return subMenu;
	}

	@Override
	public Button[] getMenuButtons() {
		return mainButtons;
	}

	private void setMainMenuActions() {
		applyEventHandler(mainButtons, MENU_BAR, (e)->{
			sender.changeToVertical();
			sender.send("alt","f");
			LOGGER.debug("menu bar opened");
			onMenuAction.accept(null);
		});
	}

	private void setOutlineSubMenu() {
		applyEventHandler(subMenu, DOWN_LABEL, (e) -> sender.send(DOWN_LABEL));
		applyEventHandler(subMenu, UP_LABEL, (e) -> sender.send(UP_LABEL));
		applyEventHandler(subMenu, ENTER_LABEL, (e) -> {
			sender.send(ENTER_LABEL);
			if (focusWizard != null)
				focusWizard.accept(null);
		});
		applyEventHandler(subMenu, RIGHT_LABEL, (e) -> sender.send(RIGHT_LABEL));
		applyEventHandler(subMenu, LEFT_LABEL, (e) -> sender.send(LEFT_LABEL));
	}

	@Override
	protected void applyStyle() {
		String css_style = "-fx-border-color: red; -fx-border-width: 3; "+ MenuButtons.FONT;
		for (Button button : subMenu)
			button.setStyle(css_style);
		for (Button button : mainButtons)
			button.setStyle(css_style);

	}

	@Override
	public void focusWizard(Consumer<Void> shiftFocus) {
		this.focusWizard = shiftFocus;		
	}

}
