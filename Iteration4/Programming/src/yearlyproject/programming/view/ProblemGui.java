package yearlyproject.programming.view;

import javafx.scene.control.Button;

import java.util.function.Consumer;

import org.apache.log4j.Logger;

import yearlyproject.programming.common.MenuButtons;
import yearlyproject.programming.controlEngine.WindowedKeySender;
import yearlyproject.programming.controlEngine.KeySender.VIEW;

import com.google.inject.Inject;

/**
 * handles problems mode both gui and actions
 * 
 * @author kfirlan
 *
 */
public class ProblemGui extends MenuButtons implements FocusWizrad{
	private static final String QUICK_FIX_LABEL = "Quick Fix";
	public static final String PROBLEMS_MENU = "Problems View";
	private static final String ENTER_LABEL = "Enter";
	private static final String UP_LABEL = "Up";
	private static final String DOWN_LABEL = "Down";
	private static final Logger LOGGER = Logger.getLogger(ProblemGui.class);
	private final Button[] subMenu = { new Button(DOWN_LABEL), new Button(UP_LABEL), new Button(ENTER_LABEL)
			,new Button(QUICK_FIX_LABEL)};
	private final Button[] mainButtons = {new Button(PROBLEMS_MENU)};
	
	@Inject
	private final WindowedKeySender sender;
	private Consumer<Void> focusWizard;
	public ProblemGui(WindowedKeySender sender) {
		this.sender = sender;
		setMainMenuActions();
		setOutlineSubMenu();
		applyStyle();
	}

	@Override
	public Button[] getSubMenu() {
		return subMenu;
	}

	@Override
	public Button[] getMenuButtons() {
		return mainButtons;
	}

	private void setMainMenuActions() {
		applyEventHandler(mainButtons, PROBLEMS_MENU, (e) -> {
			sender.openView(VIEW.PROBLEMS);
			LOGGER.debug("problem opened");
			onMenuAction.accept(null);
		});
	}

	private void setOutlineSubMenu() {
		applyEventHandler(subMenu, DOWN_LABEL, (e) -> sender.send(DOWN_LABEL));
		applyEventHandler(subMenu, UP_LABEL, (e) -> sender.send(UP_LABEL));
		applyEventHandler(subMenu, ENTER_LABEL, (e) -> {sender.send(ENTER_LABEL);});
		applyEventHandler(subMenu, QUICK_FIX_LABEL, (e)->{
			focusWizard.accept(null); //change to vertical
			sender.send("ctrl","1"); //open wizard after change to vertical
		});
	}

	@Override
	protected void applyStyle() {
		String css_style = "-fx-border-color: yellow; -fx-border-width: 3; "+ MenuButtons.FONT;
		for (Button button : subMenu)
			button.setStyle(css_style);
		for (Button button : mainButtons)
			button.setStyle(css_style);

	}

	@Override
	public void focusWizard(Consumer<Void> shiftFocus) {
		this.focusWizard = shiftFocus;		
	}

}
