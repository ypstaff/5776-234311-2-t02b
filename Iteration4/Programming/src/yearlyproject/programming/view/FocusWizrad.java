package yearlyproject.programming.view;

import java.util.function.Consumer;

public interface FocusWizrad {
	public void focusWizard(Consumer<Void> shiftFocus);
}
