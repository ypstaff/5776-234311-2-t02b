package yearlyproject.programming.view;

import javafx.scene.control.Button;

import org.apache.log4j.Logger;

import yearlyproject.programming.common.MenuButtons;
import yearlyproject.programming.controlEngine.WindowedKeySender;
import yearlyproject.programming.controlEngine.KeySender.VIEW;

import com.google.inject.Inject;

/**
 * this class handle the buttons of the outline mode. both colors and actions.
 * 
 * @author kfirlan
 *
 */
public class OutlineGui extends MenuButtons {
	private static final String OUTLINE_LABEL = "Outline";
	private static final String COLLAPSE_ALL_LABEL = "Collapse All";
	private static final String COLLAPSE_LABEL = "Collapse";
	private static final String EXPEND_LABEL = "Expand";
	private static final String UP_LABEL = "Up";
	private static final String DOWN_LABEL = "Down";
	private static final Logger LOGGER = Logger.getLogger(OutlineGui.class);
	private final Button[] subMenu = { new Button(DOWN_LABEL), new Button(UP_LABEL), new Button(EXPEND_LABEL),
			new Button(COLLAPSE_LABEL), new Button(COLLAPSE_ALL_LABEL) };
	private final Button[] mainButtons = { new Button(OUTLINE_LABEL) };
	@Inject
	private final WindowedKeySender sender;

	public OutlineGui(WindowedKeySender sender) {
		this.sender = sender;
		setMainMenuActions();
		setOutlineSubMenu();
		applyStyle();
	}

	@Override
	public Button[] getSubMenu() {
		return subMenu;
	}

	@Override
	public Button[] getMenuButtons() {
		return mainButtons;
	}

	private void setMainMenuActions() {
		applyEventHandler(mainButtons, OUTLINE_LABEL, (e) -> {
			sender.openView(VIEW.OUTLINE);
			LOGGER.debug("open outline");
			onMenuAction.accept(null);
		});
	}

	private void setOutlineSubMenu() {
		applyEventHandler(subMenu, DOWN_LABEL, (e) -> sender.send(DOWN_LABEL));
		applyEventHandler(subMenu, UP_LABEL, (e) -> sender.send(UP_LABEL));
		applyEventHandler(subMenu, EXPEND_LABEL, (e) -> sender.send("shift", "right"));
		applyEventHandler(subMenu, COLLAPSE_LABEL, (e) -> sender.send("shift", "left"));
		applyEventHandler(subMenu, COLLAPSE_ALL_LABEL, (e) -> sender.send("shift", "ctrl", "NUMPADDIV"));
	}

	@Override
	protected void applyStyle() {
		String css_style = "-fx-border-color: navy; -fx-border-width: 3; "+ MenuButtons.FONT;
		for (Button button : subMenu)
			button.setStyle(css_style);
		for (Button button : mainButtons)
			button.setStyle(css_style);
	}
}
