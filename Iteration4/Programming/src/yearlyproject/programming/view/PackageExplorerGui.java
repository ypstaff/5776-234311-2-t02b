package yearlyproject.programming.view;

import javafx.scene.control.Button;

import java.util.function.Consumer;

import org.apache.log4j.Logger;

import yearlyproject.programming.common.MenuButtons;
import yearlyproject.programming.controlEngine.WindowedKeySender;
import yearlyproject.programming.controlEngine.KeySender.VIEW;

import com.google.inject.Inject;

/**
 * handles package explorer mode both gui and actions
 * 
 * @author kfirlan
 *
 */
public class PackageExplorerGui extends MenuButtons implements FocusWizrad {
	public static final String PACKAGE_EXPLORER = "Package Explorer";
	private static final String ESC_LABEL = "Esc";
	private static final String NEW_LABEL = "New";
	private static final String COLLAPSE_ALL_LABEL = "Collapse All";
	private static final String ENTER_LABEL = "Enter";
	private static final String UP_LABEL = "Up";
	private static final String DOWN_LABEL = "Down";
	private static final Logger LOGGER = Logger.getLogger(PackageExplorerGui.class);
	private final Button[] subMenu = { new Button(DOWN_LABEL), new Button(UP_LABEL), new Button(ENTER_LABEL),
			new Button(COLLAPSE_ALL_LABEL), new Button(NEW_LABEL), new Button(ESC_LABEL) };
	private final Button[] mainButtons = { new Button(PACKAGE_EXPLORER) };
	@Inject
	private final WindowedKeySender sender;
	private Boolean focus = false;
	private Consumer<Void> focusWizard = null;

	public PackageExplorerGui(WindowedKeySender sender) {
		this.sender = sender;
		setMainMenuActions();
		setOutlineSubMenu();
		applyStyle();
	}

	@Override
	public Button[] getSubMenu() {
		return subMenu;
	}

	@Override
	public Button[] getMenuButtons() {
		return mainButtons;
	}

	private void setMainMenuActions() {
		applyEventHandler(mainButtons, PACKAGE_EXPLORER, (e) -> {
			sender.changeToVertical();
			sender.openView(VIEW.PACKAGES_EXPLORER);
			LOGGER.debug("package explorer opened");
			onMenuAction.accept(null);
		});
	}

	private void setOutlineSubMenu() {
		applyEventHandler(subMenu, DOWN_LABEL, (e) -> sender.send(DOWN_LABEL));
		applyEventHandler(subMenu, UP_LABEL, (e) -> sender.send(UP_LABEL));
		applyEventHandler(subMenu, COLLAPSE_ALL_LABEL, (e) ->sender.send("shift", "ctrl", "NUMPADDIV"));
		applyEventHandler(subMenu, ENTER_LABEL, (e) -> {
			sender.send(ENTER_LABEL);
			if (focus && focusWizard != null){
				focusWizard.accept(null);
			}
			focus = false;
		});
		applyEventHandler(subMenu, NEW_LABEL, (e) -> {
			sender.send("alt", "shift", "n");
			focus=true;
		});
		applyEventHandler(subMenu, ESC_LABEL, (e) -> {
			sender.send(ESC_LABEL);
			focus = false;
		});
	}
	@Override
	protected void applyStyle() {
		String css_style = "-fx-border-color: darkorange; -fx-border-width: 3; "+ MenuButtons.FONT;
		for (Button button : subMenu)
			button.setStyle(css_style);
		for (Button button : mainButtons)
			button.setStyle(css_style);

	}

	@Override
	public void focusWizard(Consumer<Void> shiftFocus) {
		this.focusWizard=shiftFocus;
	}

}
