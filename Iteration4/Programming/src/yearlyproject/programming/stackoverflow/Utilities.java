package yearlyproject.programming.stackoverflow;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Utilities class with parsing html source and extracting
 * code snippets from stackoverflow.com webpages capabilities
 * 
 * @author Lior Volin
 *
 */
public class Utilities {

	private static final String REGEX = "<pre><code>(.*?)</code></pre>";
	private static final String USER_AGENT_VALUE = "Mozilla/5.0";
	private static final String USER_AGENT_KEY = "User-Agent";

	private static StringBuilder getUrlSource(String adress) throws IOException {
		URL url = new URL(adress);
		URLConnection urlConnection = url.openConnection();
		// Pretending to a browser in order to prevent being blocked
		urlConnection.addRequestProperty(USER_AGENT_KEY, USER_AGENT_VALUE);
		urlConnection.connect();
		
		BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
		StringBuilder $ = new StringBuilder();
		String inputLine;
		
		while ((inputLine = bufferedReader.readLine()) != null) {
			$.append(inputLine);
			$.append('\n');
		}
		bufferedReader.close();

		return $;
	}
	
	protected static List<String> getCodeSnippetsFromSource(String source) {
		// RegEx to parse the html source of a page and find 
		// all the code snippets in a stackoverflow webpage
		Pattern pattern = Pattern.compile(REGEX, Pattern.DOTALL);
		Matcher matcher = pattern.matcher(source);
		
		List<String> $ = new ArrayList<String>();
		while (matcher.find()) 
			$.add(matcher.group(1));

		return $;
	}
	
	// Fixing the snippets strings that came from html source
	private static List<String> fixSnippetsStrings(List<String> snippets) {
		List<String> $ = new ArrayList<>();
		String newString;
		String newString2;
		for (String s : snippets) {
			newString = s.replace("&lt;", "<");
			newString2 = newString.replace("&gt;", ">");
			$.add(newString2);
		}
		
		return $;
	}

	protected static List<String> extractCodeSnippets(String adress) {
		String source = null;
		try {
			source = getUrlSource(adress).toString();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return fixSnippetsStrings(getCodeSnippetsFromSource(source));
	}
}
