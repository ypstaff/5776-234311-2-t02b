package yearlyproject.programming.stackoverflow;

import java.util.List;
import java.util.function.Consumer;

import yearlyproject.programming.common.GuiSegment;
import yearlyproject.programming.common.MenuButtons;
import yearlyproject.programming.controlEngine.KeySender;

import com.google.inject.Inject;

/**
 * A class that represents the copy code menus gui
 * 
 * @author Lior Volin
 *
 */
public class CopyCodeGui extends GuiSegment {

	private static final String WIN_TITLE = "";

	@Inject
	public CopyCodeGui(final KeySender sender) {
		this(null, sender, null, null, null);
	}

	@Inject
	public CopyCodeGui(List<String> codeSnippets, final KeySender sender, Consumer<Void> onFinish, Consumer<Void> returnToBrowserGui,
			Consumer<Void> onTut) {

		// a way around array initialization restrictions
		// All the menus in the copy code gui
		MenuButtons[] tmp = { 
				new ControlSnippetsGui(codeSnippets, onFinish),
				new SnippetsFontSizeGui(),
				new BackToBrowserGui(returnToBrowserGui),
				new ExitCopyGui(onFinish) };
		menus = tmp;

		// Empty because of the vertical
		// orientation of the snippets list screen
		winTitle = WIN_TITLE;

		applyStyles();

		//setOnTutorial(onTut);
		
		buildToVbox(false);
	}
}
