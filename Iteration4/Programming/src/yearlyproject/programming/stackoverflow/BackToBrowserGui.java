package yearlyproject.programming.stackoverflow;

import javafx.scene.control.Button;
import yearlyproject.programming.common.MenuButtons;
import java.util.function.Consumer;

/**
 * A class that represents the copy code's 
 * back to browser sub-menu gui
 * 
 * @author Lior Volin
 *
 */
public class BackToBrowserGui extends MenuButtons {

	public static final String BACK_LABEL = "Back To Browser";

	private Consumer<Void> returnToBrowserGui;

	public BackToBrowserGui(Consumer<Void> returnToBrowserGui) {
		this.returnToBrowserGui = returnToBrowserGui;

		subMenu = new Button[1];
		subMenu[0] = new Button(BACK_LABEL);

		setGuiSubMenuActions();
	}

	@Override
	public Button[] getSubMenu() {
		return subMenu;
	}

	@Override
	public Button[] getMenuButtons() {
		return null;
	}

	private void setGuiSubMenuActions() {
		applyEventHandler(subMenu, BACK_LABEL, e -> {
			// Showing the browser window
			Browser.primaryStage.setScene(Browser.browserScene);
			returnToBrowserGui.accept(null);
		});
	}
}
