package yearlyproject.programming.stackoverflow;

import javafx.scene.control.Button;
import yearlyproject.programming.common.MenuButtons;
import yearlyproject.programming.controlEngine.KeySender;

import com.google.inject.Inject;

/**
 * A class that represents the browser's page nav. sub-menu gui
 * 
 * @author Lior Volin
 *
 */
public class ArrowsGui extends MenuButtons {

	public static final String PAGE_DOWN_LABEL = "Page Down";
	public static final String PAGE_UP_LABEL = "Pageg Up";
	public static final String DOWN_LABEL = "Down";
	public static final String UP_LABEL = "Up";

	@Inject
	private final KeySender sender;

	public ArrowsGui(KeySender sender) {
		this.sender = sender;

		subMenu = new Button[4];
		subMenu[0] = new Button(PAGE_DOWN_LABEL);
		subMenu[1] = new Button(PAGE_UP_LABEL);
		subMenu[2] = new Button(DOWN_LABEL);
		subMenu[3] = new Button(UP_LABEL);

		setGuiSubMenuActions();
	}

	@Override
	public Button[] getSubMenu() {
		return subMenu;
	}

	@Override
	public Button[] getMenuButtons() {
		return null;
	}

	private void setGuiSubMenuActions() {
		applyEventHandler(subMenu, PAGE_DOWN_LABEL, e -> {
			sender.send("PgDn");
		});
		
		applyEventHandler(subMenu, PAGE_UP_LABEL, e -> {
			sender.send("PgUp");
		});
		
		applyEventHandler(subMenu, DOWN_LABEL, e -> {
			sender.send("down");
		});
		
		applyEventHandler(subMenu, UP_LABEL, e -> {
			sender.send("up");
		});
	}
}
