package yearlyproject.programming.stackoverflow;

import javafx.scene.control.Button;
import yearlyproject.programming.common.MenuButtons;
import yearlyproject.programming.controlEngine.KeySender;

import com.google.inject.Inject;

/**
 * A class that represents the browser's control sub-menu gui
 * 
 * @author Lior Volin
 *
 */
public class ControlGui extends MenuButtons {

	public static final String ENTER_LABEL = "Enter";
	public static final String BACK_LABEL = "Back";
	public static final String FORWARD_LABEL = "Forward";
	
	private static final int HISTORY_FORWARD_OFFSET = 1;
	private static final int HISTORY_BACK_OFFSET = -1;

	@Inject
	private final KeySender sender;

	public ControlGui(KeySender sender) {
		this.sender = sender;

		subMenu = new Button[3];
		subMenu[0] = new Button(ENTER_LABEL);
		subMenu[1] = new Button(BACK_LABEL);
		subMenu[2] = new Button(FORWARD_LABEL);

		setGuiSubMenuActions();
	}

	@Override
	public Button[] getSubMenu() {
		return subMenu;
	}

	@Override
	public Button[] getMenuButtons() {
		return null;
	}

	private void setGuiSubMenuActions() {
		applyEventHandler(subMenu, ENTER_LABEL, e -> {
			sender.send("enter");
		});
		
		applyEventHandler(subMenu, BACK_LABEL, e -> {
			try {
				Browser.myWebEngine.getHistory().go(HISTORY_BACK_OFFSET);
			} catch (IndexOutOfBoundsException exception) {
				// don't care - intentionally do nothing
			}
		});
		
		applyEventHandler(subMenu, FORWARD_LABEL, e -> {
			try {
				Browser.myWebEngine.getHistory().go(HISTORY_FORWARD_OFFSET);
			} catch (IndexOutOfBoundsException exception) {
				// don't care - intentionally do nothing
			}
		});
	}
}
