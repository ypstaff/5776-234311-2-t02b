package yearlyproject.programming.stackoverflow;

import javafx.application.Application;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.layout.StackPane;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import javafx.util.Pair;
import yearlyproject.programming.common.ProgrammingController;

/**
 * A class that represents the browser window.
 * It is implemented as a javafx application.
 * 
 * @author Lior Volin
 *
 */
public class Browser extends Application {

	private static final String STAGE_TITLE = "Browser";
	private static final String URL_TO_LOAD = "http://www.google.com/ncr";
	private static final String USER_AGENT = "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1";
	private static final int DEFAULT_HEIGHT = 480;
	private static final int DEFAULT_WIDTH = 640;

	protected static Stage primaryStage;
	protected static Scene browserScene;
	protected static WebEngine myWebEngine;

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) {
		Browser.primaryStage = primaryStage;
		primaryStage.setTitle(STAGE_TITLE);

		WebView myBrowser = new WebView();
		myWebEngine = myBrowser.getEngine();
		// Pretending to a newer browser in order to get the new google site
		// (for the accessibility benefit)
		myWebEngine.setUserAgent(USER_AGENT);
		// Using the 'no country redirect' to get the american site (for the
		// accessibility benefit)
		myWebEngine.load(URL_TO_LOAD);

		StackPane root = new StackPane();
		root.getChildren().add(myBrowser);

		browserScene = new Scene(root, DEFAULT_WIDTH, DEFAULT_HEIGHT);
		primaryStage.setScene(browserScene);
		resizeBrowser(ProgrammingController.mediumControllerSize);
		primaryStage.show();
	}

	// Resizing the browser window
	public static void resizeBrowser(Pair<Integer, Integer> size) {
		if (primaryStage == null)
			return;

		int numOfAllSections = size.getKey();
		setPrimaryStageSizes(
				ProgrammingController.getDimentsions(numOfAllSections, (numOfAllSections - size.getValue()), true));
	}

	public static void setPrimaryStageSizes(Rectangle2D d) {
		if (primaryStage == null)
			return;

		primaryStage.setWidth(d.getWidth());
		primaryStage.setHeight(d.getHeight());
		primaryStage.setX(d.getMinX());
		primaryStage.setY(d.getMinY());
	}
}