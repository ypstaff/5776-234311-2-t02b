package yearlyproject.programming.stackoverflow;

import javafx.scene.control.Button;
import yearlyproject.programming.common.MenuButtons;
import yearlyproject.programming.controlEngine.KeySender;

import com.google.inject.Inject;

/**
 * A class that represents the browser's links nav. sub-menu gui
 * 
 * @author Lior Volin
 *
 */
public class TabGui extends MenuButtons {

	public static final String NEXT_LABEL = "Next Link";
	public static final String PREVIOUS_LABEL = "Previous Link";
	public static final String ENTER_LABEL = "Enter Link";

	@Inject
	private final KeySender sender;

	public TabGui(KeySender sender) {
		this.sender = sender;

		subMenu = new Button[3];
		subMenu[0] = new Button(NEXT_LABEL);
		subMenu[1] = new Button(PREVIOUS_LABEL);
		subMenu[2] = new Button(ENTER_LABEL);

		setGuiSubMenuActions();
	}

	@Override
	public Button[] getSubMenu() {
		return subMenu;
	}

	@Override
	public Button[] getMenuButtons() {
		return null;
	}

	private void setGuiSubMenuActions() {
		applyEventHandler(subMenu, NEXT_LABEL, e -> {
			sender.send("tab");
		});
		
		applyEventHandler(subMenu, PREVIOUS_LABEL, e -> {
			sender.send("shift", "tab");
		});
		
		applyEventHandler(subMenu, ENTER_LABEL, e -> {
			sender.send("enter");
		});
	}
}
