package yearlyproject.programming.stackoverflow;

import javafx.scene.control.Button;
import yearlyproject.programming.common.MenuButtons;

/**
 * A class that represents the copy code's 
 * snippets font size sub-menu gui
 * 
 * @author Lior Volin
 *
 */
public class SnippetsFontSizeGui extends MenuButtons {

	public static final String UP_LABEL = "Font Up";
	public static final String DOWN_LABEL = "Font Down";
	
	private static final int FONT_DOWN_LIMIT = 5;

	public SnippetsFontSizeGui() {
		
		subMenu = new Button[2];
		subMenu[0] = new Button(UP_LABEL);
		subMenu[1] = new Button(DOWN_LABEL);

		setGuiSubMenuActions();
	}

	@Override
	public Button[] getSubMenu() {
		return subMenu;
	}

	@Override
	public Button[] getMenuButtons() {
		return null;
	}

	private void setGuiSubMenuActions() {
		applyEventHandler(subMenu, UP_LABEL, e -> {
			ControlSnippetsGui.fontSize = ControlSnippetsGui.setCodeSnippetsView(1);
		});
		
		applyEventHandler(subMenu, DOWN_LABEL, e -> {
			if (ControlSnippetsGui.fontSize > FONT_DOWN_LIMIT) 
				ControlSnippetsGui.fontSize = ControlSnippetsGui.setCodeSnippetsView(-1);
		});
	}
}
