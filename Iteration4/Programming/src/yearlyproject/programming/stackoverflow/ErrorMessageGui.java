package yearlyproject.programming.stackoverflow;

import java.util.function.Consumer;

import yearlyproject.programming.common.GuiSegment;
import yearlyproject.programming.common.MenuButtons;
import yearlyproject.programming.controlEngine.KeySender;

import com.google.inject.Inject;

/**
 * A class that represents the error messages menus gui
 * 
 * @author Lior Volin
 *
 */
public class ErrorMessageGui extends GuiSegment {

	private static final String WIN_TITLE = "Stack Overflow";

	@Inject
	public ErrorMessageGui(final KeySender sender) {
		this(sender, null, null, null);
	}

	@Inject
	public ErrorMessageGui(final KeySender sender, Consumer<Void> onFinish, Consumer<Void> returnToBrowserGui,
			Consumer<Void> onTut) {

		// a way around array initialization restrictions
		// All the menus in the error messages gui
		MenuButtons[] tmp = {
				new BackToBrowserGui(returnToBrowserGui),
				new ExitCopyGui(onFinish) };
		menus = tmp;

		winTitle = WIN_TITLE;

		applyStyles();

		//setOnTutorial(onTut);
		buildToVbox(false);
	}
}
