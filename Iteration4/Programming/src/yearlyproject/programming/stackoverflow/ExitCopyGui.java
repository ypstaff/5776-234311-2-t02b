package yearlyproject.programming.stackoverflow;

import javafx.scene.control.Button;
import yearlyproject.programming.common.MenuButtons;
import java.util.function.Consumer;

/**
 * A class that represents the copy code's 
 * exit sub-menu gui
 * 
 * @author Lior Volin
 *
 */
public class ExitCopyGui extends MenuButtons {
	
	public static final String EXIT_LABEL = "Exit Browser";
	
	private Consumer<Void> onFinish;
	
	public ExitCopyGui(Consumer<Void> onFinish) {
		this.onFinish = onFinish;
		
		subMenu = new Button[1];
		subMenu[0] = new Button(EXIT_LABEL);
		
		setGuiSubMenuActions();
	}
	
	@Override
	public Button[] getSubMenu() {	
		return subMenu;
	}
	
	@Override
	public Button[] getMenuButtons() {
		return null;
	}
	
	private void setGuiSubMenuActions() {
		applyEventHandler(subMenu, EXIT_LABEL, e -> {
			// Closing the browser app
			Browser.primaryStage.close();
			onFinish.accept(null);
		});
	}
}
