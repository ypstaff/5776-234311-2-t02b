package yearlyproject.programming.stackoverflow;

import java.util.function.Consumer;

import yearlyproject.programming.common.GuiSegment;
import yearlyproject.programming.common.MenuButtons;
import yearlyproject.programming.controlEngine.KeySender;

import com.google.inject.Inject;

/**
 * A class that represents the browser menus gui
 * 
 * @author Lior Volin
 *
 */
public class BrowserGui extends GuiSegment {

	private static final String EXIT_GUI_LABEL = "Finish";
	private static final String ARROWS_GUI_LABEL = "Page Nav.";
	private static final String TAB_GUI_LABEL = "Links Nav.";
	private static final String KEYBOARD_GUI_LABEL = "Input";
	private static final String CONTROL_GUI_LABEL = "Control";
	private static final String WIN_TITLE = "Stack Overflow";

	@Inject
	public BrowserGui(final KeySender sender) {
		this(sender, null, null);
	}

	@Inject
	public BrowserGui(final KeySender sender, Consumer<Void> onFinish, Consumer<Void> onTut) {

		// A way around array initialization restrictions
		// All the menus in the browser gui
		MenuButtons[] tmp = {
				new ControlGui(sender),
				new KeyboardGui(sender),
				new TabGui(sender),
				new ArrowsGui(sender),
				new ExitGui(onFinish) };
		menus = tmp;

		// The labels of the menus in the browser gui
		String[] tmpLabels = {
				CONTROL_GUI_LABEL,
				KEYBOARD_GUI_LABEL,
				TAB_GUI_LABEL,
				ARROWS_GUI_LABEL,
				EXIT_GUI_LABEL };
		labels = tmpLabels;

		winTitle = WIN_TITLE;

		applyStyles();

//		setOnTutorial(onTut);
		buildToVbox(false);
	}
}
