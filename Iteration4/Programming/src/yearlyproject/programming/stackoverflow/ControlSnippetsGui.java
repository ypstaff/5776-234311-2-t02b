package yearlyproject.programming.stackoverflow;

import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Separator;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;
import yearlyproject.programming.common.MenuButtons;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.util.List;
import java.util.function.Consumer;

/**
 * A class that represents the copy code nav. sub-menu gui
 * 
 * @author Lior Volin
 *
 */
public class ControlSnippetsGui extends MenuButtons {

	public static final String NEXT_LABEL = "Next Snippet";
	public static final String PREVIOUS_LABEL = "Previous Snippet";
	public static final String COPY_LABEL = "Copy Snippet";
	
	private static final int UNINITIALIZED_FONT_SIZE = -1;
	private static final int SNIPPETS_LIST_STEP = 1;
	
	private static List<String> codeSnippets;
	private static int currentIndex;
	protected static double fontSize;
	private Consumer<Void> onFinish;
	
	public ControlSnippetsGui(List<String> codeSnippets, Consumer<Void> onFinish) {
		ControlSnippetsGui.codeSnippets = codeSnippets;
		currentIndex = 0;
		this.onFinish = onFinish;
		
		subMenu = new Button[3];
		subMenu[0] = new Button(NEXT_LABEL);
		subMenu[1] = new Button(PREVIOUS_LABEL);
		subMenu[2] = new Button(COPY_LABEL);
		
		setGuiSubMenuActions();
		
		fontSize = UNINITIALIZED_FONT_SIZE;
		// Setting the code sinppets window and font size to default
		fontSize = ControlSnippetsGui.setCodeSnippetsView(currentIndex, 0);
	}
	
	@Override
	public Button[] getSubMenu() {	
		return subMenu;
	}
	
	@Override
	public Button[] getMenuButtons() {
		return null;
	}
		
	private void setGuiSubMenuActions() {
		applyEventHandler(subMenu, NEXT_LABEL, e -> {
			// List boundaries check
			if (currentIndex+SNIPPETS_LIST_STEP+1 <= codeSnippets.size()) {
				setCodeSnippetsView(currentIndex+SNIPPETS_LIST_STEP, 0);
				currentIndex = currentIndex+SNIPPETS_LIST_STEP;
			}
		});
		
		applyEventHandler(subMenu, PREVIOUS_LABEL, e -> {
			// List boundaries check
			if (currentIndex-SNIPPETS_LIST_STEP >= 0) {
				setCodeSnippetsView(currentIndex-SNIPPETS_LIST_STEP, 0);
				currentIndex = currentIndex-SNIPPETS_LIST_STEP;
			}
		});
		
		applyEventHandler(subMenu, COPY_LABEL, e -> {
			// Copying the selected code snippet to the clipboard
			StringSelection selection = new StringSelection(codeSnippets.get(currentIndex));
			Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
			clipboard.setContents(selection, selection);
			
			// Closing the browser app
			Browser.primaryStage.close();
			onFinish.accept(null);
		});
	}
		
	protected static double setCodeSnippetsView(int fontOffset){
		return setCodeSnippetsView(currentIndex, fontOffset);
	}
	
	// Updating the code snippets window
	private static double setCodeSnippetsView(int newIndex, int fontOffset) {
		GridPane gridPane = new GridPane();
		Label indexLabel = new Label("Snippet #" + new Integer(newIndex+1) + " out of " + codeSnippets.size());
		Label emptyLine1 = new Label("");
		Separator separator = new Separator();
		Label emptyLine2 = new Label("");
		Label snippetLabel = new Label(codeSnippets.get(newIndex));
		
		// If font size is initialized and not default than change it
		if ((fontSize != UNINITIALIZED_FONT_SIZE) && ((int)indexLabel.getFont().getSize() != fontOffset+(int)fontSize)) {
			indexLabel.setFont(new Font(fontOffset+(int)fontSize));
			snippetLabel.setFont(new Font(fontOffset+(int)fontSize));
		}
		
		GridPane.setConstraints(indexLabel, 0, 0);
		GridPane.setConstraints(emptyLine1, 0, 1);
		GridPane.setConstraints(separator, 0, 2);
		GridPane.setConstraints(emptyLine2, 0, 3);
		GridPane.setConstraints(snippetLabel, 0, 4);
		gridPane.getChildren().addAll(indexLabel, emptyLine1, separator, emptyLine2, snippetLabel);
		
		// Showing the new code snippets window
		Browser.primaryStage.setScene(new Scene(gridPane, Browser.primaryStage.getScene().getWidth(),
				Browser.primaryStage.getScene().getHeight()));
		
		return indexLabel.getFont().getSize();
	}
}
