package yearlyproject.programming.stackoverflow;

import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;
import yearlyproject.programming.common.MenuButtons;

import java.util.List;
import java.util.function.Consumer;

/**
 * A class that represents the browser's finish sub-menu gui
 * 
 * @author Lior Volin
 *
 */
public class ExitGui extends MenuButtons {

	public static final String COPY_LABEL = "Copy Code";
	public static final String EXIT_LABEL = "Exit Browser";
	
	private static final String ERROR_NO_SNIPPETS = "There were no code snippets in the page you were visiting";
	private static final String ERROR_NO_STACKOVERFLOW = "Copy code works only in stackoverflow.com";
	private static final int FONT_SIZE = 30;
	private static final String STACKOVERFLOW_COM = "stackoverflow.com/";

	private Consumer<Void> onFinish;
	private Scene notStackoverflowScene;
	private Scene noSnippetsScene;

	public ExitGui(Consumer<Void> onFinish) {
		this.onFinish = onFinish;

		subMenu = new Button[2];
		subMenu[0] = new Button(COPY_LABEL);
		subMenu[1] = new Button(EXIT_LABEL);

		setGuiSubMenuActions();

		notStackoverflowScene = createSeneWithLabel(ERROR_NO_STACKOVERFLOW);
		noSnippetsScene = createSeneWithLabel(ERROR_NO_SNIPPETS);
	}

	private Scene createSeneWithLabel(String text) {
		Label label = new Label(text);
		label.setFont(new Font(FONT_SIZE));
		GridPane gridPane = new GridPane();
		gridPane.getChildren().addAll(label);
		gridPane.setAlignment(Pos.CENTER);
		return new Scene(gridPane, Browser.primaryStage.getScene().getWidth(),
				Browser.primaryStage.getScene().getHeight());
	}

	@Override
	public Button[] getSubMenu() {
		return subMenu;
	}

	@Override
	public Button[] getMenuButtons() {
		return null;
	}

	private void setGuiSubMenuActions() {
		applyEventHandler(subMenu, COPY_LABEL, e -> {
			String url = Browser.myWebEngine.getLocation();
			
			// If not in stackoverflow showing an error message
			if (!url.toLowerCase().contains(STACKOVERFLOW_COM)) {
				Browser.primaryStage.setScene(notStackoverflowScene);
				errorMassageConsumer.accept(null);
			} else {
				List<String> codeSnippets = Utilities.extractCodeSnippets(url);
				
				switch (codeSnippets.size()) {
					case 0:
						// Showing the no code snippets error message
						Browser.primaryStage.setScene(noSnippetsScene);
						errorMassageConsumer.accept(null);
						break;
	
					// In stackoverflow and there are code snippets in the page
					default:
						copyCodeConsumer.accept(codeSnippets);
						break;
				}
			}
		});
		
		applyEventHandler(subMenu, EXIT_LABEL, e -> {
			// Closing the browser app
			Browser.primaryStage.close();
			onFinish.accept(null);
		});
	}
}
