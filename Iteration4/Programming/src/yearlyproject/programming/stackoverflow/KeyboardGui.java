package yearlyproject.programming.stackoverflow;

import javafx.scene.control.Button;
import yearlyproject.programming.common.MenuButtons;
import yearlyproject.programming.controlEngine.KeySender;

import com.google.inject.Inject;

/**
 * A class that represents the browser's keyboard sub-menu gui
 * 
 * @author Lior Volin
 *
 */
public class KeyboardGui extends MenuButtons {

	public static final String KEYBOARD_LABEL = "Keyboard";
	public static final String PASTE_LABEL = "Paste";

	@Inject
	private final KeySender sender;

	public KeyboardGui(KeySender sender) {
		this.sender = sender;

		subMenu = new Button[2];
		subMenu[0] = new Button(KEYBOARD_LABEL);
		subMenu[1] = new Button(PASTE_LABEL);

		setGuiSubMenuActions();
	}

	@Override
	public Button[] getSubMenu() {
		return subMenu;
	}

	@Override
	public Button[] getMenuButtons() {
		return null;
	}

	private void setGuiSubMenuActions() {
		applyEventHandler(subMenu, KEYBOARD_LABEL, e -> {
			// Only when in the main google search page automatically writes
			//the label 'stackoverflow.com ' in order to get results from there only
			if (Browser.myWebEngine.getHistory().getCurrentIndex() == 0) {
				sender.send("s");
				sender.send("i");
				sender.send("t");
				sender.send("e");
				sender.send(":");
				sender.send("s");
				sender.send("t");
				sender.send("a");
				sender.send("c");
				sender.send("k");
				sender.send("o");
				sender.send("v");
				sender.send("e");
				sender.send("r");
				sender.send("f");
				sender.send("l");
				sender.send("o");
				sender.send("w");
				sender.send(".");
				sender.send("c");
				sender.send("o");
				sender.send("m");
				sender.send("space");
			}
			keyboards.regular();
		});
		
		applyEventHandler(subMenu, PASTE_LABEL, e -> {
			sender.send("ctrl", "v");
		});
	}
}
