package yearlyproject.programming.common;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;
import java.util.function.Consumer;

import javafx.application.Platform;
import javafx.embed.swing.JFXPanel;
import javafx.event.EventHandler;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.input.MouseEvent;
import javafx.stage.Screen;
import javafx.util.Pair;

import javax.swing.JFrame;
import javax.swing.Timer;

import org.controlsfx.dialog.CommandLinksDialog;
import org.controlsfx.dialog.CommandLinksDialog.CommandLinksButtonType;

import yearlyproject.pcBluetoothApp.Controller;
import yearlyproject.programming.common.LongLabelsHBox.LABEL;
import yearlyproject.programming.controlEngine.ConfigManager;
import yearlyproject.programming.controlEngine.WindowedKeySender;
import yearlyproject.programming.debug.DebugGui;
import yearlyproject.programming.edit.EditGui;
import yearlyproject.programming.keyboard.KeyBoard;
import yearlyproject.programming.keyboard.KeyBoardNumbers;
import yearlyproject.programming.keyboard.KeyBoardSymbols;
import yearlyproject.programming.keyboard.OpenKeyboards;
import yearlyproject.programming.stackoverflow.Browser;
import yearlyproject.programming.stackoverflow.BrowserGui;
import yearlyproject.programming.stackoverflow.CopyCodeGui;
import yearlyproject.programming.stackoverflow.ErrorMessageGui;
import yearlyproject.programming.tutorial.Tutorial;
import yearlyproject.programming.tutorial.TutorialControlHBox;
import yearlyproject.programming.view.ViewGui;
import Navigation.IterableNodeGroups;
import Navigation.IterationControlHBox;

/**
 * Controller that controls the three buttons handlers for the Programming app
 * @author Roy Svinik, lmaman
 *
 */
public class ProgrammingController implements Controller {

	private IterableNodeGroups root;
	private JFXPanel panel;
	private WindowedKeySender sender;
	private MODE inMode;
	private MODE oldMode;
	private OpenKeyboards keyboards;
	private Consumer<Void> browserConsumer;
	private Consumer<Void> errorMessageConsumer;
	private Consumer<List<String>> copyCodeConsumer;
	private Consumer<Void> onFinishTut;
	private Consumer<Void> onTut;
	private GuiSegment currentSegment;
	private ViewGui viewGui;
	private boolean isInKeyboard;	
	
	private Map<NAV_BUTTONS, Consumer<Void>> navHandlers;
	private EventHandler<MouseEvent> optionsHandler;
	
	private static final String FOCUS_LABEL = "F12";
	private static final String CHANGE_PRESPECTIVE_LABEL = "F8";
	
	public static final Pair<Integer, Integer> defaultControllerSize = new Pair<Integer, Integer>(100,48);
	public static final Pair<Integer, Integer> smallControllerSize = new Pair<Integer, Integer>(100,34);
	public static final Pair<Integer, Integer> mediumControllerSize = new Pair<Integer, Integer>(100,38);
	public static Boolean BTConnected = false;
	
	private static final int LONG_LABEL_SHOW_TIME = 250;
	
	private static Consumer<Boolean> Refresh = (b) -> {};
	
	enum MODE{
		DEBUG,
		EDIT,
		VIEW, 
		BROWSER,
		BROWSER_ERROR_MESSAGE,
		COPY_CODE,
		TUTORIAL,
		KEYBOARD,
	}
	
	public enum NAV_BUTTONS{
		OPTIONS,
		LONG_BACK,
		LONG_NEXT,
		LONG_SELECT
	}

	/**
	 * Creates a ProgrammingController
	 * @param frame the window's frame
	 * @param panel the panel to fill with the ProgrammingController's GUI
	 * @param sender {@link WindowedKeySender} to send keys
	 */
	public ProgrammingController(JFrame frame, JFXPanel panel, WindowedKeySender sender) {
		this.panel = panel;
		this.sender = sender;
		
		TutorialControlHBox.initTutorial();
		
		this.onFinishTut = (e) -> {
			returnToMode(true);
		};
		
		//Akiva ask why need sender
		this.onTut = (e) ->{
			resizeAll(defaultControllerSize);
			changeMode(MODE.TUTORIAL);
			setMode(sender, new Tutorial(onFinishTut));
		};
		
		//create keyboards handlers
		keyboards = new OpenKeyboards() {
			
			@Override
			public void symbols() {
				changeMode(MODE.KEYBOARD);
				setMode(sender, new KeyBoardSymbols(sender, (y) -> {
					returnToMode();	
					isInKeyboard = false;
				},onTut, keyboards));
			}
			
			@Override
			public void regular() {
				changeMode(MODE.KEYBOARD);
								isInKeyboard = true;
								resizeAll(defaultControllerSize);
								setMode(sender, new KeyBoard(sender,(y) -> {
									returnToMode();
									isInKeyboard = false;
								}, onTut, keyboards));
			}

			@Override
			public void numbers() {
				changeMode(MODE.KEYBOARD);
				setMode(sender, new KeyBoardNumbers(sender, (y) -> {
					returnToMode();	
				}, onTut));
			}
		};
		
		//create options handlers
		optionsHandler = (x) -> {
			System.out.println("Options clicked");
			
			List<CommandLinksButtonType> links = new ArrayList<>();
			CommandLinksButtonType changeEclipseVersion = new CommandLinksButtonType("Change Eclipse version",
			        "", false);
			CommandLinksButtonType cancelCommand = new CommandLinksButtonType("Cancel", "", false);
			
			links.add(changeEclipseVersion);
			links.add(cancelCommand);
			
			CommandLinksDialog cld = new CommandLinksDialog(links);
			
			cld.setTitle("Options");
			cld.setOnShown((e)->{
				cld.setY(frame.getHeight() + 5);
			});
			
			cld.showAndWait().filter(r -> r != cancelCommand.getButtonType()).ifPresent(r -> {
				if(r == changeEclipseVersion.getButtonType()){
					ConfigManager.registerUseDeafaultToFalse();
					frame.setAlwaysOnTop(false);
					sender.restart();
					frame.setAlwaysOnTop(true);
					returnToMode();
				}
			});
			
		};
		
		browserConsumer = (x) -> {
			setBrowserMode();
		};
		
		errorMessageConsumer = (x) -> {
			setErrorMessageMode();
		};
		
		copyCodeConsumer = (List<String> codeSnippets) -> {
			setCopyCodeMode(codeSnippets);
		};
		
		Refresh = (b)->{
			oldMode = inMode;
			returnToMode(b);
		};
		
		navHandlers = new HashMap<ProgrammingController.NAV_BUTTONS, Consumer<Void>>();
		navHandlers.put(NAV_BUTTONS.LONG_BACK, (x)-> {ProgrammingController.this.longBack();});
		navHandlers.put(NAV_BUTTONS.LONG_SELECT, (x)-> {ProgrammingController.this.longSelect();});
		navHandlers.put(NAV_BUTTONS.LONG_NEXT, (x)-> {ProgrammingController.this.longNext();});

		//start from View mode
		setViewMode();
	}

	/**
	 * The Next action
	 */
	@Override
	public void next() {
		highLightLabel(LongLabelsHBox.LABEL.NEXT);
		root.next();
	}

	/**
	 * The Select action
	 */
	@Override
	public void select() {
		highLightLabel(LongLabelsHBox.LABEL.SELECT);
		root.select();
	}

	/**
	 * The Back action
	 */
	@Override
	public void back() {
		highLightLabel(LongLabelsHBox.LABEL.BACK);
		root.unselect();
	}

	/**
	 * The Long Next action
	 */
	@Override
	public void longNext() {
		System.out.println("long next");
		if (!MODE.BROWSER.equals(inMode) && !MODE.BROWSER_ERROR_MESSAGE.equals(inMode) && !MODE.COPY_CODE.equals(inMode)
				&& !MODE.VIEW.equals(inMode) && !MODE.KEYBOARD.equals(inMode) && !MODE.TUTORIAL.equals(inMode)) {
			setViewMode();
		}
		
	}

	/**
	 * The Long Select action
	 */
	@Override
	public void longSelect() {
		System.out.println("long select");
		if (!MODE.BROWSER.equals(inMode) && !MODE.BROWSER_ERROR_MESSAGE.equals(inMode) && !MODE.COPY_CODE.equals(inMode)
				&& !MODE.EDIT.equals(inMode) && !MODE.KEYBOARD.equals(inMode) && !MODE.TUTORIAL.equals(inMode)) {
			setEditMode();
		}
	}

	/**
	 * The Long Back action
	 */
	@Override
	public void longBack() {
		System.out.println("long back");
		if (!MODE.BROWSER.equals(inMode) && !MODE.BROWSER_ERROR_MESSAGE.equals(inMode) && !MODE.COPY_CODE.equals(inMode)
				&& !MODE.DEBUG.equals(inMode) && !MODE.KEYBOARD.equals(inMode) && !MODE.TUTORIAL.equals(inMode)) {
			setDebugMode();
		}
	}
	
	/**
	 * Set the mode represented in {@link rootedObject} to the screen
	 * @param sender the key sender
	 * @param rootedObject the mode
	 */
	private void setMode(WindowedKeySender sender, RootedObject rootedObject) {
		this.root = rootedObject.getIterableRoot();
		IterationControlHBox iterControl = root.getIterationControlHBoxControlHBox();
		
		if (navHandlers != null){
			//Set special navigation handlers
			iterControl.setOnBackLongClick(navHandlers.get(NAV_BUTTONS.LONG_BACK));
			iterControl.setOnSelectLongClick(navHandlers.get(NAV_BUTTONS.LONG_SELECT));
			iterControl.setOnNextLongClick(navHandlers.get(NAV_BUTTONS.LONG_NEXT));
		}
		if(optionsHandler != null){
			iterControl.setOptionsOnClickHandler(optionsHandler);
		}
		
		this.panel.setScene(new Scene(root.getParentPane()));
		this.root.start();
	}
	
	/**
	 * Set Edit mode.
	 * Side effects: 
	 * 			* Focus on the Editor
	 * 			* Maximize the Editor
	 * 			* Resize
	 * 			* Horozontial
	 * 			* Moves out of Debug perspective if needed
	 */
	private void setEditMode() {
		System.out.println("Edit");
		
		sender.send("ESC");
		sender.backToHorozotial();
		changeMode(MODE.EDIT);
		resizeAll(mediumControllerSize);
		EditGui eg = new EditGui(sender, onTut);
		eg.setOpenKDB(keyboards);
		eg.setBrowserConsumer(browserConsumer);
		setMode(sender, eg);
		//Make sure we're not in the "Debug" perspective, and terminate any running debug sessions.
		sender.checkTitleAndDo("Debug" ,false ,(x)->{  sender.send("CTRL", "F2"); sender.send("CTRL", CHANGE_PRESPECTIVE_LABEL);});

		if(oldMode != MODE.EDIT)
			maximizeEditor();
		
		sender.send(FOCUS_LABEL);
		//expand code area
		currentSegment = eg;
	}

	/**
	 * Set View mode.
	 * Side effects: 
	 * 			* Resize
	 * 			* Horozontial
	 * 			* Moves out of Debug perspective if needed
	 */
	private void setViewMode() {
		System.out.println("View");
		
		if (oldMode != null) {
			sender.send("ESC");
			sender.backToHorozotial();
		}
		changeMode(MODE.VIEW);
		resizeAll(defaultControllerSize);
		ViewGui vg = new ViewGui(sender, navHandlers, optionsHandler, onTut);
		vg.setOpenKDB(keyboards);
		setMode(sender, vg);
		//Make sure we're not in the "Debug" perspective, and terminate any running debug sessions.
		sender.checkTitleAndDo("Debug" ,false, (x)->{ sender.send("CTRL", "F2"); sender.send("CTRL", CHANGE_PRESPECTIVE_LABEL); });
		
		if(oldMode == MODE.EDIT)
			minimizeEditor();
		
		viewGui = vg;
	}
	
	/**
	 * Set Debug mode.
	 * Side effects: 
	 * 			* Moves to Debug perspective if needed
	 * 			* Resize
	 * 			* Horozontial
	 */
	private void setDebugMode() {
		System.out.println("Debug");
		
		sender.send("ESC");
		sender.backToHorozotial();
		changeMode(MODE.DEBUG);
		resizeAll(smallControllerSize);
		DebugGui dg = new DebugGui(sender, onTut);
		dg.setOpenKDB(keyboards);
		setMode(sender, dg);
		
		
		if(oldMode == MODE.EDIT)
			minimizeEditor();
		
		//Make sure we're in the "Debug" perspective.
		sender.checkTitleAndDo("Debug", true, (x)->{sender.send("CTRL", CHANGE_PRESPECTIVE_LABEL);});
		
		currentSegment = dg;
	}
	
	/**
	 * Opens web browser mode.
	 * a second java window will open containing the browser.
	 */
	private void setBrowserMode() {
		changeMode(MODE.BROWSER);
		resizeAll(mediumControllerSize);
		BrowserGui bg = new BrowserGui(sender, e -> { setEditMode(); }, onTut);
		bg.setErrorMessageConsumer(errorMessageConsumer);
		bg.setCopyCodeConsumer(copyCodeConsumer);
		bg.setOpenKDB(keyboards);
		setMode(sender, bg);
		//Make sure we're not in the "Debug" perspective, and terminate any running debug sessions.
		sender.checkTitleAndDo("Debug", false, (x)->{ sender.send("CTRL", "F2"); sender.send("CTRL", CHANGE_PRESPECTIVE_LABEL);});	
	}
	
	/**
	 * Sets an error message screen in browser mode.
	 */
	private void setErrorMessageMode() {
		changeMode(MODE.BROWSER_ERROR_MESSAGE);
		resizeAll(mediumControllerSize);
		ErrorMessageGui emg = new ErrorMessageGui(sender, e -> { setEditMode(); },e -> { setBrowserMode(); }, onTut);
		emg.setOpenKDB(keyboards);
		setMode(sender, emg);
		//Make sure we're not in the "Debug" perspective.
		sender.checkTitleAndDo("Debug", false, (x)->{sender.send("CTRL", CHANGE_PRESPECTIVE_LABEL);});		
	}
	
	/**
	 * Opens a screen to choose a code snippet
	 */
	private void setCopyCodeMode(List<String> codeSnippets) {
		changeMode(MODE.COPY_CODE);
		resizeAll(mediumControllerSize);
		CopyCodeGui ccg = new CopyCodeGui(codeSnippets, sender, e -> { sender.backToHorozotial(); setEditMode(); },e -> { sender.backToHorozotial(); setBrowserMode(); }, onTut);
		ccg.setOpenKDB(keyboards);
		setMode(sender, ccg);
		//Make sure we're not in the "Debug" perspective.
		sender.checkTitleAndDo("Debug", false, (x)->{sender.send("CTRL", CHANGE_PRESPECTIVE_LABEL);});
		sender.changeToVertical(true);
	}
	
	private void returnToMode(){
		returnToMode(false);
	}
	
	/**
	 * Return to the last mode, from keyboard or tutorial
	 * @param fromTut is returning from tutorial 
	 */
	private void returnToMode(Boolean fromTut) {
		System.out.print("Returning to mode: ");
		if (fromTut)
		{
			if (isInKeyboard){
				keyboards.regular();
				
				return;
			}
		}
		
		if (oldMode == null) {
			setViewMode();
			return;
		}

		switch (oldMode){
		case DEBUG:
			setDebugMode();
			break;
		case EDIT:
			setEditMode();
			break;
		case VIEW:
			setViewMode();
			break;
		case BROWSER:
			setBrowserMode();
			sender.send("enter");
			isInKeyboard = false;
			resizeAll(mediumControllerSize);
			break;
		case BROWSER_ERROR_MESSAGE:
			setErrorMessageMode();
			break;
		default:
			break;
			
		}
	}
	
	/**
	 * Resize both the controller and the sender windows.
	 *
	 * @param dim a {@link Pair} of dimensions. 
	 * 		  <b>Key</b> is number of sections to dived the screen to.
	 * 		  <b>Value</b> is the number of sections given to the controller window. 
	 * 		  The rest will be given to the sender window.
	 */
	private void resizeAll(Pair<Integer, Integer> dim) {
		sender.resizeController(dim.getKey(), dim.getValue(), false);
		sender.resize(dim.getKey(), dim.getKey() - dim.getValue(), true);
			if (inMode == MODE.KEYBOARD) {
				Browser.resizeBrowser(defaultControllerSize);
			} else {
				Browser.resizeBrowser(mediumControllerSize);
			}
	}

	/**
	 * Calculates the dimensions of the window, given the parameters
	 * @param numOfAllSections how many sections to divide the screen to
	 * @param numOfTakenSegments how many sections give the current window
	 * @param gravityBottom put window to or bottom
	 * @return the dimension for the window
	 */
	public static Rectangle2D getDimentsions(int numOfAllSections,
			int numOfTakenSegments, boolean gravityBottom) {
		Rectangle2D bounds = Screen.getPrimary().getVisualBounds();
		int newHeight = (int) (numOfTakenSegments * bounds.getHeight() / numOfAllSections);
        int newWidth = (int) bounds.getWidth();
        int newX = 0;
        int newY = 0;
        if(gravityBottom)
        	newY =(int) ((numOfAllSections-numOfTakenSegments) * bounds.getHeight() / numOfAllSections);
		Rectangle2D rec = new Rectangle2D(newX, newY, newWidth, newHeight);
		return rec;
	}
	
	/**
	 * Makes a long press label above the navigation button appear
	 * @param l: the Label you want to show
	 */
	private void zoomInCurrSegmentLabel(LABEL l) {
		LongLabelsHBox llHBox = getCurrLLHBOX();
		if( llHBox == null) 
			return;
		llHBox.zoomIn(l);
	}
	
	/**
	 * Makes a long press label above the navigation button disappear
	 * @param l: the Label you want to hide
	 */
	private void zoomOutCurrSegmentLabel(LABEL l) {
		LongLabelsHBox llHBox = getCurrLLHBOX();
		if( llHBox == null) 
			return;
		llHBox.zoomOut(l);
	}

	/**
	 * Finds the current gui segment and extract its LongLabelsHBox
	 * @return the current LongLabelsHBox
	 */
	private LongLabelsHBox getCurrLLHBOX() {
		LongLabelsHBox llHBox = null;
		switch (inMode) {
		case DEBUG:
			llHBox = currentSegment.getLongLablesHBox();
			break;
		case EDIT:
			llHBox = currentSegment.getLongLablesHBox();
			break;
		case VIEW:
			llHBox = viewGui.getLongLablesHBox();
			break;
		default:
			break;
		}
		return llHBox;
	}
	
	/**
	 * Makes  a long press label above the navigation button appear for a moment
	 * @param l: the Label you want to show
	 */
	private void highLightLabel(LongLabelsHBox.LABEL l) {
		zoomInCurrSegmentLabel(l);
		Timer timer = new Timer(LONG_LABEL_SHOW_TIME, new ActionListener(){
		    @Override
		    public void actionPerformed(ActionEvent e) {
		    	zoomOutCurrSegmentLabel(l);
		    }
		});
		timer.setRepeats(false);
		timer.start();
	}

	/**
	 * Refresh the current segment to get new GUI changes done visible
	 * @param fromTut
	 */
	public static void refreshCurrentSegment(Boolean fromTut) {
		Refresh.accept(fromTut);
	}
	
	/**
	 * Maximize code editor
	 */
	private void maximizeEditor() {
		sender.send("F12");
		sender.send("CTRL", "4");
		sender.send("F12");
		sender.send("CTRL", "m");
	}

	/**
	 * Minimize code editor
	 */
	private void minimizeEditor() {
		sender.send("F12");
		sender.send("CTRL", "4");
		sender.send("F12");
		sender.send("CTRL", "m");
		sender.send("CTRL", "m");
	}
	
	/**
	 * Called on connection made with the BT server. Sets a visual indicator. 
	 */
	@Override
	public void onConnectionMade(){
		BTConnected = true;
		Platform.runLater( () -> { refreshCurrentSegment(false); } );
	}
	
	/**
	 * Called on connection killed with the BT server. Sets a visuzl indicator.
	 */
	public void onConnectuonKilled() {
		BTConnected = false;
		Platform.runLater( () -> { refreshCurrentSegment(false); } );
	}
	
	/**
	 * updates inMode and oldMode fields
	 */
	private void changeMode(MODE newMode) {
		if (newMode == inMode)
			return;
		oldMode = inMode;
		inMode = newMode;
	}
}
