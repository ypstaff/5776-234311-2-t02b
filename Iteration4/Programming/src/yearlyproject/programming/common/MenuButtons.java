package yearlyproject.programming.common;

import java.util.List;
import java.util.function.Consumer;

import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;

import org.apache.log4j.Logger;

import yearlyproject.programming.keyboard.OpenKeyboards;

/**
 * classes of MenuButtons need to implement the gui and the event handlers of
 * the buttons
 * 
 * @author kfirlan
 *
 */
public abstract class MenuButtons {
	private static final Logger LOGGER = Logger.getLogger(MenuButtons.class);
	
	public static final String FONT = "-fx-font: 13 arial";
	public abstract Button[] getSubMenu();

	protected Consumer<Void> onMenuAction = (x)->{LOGGER.error("unimplemented onMenuAction");};
	protected OpenKeyboards keyboards;
	protected Consumer<Void> browserConsumer;
	protected Consumer<Void> errorMassageConsumer;
	protected Consumer<List<String>> copyCodeConsumer;
	
	protected Button[] subMenu;
	protected Button[] mainButtons;
	
	/**
	 * setOnAction(handler) for the button with the matching label assume there
	 * is only one
	 * 
	 * @param bs array with different labels
	 * @param label
	 * @param handler
	 * @return true if set the EventHandler for a button else false
	 */
	protected boolean applyEventHandler(Button[] bs, String label, EventHandler<MouseEvent> handler) {
		for (Button button : bs)
			if (label.equals(button.getText())) {
				button.setOnMouseClicked(handler);
				return true;
			}
		return false;
	}
	/**
	 * set which submenu to switch to on main menu butoon press.
	 * @param onMenuAction
	 */
	public void setMenuSwitch(Consumer<Void> onMenuAction){
		if (onMenuAction==null){
			LOGGER.error("onMenuAction set to NULL");
			this.onMenuAction = (x)->{};
			return;
		}
		this.onMenuAction = onMenuAction;
	}
	/**
	 * set how to open keyboard
	 * @param keyboards
	 */
	public void setOpenKeyboard(OpenKeyboards keyboards){
		if (keyboards == null){
			LOGGER.error("openKBD set to NULL");
			return;
		}
		this.keyboards = keyboards;
	}

	protected void applyStyle(){
		//Default style
		String css_style = "-fx-background-color: Black; -fx-text-fill: White; "+ MenuButtons.FONT;
		if (subMenu != null)
			for (Button button : subMenu)
				button.setStyle(css_style);
		if (mainButtons != null)
			for (Button button : mainButtons)
				button.setStyle(css_style);
	}
	
	public Button[] getMenuButtons(){
		return null;
	}
	

	
	public Button findButton(String label) {
		for (Button b : getSubMenu())
			if (b.getText().equals(label))
				return b;
		return null;
	}
}
