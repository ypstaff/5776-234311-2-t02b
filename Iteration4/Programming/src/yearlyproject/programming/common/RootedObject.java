package yearlyproject.programming.common;
import Navigation.IterableNodeGroups;

/**
 * Interface for Classes that uses Navigation and has a root IterableNodeGroups
 * @author Roy Svinik 
 *
 */
public interface RootedObject {

	public IterableNodeGroups getIterableRoot();
	
}
