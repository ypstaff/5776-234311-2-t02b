package yearlyproject.programming.common;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import yearlyproject.programming.keyboard.OpenKeyboards;
import yearlyproject.programming.tutorial.TutorialControlHBox;
import Navigation.IterableNodeGroup;
import Navigation.IterableNodeGroups;
import Navigation.IterationControlHBox;

/**
 * A class to represent a part of the gui
 * @author lmaman, Roy Svinik
 */
public abstract class GuiSegment implements RootedObject {

	protected MenuButtons[] menus = {};
	protected String[] labels = {};
	protected List<IterableNodeGroup> groups;
	protected VBox vbox = new VBox(VBOX_GAP);
	protected VBox lablesVbox = new VBox(VBOX_GAP);
	protected IterableNodeGroups root;
	private Label winLabel;
	protected String winTitle = "";
	private Consumer<Void> onTut;
	protected IterationControlHBox iterControl;
	private LongLabelsHBox longLablesHBox;
	protected Boolean hasTutorial = true;
	
	protected final static int HBOX_GAP = 1;
	protected final static int VBOX_GAP = 3;
	
	/**
	 * Build the Gui into vbox.
	 * @Pre <b>menuButtons</b> and <b>labels</b> need to be set prior to calling this method.
	 * @param showLabeling show labeling over the iteration buttons
	 */
	protected void buildToVbox(boolean showLabeling) {
		List<HBox> hboxes = new ArrayList<>();
		for (MenuButtons menuButtons : menus) {
			hboxes.add(createLine(menuButtons.getSubMenu()));
		}
		
		vbox.getChildren().addAll(hboxes);
		
		setLabels();
		buildRoot(showLabeling);
	}
	
	protected void buildToVbox() {
		this.buildToVbox(true);
	}
	
	/**
	 * Sets the labels in <b>labels</b> to the corresponding <b>menuButtons</b>.
	 */
	private void setLabels() {
		for (int i = 0 ; i < labels.length ; i++ ){
			Label l = new Label(labels[i], vbox.getChildren().get(i));
			lablesVbox.getChildren().add(l);
		}
	}
	
	/**
	 * Set action to execute on calling tutorial
	 * @param onTut action to execute on calling tutorial
	 */
	protected void setOnTutorial(Consumer<Void> onTut) {
		this.onTut = onTut;
	}
	
	/**
	 * Create the pane and iterable groups of the gui
	 */
	protected void buildRoot(boolean showLabeling) {
		BorderPane bp = new BorderPane();
		bp.setLeft(lablesVbox);

		bp.setCenter(vbox);
		root = new IterableNodeGroups(bp);
		
		if(onTut != null){
			// integrating the keyboard with the Tutorial
			TutorialControlHBox tutorialHBox = new TutorialControlHBox(root, onTut, bp);
			tutorialHBox.setTutorialButtons();
		}
		
		iterControl = root.getIterationControlHBoxControlHBox();
		if (showLabeling) {
			VBox controlBox = new VBox();
			longLablesHBox = new LongLabelsHBox(iterControl);
			controlBox.getChildren().addAll(longLablesHBox, iterControl);
			bp.setBottom(controlBox);
		} else {
			bp.setBottom(iterControl);
		}
		


		VBox rightVbox = new VBox(VBOX_GAP);

		winLabel = new Label(winTitle);
		winLabel.setStyle("-fx-font-size: 28;");
		rightVbox.getChildren().add(winLabel);
		
		if (ProgrammingController.BTConnected) {
			setBTIndicator(rightVbox, true);
		}
		else{
			setBTIndicator(rightVbox, false);
		}
		
		rightVbox.setAlignment(Pos.TOP_CENTER);
		bp.setRight(rightVbox);
		
		groups = new ArrayList<>();
		for(int i=0; i<vbox.getChildrenUnmodifiable().size(); ++i)
			addGroup(i);
	}
	
	/**
	 * Add a row in vbox as a group for iterations
	 * @param rowIdx: the index of the row
	 */
	protected void addGroup(int rowIdx) {
		groups.add(root.addChildGroup(((HBox) vbox.getChildrenUnmodifiable().get(rowIdx)).getChildren()));
	}
	
	/**
	 * Set the Bluetooth indicator in the rightVbox
	 * @param rightVbox the vbox to add the indicator to
	 * @param on is bluetooth connected
	 */
	public static void setBTIndicator(VBox rightVbox, Boolean on) {
		String fileName;
		if (on){
			fileName = "BluetoothOn.png";
		}
		else{
			fileName = "BluetoothOff.png";
		}
		
		InputStream is = ClassLoader.getSystemResourceAsStream("Resources/" + fileName);
		System.out.println("input stream " + is);
		
		if (is != null) {
			Image img = new Image(is);
			ImageView imgView = new ImageView();
			imgView.setImage(img);
			imgView.setFitHeight(50);
			imgView.setFitWidth(50);
			rightVbox.getChildren().add(imgView);
		}
	}
	
	@Override
	public IterableNodeGroups getIterableRoot() {
		return root;
	}
	
	/**
	 * Creates an HBox from array of buttons
	 * @param buttons array of buttons
	 * @return the HBox created
	 */
	private HBox createLine(final Button[] buttons) {
		HBox $ = new HBox(HBOX_GAP);
		$.getChildren().addAll(buttons);
		return $;
	}
	
	/**
	 * Update 'from' handler so after his action the iteration will move to 'to' row
	 * @param from
	 * @param to
	 */
	protected void setTransition(Button from, Class<?> to) {
		EventHandler<? super MouseEvent> oldHandler = from.getOnMouseClicked();
		from.setOnMouseClicked(e -> {
			oldHandler.handle(e);
			root.unselect();
			root.selectSpecific(groups.get(GuiSegment.this.getGuiIdx(to)));
			root.select();
		});
	}
	
	/**
	 * finds the index in menus array of 'guiClass' line
	 * @param guiClass
	 * @return
	 */
	protected Integer getGuiIdx(Class<?> guiClass) {
		for (int i = 0; i < menus.length; i++) {
			if(guiClass.isInstance(menus[i]))
				return i;
		}
		return null;
	}
	
	/**
	 * Set Keyboards handler
	 * @param keyboards the handler
	 */
	public void setOpenKDB(OpenKeyboards keyboards){
		for (MenuButtons menuButtons : menus)
			menuButtons.setOpenKeyboard(keyboards);
	}
	
	/**
	 * sets the browser consumer for each of the menus in the gui
	 * @param browserConsumer
	 */
	public void setBrowserConsumer(Consumer<Void> browserConsumer) {
		for (MenuButtons menu : menus) {
			menu.browserConsumer = browserConsumer;
		}
	}
	
	/**
	 * sets the error message consumer for each of the menus in the gui
	 * @param errorMessageConsumer
	 */
	public void setErrorMessageConsumer(Consumer<Void> errorMessageConsumer) {
		for (MenuButtons menu : menus) {
			menu.errorMassageConsumer = errorMessageConsumer;
		}
	}
	
	/**
	 * sets the copy code consumer for each of the menus in the gui
	 * @param copyCodeConsumer
	 */
	public void setCopyCodeConsumer(Consumer<List<String>> copyCodeConsumer) {
		for (MenuButtons menu : menus) {
			menu.copyCodeConsumer = copyCodeConsumer;
		}
	}
	
	/**
	 * Find the menu of Class menuClass
	 * @param menuClass
	 * @return the menu found
	 */
	protected MenuButtons findMenu(Class<?> menuClass) {
		return menus[getGuiIdx(menuClass)];
	}
	
	/**
	 * apply the styles for each 
	 */
	protected void applyStyles() {
		for (MenuButtons menu : menus) {
			menu.applyStyle();
		}
	}

	/**
	 * 
	 * @return the gui LongLabelsHBox
	 */
	public LongLabelsHBox getLongLablesHBox() {
		return longLablesHBox;
	}

	/**
	 * sets the gui LongLabelsHBox
	 * @param longLablesHBox: the target box
	 */
	public void setLongLablesHBox(LongLabelsHBox longLablesHBox) {
		this.longLablesHBox = longLablesHBox;
	}

}
