package yearlyproject.programming.common;
import java.awt.Image;
import java.awt.Toolkit;
import javafx.application.Application;
import javafx.embed.swing.JFXPanel;
import javafx.stage.Stage;

import javax.swing.JFrame;
import javax.swing.WindowConstants;

import yearlyproject.pcBluetoothApp.SPPBluetoothServer;
import yearlyproject.programming.controlEngine.EclipseController;
import yearlyproject.programming.controlEngine.WindowedKeySender;

import com.jacob.com.ComThread;

/**
 * The Launcher of the entire Programming application
 * @author Roy Svinik
 *
 */
public class Launcher extends Application {
	
	
	private JFrame frame;
	private JFXPanel panel;
	private WindowedKeySender sender;
	private static SPPBluetoothServer server;
	private ProgrammingController controller;
	
	private static final String FRAME_TITLE = "Brainiac Controller";
	

	
	public static void main(String[] args) {
		//Launch the control screen
        launch(args);
	}
	
	@Override
	public void stop() throws Exception {
		super.stop();
		ComThread.Release();
	}
	
	@Override
	public void start(Stage primaryStage) {

		sender = new EclipseController(primaryStage);
		
		frame = new JFrame(FRAME_TITLE);
		panel = new JFXPanel();
		
		frame.add(panel);
		frame.pack();
		frame.setFocusableWindowState(false);
		frame.setVisible(true);
		frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		frame.setAlwaysOnTop(true);
		
		//Set the Brainiac icon 
		java.net.URL url = ClassLoader.getSystemResource("Resources/brainiac.png");
		if(url != null){
			Toolkit kit = Toolkit.getDefaultToolkit();
			Image img = kit.createImage(url);
			frame.setIconImage(img);
		}
		
		controller = new ProgrammingController(frame, panel, sender);
		
		//Fire up the BT server
        server = new SPPBluetoothServer(controller);
		server.start();
	}


	
}