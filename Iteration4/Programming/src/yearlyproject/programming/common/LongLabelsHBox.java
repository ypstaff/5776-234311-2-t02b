package yearlyproject.programming.common;

import Navigation.IterationControlHBox;
import javafx.event.EventHandler;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;

/**
 * HBox for collecting labels appearing over iteration control buttons
 * @author Roy Svinik
 *
 */
public class LongLabelsHBox extends HBox {
	
	Label nextLable;
	Label selectLable;
	Label backLable;
	
	private static final int GAP = 28; 
	
	public enum LABEL {
		NEXT,
		SELECT,
		BACK
	}
	
	/**
	 * creates the HBox with the labels
	 * @param iterControl: the relevant 3 buttons HBox 
	 */
	public LongLabelsHBox(IterationControlHBox iterControl) {
		super(GAP);
		nextLable = new Label("View");
		selectLable = new Label("Edit");
		backLable = new Label("Debug");
		getChildren().addAll(nextLable, selectLable, backLable);
		
		for (LABEL l : LABEL.values()) {
			zoomOut(l);
		}
		
		iterControl.next.addEventFilter(MouseEvent.ANY, new EventHandler<MouseEvent>() {

	        @Override
	        public void handle(MouseEvent event) {
	            if (event.getEventType().equals(MouseEvent.MOUSE_PRESSED)) {
	                zoomIn(LABEL.NEXT);
	            } else {
	                zoomOut(LABEL.NEXT);
	            }

	        }
	    });
		
		iterControl.select.addEventFilter(MouseEvent.ANY, new EventHandler<MouseEvent>() {

	        @Override
	        public void handle(MouseEvent event) {
	            if (event.getEventType().equals(MouseEvent.MOUSE_PRESSED)) {
	                zoomIn(LABEL.SELECT);
	            } else {
	                zoomOut(LABEL.SELECT);
	            }

	        }
	    });
		
		iterControl.back.addEventFilter(MouseEvent.ANY, new EventHandler<MouseEvent>() {

	        @Override
	        public void handle(MouseEvent event) {
	            if (event.getEventType().equals(MouseEvent.MOUSE_PRESSED)) {
	                zoomIn(LABEL.BACK);
	            } else {
	                zoomOut(LABEL.BACK);
	            }

	        }
	    });
	}
	
	/**
	 * Shows a certain label
	 * @param l: the label to show
	 */
	public void zoomIn(LABEL l) {
		switch (l) {
		case NEXT:
			nextLable.setScaleX(1);
			break;
		case SELECT:
			selectLable.setScaleX(1);
			break;
		case BACK:
			backLable.setScaleX(1);
			break;
		}
	}
	
	/**
	 * hides a certain label
	 * @param l: the label to hide
	 */
	public void zoomOut(LABEL l) {
		switch (l) {
		case NEXT:
			nextLable.setScaleX(0);
			break;
		case SELECT:
			selectLable.setScaleX(0);
			break;
		case BACK:
			backLable.setScaleX(0);
			break;
		}
	}

}
