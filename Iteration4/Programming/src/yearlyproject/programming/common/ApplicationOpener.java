package yearlyproject.programming.common;

import javafx.application.Application;
import javafx.stage.Stage;

/**
 * Interface that enables opening a second application
 * @author Roy Svinik
 *
 */
public interface ApplicationOpener {
	
	/**
	 * Runs another application in another java window
	 * @param anotherAppClass: the class of the other application
	 * @throws Exception
	 */
	public default void runAnotherApp(Class<? extends Application> anotherAppClass) throws Exception {
	    Application app2 = anotherAppClass.newInstance(); 
	    Stage anotherStage = new Stage();
	    app2.start(anotherStage);
	}

}
