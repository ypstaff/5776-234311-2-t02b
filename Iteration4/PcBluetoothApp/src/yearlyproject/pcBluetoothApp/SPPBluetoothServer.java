package yearlyproject.pcBluetoothApp;

import java.util.ArrayList;

/**
 * A class that represents the pc bluetooth server that the input devices can
 * connect to in order to control the app's navigation system.
 * 
 * @author Lior Volin
 *
 */
public class SPPBluetoothServer {

	private SignalManager signalManager;

	public SPPBluetoothServer(Controller controller) {
		signalManager = new SignalManager(new ArrayList<Command>(), controller);
	}

	public void start() {

		(new Thread(new WaitingThread(signalManager))).start();

		(new Thread(new ServerThread(signalManager))).start();
	}
}