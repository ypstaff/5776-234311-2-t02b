package yearlyproject.pcBluetoothApp;

/**
 * The Command enum represents all the possible
 * commands in the navigation system.
 * 
 * @author Lior Volin
 *
 */
public enum Command {
	NEXT,
	SELECT,
	BACK,
	LONG_NEXT,
	LONG_SELECT,
	LONG_BACK;

	@Override
	public String toString() {
		switch (this) {
			case NEXT:
				return "next";
	
			case SELECT:
				return "select";
	
			case BACK:
				return "back";
	
			case LONG_NEXT:
				return "long next";
	
			case LONG_SELECT:
				return "long select";
	
			case LONG_BACK:
				return "long back";
	
			default:
				throw new IllegalArgumentException();
		}
	}
}
