package yearlyproject.pcBluetoothApp;

import javax.bluetooth.DiscoveryAgent;
import javax.bluetooth.LocalDevice;
import javax.bluetooth.UUID;
import javax.microedition.io.Connector;
import javax.microedition.io.StreamConnection;
import javax.microedition.io.StreamConnectionNotifier;

/**
 * A class that represents the main server thread that is responsible to listen
 * for connections and to create them.
 * 
 * @author Lior Volin
 *
 */
public class ServerThread implements Runnable {

	private SignalManager signalManager;

	public ServerThread(SignalManager signalManager) {
		this.signalManager = signalManager;
	}

	@Override
	public void run() {
		waitForConnection();
	}

	private void waitForConnection() {

		StreamConnectionNotifier notifier;

		// Setup the server to listen for connections
		try {
			LocalDevice local = LocalDevice.getLocalDevice();
			local.setDiscoverable(DiscoveryAgent.GIAC);

			notifier = (StreamConnectionNotifier) Connector
					.open(("btspp://localhost:" + (new UUID("1101", true)).toString() + ";name=SPP Bluetooth Server"));
		} catch (Exception e) {
			e.printStackTrace();
			return;
		}

		while (true) {
			try {
				// Waiting for a connection
				System.out.println("waiting for connection...");
				StreamConnection connection = notifier.acceptAndOpen();

				// Creating a thread to handle the new connection
				(new Thread(new ConnectionThread(connection, signalManager))).start();
			} catch (Exception e) {
				e.printStackTrace();
				return;
			}
		}
	}
}