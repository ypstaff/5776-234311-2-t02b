package yearlyproject.pcBluetoothApp;

/**
 * An interface that represents a controller. Every app that wants to use the
 * bluetooth server have to implement that interface in order to define the
 * behavior of the different navigation system commands in that app.
 * 
 * @author Lior Volin
 *
 */
public interface Controller {

	public void next();

	public void select();

	public void back();

	public void longNext();

	public void longSelect();

	public void longBack();
	
	public void onConnectionMade();
}