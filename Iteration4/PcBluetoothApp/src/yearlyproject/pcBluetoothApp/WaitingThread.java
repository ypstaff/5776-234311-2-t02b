package yearlyproject.pcBluetoothApp;

/**
 * A class that represents the waiting thread. This thread
 * acts as the consumer of the commands list - it waits
 * for commands and handles them.
 * 
 * @author Lior Volin
 *
 */
public class WaitingThread implements Runnable {
	
	private SignalManager signalManager;
	
	public WaitingThread(SignalManager signalManager){
		this.signalManager = signalManager;
	}
	
	@Override
	public void run() {
		while(true)
			signalManager.doWait();
	}
}
