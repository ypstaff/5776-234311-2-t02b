package yearlyproject.pcBluetoothApp;

import java.util.List;

import javafx.application.Platform;

/**
 * A class that represents the signal manager of the server's threads.
 * It manages the functionality of the producer and the consumer
 * threads in the server and their synchronized wait notify mechanism.
 * 
 * @author Lior Volin
 *
 */
public class SignalManager {

	private MonitorObject monitorObject;
	private boolean wasSignalled;
	private List<Command> commands;
	private Controller controller;

	public SignalManager(List<Command> commands, Controller controller) {
		monitorObject = new MonitorObject();
		wasSignalled = false;
		this.commands = commands;
		this.controller = controller;
	}

	// making the consumer work - waiting for commands and handling them
	public void doWait() {
		synchronized (monitorObject) {
			while (!wasSignalled)
				try {
					monitorObject.wait();
				} catch (InterruptedException e) {
				}

			// clear signal and handle the commands in the list
			wasSignalled = false;
			for (Command c : commands)
				processCommand(c);
			commands.clear();
		}
	}

	private void processCommand(Command c) {
		System.out.println(c + " signal received");

		// running the action on the FX application thread
		switch (c) {
			case NEXT:
				Platform.runLater(() -> controller.next());
				break;
	
			case SELECT:
				Platform.runLater(() -> controller.select());
				break;
	
			case BACK:
				Platform.runLater(() -> controller.back());
				break;
	
			case LONG_NEXT:
				Platform.runLater(() -> controller.longNext());
				break;
	
			case LONG_SELECT:
				Platform.runLater(() -> controller.longSelect());
				break;
	
			case LONG_BACK:
				Platform.runLater(() -> controller.longBack());
				break;
		}
	}

	// making the producer work - adding commands to the list and notifying the consumer
	public void doNotify(Command c) {
		synchronized (monitorObject) {
			wasSignalled = true;
			commands.add(c);
			monitorObject.notify();
			System.out.println(c + " signal sent");
		}
	}
	
	protected boolean getWasSignalled() {
		return wasSignalled;
	}
	
	protected List<Command> getCommandsList() {
		return commands;
	}
	
	protected Controller getController() {
		return controller;
	}
}