package yearlyproject.pcBluetoothApp;

import java.io.InputStream;

import javax.microedition.io.StreamConnection;

/**
 * A class that represents the thread that is responsible to manage a
 * connection with the server. It receives data from the client and
 * processes it. This thread also acts as the producer of the commands list.
 * 
 * @author Lior Volin
 *
 */
public class ConnectionThread implements Runnable {

	private StreamConnection connection;
	private SignalManager signalManager;

	// Constants that represent commands from the input devices
	private static final String EXIT_CMD = "-1";
	private static final String NEXT = "1";
	private static final String SELECT = "2";
	private static final String BACK = "3";
	private static final String LONG_NEXT = "4";
	private static final String LONG_SELECT = "5";
	private static final String LONG_BACK = "6";

	public ConnectionThread(StreamConnection connection, SignalManager signalManager) {
		this.connection = connection;
		this.signalManager = signalManager;
		this.signalManager.getController().onConnectionMade();
	}

	@Override
	public void run() {
		try {
			// prepare to receive data
			InputStream inputStream = connection.openInputStream();
			System.out.println("waiting for input..");

			// read data and process it
			while (true) {
				byte[] b = new byte[1];
				b[0] = (byte) inputStream.read();
				String command = new String(b);

				if (command == EXIT_CMD) {
					System.out.println("finish process");
					break;
				}

				processCommand(command);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	protected void processCommand(String command) {
		try {
			switch (command) {
				case NEXT:
					System.out.println("next received over bluetooth");
					signalManager.doNotify(Command.NEXT);
					break;
	
				case SELECT:
					System.out.println("select received over bluetooth");
					signalManager.doNotify(Command.SELECT);
					break;
	
				case BACK:
					System.out.println("back received over bluetooth");
					signalManager.doNotify(Command.BACK);
					break;
	
				case LONG_NEXT:
					System.out.println("long next received over bluetooth");
					signalManager.doNotify(Command.LONG_NEXT);
					break;
	
				case LONG_SELECT:
					System.out.println("long select received over bluetooth");
					signalManager.doNotify(Command.LONG_SELECT);
					break;
	
				case LONG_BACK:
					System.out.println("long back received over bluetooth");
					signalManager.doNotify(Command.LONG_BACK);
					break;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}