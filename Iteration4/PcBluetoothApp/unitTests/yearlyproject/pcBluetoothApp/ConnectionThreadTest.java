package yearlyproject.pcBluetoothApp;

import static org.junit.Assert.*;

import java.util.ArrayList;

import javax.microedition.io.StreamConnection;

import org.junit.Before;
import org.junit.Test;

/**
 * Testing the ConnectionThread class
 * 
 * @author Lior Volin
 *
 */
public class ConnectionThreadTest {

	private static final String NEXT = "1";
	private static final String SELECT = "2";
	private static final String BACK = "3";
	private static final String LONG_NEXT = "4";
	private static final String LONG_SELECT = "5";
	private static final String LONG_BACK = "6";
	
	private SignalManager signalManager;
	private ConnectionThread connectionThread;
	private StreamConnection connection;

	@Before
	public void setUp() throws Exception {
		signalManager = new SignalManager(new ArrayList<Command>(), new Controller() {

			@Override
			public void select() {
			}

			@Override
			public void next() {
			}

			@Override
			public void longSelect() {
			}

			@Override
			public void longNext() {
			}

			@Override
			public void longBack() {
			}

			@Override
			public void back() {
			}

			@Override
			public void onConnectionMade() {
			}
		});
		
		connectionThread = new ConnectionThread(connection, signalManager);
	}

	@Test
	public void processingNextCommandShouldMarkSignalAndAddNextToList() {
		assertFalse("after initialization wasSignalled should be false", signalManager.getWasSignalled());

		connectionThread.processCommand(NEXT);
		assertTrue("after notifying a command wasSignalled should be true", signalManager.getWasSignalled());
		assertTrue("after initialization and then notifying a next command the command list should contain only next command",
				signalManager.getCommandsList().size() == 1
						&& Command.NEXT.equals(signalManager.getCommandsList().get(0)));
	}
	
	@Test
	public void processingSelectCommandShouldMarkSignalAndAddSelectToList() {
		assertFalse("after initialization wasSignalled should be false", signalManager.getWasSignalled());

		connectionThread.processCommand(SELECT);
		assertTrue("after notifying a command wasSignalled should be true", signalManager.getWasSignalled());
		assertTrue("after initialization and then notifying a select command the command list should contain only select command",
				signalManager.getCommandsList().size() == 1
						&& Command.SELECT.equals(signalManager.getCommandsList().get(0)));
	}
	
	@Test
	public void processingBackCommandShouldMarkSignalAndAddBackToList() {
		assertFalse("after initialization wasSignalled should be false", signalManager.getWasSignalled());

		connectionThread.processCommand(BACK);
		assertTrue("after notifying a command wasSignalled should be true", signalManager.getWasSignalled());
		assertTrue("after initialization and then notifying a back command the command list should contain only back command",
				signalManager.getCommandsList().size() == 1
						&& Command.BACK.equals(signalManager.getCommandsList().get(0)));
	}
	
	@Test
	public void processingLongNextCommandShouldMarkSignalAndAddLongNextToList() {
		assertFalse("after initialization wasSignalled should be false", signalManager.getWasSignalled());

		connectionThread.processCommand(LONG_NEXT);
		assertTrue("after notifying a command wasSignalled should be true", signalManager.getWasSignalled());
		assertTrue("after initialization and then notifying a long next command the command list should contain only long next command",
				signalManager.getCommandsList().size() == 1
						&& Command.LONG_NEXT.equals(signalManager.getCommandsList().get(0)));
	}
	
	@Test
	public void processingLongSelectCommandShouldMarkSignalAndAddLongSelectToList() {
		assertFalse("after initialization wasSignalled should be false", signalManager.getWasSignalled());

		connectionThread.processCommand(LONG_SELECT);
		assertTrue("after notifying a command wasSignalled should be true", signalManager.getWasSignalled());
		assertTrue("after initialization and then notifying a long select command the command list should contain only long select command",
				signalManager.getCommandsList().size() == 1
						&& Command.LONG_SELECT.equals(signalManager.getCommandsList().get(0)));
	}
	
	@Test
	public void processingLongBackCommandShouldMarkSignalAndAddLongBackToList() {
		assertFalse("after initialization wasSignalled should be false", signalManager.getWasSignalled());

		connectionThread.processCommand(LONG_BACK);
		assertTrue("after notifying a command wasSignalled should be true", signalManager.getWasSignalled());
		assertTrue("after initialization and then notifying a long back command the command list should contain only long back command",
				signalManager.getCommandsList().size() == 1
						&& Command.LONG_BACK.equals(signalManager.getCommandsList().get(0)));
	}
}
