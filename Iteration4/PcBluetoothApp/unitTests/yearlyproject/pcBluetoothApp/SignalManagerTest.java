package yearlyproject.pcBluetoothApp;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

/**
 * Testing the SignalManager class
 * 
 * @author Lior Volin
 *
 */
public class SignalManagerTest {

	private SignalManager signalManager;

	@Before
	public void setUp() throws Exception {
		signalManager = new SignalManager(new ArrayList<Command>(), new Controller() {

			@Override
			public void select() {
			}

			@Override
			public void next() {
			}

			@Override
			public void longSelect() {
			}

			@Override
			public void longNext() {
			}

			@Override
			public void longBack() {
			}

			@Override
			public void back() {
			}

			@Override
			public void onConnectionMade() {
			}
		});
	}

	@Test
	public void nextCommandShouldMarkSignalAndAddNextToList() {
		assertFalse("after initialization wasSignalled should be false", signalManager.getWasSignalled());

		signalManager.doNotify(Command.NEXT);
		assertTrue("after notifying a command wasSignalled should be true", signalManager.getWasSignalled());
		assertTrue("after initialization and then notifying a next command the command list should contain only next command",
				signalManager.getCommandsList().size() == 1
						&& Command.NEXT.equals(signalManager.getCommandsList().get(0)));
	}
	
	@Test
	public void selectCommandShouldMarkSignalAndAddSelectToList() {
		assertFalse("after initialization wasSignalled should be false", signalManager.getWasSignalled());

		signalManager.doNotify(Command.SELECT);
		assertTrue("after notifying a command wasSignalled should be true", signalManager.getWasSignalled());
		assertTrue("after initialization and then notifying a select command the command list should contain only select command",
				signalManager.getCommandsList().size() == 1
						&& Command.SELECT.equals(signalManager.getCommandsList().get(0)));
	}
	
	@Test
	public void backCommandShouldMarkSignalAndAddBackToList() {
		assertFalse("after initialization wasSignalled should be false", signalManager.getWasSignalled());

		signalManager.doNotify(Command.BACK);
		assertTrue("after notifying a command wasSignalled should be true", signalManager.getWasSignalled());
		assertTrue("after initialization and then notifying a back command the command list should contain only back command",
				signalManager.getCommandsList().size() == 1
						&& Command.BACK.equals(signalManager.getCommandsList().get(0)));
	}
	
	@Test
	public void longNextCommandShouldMarkSignalAndAddLongNextToList() {
		assertFalse("after initialization wasSignalled should be false", signalManager.getWasSignalled());

		signalManager.doNotify(Command.LONG_NEXT);
		assertTrue("after notifying a command wasSignalled should be true", signalManager.getWasSignalled());
		assertTrue("after initialization and then notifying a long next command the command list should contain only long next command",
				signalManager.getCommandsList().size() == 1
						&& Command.LONG_NEXT.equals(signalManager.getCommandsList().get(0)));
	}
	
	@Test
	public void longSelectCommandShouldMarkSignalAndAddLongSelectToList() {
		assertFalse("after initialization wasSignalled should be false", signalManager.getWasSignalled());

		signalManager.doNotify(Command.LONG_SELECT);
		assertTrue("after notifying a command wasSignalled should be true", signalManager.getWasSignalled());
		assertTrue("after initialization and then notifying a long select command the command list should contain only long select command",
				signalManager.getCommandsList().size() == 1
						&& Command.LONG_SELECT.equals(signalManager.getCommandsList().get(0)));
	}
	
	@Test
	public void longBackCommandShouldMarkSignalAndAddLongBackToList() {
		assertFalse("after initialization wasSignalled should be false", signalManager.getWasSignalled());

		signalManager.doNotify(Command.LONG_BACK);
		assertTrue("after notifying a command wasSignalled should be true", signalManager.getWasSignalled());
		assertTrue("after initialization and then notifying a long back command the command list should contain only long back command",
				signalManager.getCommandsList().size() == 1
						&& Command.LONG_BACK.equals(signalManager.getCommandsList().get(0)));
	}
}
