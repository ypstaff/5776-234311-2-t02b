package yearlyproject.pcBluetoothApp;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * Testing the Command enum
 * 
 * @author Lior Volin
 *
 */
public class CommandTest {

	@Test
	public void nextCommandToStringShouldPrintNext() {
		assertEquals("next command to string should pring next","next",Command.NEXT.toString());
	}
	
	@Test
	public void selectCommandToStringShouldPrintSelect() {
		assertEquals("select command to string should pring select","select",Command.SELECT.toString());
	}
	
	@Test
	public void backCommandToStringShouldPrintBack() {
		assertEquals("back command to string should pring back","back",Command.BACK.toString());
	}
	
	@Test
	public void longNextCommandToStringShouldPrintLongNext() {
		assertEquals("long next command to string should pring long next","long next",Command.LONG_NEXT.toString());
	}
	
	@Test
	public void longSelectCommandToStringShouldPrintLongSelect() {
		assertEquals("long select command to string should pring long select","long select",Command.LONG_SELECT.toString());
	}
	
	@Test
	public void longBackCommandToStringShouldPrintLongBack() {
		assertEquals("long back command to string should pring long back","long back",Command.LONG_BACK.toString());
	}

}
