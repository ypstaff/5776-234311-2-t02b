To use Autoit, you need to enter cmd as an admin, and run this command from within the lib dir:
\Windows\SysWOW64\regsvr32.exe AutoItX3_x64.dll

This will register a required dll, saving you the need to actually install autoit.

To use Eclipse, 
1. download a version, preferably Mars, from here:
http://www.eclipse.org/downloads/download.php?file=/technology/epp/downloads/release/mars/2/eclipse-java-mars-2-win32-x86_64.zip

2.  extract the zip into eclipse directory in Iteration4/Programming/eclipse.
	Make sure the executable is in that directory.
	
3.  run eclipse and go to window -> Preferences -> General -> Workspace -> Worspacename . 
	and change the Worspacename  to "3 Buttons Eclipse"

In case an Eclipse executable will not be found at that location, Notepad will be used.
