function varargout = setupAcquisition(varargin)
% SETUPACQUISITION MATLAB code for setupAcquisition.fig
%      SETUPACQUISITION, by itself, creates a new SETUPACQUISITION or raises the existing
%      singleton*.
%
%      H = SETUPACQUISITION returns the handle to a new SETUPACQUISITION or the handle to
%      the existing singleton*.
%
%      SETUPACQUISITION('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in SETUPACQUISITION.M with the given input arguments.
%
%      SETUPACQUISITION('Property','Value',...) creates a new SETUPACQUISITION or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before setupAcquisition_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to setupAcquisition_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help setupAcquisition

% Last Modified by GUIDE v2.5 04-Dec-2015 12:41:37

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @setupAcquisition_OpeningFcn, ...
                   'gui_OutputFcn',  @setupAcquisition_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT
end

% --- Executes just before setupAcquisition is made visible.
function setupAcquisition_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to setupAcquisition (see VARARGIN)

% Choose default command line output for setupAcquisition
handles.output = hObject;

% build graphical objects
f = hObject;
% tab group - holds all tabs
warning('off','all');
tgroup = uitabgroup('Parent', f,'TabLocation','Top');
tabSubject = uitab('Parent', tgroup, 'Title', 'Subject Info');
tabHardware = uitab('Parent', tgroup, 'Title', 'Hardware Setup');
tabPlots = uitab('Parent', tgroup, 'Title', 'Real-time Plots');
warning('on','all');

% subject details
load subjectGroups.mat;
% name
handles.labelSubjectName = uicontrol('Parent', tabSubject, 'Style', 'text', 'String', 'Subject Name:', ...
  'HorizontalAlignment', 'left', 'units', 'normalized', 'Position', [0.05 0.80 0.2 0.1], 'fontsize', 14) ;
handles.editSubjectName = uicontrol('Parent', tabSubject, 'Style', 'edit', ...
 'units', 'normalized', 'Position', [0.25 0.85 0.2 0.05], 'fontsize', 14);
% group
handles.labelSubjectGroup = uicontrol('Parent', tabSubject, 'Style', 'text', 'String', 'Subject Group:', ...
  'HorizontalAlignment', 'left', 'units', 'normalized', 'Position', [0.05 0.6 0.2 0.1], 'fontsize', 14) ;
handles.popupSubjectGroup =  uicontrol('Parent', tabSubject, 'Style', 'popup', 'String', subjectGroups, ...
 'units', 'normalized','Position', [0.25 0.65 0.2 0.1],'fontsize', 14);
% base folder
handles.savedDataFolder = pwd();
handles.labelSubjectFolder = uicontrol('Parent', tabSubject, 'Style', 'text', 'String', 'Save Data Folder:', ...
  'HorizontalAlignment', 'left', 'units', 'normalized', 'Position', [0.05 0.4 0.2 0.1], 'fontsize', 14) ;
handles.editSubjectFolder =  uicontrol('Parent', tabSubject, 'Style', 'edit', 'String', handles.savedDataFolder, ...
 'units', 'normalized','Position', [0.25 0.45 0.5 0.1],'fontsize', 14);
handles.pushSubjectFolder =  uicontrol('Parent', tabSubject, 'Style', 'push', 'String', 'Browse...', ...
 'units', 'normalized','Position', [0.8 0.45 0.2 0.1],'fontsize', 14, ...
 'Callback', @pushSubjectFolder_Callback);


% hardware setup tab
portInfo = instrhwinfo('serial');
availablePorts = portInfo.SerialPorts;
handles.labelPortName = uicontrol('Parent', tabHardware, 'Style', 'text', 'String', 'Selected Port:', ...
  'HorizontalAlignment', 'left', 'units', 'normalized', 'Position', [0.05 0.85 0.2 0.05], 'fontsize', 14);
handles.popupPortName = uicontrol('Parent', tabHardware, 'Style', 'popup', 'String', availablePorts, ...
  'HorizontalAlignment', 'left', 'units', 'normalized', 'Position', [0.25 0.85 0.5 0.05], 'fontsize', 14, ...
  'Callback', @popupPortName_Callback);
handles.labelPortStatus = uicontrol('Parent', tabHardware, 'Style', 'text', 'String', 'Port Status:', ...
  'HorizontalAlignment', 'left', 'units', 'normalized', 'Position', [0.05 0.75 0.2 0.05], 'fontsize', 14);
handles.ledPortStatus = uicontrol('Parent', tabHardware, 'Style', 'text', 'String', 'Disconnected', ...
  'BackgroundColor','r','HorizontalAlignment', 'center', 'units', 'normalized', 'Position', [0.25 0.75 0.12 0.05], 'fontsize', 12);
handles.pushPortConnect = uicontrol('Parent', tabHardware, 'Style', 'push', 'String', 'CONNECT', ...
  'HorizontalAlignment', 'center', 'units', 'normalized', 'Position', [0.8 0.85 0.2 0.05], 'fontsize', 14, ...
  'Callback', @pushPortConnect_Callback);


% plots tab
handles.axesRawPlot = axes('Parent', tabPlots, 'Units', 'normalized', 'Position', [0.1 0.1 0.6 0.7]);
handles.togglePlot = uicontrol('Parent', tabPlots, 'Style', 'togglebutton', 'String', 'Show Data', ...
  'HorizontalAlignment', 'left', 'units', 'normalized', 'Position', [0.1 0.9 0.2 0.05], 'fontsize', 14, ...
  'Callback', @togglePlot_Callback);
handles.pushSaveRawData = uicontrol('Parent', tabPlots, 'Style', 'push', 'String', 'SAVE', ...
  'HorizontalAlignment', 'left', 'units', 'normalized', 'Position', [0.4 0.9 0.2 0.05], 'fontsize', 14, ...
  'Callback', @pushSaveRawData_Callback);
handles.toggleTrigger = uicontrol('Parent', tabPlots, 'Style', 'togglebutton', 'String', 'Run Trigger', ...
  'HorizontalAlignment', 'left', 'units', 'normalized', 'Position', [0.7 0.9 0.2 0.05], 'fontsize', 14, ...
  'Callback', @toggleTrigger_Callback);
handles.ledTriggerStatus = uicontrol('Parent', tabPlots, 'Style', 'text', 'String', '', ...
  'BackgroundColor',[0.925 0.925 0.925],'HorizontalAlignment', 'center', 'units', 'normalized', 'Position', [0.7 0.8 0.2 0.05], 'fontsize', 12);
handles.sliderThreshold = uicontrol('Parent', tabPlots, 'Style', 'slider', 'Min', 0, 'Max', 5, 'Value', 2.5, ...
  'units', 'normalized', 'Position', [0.8 0.4 0.15 0.05], 'fontsize', 12, ...
  'Callback', @sliderThreshold_Callback);
handles.editThreshold = uicontrol('Parent', tabPlots, 'Style', 'edit', 'String', '2.5', ...
  'HorizontalAlignment', 'center', 'units', 'normalized', 'Position', [0.85 0.45 0.05 0.05], 'fontsize', 14, ...
  'Callback', @editThreshold_Callback);
handles.labelThreshold = uicontrol('Parent', tabPlots, 'Style', 'text', 'String', 'Threshold:', ...
  'HorizontalAlignment', 'center', 'units', 'normalized', 'Position', [0.825 0.5 0.1 0.05], 'fontsize', 14);

  
% Update handles structure
guidata(hObject, handles);

% UIWAIT makes setupAcquisition wait for user response (see UIRESUME)
% uiwait(handles.figure1);
end

% --- Outputs from this function are returned to the command line.
function varargout = setupAcquisition_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;
end

function popupPortName_Callback(hObject, eventdata, handles)
    
    handles = guidata(gcf);
    
    selection = get(hObject, 'value');
    options = get(hObject, 'String');
    handles.selectedPort = options{selection};
    
    guidata(handles.figure1, handles);
end

function pushPortConnect_Callback(hObject, eventdata, handles)

handles = guidata(gcf);
currentStatus = get(handles.ledPortStatus, 'String');
switch currentStatus
    case 'Disconnected'
        % connect to serial port
        try
            handles.serialObject = setupSerialObject(handles.selectedPort);
            
            set(handles.ledPortStatus, 'BackgroundColor', 'g', 'String', 'Connected');
            set(handles.pushPortConnect, 'String', 'DISCONNECT');
        catch ME
            fprintf('Could not connect to port %s\n', handles.selectedPort);
            fprintf(ME.message);
        end
        
        % data initialization
        handles = initRawData(handles);
        
    case 'Connected'
        try
            closeSerialObject(handles.serialObject);
            
            set(handles.ledPortStatus, 'BackgroundColor', 'r', 'String', 'Disconnected');
            set(handles.pushPortConnect, 'String', 'CONNECT');
        catch ME
            fprintf('Could not close the port %s\n', handles.selectedPort);
            fprintf(ME.message);
        end
end

guidata(handles.figure1, handles);

end    


function togglePlot_Callback(hObject, eventdata, handles)

handles = guidata(gcf);
toggleOn = get(hObject, 'value');
threshold = 2.5;

% TODO - check port is connected before reading data

if toggleOn
    handles = initRawData(handles);
    set(handles.togglePlot, 'String', 'STOP');
    handles.t0 = tic;
else
    set(handles.togglePlot, 'String', 'Show Data');
end

% read and plot data as long as toggle button is 'on'
while toggleOn
    voltage = readRaw(handles.serialObject);
%     voltage = rand()*5; % [TOM SIMULATION]
    if isempty(voltage)
        continue;
    end
    % get time of current sample
    t = toc(handles.t0);
    
    % determine point-process value
    currentThreshold = str2double(get(handles.editThreshold, 'String'));
    point = (voltage > currentThreshold);
%     pointProcess = [pointProcess point];
%    fprintf('%d\n',point);

%   read bit and if classified as a kind of press send it
    handleBit(point);

    % update data structures
    handles = updateRawData(handles, voltage, t);
    
    % plot data
    plot(handles.axesRawPlot, handles.rawTimeRunning, handles.rawDataRunning);
    ylim([0 5]);
    xlim([handles.rawTimeRunning(1) handles.rawTimeRunning(end)]);
    xlabel('time');
    ylabel('Voltage');
    drawnow;
    
    % check whether toggle button is still set to 'on'
    toggleOn = get(hObject, 'value');
end

% save data and init vectors
% saveRawData(handles);
% handles = initRawData(handles);

% update handles structure
guidata(handles.figure1, handles);

end


function pushSubjectFolder_Callback(hObject, eventdata, handles)

handles = guidata(gcf);
selectedFolder = uigetdir(handles.savedDataFolder);
handles.savedDataFolder = selectedFolder;
set(handles.editSubjectFolder, 'String', handles.savedDataFolder);
guidata(handles.figure1, handles);


end


function pushSaveRawData_Callback(hObject, eventdata, handles)

handles = guidata(gcf);

subjectName = get(handles.editSubjectName, 'String');
groupSelection = get(handles.popupSubjectGroup, 'value');
groupOptions = get(handles.popupSubjectGroup, 'String');
subjectGroup = groupOptions{groupSelection};

saveRawData(subjectName, subjectGroup, handles);

guidata(handles.figure1, handles);

end


function toggleTrigger_Callback(hObject, eventdata, handles)

handles = guidata(gcf);
toggleOn = get(hObject, 'value');
triggerDuration = 2;

if toggleOn
%     handles = initTriggerData(handles);
    triggerTimes = [];
    set(handles.toggleTrigger, 'String', 'Stop trigger');
    set(handles.ledTriggerStatus, 'BackgroundColor', 'y');
    set(handles.ledTriggerStatus, 'String', 'WAIT');

    if ~isfield(handles, 't0')
        handles.t0 = tic;
    end
else
    set(handles.toggleTrigger, 'String', 'Run trigger');
    set(handles.ledTriggerStatus, 'BackgroundColor', [0.925 0.925 0.925]);
    set(handles.ledTriggerStatus, 'String', '');
end

while toggleOn
    
    % randomize interval
    interval = generateTriggerInterval(handles);
    
    % wait the interval
%     pause(interval);
    t1 = toc(handles.t0);
    t2 = toc(handles.t0);
    while t2 < (t1+interval)
        t2 = toc(handles.t0);
    end
    % show trigger and record time
    set(handles.ledTriggerStatus, 'BackgroundColor', 'g');
    set(handles.ledTriggerStatus, 'String', 'GO');
    tTrigger = toc(handles.t0);
%     drawnow;
    % keep on for 2 secs.
%     pause(triggerDuration);
    tDur = toc(handles.t0);
    while tDur < (tTrigger + triggerDuration)
        tDur = toc(handles.t0);
    end
    
    set(handles.ledTriggerStatus, 'BackgroundColor', 'y');
    set(handles.ledTriggerStatus, 'String', 'WAIT');
%     drawnow;
    % update times
    triggerTimes = [triggerTimes tTrigger];
%     handles = updateTriggerTimes(handles, tTrigger);

     
    % check whether toggle button is still set to 'on'
    toggleOn = get(hObject, 'value');
end

% update handles structure
if exist('triggerTimes','var')
    handles.triggerTimes = triggerTimes;
end

guidata(handles.figure1, handles);

end


function sliderThreshold_Callback(hObject, eventdata, handles)

handles = guidata(gcf);

currentThreshold = get(hObject, 'Value');

handles.threshold = currentThreshold;
set(handles.editThreshold, 'String', num2str(currentThreshold));

guidata(handles.figure1, handles);

end


function editThreshold_Callback(hObject, eventdata, handles)

handles = guidata(gcf);

currentThreshold = str2double(get(hObject, 'String'));

set(handles.sliderThreshold, 'Value', currentThreshold);

guidata(handles.figure1, handles);

end

    

