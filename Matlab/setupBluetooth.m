% this function use fopen do not fopen by yourself
function setupBluetooth = setupBluetooth(blueTooth)
global count; % global variable is shared acroos functions
global long_press_delt;
global bt;
count=0;
long_press_delt = 0;
setLowerBounds(3,10);
bt = blueTooth;
fopen(bt);
end