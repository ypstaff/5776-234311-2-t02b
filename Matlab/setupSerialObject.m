function obj = setupSerialObject(portname, baudrate)

if nargin < 2
    baudrate = 28800;
end

% delete existing serial objects to this port
delete(instrfind({'Port'},{portname}));

% USING THE ARDUINO PACKAGE
% obj = arduino(portname, 'uno');


% OR USE THE SERIAL OBJECT
% setup new serial object
obj = serial(portname, 'baudrate', baudrate);
% set(obj, 'Terminator', 'LF');
% set(obj, 'Timeout', 100);
% set(obj,'DataBits',8);
% set(obj,'StopBits',1);
% set(obj,'Parity','none');
fopen(obj);