function handles = initRawData(handles)

handles.bufferLen = 100;
handles.rawData = [];
handles.rawDataRunning = zeros(1, handles.bufferLen);
handles.rawTime = [];
handles.rawTimeRunning = zeros(1, handles.bufferLen);
