function setLowerBounds = setLowerBounds(single,long)
global single_press_lower_bound ;
global long_press_lower_bound ;
single_press_lower_bound = single;
long_press_lower_bound = long;
end