function interval = generateTriggerInterval(handles)

% randomize an interval between 1-5 secs.
intMax = 5;
intMin = 1;
interval = rand() * intMax + intMin;



