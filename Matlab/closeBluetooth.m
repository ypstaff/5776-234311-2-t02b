% this function close the bluetooth connection
function closeBluetooth = closeBluetooth()
global bt;
fclose(bt);
clear('bt');
end