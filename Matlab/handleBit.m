%   @author: kfir
%   called in initRawData (init the values of count and long_press_delt
%   called in setUpAcquisition to get the bit of input from the arduino
function handleBit = handleBit(bit)
global count; % global variable is shared acroos functions
global long_press_delt;
global bt;
global single_press_lower_bound ;
global long_press_lower_bound ;
if (bit == 1)
    count = count + 1;
    if (count >= long_press_lower_bound & long_press_delt == 0)
        handleLongPress();
        long_press_delt = 1; %true
    end
elseif (bit ~= 0)
        error('bit isnt 0 or 1');
else % bit == 0
    if (count >= single_press_lower_bound & long_press_delt == 0)
        handleSinglePress();
    end
    count = 0;
    long_press_delt = 0;
end
end

function handleLongPress = handleLongPress()
global bt;
fprintf(bt,'long press\n');
fprintf('long press\n');
end

function handleSinglePress = handleSinglePress()
global bt;
fprintf(bt,'single press\n');
fprintf('single press\n');
end



