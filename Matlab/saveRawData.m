function savePath = saveRawData(subjectName, subjectGroup, handles)

savePath = fullfile(handles.savedDataFolder, sprintf('%s-%s-%s',subjectName, subjectGroup, strrep(datestr(now),' ','_')));

rawData = [handles.rawTime ; handles.rawData];

save(savePath, 'rawData');

msgbox(sprintf('Raw data was saved to:\n %s', savePath));