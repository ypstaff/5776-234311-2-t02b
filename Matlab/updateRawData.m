function handles = updateRawData(handles, voltage, t)

handles.rawData = [handles.rawData voltage];
handles.rawTime = [handles.rawTime t];

handles.rawDataRunning = [handles.rawDataRunning(2:end), voltage];
handles.rawTimeRunning = [handles.rawTimeRunning(2:end), t];