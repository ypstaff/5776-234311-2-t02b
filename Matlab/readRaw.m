function voltage = readRaw(serialObj)

% USING THE MATLAB SERIAL OBJECT
maxVoltage = 5;
maxInt = 1023;
command = 'R';
fprintf(serialObj,command);  
% Read value returned via Serial communication 
% output = fread(serialObj);
[output,~,errmsg] = fscanf(serialObj);

if ~isempty(errmsg)
    disp errmsg;
    voltage = [];
else
    voltage = str2double(output)*(maxVoltage/maxInt);
end

% voltage = str2double(output)/maxInt;
% output = fscanf(serialObj,'%f');

% USING THE ARDUINO PACKAGE
% pinNo = 0;
% output = arduinoControl.readVoltage(pinNo);