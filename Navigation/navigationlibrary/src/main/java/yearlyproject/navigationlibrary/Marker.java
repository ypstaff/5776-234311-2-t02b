package yearlyproject.navigationlibrary;

import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.view.View;

/**
 * A class for marking and unmarking Views
 * @author Roy Svinik
 */

public class Marker {

    private int color;

    /**
     * Default constructor. color is yellow
     */
    public Marker() {
        this.color = Color.YELLOW;
    }

    /**
     * Customized color constructor
     * @param color - color of your choice
     */
    public Marker(int color) {
        this.color = color;
    }

    /**
     * Marks the view with the selected color by changing it's background color
     * @param view - the View designated for marking
     */
    public void mark(View view) {
        view.setBackgroundColor(this.color);
    }

    /**
     * Unmarks the view by changing its background to its original background
     * @param view - the View designated for unmarking
     * @param origBackground - the View's original background
     */
    public void unmark(View view, Drawable origBackground) {
        view.setBackground(origBackground);
    }

}
