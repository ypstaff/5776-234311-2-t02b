package yearlyproject.navigationlibrary;

/**
 * A class simulating an automaton for detecting emergency call combination
 * @author Roy Svinik
 */
public class EmergencyCallDetector {

    private State state = State.I;

    /**
     * Indicates that 'Next' was selected
     * @return returns true if emergency call has been detected
     */
    public boolean next() {
        state = state == State.I ? State.N : State.I;
        return false;
    }

    /**
     * Indicates that 'Select' was selected
     * @return returns true if emergency call has been detected
     */
    public boolean select() {
        state = state != State.N ? State.I : State.NS;
        return false;
    }

    /**
     * Indicates that 'Back' was selected
     * @return returns true if emergency call has been detected
     */
    public boolean back() {
    	boolean $ = state == State.NS;
    	state = State.I;
        return $;
    }

    /**
     * Inits the state machine
     */
    public void init() {
        state = State.I;
    }

    /**
     * States of the detector:
     * I: initial state, no part of the combination occurred
     * N: Next was long pressed
     * NS: Next and select were long pressed 
     */
    private enum State {
        I, N, NS
    }

}
