package yearlyproject.navigationlibrary;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.content.res.ResourcesCompat;
import android.view.View;
import android.widget.EditText;

/**
 * A class for circling and uncircling Views
 * @author Roy Svinik
 */
public class Circler {

    Context context;
    Drawable border;
    //special border for edit texts because they have different behaviour
    Drawable etBorder;

    /**
     * Default constructor. border is light with red lines
     * @param context
     */
    public Circler(Context context) {
        this.context = context;
        border = ResourcesCompat.getDrawable(context.getResources(), R.drawable.border, null);
        etBorder = ResourcesCompat.getDrawable(context.getResources(), R.drawable.edit_text_border, null);
    }

    /**
     * Customized border constructor
     * @param context
     * @param border - border of your choice
     * @param etBorder - border of your choice for edit texts
     */
    public Circler(Context context, Drawable border, Drawable etBorder) {
        this.context = context;
        this.border = border;
        this.etBorder = etBorder;
    }

    /**
     * Circles the view by swapping its background
     * @param v - the View designated for circling
     */
    public void circle(View v) {
        v.setBackground(v instanceof EditText ? etBorder : border);
    }

    /**
     * Uncircles the view by changing its background to its original background
     * @param v - the View designated for uncircling
     * @param origBackground - the View's original background
     */
    public void uncircle(View v, Drawable origBackground) {
        v.setBackground(origBackground);
    }

}
