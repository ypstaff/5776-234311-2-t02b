package yearlyproject.navigationlibrary;

import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.support.design.widget.FloatingActionButton;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import java.lang.Override;

/**
 * A custom activity to enable Navigation and Emergency call capabilities
 * @author Roy Svinik
 */
public abstract class EmergencyCallActivity extends AppCompatActivity
        implements View.OnClickListener, View.OnLongClickListener {

    private static final String NEXT = "1";
    private static final String SELECT = "2";
    private static final String BACK = "3";
    private static final String LONG_NEXT = "4";
    private static final String LONG_SELECT = "5";
    private static final String LONG_BACK = "6";

    final int PERMISSIONS_REQUEST_CALL_PHONE = 1;

    Intent dialIntent;

    //add your activity global group here
    protected IterableGroup group;
    protected FloatingActionButton fab_left, fab_right, fab_middle;
    EmergencyCallDetector detector = new EmergencyCallDetector();

    @Override
    public void onClick(View v) {
        detector.init();

        if (v.getId() == R.id.fab_left)
			group.iterate();
		else if (v.getId() == R.id.fab_middle)
			group.select();
		else if (v.getId() == R.id.fab_right && group.back())
			onBackPressed();
    }

    @Override
    public boolean onLongClick(View v) {
        if (v.getId() == R.id.fab_left)
			detector.next();
		else if (v.getId() == R.id.fab_middle)
			detector.select();
		else if (v.getId() == R.id.fab_right && detector.back())
			try {
				startActivity((new Intent(
						"yearlyproject.phonedialer.ACTION_EMERGENCY")));
			} catch (ActivityNotFoundException anfe) {
				Toast.makeText(
						this,
						"Can't find the custom phone app on your device. So, it is impossible to make the emergency call.",
						Toast.LENGTH_LONG).show();
			} catch (Exception e) {
				Toast.makeText(this, "Can't make the emergency call.",
						Toast.LENGTH_SHORT).show();
			}
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSIONS_REQUEST_CALL_PHONE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
					startActivity(dialIntent);
				else
					Toast.makeText(
							this,
							"Can't make a phone call without the requested permission",
							Toast.LENGTH_SHORT).show();
            }
        }
    }

    //bluetooth messages receiver
    private final BroadcastReceiver btMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if ("BluetoothConnector.BT_MESSAGE_RECEIVED".equals(action)) {
                //String msg = intent.getData().toString();
                String msg = intent.getStringExtra("msg");
                performAction(msg.split("\\u0000")[0]);
            }
        }
    };

    public void performAction(final String s) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(s.equals(NEXT))
                    fab_left.callOnClick();
                if(s.equals(SELECT))
                    fab_middle.callOnClick();
                if(s.equals(BACK))
                    fab_right.callOnClick();
                if(s.equals(LONG_NEXT))
                    fab_left.performLongClick();
                if(s.equals(LONG_SELECT))
                    fab_middle.performLongClick();
                if(s.equals(LONG_BACK))
                    fab_right.performLongClick();
            }
        });
    }

    protected void unregisterBTReceiver() {
        try {
            unregisterReceiver(btMessageReceiver);
        }catch (IllegalArgumentException e) {}
    }

    protected void registerBTReceiver() {
        registerReceiver(btMessageReceiver, new IntentFilter("BluetoothConnector.BT_MESSAGE_RECEIVED"));
    }
    @Override
    protected void onPause() {
        unregisterBTReceiver();
        super.onPause();
    }

    @Override
    protected void onResume(){
        super.onResume();
        registerBTReceiver();
    }

    protected void showSoftKey(EditText et){
        unregisterBTReceiver();
        final InputMethodManager inputMethodManager = (InputMethodManager)
                this.getSystemService(this.INPUT_METHOD_SERVICE);
        inputMethodManager.showSoftInput(et, InputMethodManager.SHOW_IMPLICIT);
    }

    protected void hideSoftKey(){
        registerBTReceiver();
    }

}
