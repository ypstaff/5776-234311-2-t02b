package yearlyproject.navigationlibrary;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

/**
 * A class for iterating over a group of a singular View
 * @author Roy Svinik
 */
public class IterableSingleView implements IterableGroup {

    View view;
    Drawable origBackground;
    Marker marker;
    Circler circler;

    /**
     * Common use constructor
     * @param view - the view in the group
     * @param context
     */
    public IterableSingleView(View view, Context context) {
        this.view = view;
        this.origBackground = view.getBackground();
        marker = new Marker();
        circler = new Circler(context);
    }

    /**
     * Customized constructor
     * @param view - the view in the group
     * @param context
     * @param border - customized border for circling views
     * @param etBorder - customized edit text border for circling views
     * @param markColor - customized marking color
     */
    public IterableSingleView(View view, Context context, Drawable border,
                              Drawable etBorder, int markColor) {
        this(view, context);
        marker = new Marker(markColor);
        circler = new Circler(context, border, etBorder);
    }

    @Override
    public boolean start() {
        markGroup();
        return false;
    }

    @Override
    public boolean iterate() {
        return false;
    }

    @Override
    public void select() {
        view.performClick();
    }

    @Override
    public boolean back() {
        return true;
    }

    @Override
    public void markGroup() {
        marker.mark(view);
    }

    @Override
    public void unmarkGroup() {
        marker.unmark(view, origBackground);
    }

    @Override
    public void markAllChilds() {
        markGroup();
    }

    @Override
    public void unmarkAllChilds() {
        unmarkGroup();
    }

    @Override
    public void circleGroup() {
        circler.circle(view);
    }

    @Override
    public void uncircleGroup() {
        circler.uncircle(view, origBackground);
    }

    @Override
    public void circleAllChilds() {
        circleGroup();
    }

    @Override
    public void uncircleAllChilds() {
        uncircleGroup();
    }

    @Override
    public List<View> getSelectedViews() {
        List<View> $ = new ArrayList<>();
        $.add(view);
        return $;
    }

    @Override
    public View getGroupView() {
        return view;
    }

    @Override
    public void stop() {
        unmarkGroup();
        uncircleGroup();
    }
}
