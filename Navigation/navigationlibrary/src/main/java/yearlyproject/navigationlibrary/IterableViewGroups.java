package yearlyproject.navigationlibrary;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

/**
 * A class for iterating over a group of groups
 * @author Roy Svinik
 */
public class IterableViewGroups implements IterableGroup{

    List<IterableGroup > groups;
    Integer selected;
    Boolean groupMode = true;
    View groupView;
    Drawable origBackground;
    Marker marker;
    Circler circler;

    /**
     * Common use constructor
     * @param groups - the groups to iterate on
     * @param groupView - the View containing all groups
     * @param context
     */
    public IterableViewGroups(List<IterableGroup> groups, View groupView, Context context) {
        this.groups = groups;
        this.groupView = groupView;
        origBackground = groupView.getBackground();
        marker = new Marker();
        circler = new Circler(context);
    }

    /**
     * Customized constructor
     * @param groups - the groups to iterate on
     * @param groupView - the View containing all groups
     * @param context
     * @param border - customized border for circling views
     * @param etBorder - customized edit text border for circling views
     * @param markColor - customized marking color
     */
    public IterableViewGroups(List<IterableGroup> groups, View groupView, Context context, Drawable border,
                             Drawable etBorder, int markColor) {
        this(groups, groupView, context);
        marker = new Marker(markColor);
        circler = new Circler(context, border, etBorder);
    }

    @Override
    public boolean start() {
        if(groups.size() > 0) {
            selected = 0;
            unmarkAllChilds();
            for (IterableGroup group : groups) {
                group.unmarkAllChilds();
                group.uncircleAllChilds();
            }
            circleAllChilds();
            groups.get(selected).markGroup();
        }
        return true;
    }

    @Override
    public boolean iterate() {
        if (!groupMode) {
			if (!groups.get(selected).iterate()) {
				back();
				iterate();
			}
		} else {
			groups.get(selected).unmarkGroup();
			groups.get(selected).circleGroup();
			selected = (selected + 1) % groups.size();
			groups.get(selected).markGroup();
		}
        return true;
    }

    @Override
    public void select() {
        if (!groupMode)
			groups.get(selected).select();
		else {
			uncircleAllChilds();
			if (!groups.get(selected).start()) {
				circleAllChilds();
				groups.get(selected).markGroup();
				groups.get(selected).select();
			}
			groupMode = false;
		}
    }

    @Override
    public boolean back() {
		if (groupMode)
			return true;
		if (groups.get(selected).back()) {
			circleAllChilds();
            groups.get(selected).uncircleAllChilds();
			groups.get(selected).markGroup();
			groupMode = true;
		}
		return false;
	}

    @Override
    public void markGroup() {
        marker.mark(groupView);
    }

    @Override
    public void unmarkGroup() {
        marker.unmark(groupView, origBackground);
    }

    @Override
    public void markAllChilds() {
        for (IterableGroup group: groups)
			group.markGroup();
    }

    @Override
    public void unmarkAllChilds() {
        for (IterableGroup group: groups)
			group.unmarkGroup();
    }

    @Override
    public void circleGroup() {
        circler.circle(groupView);
    }

    @Override
    public void uncircleGroup() {
        circler.uncircle(groupView, origBackground);
    }

    @Override
    public void circleAllChilds() {
        for (IterableGroup group: groups)
			group.circleGroup();
    }

    @Override
    public void uncircleAllChilds() {
        for (IterableGroup group: groups)
			group.uncircleGroup();
    }

    @Override
    public List<View> getSelectedViews() {
        List<View> $ = new ArrayList<>();
        if(groupMode)
			$.add(groups.get(selected).getGroupView());
		else
			$.addAll(groups.get(selected).getSelectedViews());
        return $;
    }

    @Override
    public View getGroupView() {
        return groupView;
    }

    @Override
    public void stop() {
        unmarkGroup();
        uncircleGroup();
        for(IterableGroup son : groups)
			son.stop();
    }

}
