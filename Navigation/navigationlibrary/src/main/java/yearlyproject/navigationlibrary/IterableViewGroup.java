package yearlyproject.navigationlibrary;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.View;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * A class for iterating over a group of Views
 * @author Roy Svinik
 */

public class IterableViewGroup implements IterableGroup {

    List<View> elements;
    //the index of the selected element
    Integer selected;
    Map<View, Drawable> origBackgrounds;
    //the view containing the group elements
    View groupView;
    Marker marker;
    Circler circler;

    /**
     * Common use constructor
     * @param views - the views in the group
     * @param groupView - the view containing all group elements
     * @param context
     */
    public IterableViewGroup(Collection<View> views, View groupView, Context context) {
        elements = new ArrayList<View>(views);
        origBackgrounds = new HashMap<>();
        for (View v: elements)
			origBackgrounds.put(v, v.getBackground());
        this.groupView = groupView;
        origBackgrounds.put(groupView, groupView.getBackground());
        marker = new Marker();
        circler = new Circler(context);
    }

    /**
     * Customized constructor
     * @param views - the views in the group
     * @param groupView - the view containing all group elements
     * @param context
     * @param border - customized border for circling views
     * @param etBorder - customized edit text border for circling views
     * @param markColor - customized marking color
     */
    public IterableViewGroup(List<View> views, View groupView, Context context, Drawable border,
                              Drawable etBorder, int markColor) {
        this(views, groupView, context);
        marker = new Marker(markColor);
        circler = new Circler(context, border, etBorder);
    }

    public boolean start() {
        if(elements.size() > 0 ) {
            unmarkAllChilds();
            circleAllChilds();
            selected = 0;
            circler.uncircle(elements.get(selected), origBackgrounds.get(elements.get(selected)));
            marker.mark(elements.get(selected));
        }
        return true;
    }

    public boolean iterate() {
        marker.unmark(elements.get(selected), origBackgrounds.get(elements.get(selected)));
        circler.circle(elements.get(selected));
        selected = (selected + 1) % elements.size();
        marker.mark(elements.get(selected));
        return true;
    }

    /**
     * invokes the onClick listener of the selected view
     * Note: EditView onClick listener does NOT open keyboard
     */
    public void select() {
        elements.get(selected).performClick();
    }

    @Override
    public boolean back() {
        unmarkAllChilds();
        uncircleAllChilds();
        return true;
    }

    @Override
    public void markGroup() {
        marker.mark(groupView);
    }

    @Override
    public void unmarkGroup() {
        marker.unmark(groupView, origBackgrounds.get(groupView));
    }


    public void markAllChilds() {
        for (View v: elements)
			marker.mark(v);
    }

    public void unmarkAllChilds() {
        for (View v: elements)
			marker.unmark(v, origBackgrounds.get(v));
    }

    @Override
    public void circleGroup() {
        circler.circle(groupView);
    }

    @Override
    public void uncircleGroup() {
        circler.uncircle(groupView, origBackgrounds.get(groupView));
    }

    @Override
    public void circleAllChilds() {
        for (View v: elements)
			circler.circle(v);
    }

    @Override
    public void uncircleAllChilds() {
        for (View v: elements)
			circler.uncircle(v, origBackgrounds.get(v));
    }

    @Override
    public List<View> getSelectedViews() {
        List<View> $ = new ArrayList<>();
        $.add(elements.get(selected));
        return $;
    }

    @Override
    public View getGroupView() {
        return groupView;
    }

    @Override
    public void stop() {
        unmarkGroup();
        uncircleGroup();
        unmarkAllChilds();
        uncircleAllChilds();
    }

}
