package yearlyproject.navigationlibrary;

import android.content.Context;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.ListView;
import java.util.ArrayList;
import java.util.List;

/**
 * A class for iterating over a ListView
 * Note: This class does not support lists with rows that can change location
 * @author Roy Svinik
 */
public class IterableListView  implements IterableGroup {

    ListView list;
    Integer localPos;
    Integer globalPos;
    Boolean groupMode;
    Drawable origBackground;
    Marker marker;
    Circler circler;

    /**
     * Common use constructor
     * @param list - the ListView to iterate on
     * @param context
     */
    public IterableListView(ListView list, Context context) {
        this.list = list;
        origBackground = list.getBackground();
        marker = new Marker();
        circler = new Circler(context);
    }

    /**
     * Customized constructor
     * @param list - the ListView to iterate on
     * @param context
     * @param border - customized border for circling views
     * @param etBorder - customized edit text border for circling views
     * @param markColor - customized marking color
     */
    public IterableListView(ListView list, Context context, Drawable border,
                            Drawable etBorder, int markColor) {
        this(list, context);
        marker = new Marker(markColor);
        circler = new Circler(context, border, etBorder);
    }

    @Override
    public boolean start() {
        groupMode = true;
        markAllChilds();
        globalPos = localPos = 0;
		list.setSelection(localPos);
        //if all rows in the list fits the screen, automatically go into selecting rows
        if(list.getLastVisiblePosition() - list.getFirstVisiblePosition() + 1 == list.getCount() &&
                list.getCount() > 0 &&
                isViewVisible(list.getChildAt(0)) &&
                isViewVisible(list.getChildAt(list.getLastVisiblePosition())))
			select();
        return true;
    }

    @Override
    public boolean iterate() {
        if(list.getCount() == 0) return false;
        if(groupMode) {
            globalPos = (globalPos + list.getLastVisiblePosition() - list.getFirstVisiblePosition());
            if(globalPos >= list.getCount())
                globalPos = 0;
            list.setSelection(globalPos);
        } else {
            circle(list.getChildAt(localPos));
            localPos = (localPos + 1) %
                    (list.getLastVisiblePosition() - list.getFirstVisiblePosition() + 1);
            while(!isViewVisible(list.getChildAt(localPos)))
				localPos = (localPos + 1)
						% (list.getLastVisiblePosition()
								- list.getFirstVisiblePosition() + 1);
            mark(list.getChildAt(localPos));
        }
        return true;
    }

    @Override
    public void select() {
        if (!groupMode) {
			int pos = list.getFirstVisiblePosition() + localPos;
			list.performItemClick(list.getAdapter().getView(pos, null, null),
					pos, list.getAdapter().getItemId(pos));
		} else {
			if (list.getCount() == 0)
				return;
			unmarkAllChilds();
			localPos = 0;
			if (!isViewVisible(list.getChildAt(0)))
				localPos++;
			circleAllChilds();
			mark(list.getChildAt(localPos));
			groupMode = false;
		}
    }

    @Override
    public boolean back() {
		if (groupMode)
			return true;
		groupMode = true;
		unmark(list.getChildAt(localPos));
		localPos = 0;
		uncircleAllChilds();
		mark(list);
		return (list.getLastVisiblePosition() - list.getFirstVisiblePosition()
				+ 1 == list.getCount()
				&& list.getCount() > 0 && isViewVisible(list.getChildAt(0)) && isViewVisible(list
					.getChildAt(list.getLastVisiblePosition())));
	}

    @Override
    public void markGroup() {
        mark(list);
    }

    @Override
    public void unmarkGroup() {
        unmark(list);
    }

    @Override
    public void markAllChilds() {
        mark(list);
    }

    @Override
    public void unmarkAllChilds() {
        unmark(list);
    }

    @Override
    public void circleGroup() {
        circle(list);
    }

    @Override
    public void uncircleGroup() {
        uncircle(list);
    }

    @Override
    public void circleAllChilds() {
        for(int i = 0; i<= list.getLastVisiblePosition() - list.getFirstVisiblePosition(); ++i)
			if (isViewVisible(list.getChildAt(i)))
				circle(list.getChildAt(i));
    }

    @Override
    public void uncircleAllChilds() {
        for(int i = 0; i<= list.getLastVisiblePosition() - list.getFirstVisiblePosition(); ++i)
			uncircle(list.getChildAt(i));
    }

    @Override
    public List<View> getSelectedViews() {
        List<View> $ = new ArrayList<>();
        $.add(groupMode ? list : list.getChildAt(localPos));
        return $;
    }

    @Override
    public View getGroupView() {
        return list;
    }

    private void mark(View v) {
        marker.mark(v);
    }

    private void unmark(View v) {
        marker.unmark(v, origBackground);
    }

    private void circle(View v) {
        circler.circle(v);
    }

    private void uncircle(View v) {
        circler.uncircle(v, origBackground);
    }

    private boolean isViewVisible(View v) {
		if(v.getVisibility() != View.VISIBLE)
			return false;
        Rect scrollBounds = new Rect();
        list.getDrawingRect(scrollBounds);

        float top = v.getY();
        return (scrollBounds.top <= top && scrollBounds.bottom >= top + v.getHeight());
    }


    @Override
    public void stop() {
        unmarkGroup();
        uncircleGroup();
    }
}
