package yearlyproject.navigationlibrary;

import android.view.View;

import java.util.List;

/**
 * An interface for dictating iterating protocol over all sorts of Views
 * @author Roy Svinik
 */

public interface IterableGroup {

    /**
     * starts the iteration, marks first element in the group
     * @return false if there is a single element in the group, otherwise returns true
     */
    boolean start();

    /**
     * stops the iteration. using stop isn't the common case for this interface,
     * use it when you want to change the group dynamically
     */
    void stop();

    /**
     * performs an iteration over the group.
     * @return true if iteration is supported, otherwise returns false
     * an example of when you return false: if the group has only 1 element
     */
    boolean iterate();

    /**
     * performs a selection over the current group/element
     * either enters a sub group or invokes the selected element
     */
    void select();

    /**
     * performs back. either pass control to the outer group or performs back press.
     * @return true if control is passed to the father group, otherwise returns false
     */
    boolean back();

    /**
     *  marks the entire group
     */
    void markGroup();

    /**
     *  unmarks the entire group
     */
    void unmarkGroup();

    /**
     *  marks all elements in the group
     */
    void markAllChilds();

    /**
     * unmarks all elements in the group
     */
    void unmarkAllChilds();

    /**
     * circles the entire group
     */
    void circleGroup();

    /**
     * uncircles the entire group
     */
    void uncircleGroup();

    /**
     * circles all the elements in the group
     */
    void circleAllChilds();

    /**
     * uncircles all the elements in the group
     */
    void uncircleAllChilds();

    /**
     * @return list of the views currently selected in the group
     */
    List<View> getSelectedViews();

    /**
     * @return the view containing the group
     */
    View getGroupView();

}
