package yearlyproject.brainiac;

import android.support.design.widget.FloatingActionButton;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import yearlyproject.navigationlibrary.EmergencyCallActivity;
import yearlyproject.navigationlibrary.IterableGroup;
import yearlyproject.navigationlibrary.IterableSingleView;
import yearlyproject.navigationlibrary.IterableViewGroup;
import yearlyproject.navigationlibrary.IterableViewGroups;

/**
 * @author Lior Volin
 */
public class MainActivity extends EmergencyCallActivity implements View.OnClickListener {

    ImageButton ib_messages;
    ImageButton ib_phone;
    ImageButton ib_rss;
    ImageButton ib_media_player;
    ImageButton ib_flying_fred;
    ImageButton ib_snake;
    ImageButton ib_bluetooth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViews();

        setListeners();

        setThreeButtonsNavigation();
    }

    private void setThreeButtonsNavigation() {
        //Setting the 3 buttons navigation system for the activity
        fab_left = (FloatingActionButton) findViewById(R.id.fab_left);
        fab_right = (FloatingActionButton) findViewById(R.id.fab_right);
        fab_middle = (FloatingActionButton) findViewById(R.id.fab_middle);

        //Setting the action buttons to be semi transparent
        fab_left.setAlpha((float) 0.5);
        fab_middle.setAlpha((float) 0.5);
        fab_right.setAlpha((float) 0.5);

        //Setting the listeners for the navigation buttons
        fab_left.setOnClickListener(this);
        fab_right.setOnClickListener(this);
        fab_middle.setOnClickListener(this);

        fab_left.setOnLongClickListener(this);
        fab_right.setOnLongClickListener(this);
        fab_middle.setOnLongClickListener(this);

        group = createNavigationGroup();
        group.start();
    }

    private IterableViewGroups createNavigationGroup() {

        IterableGroup row1_group = createOneRowGroup(ib_phone, ib_messages, R.id.ll_row1);
        IterableGroup row2_group = createOneRowGroup(ib_rss, ib_media_player, R.id.ll_row2);
        IterableGroup row3_group = createOneRowGroup(ib_snake, ib_flying_fred, R.id.ll_row3);
        IterableGroup row4_group = new IterableSingleView(ib_bluetooth, this);

        //The general group
        List<IterableGroup> all_rows_list = new ArrayList<>();
        all_rows_list.add(row1_group);
        all_rows_list.add(row2_group);
        all_rows_list.add(row3_group);
        all_rows_list.add(row4_group);
        return new IterableViewGroups(all_rows_list, findViewById(R.id.ll_all_rows), this);
    }

    IterableGroup createOneRowGroup(View view1, View view2, int row_id) {
        List<View> row_list = new ArrayList<>();
        row_list.add(view1);
        row_list.add(view2);

        return new IterableViewGroup(row_list, findViewById(row_id), this);
    }

    //Setting this activity as the on click listener for all the image buttons
    private void setListeners() {
        ib_messages.setOnClickListener(this);
        ib_phone.setOnClickListener(this);
        ib_rss.setOnClickListener(this);
        ib_media_player.setOnClickListener(this);
        ib_snake.setOnClickListener(this);
        ib_flying_fred.setOnClickListener(this);
        ib_bluetooth.setOnClickListener(this);
    }

    //Getting all the image buttons
    private void findViews() {
        ib_messages = (ImageButton) findViewById(R.id.ib_messages);
        ib_phone = (ImageButton) findViewById(R.id.ib_phone);
        ib_rss = (ImageButton) findViewById(R.id.ib_rss);
        ib_media_player = (ImageButton) findViewById(R.id.ib_media_player);
        ib_snake = (ImageButton) findViewById(R.id.ib_snake);
        ib_flying_fred = (ImageButton) findViewById(R.id.ib_flying_fred);
        ib_bluetooth = (ImageButton) findViewById(R.id.ib_bluetooth);
    }

    @Override
    public void onClick(View v) {
        //For the navigation buttons
        super.onClick(v);

        switch (v.getId()) {

            case R.id.ib_messages:
                startAppForPackage("yearlyproject.messages");
                break;

            case R.id.ib_phone:
                startAppForPackage("yearlyproject.phonedialer");
                break;

            case R.id.ib_rss:
                startAppForPackage("yearlyproject.rss");
                break;

            case R.id.ib_media_player:
                startAppForPackage("yearlyproject.mediaplayer");
                break;

            case R.id.ib_snake:
                startAppForPackage("yearlyproject.snake");
                break;

            case R.id.ib_flying_fred:
                startAppForPackage("yearlyproject.flyingfred");
                break;

            case R.id.ib_bluetooth:
                startAppForPackage("yearlyproject.bluetooth");
                break;
        }
    }

    private void startAppForPackage(String packageName) {
        try {
            startActivity(getPackageManager().getLaunchIntentForPackage(packageName));
        } catch (Exception e) {
            Toast.makeText(this, "Can't find the application on your device", Toast.LENGTH_LONG).show();
        }
    }
}
