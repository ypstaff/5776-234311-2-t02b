package yearlyproject.mediaplayer.VideoPlayer.Listeners;

import android.view.View;
import android.widget.Button;
import android.widget.VideoView;

/**
 * Created by Sergey on 23/12/2015.
 */
public class PauseAndPlayVideoOnClickListener implements View.OnClickListener{

    private VideoView videoView;
    public PauseAndPlayVideoOnClickListener(VideoView videoView) {
        this.videoView=videoView;
    }

    @Override
    public void onClick(View v) {
        if(videoView.isPlaying()) {
            videoView.pause();
            ((Button)v).setText("Play");
        } else {
            videoView.start();
            ((Button)v).setText("Pause");
        }
    }
}
