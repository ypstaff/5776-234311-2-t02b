package yearlyproject.mediaplayer.VideoPlayer.Listeners;

import android.view.View;
import android.widget.TextView;
import android.widget.VideoView;

import yearlyproject.mediaplayer.VideoPlayer.VideoItemView;

/**
 * Created by Sergey on 09/01/2016.
 */
public class UpdateVideoTotalTimeOnClickListener implements View.OnClickListener {
    private TextView textView;

    public UpdateVideoTotalTimeOnClickListener(TextView targetTextview) {
        this.textView=targetTextview;
    }

    @Override
    public void onClick(View v) {
        int duration=Integer.parseInt(((VideoItemView) v).getVideo().getDuration())/1000;
        textView.setText(" / " + (duration / 60) + " : " + (duration % 60));
    }
}
