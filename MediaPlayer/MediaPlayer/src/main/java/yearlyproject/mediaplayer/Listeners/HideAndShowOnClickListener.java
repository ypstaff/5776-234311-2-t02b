package yearlyproject.mediaplayer.VideoPlayer.Listeners;

import android.view.View;

/**
 * Created by Sergey on 23/12/2015.
 */
public class HideAndShowOnClickListener implements View.OnClickListener {
    private View changedView;

    public HideAndShowOnClickListener(View changedView) {
        this.changedView=changedView;
    }

    @Override
    public void onClick(View v) {
        changedView
				.setVisibility(changedView.getVisibility() != View.GONE ? View.GONE
						: View.VISIBLE);
    }
}
