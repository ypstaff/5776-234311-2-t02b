package yearlyproject.mediaplayer.VideoPlayer.Listeners;

import android.view.View;
import android.view.View.OnClickListener;

import java.util.List;

/**
 * Created by Sergey on 09/01/2016.
 */
public class CompositeOnClickListener implements OnClickListener {

    private List<OnClickListener> actions;

    public CompositeOnClickListener(List<OnClickListener> listeners) {
        this.actions=listeners;
    }

    @Override
    public void onClick(View v) {
        for(OnClickListener $ : actions)
			$.onClick(v);
    }
}
