package yearlyproject.mediaplayer.VideoPlayer.Listeners;

import android.view.View;
import android.widget.ProgressBar;
import android.widget.VideoView;

import yearlyproject.mediaplayer.VideoPlayer.VideoItemView;

/**
 * Created by Sergey on 09/01/2016.
 */
public class UpdateProgressBarOnClickListener implements View.OnClickListener {
    private ProgressBar pb;
    public UpdateProgressBarOnClickListener(ProgressBar target) {
        pb=target;
    }

    @Override
    public void onClick(View v) {
        this.pb.setMax((Integer.parseInt(((VideoItemView) v).getVideo()
				.getDuration()) / 1000));
    }
}
