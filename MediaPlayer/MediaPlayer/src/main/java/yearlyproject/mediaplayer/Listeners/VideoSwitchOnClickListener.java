package yearlyproject.mediaplayer.VideoPlayer.Listeners;

import android.content.Context;
import android.net.Uri;
import android.view.View;
import android.widget.VideoView;

import yearlyproject.mediaplayer.VideoPlayer.VideoItemView;

/**
 * Created by Sergey on 31/12/2015.
 */
public class VideoSwitchOnClickListener implements View.OnClickListener {

    private Context context;
    private VideoView videoView;
    public VideoSwitchOnClickListener(final Context context,VideoView videoView) {
        this.context=context;
        this.videoView=videoView;
    }

    @Override
    public void onClick(View v) {
        videoView.setVideoURI(Uri.parse(((VideoItemView)v).getVideo().getPath()));
        videoView.seekTo(0);
    }
}
