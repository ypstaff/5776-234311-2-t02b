package yearlyproject.mediaplayer;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

import yearlyproject.mediaplayer.MusicPlayer.MusicPlayerActivity;
import yearlyproject.mediaplayer.VideoPlayer.VideoActivity;
import yearlyproject.navigationlibrary.EmergencyCallActivity;
import yearlyproject.navigationlibrary.IterableGroup;
import yearlyproject.navigationlibrary.IterableViewGroup;


public class MainActivity extends EmergencyCallActivity implements View.OnClickListener {
    private IterableGroup m_iterableGroup;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        prepareIterableGroupsAndConnectListeners();
    }

    private void prepareIterableGroupsAndConnectListeners(){

        fab_left = (FloatingActionButton) findViewById(R.id.fab_left);
        fab_right = (FloatingActionButton) findViewById(R.id.fab_right);
        fab_middle = (FloatingActionButton) findViewById(R.id.fab_middle);
        findViewById(R.id.music_tab_button).setOnClickListener(this);
        findViewById(R.id.video_tab_button).setOnClickListener(this);
        List<View> buttonList = new ArrayList<>();
        buttonList.add(findViewById(R.id.music_tab_button));
        buttonList.add(findViewById(R.id.video_tab_button));
        m_iterableGroup= new IterableViewGroup(buttonList, findViewById(R.id.main_tab), this);
        m_iterableGroup.start();
        initTouchListeners();
    }


    protected void initTouchListeners() {
        fab_left.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                m_iterableGroup.iterate();
            }
        });
        fab_right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (m_iterableGroup.back())
                    onBackPressed();
            }
        });
        fab_middle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                m_iterableGroup.select();
            }
        });
        fab_left.setOnLongClickListener(this);
        fab_right.setOnLongClickListener(this);
        fab_middle.setOnLongClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.music_tab_button:
                Intent musicActivityOpenIntent= new Intent(this, MusicPlayerActivity.class);
                startActivity(musicActivityOpenIntent);
                break;
            case R.id.video_tab_button:
                Intent videoActivityOpenIntent = new Intent(this, VideoActivity.class);
                startActivity(videoActivityOpenIntent);
        }
    }

}
