package yearlyproject.mediaplayer.DataBaseBridge;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.BaseColumns;
import android.provider.MediaStore.*;
import android.provider.MediaStore.Audio.AlbumColumns;

import java.util.List;
import java.util.Set;

import yearlyproject.mediaplayer.Pojos.MediaPlayerSong;

/**
 *
 * @author roman - Major refactoring from previous project
 * @class MusicDB- audio files DB parser
 */

public  final class MusicDBParser extends DBParser {
    final static Uri URI = Audio.Media.EXTERNAL_CONTENT_URI;
    final static String _ID = BaseColumns._ID;
    final static String SONG_NAME = MediaColumns.TITLE;
    final static String PATH = MediaColumns.DATA;
    final static String ALBUM_NAME = AlbumColumns.ALBUM;
    final static String ARTIST_NAME = AlbumColumns.ARTIST;


    public Set<String> getAllArtists(Context context){
        Cursor cursor=getCursor(context, "", null);
        Set<String> $ = getColumnFromCursor(cursor, ARTIST_NAME);
        cursor.close();
        return $;
    }

    public Set<String> getAllAlbums(Context context){
        Cursor cursor=getCursor(context, "", null);
        Set<String> $ = getColumnFromCursor(cursor, ALBUM_NAME);
        cursor.close();
        return $;
    }

    public  List<MediaPlayerSong> getAllSongs(Context context){
        return getAllItemsBy(context, "", null);
    }

    public  List<MediaPlayerSong> getAllSongsByAlbum(Context context, String album){
        return getAllItemsBy(context, ALBUM_NAME + "=?",  new String[]{album});
    }

    public  List<MediaPlayerSong> getAllSongsByArtist(Context context, String artist){
        return getAllItemsBy(context, ARTIST_NAME + "=?", new String[]{artist});
    }


    //Private funcs-------------------------------------------------------------------------------

    @Override
    protected Cursor getCursor(Context context, String selection, String[] selectionArgs) {
        String whereMusic = Audio.Media.IS_MUSIC + "!=0";
        final ContentResolver cr = context.getContentResolver();
        final String[] columns = { _ID, SONG_NAME, ALBUM_NAME, ARTIST_NAME, PATH };
        selection = selection=="" ? whereMusic : whereMusic + " and " +selection;
        Cursor $ = cr.query(URI, columns, selection ,selectionArgs ,SONG_NAME);
        return $;
    }

    @Override
    protected ItemProducer<MediaPlayerSong> getItemProducer() {
        return new ItemProducer<MediaPlayerSong>() {
			@Override
			public MediaPlayerSong Produce(Cursor cursor) {
				final String songPath = cursor.getString(cursor
						.getColumnIndex(PATH));
				String songName = cursor.getString(cursor
						.getColumnIndex(SONG_NAME));
				final String artist = cursor.getString(cursor
						.getColumnIndex(ARTIST_NAME));
				final String album = cursor.getString(cursor
						.getColumnIndex(ALBUM_NAME));
				return new MediaPlayerSong(songName, artist, album, songPath);
			}
		};
    }
}
