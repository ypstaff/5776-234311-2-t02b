package yearlyproject.mediaplayer.DataBaseBridge;

import android.database.Cursor;

/**
 * Created by Sergey on 12/12/2015.
 */
public interface ItemProducer<T> {
    public T Produce(Cursor c);
}
