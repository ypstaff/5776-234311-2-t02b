package yearlyproject.mediaplayer.DataBaseBridge;

import android.content.Context;
import android.database.Cursor;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Sergey on 12/12/2015.
 */
public abstract class DBParser {

    protected final <T> List<T> getAllItemsBy(final Context c, String Selection, String[] selectionArgs) {
        List<T> $ = new ArrayList<T>();
        ItemProducer<T> itemProducer= getItemProducer();
        final Cursor cursor = getCursor(c, Selection, selectionArgs);
        for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext())
			$.add(itemProducer.Produce(cursor));
        return $;
    }

    protected final Set<String> getColumnFromCursor(final Cursor c, String column) {
        final HashSet<String> $ = new HashSet<String>();
        for (c.moveToFirst(); !c.isAfterLast(); c.moveToNext())
			$.add(c.getString(c.getColumnIndex(column)));
        return $;
    }

    protected abstract Cursor getCursor(final Context c, String selection, String[] selectionArgs);
    protected abstract <T> ItemProducer<T> getItemProducer();
}
