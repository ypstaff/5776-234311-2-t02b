package yearlyproject.mediaplayer.DataBaseBridge;


import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.provider.MediaStore;

import android.net.Uri;

import java.util.List;

import yearlyproject.mediaplayer.Pojos.MediaPlayerVideo;

/**
 * Created by Sergey on 12/12/2015.
 */
public class VideoDBParser extends DBParser {
    final static Uri extern_uri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
    final static String videoID = MediaStore.Video.Media._ID;
    final static String videoAlbum = MediaStore.Video.Media.ALBUM;  //text
    final static String videoArtist = MediaStore.Video.Media.ARTIST; //text
    final static String videoDate = MediaStore.Video.Media.DATE_TAKEN; //integer
    final static String videoDescription = MediaStore.Video.Media.DESCRIPTION; //text
    final static String videoDuration = MediaStore.Video.Media.DURATION; //text
    final static String videoResolution = MediaStore.Video.Media.RESOLUTION; //text
    final static String videoTitle = MediaStore.Video.Media.TITLE; //text
    final static String videoPath = MediaStore.Video.Media.DATA; //data stream - text for us


    public List<MediaPlayerVideo> getAllVideos(final Context c) {
        return this.getAllItemsBy(c, "",null);
    }

    public List<MediaPlayerVideo> getAllVideosByName(final Context c,String name) {
        return this.getAllItemsBy(c,videoTitle+" LIKE '%"+name+"%'",null);
    }

    @Override
    protected Cursor getCursor(Context context, String selection,String[] selectionArgs) {
        final ContentResolver cr = context.getContentResolver();
        final String[] columns = { videoPath, videoAlbum, videoArtist, videoDate,
                videoDescription, videoDuration, videoResolution, videoTitle, videoID};
        Cursor $ = cr.query(extern_uri, columns,selection ,null ,videoTitle);
        return $;
    }

    @Override
    protected ItemProducer<MediaPlayerVideo> getItemProducer() {
        return new ItemProducer<MediaPlayerVideo>() {
			@Override
			public MediaPlayerVideo Produce(Cursor c) {
				return new MediaPlayerVideo(c.getString(c.getColumnIndex(videoID)),
						c.getString(c.getColumnIndex(videoAlbum)),
						c.getString(c.getColumnIndex(videoArtist)),
						String.valueOf(c.getInt(c.getColumnIndex(videoDate))),
						c.getString(c.getColumnIndex(videoDescription)),
						c.getString(c.getColumnIndex(videoDuration)),
						c.getString(c.getColumnIndex(videoResolution)),
						c.getString(c.getColumnIndex(videoTitle)),
						c.getString(c.getColumnIndex(videoPath)));
			}
		};
    }
}
