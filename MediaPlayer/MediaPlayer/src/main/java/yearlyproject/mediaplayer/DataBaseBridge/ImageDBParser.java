package yearlyproject.mediaplayer.DataBaseBridge;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;

import java.util.List;

import yearlyproject.mediaplayer.Pojos.MediaPlayerImage;

/**
 * Created by Sergey on 25/12/2015.
 */
public class ImageDBParser extends DBParser {
    final static Uri URI = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
    static final String path = MediaStore.Images.ImageColumns.DATA;
    static final String date = MediaStore.Images.ImageColumns.DATE_TAKEN;
    static final String description = MediaStore.Images.ImageColumns.DESCRIPTION;

    public List<MediaPlayerImage> getAllImages(final Context context) {
        return this.getAllItemsBy(context,"",null);
    }

    @Override
    protected Cursor getCursor(Context context, String selection, String[] selectionArgs) {
        final ContentResolver cr = context.getContentResolver();
        final String[] columns = { path,date,description };
        Cursor cursor = cr.query(URI, columns, selection ,selectionArgs ,date);
        return cursor;
    }

    @Override
    protected ItemProducer<MediaPlayerImage> getItemProducer() {
        return new ItemProducer<MediaPlayerImage>() {
            @Override
            public MediaPlayerImage Produce(Cursor cursor) {
                final String path_ = cursor.getString(cursor.getColumnIndex(path));
                final String date_ = cursor.getString(cursor.getColumnIndex(date));
                final String description_ = cursor.getString(cursor.getColumnIndex(description));
                return new MediaPlayerImage(path_,date_,description_);
            }
        };
    }
}
