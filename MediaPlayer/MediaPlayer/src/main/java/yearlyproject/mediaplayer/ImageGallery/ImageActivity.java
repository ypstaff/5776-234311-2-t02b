package yearlyproject.mediaplayer.ImageGallery;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import yearlyproject.mediaplayer.DataBaseBridge.ImageDBParser;
import yearlyproject.mediaplayer.R;
import yearlyproject.mediaplayer.Pojos.MediaPlayerImage;
import yearlyproject.navigationlibrary.EmergencyCallActivity;

/**
 * Created by roman on 12/25/2015.
 */
public class ImageActivity extends EmergencyCallActivity{

    List<MediaPlayerImage> m_currentImages;
    GridView mainGridView;
    ImageDBParser m_imageDBParser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image);
        m_imageDBParser= new ImageDBParser();
        mainGridView=(GridView) findViewById(R.id.grid_view);
        //initiateViews();
        //initTouchListeners();
        //prepareIterableGroups();
        updateImages();
        showImagesGrid();
    }

    private void updateImages() {
        m_currentImages = m_imageDBParser.getAllImages(this);
    }

    private void showImagesGrid() {
        ImageAdapter imageAdapter = new ImageAdapter(ImageActivity.this, R.layout.image_item, m_currentImages);
        mainGridView.setAdapter(imageAdapter);
        mainGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                MediaPlayerImage imageItem = (MediaPlayerImage) parent.getItemAtPosition(position);
                //Create intent
                Intent intent = new Intent(ImageActivity.this, DetailsActivity.class);
                intent.putExtra("imagePath", imageItem.getPath());
                //Start details activity
                startActivity(intent);
            }
        });
        if (m_currentImages == null || m_currentImages.size() == 0)
            showToast(getString(R.string.no_songs_found));
        else
            Collections.sort(m_currentImages, new Comparator<MediaPlayerImage>() {
                @Override
                public int compare(final MediaPlayerImage lhs, final MediaPlayerImage rhs) {
                    return lhs.getDate().compareTo(rhs.getDate());
                }
            });
    }



    /**
     * Toasts a message on screen.
     *
     * @param msg
     *            - the messaged to be displayed.
     */
    private void showToast(final String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }




}
