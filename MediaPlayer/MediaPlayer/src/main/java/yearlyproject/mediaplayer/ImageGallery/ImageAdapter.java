package yearlyproject.mediaplayer.ImageGallery;


import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.lang.ref.WeakReference;
import java.util.List;

import yearlyproject.mediaplayer.R;
import yearlyproject.mediaplayer.Pojos.MediaPlayerImage;


/**
 * Created by roman on 25-12-15.
 *
 */


public class ImageAdapter extends ArrayAdapter<MediaPlayerImage> {

    List<MediaPlayerImage> images;
    int itemRow;

    // View lookup cache
    private static class ImageViewHolder {
        TextView text;
        TextView timestamp;
        ImageView image;
        ProgressBar progress;
        int position;
    }

    public ImageAdapter(Context context, int row, List<MediaPlayerImage> images) {
        super(context, row, images);
        this.itemRow = row;
        this.images = images;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {


        ImageViewHolder imageViewHolder;

        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(itemRow, parent, false);

            imageViewHolder = new ImageViewHolder();
            imageViewHolder.image = (ImageView) convertView.findViewById(R.id.image);

            convertView.setTag(imageViewHolder);
        } else {
            imageViewHolder = (ImageViewHolder) convertView.getTag();
        }


        MediaPlayerImage imageItem = getItem(position);
        imageViewHolder.image.setImageBitmap(null);
        imageViewHolder.image.setTag(imageItem.getPath());
        ImageGridHandler handler = new ImageGridHandler(imageViewHolder.image, imageItem);
        handler.execute();
        return convertView;
    }
}

     class ImageGridHandler extends AsyncTask<String, Void, Bitmap>{
        private final WeakReference<ImageView> imageViewReference;
        MediaPlayerImage imageItem;

        public ImageGridHandler(ImageView img, MediaPlayerImage imageItem){
            imageViewReference = new WeakReference<ImageView>(img);
            this.imageItem=imageItem;
        }

        @Override
        protected Bitmap doInBackground(String... params) {
            return imageItem.getBitmap(true);
        }

        @Override
        protected void onPostExecute(Bitmap result) {
            final ImageView imageView = imageViewReference.get();
            if(imageItem.getPath()!=imageView.getTag()){
                return;
            }
            imageView.setImageBitmap(result);
        }
    }
