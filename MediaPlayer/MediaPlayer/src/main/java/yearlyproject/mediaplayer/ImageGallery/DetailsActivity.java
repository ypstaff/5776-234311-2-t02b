package yearlyproject.mediaplayer.ImageGallery;

import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.widget.ImageView;

import yearlyproject.mediaplayer.R;
import yearlyproject.mediaplayer.Pojos.MediaPlayerImage;
import yearlyproject.navigationlibrary.EmergencyCallActivity;

/**
 * Created by roman on 12/26/2015.
 */
public class DetailsActivity extends EmergencyCallActivity{

    MediaPlayerImage m_mediaPlayerImage;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.image_details_activity);
        ((ImageView) findViewById(R.id.details_activity_image))
				.setImageBitmap(BitmapFactory.decodeFile(getIntent()
						.getStringExtra("imagePath")));
    }

}
