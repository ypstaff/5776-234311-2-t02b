package yearlyproject.mediaplayer.Pojos;

/**
 * Created by roman on 12/8/2015.
 */
public class MediaPlayerSong {
    private String name;
    private String artist;
    private String path;
    private String album;


    public String getAlbum() {
        return album;
    }

    public String getPath() {
        return path;
    }

    public String getArtist() {
        return artist;
    }

    public String getName() {
        return name;
    }

    public MediaPlayerSong(String name, String artist, String album, String path){
        this.name=name;
        this.artist=artist;
        this.path=path;
        this.album=album;
    }

}
