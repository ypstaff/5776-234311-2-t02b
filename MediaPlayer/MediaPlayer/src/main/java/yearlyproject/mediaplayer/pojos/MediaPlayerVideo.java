package yearlyproject.mediaplayer.Pojos;

import java.io.Serializable;

/**
 * Created by Sergey on 12/12/2015.
 */
public class MediaPlayerVideo implements Serializable{
      String ID;
      String Album;
      String Artist;
      String Date;
      String Description;
      String Duration;
      String Resolution;
      String Title;
      String path;

    public MediaPlayerVideo(String ID,String Album,String Artist,String Date,String Description,
                            String Duration,String Resolution,String Title,
                            String path) {
          this.ID=ID;
          this.Album=Album;
          this.Artist=Artist;
          this.Date=Date;
          this.Description=Description;
          this.Duration=Duration;
          this.Resolution=Resolution;
          this.Title=Title;
          this.path=path;
    }

      public String getID() {
            return ID;
      }

      public String getAlbum() {
            return Album;
      }

      public String getArtist() {
            return Artist;
      }

      public String getDate() {
            return Date;
      }

      public String getDescription() {
            return Description;
      }

      public String getDuration() {
            return Duration;
      }

      public String getResolution() {
            return Resolution;
      }

      public String getTitle() {
            return Title;
      }

      public String getPath() {
            return path;
      }

}
