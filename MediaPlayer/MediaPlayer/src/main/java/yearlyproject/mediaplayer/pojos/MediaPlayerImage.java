package yearlyproject.mediaplayer.Pojos;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.File;
import java.io.Serializable;

/**
 * Created by Sergey on 25/12/2015.
 */
public class MediaPlayerImage implements Serializable {

    private String path;
    private String date;
    private String description;
    private Bitmap m_bitmap;
    public MediaPlayerImage(String path, String date, String description) {
        this.path=path;
        this.date=date;
        this.description=description;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Bitmap getBitmap(boolean isThumbnail){
        //return decodeSampledBitmapFromFile(getPath(), height, width);
        if(!isThumbnail)
			return BitmapFactory.decodeFile(getPath());
        if(m_bitmap!= null)
			return m_bitmap;
        Bitmap $ = null;
        File imgFile = new  File(getPath());
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = options.inDither = false;
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
        options.inSampleSize = 15;
        options.inPurgeable = true;
        if(imgFile.exists())
			$ = BitmapFactory.decodeFile(imgFile.getAbsolutePath(), options);
        return $;
    }

}
