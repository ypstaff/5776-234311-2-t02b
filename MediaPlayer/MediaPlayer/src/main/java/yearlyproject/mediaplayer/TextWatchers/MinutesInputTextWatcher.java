package yearlyproject.mediaplayer.VideoPlayer.TextWatchers;

import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.VideoView;

import yearlyproject.mediaplayer.Pojos.MediaPlayerVideo;


/**
 * Created by Sergey on 11/01/2016.
 */
public class MinutesInputTextWatcher implements TextWatcher {

    private VideoView videoView;
    private ProgressBar progressBar;
    private EditText secondsET;

    public MinutesInputTextWatcher(VideoView videoView,EditText secondsET,
                                   ProgressBar progressBar) {
        this.videoView=videoView;
        this.progressBar=progressBar;
        this.secondsET=secondsET;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        String sValue = s.toString();
        int minutes;
        if (sValue.equals(""))
            minutes = 0;
        else {
            minutes = Integer.parseInt(sValue);
        }
        int currSeconds = progressBar.getProgress();
        String secondsValue=secondsET.getText().toString();
        if(secondsValue.equals("")) {
            currSeconds = 0;
        }
        videoView.seekTo((currSeconds % 60 + minutes * 60) * 1000);
    }
}
