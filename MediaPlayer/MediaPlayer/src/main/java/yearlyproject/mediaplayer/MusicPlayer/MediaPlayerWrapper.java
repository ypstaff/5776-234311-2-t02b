package yearlyproject.mediaplayer.MusicPlayer;

import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import yearlyproject.mediaplayer.Pojos.MediaPlayerSong;

enum PlayerStatus {
    PLAY, PAUSE, STOP
};

/**
 * 
 * @author roman
 * Class from previous project - Major refactoring
 * @class this class is used to play a List of songs, and it is singleton.
 */
public class MediaPlayerWrapper implements OnCompletionListener {

    final static int NEXT_VALUE = 1;
    final static int PREV_VALUE = -1;
    private static List<MediaPlayerSong> songsList = new ArrayList<MediaPlayerSong>();
    private static int currentSongIndex = 0;
    private static PlayerStatus m_playerStatus = PlayerStatus.STOP;
    private static MediaPlayer mediaPlayer;
    protected static MediaPlayerEventListener mediaEventListener;
    private static MediaPlayerWrapper mediaPlayerWrapperInstance = null;
    static boolean shuffle = false;


    //Init Static class------------------------------------------------------------------------

    /**
     * created new instance of the this singleton class.
     * 
     * @param list
     */
    public static MediaPlayerWrapper MediaPlayerCreate(final List<MediaPlayerSong> list, MediaPlayerEventListener listener) {
        if (mediaPlayerWrapperInstance == null) {
            mediaPlayerWrapperInstance = new MediaPlayerWrapper(list);
            mediaEventListener= listener;
        }
        else if (!list.equals(mediaPlayerWrapperInstance.songsList)) {
            mediaPlayerWrapperInstance.stop();
            mediaPlayerWrapperInstance = new MediaPlayerWrapper(list);
        }
        return mediaPlayerWrapperInstance;
    }

    /**
     * @return returns the instance of the singlton.
     */
    public static MediaPlayerWrapper getInstance() {
        return mediaPlayerWrapperInstance;
    }

    public  void MediaPlayerDestroyInstance() {
        if (mediaPlayerWrapperInstance == null)
            return;
        mediaPlayerWrapperInstance.stop();
        mediaPlayerWrapperInstance = null;
    }


    /**
     * Handler which calls when the current song is ended.
     */
    @Override
    public void onCompletion(final MediaPlayer p) {
        playNext();
    }

    //API for playing songs----------------------------------------------------------------------
    /**
     * Plays the song, if paused, continues
     */
    public  void play() {
        if (songsList.size() == 0)
            return;
        MediaPlayerSong current_song = songsList.get(currentSongIndex);
        if (m_playerStatus == PlayerStatus.STOP) {
            playAudio(current_song.getPath());
            m_playerStatus= PlayerStatus.PLAY;
            mediaEventListener.playerPlaying(current_song);
        } else if (m_playerStatus != PlayerStatus.PAUSE) {
			if (m_playerStatus == PlayerStatus.PLAY)
				return;
		} else {
			mediaPlayer.start();
			m_playerStatus = m_playerStatus.PLAY;
			mediaEventListener.playerPlaying(current_song);
		}
    }

    /**
     * stops the playing song.
     */
    public  void stop() {
		if (mediaPlayer == null || songsList.size() == 0
				|| m_playerStatus == PlayerStatus.STOP)
			return;
		try {
			mediaPlayer.stop();
			mediaPlayer = new MediaPlayer();
			m_playerStatus = PlayerStatus.STOP;
			mediaEventListener.playerStopped(songsList.get(currentSongIndex));
		} catch (final Exception e) {
			e.printStackTrace();
		}
	}

    /**
     * pauses the current playing song.
     */
    public  void pause() {
        if (songsList.size() == 0)
            return;
        try {
            mediaPlayer.pause();
            m_playerStatus = PlayerStatus.PAUSE;
            mediaEventListener.playerPaused(songsList.get(currentSongIndex));
        } catch (final Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * plays the next song in the list .
     */
    public  void playNext() {
        moveIndex(NEXT_VALUE);
    }

    /**
     * plays the previous song in the list.
     */
    public  void playPrev() {
        moveIndex(PREV_VALUE);
    }

    public  void setCurrentSongIndex(int id){
        if(id==currentSongIndex) {
			if (m_playerStatus == PlayerStatus.PAUSE) {
                MediaPlayerSong current_song = songsList.get(currentSongIndex);
                mediaEventListener.playerPlaying(current_song);
				mediaPlayer.start();
				return;
			}
			if (m_playerStatus == PlayerStatus.PLAY)
				return;
		}
        currentSongIndex= id;
        mediaPlayer.reset();
        m_playerStatus = PlayerStatus.STOP;
        play();
    }

    public void togglePlayPause(){
        if(m_playerStatus==PlayerStatus.PAUSE){
            play();
            return;
        }
        if (m_playerStatus==PlayerStatus.PLAY)
			pause();
    }

    public void setShuffle(){
        shuffle=true;
    }

    public void unsetShuffle(){
        shuffle=false;
    }

    public int getCurrentTime() {
        return mediaPlayer.getCurrentPosition();
    }

    // Private Funcs =====================================================================


    /**
     * private constructor for the singleton;
     *
     * @param list
     *            is a list of music to play.
     */
    private MediaPlayerWrapper(final List<MediaPlayerSong> list) {
        mediaPlayer = new MediaPlayer();
        mediaPlayer.setOnCompletionListener(this);
        songsList = list;
    }

    /**
     * handles the inner index of the current playing song.
     *
     * @param value
     *            if NEXT_VALUE +1 for the inner index if PREV_VALUE -1 for the
     *            inner index and it makes the list circuit.
     */
    private void stepSong(final int value) {
        // if +1 then next (need to return to first if got to the end).
        // if -1 then previous (need to go to last if got to the first).
        if (value == NEXT_VALUE)
			if (currentSongIndex + 1 == songsList.size())
				currentSongIndex = 0;
			else
				++currentSongIndex;
		else {
			if (value != PREV_VALUE)
				return;
			if (currentSongIndex + PREV_VALUE != -1)
				--currentSongIndex;
			else
				currentSongIndex = songsList.size() - 1;
		}
    }

    private void moveIndex(final int to) {
        if (songsList.size() == 0)
            return;
        if (shuffle)
            shuffle();
        else {
            stepSong(to);
            mediaPlayer.reset();
            m_playerStatus = PlayerStatus.STOP;
            play();
        }
    }

    /**
     * plays the song in the path.
     *
     * @param path
     *            is path of the song to play.
     */
    private static void playAudio(final String path) {
        try {
            mediaPlayer.setDataSource(path);
            mediaPlayer.prepare();
            mediaPlayer.start();
        } catch (final Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * shuffles a new index and plays it.
     */
    private void shuffle() {
        if (songsList.size() == 0)
            return;
        final Random generator = new Random();
        currentSongIndex = generator.nextInt(songsList.size());
        mediaPlayer.reset();
        m_playerStatus = PlayerStatus.STOP;
        play();
    }


}
