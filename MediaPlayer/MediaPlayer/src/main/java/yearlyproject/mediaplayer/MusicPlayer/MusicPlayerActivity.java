package yearlyproject.mediaplayer.MusicPlayer;

import android.content.Context;
import android.media.AudioManager;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.*;
import java.util.*;
import java.util.concurrent.TimeUnit;
import yearlyproject.mediaplayer.DataBaseBridge.MusicDBParser;
import yearlyproject.mediaplayer.Pojos.MediaPlayerSong;
import yearlyproject.mediaplayer.R;
import yearlyproject.navigationlibrary.*;

/**
 * Created by roman on 12/11/2015.
 * MusicPlayerActivity
 * Encapsulates Artists Tab, Albums Tab, and All Songs Tab
 * Allows to play songs, navigate between them, and search for songs
 */
public class MusicPlayerActivity extends EmergencyCallActivity implements OnClickListener, MediaPlayerEventListener {
    MediaPlayerWrapper mediaPlayerWrapper;
    SongAdapter songAdapter;
    Timer m_timer;
    List<MediaPlayerSong> m_currentSongs, m_originalCurrentSongs;
    ListView mainListView;
    View m_currentSongView;
    TextView m_currentSongNameView, m_currentSongArtistView, m_currentSongTimeView;
    MusicDBParser m_musicDBParser;
    EditText searchEditText;
    Button allSongsTabButton, artistsTabButton, albumsTabButton, prevButton, nextButton, playPauseButton,
        volumeUpButton, volumeDownButton, shuffleButton;
    LinearLayout volumeLayout, currentSongLayout;
    Boolean mediaPlayerNeedsUpdate=false, isShuffle=false;
    IterableGroup m_iterableGroup;
    Utils utils;
    String SHUFFLE_ON = "Shuffle(On)", SHUFFLE_OFF="shuffleOff";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_music);
        m_musicDBParser= new MusicDBParser();
        initViews();
        initTouchListeners();
        prepareIterableGroups();
        updateAllSongs();
        showSongList();
        utils = new Utils(this);
        isShuffle=false;
    }

    //MediaPlayer Event listeners------------------------------------------------------------------
    @Override
    public void playerPlaying(MediaPlayerSong song) {
        runTimer();
        playPauseButton.setText(R.string.pause_button_name);
        updateCurrentSongGUI(song);
    }

    @Override
    public void playerPaused(MediaPlayerSong song) {
        stopTimer();
        playPauseButton.setText(R.string.play_button_name);
        updateCurrentSongGUI(song);
    }

    @Override
    public void playerStopped(MediaPlayerSong song) {
        stopTimer();
    }

    @Override
    public void playerTimeUpdated(int currentPosition) {
        m_currentSongTimeView.setText(currentPosition);
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.all_songs_tab_button:
                updateAllSongs();
                showSongList();
                break;
            case R.id.albums_tab_button:
                ArrayList<String> albums = new ArrayList<>();
                albums.addAll(m_musicDBParser.getAllAlbums(this));
                showAlbumList(albums);
                break;
            case R.id.artists_tab_button:
                ArrayList<String> artists = new ArrayList<>();
                artists.addAll(m_musicDBParser.getAllArtists(this));
                showArtistList(artists);
                break;
            case R.id.prev_button:
                if(mediaPlayerWrapper == null){
                    updateMediaPlayerWrapper();
                }
                mediaPlayerWrapper.playPrev();
                break;
            case R.id.next_button:
                if(mediaPlayerWrapper == null){
                    updateMediaPlayerWrapper();
                }
                mediaPlayerWrapper.playNext();
                break;
            case R.id.play_pause_button:
                if(mediaPlayerWrapper == null){
                    updateMediaPlayerWrapper();
                    mediaPlayerWrapper.playNext();
                }
                else
                    mediaPlayerWrapper.togglePlayPause();
                break;
            case R.id.shuffle_button:
                isShuffle = !isShuffle;
                if(isShuffle){
                    if(mediaPlayerWrapper!=null)
                        mediaPlayerWrapper.setShuffle();
                    shuffleButton.setText(SHUFFLE_ON);
                }else{
                    if(mediaPlayerWrapper!=null)
                        mediaPlayerWrapper.unsetShuffle();
                    shuffleButton.setText(SHUFFLE_OFF);
                }
                break;
            case R.id.volume_down_button:
                utils.decreaseVolume((AudioManager) getSystemService(Context.AUDIO_SERVICE));
                break;
            case R.id.volume_up_button:
                utils.increaseVolume((AudioManager) getSystemService(Context.AUDIO_SERVICE));
                break;
            case R.id.edit_text_search:
                if(!searchEditText.isEnabled()){
                    return;
                }
                searchEditText.setText("");
                searchEditText.requestFocus();
                onSearchFocus();
                showSoftKey(searchEditText);
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if(mediaPlayerWrapper!= null) {
            mediaPlayerWrapper.MediaPlayerDestroyInstance();
        }
    }

    //Private-----------------------------------------------------------------------------------------------

    protected void initTouchListeners() {
        fab_left.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                m_iterableGroup.iterate();
                onSearchBlur();
            }
        });
        fab_right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (m_iterableGroup.back())
                    onBackPressed();
            }
        });
        fab_middle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                m_iterableGroup.select();
            }
        });
        fab_left.setOnLongClickListener(this);
        fab_right.setOnLongClickListener(this);
        fab_middle.setOnLongClickListener(this);


        prevButton.setOnClickListener(this);
        playPauseButton.setOnClickListener(this);
        nextButton.setOnClickListener(this);

        allSongsTabButton.setOnClickListener(this);
        artistsTabButton.setOnClickListener(this);
        albumsTabButton.setOnClickListener(this);

        volumeDownButton.setOnClickListener(this);
        volumeUpButton.setOnClickListener(this);
        shuffleButton.setOnClickListener(this);
        initSearchEditText();
    }

    private void initSearchEditText(){
        searchEditText.setOnClickListener(this);
        searchEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                updateSongsByPrefix(s.toString());
            }
        });
        searchEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent e) {
                boolean $ = false;
                if (actionId == EditorInfo.IME_ACTION_SEND || actionId == EditorInfo.IME_ACTION_NEXT
                        || actionId == EditorInfo.IME_ACTION_DONE) {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                    hideSoftKey();
                }
                return $;
            }
        });


    }

    private void initViews() {

        volumeLayout = (LinearLayout) findViewById(R.id.ll_volume);
        currentSongLayout = (LinearLayout) findViewById(R.id.current_song);

        fab_left = (FloatingActionButton) findViewById(R.id.fab_left);
        fab_right = (FloatingActionButton) findViewById(R.id.fab_right);
        fab_middle = (FloatingActionButton) findViewById(R.id.fab_middle);

        mainListView = (ListView) findViewById(R.id.main_list_view);
        m_currentSongView =  findViewById(R.id.current_song);
        m_currentSongNameView = (TextView) findViewById(R.id.current_song_name);
        m_currentSongArtistView = (TextView) findViewById(R.id.current_song_artist_name);
        m_currentSongTimeView =(TextView) findViewById(R.id.current_song_time);

        allSongsTabButton = (Button)   findViewById(R.id.all_songs_tab_button);
        artistsTabButton = (Button)   findViewById(R.id.albums_tab_button);
        albumsTabButton = (Button)   findViewById(R.id.artists_tab_button);

        searchEditText = (EditText) findViewById(R.id.edit_text_search);
        volumeDownButton = (Button)   findViewById(R.id.volume_down_button);
        volumeUpButton = (Button)   findViewById(R.id.volume_up_button);

        prevButton = (Button) findViewById(R.id.prev_button);
        playPauseButton = (Button) findViewById(R.id.play_pause_button);
        nextButton = (Button) findViewById(R.id.next_button);
        shuffleButton = (Button) findViewById(R.id.shuffle_button);
    }

    private void prepareIterableGroups(){
        List<View> tabButtonList = new ArrayList<>();
        tabButtonList.add(allSongsTabButton);
        tabButtonList.add(artistsTabButton);
        tabButtonList.add(albumsTabButton);

        IterableSingleView searchView = new IterableSingleView(searchEditText,this);

        List<View> volumeButtonList = new ArrayList<>();
        volumeButtonList.add(volumeUpButton);
        volumeButtonList.add(volumeDownButton);
        volumeButtonList.add(shuffleButton);

        List<View> navigationButtonList = new ArrayList<>();
        navigationButtonList.add(prevButton);
        navigationButtonList.add(playPauseButton);
        navigationButtonList.add(nextButton);


        IterableViewGroup tabIterableGroup = new IterableViewGroup(tabButtonList, findViewById(R.id.ll_music_top_buttons), this);
        IterableViewGroup volumeIterableGroup= new IterableViewGroup(volumeButtonList, findViewById(R.id.ll_volume), this);
        IterableViewGroup navigationIterableGroup = new IterableViewGroup(navigationButtonList, findViewById(R.id.current_song), this);
        IterableListView mainListGroup = new IterableListView(mainListView, this);

        ArrayList<IterableGroup> mainIterableGroup = new ArrayList<IterableGroup>();
        mainIterableGroup.add(tabIterableGroup);
        mainIterableGroup.add(searchView);
        mainIterableGroup.add(volumeIterableGroup);
        mainIterableGroup.add(navigationIterableGroup);
        mainIterableGroup.add(mainListGroup);
        m_iterableGroup= new IterableViewGroups(mainIterableGroup, findViewById(R.id.ll_music), this);
        m_iterableGroup.start();
    }

    private void runTimer(){
        m_timer = new Timer();
        m_timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        int currentTime = mediaPlayerWrapper.getCurrentTime();
                        m_currentSongTimeView
                                .setText(String
                                        .format("%02d : %02d",
                                                TimeUnit.MILLISECONDS.toMinutes(currentTime),
                                                TimeUnit.MILLISECONDS.toSeconds(currentTime) -
                                                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(currentTime))));
                    }
                });
            }
        }, 0, 1000);
    }

    private void stopTimer(){
        m_currentSongTimeView.setText("");
        m_timer.purge();
        m_timer.cancel();
    }

    private void onSearchFocus() {
        volumeLayout.setVisibility(View.GONE);
        currentSongLayout.setVisibility(View.GONE);
    }

    private void onSearchBlur(){
        volumeLayout.setVisibility(View.VISIBLE);
        currentSongLayout.setVisibility(View.VISIBLE);
    }


    //Update MediaPlayerWrapper---------------------------------------------------------

    private void updateAllSongs() {
        m_originalCurrentSongs = m_musicDBParser.getAllSongs(this);
        m_currentSongs = new ArrayList<>(m_originalCurrentSongs);
        mediaPlayerNeedsUpdate=true;
    }

    private void updateSongsByAlbum(String album){
        m_originalCurrentSongs = m_musicDBParser.getAllSongsByAlbum(this, album);
        m_currentSongs = new ArrayList<>(m_originalCurrentSongs);
        mediaPlayerNeedsUpdate=true;
    }

    private void updateSongsByArtist(String artist){
        m_originalCurrentSongs = new MusicDBParser().getAllSongsByArtist(this, artist);
        m_currentSongs = new ArrayList<>(m_originalCurrentSongs);
        mediaPlayerNeedsUpdate=true;
    }

    private void updateMediaPlayerWrapper(){
        MediaPlayerWrapper.MediaPlayerCreate(m_currentSongs, this);
        mediaPlayerWrapper = MediaPlayerWrapper.getInstance();
        if(isShuffle){
            mediaPlayerWrapper.setShuffle();
        }
    }

    private void updateSongsByPrefix(String songName){
        m_currentSongs=new ArrayList<>();
        for(int i=0; i<m_originalCurrentSongs.size(); i++ ) {
            if (m_originalCurrentSongs.get(i).getName().toUpperCase().startsWith(songName.toUpperCase())) {
                m_currentSongs.add(m_originalCurrentSongs.get(i));
                mediaPlayerNeedsUpdate = true;
            }
        }
        showSongList();
    }

    //Update List view ----------------------------------------------------------------

    private void showSongList() {
        searchEditText.setEnabled(true);
        songAdapter = new SongAdapter(MusicPlayerActivity.this, R.layout.song_item, m_currentSongs);
        mainListView.setAdapter(songAdapter);
        mainListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if(mediaPlayerNeedsUpdate== true){
                    mediaPlayerNeedsUpdate = false;
                    updateMediaPlayerWrapper();
                }
                mediaPlayerWrapper.setCurrentSongIndex(position);
            }
        });
        if (m_currentSongs == null || m_currentSongs.size() == 0)
            utils.showToast(getString(R.string.no_songs_found));
        else
            Collections.sort(m_currentSongs, new Comparator<MediaPlayerSong>() {
                @Override
                public int compare(final MediaPlayerSong lhs, final MediaPlayerSong rhs) {
                    return lhs.getName().compareTo(rhs.getName());
                }
            });
    }

    private void showAlbumList(ArrayList<String> albums){
        showSingleLineList(albums);
        mainListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                TextView textView = (TextView) view.findViewById(R.id.string_content);
                updateSongsByAlbum(textView.getText().toString());
                showSongList();
                prepareIterableGroups();
            }
        });
    }

    private void showArtistList(ArrayList<String> artists){
        showSingleLineList(artists);
        mainListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                TextView textView = (TextView) view.findViewById(R.id.string_content);
                updateSongsByArtist(textView.getText().toString());
                showSongList();
                prepareIterableGroups();
            }
        });
    }

    private void showSingleLineList(ArrayList<String> lines){
        searchEditText.setEnabled(false);
        ArrayAdapter arrayAdapter= new ArrayAdapter(this, R.layout.text_item, lines);
        mainListView.setAdapter(arrayAdapter);
        if (lines == null || lines.size() == 0)
            utils.showToast(getString(R.string.nothing_to_show));
        else
            Collections.sort(lines, new Comparator<String>() {
                @Override
                public int compare(final String lhs, final String rhs) {
                    return lhs.compareTo(rhs);
                }
            });
    }

    //---------------------------------------------------------------------------------

    private void updateCurrentSongGUI(MediaPlayerSong song){
        m_currentSongView.setVisibility(View.VISIBLE);
        m_currentSongNameView.setText(song.getName());
        m_currentSongArtistView.setText(song.getArtist());
    }

}
