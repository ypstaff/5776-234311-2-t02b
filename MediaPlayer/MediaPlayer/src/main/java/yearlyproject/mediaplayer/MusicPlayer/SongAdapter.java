package yearlyproject.mediaplayer.MusicPlayer;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import yearlyproject.mediaplayer.R;
import yearlyproject.mediaplayer.Pojos.MediaPlayerSong;


/**
 * Created by roman on 12-12-15.
 *
 */


public class SongAdapter extends ArrayAdapter<MediaPlayerSong> {

    List<MediaPlayerSong> songs;
    int itemRow;

    // View lookup cache
    private static class SongViewHolder {
        TextView songName;
        TextView artistName;
    }

    public SongAdapter(Context context,int row,  List<MediaPlayerSong> songs) {
        super(context, row, songs);
        this.itemRow= row;
        this.songs=songs;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        MediaPlayerSong song = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        SongViewHolder songViewHolder; // view lookup cache stored in tag
        if (convertView == null) {
            songViewHolder = new SongViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(itemRow, parent, false);
            songViewHolder.songName = (TextView) convertView.findViewById(R.id.song_name);
            songViewHolder.artistName = (TextView) convertView.findViewById(R.id.song_artist);
            convertView.setTag(songViewHolder);
        } else
			songViewHolder = (SongViewHolder) convertView.getTag();
        // Populate the data into the template view using the data object
        songViewHolder.songName.setText(song.getName());
        songViewHolder.artistName.setText(song.getArtist());
        // Return the completed view to render on screen
        return convertView;
    }

}