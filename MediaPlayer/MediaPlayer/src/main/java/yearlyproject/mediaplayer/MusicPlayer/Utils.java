package yearlyproject.mediaplayer.MusicPlayer;

import android.content.Context;
import android.media.AudioManager;
import android.widget.Toast;

/**
 * Created by RomanM on 4/18/2016.
 */
public final class Utils {

    static int  volInterval= 6 ;
    static Toast m_toast;
    Utils(Context c){
        m_toast=Toast.makeText(c, "", Toast.LENGTH_SHORT);
    }

    public static void increaseVolume(AudioManager am){
        am.setStreamVolume(
                AudioManager.STREAM_MUSIC,
                am.getStreamVolume(AudioManager.STREAM_MUSIC)   +  am.getStreamMaxVolume(AudioManager.STREAM_MUSIC) / volInterval,
                0);
        showToast(Integer.toString(am.getStreamVolume(AudioManager.STREAM_MUSIC)));
    }

    public static void decreaseVolume(AudioManager am){

        am.setStreamVolume(
                AudioManager.STREAM_MUSIC,
                am.getStreamVolume(AudioManager.STREAM_MUSIC) - am.getStreamMaxVolume(AudioManager.STREAM_MUSIC) / volInterval,
                0);
        showToast(Integer.toString(am.getStreamVolume(AudioManager.STREAM_MUSIC)));
    }

    /**
     * Toasts a message on screen.
     *
     * @param msg
     *            - the messaged to be displayed.
     */
    public static void showToast(final String msg) {
        m_toast.setText(msg);
        m_toast.show();
    }


}
