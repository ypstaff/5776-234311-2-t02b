package yearlyproject.mediaplayer.MusicPlayer;

import java.util.EventListener;

import yearlyproject.mediaplayer.Pojos.MediaPlayerSong;

/**
 * Created by roman on 12/8/2015.
 */


interface MediaPlayerEventListener extends EventListener {
     void playerPlaying(MediaPlayerSong song);
     void playerPaused(MediaPlayerSong song);
     void playerStopped(MediaPlayerSong song);
     void playerTimeUpdated(int currentPosition);
}