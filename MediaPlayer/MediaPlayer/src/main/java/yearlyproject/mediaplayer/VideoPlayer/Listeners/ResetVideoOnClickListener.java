package yearlyproject.mediaplayer.VideoPlayer.Listeners;

import android.view.View;
import android.widget.VideoView;

/**
 * Created by Sergey on 23/12/2015.
 */
public class ResetVideoOnClickListener implements View.OnClickListener {
    private VideoView videoView;
    public ResetVideoOnClickListener(VideoView videoView) {
        this.videoView=videoView;
    }

    @Override
    public void onClick(View v) {
        boolean hasPlayed = videoView.isPlaying();
        if(hasPlayed) videoView.pause();
        videoView.seekTo(0);
        if(hasPlayed) videoView.start();
    }
}
