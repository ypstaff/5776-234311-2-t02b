package yearlyproject.mediaplayer.VideoPlayer.Listeners;

import android.view.View;

/**
 * Created by Sergey on 23/12/2015.
 */
public class HideAndShowOnClickListener implements View.OnClickListener {
    private View changedView;

    public HideAndShowOnClickListener(View changedView) {
        this.changedView=changedView;
    }

    @Override
    public void onClick(View v) {
        if(changedView.getVisibility()==View.GONE) {
            changedView.setVisibility(View.VISIBLE);
        } else {
            changedView.setVisibility(View.GONE);
        }
    }
}
