package yearlyproject.mediaplayer.VideoPlayer;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import yearlyproject.mediaplayer.DataBaseBridge.VideoDBParser;
import yearlyproject.mediaplayer.R;
import yearlyproject.mediaplayer.VideoPlayer.Listeners.OnItemClickWrapper;
import yearlyproject.mediaplayer.VideoPlayer.Listeners.VideoSelectOnClickListener;
import yearlyproject.mediaplayer.Pojos.MediaPlayerVideo;
import yearlyproject.navigationlibrary.EmergencyCallActivity;
import yearlyproject.navigationlibrary.IterableGroup;
import yearlyproject.navigationlibrary.IterableListView;
import yearlyproject.navigationlibrary.IterableSingleView;
import yearlyproject.navigationlibrary.IterableViewGroups;

public class VideoActivity extends EmergencyCallActivity {
    public static final String DISPLAYING_ALL_VIDEOS = "Displaying All Videos";
    public static final String FOUND_MATCHING_VIDEOS = "Found %d Matching Videos";
    private IterableGroup m_rootIterableGroup;
    private ListView m_videosList;
    private EditText m_searchQueryInputField;
    private TextView m_searchMessageLabel;

    /***
     * Initiates gui elements.
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //android.os.Debug.waitForDebugger();
        setContentView(R.layout.activity_video);
        //initiate navigation buttons
        fab_left = (FloatingActionButton) findViewById(R.id.fab_left);
        fab_right = (FloatingActionButton) findViewById(R.id.fab_right);
        fab_middle = (FloatingActionButton) findViewById(R.id.fab_middle);
        initTouchListeners();

        fillListView(getApplicationContext(), R.layout.activity_video);
        setupSearchBar(getApplicationContext());
        m_searchMessageLabel.setText(String.format(DISPLAYING_ALL_VIDEOS, m_videosList.getAdapter().getCount()));

        //initiate groups for navigation.
        IterableListView ilv = new IterableListView(m_videosList,this);
        IterableSingleView iet = new IterableSingleView(m_searchQueryInputField,this);
        List<IterableGroup> allElements = new ArrayList<>();
        allElements.add(ilv);
        allElements.add(iet);
        m_rootIterableGroup = new IterableViewGroups(allElements,findViewById(R.id.video_tab),this);
        m_rootIterableGroup.start();

    }

    //filling a listview with all videos
    private ListView fillListView(Context context,int layout) {
        m_videosList = (ListView)findViewById(R.id.video_list_view);
        Object[] res= new VideoDBParser().getAllVideos(context).toArray();
        MediaPlayerVideo[] videos=new MediaPlayerVideo[res.length];
        for(int i=0;i<videos.length;i++) {
            videos[i]=(MediaPlayerVideo)res[i];
        }
        VideoArrayAdapter adapter = new VideoArrayAdapter(context,layout,videos,
                new VideoSelectOnClickListener(context));
        m_videosList.setOnItemClickListener(new OnItemClickWrapper(new VideoSelectOnClickListener(context)));
        m_videosList.setAdapter(adapter);
        return m_videosList;
    }

    //filling a listview with all names that contain name parameter.
    private ListView fillListView(Context context,int layout, String name) {
        m_videosList = (ListView)findViewById(R.id.video_list_view);
        Object[] res= new VideoDBParser().getAllVideosByName(context, name).toArray();
        MediaPlayerVideo[] videos=new MediaPlayerVideo[res.length];
        for(int i=0;i<videos.length;i++) {
            videos[i]=(MediaPlayerVideo)res[i];
        }
        VideoArrayAdapter adapter = new VideoArrayAdapter(context,layout,videos,
                new VideoSelectOnClickListener(context));
        m_videosList.setAdapter(adapter);
        return m_videosList;
    }


    private EditText setupSearchBar(Context context) {
        m_searchQueryInputField = (EditText)findViewById(R.id.video_search_textview);
        m_searchMessageLabel = (TextView)findViewById(R.id.search_message_label);
        m_searchQueryInputField.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showSoftKey(m_searchQueryInputField);
            }
        });
        m_searchQueryInputField.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                boolean handled = false;
                if (actionId == EditorInfo.IME_ACTION_SEND || actionId == EditorInfo.IME_ACTION_NEXT
                        || actionId == EditorInfo.IME_ACTION_DONE) {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                    hideSoftKey();
                }
                return handled;
            }
        });
        m_searchQueryInputField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!s.toString().equals("")) {
                    fillListView(getApplicationContext(), 0, s.toString());
                    m_searchMessageLabel.setText(String.format(FOUND_MATCHING_VIDEOS, m_videosList.getAdapter().getCount()));
                } else {
                    fillListView(getApplicationContext(), 0);
                    m_searchMessageLabel.setText(String.format(DISPLAYING_ALL_VIDEOS, m_videosList.getAdapter().getCount()));
                }
            }
        });
        return m_searchQueryInputField;
    }

    /***
     * Initiates navigation touch listeners.
     */
    protected void initTouchListeners() {
        fab_left.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                m_rootIterableGroup.iterate();
            }
        });
        fab_right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (m_rootIterableGroup.back())
                    onBackPressed();
            }
        });
        fab_middle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                m_rootIterableGroup.select();
            }
        });
		  fab_left.setOnLongClickListener(this);
        fab_right.setOnLongClickListener(this);
        fab_middle.setOnLongClickListener(this);
    }
}
