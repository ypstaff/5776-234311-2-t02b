package yearlyproject.mediaplayer.VideoPlayer.Listeners;

import android.content.Context;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

/**
 * Created by Sergey on 11/01/2016.
 */
public class PopUpKeyBoardOnCLickListener implements View.OnClickListener {

    private Context context;

    public PopUpKeyBoardOnCLickListener(Context context) {
        this.context=context;
    }

    @Override
    public void onClick(View v) {
        EditText $ = (EditText)v;
        $.selectAll();
        $.requestFocus();
        final InputMethodManager inputMethodManager = (InputMethodManager)
                context.getSystemService(context.INPUT_METHOD_SERVICE);
        inputMethodManager.showSoftInput($, InputMethodManager.SHOW_IMPLICIT);

    }
}
