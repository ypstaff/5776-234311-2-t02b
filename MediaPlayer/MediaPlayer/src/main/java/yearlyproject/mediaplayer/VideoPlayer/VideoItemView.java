package yearlyproject.mediaplayer.VideoPlayer;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.media.ThumbnailUtils;
import android.provider.MediaStore;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import yearlyproject.mediaplayer.R;
import yearlyproject.mediaplayer.Pojos.MediaPlayerVideo;

/**
 * Created by Sergey on 02/01/2016.
 */
public class VideoItemView extends LinearLayout {

    public static final String ARTIST = "Artist:";
    public static final String TITLE = "Title:";
    private TextView m_titleTextView;
    private TextView m_artistTextView;
    private ImageView m_previewImage;

    private MediaPlayerVideo video;



    public VideoItemView(Context context) {
        super(context);
    }

    public VideoItemView(Context context,AttributeSet attributeSet) {
        super(context,attributeSet);
    }

    public VideoItemView(Context context, AttributeSet attrs, int defStyle) {
        super(context,attrs,defStyle);
    }



    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        m_titleTextView =(TextView)findViewById(R.id.video_name_textview);
        m_artistTextView =(TextView)findViewById(R.id.video_artist_textview);
        m_previewImage =(ImageView)findViewById(R.id.video_preview);
    }

    public void showVideoItem(MediaPlayerVideo video) {
        this.video=video;
        Bitmap thumb = ThumbnailUtils.createVideoThumbnail(video.getPath(), MediaStore.Images.Thumbnails.MINI_KIND);
        m_previewImage.setBackgroundColor(Color.BLACK);
        m_previewImage.setImageBitmap(thumb);
        m_artistTextView.setText(ARTIST +video.getArtist());
        m_titleTextView.setText(TITLE +video.getTitle());
    }

    public MediaPlayerVideo getVideo() {
        return this.video;
    }
}
