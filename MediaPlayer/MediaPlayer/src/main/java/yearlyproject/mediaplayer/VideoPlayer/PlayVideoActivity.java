package yearlyproject.mediaplayer.VideoPlayer;

import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.net.Uri;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.VideoView;

import java.util.ArrayList;
import java.util.List;

import yearlyproject.mediaplayer.DataBaseBridge.VideoDBParser;
import yearlyproject.mediaplayer.R;
import yearlyproject.mediaplayer.VideoPlayer.Listeners.HideAndShowOnClickListener;
import yearlyproject.mediaplayer.VideoPlayer.Listeners.OnItemClickWrapper;
import yearlyproject.mediaplayer.VideoPlayer.Listeners.PauseAndPlayVideoOnClickListener;
import yearlyproject.mediaplayer.VideoPlayer.Listeners.CompositeOnClickListener;
import yearlyproject.mediaplayer.VideoPlayer.Listeners.PopUpKeyBoardOnCLickListener;
import yearlyproject.mediaplayer.VideoPlayer.Listeners.ResetVideoOnClickListener;
import yearlyproject.mediaplayer.VideoPlayer.Listeners.UpdateProgressBarOnClickListener;
import yearlyproject.mediaplayer.VideoPlayer.Listeners.UpdateVideoTotalTimeOnClickListener;
import yearlyproject.mediaplayer.VideoPlayer.Listeners.VideoSwitchOnClickListener;
import yearlyproject.mediaplayer.Pojos.MediaPlayerVideo;
import yearlyproject.mediaplayer.VideoPlayer.TextWatchers.MinutesInputTextWatcher;
import yearlyproject.mediaplayer.VideoPlayer.TextWatchers.SecondsInputTextWatcher;
import yearlyproject.navigationlibrary.EmergencyCallActivity;
import yearlyproject.navigationlibrary.IterableGroup;
import yearlyproject.navigationlibrary.IterableListView;
import yearlyproject.navigationlibrary.IterableViewGroup;
import yearlyproject.navigationlibrary.IterableViewGroups;

/**
 * Created by Sergey on 18/12/2015.
 */
public class PlayVideoActivity  extends EmergencyCallActivity {
    private VideoView m_videoView;
    private ProgressBar m_progressBar;
    private TextView m_progressTextView;
    private TextView m_totalVideolLength;
    private Handler m_progressUpdateHandler = new Handler();
    private LinearLayout m_navigationPan;
    private EditText m_secondsNavigationInput;
    private EditText m_MinutesNavigationInput;
    private Button m_volUp;
    private Button m_volDown;
    private Button m_playAndPauseButton;
    private Button m_resetButton;
    private Button m_switchNavigatePanel;
    private ListView m_videoItemListView;
    private ProgressBar m_volumeBar;
    private IterableGroup m_rootIterableGroup;
    private MediaPlayerVideo m_currVideo;

    private void setUpGroups() {
        List<View> volButtons=new ArrayList<>();
        volButtons.add(m_volUp);
        volButtons.add(m_volDown);
        List<View> controlList = new ArrayList<>();
        controlList.add(m_playAndPauseButton);
        controlList.add(m_resetButton);
        //controlList.add(m_switchNavigatePanel);
        controlList.add(m_secondsNavigationInput);
        controlList.add(m_MinutesNavigationInput);

        IterableViewGroup volButtonsGroup = new IterableViewGroup(volButtons,findViewById(R.id.vol_panel),this);
        IterableViewGroup controlGroup = new IterableViewGroup(controlList,findViewById(R.id.video_player_control_panel),this);
        IterableListView videoListView = new IterableListView(m_videoItemListView,this);

        List<IterableGroup> mainGroup=new ArrayList<>();
        mainGroup.add(controlGroup);
        mainGroup.add(volButtonsGroup);
        mainGroup.add(videoListView);

        m_rootIterableGroup = new IterableViewGroups(mainGroup,findViewById(R.id.play_video_wrapper),this);
        m_rootIterableGroup.start();
    }

    private void setUpVideoPlayerAndProgressTimer() {

        m_videoView = (VideoView)findViewById(R.id.video_player_videoview);
        m_progressTextView = (TextView)findViewById(R.id.video_player_progress_textView);
        m_progressBar = (ProgressBar)findViewById(R.id.video_player_progress_bar);
        m_totalVideolLength =(TextView)findViewById(R.id.video_player_total_length_textView);
        m_progressBar.setProgress(0);

        Intent intent=getIntent();
        m_currVideo =null;
        if (intent != null && intent.getExtras()!=null) {
            m_currVideo =(MediaPlayerVideo)intent.getExtras().getSerializable("video");
            m_videoView.setVideoURI(Uri.parse(m_currVideo.getPath()));
            int duration=Integer.parseInt(m_currVideo.getDuration()) / 1000;
            m_progressBar.setMax(duration);
            m_totalVideolLength.setText(" / " + (duration / 60) + " : " + (duration % 60));
        }
    }

    private void inflateFABs() {
        fab_left = (FloatingActionButton) findViewById(R.id.fab_left);
        fab_right = (FloatingActionButton) findViewById(R.id.fab_right);
        fab_middle = (FloatingActionButton) findViewById(R.id.fab_middle);
    }

    private void setupBluetoothEditTextConnection(EditText et) {
        et.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText etTemp=(EditText)v;
                etTemp.requestFocus();
                showSoftKey(etTemp);
            }
        });
        et.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                boolean handled = false;
                if (actionId == EditorInfo.IME_ACTION_SEND || actionId == EditorInfo.IME_ACTION_NEXT
                        || actionId == EditorInfo.IME_ACTION_DONE) {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                    hideSoftKey();
                }
                return handled;
            }
        });
    }

    private void setUpControlPanel() {
        m_playAndPauseButton = (Button)findViewById(R.id.video_player_play_pause);
        m_resetButton = (Button)findViewById(R.id.video_player_reset);
        m_switchNavigatePanel = (Button)findViewById(R.id.video_player_navigate);
        m_navigationPan =(LinearLayout)findViewById(R.id.video_player_navigation_panel);
        m_secondsNavigationInput =(EditText)findViewById(R.id.video_player_minutes_editText);
        m_MinutesNavigationInput =(EditText)findViewById(R.id.video_player_seconds_editText);

        findViewById(R.id.video_player_navigation_panel).setVisibility(View.VISIBLE);

        m_switchNavigatePanel.setOnClickListener(new HideAndShowOnClickListener(m_navigationPan));
        m_playAndPauseButton.setOnClickListener(new PauseAndPlayVideoOnClickListener(m_videoView));
        m_resetButton.setOnClickListener(new ResetVideoOnClickListener(m_videoView));
        m_secondsNavigationInput.setOnClickListener(new PopUpKeyBoardOnCLickListener(this));
        m_secondsNavigationInput.addTextChangedListener(new MinutesInputTextWatcher(m_videoView, m_MinutesNavigationInput,
                m_progressBar));
        m_MinutesNavigationInput.setOnClickListener(new PopUpKeyBoardOnCLickListener(this));
        m_MinutesNavigationInput.addTextChangedListener(
                new SecondsInputTextWatcher(m_secondsNavigationInput, m_videoView, m_progressBar));

        setupBluetoothEditTextConnection(m_MinutesNavigationInput);
        setupBluetoothEditTextConnection(m_secondsNavigationInput);

    }

    private void setUpVolumePanel() {
        m_volumeBar = (ProgressBar)findViewById(R.id.volume_bar);
        m_volumeBar.setMax(((AudioManager) getSystemService(Context.AUDIO_SERVICE)).getStreamMaxVolume(AudioManager.STREAM_MUSIC));
        m_volUp = (Button)findViewById(R.id.vol_up_button);
        m_volUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AudioManager am = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
                am.adjustStreamVolume(AudioManager.STREAM_MUSIC, 1, 0);
                m_volumeBar.setProgress(m_volumeBar.getProgress() + 1);
            }
        });
        m_volDown = (Button)findViewById(R.id.vol_down_button);
        m_volDown.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                AudioManager am = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
                am.adjustStreamVolume(AudioManager.STREAM_MUSIC, -1, 0);
                m_volumeBar.setProgress(m_volumeBar.getProgress() - 1);

            }
        });
    }

    private void setUpVideoListView() {
        m_videoItemListView =(ListView)findViewById(R.id.activity_play_video_listview);
        Context context = getApplicationContext();
        Object[] res= new VideoDBParser().getAllVideos(context).toArray();
        MediaPlayerVideo[] videos=new MediaPlayerVideo[res.length];
        for(int i=0;i<videos.length;i++) {
            videos[i]=(MediaPlayerVideo)res[i];
        }
        List<View.OnClickListener> actions = new ArrayList<View.OnClickListener>();
        actions.add(new VideoSwitchOnClickListener(context, m_videoView));
        actions.add(new UpdateVideoTotalTimeOnClickListener(m_totalVideolLength));
        actions.add(new UpdateProgressBarOnClickListener(m_progressBar));
        VideoArrayAdapter adapter = new VideoArrayAdapter(context,R.id.video_list_item,videos,
                new CompositeOnClickListener(actions));
        m_videoItemListView.setAdapter(adapter);
        m_videoItemListView.setOnItemClickListener(new OnItemClickWrapper(new CompositeOnClickListener(actions)));
    }

    /***
     * initiates gui elements
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play_video);
        inflateFABs();
        setUpVideoPlayerAndProgressTimer();
        setUpControlPanel();
        setUpVolumePanel();
        setUpVideoListView();
        //android.os.Debug.waitForDebugger();


        //Explanation on what's going on here - creating a new thread and defining a handler which
        //will post change requests (update video progress)
        //to the main thread from the secondary thread we created.
        Thread t=new Thread(new Runnable() {
            @Override
            public void run() {
                while(true) {
                    m_progressUpdateHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            int seconds = m_videoView.getCurrentPosition() / 1000;
                            m_progressBar.setProgress(m_videoView.getCurrentPosition() / 1000);
                            m_progressTextView.setText(String.format("%d : %d", seconds / 60, seconds % 60));
                        }
                    });

                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        t.start();
        setUpGroups();
        initTouchListeners();

    }

    protected void initTouchListeners() {
        fab_left.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                m_rootIterableGroup.iterate();
            }
        });
        fab_right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (m_rootIterableGroup.back())
                    onBackPressed();
            }
        });
        fab_middle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                m_rootIterableGroup.select();
            }
        });
		  fab_left.setOnLongClickListener(this);
        fab_right.setOnLongClickListener(this);
        fab_middle.setOnLongClickListener(this);
    }
}
