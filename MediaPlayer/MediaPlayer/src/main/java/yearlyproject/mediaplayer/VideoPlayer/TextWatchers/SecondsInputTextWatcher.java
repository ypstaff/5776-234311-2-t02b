package yearlyproject.mediaplayer.VideoPlayer.TextWatchers;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.VideoView;

/**
 * Created by Sergey on 11/01/2016.
 */
public class SecondsInputTextWatcher implements TextWatcher {

    private EditText m_editText;
    private VideoView m_videoView;
    private ProgressBar m_progressBar;

    public SecondsInputTextWatcher(EditText mEditText,
                                   VideoView videoView,ProgressBar progressBar) {
        this.m_editText =mEditText;
        this.m_videoView =videoView;
        this.m_progressBar =progressBar;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        String sValue = s.toString();
        int seconds;
        int minutes = 0;
        if (sValue.equals(""))
            seconds = 0;
        else
            seconds = Integer.parseInt(sValue);
        if (seconds >= 60) {
            minutes = seconds / 60;
            s.replace(0, s.toString().length(), new Integer(seconds%60).toString());
        }
        int currSeconds = m_progressBar.getProgress();
        int currMinutes=0;
        if(!m_editText.getText().toString().equals(""))
            currMinutes=Integer.parseInt(m_editText.getText().toString());
        if (minutes > 0)
            m_editText.setText(new Integer(minutes+currMinutes).toString());
        m_videoView.seekTo((seconds + currSeconds - (currSeconds % 60)) * 1000);
    }
}
