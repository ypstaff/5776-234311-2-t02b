package yearlyproject.mediaplayer.VideoPlayer.TextWatchers;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.VideoView;


/**
 * Created by Sergey on 11/01/2016.
 */
public class MinutesInputTextWatcher implements TextWatcher {

    private VideoView m_videoView;
    private ProgressBar m_progressBar;
    private EditText m_secondsET;

    public MinutesInputTextWatcher(VideoView videoView,EditText secondsET,
                                   ProgressBar progressBar) {
        this.m_videoView =videoView;
        this.m_progressBar =progressBar;
        this.m_secondsET =secondsET;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        String sValue = s.toString();
        int minutes;
        if (sValue.equals(""))
            minutes = 0;
        else {
            minutes = Integer.parseInt(sValue);
        }
        int currSeconds = m_progressBar.getProgress();
        String secondsValue= m_secondsET.getText().toString();
        if(secondsValue.equals("")) {
            currSeconds = 0;
        }
        m_videoView.seekTo((currSeconds % 60 + minutes * 60) * 1000);
    }
}
