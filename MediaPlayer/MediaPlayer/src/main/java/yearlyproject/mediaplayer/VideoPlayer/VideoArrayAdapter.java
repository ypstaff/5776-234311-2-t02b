package yearlyproject.mediaplayer.VideoPlayer;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import yearlyproject.mediaplayer.R;
import yearlyproject.mediaplayer.Pojos.MediaPlayerVideo;

/**
 * Created by Sergey on 31/12/2015.
 */
public class VideoArrayAdapter extends ArrayAdapter {
    private MediaPlayerVideo[] m_videos;
    private View.OnClickListener m_onClickListener;
    private LayoutInflater m_layoutInflater;

    public VideoArrayAdapter(Context context, int layout, MediaPlayerVideo[] videos,
                             View.OnClickListener onClickAction) {
        super(context, layout,videos);
        this.m_videos =videos;
        this.m_onClickListener =onClickAction;
        m_layoutInflater =(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    /***
     * Creates a VideoItemView at position <i>position</i> in the array
     * @param position
     * @param convertView
     * @param parent
     * @return
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        VideoItemView itemView;
        if(convertView==null) {
            itemView=(VideoItemView) m_layoutInflater.inflate(R.layout.video_list_item,null);
        } else {
            itemView=(VideoItemView)convertView;
        }
        itemView.showVideoItem(m_videos[position]);
        itemView.setOnClickListener(m_onClickListener);
        return itemView;
    }
}
