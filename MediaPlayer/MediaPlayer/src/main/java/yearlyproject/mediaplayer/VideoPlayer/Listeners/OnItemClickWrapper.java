package yearlyproject.mediaplayer.VideoPlayer.Listeners;

import android.view.View;
import android.widget.AdapterView;

/**
 * Created by Sergey on 10/01/2016.
 */
public class OnItemClickWrapper implements AdapterView.OnItemClickListener {
    private View.OnClickListener listener;
    public OnItemClickWrapper(View.OnClickListener listener) {
        this.listener=listener;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        listener.onClick(view);
    }
}
