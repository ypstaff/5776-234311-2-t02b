package yearlyproject.mediaplayer.VideoPlayer.Listeners;

import android.content.Context;
import android.content.Intent;
import android.view.View;

import yearlyproject.mediaplayer.VideoPlayer.PlayVideoActivity;
import yearlyproject.mediaplayer.VideoPlayer.VideoItemView;

/**
 * Created by Sergey on 23/12/2015.
 */
public class VideoSelectOnClickListener implements View.OnClickListener {
    private Context context;
    public VideoSelectOnClickListener(final Context context) {
        this.context=context;
    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent(context, PlayVideoActivity.class);
        intent.putExtra("video",  ((VideoItemView)v).getVideo());
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }
}
