package yearlyproject.mediaplayer.MusicPlayer;

import android.content.Context;
import android.media.AudioManager;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.NoMatchingViewException;
import android.support.test.espresso.ViewAssertion;
import android.support.test.runner.AndroidJUnit4;
import android.test.ActivityInstrumentationTestCase2;
import android.view.View;
import android.widget.Button;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import yearlyproject.mediaplayer.R;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

/**
 * Created by Sergey on 08/01/2016.
 */

@RunWith(AndroidJUnit4.class)
public class MusicPlayerActivityTest  extends ActivityInstrumentationTestCase2<MusicPlayerActivity> {

    private MusicPlayerActivity mActivity;
    private int musicViewID;

    private class guiInPlayStateAssertion implements ViewAssertion {
        @Override
        public void check(View view, NoMatchingViewException noViewFoundException) {
            assertTrue(((Button) view).getText().toString().equals(mActivity.getString(R.string.pause_button_name)));
        }
    }

    private class guiInPauseStateAssertion implements ViewAssertion {
        @Override
        public void check(View view, NoMatchingViewException noViewFoundException) {
            Button but = (Button) view;
            assertTrue(but.getText().toString().equals(mActivity.getString(R.string.play_button_name)));
        }
    }

    private void assertMusicPlays() {
        onView(withId(R.id.play_pause_button)).check(new guiInPlayStateAssertion());
    }

    private void assertMusicPaused() {
        onView(withId(R.id.play_pause_button)).check(new guiInPauseStateAssertion());
    }

    private void pressButton(int buttonID) {
        onView(withId(buttonID)).perform(click());
    }

    public MusicPlayerActivityTest() {
        super(MusicPlayerActivity.class);
    }


    @Before
    public void setUp() throws Exception{
        super.setUp();
        injectInstrumentation(InstrumentationRegistry.getInstrumentation());
        mActivity=getActivity();
        musicViewID =R.id.ll_music;
    }

    @Test
    public void increaseVolumeOnVolUpClick() {
        AudioManager m = (AudioManager)mActivity.getSystemService(Context.AUDIO_SERVICE);
        int previous = m.getStreamVolume(AudioManager.STREAM_MUSIC);
        onView(withId(R.id.volume_up_button))
                .perform(click());
        int updatedVolume = m.getStreamVolume(AudioManager.STREAM_MUSIC);
        assertTrue(previous <= updatedVolume);
    }

    @Test
    public void decreaseVolumeOnVolDownClick() {
        AudioManager m = (AudioManager)mActivity.getSystemService(Context.AUDIO_SERVICE);
        int previous = m.getStreamVolume(AudioManager.STREAM_MUSIC);
        onView(withId(R.id.volume_down_button)).perform(click());
        int updatedVolume = m.getStreamVolume(AudioManager.STREAM_MUSIC);
        assertTrue(previous >= updatedVolume);
    }

}
