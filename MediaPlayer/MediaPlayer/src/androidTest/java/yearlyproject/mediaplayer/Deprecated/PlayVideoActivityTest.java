package yearlyproject.mediaplayer.Deprecated;

import android.content.Context;
import android.media.AudioManager;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.NoMatchingViewException;
import android.support.test.espresso.ViewAssertion;
import android.support.test.runner.AndroidJUnit4;
import android.test.ActivityInstrumentationTestCase2;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.VideoView;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import yearlyproject.mediaplayer.R;
import yearlyproject.mediaplayer.VideoPlayer.PlayVideoActivity;
import yearlyproject.mediaplayer.VideoPlayer.VideoItemView;
import yearlyproject.mediaplayer.Pojos.MediaPlayerVideo;

import static android.support.test.espresso.Espresso.onData;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.matcher.ViewMatchers.withContentDescription;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.instanceOf;

/**
 * Created by Sergey on 08/01/2016.
 */
//Doesn't work with after integrating navigation.
@RunWith(AndroidJUnit4.class)
public class PlayVideoActivityTest  extends ActivityInstrumentationTestCase2<PlayVideoActivity> {

    private PlayVideoActivity mActivity;
    private int videoViewID;
    private class videoIsPlayingAssertion implements ViewAssertion {

        @Override
        public void check(View view, NoMatchingViewException noViewFoundException) {
            assertTrue(((VideoView)view).isPlaying());
        }
    }

    private class videoIsStoppedAssertion implements ViewAssertion {

        @Override
        public void check(View view, NoMatchingViewException noViewFoundException) {
            assertFalse(((VideoView)view).isPlaying());
        }
    }
    private void assertVideoPlays() {
        onView(withId(videoViewID)).check(new videoIsPlayingAssertion());
    }

    private void assertVideoStopped() {
        onView(withId(videoViewID)).check(new videoIsStoppedAssertion());
    }

    private void pressButtun(int buttunId) {
        onView(withId(buttunId)).perform(click());
    }

    public PlayVideoActivityTest() {
        super(PlayVideoActivity.class);
    }


    @Before
    public void setUp() throws Exception{
        super.setUp();
        injectInstrumentation(InstrumentationRegistry.getInstrumentation());
        mActivity=getActivity();
        videoViewID= R.id.video_player_videoview;
    }

    @Test
    public void increaseVolumeOnVolUpClick() {
        AudioManager m = (AudioManager)mActivity.getSystemService(Context.AUDIO_SERVICE);
        int expected = m.getStreamVolume(AudioManager.STREAM_MUSIC);
        if(m.getStreamMaxVolume(AudioManager.STREAM_MUSIC)>expected)
            expected+=1;
        onView(withId(R.id.vol_up_button))
                .perform(click());
        int updatedVolume = m.getStreamVolume(AudioManager.STREAM_MUSIC);
        assertEquals(expected,updatedVolume);
    }
    @Test
    public void decreaseVolumeOnVolDownClick() {
        AudioManager m = (AudioManager)mActivity.getSystemService(Context.AUDIO_SERVICE);
        int expected = m.getStreamVolume(AudioManager.STREAM_MUSIC);
        if(expected>0)
            expected-=1;
        onView(withId(R.id.vol_down_button))
                .perform(click());
        int updatedVolume = m.getStreamVolume(AudioManager.STREAM_MUSIC);
        assertEquals(expected,updatedVolume);
    }

    @Test
    public void startPlayingVideoOnClickPlay() {
        onView(withId(R.id.video_player_play_pause))
                .perform(click());
        assertVideoPlays();
    }

    @Test
    public void stopPlayingVideoOnClickPause() {
        onView(withId(R.id.video_player_play_pause))
                .perform(click(),click());
        assertVideoStopped();
    }


    //TODO: see if a convinient way to create a long timeout exists.
    @Test
    public void resetVideoOnClickReset() {
        onView(withId(R.id.video_player_play_pause))
                .perform(click(),click());
                //two clicks are required since otherwise the test will always fail because
                //some time passes between the reset and the assertion, and if the video plays
                //the position will never be 0.
        onView(withId(R.id.video_player_reset))
                .perform(click());
        onView(withId(R.id.video_player_videoview))
                .check(new ViewAssertion() {
                    @Override
                    public void check(View view, NoMatchingViewException noViewFoundException) {
                        //position is in milliseconds, and a delay exists between the click and the
                        //assertion, so we should fail if the video wasn't reset.
                        assertEquals(0, ((VideoView) view).getCurrentPosition());
                    }
                });
    }

    @Test
    public void resetDoesNotStopVideo() {
        onView(withId(R.id.video_player_play_pause))
                .perform(click());
        onView(withId(R.id.video_player_reset))
                .perform(click());
        assertVideoPlays();
    }

    @Test
    public void resetDoesNotStartVideo() {
        onView(withId(R.id.video_player_reset))
                .perform(click());
        assertVideoStopped();
        onView(withId(R.id.video_player_play_pause))
                .perform(click(), click());
        onView(withId(R.id.video_player_reset))
                .perform(click());
        assertVideoStopped();
    }

   /* @Test
    public void showHiddenNavPanelOnNavigateClick() {
        onView(withId(R.id.video_player_navigate))
                .perform(click());
        onView(withId(R.id.video_player_navigation_panel))
                .check(new ViewAssertion() {
                    @Override
                    public void check(View view, NoMatchingViewException noViewFoundException) {
                        assertEquals(View.VISIBLE, view.getVisibility());
                    }
                });
    }

    @Test
    public void hideVisibleNavPanelOnNavigateClick() {
        onView(withId(R.id.video_player_navigate))
                .perform(click(),click());
        onView(withId(R.id.video_player_navigation_panel))
                .check(new ViewAssertion() {
                    @Override
                    public void check(View view, NoMatchingViewException noViewFoundException) {
                        assertEquals(View.GONE,view.getVisibility());
                    }
                });
    }*/

    @Test
    public void navigateToCorrectPointInVideo() {
        onView(withId(R.id.video_player_navigate))
                .perform(click());
        onView(withId(R.id.video_player_seconds_editText))
                .perform(typeText("14"),closeSoftKeyboard());
        onView(withId(R.id.video_player_videoview))
                .check(new ViewAssertion() {
                    @Override
                    public void check(View view, NoMatchingViewException noViewFoundException) {
                        assertEquals(14 * 1000, ((VideoView) view).getCurrentPosition());
                    }
                });
        onView(withId(R.id.video_player_minutes_editText))
                .perform(typeText("1"), closeSoftKeyboard());
        onView(withId(R.id.video_player_videoview))
                .check(new ViewAssertion() {
                    @Override
                    public void check(View view, NoMatchingViewException noViewFoundException) {
                        assertEquals(74 * 1000, ((VideoView) view).getCurrentPosition());
                    }
                });
    }

    @Test
    public void progressBarContainsCorrectVideoProgress() {
        pressButtun(R.id.video_player_navigate);
        onView(withId(R.id.video_player_progress_bar))
                .check(new ViewAssertion() {
                    @Override
                    public void check(View view, NoMatchingViewException noViewFoundException) {
                        assertEquals(0, ((ProgressBar) view).getProgress());
                    }
                });
        onView(withId(R.id.video_player_seconds_editText))
                .perform(typeText("18"), closeSoftKeyboard());
        onView(withId(R.id.video_player_progress_bar))
                .check(new ViewAssertion() {
                    @Override
                    public void check(View view, NoMatchingViewException noViewFoundException) {
                        assertEquals(18, ((ProgressBar) view).getProgress());
                    }
                });
        onView(withId(R.id.video_player_minutes_editText))
                .perform(typeText("1"), closeSoftKeyboard());
        onView(withId(R.id.video_player_progress_bar))
                .check(new ViewAssertion() {
                    @Override
                    public void check(View view, NoMatchingViewException noViewFoundException) {
                        assertEquals(78,((ProgressBar)view).getProgress());
                    }
                });
    }

    @Test
    public void roundUpSecondsInputToMinute() {
        pressButtun(R.id.video_player_navigate);
        onView(withId(R.id.video_player_seconds_editText))
                .perform(typeText("72"), closeSoftKeyboard());
        onView(withId(R.id.video_player_seconds_editText))
                .check(new ViewAssertion() {
                    @Override
                    public void check(View view, NoMatchingViewException noViewFoundException) {
                        assertEquals("59", ((EditText) view).getText().toString());
                    }
                });
        onView(withId(R.id.video_player_videoview))
                .check(new ViewAssertion() {
                    @Override
                    public void check(View view, NoMatchingViewException noViewFoundException) {
                        assertEquals(59000, ((VideoView) view).getCurrentPosition());
                    }
                });
    }

    @Test
    public void remainderOfSecondsInputGoesToMinutes() {
        pressButtun(R.id.video_player_navigate);
        onView(withId(R.id.video_player_seconds_editText))
                .perform(typeText("74"), closeSoftKeyboard());
        onView(withId(R.id.video_player_minutes_editText))
                .check(new ViewAssertion() {
                    @Override
                    public void check(View view, NoMatchingViewException noViewFoundException) {
                        assertEquals("1",((EditText)view).getText().toString());
                    }
                });
    }

    @Test
    public void switchVideoOnListVideoItemClick() {
        onData(is(instanceOf(MediaPlayerVideo.class)))
                .inAdapterView(withId(R.id.activity_play_video_listview))
                .atPosition(3).perform(click())
                .check(new ViewAssertion() {
                    @Override
                    public void check(View view, NoMatchingViewException noViewFoundException) {
                        VideoView videoView = (VideoView) mActivity.findViewById(R.id.video_player_videoview);
                        assertEquals(Integer.parseInt(((VideoItemView) view).getVideo().getDuration()),
                                videoView.getDuration()); //no way to get the actual path to the video.
                    }
                });
    }

    @Test
    public void switchVideoLengthOnVideoItemClick() {
        onData(is(instanceOf(MediaPlayerVideo.class)))
                .inAdapterView(withId(R.id.activity_play_video_listview))
                .atPosition(3).perform(click())
                .check(new ViewAssertion() {
                    @Override
                    public void check(View view, NoMatchingViewException noViewFoundException) {
                        TextView tv = (TextView)mActivity.findViewById(R.id.video_player_total_length_textView);
                        MediaPlayerVideo video = ((VideoItemView)view).getVideo();
                        int duration= Integer.parseInt(video.getDuration())/1000;
                        assertEquals(" / "+(duration/60)+" : "+(duration%60),tv.getText());
                    }
                });

    }
}
