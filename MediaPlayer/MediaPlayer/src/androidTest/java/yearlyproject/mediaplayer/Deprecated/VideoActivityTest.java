package yearlyproject.mediaplayer.Deprecated;

import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.NoMatchingViewException;
import android.support.test.espresso.UiController;
import android.support.test.espresso.ViewAction;
import android.support.test.espresso.ViewAssertion;
import android.support.test.espresso.intent.Intents;
import android.support.test.espresso.matcher.ViewMatchers;
import android.support.test.runner.AndroidJUnit4;
import android.test.ActivityInstrumentationTestCase2;
import android.view.View;


import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import yearlyproject.mediaplayer.R;
import yearlyproject.mediaplayer.VideoPlayer.PlayVideoActivity;
import yearlyproject.mediaplayer.VideoPlayer.VideoActivity;
import yearlyproject.mediaplayer.VideoPlayer.VideoItemView;
import yearlyproject.mediaplayer.Pojos.MediaPlayerVideo;

import static android.support.test.espresso.Espresso.onData;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.intent.Intents.intended;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasComponent;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasExtra;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasExtraWithKey;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.hasEntry;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.core.IsEqual.equalTo;

/**
 * Created by Sergey on 08/01/2016.
 */
@RunWith(AndroidJUnit4.class)
public class VideoActivityTest extends ActivityInstrumentationTestCase2<VideoActivity> {

    private VideoActivity mActivity;
    private MediaPlayerVideo video;

    public VideoActivityTest() {
        super(VideoActivity.class);
    }

    @Before
    public void setUp() throws Exception{
        super.setUp();
        injectInstrumentation(InstrumentationRegistry.getInstrumentation());
        mActivity=getActivity();
    }

    @Test
    public void sendCorrectIntentOnVideoSelection() {
        Intents.init();
        onData(is(instanceOf(MediaPlayerVideo.class)))
                .inAdapterView(ViewMatchers.withId(R.id.video_list_view))
                .atPosition(0)
                .perform(click());
        intended(hasComponent(PlayVideoActivity.class.getName()));
        //intended(hasExtra("video", video));
        Intents.release();
    }
}
