package yearlyproject.bluetooth;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class InterfaceActivity extends AppCompatActivity implements BluetoothAbleActivity{
    BluetoothSlaveHandler mBluetoothSlaveHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        android.os.Debug.waitForDebugger();
        setContentView(R.layout.activity_interface);

        Intent i = getIntent();
        mBluetoothSlaveHandler = (BluetoothSlaveHandler)i.getSerializableExtra("EXTRA_BT_SLAVE_HANDLER");
        mBluetoothSlaveHandler.mBluetoothConnector.changeActivity(this);
        mBluetoothSlaveHandler.mBluetoothConnector.getmBluetoothAdapter();
    }

    @Override
    public void BluetoothOn() {

    }

    @Override
    public void toastToActivity(String message) {

    }
}
