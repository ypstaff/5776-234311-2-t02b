package yearlyproject.bluetooth;

import android.app.Activity;
import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.widget.Toast;

import java.io.IOException;
//import java.io.Serializable;
import java.util.UUID;
//import org.apache.commons.lang.SerializationUtils;

/**
 * Created by lmaman on 08-Dec-15.
 */
public class BluetoothConnector{

    private static final String TAG = BluetoothConnector.class.getSimpleName();
    static final int REQUEST_ENABLE_BT=1;
    static final int BT_CONNECTED = 2;
    static final String BT_MESSAGE_SEND ="BluetoothConnector.BT_MESSAGE_SEND";
    static final String BT_MESSAGE_RECEIVED ="BluetoothConnector.BT_MESSAGE_RECEIVED";
    static final UUID uuid = UUID.fromString("225cbd1e-9df3-11e5-8994-feff819cdc9f");
    static final String serverName="3-Buttons";

    //Parent app related variables
    private Context activityContext;

    private BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
    private BluetoothMasterHandler bluetoothMasterHandler;
    private BluetoothSlaveHandler bluetoothSlaveHandler;

    private final BroadcastReceiver discoverChangeReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            // When discovery mode changes
            if (BluetoothAdapter.ACTION_SCAN_MODE_CHANGED.equals(action)) {
                int previousState = intent.getIntExtra(BluetoothAdapter.EXTRA_PREVIOUS_SCAN_MODE,BluetoothAdapter.SCAN_MODE_NONE);
                int currentState = intent.getIntExtra(BluetoothAdapter.EXTRA_SCAN_MODE,BluetoothAdapter.SCAN_MODE_NONE);
                if(currentState == BluetoothAdapter.SCAN_MODE_NONE)
                    BluetoothConnector.this.toastToActivity("Bluetooth state is off, killing server.");
                    killServer();
            }
        }
    };



    public void initiate(Context context_) {
        this.activityContext = context_;

        if(mBluetoothAdapter == null){
            toastToActivity("Running on emulator, no Bluetooth capabilities.");
        }

        if (!mBluetoothAdapter.isEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            ((Activity) activityContext).startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
        }else{
            ((BluetoothAbleActivity)activityContext).BluetoothOn();
        }

    }

    public void discoverDevices() {

        if (mBluetoothAdapter.isDiscovering()){
            mBluetoothAdapter.cancelDiscovery();
        }

        if(mBluetoothAdapter.startDiscovery() == true)
        toastToActivity("Started searching for nearby devices.");
    }

    public void runServer(){
        bluetoothMasterHandler = new BluetoothMasterHandler(this);
        IntentFilter filter = new IntentFilter(BluetoothAdapter.ACTION_SCAN_MODE_CHANGED);
        activityContext.registerReceiver(discoverChangeReceiver, filter);
        //Run server on a different thread
        bluetoothMasterHandler.start();
    }

    public void connectAsServer(int duration){
        setDiscoverable(duration);
        //Wait for an onActivityResult to runServer
    }

    public void connectAsClient(BluetoothDevice mBluetoothDevice) throws IOException {
        bluetoothSlaveHandler = new BluetoothSlaveHandler(this,mBluetoothDevice);
        bluetoothSlaveHandler.start();
    }


    public void setDiscoverable(int duration){
        //Set device as discoverable
        Intent discoverableIntent = new
                Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
        discoverableIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, duration);
        ((Activity) activityContext).startActivityForResult(discoverableIntent, duration);
    }

    private void killServer(){
        try{
            activityContext.unregisterReceiver(discoverChangeReceiver);
        }catch(IllegalArgumentException e){
        }
        if(bluetoothMasterHandler != null)
            bluetoothMasterHandler.cancel();
        bluetoothMasterHandler = null;
    }

    private void killClient(){
        if(bluetoothSlaveHandler != null)
            bluetoothSlaveHandler.cancel();
        bluetoothSlaveHandler = null;
    }

    public void onDestroy() {
        killServer();
        killClient();
        if (mBluetoothAdapter.isDiscovering()){
            mBluetoothAdapter.cancelDiscovery();
        }

    }

    public void toastToActivity(final String message) {
        if(activityContext== null)
            return;
        ((Activity)activityContext).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ((BluetoothAbleActivity) activityContext).toastToActivity(message);
            }
        });

    }

    public void cancelDiscovery(){
        if (mBluetoothAdapter.isDiscovering()){
            mBluetoothAdapter.cancelDiscovery();
        }
    }

    public void send(String msg){
        //send msg, intent
        Intent intent= new Intent(BluetoothConnector.BT_MESSAGE_SEND);
        intent.putExtra("msg",msg);
        ((Activity) activityContext).sendBroadcast(intent);
    }

//    private void getReadyForActivitySwitch(){
//        activityContext=null;
//    }
//    public BluetoothConnector getSerializableCopy(){
//        Context oldContext=getActivityContext();
//        getReadyForActivitySwitch();
//        BluetoothConnector copy= (BluetoothConnector)SerializationUtils.clone(this);
//        activityContext=oldContext;
//        return copy;
//    }

//    public void changeActivity(Context context_){
//        activityContext=context_;
//    }

//    public void startActivity(Intent intent){
//
//    }

    //Getters
    public Context getActivityContext() {
        return activityContext;
    }

    public BluetoothAdapter getmBluetoothAdapter() {
        return mBluetoothAdapter;
    }



}



