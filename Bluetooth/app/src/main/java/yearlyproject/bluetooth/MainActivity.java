package yearlyproject.bluetooth;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

/**
 * Created by lmaman on 07-Dec-15.
 */

public class MainActivity extends AppCompatActivity implements BluetoothAbleActivity{
    private static final String TAG = MainActivity.class.getSimpleName();
    static int discoveraviltyDuration = 300; //seconds
    Thread connectedThread;

    BluetoothConnector bluetoothConnector=null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //android.os.Debug.waitForDebugger();
        setContentView(R.layout.activity_main);


        Button connectAsServerButton = (Button) findViewById(R.id.buttonServer);
        connectAsServerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bluetoothConnector = new BluetoothConnector();
                bluetoothConnector.initiate(MainActivity.this);

            }
        });

        Button connectAsClientButton = (Button) findViewById(R.id.buttonClient);
        connectAsClientButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, SlaveActivity.class);
                MainActivity.this.startActivity(intent);
            }
        });

        findViewById(R.id.buttonCancel).setVisibility(View.INVISIBLE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == BluetoothConnector.REQUEST_ENABLE_BT) {
            if (resultCode == RESULT_OK) {
                BluetoothOn();
            } else {
                Toast.makeText(this, "Bluetooth was not allowed to turn on, no Bluetooth capabilities.", Toast.LENGTH_SHORT).show();
                Log.d(TAG, "onActivityResult(REQUEST_ENABLE_BT) got RESULT_CANCEL. Bluetooth was not allowed to turn on, no Bluetooth capabilities");
            }
        }

        //Callback from setting on discoverablity
        if(requestCode == discoveraviltyDuration){
            if(resultCode == RESULT_CANCELED){
                Toast.makeText(this, "Bluetooth was not allowed to make the device discoverable, can't open a Bluetooth server.", Toast.LENGTH_LONG).show();
                Log.d(TAG, "onActivityResult(SetDiscover) got RESULT_CANCEL. Bluetooth was not allowed to make the device discoverable, can't open a Bluetooth server.");
            }
            else
            {

                bluetoothConnector.runServer();
                //go to app menu
            }

        }
    }

    public void BluetoothOn(){
        Toast.makeText(this, "Bluetooth turned on and ready.", Toast.LENGTH_SHORT).show();
        bluetoothConnector.connectAsServer(discoveraviltyDuration);
    }

    @Override
    public void toastToActivity(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void BluetoothConnected() {
        //TODO light something?
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                findViewById(R.id.buttonCancel).setVisibility(View.VISIBLE);
                findViewById(R.id.buttonClient).setVisibility(View.INVISIBLE);
                findViewById(R.id.buttonServer).setVisibility(View.INVISIBLE);

                ((Button) findViewById(R.id.buttonCancel)).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (connectedThread != null)
                            connectedThread.interrupt();
                        // get back to starting point
                        findViewById(R.id.buttonCancel).setVisibility(View.INVISIBLE);
                        findViewById(R.id.buttonClient).setVisibility(View.VISIBLE);
                        findViewById(R.id.buttonServer).setVisibility(View.VISIBLE);
                        bluetoothConnector=null;
                    }
                });
            }
        });


    }

    @Override
    public void setConnectedThread(Thread connectedThread) {
        this.connectedThread = connectedThread;
    }

    @Override
    public void onDestroy(){
        if (connectedThread != null)
            connectedThread.interrupt();
        if(bluetoothConnector != null)
            bluetoothConnector.onDestroy();
        super.onDestroy();
    }


}
