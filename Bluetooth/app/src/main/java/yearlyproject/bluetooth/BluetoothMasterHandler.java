package yearlyproject.bluetooth;

import android.app.Activity;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.util.Log;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

/**
 * Created by lmaman on 09-Dec-15.
 */
public class BluetoothMasterHandler extends Thread {
    private static final String TAG = BluetoothMasterHandler.class.getSimpleName();

    private final BluetoothServerSocket mmServerSocket;
    BluetoothConnector mBluetoothConnector;
    private BluetoothSocket mBluetoothSocket;

    private final BroadcastReceiver btMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            Log.d(TAG, "trying to get message");
            if (BluetoothConnector.BT_MESSAGE_RECEIVED.equals(action)) {
                    BluetoothMasterHandler.this.mBluetoothConnector.toastToActivity("received using intent: " + intent.getStringExtra("msg"));
            }
        }
    };


    public BluetoothMasterHandler(BluetoothConnector mBluetoothConnector) {
        BluetoothServerSocket tmp = null;
        try {
            tmp = mBluetoothConnector.getmBluetoothAdapter()
                    .listenUsingRfcommWithServiceRecord(BluetoothConnector.serverName,
                            BluetoothConnector.uuid);
        } catch (IOException e) { }
        mmServerSocket = tmp;
        this.mBluetoothConnector=mBluetoothConnector;

        mBluetoothConnector.toastToActivity("Connected as a server.");
    }

    public void run() {
        Log.d(TAG, "BluetoothMasterHandler.run() Waiting for connection");
        //BluetoothAdapter.getDefaultAdapter().
        BluetoothSocket socket = null;
        // Keep listening until exception occurs or a socket is returned
        while (true) {
            try {
                socket = mmServerSocket.accept();
            } catch (IOException e) {
                break;
            }
            // If a connection was accepted
            if (socket != null) {
                // Do work to manage the connection (in a separate thread)
                manageConnectedSocket(socket);
                try {
                    mmServerSocket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                    mBluetoothConnector.toastToActivity("Could not connect.");
                }
                break;
            }
        }
    }

    private void manageConnectedSocket(BluetoothSocket socket) {
        mBluetoothConnector.toastToActivity("Connected to a client device.");
        Log.d(TAG, "manageConnectedSocket() Managing connection,server side");
        //TODO
        mBluetoothSocket=socket;
        IntentFilter filter = new IntentFilter(BluetoothConnector.BT_MESSAGE_RECEIVED);
        mBluetoothConnector.getActivityContext().registerReceiver(btMessageReceiver, filter);
        ((BluetoothAbleActivity)mBluetoothConnector.getActivityContext()).BluetoothConnected();

        Thread connectedThread = new Thread(new Runnable() {
            @Override
            public void run() {
//                while (true){
//                    Intent intent= new Intent(BluetoothConnector.BT_MESSAGE_RECEIVED);
//                    BluetoothMasterHandler.this.mBluetoothConnector.getActivityContext().sendBroadcast(intent);
//                }
                while (!Thread.interrupted()) {
                    Log.d(TAG, "trying to get message");
                    byte[] buffer = new byte[1024];
                    try {
                        mBluetoothSocket.getInputStream().read(buffer); //blocking call
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    if(Thread.interrupted())
                        break;
                    Log.d(TAG, "got message");
                    try {
                        //BluetoothMasterHandler.this.mBluetoothConnector.toastToActivity("received: " + new String(buffer, "UTF-8"));
                        Intent intent= new Intent(BluetoothConnector.BT_MESSAGE_RECEIVED);
                        intent.putExtra("msg",new String(buffer, "UTF-8"));
                        BluetoothMasterHandler.this.mBluetoothConnector.getActivityContext().sendBroadcast(intent);

                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                }
                cancel();

            }
        });
        connectedThread.start();
        ((BluetoothAbleActivity)mBluetoothConnector.getActivityContext()).setConnectedThread(connectedThread);

    }

    /** Will cancel the listening socket, and cause the thread to finish */
    public void cancel() {
        try {
            mmServerSocket.close();
        } catch (IOException e) { }
        try{
            ((Activity)mBluetoothConnector.getActivityContext()).unregisterReceiver(btMessageReceiver);
        }catch(IllegalArgumentException e){
        }
        //mOpenServerSocket.cancel();
        //
        Log.d(TAG, "master.cancel() Killing master");
    }
}
