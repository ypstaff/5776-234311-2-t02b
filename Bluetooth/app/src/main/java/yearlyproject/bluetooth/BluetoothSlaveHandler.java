package yearlyproject.bluetooth;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.util.Log;

import java.io.IOException;

/**
 * Created by lmaman on 09-Dec-15.
 */
public class BluetoothSlaveHandler extends Thread {
    private static final String TAG = BluetoothSlaveHandler.class.getSimpleName();
    BluetoothConnector mBluetoothConnector;
    final BluetoothSocket mBluetoothSocket;



    private final BroadcastReceiver btMessageSender = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (BluetoothConnector.BT_MESSAGE_SEND.equals(action)) {
                String msg = intent.getStringExtra("msg");
                Log.d(TAG, "Trying to send message "+ msg);
                try {
                    mBluetoothSocket.getOutputStream().write(msg.getBytes("UTF-8"));
                    Log.d(TAG, "sent message " + msg);

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    };


    private final BroadcastReceiver btKilled = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (BluetoothAdapter.ACTION_CONNECTION_STATE_CHANGED.equals(action)) {
                BluetoothSlaveHandler.this.mBluetoothConnector.toastToActivity("Connection ended.");
                BluetoothSlaveHandler.this.cancel();
            }
        }
    };


    public BluetoothSlaveHandler(BluetoothConnector bluetoothConnector, BluetoothDevice mBluetoothDevice) {
        mBluetoothConnector=bluetoothConnector;
        BluetoothSocket tmp = null;
        try {
            tmp = mBluetoothDevice.createRfcommSocketToServiceRecord(BluetoothConnector.uuid);
        } catch (IOException e) { }
        mBluetoothSocket = tmp;

        IntentFilter filter = new IntentFilter(BluetoothAdapter.ACTION_CONNECTION_STATE_CHANGED);
        mBluetoothConnector.getActivityContext().registerReceiver(btKilled, filter);


    }

    @Override
    public void run(){
        // Cancel discovery because it will slow down the connection
        mBluetoothConnector.cancelDiscovery();

        try {
            // Connect the device through the socket. This will block
            // until it succeeds or throws an exception
            mBluetoothSocket.connect();
        } catch (IOException connectException) {
            // Unable to connect; close the socket and get out
            try {
                mBluetoothSocket.close();
            } catch (IOException closeException) {
                closeException.printStackTrace();}
            mBluetoothConnector.toastToActivity("Could not connect to device. Try again.");
            return;
        }

        // Do work to manage the connection (in a separate thread)
        manageConnectedSocket();
    }

    private void manageConnectedSocket() {
        mBluetoothConnector.toastToActivity("Connected to a server device.");
        Log.d(TAG, "manageConnectedSocket() Managing connection, client side.");
        //TODO
        IntentFilter filter = new IntentFilter(BluetoothConnector.BT_MESSAGE_SEND);
        mBluetoothConnector.getActivityContext().registerReceiver(btMessageSender, filter);
        //TODO enable activitie's buttons
        ((BluetoothAbleActivity)mBluetoothConnector.getActivityContext()).BluetoothConnected();


//        Intent intent = new Intent(mBluetoothConnector.getActivityContext(), InterfaceActivity.class);
//        intent.putExtra("EXTRA_BT_SLAVE_HANDLER", getSerializableCopy());
//        mBluetoothConnector.getActivityContext().startActivity(intent);

    }

    /** Will cancel an in-progress connection, and close the socket */
    public void cancel() {
        try {
            mBluetoothSocket.close();
        } catch (IOException e) { }
        try{
            ((Activity)mBluetoothConnector.getActivityContext()).unregisterReceiver(btMessageSender);
            ((Activity)mBluetoothConnector.getActivityContext()).unregisterReceiver(btKilled);
        }catch(IllegalArgumentException e){
        }
        Log.d(TAG, "slave.cancel() Killing slave");
    }

//    public BluetoothSlaveHandler getSerializableCopy(){
//        BluetoothConnector oldConnector=mBluetoothConnector;
//        mBluetoothConnector = mBluetoothConnector.getSerializableCopy();
//        BluetoothSlaveHandler copy= (BluetoothSlaveHandler) SerializationUtils.clone(this);
//        mBluetoothConnector = oldConnector;
//        return copy;
//    }

    // private final BluetoothSocket mSocket;

}
