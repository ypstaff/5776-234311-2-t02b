package yearlyproject.bluetooth;

import android.app.NotificationManager;
import android.app.Service;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.support.annotation.Nullable;


/**
 * Created by lmaman on 14-Dec-15.
 */
public class BluetoothService extends Service {
    private BluetoothSocket mBluetoothSocket;
    public class BluetoothBinder extends Binder {
        void injectBlueetoothSocket(BluetoothSocket mBluetoothSocket){
            BluetoothService.this.mBluetoothSocket = mBluetoothSocket;
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return new BluetoothBinder();
    }

}
