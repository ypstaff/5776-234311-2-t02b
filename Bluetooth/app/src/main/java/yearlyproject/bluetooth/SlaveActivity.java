package yearlyproject.bluetooth;

import android.app.ListActivity;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class SlaveActivity extends ListActivity implements BluetoothAbleActivity{
    private static final String TAG = SlaveActivity.class.getSimpleName();


    ArrayAdapter<String> adapter;
    BluetoothConnector bluetoothConnector = new BluetoothConnector();
    final List<String> mArrayAdapter= new ArrayList<>();
    final List<BluetoothDevice> devices = new ArrayList<>();

    // Create a BroadcastReceiver for ACTION_FOUND
    final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            // When discovery finds a device
            if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                // Get the BluetoothDevice object from the Intent
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                devices.add(device);
                // Add the name and address to an array adapter to show in a ListView
                mArrayAdapter.add(device.getName() + "\n" + device.getAddress());
//                Toast.makeText(MainActivity.this, "Discovered: " + device.getName() + "\n" + device.getAddress(), Toast.LENGTH_SHORT).show();
                adapter.notifyDataSetChanged();
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //android.os.Debug.waitForDebugger();
        setContentView(R.layout.activity_slave);

        adapter=new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1,
                mArrayAdapter);
        setListAdapter(adapter);

        bluetoothConnector.initiate(this);

        this.getListView().setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, final View view,
                                    int position, long id) {
                final String item = (String) parent.getItemAtPosition(position);
                Toast.makeText(SlaveActivity.this, "Selected: " + item, Toast.LENGTH_LONG).show();
                try {
                    bluetoothConnector.connectAsClient(devices.get(position));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

        findViewById(R.id.select).setVisibility(View.INVISIBLE);
        findViewById(R.id.next).setVisibility(View.INVISIBLE);
        findViewById(R.id.back).setVisibility(View.INVISIBLE);

    }

    @Override
    protected void onActivityResult (int requestCode, int resultCode, Intent data){
        if (requestCode == BluetoothConnector.REQUEST_ENABLE_BT) {
            if (resultCode == RESULT_OK) {
                BluetoothOn();
            } else {
                Toast.makeText(this, "Bluetooth was not allowed to turn on, no Bluetooth capabilities.", Toast.LENGTH_SHORT).show();
                Log.d(TAG, "onActivityResult(REQUEST_ENABLE_BT) got RESULT_CANCEL. Bluetooth was not allowed to turn on, no Bluetooth capabilities");
            }
        }

    }

    public void enableButtons(){
        Button selectButton = (Button) findViewById(R.id.select);
        selectButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bluetoothConnector.send("select");
            }
        });

        Button nextButton = (Button) findViewById(R.id.next);
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bluetoothConnector.send("next");
            }
        });

        Button backButton = (Button) findViewById(R.id.back);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bluetoothConnector.send("back");
            }
        });

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                findViewById(R.id.select).setVisibility(View.VISIBLE);
                findViewById(R.id.next).setVisibility(View.VISIBLE);
                findViewById(R.id.back).setVisibility(View.VISIBLE);
            }
        });


    }

    @Override
    public void BluetoothOn(){
        Toast.makeText(this, "Bluetooth turned on and ready.", Toast.LENGTH_SHORT).show();
        // Register the BroadcastReceiver
        IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
        registerReceiver(mReceiver, filter); // Don't forget to unregister during onDestroy
        bluetoothConnector.discoverDevices();
    }

    @Override
    protected void onDestroy(){
        try{
            unregisterReceiver(mReceiver);
        }catch(IllegalArgumentException e){
        }
        bluetoothConnector.onDestroy();

        super.onDestroy();
    }

    @Override
    public void toastToActivity(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void BluetoothConnected() {
        enableButtons();
    }

    @Override
    public void setConnectedThread(Thread connectedThread) {
        //TODO maybe
    }

}
