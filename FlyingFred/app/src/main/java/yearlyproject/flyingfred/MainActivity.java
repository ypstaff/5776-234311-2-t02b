package yearlyproject.flyingfred;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
        import android.graphics.Bitmap.Config;
        import android.os.Bundle;
        import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.Window;
        import android.view.WindowManager;

/**
 * @author Roy Svinik
 */
public class MainActivity extends Activity {

    private GameView gameView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Go to fullscreen
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        // Create the framebuffer
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        Bitmap frameBuffer = Bitmap.createBitmap(dm.widthPixels, dm.heightPixels, Config.ARGB_8888);

        gameView = new GameView(this, frameBuffer);
        setContentView(gameView);
    }

    //bluetooth messages receiver
    private final BroadcastReceiver btMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context c, Intent i) {
            if ("BluetoothConnector.BT_MESSAGE_RECEIVED".equals(i.getAction()))
				performAction(i.getStringExtra("msg").split("\\u0000")[0]);
        }
    };

    public void performAction(final String s) {
        switch (s) {
            case "single press":
            case "next":
            case "select":
                gameView.onTouchEvent(MotionEvent.obtain(1, 1, MotionEvent.ACTION_DOWN, 0, 0, 0));
                break;
            case "long press":
            case "back":
                //kill game
                finish();
                break;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        //screen.resume();
        registerReceiver(btMessageReceiver, new IntentFilter("BluetoothConnector.BT_MESSAGE_RECEIVED"));
        gameView.resume();
    }

    @Override
    public void onPause() {
        unregisterReceiver(btMessageReceiver);
        super.onPause();
        gameView.pause();
    }

}
