package yearlyproject.flyingfred;

import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;

/**
 * @author Roy Svinik
 */
public class RotatingObstacle extends Obstacle {

    private Matrix matrix;
    float angle = 0;

@Override
    public void advance(float deltaTime, int screenWidth) {
        super.advance(deltaTime, screenWidth);
        //rotate bitmap by 1 degree
        angle++;
        matrix = new Matrix();
        matrix.postRotate(angle, bitmap.getWidth() / 2, bitmap.getHeight() / 2);
        matrix.postTranslate(xPosition, yPosition);
    }

@Override
    public void draw(Canvas c, Paint p) {
        if (matrix != null)
			c.drawBitmap(bitmap, matrix, p);
		else {
			matrix = new Matrix();
			matrix.postRotate(angle, bitmap.getWidth() / 2,
					bitmap.getHeight() / 2);
			matrix.postTranslate(xPosition, yPosition);
		}
    }
}
