package yearlyproject.flyingfred;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;

/**
 * A class for presenting an element on the screen
 * @author Roy Svinik
 */
public class Element {

    //current element position
    public float xPosition, yPosition;

    public Bitmap bitmap;

    /**
     * Method for checking collisions with other elements
     * simple rectangle congruence check
     * @param other - the other element
     * @return true if there is a collision, false otherwise
     */
    public boolean isCollidingWith(Element other) {
        return xPosition + bitmap.getWidth() >= other.xPosition
				&& other.xPosition + other.bitmap.getWidth() >= xPosition
				&& yPosition + bitmap.getHeight() >= other.yPosition
				&& other.yPosition + other.bitmap.getHeight() >= yPosition;
    }

    /**
     * Draws the element bitmap on the screen using canvas and paint
     * @param c - the canvas you wish to draw the element on
     * @param p
     */
    public void draw(Canvas c, Paint p) {
        c.drawBitmap(bitmap, xPosition, yPosition, p);
    }
}
