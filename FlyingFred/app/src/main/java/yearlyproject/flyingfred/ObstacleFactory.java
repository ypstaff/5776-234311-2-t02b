package yearlyproject.flyingfred;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.SurfaceView;

/**
 * A class for generating obstacles
 * @author Roy Svinik
 */
public class ObstacleFactory {

    Bitmap ninjaStar1Bitmap;

    ObstacleFactory(SurfaceView context) {
        //set up obstacles bitmaps
        ninjaStar1Bitmap = BitmapFactory.decodeResource(context.getResources(),
                R.drawable.ninja_star_1);
    }

    /**
     * Generates a new obstacle with random location and speed
     * @param screenWidth
     * @param screenHeight
     * @return a new obstacle
     */
    public Obstacle getObstacle(float screenWidth, float screenHeight, float bottomHeight) {
        Obstacle $ = new RotatingObstacle();
        //set obstacle bitmap - for now only ninja star
        $.bitmap = ninjaStar1Bitmap;
        // Set the obstacle X position out of the screen (The screen is moving from right to left)
        $.xPosition = screenWidth;
        // create the the obstacle at a random Y position
        $.yPosition = (int) (Math.random() * (screenHeight - bottomHeight));
        //set obstacle speed
        $.setMoveTime(randomFloat(4,6));
        return $;
    }

    /**
     * generates a random float in the range [from,to)
     * @param from
     * @param to
     * @return random float
     */
    private float randomFloat(float from, float to) {
        return (from - to) * (float) Math.random() + from;
    }

}
