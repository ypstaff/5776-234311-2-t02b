package yearlyproject.flyingfred;

/**
 * A class for representing an obstacle
 * @author Roy Svinik
 */
public class Obstacle extends Element {

    //The number of seconds the obstacle pass from the right side of the screen to the left one
    private float moveTime = 6f;

    /**
     * advance the obstacle
     * default implementation uses X = V * t formula
     */
    public void advance(float deltaTime, int screenWidth) {
        xPosition -= (deltaTime / 1000) * (int) (screenWidth / moveTime);
    }

    public void setMoveTime(float time) {
        moveTime = time;
    }
}
