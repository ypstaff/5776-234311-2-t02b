package yearlyproject.flyingfred;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.util.ArrayList;
import java.util.List;

/**
 * A class for orginizing the flow of the game
 * @author Roy Svinik
 */
public class GameView extends SurfaceView implements Runnable {

    private static final String TAG = "FlyingFred";

    /**
     *  Defines the gravity (The smaller - The faster fred will fall)
     */
    private static final float GRAVITY_FACTOR = 5f;

    private Bitmap framebuffer;
    private Thread renderThread = null;
    private SurfaceHolder holder;
    volatile boolean running = false;

    private enum GameState {
        BeforeStart,	// Game has not started, showing 'start' option
        Running,		// Game is running
        Finished		// Game is finished (after user crash)
    }

    /**
     * 	Holds the current game state according to {@link}{@link GameState} value}
     */
    private GameState gameState;

    /**
     * The total time Fred is flying
     * (used to calculate the current state of the wings that change every 500 ms)
     */
    private float fredTotalFlyTime = 0;

    /**
     * The total time Fred is in the air after user tap the screen
     * (used to calculate Fred's speed and height using physics
     */
    private float fredFallTime;

    private float fredStartYPosition = 0;

    /**
     * When this value is > 0 Fred get a 'boost' up (We use it when the user tap the screen)
     */
    private float fredBoostUpSpeed;

    /**
     * Current obstacles
     */
    private List<Obstacle> obstacles = new ArrayList<>();

    /**
     * the number of milliseconds an obstacle is created
     */
    private float obstacleFrequency;

    /**
     *  the last time we created an obstacle
     */
    private float lastObstacleCreationTime;

    /**
     * Fred's element, holds his X, Y positions and bitmap
     */
    private Element fred = new Element();

    /**
     * Fred's current angle
     */
    private float fredAngle;

    private Bitmap fredWingsUpBitmap, fredWingsDownBitmap;
    private Bitmap backgroundImage;
    private Bitmap backgroundBottom;
    private Bitmap startButtonBitmap;
    private Bitmap finishButtonBitmap;

    /**
     * Factory for creating new obstacles
     */
    ObstacleFactory obstacleFactory;

    /**
     * Holds the current background bottom line position
     * (we move it to give a feeling that the background is moving)
     */
    private float backgroundBottmXPosition;

    /**
     * Number of point gained for the current game session
     */
    private int gamePoints;

    /**
     * The total of obstacles created in the current game session
     * (used to help calculate the user points)
     */
    private int numOfObstaclesCreated;

    /**
     * Holds Fred's current wings state.
     * If 'true' Fred's wings up bitmap is used, if 'false' Fred's wings down is used
     */
    private boolean areFredWingsUp;

    /**
     * In order to keep the same game experience for each device
     * this variable (that depends on the height of the screen) is calculated on game start
     */
    private float deviceGravity;

    private Paint paint;

    public GameView(Context context, Bitmap framebuffer) {
        super(context);

        this.framebuffer = framebuffer;
        this.holder = getHolder();

        paint = new Paint();
        paint.setColor(Color.BLACK);
        paint.setTextSize(60);

        prepareAssets();

        ResetGame();
    }

    /**
     * Resume the game
     */
    public void resume() {
        running = true;
        renderThread = new Thread(this);
        renderThread.start();
    }

    /**
     * Pause the game
     */
    public void pause() {
        running = false;
        while (true)
			try {
				renderThread.join();
				break;
			} catch (InterruptedException e) {}
    }

    public void run() {
        Rect dstRect = new Rect();
        long startTime = System.nanoTime();

        while (running) {
            if (!holder.getSurface().isValid())
                continue;

            // Get the time in ms
            float deltaTime = (System.nanoTime() - startTime) / 1000000.0f;
            startTime = System.nanoTime();

            if (backgroundImage == null)
				prepareAssets();

            updateLogic(deltaTime);
            updateView();

            // Draw the framebuffer
            Canvas canvas = holder.lockCanvas();
            canvas.getClipBounds(dstRect);
            canvas.drawBitmap(framebuffer, 0, 0, null);
            holder.unlockCanvasAndPost(canvas);
        }
    }

    /**
     * Draw all the game elements on the screen
     */
    private void updateView() {
        Canvas canvas = new Canvas(framebuffer);

        // Draw the background
        canvas.drawBitmap(backgroundImage, 0, 0, paint);

        // Draw the obstacles
        try {
            for (Obstacle obstacle : obstacles)
				obstacle.draw(canvas, paint);
        } catch (Exception e) {}

        // Decide which Fred image to show (wings up or wings down)
        fred.bitmap = areFredWingsUp ? fredWingsUpBitmap : fredWingsDownBitmap;

        // Create a rotated Fred bitmap according to the angle
        Matrix matrix = new Matrix();
        matrix.postRotate(fredAngle, fred.bitmap.getWidth() / 2, fred.bitmap.getHeight() / 2);
        matrix.postTranslate(fred.xPosition, fred.yPosition);

        // Draw Fred
        canvas.drawBitmap(fred.bitmap, matrix, paint);

        // Draw the bottom moving background part
        canvas.drawBitmap(backgroundBottom, backgroundBottmXPosition, framebuffer.getHeight() - backgroundBottom.getHeight(), paint);
        canvas.drawBitmap(backgroundBottom, backgroundBottmXPosition, framebuffer.getHeight() - backgroundBottom.getHeight(), paint);

        // Show the game points
        paint.setColor(Color.BLACK);
        canvas.drawText(Integer.toString(gamePoints), 32, 62, paint);
        paint.setColor(Color.WHITE);
        canvas.drawText(Integer.toString(gamePoints), 30, 60, paint);

        if (gameState.equals(GameState.BeforeStart))
			canvas.drawBitmap(
					startButtonBitmap,
					(framebuffer.getWidth() - startButtonBitmap.getWidth()) / 2,
					framebuffer.getHeight() / 5, paint);
		else if (gameState.equals(GameState.Finished))
			canvas.drawBitmap(
					finishButtonBitmap,
					(framebuffer.getWidth() - startButtonBitmap.getWidth()) / 2,
					framebuffer.getHeight() / 5, paint);
    }

    private void prepareAssets() {

        fredWingsUpBitmap = BitmapFactory.decodeResource(getResources(),
                R.drawable.droid_wings_up);

        fredWingsDownBitmap = BitmapFactory.decodeResource(getResources(),
                R.drawable.droid_wings_down);

        startButtonBitmap = BitmapFactory.decodeResource(getResources(),
                R.drawable.start_button);

        finishButtonBitmap = BitmapFactory.decodeResource(getResources(),
                R.drawable.finish_button);

        obstacleFactory = new ObstacleFactory(this);

        Bitmap originaBackgroundImage = BitmapFactory.decodeResource(getResources(),
                R.drawable.game_background);

        Bitmap originaBackgroundBottom = BitmapFactory.decodeResource(getResources(),
                R.drawable.background_bottom);

        float scaleWidth = ((float) framebuffer.getWidth()) / originaBackgroundImage.getWidth();
        float scaleHeight = ((float) framebuffer.getHeight()) / originaBackgroundImage.getHeight();

        Matrix matrix = new Matrix();
        matrix.postScale(scaleWidth, scaleHeight);

        // Create the assets scaled to this specific screen size
        backgroundImage = Bitmap.createBitmap(originaBackgroundImage, 0, 0, originaBackgroundImage.getWidth(), originaBackgroundImage.getHeight(), matrix, false);
        backgroundBottom = Bitmap.createBitmap(originaBackgroundBottom, 0, 0, originaBackgroundBottom.getWidth(), originaBackgroundBottom.getHeight(), matrix, false);

        if (backgroundImage != originaBackgroundImage)
			originaBackgroundImage.recycle();

        if (backgroundBottom != originaBackgroundBottom)
			originaBackgroundBottom.recycle();
    }

    /**
     * Update all the game variables according to the given time
     *
     * @param deltaTime in ms from the last cycle
     */
    private void updateLogic(float deltaTime) {

        if (!gameState.equals(GameState.Running))
			return;
		if (fredTotalFlyTime - lastObstacleCreationTime > obstacleFrequency) {
			createNewObstacle();
			lastObstacleCreationTime = fredTotalFlyTime;
		}
		List<Obstacle> outOfRange = new ArrayList<>();
		for (int i = 0; i < obstacles.size(); ++i)
			if (obstacles.get(i).xPosition + obstacles.get(i).bitmap.getWidth() < 0) {
				outOfRange.add(obstacles.get(i));
				++gamePoints;
			}
		obstacles.removeAll(outOfRange);
		for (Obstacle obstacle : obstacles)
			obstacle.advance(deltaTime, framebuffer.getWidth());
		fredFallTime += deltaTime;
		fred.yPosition = (deviceGravity * (fredFallTime / 1000) * (fredFallTime / 1000))
				- fredBoostUpSpeed * (fredFallTime / 1000) + fredStartYPosition;
		float fredVSpeed = deviceGravity * (fredFallTime / 1000)
				- fredBoostUpSpeed;
		fredAngle = fredVSpeed <= 0 ? -30 : -30 + fredVSpeed / 2;
		fredTotalFlyTime += deltaTime;
		areFredWingsUp = fredTotalFlyTime % 500 > 250;
		backgroundBottmXPosition -= 4f;
		if (backgroundBottmXPosition < -framebuffer.getWidth())
			backgroundBottmXPosition += framebuffer.getWidth();
		checkCollision();
		updateObstaclesFrequency();
    }

    /**
     * Check if there is a collision between Fred and the other elements
     */
    private void checkCollision() {

        boolean doWeHaveCollision = false;

        // Check if the Fred collide horizontally with an obstacle
        for (Obstacle obstacle : obstacles) {
            doWeHaveCollision = fred.isCollidingWith(obstacle);
            if (doWeHaveCollision)
				break;
        }

        if (!doWeHaveCollision
				&& fred.yPosition + fredWingsDownBitmap.getHeight() > framebuffer
						.getHeight() - backgroundBottom.getHeight()) {
			Log.i(TAG, "Fred just crashed into the floor");
			doWeHaveCollision = true;
		}

        if (doWeHaveCollision) //stop the game
			gameState = GameState.Finished;
    }

    /**
     * Creates a new wall and place it to the right of the screen (off the screen)
     */
    private void createNewObstacle() {
        int screenWidth = this.framebuffer.getWidth();
        int screenHeight = this.framebuffer.getHeight();

        obstacles.add(obstacleFactory.getObstacle(screenWidth, screenHeight, backgroundBottom.getHeight()));

        ++numOfObstaclesCreated;
    }

    @Override
    public boolean onTouchEvent(MotionEvent e) {
        if (e.getAction() == MotionEvent.ACTION_DOWN) {
            if (gameState.equals(GameState.BeforeStart)) // The user started the game
                gameState = GameState.Running;

            if (gameState.equals(GameState.Finished)) // The user re-started the game
				ResetGame();

            if (fred.yPosition <= 0)
				Log.d(TAG, "Fred is out of screen - do nothing with touch event");
			else {
				Log.i(TAG, "Fred boost");
				fredStartYPosition = fred.yPosition;
				fredFallTime = 0;
				fredBoostUpSpeed = 2f * deviceGravity;
			}
        }
        return super.onTouchEvent(e);
    }

    /**
     * Reset all game values and prepare for a new game
     */
    private void ResetGame() {
        Log.i(TAG, "Reset Game");
        gameState = GameState.BeforeStart;
        backgroundBottmXPosition = 0;

        // Place the fred ~ in the left side of the screen
        fred.yPosition = (framebuffer.getHeight() / 2);
        fred.xPosition = framebuffer.getWidth() / 10;
        
        numOfObstaclesCreated = gamePoints = 0;
        fredFallTime = fredAngle = fredBoostUpSpeed = fredTotalFlyTime = 0;
        areFredWingsUp = true;
        deviceGravity = framebuffer.getHeight() / GRAVITY_FACTOR;
        obstacles.clear();
        updateObstaclesFrequency();
        lastObstacleCreationTime = 0;
        createNewObstacle();
    }

    /**
     * updates obstacles frequency according to game points
     */
    private void updateObstaclesFrequency() {
        obstacleFrequency = gamePoints == 0 ? 6000 : gamePoints < 5 ? 5000
				: gamePoints < 10 ? 4000 : gamePoints < 15 ? 3000
						: gamePoints < 20 ? 2000 : 1000;
    }
}
