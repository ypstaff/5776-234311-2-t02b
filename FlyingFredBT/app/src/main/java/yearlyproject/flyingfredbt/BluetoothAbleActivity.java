package yearlyproject.flyingfredbt;

/**
 * Created by lmaman on 09-Dec-15.
 */
public interface BluetoothAbleActivity {
    void BluetoothOn();
    void toastToActivity(String message);
    void BluetoothConnected();
    void performAction(String s);
    void connectionDisconected();
}
