package yearlyproject.flyingfredbt;

import android.app.Activity;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.util.Log;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

/**
 * Created by lmaman on 09-Dec-15.
 */
public class BluetoothMasterHandler extends Thread {
    private static final String TAG = BluetoothMasterHandler.class.getSimpleName();

    private final BluetoothServerSocket mmServerSocket;
    BluetoothConnector mBluetoothConnector;
    private BluetoothSocket mBluetoothSocket;
    Thread connectedThread;

    private final BroadcastReceiver btMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (BluetoothConnector.BT_MESSAGE_RECEIVED.equals(action)) {
                String msg = intent.getStringExtra("msg");
                //BluetoothMasterHandler.this.mBluetoothConnector.toastToActivity("received using intent: " + msg);
                BluetoothMasterHandler.this.mBluetoothConnector.performAction(msg.split("\\u0000")[0]);
            }
        }
    };


    public BluetoothMasterHandler(BluetoothConnector mBluetoothConnector) {
        BluetoothServerSocket tmp = null;
        try {
            tmp = mBluetoothConnector.getmBluetoothAdapter()
                    .listenUsingRfcommWithServiceRecord(BluetoothConnector.serverName,
                            BluetoothConnector.uuid);
        } catch (IOException e) {
        }
        mmServerSocket = tmp;
        this.mBluetoothConnector = mBluetoothConnector;
        mBluetoothConnector.toastToActivity("Connected as a server.");
    }

    public void run() {
        Log.d(TAG, "BluetoothMasterHandler.run() Waiting for connection");
        //BluetoothAdapter.getDefaultAdapter().
        BluetoothSocket socket = null;
        // Keep listening until exception occurs or a socket is returned
        while (!Thread.interrupted()) {
            try {
                socket = mmServerSocket.accept();
            } catch (IOException e) {
                break;
            }
            // If a connection was accepted
            if (socket != null) {
                // Do work to manage the connection (in a separate thread)
                manageConnectedSocket(socket);
                try {
                    mmServerSocket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                    mBluetoothConnector.toastToActivity("Could not connect.");
                }
                break;
            }
        }
    }

    private void manageConnectedSocket(BluetoothSocket socket) {
        mBluetoothConnector.toastToActivity("Connected to a client device.");
        Log.d(TAG, "manageConnectedSocket() Managing connection,server side");
        //TODO
        mBluetoothSocket = socket;
        IntentFilter filter = new IntentFilter(BluetoothConnector.BT_MESSAGE_RECEIVED);
        mBluetoothConnector.getActivityContext().registerReceiver(btMessageReceiver, filter);
        ((BluetoothAbleActivity) mBluetoothConnector.getActivityContext()).BluetoothConnected();
        connectedThread = new Thread(new Runnable() {
            @Override
            public void run() {
                Log.d(TAG, "Listening thread started ");
                while (!Thread.interrupted()) {
                    Log.d(TAG, "trying to get message");
                    byte[] buffer = new byte[1024];
                    int resVal = 0;
                    try {
                        resVal = mBluetoothSocket.getInputStream().read(buffer); //blocking call
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    String msg = null;
                    try {
                        msg = new String(buffer, "UTF-8");
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                    if (Thread.interrupted() || resVal == 0)
                        break;
                    Log.d(TAG, "got message");
                    //BluetoothMasterHandler.this.mBluetoothConnector.toastToActivity("received: " + new String(buffer, "UTF-8"));
                    Intent intent = new Intent(BluetoothConnector.BT_MESSAGE_RECEIVED);

                    intent.putExtra("msg", msg);
                    BluetoothMasterHandler.this.mBluetoothConnector.getActivityContext().sendBroadcast(intent);
                }
                cancel();

            }
        });
        connectedThread.start();
    }

    /**
     * Will cancel the listening socket, and cause the thread to finish
     */
    private void cancel() {
        try {
            mmServerSocket.close();
            mBluetoothSocket.close();
        } catch (IOException e) {
        }
        try {
            ((Activity) mBluetoothConnector.getActivityContext()).unregisterReceiver(btMessageReceiver);
        } catch (IllegalArgumentException e) {
        }
        //mOpenServerSocket.cancel();
        //
        Log.d(TAG, "master.cancel() Killing master");
        ((BluetoothAbleActivity) mBluetoothConnector.getActivityContext()).connectionDisconected();
    }

    @Override
    public void interrupt(){
        try {
            if(mBluetoothSocket != null)
                mBluetoothSocket.getInputStream().close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        if(connectedThread != null)
            connectedThread.interrupt();
        super.interrupt();
    }
}
