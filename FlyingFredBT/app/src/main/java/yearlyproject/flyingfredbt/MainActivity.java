package yearlyproject.flyingfredbt;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

/**
 * Created by lmaman on 07-Dec-15.
 */

public class MainActivity extends AppCompatActivity implements BluetoothAbleActivity {
    private static final String TAG = MainActivity.class.getSimpleName();
    static int discoveraviltyDuration = 300; //seconds

    BluetoothConnector bluetoothConnector = null;

    //FlyingFred game
    private GameView gameView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // Go to fullscreen
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        super.onCreate(savedInstanceState);
        //android.os.Debug.waitForDebugger();

        setDefaultSetup();
    }

    protected void setDefaultSetup() {

        setContentView(R.layout.activity_main);
        findViewById(R.id.loadingPanel).setVisibility(View.INVISIBLE);
        Button connectAsServerButton = (Button) findViewById(R.id.buttonServer);
        connectAsServerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                findViewById(R.id.loadingPanel).setVisibility(View.VISIBLE);
                if (bluetoothConnector == null) {
                    bluetoothConnector = new BluetoothConnector();
                    bluetoothConnector.initiate(MainActivity.this);
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == BluetoothConnector.REQUEST_ENABLE_BT) {
            if (resultCode == RESULT_OK) {
                BluetoothOn();
            } else {
                Toast.makeText(this, "Bluetooth was not allowed to turn on, no Bluetooth capabilities.", Toast.LENGTH_SHORT).show();
                Log.d(TAG, "onActivityResult(REQUEST_ENABLE_BT) got RESULT_CANCEL. Bluetooth was not allowed to turn on, no Bluetooth capabilities");
            }
        }

        //Callback from setting on discoverablity
        if (requestCode == discoveraviltyDuration) {
            if (resultCode == RESULT_CANCELED) {
                Toast.makeText(this, "Bluetooth was not allowed to make the device discoverable, can't open a Bluetooth server.", Toast.LENGTH_LONG).show();
                Log.d(TAG, "onActivityResult(SetDiscover) got RESULT_CANCEL. Bluetooth was not allowed to make the device discoverable, can't open a Bluetooth server.");
            } else {

                bluetoothConnector.runServer();
                //go to app menu
            }
        }
    }

    public void BluetoothOn() {
        Toast.makeText(this, "Bluetooth turned on and ready.", Toast.LENGTH_SHORT).show();
        bluetoothConnector.connectAsServer(discoveraviltyDuration);
    }

    @Override
    public void toastToActivity(final String message) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(MainActivity.this, message, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void BluetoothConnected() {
        //start game
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                findViewById(R.id.loadingPanel).setVisibility(View.INVISIBLE);

                // Create the framebuffer
                DisplayMetrics dm = new DisplayMetrics();
                getWindowManager().getDefaultDisplay().getMetrics(dm);
                Bitmap frameBuffer = Bitmap.createBitmap(dm.widthPixels, dm.heightPixels, Bitmap.Config.ARGB_8888);

                gameView = new GameView(MainActivity.this, frameBuffer);
                setContentView(gameView);
                gameView.resume();
            }
        });
    }

    @Override
    public void performAction(final String s) {
        switch (s) {
            case "single press":
            case "next":
            case "select":
                gameView.onTouchEvent(MotionEvent.obtain(1, 1, MotionEvent.ACTION_DOWN, 0, 0, 0));
                break;
            case "long press":
            case "back":
                //kill game
                toastToActivity("Long press - killing game and connection ");
                endSession();
                break;
            default:
                return;
        }
        //toastToActivity("recived: "+ s);
    }

    @Override
    public void connectionDisconected() {
        toastToActivity("BT connection ended. ");
        endSession();
    }

    private void endSession() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (gameView != null)
                    gameView.pause();
                if (bluetoothConnector != null) {
                    bluetoothConnector.onDestroy();
                    bluetoothConnector = null;
                }
                setDefaultSetup();
            }
        });
    }

    @Override
    public void onDestroy() {
        if (bluetoothConnector != null)
            bluetoothConnector.onDestroy();
        super.onDestroy();
    }

    @Override
    public void onResume() {
        super.onResume();
        //screen.resume();
        if (gameView != null)
            gameView.resume();
    }

    @Override
    public void onPause() {
        super.onPause();
        if (gameView != null)
            gameView.pause();
    }


}
