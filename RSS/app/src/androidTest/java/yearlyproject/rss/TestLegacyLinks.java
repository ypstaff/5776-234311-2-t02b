package yearlyproject.rss;

import android.app.Instrumentation;
import android.content.Intent;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;
import android.test.ActivityInstrumentationTestCase2;
import android.util.Log;

import com.robotium.solo.Solo;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import yearlyproject.rssOperations.ArticlesListActivity;

/**
 * @author kfirlan
 *         this test check if the file links.txt contatin illegal links
 *         this test have difficulty to handle big files. can check about 10 links at a time
 */
@RunWith(AndroidJUnit4.class)
public class TestLegacyLinks extends ActivityInstrumentationTestCase2<ArticlesListActivity> {
    private static int currentLine = 1;
    @Rule
    public RepeatRule repeatRule = new RepeatRule();
    private ArticlesListActivity articlesListActivity;
    private BufferedReader reader = null;
    private Instrumentation instrumentation;
    private String url = null;


    public TestLegacyLinks() throws IOException {
        super(ArticlesListActivity.class);
    }

    @Override
    @Before
    public void setUp() throws Exception {
        super.setUp();
        injectInstrumentation(InstrumentationRegistry.getInstrumentation());
        instrumentation = getInstrumentation();
        InputStream links = instrumentation.getContext().getResources().getAssets().open("links.txt");
        reader = new BufferedReader(new InputStreamReader(links));
    }

    @Override
    @After
    public void tearDown() throws Exception {
        if (articlesListActivity != null) {
            articlesListActivity.finish();
            setActivity(null);
        }
        super.tearDown();
        Thread.sleep(2000);
    }

    private void setUrl(String url) {
        Intent intent = new Intent();
        intent.setClassName("yearlyproject.rssOperations", "yearlyproject.rssOperations.RssReaderActivity");
        intent.putExtra("XML_LINK", url);
        setActivityIntent(intent);
    }

    @Test
    @RepeatRule.Repeat(times = 10)
    public void testLink() throws Exception {
        for (int i = 0; i < currentLine; i++) {
            url = reader.readLine();
            if (url == null) {
                return;
            }
        }
        Log.d("tst", "line:" + currentLine);
        currentLine++;
        setUrl(url);
        articlesListActivity = getActivity();
        if (articlesListActivity == null) {
            fail("activity is null");
        }
        Solo solo = new Solo(instrumentation, articlesListActivity);
        solo.waitForActivity(articlesListActivity.getClass());
        boolean illegal = solo.waitForText("Link has no RSS feed available");  //FIXME use string resource instead of times
//            assertFalse("illegal url:"+url,illegal);
        if (illegal) {
            Log.e("url", ".*" + url + ".*");
        }
//            url = reader.readLine();
    }
}
