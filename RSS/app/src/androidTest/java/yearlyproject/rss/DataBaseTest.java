package yearlyproject.rss;

import android.support.test.runner.AndroidJUnit4;

import junit.framework.Assert;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.List;

import yearlyproject.database.Category;
import yearlyproject.database.DataBaseHelper;
import yearlyproject.database.LinkToRSS;

/**
 * @author kfirlan
 *         <p/>
 *         TODO read the fucking comment
 *         THESE TESTS WILL DESTROY THE DATABASE
 *         these tests clear the app database.
 *         run these tests on emulator.
 *         <p/>
 *         these tests are designed to check the DataBaseHelper class.
 *         they check SQL queries.
 */
@RunWith(AndroidJUnit4.class)
public class DataBaseTest {
    DataBaseHelper dataBaseHelper;

    @AfterClass
    public static void clear() {
        LinkToRSS.deleteAll(LinkToRSS.class);
        Category.deleteAll(Category.class);
    }

    @Before
    public void setUp() {
        clear();
        dataBaseHelper = DataBaseHelper.getInstance();
    }

    @Test
    public void categoryExistAfterAddingIt() {
        boolean exist = dataBaseHelper.categoryExist("tst");
        Assert.assertFalse("category exist before adding it", exist);
        new Category("tst", null).save();
        exist = dataBaseHelper.categoryExist("tst");
        Assert.assertTrue(exist);
    }

    @Test
    public void categoryListIsntEmptyAfterAdd() {
        List<String> categories = dataBaseHelper.getCategoriesTitles();
        Assert.assertTrue("categories list should be empty before insert", categories.isEmpty());
        new Category("tst1", null).save();
        new Category("tst2", null).save();
        categories = dataBaseHelper.getCategoriesTitles();
        Assert.assertFalse(categories.isEmpty());
        Assert.assertEquals(2, categories.size());
        // should sort by title
        Assert.assertEquals("tst1", categories.get(0));
        Assert.assertEquals("tst2", categories.get(1));
    }

    @Test
    public void shouldRemoveCategoryAndAllItsLinks() {
        createCategoryWith2Links();
        //test the delete
        dataBaseHelper.deleteCategory("tst1");
        long size = Category.count(Category.class);
        Assert.assertEquals(0, size);
        size = LinkToRSS.count(LinkToRSS.class);
        Assert.assertEquals(0, size);
    }

    @Test
    public void linkShouldNotExist() {
        createCategoryWith2Links();
        new LinkToRSS("link3", "link3").save(); //create inconsistent database
        boolean exist = dataBaseHelper.linkExists("tst1", "link3");
        Assert.assertFalse(exist);
    }

    @Test
    public void linkShouldExist() {
        createCategoryWith2Links();
        boolean exist = dataBaseHelper.linkExists("tst1", "link2");
        Assert.assertTrue(exist);
    }

    @Test
    public void shouldGetLink() {
        createCategoryWith2Links();
        LinkToRSS res = dataBaseHelper.getLink("tst1", "link1");
        Assert.assertNotNull(res);
        Assert.assertEquals("link1", res.title);
        Assert.assertEquals("link1_link", res.link);
    }

    @Test
    public void shouldGetAllLinks() {
        createCategoryWith2Links();
        new Category("tst2", null).save();
        LinkToRSS link1 = new LinkToRSS("link1", "link1_link");
        link1.save();
        new Category("tst2", link1).save();

        List<LinkToRSS> links = dataBaseHelper.getLinks("tst1");
        Assert.assertNotNull(links);
        Assert.assertEquals(2, links.size());
        Assert.assertEquals("link1", links.get(0).title);
        Assert.assertEquals("link2", links.get(1).title);
    }

    @Test
    public void shouldDeleteLink() {
        createCategoryWith2Links();
        dataBaseHelper.deleteLink("tst1", "link1");
        long size = Category.count(Category.class);
        Assert.assertEquals(2, size);
        size = LinkToRSS.count(LinkToRSS.class);
        Assert.assertEquals(1, size);
        boolean exist = dataBaseHelper.linkExists("tst1", "link1");
        Assert.assertFalse(exist);
    }

    private void createCategoryWith2Links() {
        //create tst1 category with 2 links
        new Category("tst1", null).save();
        LinkToRSS link1 = new LinkToRSS("link1", "link1_link");
        link1.save();
        LinkToRSS link2 = new LinkToRSS("link2", "link2_link");
        link2.save();
        new Category("tst1", link1).save();
        new Category("tst1", link2).save();
        //test inserts
        long size = Category.count(Category.class);
        Assert.assertEquals(3, size);
        size = LinkToRSS.count(LinkToRSS.class);
        Assert.assertEquals(2, size);
    }


}
