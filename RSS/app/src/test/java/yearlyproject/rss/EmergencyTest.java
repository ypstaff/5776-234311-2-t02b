package yearlyproject.rss;

import android.content.Intent;

import junit.framework.Assert;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.Shadows;
import org.robolectric.annotation.Config;

import java.util.Arrays;
import java.util.List;

import yearlyproject.database.CategoriesDBMenu;
import yearlyproject.database.LinksDBMenu;
import yearlyproject.navigationlibrary.EmergencyCallActivity;
import yearlyproject.rssOperations.ArticlesListActivity;
import yearlyproject.rssOperations.MagazineViewActivity;

/**
 * @author kfirlan
 *         need fake android.net.http.AndroidHttpClient to run
 *         checkout the fake class AndroidHttpClient
 */

@RunWith(RobolectricGradleTestRunner.class)
@Config(constants = BuildConfig.class, sdk = 21)
public class EmergencyTest {
    List<Class<? extends EmergencyCallActivity>> activities = Arrays.asList(MagazinesCategoriesMenuActivity.class
            , MagazinesMenuActivity.class, RssReader.class, ArticlesListActivity.class, MagazineViewActivity.class,
            CategoriesDBMenu.class, LinksDBMenu.class);

    @Test
    // this test might take a lot of time
    public void checkEmergencyPhysicalMagazineViewActivity() {
        EmergencyCallActivity activity = Robolectric.setupActivity(MagazineViewActivity.class);
        activity.findViewById(R.id.YellowButton).performLongClick();
        activity.findViewById(R.id.GreenButton).performLongClick();
        activity.findViewById(R.id.RedButton).performLongClick();
        Intent expectedIntent = new Intent("yearlyproject.phonedialer.ACTION_EMERGENCY");
        Intent realIntent = Shadows.shadowOf(activity).getNextStartedActivity();
        Assert.assertEquals(expectedIntent, realIntent);
    }

    @Test
    public void checkFabsEmergency() {
        EmergencyCallActivity activity = null;
        System.setErr(null);//it try to open a url and fail because we didnt set up a url. this line prevent it from showing it.
        for (Class<? extends EmergencyCallActivity> clazz : activities) {
            try {
                activity = Robolectric.setupActivity(clazz);
            } catch (RuntimeException e) {
            }//some activities setup might fail on retrieving data
            activity.findViewById(R.id.fab_left).performLongClick();
            activity.findViewById(R.id.fab_middle).performLongClick();
            activity.findViewById(R.id.fab_right).performLongClick();
            Intent expectedIntent = new Intent("yearlyproject.phonedialer.ACTION_EMERGENCY");
            Intent realIntent = Shadows.shadowOf(activity).getNextStartedActivity();
            Assert.assertEquals(expectedIntent, realIntent);
        }
    }

}
