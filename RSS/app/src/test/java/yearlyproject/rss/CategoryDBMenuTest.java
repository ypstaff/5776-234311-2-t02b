package yearlyproject.rss;

import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowLog;
import org.robolectric.shadows.ShadowToast;
import org.robolectric.util.ActivityController;

import java.util.ArrayList;
import java.util.List;

import yearlyproject.database.CategoriesDBMenu;
import yearlyproject.database.DataBaseHelper;

/**
 * @author kfirlan
 */
@RunWith(RobolectricGradleTestRunner.class)
@Config(constants = BuildConfig.class, sdk = 21)
public class CategoryDBMenuTest {
    private CategoriesDBMenu mainActivity;
    private ListView lstView;
    private Button add;
    private Button remove;
    private EditText name;

    @Before
    public void setup() throws Exception {
        ActivityController<CategoriesDBMenu> tmp = Robolectric.buildActivity(CategoriesDBMenu.class);
        mainActivity = tmp.get();
        mainActivity.setDBHelper(getMock());
        tmp.setup();
        Assert.assertNotNull("Mainactivity not intsantiated", mainActivity);
        lstView = (ListView) mainActivity.findViewById(R.id.categories_list);//getting the list layout xml
        add = (Button) mainActivity.findViewById(R.id.add_category);
        remove = (Button) mainActivity.findViewById(R.id.category_remove);
        name = (EditText) mainActivity.findViewById(R.id.category_name);
        ShadowLog.stream = System.out; //This is for printing log messages in console
    }

    @Test
    public void addEmptyCategoryShouldFail() {
        Assert.assertNotNull(add);
        add.performClick();
        String error = ShadowToast.getTextOfLatestToast();
        Assert.assertEquals("empty categories are not allowed", error);
    }

    @Test
    public void removeNonExistentFail() {
        Assert.assertNotNull(remove);
        DataBaseHelper helper = getMock();
        Mockito.when(helper.categoryExist(Mockito.anyString())).thenReturn(false);
        mainActivity.setDBHelper(helper);
        remove.performClick();
        Mockito.verify(helper).categoryExist(Mockito.anyString());
        String error = ShadowToast.getTextOfLatestToast();
        Assert.assertEquals("category does not exist", error);
    }

    @Test
    public void addTwiceFail() {
        DataBaseHelper helper = getMock();
        Mockito.when(helper.categoryExist(Mockito.anyString())).thenReturn(true);
        mainActivity.setDBHelper(helper);
        name.setText("tst");
        add.performClick();
        String error = ShadowToast.getTextOfLatestToast();
        Assert.assertEquals("category already exist", error);
    }

    private DataBaseHelper getMock() {
        List<String> titles = new ArrayList<>();
        DataBaseHelper helper = Mockito.mock(DataBaseHelper.class);
        Mockito.when(helper.getCategoriesTitles()).thenReturn(titles);
        return helper;
    }

    private DataBaseHelper getMock(List<String> titles) {
        DataBaseHelper helper = Mockito.mock(DataBaseHelper.class);
        Mockito.when(helper.getCategoriesTitles()).thenReturn(titles);
        return helper;
    }
}
