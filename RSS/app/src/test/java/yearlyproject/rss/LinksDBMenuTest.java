package yearlyproject.rss;

import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowLog;
import org.robolectric.shadows.ShadowToast;
import org.robolectric.util.ActivityController;

import java.util.ArrayList;
import java.util.List;

import yearlyproject.database.DataBaseHelper;
import yearlyproject.database.LinkToRSS;
import yearlyproject.database.LinksDBMenu;

/**
 * @author kfirlan
 */
@RunWith(RobolectricGradleTestRunner.class)
@Config(constants = BuildConfig.class, sdk = 21)
public class LinksDBMenuTest {
    private LinksDBMenu mainActivity;
    private ListView lstView;
    private Button add;
    private Button remove;
    private EditText name;
    private EditText link;

    @Before
    public void setup() throws Exception {
        ActivityController<LinksDBMenu> tmp = Robolectric.buildActivity(LinksDBMenu.class);
        mainActivity = tmp.get();
        mainActivity.setDataBaseHelper(getMock());
        tmp.setup();
        Assert.assertNotNull("Mainactivity not intsantiated", mainActivity);
        lstView = (ListView) mainActivity.findViewById(R.id.categories_list);//getting the list layout xml
        add = (Button) mainActivity.findViewById(R.id.add_link_to_category);
        remove = (Button) mainActivity.findViewById(R.id.link_remove);
        name = (EditText) mainActivity.findViewById(R.id.link_title);
        link = (EditText) mainActivity.findViewById(R.id.link);
        ShadowLog.stream = System.out; //This is for printing log messages in console
    }

    @Test
    public void emptyFieldsAreNotAllowed() {
        add.performClick();
        String error = ShadowToast.getTextOfLatestToast();
        Assert.assertEquals("empty fields are not allowed", error);
        name.setText("tst");
        add.performClick();
        error = ShadowToast.getTextOfLatestToast();
        Assert.assertEquals("empty fields are not allowed", error);
        name.setText("");
        link.setText("tst");
        add.performClick();
        error = ShadowToast.getTextOfLatestToast();
        Assert.assertEquals("empty fields are not allowed", error);
    }

    @Test
    public void linkMustBeAHttp() {
        name.setText("tst");
        link.setText("tst");
        add.performClick();
        String error = ShadowToast.getTextOfLatestToast();
        Assert.assertEquals("unsupported link format", error);
    }

    @Test
    public void shouldNotOverwriteLink() {
        DataBaseHelper helper = getMock();
        Mockito.when(helper.linkExists(Mockito.anyString(), Mockito.anyString())).thenReturn(true);
        mainActivity.setDataBaseHelper(helper);
        mainActivity.setCategory("tst"); //empty string is apparently not anyString()
        name.setText("tst");
        link.setText("http://null");
        add.performClick();
        String error = ShadowToast.getTextOfLatestToast();
        Assert.assertEquals("link already exist", error);
    }

    @Test
    public void shouldNotRemoveNonExistingLink() {
        DataBaseHelper helper = getMock();
        Mockito.when(helper.linkExists(Mockito.anyString(), Mockito.anyString())).thenReturn(false);
        mainActivity.setDataBaseHelper(helper);
        mainActivity.setCategory("tst"); //empty string is apparently not anyString()
        name.setText("tst");
        remove.performClick();
        String error = ShadowToast.getTextOfLatestToast();
        Assert.assertEquals("link does not exist", error);

    }


    private DataBaseHelper getMock() {
        List<LinkToRSS> titles = new ArrayList<>();
        DataBaseHelper helper = Mockito.mock(DataBaseHelper.class);
        Mockito.when(helper.getLinks(Mockito.anyString())).thenReturn(titles);
        return helper;
    }

}
