#!/bin/bash

# author: kfir
# this script create a file of all the invalid links pattern called "badlinks.txt" and file of all the rss links in the htm file called "links.txt"
# to delete the links from the htm file just use   grep -v -f $badlinks $html > $tmp and then mv $tmp $html

if [[ $# -ne 0 ]]; then
	echo "illegal number of parameters"
fi

html="rss-directory.htm"
links="links.txt"
badlinks="badlinks.txt"
tmp="tmp.txt"
i=0
grep  'http.*xml' $html | tr '"' '\n' | grep  'http.*xml' | grep "^http" > $links
while read URL; do
	# echo $URL
	if [ -z $URL ]; then
		continue      
	fi
	i=$(($i+1))
	if curl --output /dev/null --silent --fail $URL 
	then
			echo $i
	# url does not exist
	else
		echo ".*$URL.*" >> $badlinks
	fi
done < $links

# grep -v -f $badlinks $html > $tmp
# mv $tmp $html
# rm $links
# rm $badlinks
