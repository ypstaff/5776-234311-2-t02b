package yearlyproject.rssOperations;

/**
 * An object that represents a magazine.
 *
 * @author michael leybovich
 * @mail mishana4life@gmail.com
 */
public class Magazine {
    public final String xmlLink;
    public final String name;

    /**
     * An empty constructor for a Magazine Object
     */
    public Magazine() {
        xmlLink = "";
        name = "";
    }

    /**
     * A constructor for a Contact Object.
     *
     * @param xmlLink - the link of the magazines' rss feed.
     * @param name    - the magazine name.
     */
    public Magazine(final String xmlLink, final String name) {
        this.xmlLink = xmlLink;
        this.name = name;
    }
}
