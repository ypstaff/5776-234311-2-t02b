package yearlyproject.rssOperations;

/**
 * An rss item, that's stored in the ListView adapter in RssReaderActivity.
 *
 * @author michael leybovich
 * @mail mishana4life@gmail.com
 */
public class RssItem {
    public String title = "";
    public String date = "";
    public String link = "";
    public String description = "";
}