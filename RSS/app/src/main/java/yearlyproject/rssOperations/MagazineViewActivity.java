package yearlyproject.rssOperations;

import android.animation.ObjectAnimator;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import yearlyproject.navigationlibrary.EmergencyCallActivity;
import yearlyproject.rss.R;

/**
 * Responsible for showing an article on screen.
 *
 * @author kfir
 *         based on legacy code from michael leybovich
 */
public class MagazineViewActivity extends EmergencyCallActivity {
    private static final int SHIFT_UP = 2, SHIFT_DOWN = 0, SHIFT_LEFT = 3, SHIFT_RIGHT = 1;
    private static final String tag = "MagazineViewActivity";
    private static final int duration = 350;
    private CustomWebView webview;
    private ProgressBar progress;
    private Button yellow, red, green;
    private int shift;

    protected void initViews() {
        red = (Button) findViewById(R.id.RedButton);
        green = (Button) findViewById(R.id.GreenButton);
        yellow = (Button) findViewById(R.id.YellowButton);

        fab_left = (FloatingActionButton) findViewById(R.id.fab_left);
        fab_right = (FloatingActionButton) findViewById(R.id.fab_right);
        fab_middle = (FloatingActionButton) findViewById(R.id.fab_middle);

        green.setText(getString(R.string.magazine_shift));
        updateOnShift();

        progress = (ProgressBar) findViewById(R.id.progress_bar);
    }

    protected void initTouchListeners() {
        fab_right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                red.performClick();
            }
        });
        fab_middle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                green.performClick();
            }
        });
        fab_left.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                yellow.performClick();
            }
        });
        fab_right.setOnLongClickListener(this);
        fab_middle.setOnLongClickListener(this);
        fab_left.setOnLongClickListener(this);
        yellow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (shift) {
                    case SHIFT_DOWN:
                        arrowDown();
                        break;
                    case SHIFT_RIGHT:
                        arrowRight();
                        break;
                    case SHIFT_UP:
                        arrowUp();
                        break;
                    case SHIFT_LEFT:
                        arrowLeft();
                        break;
                    default:
                        Log.e(tag, "shift is bigger then 4 shift = " + shift);
                }
            }
        });
        green.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calcShift();
                updateOnShift();
            }
        });
        red.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        yellow.setOnLongClickListener(this);
        green.setOnLongClickListener(this);
        red.setOnLongClickListener(this);
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.magazine_view);

        findViewById(R.id.included_buttons).setVisibility(View.INVISIBLE);

        initViews();
        initTouchListeners();

        webview = (CustomWebView) findViewById(R.id.webview);
        webview.setWebViewClient(new MyBrowser());
        webview.getSettings().setJavaScriptEnabled(true);
//        webview.getSettings().setUseWideViewPort(true); //not sure of needed
//        webview.getSettings().setDomStorageEnabled(true); //not sure if needed
        webview.loadUrl(getIntent().getStringExtra("URL_LINK"));
    }

    /**
     * Updates the four buttons layout on every press of the Shift button.
     */
    private void updateOnShift() {
        Drawable shiftToggle = ContextCompat.getDrawable(this,
                R.drawable.ic_action_shift_1_512);
        Drawable toggle = null;

        switch (shift) {
            case SHIFT_DOWN: {
                green.setTextColor(Color.BLACK);
                yellow.setText(getString(R.string.magazine_scroll_down));
                toggle = ContextCompat.getDrawable(this, R.drawable.ic_action_expand);
                break;
            }
            case SHIFT_RIGHT: {
                green.setTextColor(Color.WHITE);
                yellow.setText(getString(R.string.magazine_scroll_right));
                shiftToggle = ContextCompat.getDrawable(this, R.drawable.ic_action_shift_1_128);
                toggle = ContextCompat.getDrawable(this, R.drawable.ic_action_next_item);
                break;
            }
            case SHIFT_UP: {
                green.setTextColor(Color.BLACK);
                yellow.setText(getString(R.string.magazine_scroll_up));
                toggle = ContextCompat.getDrawable(this, R.drawable.ic_action_collapse);
                break;
            }
            case SHIFT_LEFT: {
                green.setTextColor(Color.WHITE);
                yellow.setText(getString(R.string.magazine_scroll_left));
                shiftToggle = ContextCompat.getDrawable(this, R.drawable.ic_action_shift_1_128);
                toggle = ContextCompat.getDrawable(this, R.drawable.ic_action_previous_item);
                break;
            }
            default:
                Log.e(tag, "shift is bigger then 4 shift = " + shift);
        }

        green.setCompoundDrawablesWithIntrinsicBounds(null, shiftToggle,
                null, null);
        yellow.setCompoundDrawablesWithIntrinsicBounds(null,
                toggle, null, null);
    }

    /**
     * scroll up one item in the list view when the arrowUp is pressed
     */
    private void arrowUp() {
        final ObjectAnimator anim = ObjectAnimator.ofInt(webview, "scrollY",
                webview.getScrollY(),
                Math.max(webview.getScrollY() - webview.getHeight() / 4, 0));
        anim.setDuration(duration).start();
    }

    /**
     * scroll down one item in the list view when the arrowDown is pressed
     */
    private void arrowDown() {
        @SuppressWarnings("deprecation")
        final ObjectAnimator anim = ObjectAnimator.ofInt(webview, "scrollY",
                webview.getScrollY(), Math.min(
                        webview.getScrollY() + webview.getHeight() / 4,
                        (int) Math.floor(webview.getContentHeight()
                                * webview.getScale())
                                - webview.getHeight()));
        anim.setDuration(duration).start();
    }

    /**
     * scroll up one item in the list view when the arrowLeft is pressed
     */
    private void arrowLeft() {
        final ObjectAnimator anim = ObjectAnimator.ofInt(webview, "scrollX",
                webview.getScrollX(),
                Math.max(webview.getScrollX() - webview.getWidth() / 8, 0));
        anim.setDuration(duration).start();
    }

    /**
     * scroll down one item in the list view when the arrowRight is pressed
     */
    private void arrowRight() {
        final ObjectAnimator anim = ObjectAnimator.ofInt(webview, "scrollX",
                webview.getScrollX(), Math.min(
                        webview.getScrollX() + webview.getWidth() / 8,
                        (int) Math.floor(webview.getContentWidth()
                                * webview.getScaleX())
                                - webview.getWidth()));
        anim.setDuration(duration).start();
    }

    private void calcShift() {
        for (int i = 0; i < 3; ++i) {
            shift = (shift + 1) % 4;
            switch (shift) {
                case (SHIFT_DOWN): {
                    int height = (int) Math.floor(webview.getContentHeight() * webview.getScale());
                    int webViewHeight = webview.getMeasuredHeight();
                    if (webview.getScrollY() + webViewHeight < height) {
                        return;
                    }
                    break;
                }
                case (SHIFT_UP): {
                    if (webview.getScrollY() != 0) {
                        return;
                    }
                    break;
                }
                case (SHIFT_LEFT): {
                    if (webview.getScrollX() != 0) {
                        Log.d(tag, "webview.getScrollX() = " + webview.getScrollX());
                        return;
                    }
                    break;
                }
                case (SHIFT_RIGHT): {
                    int width = (int) Math.floor(webview.getContentWidth() * webview.getScaleX());
                    int webViewWidth = webview.getMeasuredWidth();
                    if (webview.getScrollX() + webViewWidth < width) {
                        return;
                    }
                    break;
                }
                default:
                    Log.e(tag, "shift is bigger then 4 shift = " + shift);
            }
        }
        shift = (shift + 1) % 4; // complete the cycle
        Toast.makeText(this, "you can move just in one direction", Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onLongClick(View v) {
        boolean res = super.onLongClick(v);
        switch (v.getId()) {
            case (R.id.YellowButton): {
                fab_left.performLongClick();
                break;
            }
            case (R.id.GreenButton): {
                fab_middle.performLongClick();
                break;
            }
            case (R.id.RedButton): {
                fab_right.performLongClick();
                break;
            }
        }
        return res;
    }

    /**
     * A custom WebViewClient that adds progress circle functionality on loading
     * of pages and makes all subsequent links opened in our WebView.
     */
    private class MyBrowser extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(final WebView view,
                                                final String url) {
            view.loadUrl(url);
            return true;
        }

        @Override
        public void onPageStarted(final WebView view, final String url,
                                  final Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            progress.setVisibility(View.VISIBLE);
        }

        @Override
        public void onPageFinished(final WebView view, final String url) {
            super.onPageFinished(view, url);
            progress.setVisibility(View.INVISIBLE);
        }
    }
}
