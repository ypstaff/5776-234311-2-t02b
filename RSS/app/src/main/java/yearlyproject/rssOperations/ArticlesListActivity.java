package yearlyproject.rssOperations;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.design.widget.FloatingActionButton;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.code.rome.android.repackaged.com.sun.syndication.feed.synd.SyndEntry;
import com.google.code.rome.android.repackaged.com.sun.syndication.io.FeedException;
import com.google.code.rome.android.repackaged.com.sun.syndication.io.SyndFeedInput;
import com.google.code.rome.android.repackaged.com.sun.syndication.io.XmlReader;

import org.jsoup.Jsoup;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import yearlyproject.navigationlibrary.EmergencyCallActivity;
import yearlyproject.navigationlibrary.IterableListView;
import yearlyproject.rss.R;

/**
 * Responsible for parsing an rss link, retrieving the rss feed and showing it
 * on screen.
 *
 * @author kfirlan
 */
public class ArticlesListActivity extends EmergencyCallActivity {
    private ListView listView;
    private ArrayList<RssItem> itemlist = null;
    private RSSListAdaptor rssadapter = null;
    private String xmlLink;

    private void updateListView() {
        listView.setAdapter(rssadapter);
        listView.setChoiceMode(AbsListView.CHOICE_MODE_SINGLE);
//        listView.setItemChecked(0, true);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                openNextWindowWithSelection();
            }
        });

        if (itemlist == null || itemlist.size() == 0)
            showToast(getString(R.string.magazine_no_rss_feed_found));
    }

    /**
     * init the view that are specific to this activity (ulike the fabs which all activities have)
     */
    protected void initSpecificViews() {
        listView = (ListView) findViewById(R.id.rssList);
        group = new IterableListView(listView, this);
        group.start();
    }

    protected void initFabs() {
        fab_left = (FloatingActionButton) findViewById(R.id.fab_left);
        fab_right = (FloatingActionButton) findViewById(R.id.fab_right);
        fab_middle = (FloatingActionButton) findViewById(R.id.fab_middle);
        fab_left.setOnClickListener(this);
        fab_right.setOnClickListener(this);
        fab_middle.setOnClickListener(this);
        fab_left.setOnLongClickListener(this);
        fab_middle.setOnLongClickListener(this);
        fab_right.setOnLongClickListener(this);
    }

    @Override
    public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.articles_list);
        initFabs();
        initSpecificViews();
        itemlist = new ArrayList<RssItem>();
        xmlLink = getIntent().getStringExtra("XML_LINK");
        new RetrieveRSSFeeds().execute();
    }

    /**
     * Opens the selected article.
     *
     * @param position - the position of the selected article in the listView.
     */
    private void openArticleFromPosition(final int position) {
        final RssItem data = itemlist.get(position);
        final Intent intent = new Intent(ArticlesListActivity.this,
                MagazineViewActivity.class);
        intent.putExtra("URL_LINK", data.link);
        ;
        startActivity(intent);
    }

    /**
     * Retrieves and parses the RSS feed from the URL passed and stores all
     * parsed RssItem objects in list.
     *
     * @param urlToRssFeed - the URL link of the current RSS feed to load
     * @param list         - the list of items that are shown in web view
     */
    private void getRSS(final String urlToRssFeed, final ArrayList<RssItem> list) {
        try {
            final List<?> entries = new SyndFeedInput().build(
                    new XmlReader(new URL(urlToRssFeed))).getEntries();
            final Iterator<?> iterator = entries.listIterator();
            while (iterator.hasNext()) {
                final SyndEntry ent = (SyndEntry) iterator.next();
                final RssItem item = new RssItem();
                item.title = htmlToText(ent.getTitle());
                if (ent.getPublishedDate() != null)
                    item.date = ent.getPublishedDate().toString();
                item.link = ent.getLink();
                final int MAX_LENGTH_DESCRIPTION = 1000;
                if (ent.getDescription() != null) {
                    item.description = htmlToText(ent.getDescription().getValue());
                    item.description = item.description.substring(
                            0,
                            Math.min(MAX_LENGTH_DESCRIPTION,
                                    item.description.length()));
                }
                list.add(item);
            }
        } catch (final MalformedURLException e) {
            e.printStackTrace();
        } catch (final IllegalArgumentException e) {
            e.printStackTrace();
        } catch (final IOException e) {
            e.printStackTrace();
        } catch (final FeedException e) {
            e.printStackTrace();
        }
    }

    /**
     * Opens the next window from the currently selected in the list view.
     */
    protected void openNextWindowWithSelection() {
        if (rssadapter.getCount() == 0)
            return;
        openArticleFromPosition(listView.getCheckedItemPosition());
    }

    /**
     * Toasts a message on screen.
     *
     * @param msg - the messaged to be displayed.
     */
    private void showToast(final String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    private String htmlToText(String html) {
        return Jsoup.parse(html).text();
    }

    /**
     * An Async Task object that retrieves the rss feed of a magazine and shows
     * a progress dialog while loading.
     */
    private class RetrieveRSSFeeds extends AsyncTask<Void, Void, Void> {
        private ProgressDialog progress = null;

        @Override
        protected Void doInBackground(final Void... params) {
            getRSS(xmlLink, itemlist);
            rssadapter = new RSSListAdaptor(ArticlesListActivity.this,
                    R.layout.rssitemview, itemlist);
            return null;
        }

        @Override
        protected void onCancelled() {
            Log.d("tst", "canceled");
            progress.dismiss();
            itemlist.clear();
            updateListView();
            super.onCancelled();
        }

        @Override
        protected void onPreExecute() {
            progress = ProgressDialog.show(ArticlesListActivity.this, null,
                    "Loading RSS Feeds...");
            long max_time = 20000;
            new CountDownTimer(max_time, max_time) {
                public void onTick(long millisUntilFinished) {
                }

                public void onFinish() {
                    // stop async task
                    RetrieveRSSFeeds.this.cancel(true);
                }
            }.start();
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(final Void result) {
            updateListView();
            progress.dismiss();
            super.onPostExecute(result);
        }

        @Override
        protected void onProgressUpdate(final Void... values) {
            super.onProgressUpdate(values);
        }
    }

    /**
     * An ArrayAdapter that contains RssItem Objects.
     */
    private class RSSListAdaptor extends ArrayAdapter<RssItem> {
        private List<RssItem> objects = null;

        public RSSListAdaptor(final Context context, final int textviewid,
                              final List<RssItem> objects) {
            super(context, textviewid, objects);

            this.objects = objects;
        }

        @Override
        public int getCount() {
            return null != objects ? objects.size() : 0;
        }

        @Override
        public long getItemId(final int position) {
            return position;
        }

        @Override
        public RssItem getItem(final int position) {
            return null != objects ? objects.get(position) : null;
        }

        @Override
        public View getView(final int position, final View convertView,
                            final ViewGroup parent) {
            View view = convertView;

            if (null == view) {
                view = LayoutInflater.from(getContext()).inflate(R.layout.rssitemview, parent,false);
            }

            final RssItem data = objects.get(position);

            if (null != data) {
                final TextView title = (TextView) view
                        .findViewById(R.id.txtTitle);
                final TextView date = (TextView) view
                        .findViewById(R.id.txtDate);
                final TextView description = (TextView) view
                        .findViewById(R.id.txtDescription);

                title.setText(data.title);
                date.setText(data.date);
                description.setText(data.description);
            }

            return view;
        }
    }
}