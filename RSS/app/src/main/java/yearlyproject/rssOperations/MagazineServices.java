package yearlyproject.rssOperations;

import android.content.Context;
import android.content.Intent;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * A service class with static methods that provides various magazine related
 * services. Also, it stores the categories and magazines lists as static
 * members.
 *
 * @author michael leybovich
 * @mail mishana4life@gmail.com
 */
public class MagazineServices {
    private static Map<String, List<Magazine>> categories_to_magazines;
    private static List<String> categories;

    /**
     * @param context  - the current app context
     * @param magazine - the desired magazine to open an RssFeed
     * @return An intent that will start an ArticlesListActivity for the passed
     * magazine
     */
    public static Intent getRssReaderIntentWithMagazine(final Context context,
                                                        final Magazine magazine) {
        final Intent intent = new Intent(context, ArticlesListActivity.class);
        intent.putExtra("XML_LINK", magazine.xmlLink);

        return intent;
    }

    /**
     * @param category - the category of magazines to return.
     * @return A list of all magazines for the passed category.
     */
    public static List<Magazine> getAllMagazinesForCategory(
            final String category) {
        if (categories_to_magazines == null)
            categories_to_magazines = new HashMap<String, List<Magazine>>();

        return categories_to_magazines.get(category);
    }

    /**
     * Saves a local copy of all magazines for a specific category.
     *
     * @param category  - the magazines's category.
     * @param magazines - a list of magazines for the passed category.
     */
    public static void setAllMagazinesForCategory(final String category,
                                                  final List<Magazine> magazines) {
        if (categories_to_magazines == null)
            categories_to_magazines = new HashMap<String, List<Magazine>>();

        MagazineServices.categories_to_magazines.put(category, magazines);
    }

    public static List<String> getAllCategories() {
        return categories;
    }

    /**
     * Saves a local copy of all magazines' categories.
     *
     * @param categories - a list of all the categories.
     */
    public static void setAllCategories(final List<String> categories) {
        MagazineServices.categories = categories;
    }

}
