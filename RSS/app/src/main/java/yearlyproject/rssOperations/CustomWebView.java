package yearlyproject.rssOperations;

import android.content.Context;
import android.util.AttributeSet;
import android.webkit.WebView;

/**
 * A custom WebView, that has a public getContentWidth() method.
 *
 * @author michael leybovich
 * @mail mishana4life@gmail.com
 */
public class CustomWebView extends WebView {
    public CustomWebView(final Context context) {
        super(context);
    }

    public CustomWebView(final Context context, final AttributeSet attrs) {
        super(context, attrs);
    }

    /**
     * @return The width of the web content loaded in web view (not web view
     * width, see getContentHeight()).
     */
    public int getContentWidth() {
        return this.computeHorizontalScrollRange();
    }
}
