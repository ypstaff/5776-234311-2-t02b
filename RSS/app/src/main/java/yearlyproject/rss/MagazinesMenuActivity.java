package yearlyproject.rss;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import yearlyproject.navigationlibrary.EmergencyCallActivity;
import yearlyproject.navigationlibrary.IterableListView;
import yearlyproject.rssOperations.Magazine;
import yearlyproject.rssOperations.MagazineAdapter;
import yearlyproject.rssOperations.MagazineServices;

/**
 * A class that's responsible for showing a list of magazines for a
 * certain category.
 *
 * @author kfir
 *         based on legacy by michael leybovich
 */
public class MagazinesMenuActivity extends EmergencyCallActivity {


    private ListView listView;
    private List<Magazine> listItems;
    private ArrayAdapter<Magazine> adapter;

    private String magazines_category;

    /**
     * Updates the List View Shown On screen, sets it's adapter, checking the
     * first item, sorting etc.
     */
    private void updateListView() {
        adapter = new MagazineAdapter(MagazinesMenuActivity.this,
                R.layout.list_row_small_highlighted, listItems);

        listView.setAdapter(adapter);
        listView.setChoiceMode(AbsListView.CHOICE_MODE_SINGLE);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                openNextWindowWithSelection();
            }
        });

        if (listItems == null || listItems.size() == 0)
            showToast(getString(R.string.magazine_no_magazines_found));
        else
            Collections.sort(listItems, new Comparator<Magazine>() {

                @Override
                public int compare(final Magazine lhs, final Magazine rhs) {
                    return lhs.name.compareTo(rhs.name);
                }
            });

//        listView.setItemChecked(0, true);
    }

    /**
     * Retrieve all needed data and initialize the items list
     */
    private void getAllListItems() {
        listItems = MagazineServices
                .getAllMagazinesForCategory(magazines_category);

        if (listItems != null) {
            updateListView();
            return;
        }

        listItems = new ArrayList<Magazine>();
        new RetrieveMagazines().execute();
    }


    /**
     * Opens next window according to the selected item in the ListView
     */
    protected void openNextWindowWithSelection() {
        if (getAdapter().getCount() == 0)
            return;

        final Magazine selected = getAdapter().getItem(
                getListView().getCheckedItemPosition());

        startActivity(MagazineServices.getRssReaderIntentWithMagazine(
                getApplicationContext(), selected));
    }

    protected void initRest() {
        listView = (ListView) findViewById(R.id.magazines_list);

        group = new IterableListView(listView, this);
        group.start();
    }

    protected void initFabs() {
        fab_left = (FloatingActionButton) findViewById(R.id.fab_left);
        fab_right = (FloatingActionButton) findViewById(R.id.fab_right);
        fab_middle = (FloatingActionButton) findViewById(R.id.fab_middle);
        fab_left.setOnClickListener(this);
        fab_right.setOnClickListener(this);
        fab_middle.setOnClickListener(this);
        fab_left.setOnLongClickListener(this);
        fab_middle.setOnLongClickListener(this);
        fab_right.setOnLongClickListener(this);
    }

    /**
     * @return the array adapter of the List View
     */
    protected ArrayAdapter<Magazine> getAdapter() {
        return adapter;
    }

    /**
     * @return the listView Object shown on screen.
     */
    protected ListView getListView() {
        return listView;
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.magazines_menu);

        initFabs();
        initRest();

        magazines_category = getIntent().getStringExtra("MAGAZINES_CATEGORY");
        getAllListItems();
    }

    /**
     * Toasts a message on screen.
     *
     * @param msg - the messaged to be displayed.
     */
    private void showToast(final String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onLongClick(View v) {
        return super.onLongClick(v);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
    }

    /**
     * An Async Task object that retrieves and parses the magazines links and
     * names from rss-derectory.htm for a specific category and displays a
     * progress dialog.
     */
    private class RetrieveMagazines extends AsyncTask<Void, Void, Void> {
        private ProgressDialog progress = null;

        @Override
        protected Void doInBackground(final Void... params) {
            InputStream input = null;
            try {
                input = getAssets().open("rss-directory.htm");
            } catch (final IOException e) {
                e.printStackTrace();
                return null;
            }
            Document doc;

            try {
                doc = Jsoup.parse(input, null, "");
            } catch (final IOException e) {
                e.printStackTrace();
                return null;
            }
            final Element category = doc.select(
                    "h1:contains(" + magazines_category + ")").get(0);
            final Elements magazines = category.nextElementSibling().select(
                    "tr:contains(XML)");

            for (final Element magazine : magazines) {
                final Elements link = magazine.select("a[href]:contains(XML)");
                final Elements names = magazine.select("td");

                listItems.add(new Magazine(link.get(0).attr("abs:href"), names
                        .get(0).text()));
            }
            return null;
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
        }

        @Override
        protected void onPreExecute() {
            progress = ProgressDialog.show(MagazinesMenuActivity.this, null,
                    "Loading Magazines List...");

            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(final Void result) {
            updateListView();

            MagazineServices.setAllMagazinesForCategory(magazines_category,
                    listItems);

            progress.dismiss();

            super.onPostExecute(result);
        }

        @Override
        protected void onProgressUpdate(final Void... values) {
            super.onProgressUpdate(values);
        }
    }

}
