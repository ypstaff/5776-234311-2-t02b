package yearlyproject.rss;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.view.View;

import java.util.Arrays;
import java.util.List;

import yearlyproject.database.CategoriesDBMenu;
import yearlyproject.navigationlibrary.EmergencyCallActivity;
import yearlyproject.navigationlibrary.IterableViewGroup;

public class RssReader extends EmergencyCallActivity {

    View legacy;
    View customLinks;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initFabs();
        initRest();

        List<View> views = Arrays.asList(legacy, customLinks);
        View parent = findViewById(R.id.activity_rss_buttons);
        group = new IterableViewGroup(views, parent, this);
        group.start();
    }

    public void switchToDontPanic(View view) {
        Intent intent = new Intent(RssReader.this, MagazinesCategoriesMenuActivity.class);
        startActivity(intent);
    }

    public void customLinks(View view) {

        Intent intent = new Intent(RssReader.this, CategoriesDBMenu.class);
        startActivity(intent);

    }

    protected void initFabs() {
        fab_left = (FloatingActionButton) findViewById(R.id.fab_left);
        fab_right = (FloatingActionButton) findViewById(R.id.fab_right);
        fab_middle = (FloatingActionButton) findViewById(R.id.fab_middle);
        fab_left.setOnClickListener(this);
        fab_right.setOnClickListener(this);
        fab_middle.setOnClickListener(this);
        fab_left.setOnLongClickListener(this);
        fab_middle.setOnLongClickListener(this);
        fab_right.setOnLongClickListener(this);
    }

    protected void initRest() {
        legacy = findViewById(R.id.legacy);
        customLinks = findViewById(R.id.custom_links);
        legacy.setOnClickListener(this);
        customLinks.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case (R.id.custom_links):
                customLinks(v);
                break;
            case (R.id.legacy):
                switchToDontPanic(v);
                break;
        }
    }
}
