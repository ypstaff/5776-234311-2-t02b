package yearlyproject.rss;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import yearlyproject.navigationlibrary.EmergencyCallActivity;
import yearlyproject.navigationlibrary.IterableListView;
import yearlyproject.rssOperations.MagazineServices;

/**
 * A class that's responsible for showing a list of magazine
 * categories.
 *
 * @author kfir
 *         based on legacy from michael leybovich
 *         <p/>
 *         i changed the ui from 4 buttons to our implementation of 3 buttons
 */
public class MagazinesCategoriesMenuActivity extends EmergencyCallActivity {

    private ListView listView;
    private List<String> listItems;
    private ArrayAdapter<String> adapter;

    /**
     * Updates the List View Shown On screen, sets it's adapter, checking the
     * first item, sorting etc.
     */
    private void updateListView() {
        adapter = new ArrayAdapter<String>(
                MagazinesCategoriesMenuActivity.this,
                R.layout.list_row_small_highlighted, R.id.listItemText,
                listItems);

        listView.setAdapter(adapter);
        listView.setChoiceMode(AbsListView.CHOICE_MODE_SINGLE);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                openNextWindowWithSelection();
            }
        });

        if (listItems == null || listItems.size() == 0)
            showToast(getString(R.string.magazine_no_categories_found));
        else
            Collections.sort(listItems);

//        listView.setItemChecked(0, true);
    }

    /**
     * Retrieve all needed data and initialize the items list
     */
    private void getAllListItems() {
        listItems = MagazineServices.getAllCategories();

        if (listItems != null) {
            updateListView();
            return;
        }

        listItems = new ArrayList<String>();
        new RetrieveCategories().execute();
    }

    /**
     * Sets the title of the activity
     */

    /**
     * Opens next window according to the selected item in the ListView
     */
    protected void openNextWindowWithSelection() {
        if (getAdapter().getCount() == 0)
            return;

        final String selected = getAdapter().getItem(
                getListView().getCheckedItemPosition());

        final Intent intent = new Intent(
                MagazinesCategoriesMenuActivity.this,
                yearlyproject.rss.MagazinesMenuActivity.class);
        intent.putExtra("MAGAZINES_CATEGORY", selected);
        startActivity(intent);
    }

    protected void initRest() {
        listView = (ListView) findViewById(R.id.categories_list);
        group = new IterableListView(listView, this);
        group.start();
    }

    protected void initFabs() {
        fab_left = (FloatingActionButton) findViewById(R.id.fab_left);
        fab_right = (FloatingActionButton) findViewById(R.id.fab_right);
        fab_middle = (FloatingActionButton) findViewById(R.id.fab_middle);
        fab_left.setOnClickListener(this);
        fab_right.setOnClickListener(this);
        fab_middle.setOnClickListener(this);
        fab_left.setOnLongClickListener(this);
        fab_middle.setOnLongClickListener(this);
        fab_right.setOnLongClickListener(this);
    }

    /**
     * @return the array adapter of the List View
     */
    protected ArrayAdapter<String> getAdapter() {
        return adapter;
    }

    @Override
    public boolean onLongClick(View v) {
        return super.onLongClick(v);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
    }

    /**
     * @return the listView Object shown on screen.
     */
    protected ListView getListView() {
        return listView;
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.magazines_categories_menu);
        initFabs();
        initRest();
        getAllListItems();
    }

    /**
     * Toasts a message on screen.
     *
     * @param msg - the messaged to be displayed.
     */
    private void showToast(final String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    /**
     * An Async Task object that retrieves and parses all the categories from
     * rss-derectory.htm and displays a progress dialog.
     */
    private class RetrieveCategories extends AsyncTask<Void, Void, Void> {
        private ProgressDialog progress = null;

        @Override
        protected Void doInBackground(final Void... params) {
            InputStream input = null;
            try {
                input = getAssets().open("rss-directory.htm");
            } catch (final IOException e) {
                e.printStackTrace();
                return null;
            }
            Document doc;

            try {
                doc = Jsoup.parse(input, null, "");
            } catch (final IOException e) {
                e.printStackTrace();
                return null;
            }
            final Elements categories = doc.select("li");

            for (final Element category : categories)
                listItems.add(category.text());

            return null;
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
        }

        @Override
        protected void onPreExecute() {
            progress = ProgressDialog.show(
                    MagazinesCategoriesMenuActivity.this, null,
                    "Loading Categories List...");

            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(final Void result) {
            updateListView();

            MagazineServices.setAllCategories(listItems);

            progress.dismiss();

            super.onPostExecute(result);
        }

        @Override
        protected void onProgressUpdate(final Void... values) {
            super.onProgressUpdate(values);
        }
    }


}