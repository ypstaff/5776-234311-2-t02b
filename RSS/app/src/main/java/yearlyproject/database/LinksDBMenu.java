package yearlyproject.database;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import yearlyproject.navigationlibrary.IterableGroup;
import yearlyproject.navigationlibrary.IterableListView;
import yearlyproject.navigationlibrary.IterableViewGroup;
import yearlyproject.navigationlibrary.IterableViewGroups;
import yearlyproject.rss.R;
import yearlyproject.rssOperations.ArticlesListActivity;

/**
 * @author kfirlan
 */
public class LinksDBMenu extends DBmanu {
    private Button add, remove;
    private EditText link_name;
    private EditText link_xml;
    private String category;
    private List<LinkToRSS> listLinks = new ArrayList<>();
    private DataBaseHelper helper = DataBaseHelper.getInstance();

    protected void initRest() {
        listView = (ListView) findViewById(R.id.categories_list);
        add = (Button) findViewById(R.id.add_link_to_category);
        remove = (Button) findViewById(R.id.link_remove);
        link_name = (EditText) findViewById(R.id.link_title);
        link_xml = (EditText) findViewById(R.id.link);
        add.setOnClickListener(this);
        remove.setOnClickListener(this);
        link_name.setOnClickListener(this);
        link_xml.setOnClickListener(this);
        link_name.setOnEditorActionListener(this);
        link_xml.setOnEditorActionListener(this);

        View parentInputView = findViewById(R.id.links_db_input);
        View parentView = findViewById(R.id.links_db);

        IterableGroup listGroup = new IterableListView(listView, this);
        List<View> views = Arrays.asList((View) link_name, remove, link_xml, add);
        IterableGroup restOfViews = new IterableViewGroup(views, parentInputView, this);
        List<IterableGroup> groups = Arrays.asList(listGroup, restOfViews);
        group = new IterableViewGroups(groups, parentView, this);

        group.start();

    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.links_db);
        initFabs();
        initRest();
        category = getIntent().getStringExtra("MAGAZINES_CATEGORY");
        setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                LinkToRSS linkToRSS = listLinks.get(position);
                final Intent intent = new Intent(LinksDBMenu.this, ArticlesListActivity.class);
                intent.putExtra("XML_LINK", linkToRSS.link);
                startActivity(intent);
            }
        });
        getAllListItems();
    }


    @Override
    protected void retrieveCategories() {
        new RetrieveCategories().execute();
    }

    public void setDataBaseHelper(DataBaseHelper helper) {
        this.helper = helper;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case (R.id.add_link_to_category): {
                String title = link_name.getText().toString();
                String link = link_xml.getText().toString();
                if (title.isEmpty() || link.isEmpty()) {
                    showToast("empty fields are not allowed");
                    return;
                }
                if (!link.matches("http.*")) {
                    showToast("unsupported link format");
                    return;
                }
                if (helper.linkExists(category, title)) {
                    showToast("link already exist");
                    return;
                }
                // add to db
                LinkToRSS tmp = new LinkToRSS(title, link);
                tmp.save();
                new Category(category, tmp).save();
                // referesh list
                retrieveCategories();
                link_name.getText().clear();
                link_xml.getText().clear();
                break;
            }
            case (R.id.link_remove): {
                String title = link_name.getText().toString();
                if (!helper.linkExists(category, title)) {
                    showToast("link does not exist");
                    return;
                }
                helper.deleteLink(category, title);
                // referesh list
                retrieveCategories();
                link_name.getText().clear();
                link_xml.getText().clear();
                break;
            }
            case (R.id.fab_left): {
                link_xml.setCursorVisible(false);
                link_name.setCursorVisible(false);
            }
        }
    }

    private class RetrieveCategories extends AsyncTask<Void, Void, Void> {
        private ProgressDialog progress = null;

        @Override
        protected Void doInBackground(Void... params) {
            listLinks.clear();
            listLinks = helper.getLinks(category);
            List<String> res = new ArrayList<>();
            for (LinkToRSS l : listLinks) {
                res.add(l.title);
            }
            listItems = res;
            return null;
        }

        @Override
        protected void onPreExecute() {
            progress = ProgressDialog.show(
                    LinksDBMenu.this, null,
                    "Loading Categories...");

            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(final Void result) {
            updateListView();
            progress.dismiss();
            super.onPostExecute(result);
        }
    }

}
