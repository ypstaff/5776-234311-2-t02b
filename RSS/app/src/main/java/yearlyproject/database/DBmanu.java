package yearlyproject.database;

import android.support.design.widget.FloatingActionButton;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import yearlyproject.navigationlibrary.EmergencyCallActivity;
import yearlyproject.rss.R;

/**
 * @author kfirlan
 */
public abstract class DBmanu extends EmergencyCallActivity implements TextView.OnEditorActionListener {
    protected ListView listView;
    protected List<String> listItems;
    protected ArrayAdapter<String> adapter;
    private AdapterView.OnItemClickListener onItemClickListener;

    protected void updateListView() {
        adapter = new ArrayAdapter<String>(
                DBmanu.this,
                R.layout.list_row_small_highlighted, R.id.listItemText,
                listItems);

        listView.setAdapter(adapter);
        listView.setChoiceMode(AbsListView.CHOICE_MODE_SINGLE);
        if (onItemClickListener == null) {
            Log.e("DBmenu", "onItemClickListener is null");
        } else {
            listView.setOnItemClickListener(onItemClickListener);
        }

    }

    protected void setOnItemClickListener(AdapterView.OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    protected void getAllListItems() {
        if (listItems != null) {
            updateListView();
            return;
        }
        retrieveCategories();
    }

    protected void showToast(final String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    protected abstract void retrieveCategories();

    @Override
    public void onClick(View v) {
        super.onClick(v);
        if (v instanceof EditText) {
            ((EditText) v).setCursorVisible(true);
            v.setFocusableInTouchMode(true);
            v.requestFocus();
            showSoftKey((EditText) v);
        }
    }

    @Override
    public boolean onLongClick(View v) {
        return super.onLongClick(v);
    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        boolean handled = false;
        if (actionId == EditorInfo.IME_ACTION_SEND || actionId == EditorInfo.IME_ACTION_NEXT
                || actionId == EditorInfo.IME_ACTION_DONE) {
            hideSoftKey();
        }
        return handled;
    }

    protected void initFabs() {
        fab_left = (FloatingActionButton) findViewById(R.id.fab_left);
        fab_right = (FloatingActionButton) findViewById(R.id.fab_right);
        fab_middle = (FloatingActionButton) findViewById(R.id.fab_middle);
        fab_left.setOnClickListener(this);
        fab_right.setOnClickListener(this);
        fab_middle.setOnClickListener(this);
        fab_left.setOnLongClickListener(this);
        fab_middle.setOnLongClickListener(this);
        fab_right.setOnLongClickListener(this);
    }
}
