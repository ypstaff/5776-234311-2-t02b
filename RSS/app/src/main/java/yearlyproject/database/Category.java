package yearlyproject.database;

import com.orm.SugarRecord;

/**
 * @author kfirlan
 *         category can contain links to articles
 *         on the sql table linkToRss is the id of the object (field inherited from SugarRecord)
 */
public class Category extends SugarRecord {
    public String title;
    public LinkToRSS link;

    /**
     * DO NOT USE THIS
     * THIS IS HERE FOR THE DATABASE
     */
    public Category() {
    }

    public Category(String title, LinkToRSS link) {
        this.title = title;
        this.link = link;
    }

}
