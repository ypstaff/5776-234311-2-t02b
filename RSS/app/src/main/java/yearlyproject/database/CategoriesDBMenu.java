package yearlyproject.database;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import java.util.Arrays;
import java.util.List;

import yearlyproject.navigationlibrary.IterableGroup;
import yearlyproject.navigationlibrary.IterableListView;
import yearlyproject.navigationlibrary.IterableViewGroup;
import yearlyproject.navigationlibrary.IterableViewGroups;
import yearlyproject.rss.R;

/**
 * @author kfirlan
 */
public class CategoriesDBMenu extends DBmanu {

    private Button add, remove;
    private EditText name;
    private DataBaseHelper helper = DataBaseHelper.getInstance();


    protected void initRest() {
        listView = (ListView) findViewById(R.id.categories_list);

        add = (Button) findViewById(R.id.add_category);
        remove = (Button) findViewById(R.id.category_remove);
        name = (EditText) findViewById(R.id.category_name);

        add.setOnClickListener(this);
        remove.setOnClickListener(this);
        name.setOnClickListener(this);
        name.setOnEditorActionListener(this);

        View parentInputView = findViewById(R.id.categories_db_input);
        View parentView = findViewById(R.id.categories_db);

        IterableGroup listGroup = new IterableListView(listView, this);
        List<View> views = Arrays.asList((View) name, add, remove);
        IterableGroup restOfViews = new IterableViewGroup(views, parentInputView, this);
        List<IterableGroup> groups = Arrays.asList(listGroup, restOfViews);
        group = new IterableViewGroups(groups, parentView, this);

        group.start();
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.categories_db);
        initFabs();
        initRest();
        setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String title = (String) listView.getItemAtPosition(position);
                Intent intent = new Intent(CategoriesDBMenu.this, LinksDBMenu.class);
                intent.putExtra("MAGAZINES_CATEGORY", title);
                startActivity(intent);
            }
        });
        getAllListItems();
    }


    @Override
    protected void retrieveCategories() {
        new RetrieveCategories().execute();
    }

    // used for testing
    public void setDBHelper(DataBaseHelper helper) {
        this.helper = helper;
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case (R.id.add_category): {
                String title = name.getText().toString();
                if (title.isEmpty()) {
                    showToast("empty categories are not allowed");
                    return;
                }
                if (helper.categoryExist(title)) {
                    showToast("category already exist");
                    return;
                }
                // add to db
                new Category(title, null).save();
                // referesh list
                retrieveCategories();
                name.getText().clear();
                break;
            }
            case (R.id.category_remove): {
                String title = name.getText().toString();
                if (!helper.categoryExist(title)) {
                    showToast("category does not exist");
                    return;
                }
                helper.deleteCategory(title);
                // referesh list
                retrieveCategories();
                name.getText().clear();
                break;
            }
            case (R.id.fab_left): {
                name.setCursorVisible(false);
                break;
            }
        }
    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        boolean handled = false;
        if (actionId == EditorInfo.IME_ACTION_SEND || actionId == EditorInfo.IME_ACTION_NEXT
                || actionId == EditorInfo.IME_ACTION_DONE) {
            hideSoftKey();
        }
        return handled;
    }

    /**
     * retrieve categories from the database
     */
    private class RetrieveCategories extends AsyncTask<Void, Void, Void> {
        private ProgressDialog progress = null;

        @Override
        protected Void doInBackground(Void... params) {
            listItems = helper.getCategoriesTitles();
            return null;
        }

        @Override
        protected void onPreExecute() {
            progress = ProgressDialog.show(
                    CategoriesDBMenu.this, null,
                    "Loading Categories...");
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(final Void result) {
            updateListView();
            progress.dismiss();
            super.onPostExecute(result);
        }
    }

}
