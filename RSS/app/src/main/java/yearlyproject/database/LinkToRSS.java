package yearlyproject.database;

import com.orm.SugarRecord;

/**
 * @author kfirlan
 */
public class LinkToRSS extends SugarRecord {
    public String title;
    public String link;

    public LinkToRSS() {
    }

    public LinkToRSS(String title, String link) {
        this.title = title;
        this.link = link;
    }

    @Override
    public String toString() {
        return "title: " + title + " link: " + link;
    }
}
