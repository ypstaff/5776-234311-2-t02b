package yearlyproject.database;


import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * @author kfirlan
 */
public class DataBaseHelper {
    private static DataBaseHelper ourInstance = new DataBaseHelper();

    private DataBaseHelper() {
    }

    public static DataBaseHelper getInstance() {
        return ourInstance;
    }

    public boolean categoryExist(String title) {
        String[] args = {title};
        long tmp = Category.count(Category.class, "title = ? ", args);
        if (tmp <= 0) {
            return false;
        }
        return true;
    }

    public boolean linkExists(String categoryTitle, String linkTitle) {
        return getLink(categoryTitle, linkTitle) != null;
    }

    public List<String> getCategoriesTitles() {
        // select title instead of select * does not work
        List<Category> categories = Category.findWithQuery(Category.class,
                "SELECT * FROM Category GROUP BY title ORDER BY title");
        List<String> res = new ArrayList<>(categories.size());
        for (Category c : categories) {
            res.add(c.title);
        }
        return res;
    }

    public List<LinkToRSS> getLinks(String categoryTitle) {
        List<LinkToRSS> links = LinkToRSS.findWithQuery(LinkToRSS.class,
                "SELECT * FROM Category c, Link_To_RSS l WHERE l.id = c.link and c.title = ? ORDER BY l.title", categoryTitle);
        return links;
    }

    public LinkToRSS getLink(String categoryTitle, String linkTitle) {
        List<LinkToRSS> links = LinkToRSS.findWithQuery(LinkToRSS.class,
                "SELECT * FROM Category c, Link_To_RSS l WHERE l.id = c.link and c.title = ? and l.title = ?", categoryTitle, linkTitle);
        if (links.size() != 1) {
            Log.e(this.getClass().getSimpleName(), "found " + links.size() + " links intead of one link");
            return null;
        }
        return links.get(0);
    }

    private void removeFromCategory(String categoryTitle, LinkToRSS link) {
        Category.deleteAll(Category.class, "title = ? and link = ?", categoryTitle, link.getId().toString());
    }

    public void deleteLink(String categoryTitle, String linkTitle) {
        LinkToRSS linkToRSS = this.getLink(categoryTitle, linkTitle);
        this.removeFromCategory(categoryTitle, linkToRSS);
        linkToRSS.delete();
    }

    public void deleteCategory(String categoryTitle) {
        List<Category> categories = Category.find(Category.class, " title = ? ", categoryTitle);
        for (Category category : categories) {
            if (category.link != null) {
                category.link.delete();
            }
            category.delete();
        }
    }

}
