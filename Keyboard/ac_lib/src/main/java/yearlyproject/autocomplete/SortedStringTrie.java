package yearlyproject.autocomplete;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

/**
 * Created by Sergey on 06/12/2015.
 */
public class SortedStringTrie {

	private List<SortedStringTrie> children;
    private String value;
    private int weight;
    private boolean isEndChar;

    public SortedStringTrie() {
    	this(0,"",new ArrayList<SortedStringTrie>(),false);
    }
    
    public SortedStringTrie(int weight,String value,List<SortedStringTrie> children,boolean isEndChar) {
        this.value=value;
        this.weight=weight;
        this.children=children;
        this.isEndChar=isEndChar;
    }

    public SortedStringTrie(int weight,String value) {
        this(weight,value,new ArrayList<SortedStringTrie>(),false);
    }

    public SortedStringTrie(int weight,String value,boolean isEndChar) {
        this(weight,value,new ArrayList<SortedStringTrie>(),isEndChar);
    }


    public void insert(String word) {
        for(SortedStringTrie child : children) {
            if(child.value.equals(word.substring(0,1))) {
                if(word.length()>1) {
                    child.insert(word.substring(1, word.length() ));
                } else {
                    child.isEndChar=true;
                }
                child.weight+=1;
                sort(children.indexOf(child));
                return;
            }
        }
        children.add(new SortedStringTrie(0,word.substring(0,1)));
        if(word.length()==1) {
            children.get(children.size()-1).isEndChar=true;
            return;
        };
        children.get(children.size()-1).insert(word.substring(1, word.length()));
    }

    public boolean exists(String word) {
        for(SortedStringTrie child : children) {
            if(child.value.equals(word.substring(0,1))) {
                if(word.length()==1) return child.isEndChar;
                return child.exists(word.substring(1, word.length()));
            }
        }
        return false;
    }

    private List<String> getMostCommonByPrefix(String prefix,int maxWords,int depth) {
        ArrayList<String> res=new ArrayList<String>();
        List<String> tmp;
        if(prefix.length()>0) {
            for (SortedStringTrie child : children) {
                if (child.value.equals(prefix.substring(0,1))) {
                    tmp=child.getMostCommonByPrefix(prefix.substring(1), maxWords);
                    for(String s : tmp) {
                        if((this.value+s).length()>1)
                            res.add(this.value+s);
                    }
                    return res;

                }
            }
        } else {
            return this.getMostCommon(maxWords);
        }
        return res;
    }

    public List<String> getMostCommonByPrefix(String prefix, int maxWords) {
        ArrayList<String> res=new ArrayList<String>();
        List<String> tmp;
        if(prefix.length()>0) {
            for (SortedStringTrie child : children) {
                if (child.value.equals(prefix.substring(0,1))) {
                    tmp=child.getMostCommonByPrefix(prefix.substring(1), maxWords);
                    for(String s : tmp) {
                        res.add(this.value+s);
                    }
                    return res;

                }
            }
        } else {
            return this.getMostCommon(maxWords);
        }
        return res;
    }

    public void remove(String word) {

    }

    public static void serealize(OutputStream os,SortedStringTrie trie) {
        SortedStringTrieSerializer serealizer = new SortedStringTrieSerializer();
        serealizer.serealize(os,trie);
    }

    public static SortedStringTrie deserealize(InputStream is) {
        SortedStringTrieSerializer deserealizer = new SortedStringTrieSerializer();
        return deserealizer.deserialize(is);
    }

    //private functions

    private List<String> getMostCommon(int maxWords) {
        ArrayList<String> res=new ArrayList<String>();
        List<String> tmp=null;
        if(children.size()==0) {
            res.add(value);
            return res;
        }
        if(this.isEndChar) {
        	res.add(value);
        }
        for(SortedStringTrie child : children) {
            if(res.size()>=maxWords) return res.subList(0,maxWords);
            tmp=child.getMostCommon(maxWords);
            for(String s : tmp) {
                res.add(value+s);
            }
        }
        return res;
    }


    @SuppressWarnings("unused")
	private String getLongestCommonSubstring(String word, HashSet<String> values) {
        String subWord=word.substring(0,1);
        for(int i=0;i<word.length();i--) {
            if(!values.contains(subWord)) {
                return subWord;
            } else {
                subWord=subWord.substring(0,i);
            }
        }
        return subWord;
    }
    
    private void sort(int changedIndex) {
        if(changedIndex==0) return;
        SortedStringTrie tmp=null;
        for(int i=changedIndex;i>0;i--) {
            if(children.get(i).weight<=children.get(i-1).weight) {
                break;
            } else {
                tmp=children.get(i);
                children.remove(i);
                children.add(i-1,tmp);
            }
        }
    }

    private static class SortedStringTrieSerializer {
        SortedStringTrieSerializer() {
        }

        public SortedStringTrie deserialize(InputStream is) {
            SortedStringTrie res=new SortedStringTrie();
            try {
                ObjectInputStream ois=new ObjectInputStream(is);
                res= deserealizeAux(ois);
                ois.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return res;
        }

        private SortedStringTrie deserealizeAux(ObjectInputStream is) {
            SortedStringTrie trie=new SortedStringTrie();
            try {
                trie.value = (String) is.readObject();
                trie.weight = is.readInt();
                trie.isEndChar = is.readBoolean();
                int childrenNum = is.readInt();
                for (int i = 0; i < childrenNum; i++) {
                    trie.children.add(deserealizeAux(is));
                }
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return trie;
        }

        public void serealize(OutputStream os, SortedStringTrie trie) {
            try {
                ObjectOutputStream oos = new ObjectOutputStream(os);
                serealizeAux(oos,trie);
                oos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        private void serealizeAux(ObjectOutputStream os, SortedStringTrie trie) {
            try {
                os.writeObject(trie.value);
                os.writeInt(trie.weight);
                os.writeBoolean(trie.isEndChar);
                os.writeInt(trie.children.size());
                for (SortedStringTrie child : trie.children) {
                    serealizeAux(os, child);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}
