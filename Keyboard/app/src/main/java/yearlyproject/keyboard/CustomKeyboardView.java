package yearlyproject.keyboard;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.inputmethodservice.Keyboard;
import android.inputmethodservice.KeyboardView;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.widget.Toast;

import com.google.common.base.Function;
import com.google.common.collect.Collections2;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.primitives.Ints;

import java.security.Key;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * Created by roman on 11/8/2015.
 * Drawing logic implemented by Kfir
 */
public class CustomKeyboardView extends KeyboardView
{

    Context m_context; //needed to get resources for drawing
    List<Integer> m_currentKeyGroup;
    List<List<Integer>> m_groupChoices = new ArrayList<>();
    public CustomKeyboardView(Context context, AttributeSet attrs) {
        super(context, attrs);
        m_currentKeyGroup= new ArrayList<Integer>();
        m_context=context;
    }

    @Override
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        Iterable<Keyboard.Key> keys = getKeysToPaint(m_currentKeyGroup);
        for (Keyboard.Key key : keys){
            highLightKey(key,canvas);
        }
        for (List<Integer> list : m_groupChoices){
            highlightGroup(getKeysToPaint(list),canvas);
        }
    }

    private Iterable<Keyboard.Key> getKeysToPaint(List<Integer> keyGroup) {

        final Collection<Integer> keyCodesGroup = keyGroup;
        // filter all keys to get the keys with specified key codes
        return Iterables.filter(getKeyboard().getKeys(), new com.google.common.base.Predicate<Keyboard.Key>() {
            @Override
            public boolean apply(Keyboard.Key input) {
                return !Collections.disjoint(keyCodesGroup, Ints.asList(input.codes));
            }
        });
    }


    private void highLightKey(Keyboard.Key key,Canvas canvas){
        Drawable dr = ContextCompat.getDrawable(m_context, R.drawable.key_background);
        dr.setBounds(key.x, key.y, key.x + key.width, key.y + key.height);
        dr.draw(canvas);
    }

    /**
     * set both current group and parent group
     * @param currentGroup current group to be selected
     * @param allGroups group that all the group next iterate over
     */
    public void setCurrentGroup(Collection<String> currentGroup,List<List<String>> allGroups){
        m_currentKeyGroup= codesToIntegers(currentGroup);
        m_groupChoices.clear();
        for (List<String> list: allGroups){
            m_groupChoices.add(codesToIntegers(list));
        }
    }

    private List<Integer> codesToIntegers(Collection<String> currentGroup) {

        return Lists.newArrayList(Iterables.transform(currentGroup, new Function<String, Integer>() {
            @Override
            public Integer apply(String from) {
                return Integer.parseInt(from.split(",")[0]); // the first keycode from each group is enough
            }
        }));
    }

    private void highlightGroup(Iterable<Keyboard.Key> keys, Canvas canvas){
        Keyboard.Key first = keys.iterator().next();
        Keyboard.Key last = Iterables.getLast(keys);
        Drawable dr = ContextCompat.getDrawable(m_context, R.drawable.key_choices);
        dr.setBounds(first.x, first.y, last.x + last.width, last.y + last.height);
        dr.draw(canvas);
    }
}
