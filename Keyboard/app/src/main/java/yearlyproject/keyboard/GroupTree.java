package yearlyproject.keyboard;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sergey on 07/11/2015.
 */
public class GroupTree<E> {
    private List<GroupTree> children;
    private boolean isSelected;
    private GroupTree<E> parent;
    private List<E> group;


    public GroupTree(List<GroupTree> children,GroupTree<E> parent,boolean isSelected,List<E> group) {
        this.children=children;
        this.isSelected=isSelected;
        this.parent=parent;
        this.group=group;
    }

    public GroupTree(ArrayList<GroupTree> children,GroupTree parent) {
        isSelected=false;
        this.group=new ArrayList<E>();
        this.parent=parent;
        this.children=children;
    }

    public void addChild(GroupTree<E> child) {
        if(children==null) {
            children=new ArrayList<GroupTree>();
        }
        children.add(child);
        child.parent=this;
        child.isSelected=false;
    }

    /***
     * selects next child <i>s</i> of the parent <i>p</i> of the <b>currently selected</b> node.
     * @return the union of the elements which belong to the groups of the leafs in the
     * sub-tree of which <i>s</i> is the root.
     */
    public List<E> selectNextSibling() {
        if(children==null) return null;
        for(GroupTree child : children) {
            if(child.isSelected) {
                child.isSelected=false;
                int newIndex=(children.indexOf(child)+1)%children.size();
                children.get(newIndex).isSelected=true;
                return children.get(newIndex).returnSubgroupsUnion();
            }
        }
        if(children==null) return null;
        for(GroupTree child : children) {
            List<E> $=child.selectNextSibling();
            if($!=null) {
                return $;
            }
        }
        return null;
    }

    /***
     * selects the parent <i>p</i> of the <b>currently selected</b> node.
     * @return the union of the elements which belong to the groups of the leafs in the
     * sub-tree of which <i>p</i> is the root.
     */
    public List<E> selectParent() {
        if(this.parent!=null && this.parent.parent!=null && this.isSelected) {
            this.isSelected=false;
            this.parent.isSelected=true;
            return this.parent.returnSubgroupsUnion();
        }
        if(this.parent!=null && this.parent.parent==null && this.isSelected) {
            return this.returnSubgroupsUnion();
        }
        if(children==null) {
            return null;
        }
        List<E> $;
        for(GroupTree<E> child : children) {
            $=child.selectParent();
            if($!=null) {
                return $;
            }
        }
        return null;

    }

    /***
     * selects the child <i>c</i> of the <b>currently selected</b> node.
     * @return the union of the elements which belong to the groups of the leafs in the
     * sub-tree of which <i>c</i> is the root.
     */
    public List<E> selectChild() {
        if(isSelected) {
            if(children==null) {
                return this.group;
            }
            isSelected=false;
            children.get(0).isSelected=true;
            return children.get(0).returnSubgroupsUnion();
        }
        if(children==null) return null;
        for(GroupTree child : children) {
            List<E> $=child.selectChild();
            if($!=null) {
                return $;
            }
        }
        return null;
    }

    /***
     * for a GroupTree t returns a group of elements of type E which is a union of
     * the groups of all the leafs in t.
     * @return
     */
    private List<E> returnSubgroupsUnion() {
        if(children==null) {
            return group;
        }
        ArrayList<E> $=new ArrayList<E>();
        for(GroupTree child : children ) {
            $.addAll(child.returnSubgroupsUnion());
        }
        return $;
    }

    /***
     *
     * @return returns the group of elements of type E which belong to the currently selected sub-tree.
     */
    public List<E> getSelectedGroup() {
        if(isSelected) {
            return returnSubgroupsUnion();
        } else if(children!=null) {
            List<E> $;
            for(GroupTree child : children) {
                $=child.getSelectedGroup();
                if($!=null) return $;
            }
        }
        return null;
    }

    /***
     *
     * @return all the groups of the siblings of the currently selected node.
     */
    public List<List<E>> getAllSiblingsOfSelectedNode() {
        if(children==null) return null;
        for(GroupTree<E> child : children) {
            if(child.isSelected) {
                List<List<E>> $=new ArrayList<>();
                for(GroupTree<E> sibling : children)
                    $.add(sibling.returnSubgroupsUnion());
                return $;
            }
        }
        for(GroupTree<E> child : children) {
            List<List<E>> res=child.getAllSiblingsOfSelectedNode();
            if(res!=null) {
                return res;
            }
        }
        return null;
    }
}

