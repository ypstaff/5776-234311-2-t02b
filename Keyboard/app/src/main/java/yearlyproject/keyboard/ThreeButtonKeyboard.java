package yearlyproject.keyboard;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.inputmethodservice.InputMethodService;
import android.inputmethodservice.Keyboard;
import android.inputmethodservice.KeyboardView;
import android.view.View;
import android.view.inputmethod.EditorInfo;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Sergey on 30/03/2016.
 */
public class ThreeButtonKeyboard extends InputMethodService
        implements KeyboardView.OnKeyboardActionListener {

    public static final String BLUETOOTH_CONNECTOR_BT_MESSAGE_RECEIVED = "BluetoothConnector.BT_MESSAGE_RECEIVED";
    public static final String NEXT = "next";
    public static final String SELECT = "select";
    public static final String BACK = "back";
    public static final String INTENT_MSG_FIELD = "msg";
    private Resources m_resources;
    private KeyboardState m_keyboardState;
    private InputTypeHandler m_inputTypeHandler;
    private BranchingFactorsProducer m_braBranchingFactorsProducer;

    private GroupTree<String> m_GroupTree;
    private List<String> m_currentKeyGroup;
    private List<String> m_firstGroup;

    /***
     * Prepares the View, when an Input Method action is chosen
     * @return View - the CustomKeyboardView we've created
     */
    @Override
    public View onCreateInputView() {
        //android.os.Debug.waitForDebugger();
        m_resources = getResources();
        m_keyboardState=new KeyboardState();
        m_keyboardState.setCapsOn(false);
        m_keyboardState.setSymbolsView(false);
        m_keyboardState.setCustomKeyboardView(
                (CustomKeyboardView) getLayoutInflater().inflate(R.layout.keyboard, null));
        return  m_keyboardState.getCustomKeyboardView();
    }

    /***
     *
     * @param info
     * @param restarting
     */
    @Override //need to change InputTypeHandler in this function.
    public void onStartInputView(EditorInfo info, boolean restarting) {

        if(info.inputType==EditorInfo.TYPE_CLASS_NUMBER) {
            m_inputTypeHandler=new NumbersInputTypeHandler(this);
            m_braBranchingFactorsProducer = new NumbersBranchingFactorsProducer();
            initiateInputTypeView(R.xml.numbers);
        } else {
            m_inputTypeHandler = new TextInputTypeHandler(m_keyboardState,this);
            m_braBranchingFactorsProducer = new TextBranchingFactorsProducer();
            initiateInputTypeView(R.xml.qwerty);
        }
        super.onStartInputView(info, restarting);

    }

    //KeyboardView.OnKeyboardActionListener methods implementation
    @Override
    public void onPress(int primaryCode) {

    }

    @Override
    public void onRelease(int primaryCode) {

    }

    /***
     * Called when a key is pressed. Changes the selected groups according to the pressed key.
     * @param primaryCode
     * @param keyCodes
     */
    @Override
    public void onKey(int primaryCode, int[] keyCodes) {
        //selectKey(primaryCode, keyCodes); //TODO: remove after the following code works
        if (primaryCode == m_resources.getInteger(R.integer.selectCode)) {
            if (m_GroupTree.getSelectedGroup().size() == 1) {
                m_inputTypeHandler.onClick(Integer.parseInt(m_currentKeyGroup.get(0).split(",")[0]), m_keyboardState);
            } else {
                clearAndAddAll(m_GroupTree.selectChild());
            }
        }
        if (primaryCode == m_resources.getInteger(R.integer.nextCode)) {
            clearAndAddAll(m_GroupTree.selectNextSibling());
        }
        if (primaryCode == m_resources.getInteger(R.integer.backCode)) {
            if(m_GroupTree.selectParent().equals(m_currentKeyGroup)){ //Back when we're in the main group.
                resetGUIState();
            }
            else{
                clearAndAddAll(m_GroupTree.getSelectedGroup());
            }

        }
        CustomKeyboardView ckv=m_keyboardState.getCustomKeyboardView();
        ArrayList<String> clone =  new ArrayList<String>();
        clone.addAll(m_currentKeyGroup);
        ckv.setCurrentGroup(clone, m_GroupTree.getAllSiblingsOfSelectedNode());
        ckv.invalidateAllKeys(); //for redrawing of the keyboard
    }



    /***
     * Resets the keyboard GUI to the original state
     */
    protected void resetGUIState() {
        List<String> tmp = m_GroupTree.selectParent();
        while (!m_GroupTree.selectParent().equals(tmp)) {
            tmp = m_GroupTree.selectParent();
        }
        //Get to the first Group
        while (!m_GroupTree.selectNextSibling().equals(m_firstGroup)) {
        }
        m_currentKeyGroup=(m_GroupTree.getSelectedGroup());
    }

    /***
     * Switches the inputTypeHandler and the keyboard view according to the given layout.
     * @param layout
     */
    protected void initiateInputTypeView(int layout) {
        initiateGroups(layout);
        changeKeyboard(layout);
    }

    /***
     * Change the layout of the keyboard according to the given layout.
     * @param xmlLayoutResId
     */
    private void changeKeyboard(int xmlLayoutResId){
        Keyboard m_keyboard = new Keyboard(this, xmlLayoutResId);
        CustomKeyboardView m_ckv= m_keyboardState.getCustomKeyboardView();
        m_keyboardState.setKeyboard(m_keyboard);
        m_ckv.setKeyboard(m_keyboard);
        m_ckv.setOnKeyboardActionListener(this);
        m_ckv.setCurrentGroup(m_currentKeyGroup, m_GroupTree.getAllSiblingsOfSelectedNode());
        m_ckv.invalidateAllKeys(); //for redrawing of the keyboard
    }

    /***
     * Create key groups according to the given layout
     * @param xmlLayoutResId
     */
    private void initiateGroups(int xmlLayoutResId) {
        XmlKeyGroupsCreator xmlKeyGroupsCreator = new XmlKeyGroupsCreator();
        ArrayList<ArrayList<String>> rows = xmlKeyGroupsCreator.create(this, xmlLayoutResId);
        GroupTreeBuilder groupTreeBuilder = new GroupTreeBuilder();

        HashMap<Integer,List<Integer>> specialRows =
                m_braBranchingFactorsProducer.produceSpecialBranchingFactors(xmlLayoutResId,rows);
        List<Integer> defaultBranchingFactors =
                m_braBranchingFactorsProducer.produceDefaultBranchingFactors(xmlLayoutResId);
        m_GroupTree=groupTreeBuilder.buildTree(rows, defaultBranchingFactors, specialRows);
        m_currentKeyGroup=m_GroupTree.getSelectedGroup();
        m_firstGroup=new ArrayList<String>();
        m_firstGroup.addAll(m_currentKeyGroup);
    }

    private void clearAndAddAll(List<String> group) {
        m_currentKeyGroup.clear();
        m_currentKeyGroup.addAll(group);
    }


    //bluetooth messages receiver
    private final BroadcastReceiver btMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (BLUETOOTH_CONNECTOR_BT_MESSAGE_RECEIVED.equals(action)) {
                //String msg = intent.getData().toString();
                String msg = intent.getStringExtra(INTENT_MSG_FIELD);
                performAction(msg.split("\\u0000")[0]);
            }
        }
    };

    public void performAction(final String s) {
        if (s.equals(NEXT))
            onKey(m_resources.getInteger(R.integer.nextCode),null);
        if (s.equals(SELECT))
            onKey(m_resources.getInteger(R.integer.selectCode),null);
        if (s.equals(BACK))
            onKey(m_resources.getInteger(R.integer.backCode),null);
    }

    @Override
    public void onStartInput(EditorInfo attribute, boolean restarting) {
        super.onStartInput(attribute, restarting);
        if(btMessageReceiver!=null){
            registerReceiver(btMessageReceiver, new IntentFilter(BLUETOOTH_CONNECTOR_BT_MESSAGE_RECEIVED));
        }
    }

    @Override
    public void onText(CharSequence text) {

    }

    @Override
    public void swipeLeft() {

    }

    @Override
    public void swipeRight() {

    }

    @Override
    public void swipeDown() {

    }

    @Override
    public void swipeUp() {

    }
}
