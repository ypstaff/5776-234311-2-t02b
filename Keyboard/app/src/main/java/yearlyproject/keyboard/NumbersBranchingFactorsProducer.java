package yearlyproject.keyboard;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Sergey on 18/04/2016.
 */
public class NumbersBranchingFactorsProducer implements BranchingFactorsProducer {
    @Override
    public List<Integer> produceDefaultBranchingFactors(int xmlLayoutResID) {
        return  new ArrayList<Integer>(Arrays.asList(2, -1));
    }

    @Override
    public HashMap<Integer,List<Integer>>
    produceSpecialBranchingFactors(int xmlLayoutResID, List<? extends List<String>> rows) {
        return new HashMap<Integer,List<Integer>>();
    }
}
