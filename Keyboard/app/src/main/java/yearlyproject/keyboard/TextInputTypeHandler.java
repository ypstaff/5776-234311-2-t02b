package yearlyproject.keyboard;

import android.inputmethodservice.Keyboard;
import android.view.inputmethod.ExtractedTextRequest;
import android.view.inputmethod.InputConnection;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import yearlyproject.keyboard.SpellChecking.SpellChecker;

/**
 * Created by Sergey on 30/03/2016.
 */
public class TextInputTypeHandler extends InputTypeHandler {
    private Consumer<List<String>> suggestionsConsumer;
    private SpellChecker ac;
    private ThreeButtonKeyboard parent;

    public TextInputTypeHandler(final KeyboardState state,ThreeButtonKeyboard parent) {
        super(parent);
        this.parent=parent;
        suggestionsConsumer =  new Consumer<List<String>>() {
            @Override
            public void accept(List<String> strings) {
                int remaining=strings.size();
                ArrayList<String> al=new ArrayList<String>();
                for(int i=0;i<4;i++) {
                    if(i<remaining)
                        al.add(strings.get(i));
                    else
                        al.add(" ");
                }
                for(Keyboard.Key key : state.getKeyboard().getKeys() ) {
                    if(key.codes[0]>=1000 && key.codes[0]<=1003) {
                        key.text=al.get(key.codes[0]-1000).toString();
                        key.label=al.get(key.codes[0]-1000).toString();
                    }
                }
                state.getCustomKeyboardView().invalidateAllKeys();
            }
        };
        ac = new SpellChecker(parent.getApplicationContext(),suggestionsConsumer);
    }



    @Override
    protected void onKeyCodeDelete(InputConnection ic) {
        super.onKeyCodeDelete(ic);
        changeAutoCorrectSuggestions(ic);
    }

    @Override
    protected void onKeyCodeShift(KeyboardState state) {
        //added validity checks for the values of the autocomplete keys, otherwise reading them
        //would cause errors.
        for(Keyboard.Key key : state.getKeyboard().getKeys()) {
            if(key.codes[0]>=1000 && key.codes[0]<=1003 && (key.text==null || key.label==null ||
                    key.text.equals("") || key.label.equals(""))) {
                key.text=" ";
                key.label=" ";
            }
        }
        super.onKeyCodeShift(state);
    }

    @Override
    protected void onOther(InputConnection ic, KeyboardState state, int KeyCode) {
        if(KeyCode==-105) {
            //Symbols key
            toggleSymbols(state);
            return;
        } else if(KeyCode==-103) {
            //Copy key
            ic.beginBatchEdit();
            ic.performContextMenuAction(android.R.id.selectAll);
            ic.performContextMenuAction(android.R.id.copy);
            ic.endBatchEdit();

            String text=ic.getSelectedText(0).toString();
            int textLength=text.length();
            ic.setSelection(textLength,textLength);
            return;
        } else if(KeyCode==-104) {
            //Paste key
            ic.beginBatchEdit();
            ic.performContextMenuAction(android.R.id.paste);
            ic.endBatchEdit();
            return;
        } else if(KeyCode<1000) {
            //not an autocomplete key.
            char code = (char) KeyCode;
            if (Character.isLetter(code) && state.isCapsOn()) {
                code = Character.toUpperCase(code);
                // lmaman: toggle shift key (keyCodes never used, so i can send this one)
                onClick(Keyboard.KEYCODE_SHIFT, state);
            }
            ic.commitText(String.valueOf(code), 1);
            changeAutoCorrectSuggestions(ic);
        } else {
            //autocomplete key
            onAutoCorrectKeySelect(ic,(char) KeyCode,state);
        }
    }

    //private functions
    private void toggleSymbols(KeyboardState state){
        int layoutID=0;
        boolean symbols=state.isSymbolsView();
        if(symbols) {
            layoutID=R.xml.qwerty;
        } else {
            layoutID=R.xml.symbols;
        }
        state.setSymbolsView(!symbols);
        parent.initiateInputTypeView(layoutID);
    }
    //Retrieving the sentence written so far, and requesting suggestions for the last word.
    private void changeAutoCorrectSuggestions(InputConnection ic) {
        if(!ac.isTurnedOn()) return;
        CharSequence text=ic.getExtractedText(new ExtractedTextRequest(),0).text;
        if(text.length()==0){
            text="hello";
        }
        String[] words = text.toString().split(" ");

        ArrayList<String> sentence=new ArrayList<String>();

        for(int i=0;i<words.length;i++) {
            String tmp=words[i].replaceAll("[^a-zA-Z0-9]", "");
            if(!tmp.equals(""))
                sentence.add(tmp);
        }
        if(text.charAt(text.length()-1)==' ' || sentence.size()==0) {
            sentence.add("hello");
        }

        ac.getSentenceSuggestions(sentence, 4, sentence.size() - 1);
    }

    //Updating the InputConnection after a key which held a suggestion waas pressed
    private void onAutoCorrectKeySelect(InputConnection ic,int keyCode,KeyboardState state) {
        CharSequence newWord="";
        for(Keyboard.Key key : state.getKeyboard().getKeys()) {
            if(key.codes[0]==keyCode) {
                newWord=key.text;
                break;
            }
        }
        if(newWord==null || newWord.toString().equals("") || newWord.toString().equals(" ")) {
            return;
        }
        CharSequence currentText=ic.getExtractedText(new ExtractedTextRequest(),0).text;
        boolean isNewWord = ic.getTextBeforeCursor(1,0).toString().equals(" ");
        ArrayList<String> currentTextAsSentence=new ArrayList<>(Arrays.asList(currentText.toString().split(" ")));
        if(!isNewWord) {
            currentTextAsSentence.remove(currentTextAsSentence.size()- 1);
        }
        currentTextAsSentence.add(newWord.toString());
        ic.deleteSurroundingText(ic.getTextBeforeCursor(1000,0).length(),ic.getTextAfterCursor(1000,0).length());
        String newSequence="";
        for(int i=0;i<currentTextAsSentence.size();i++) {
            newSequence+=currentTextAsSentence.get(i)+" ";
        }
        ic.commitText(newSequence, 1);
        changeAutoCorrectSuggestions(ic);
    }
}
