package yearlyproject.keyboard;

/**
 * Created by Sergey on 03/04/2016.
 */
public class NumbersInputTypeHandler extends InputTypeHandler {

    private ThreeButtonKeyboard parent;

    public NumbersInputTypeHandler(ThreeButtonKeyboard parent) {
        super(parent);
        this.parent=parent;
    }
}
