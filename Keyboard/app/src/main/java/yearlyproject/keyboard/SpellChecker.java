package yearlyproject.keyboard;

import android.annotation.TargetApi;
import android.content.Context;
import android.inputmethodservice.InputMethodService;
import android.os.Build;
import android.view.textservice.SentenceSuggestionsInfo;
import android.view.textservice.SpellCheckerSession;
import android.view.textservice.SuggestionsInfo;
import android.view.textservice.TextInfo;
import android.view.textservice.TextServicesManager;
import android.widget.Toast;

import com.google.common.base.Function;
import com.google.common.collect.Collections2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

/**
 * Created by kfirlan on 10/11/2015.
 *
 * @author kfir
 */
public class SpellChecker {

    public static final int DEFAULT_SIZE = 4;
    Consumer<List<String>> consumer;
    private Context context;

    public SpellChecker(Context context, Consumer<List<String>> consumer) {
        this.context = context;
        this.consumer = consumer;
    }

    /**
     * USE ON API < 16 (uses methods that are deprecated on api 16)
     * get corrections suggestions for str and activate
     * the consumer (from the constructor) on it
     *
     * @param str   the string to get corrections suggestions on
     * @param s_num maximum number of suggestions consumed
     */
    public void getSuggestions(String str, int s_num) {
        final TextServicesManager tsm = (TextServicesManager) context.getSystemService(Context.TEXT_SERVICES_MANAGER_SERVICE);
        final SpellCheckerSession scs = tsm.newSpellCheckerSession(null, null, new SpellCheckerSessionListener(0), true);
        scs.getSuggestions(new TextInfo(str), s_num);
    }

    /**
     * USE ON API >= 16 (uses methods that exist only from api 16)
     * get corrections suggestions for a word from the  sentence and activate
     * the consumer (from the constructor) on it
     *
     * @param sentence list of string composing a sentence
     * @param s_num    maximum number of suggestions consumed
     * @param pos      position of the word u want a suggestion on in the sentence
     */
    @TargetApi(16)
    public void getSentenceSuggestions(final List<String> sentence, int s_num, int pos) {
        final TextServicesManager tsm = (TextServicesManager) context.getSystemService(Context.TEXT_SERVICES_MANAGER_SERVICE);
        final SpellCheckerSession scs = tsm.newSpellCheckerSession(null, null, new SpellCheckerSessionListener(pos), true);
       /* map function strings to textinfo and turn them to array*/
        Object[] tmp = Collections2.transform(sentence, new Function<String, TextInfo>() {
            @Override
            public TextInfo apply(String from) {
                return new TextInfo(from);
            }
        }).toArray();
        TextInfo[] textInfos = Arrays.copyOf(tmp,tmp.length,TextInfo[].class);
        scs.getSentenceSuggestions(textInfos, s_num);
    }


    private class SpellCheckerSessionListener
            implements SpellCheckerSession.SpellCheckerSessionListener {
        private int pos = 0;

        private SpellCheckerSessionListener(int pos) {
            this.pos = pos;
        }

        /**
         * async task, callback for SpellCheckerSession.getSuggestions
         *
         * @param results
         */
        @Override
        public void onGetSuggestions(final SuggestionsInfo[] results) {
            List<String> suggestions = new ArrayList<>();
            for (int i = 0; i < results.length; ++i) {
                final int len = results[i].getSuggestionsCount();
                for (int j = 0; j < len; ++j) {
                    suggestions.add(results[i].getSuggestionAt(j));
                }
            }
            consumer.accept(suggestions);
        }

        /**
         * async task, callback for SpellCheckerSession.getSentenceSuggestions
         *
         * @param results
         */
        @Override
        @TargetApi(16)
        public void onGetSentenceSuggestions(SentenceSuggestionsInfo[] results) {
            List<String> suggestions = new ArrayList<>();
            for (int i = 0; i <  results[pos].getSuggestionsCount(); ++i) {
                final int len = results[pos].getSuggestionsInfoAt(i).getSuggestionsCount();
                for (int j = 0; j < len; ++j) {
                    suggestions.add(results[pos].getSuggestionsInfoAt(i).getSuggestionAt(j));
                }
            }
            consumer.accept(suggestions);
        }
    }


}
