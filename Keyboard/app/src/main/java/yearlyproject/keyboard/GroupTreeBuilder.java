package yearlyproject.keyboard;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Sergey on 07/11/2015.
 */
public class GroupTreeBuilder<E> {

    public GroupTreeBuilder() {
    }

    private GroupTree<E> createLeaf(List<E> group) {
        return new GroupTree<E>(null,null,false,group);
    }

    private  GroupTree<E> createNode() {
        return new GroupTree<E>(null,null,false,null);
    }

    private GroupTree<E> createRoot() {
        return new GroupTree<E>(null,null,true,null);
    }


    private GroupTree<E> buildTreeAux(int level,List<E> group,List<Integer> branchingFactors) {
        GroupTree<E> $;
        if(group.size()==1) {
            $=createLeaf(group);
            return $;
        }
        $=createNode();
        int startingIndex=0,endingIndex=0;
        int branchingFactor=(branchingFactors.get(level-1)!=-1?branchingFactors.get(level-1):group.size());
        for(int i=0;i<branchingFactor;i++) {
            startingIndex=(group.size()/branchingFactor)*i;
            endingIndex=(group.size()/branchingFactor)*(i+1);
            if(group.size()%branchingFactor!=0 && i==branchingFactor-1) {
                endingIndex+=group.size()%branchingFactor;
            }
            $.addChild(buildTreeAux(level+1,
                    new ArrayList<E>(group.subList(startingIndex,endingIndex)),
                    branchingFactors));
        }
        return $;
    }


    /***
     *
     * @param groups the groups of elements of type E which will be assigned to each child of the root
     *               and split again according to the branching factors.
     * @param branchingFactors determines the amount of subgroups which will be created from
     *                         every group. the <i>i-th</i> element in this list
     *                         determines the branching of the <i>(i+1)-th<i/>
     *                         level in the tree, as the first level is always split into groups.size()
     * @return  a GroupTree created according to the division of <i>groups</i> param and the
     * branching factors.
     */
    public GroupTree<E> buildTree(ArrayList<ArrayList<E>> groups,ArrayList<Integer> branchingFactors) {
        GroupTree<E> root=createRoot();
        for(ArrayList<E> group : groups) {
            root.addChild(buildTreeAux(1, group,branchingFactors));
        }
        root.selectChild();
        return root;
    }

    public GroupTree<E> buildTree(List<List<E>> groups,List<Integer> defaultBranchingFactors,
                                  HashMap<Integer,List<Integer>> branchingFactorLists) {
        GroupTree<E> root=createRoot();
        for(int i=0;i<groups.size();i++) {
            if(branchingFactorLists.containsKey(i))
                root.addChild(buildTreeAux(1, groups.get(i), branchingFactorLists.get(i)));
            else
                root.addChild(buildTreeAux(1, groups.get(i), defaultBranchingFactors));
        }
        root.selectChild();
        return root;
    }
}
