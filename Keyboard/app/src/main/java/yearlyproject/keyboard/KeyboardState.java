package yearlyproject.keyboard;

import android.inputmethodservice.Keyboard;

import java.util.ArrayList;

/**
 * Created by Sergey on 30/03/2016.
 */
public class KeyboardState {
    public KeyboardState(Keyboard m_keyboard, boolean m_capsOn, boolean m_symbolsView,
                         CustomKeyboardView m_ckv, GroupTree<String> m_GroupTree, ArrayList<String> m_currentKeyGroup,
                         ArrayList<String> m_firstGroup) {
        this.m_keyboard = m_keyboard;
        this.m_capsOn = m_capsOn;
        this.m_symbolsView = m_symbolsView;
        this.m_ckv = m_ckv;
    }

    public KeyboardState() {
        this.m_keyboard = null;
        this.m_capsOn = false;
        this.m_symbolsView = false;
        this.m_ckv = null;
    }

    private enum Language{ENGLISH, HEBREW};
    private Keyboard m_keyboard;
    private boolean m_capsOn;
    private boolean m_symbolsView;
    private Language m_currentLanguage=Language.ENGLISH;
    private CustomKeyboardView m_ckv;

    public boolean isSymbolsView() {
        return m_symbolsView;
    }

    public void setSymbolsView(boolean m_symbolsView) {
        this.m_symbolsView = m_symbolsView;
    }

    public Language getCurrentLanguage() {
        return m_currentLanguage;
    }

    public void setCurrentLanguage(Language m_currentLanguage) {
        this.m_currentLanguage = m_currentLanguage;
    }

    public Keyboard getKeyboard() {
        return m_keyboard;
    }

    public void setKeyboard(Keyboard m_keyboard) {
        this.m_keyboard = m_keyboard;
    }

    public boolean isCapsOn() {
        return m_capsOn;
    }

    public void setCapsOn(boolean m_capsOn) {
        this.m_capsOn = m_capsOn;
    }

    public CustomKeyboardView getCustomKeyboardView() {
        return m_ckv;
    }

    public void setCustomKeyboardView(CustomKeyboardView m_ckv) {
        this.m_ckv = m_ckv;
    }

}
