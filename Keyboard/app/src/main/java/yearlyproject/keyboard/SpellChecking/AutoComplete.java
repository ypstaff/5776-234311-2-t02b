package yearlyproject.keyboard.SpellChecking;

import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.service.textservice.SpellCheckerService;
import android.support.v4.app.NotificationCompat;
import android.widget.Toast;


import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.OutputStream;
import java.io.StreamCorruptedException;
import java.util.List;


import yearlyproject.autocomplete.SortedStringTrie;
import yearlyproject.keyboard.R;


/**
 * Created by Sergey on 01/12/2015.
 */
public class AutoComplete extends SpellCheckerService {
    private static SortedStringTrie ds;
    private static final String DS_FILENAME="TBK_AutoComplete2.txt";

    private class LoadDictionaryAsyncTask extends AsyncTask<InputStream,Integer,SortedStringTrie> {
        private Context context;
        public LoadDictionaryAsyncTask(Context context) {
            this.context=context;
        }

        @Override
        protected SortedStringTrie doInBackground(InputStream... params) {
            SortedStringTrie sst=SortedStringTrie.deserealize(params[0]);
            return sst;
        }

        @Override
        protected void onPreExecute() {
            /*NotificationCompat.Builder notificationsBuilder = new NotificationCompat.Builder(context);
            notificationsBuilder.setSmallIcon(R.drawable.notification_icon)
                    .setContentTitle("ThreeButtonKeyboard")
                    .setContentText("Loading AC dictionary...please wait.");
            NotificationManager nm=(NotificationManager)getSystemService(context.NOTIFICATION_SERVICE);
            nm.notify(150,notificationsBuilder.build());*/
            Toast toast = Toast.makeText(context,"Loading AC dict,please wait.",Toast.LENGTH_SHORT);
            toast.show();
        }

        @Override
        protected void onPostExecute(SortedStringTrie result) {
            AutoComplete.ds=result;
           /* NotificationCompat.Builder notificationsBuilder = new NotificationCompat.Builder(context);
            notificationsBuilder.setSmallIcon(R.drawable.notification_icon)
                    .setContentTitle("ThreeButtonKeyboard")
                    .setContentText("AC dictionary is loaded.");
            NotificationManager nm=(NotificationManager)getSystemService(context.NOTIFICATION_SERVICE);
            nm.notify(150,notificationsBuilder.build());*/
            Toast toast = Toast.makeText(context,"AC dict loaded",Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    @Override
    public void onCreate() {
        if(ds==null) {
            Resources r = getApplicationContext().getResources();
            InputStream is = r.openRawResource(R.raw.ac_trie);
            new LoadDictionaryAsyncTask(getApplicationContext()).execute(is);
        }
    }

    @Override
    public Session createSession() {
        return new AutoCompleteSession(ds);
    }

    public static void learnUserPreferences(final List<String> sentece,Context context) {

    }

}
