package yearlyproject.keyboard.SpellChecking;

import android.service.textservice.SpellCheckerService;
import  android.service.textservice.SpellCheckerService.Session;
import android.view.textservice.SuggestionsInfo;
import android.view.textservice.TextInfo;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

import yearlyproject.autocomplete.SortedStringTrie;


/**
 * Created by Sergey on 01/12/2015.
 */
public class AutoCompleteSession extends Session {

    private SortedStringTrie ds;

    public AutoCompleteSession(SortedStringTrie ds) {
        super();
        this.ds=ds;
    }

    @Override
    public void onCreate() {

    }

    @Override
    public SuggestionsInfo onGetSuggestions(TextInfo textInfo, int suggestionsLimit) {
        String[] arr;
        if(ds!=null) {
            String str=textInfo.getText();
            List<String> suggestions=ds.getMostCommonByPrefix(str, suggestionsLimit);
            arr=new String[suggestions.size()];
            for(int i=0;i<arr.length;i++) {
                arr[i]=suggestions.get(i);
            }
        } else {
            arr=new String[4];
            for(int i=0;i<arr.length;i++) {
                arr[i]="";
            }
        }
        SuggestionsInfo si=new SuggestionsInfo(SuggestionsInfo.RESULT_ATTR_IN_THE_DICTIONARY,arr);
        return si;
    }
}
