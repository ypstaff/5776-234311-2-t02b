package yearlyproject.keyboard;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Sergey on 18/04/2016.
 */
public interface BranchingFactorsProducer {
    List<Integer> produceDefaultBranchingFactors(int xmlLayoutResID);
    HashMap<Integer,List<Integer>> produceSpecialBranchingFactors(
            int xmlLayoutResID,List<? extends List<String>> rows);
}
