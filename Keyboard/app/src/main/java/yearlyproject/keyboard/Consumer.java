package yearlyproject.keyboard;

/**
 * Created by kfirlan on 11/11/2015.
 * @author kfir
 * general interface for passing functionality (especially ui) to Asynctask / Async function
 */
public interface Consumer<T> {
    public void accept(T t);
}
