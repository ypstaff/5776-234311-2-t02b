package yearlyproject.keyboard;

import android.content.res.Resources;
import android.inputmethodservice.Keyboard;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputConnection;

/**
 * Created by Sergey on 30/03/2016.
 */
public class InputTypeHandler {
    private ThreeButtonKeyboard parent;

    public InputTypeHandler(ThreeButtonKeyboard parent) {
        this.parent=parent;
    }

    /***
     * Implements default behavior of a keyboard.
     * @param keyCode
     * @param state
     */
    public void onClick(int keyCode,KeyboardState state) {
        InputConnection ic = parent.getCurrentInputConnection();
        Resources m_resources = parent.getResources();
        switch (keyCode) {
            case -101:
            case -102: //Left or Right
                changeCursorPosition(ic,keyCode,m_resources);
                break;
            case Keyboard.KEYCODE_DELETE:
                onKeyCodeDelete(ic);
                break;
            case Keyboard.KEYCODE_SHIFT:
                onKeyCodeShift(state);
                break;
            case Keyboard.KEYCODE_DONE:
                onKeyCodeDone(ic);
                break;
            case Keyboard.KEYCODE_CANCEL:
                onKeyCodeCancel(ic);
                break;
            default:
                onOther(ic, state, keyCode);
        }
        if (keyCode != 46 && keyCode != -5 && keyCode != m_resources.getInteger(R.integer.leftCode)
                && keyCode != m_resources.getInteger(R.integer.rightCode)) {
            parent.resetGUIState();
        }
    }

    /***
     * Change position of the cursor after 'left' or 'right' key was pressed.
     * @param ic - The target InputConnection
     * @param keyCode
     * @param resources
     */
    protected void changeCursorPosition(InputConnection ic, int keyCode,Resources resources) {
        String textBeforeCursor = ic.getTextBeforeCursor(1000, 0).toString();
        String textAfterCursor = ic.getTextAfterCursor(1000, 0).toString();
        int textBeforeLength = textBeforeCursor.length();
        int textAfterLength = textAfterCursor.length();
        if (keyCode == resources.getInteger(R.integer.leftCode)) {
            if (textBeforeLength != 0)
                ic.setSelection(textBeforeLength - 1, textBeforeLength - 1);
        } else if (keyCode == resources.getInteger(R.integer.rightCode)) {
            if (textAfterLength != 0)
                ic.setSelection(textBeforeLength + 1, textBeforeLength + 1);
        }
    }

    /***
     * Default implementation for deletion.
     * @param ic
     */
    protected void onKeyCodeDelete(InputConnection ic) {
        ic.deleteSurroundingText(1, 0);
    }

    /***
     * Default implementation for toggling shift mode on and off.
     * @param state
     */
    protected void onKeyCodeShift(KeyboardState state) {
        state.setCapsOn(!state.isCapsOn());
        state.getKeyboard().setShifted(state.isCapsOn());
        state.getCustomKeyboardView().invalidateAllKeys();
    }

    /***
     * Default implementation for finishing input.
     * @param ic
     */
    protected void onKeyCodeDone(InputConnection ic) {
        ic.performEditorAction(EditorInfo.IME_ACTION_DONE);
        parent.onFinishInput();
        parent.hideWindow();
    }

    /**
     * Default implementation for deleting all written text
     * @param ic
     */
    protected void onKeyCodeCancel(InputConnection ic) {
        ic.deleteSurroundingText(1000, 1000); //Arbitrary big number
    }

    /**Default behavior when any other key is presssed.
     *If you have any more special behavior for certain keys, you should implement
     *the checks and calls to the matching functions in this function.
     * @param ic
     * @param state
     * @param KeyCode
     */
    protected void onOther(InputConnection ic, KeyboardState state, int KeyCode) {
        char code = (char) KeyCode;
        ic.commitText(String.valueOf(code), 1);
    }

}
