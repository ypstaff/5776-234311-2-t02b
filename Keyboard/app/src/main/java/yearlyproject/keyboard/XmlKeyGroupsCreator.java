package yearlyproject.keyboard;

import android.content.Context;

import org.xmlpull.v1.XmlPullParser;

import java.util.ArrayList;

/**
 * Created by Sergey on 07/11/2015.
 */
public class XmlKeyGroupsCreator {

    public static final String ANDROID_RES_SCHEMA = "http://schemas.android.com/apk/res/android";

    private ArrayList<String> parseRow(XmlPullParser parser) {
        ArrayList<String> $=new ArrayList<String>();
        try {
            parser.next();
            int i=0;
            while(!parser.getName().equals("Row")){
                if(parser.getName().equals("Key")) {
                    $.add(parser.getAttributeValue(ANDROID_RES_SCHEMA, "codes"));
                }

                parser.next();
                parser.next(); // need two nexts for some reason
            }
        } catch(Exception ex) {
            ex.printStackTrace();
        }
        return $;
    }

    public ArrayList<ArrayList<String>> create(Context context,int fileCode){
        XmlPullParser parser= context.getResources().getXml(fileCode);
        ArrayList<ArrayList<String>> groups=new ArrayList<ArrayList<String>>();
        try {
            while(parser.next()!=XmlPullParser.END_DOCUMENT) {
                if(parser.getEventType()==XmlPullParser.START_TAG) {
                    String name=parser.getName();
                    if(name.equals("Row")) {
                        if(parser.getAttributeValue(ANDROID_RES_SCHEMA,"rowEdgeFlags")==null){
                            groups.add(parseRow(parser));
                        }
                    }
                }
            }
        } catch (Exception ex) {

        }
        return  groups;
    }

}
