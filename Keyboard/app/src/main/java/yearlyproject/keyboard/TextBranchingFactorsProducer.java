package yearlyproject.keyboard;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Sergey on 18/04/2016.
 */
public class TextBranchingFactorsProducer implements BranchingFactorsProducer {
    @Override
    public List<Integer> produceDefaultBranchingFactors(int xmlLayoutResID) {
        return new ArrayList<Integer>(Arrays.asList(3, -1));
    }

    @Override
    public HashMap<Integer,List<Integer>>
    produceSpecialBranchingFactors(int xmlLayoutResID, List<? extends List<String>> rows) {
        HashMap<Integer, List<Integer>> tmp = new HashMap<Integer, List<Integer>>();
        List<Integer> branchFactorsFirst = new ArrayList<Integer>(Arrays.asList(2, -1));
        List<Integer> branchFactorsCopyPaste = new ArrayList<Integer>(Arrays.asList(-1));
        if (xmlLayoutResID == R.xml.symbols) {
            tmp.put(1, branchFactorsCopyPaste);
        }
        tmp.put(0, branchFactorsFirst);
        tmp.put(rows.size() - 1, branchFactorsFirst);
        return tmp;
    }
}
