package yearlyproject.keyboard;


import org.junit.Before;
import org.junit.Test;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import static org.junit.Assert.*;
/**
 * Created by Sergey on 11/11/2015.
 */
public class GroupTreeBuilderTest {

    private ArrayList<ArrayList<Integer>> myGroups;
    private ArrayList<Integer> myBranchingFactors;
    private GroupTree<Integer> $;

    private void setUpGroups() {
        myGroups=new ArrayList<ArrayList<Integer>>();
        for(int i=0;i<5;i++) {
            myGroups.add(new ArrayList<Integer>());
            for(int j=0;j<10;j++) {
                myGroups.get(i).add(i*10+j);
            }
        }
    }

    private void setUpBranchingFactors() {
        myBranchingFactors=new ArrayList<>();
        myBranchingFactors.add(5);
        myBranchingFactors.add(-1);
    }

    @Before
    public void setUp() {
        setUpGroups();
        setUpBranchingFactors();
        $=new GroupTreeBuilder<Integer>().buildTree(this.myGroups,this.myBranchingFactors);

    }

    @Test
    public void firstChildIsSelectedAfterCreation() {
        ArrayList<Integer> currentlySelected=$.getSelectedGroup();
        assertTrue(currentlySelected.equals(myGroups.get(0)));
    }

    @Test
    public void splitsAccordingToBranchingFactor() {
        ArrayList<Integer>  selected;
        $.selectChild();
        for(int i=0;i<myBranchingFactors.get(0);i++) {
            selected=$.getSelectedGroup();
            assertEquals(2, selected.size());
            $.selectNextSibling();
            assertEquals(2, selected.size());
        }
    }

    @Test
    public void createRequestedAmountOfLevelsIfPossible() {
        //set up a different branching factors array and different group values:
        ArrayList<Integer> branchingFactor=new ArrayList<Integer>();
        for(int i=0;i<5;i++) {
            branchingFactor.add(2);
        }
        branchingFactor.add(-1);
        ArrayList<ArrayList<Integer>> groups=new ArrayList<ArrayList<Integer>>();
        groups.add(new ArrayList<Integer>());
        for(int i=0;i<64;i++) {
            groups.get(0).add(i);
        }
        //create a group tree
        GroupTree<Integer> $1=new GroupTreeBuilder<Integer>().buildTree(groups,branchingFactor);
        for(int i=0;i<6;i++) {
            assertEquals((int)Math.pow(2, 6-i),$1.getSelectedGroup().size());
            $1.selectChild();
        }
    }

    @Test(expected = Exception.class)
    public void throwsExceptionWhenBranchingFactorGreatherThanNumberOfElements() {
        ArrayList<ArrayList<Integer>> groups=new ArrayList<>();
        groups.add(new ArrayList<Integer>());
        for(int i=0;i<64;i++) {
            groups.get(0).add(i);
        }
        ArrayList<Integer> branchingFactors=new ArrayList<Integer>();
        branchingFactors.add(100);
        new GroupTreeBuilder<Integer>().buildTree(groups,branchingFactors);
    }

    @Test
    public void splittingByAllMembersWhenSpecialBranchingFactor() {
        $.selectChild();
        $.selectChild();
        ArrayList<Integer> expected=new ArrayList<Integer>();
        for(int j=0;j<2;j++) {
            expected.add(j);
            assertEquals(expected,$.getSelectedGroup());
            expected.remove(0);
            $.selectNextSibling();
        }
    }

    @Test
    public void remainderIsInLastGroupIfCantDevideEqually() {
        myGroups=new ArrayList<ArrayList<Integer>>();
        for(int i=0;i<5;i++) {
            myGroups.add(new ArrayList<Integer>());
            for(int j=0;j<5;j++) {
                myGroups.get(i).add(i*10+j);
            }
        }
        myBranchingFactors=new ArrayList<>(Arrays.asList(2,-1));
        $=new GroupTreeBuilder<Integer>().buildTree(myGroups, myBranchingFactors);
        $.selectChild();
        assertEquals(2,$.getSelectedGroup().size());
        assertEquals(3,$.selectNextSibling().size());
    }
        @Test
        public void splittingSpecifiedGroupsByTheirBranchingFactors() {
            HashMap<Integer,ArrayList<Integer>> specialBranchingFactors=new HashMap<>();
            specialBranchingFactors.put(0,new ArrayList<Integer>(Arrays.asList(2,-1)));
            specialBranchingFactors.put(1,new ArrayList<Integer>(Arrays.asList(10,-1)));
            $=new GroupTreeBuilder<Integer>().buildTree(myGroups,myBranchingFactors,specialBranchingFactors);
            assertEquals(5, $.selectChild().size());
            $.selectParent();
            $.selectNextSibling();
            assertEquals(1, $.selectChild().size());
        }

    @Test
    public void splittingUnspecifiedGroupsByDefaultBranchingFactors() {
        HashMap<Integer,ArrayList<Integer>> specialBranchingFactors=new HashMap<>();
        specialBranchingFactors.put(0,new ArrayList<Integer>(Arrays.asList(2,-1)));
        specialBranchingFactors.put(1,new ArrayList<Integer>(Arrays.asList(10,-1)));
        $=new GroupTreeBuilder<Integer>().buildTree(myGroups,myBranchingFactors,specialBranchingFactors);
        $.selectNextSibling();
        $.selectNextSibling();
        for(int i=0;i<3;i++) {
            assertEquals(2,$.selectChild().size());
            $.selectParent();
            $.selectNextSibling();
        }
    }



}
