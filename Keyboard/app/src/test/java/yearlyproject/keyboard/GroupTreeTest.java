package yearlyproject.keyboard;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.Assert.assertEquals;

/**
 * Created by Sergey on 11/11/2015.
 */
public class GroupTreeTest {
    private GroupTree<Integer> $;

    @Before
    public void setUp() {
        ArrayList<GroupTree> children=new ArrayList<>();
        ArrayList<Integer> group=new ArrayList<>();
        $=new GroupTree<Integer>(children,null,false,null);
        group.add(0);
        children.add(new GroupTree<Integer>(null,$,true,group));
        for(int i=1;i<10;i++) {
            group=new ArrayList<>();
            group.add(i);
            children.add(new GroupTree<Integer>(null,$,false,group));
        }
    }


    @Test
    public void selectsSiblingsInRotation() {
        $.selectChild();
        ArrayList<Integer> expectedRes=new ArrayList<Integer>();
        for(int i=0;i<20;i++) {
            expectedRes.add((i+1)%10);
            assertEquals(expectedRes, $.selectNextSibling());
            expectedRes.remove(0);
        }
    }



    @Test
    public void selectsCorrectNodeOnSelectParent() {
        ArrayList<Integer> originalGroup=$.getSelectedGroup();
        $.selectChild();
        ArrayList<Integer> onSelectParentGroup=$.selectParent();
        ArrayList<Integer> afterSelectParentGroup=$.selectParent();
        assertEquals(originalGroup, onSelectParentGroup);
        assertEquals(originalGroup,afterSelectParentGroup);
    }

    @Test
    public void stayInPlaceWhenSelectingParentWhileUnderRoot() {
        ArrayList<Integer> expectedRes=new ArrayList<Integer>();
        expectedRes.add(1);
        $.selectChild();
        $.selectNextSibling();
        ArrayList<Integer> onSelectParentGroup=$.selectParent();
        ArrayList<Integer> afterSelectParentGroup=$.selectParent();
        assertEquals(expectedRes,afterSelectParentGroup);
        assertEquals(expectedRes,onSelectParentGroup);
    }

    @Test
    public void stayInPlaceWhenSelectinChildOfALeaf() {
        //TODO:initiate $ with a short tree here
        $.selectChild();
        $.selectChild();
        ArrayList<Integer> selectedGroup=$.selectChild();
        ArrayList<Integer> res;
        for(int i=0;i<10;i++) {
            res=$.selectChild();
            assertEquals(selectedGroup,res);
        }
    }

    @Test
    public void selectsCorrectNodeOnSelectChild() {
        ArrayList<GroupTree> firstGen=new ArrayList<GroupTree>();
        $=new GroupTree<Integer>(firstGen,null,false,null);
        boolean isSelected=false;
        ArrayList<GroupTree> secondGen=new ArrayList<GroupTree>();
        ArrayList<Integer> group=new ArrayList<Integer>();
        for(int i=0;i<10;i++) {
            isSelected=(i==0?true:false);
            secondGen=new ArrayList<GroupTree>();
            firstGen.add(new GroupTree<>(secondGen, $,isSelected,null));
            for(int j=0;j<10;j++) {
                group=new ArrayList<Integer>();
                group.add(i*10+j);
                secondGen.add(new GroupTree<>(null, firstGen.get(i),false,group));
            }
        }
        for(int i=0;i<10;i++) {
            ArrayList<Integer> expected=new ArrayList<Integer>();
            expected.add(i*10);
            assertEquals(expected,$.selectChild());
            $.selectParent();
            $.selectNextSibling();
        }
    }

    @Test
    public void returnsCorrectSetOfSiblings() {
        ArrayList<GroupTree> firstGen=new ArrayList<GroupTree>();
        $=new GroupTree<Integer>(firstGen,null,false,null);
        boolean isSelected=false;
        ArrayList<GroupTree> secondGen=new ArrayList<GroupTree>();
        ArrayList<Integer> group=new ArrayList<Integer>();
        for(int i=0;i<10;i++) {
            isSelected=(i==0?true:false);
            secondGen=new ArrayList<GroupTree>();
            firstGen.add(new GroupTree<>(secondGen, $,isSelected,null));
            for(int j=0;j<10;j++) {
                group=new ArrayList<Integer>();
                group.add(i*10+j);
                secondGen.add(new GroupTree<>(null, firstGen.get(i),false,group));
            }
        }
        ArrayList<ArrayList<Integer>> expectedRes1=new ArrayList<>();
        ArrayList<ArrayList<Integer>> expectedRes2=new ArrayList<>();
        for(int i=0;i<10;i++) {
            expectedRes1.add(new ArrayList<Integer>());
            for(int j=0;j<10;j++) {
                expectedRes1.get(i).add(i*10+j);
            }
        }
        for(int i=0;i<10;i++) {
            expectedRes2.add(new ArrayList<Integer>(Arrays.asList(i)));
        }
        assertEquals(expectedRes1,$.getAllSiblingsOfSelectedNode());
        $.selectChild();
        assertEquals(expectedRes2,$.getAllSiblingsOfSelectedNode());
    }
}
