package yearlyproject.keyboard;

import android.content.res.Resources;

import com.google.inject.*;
import org.junit.*;
import org.mockito.Mockito;
import java.util.ArrayList;
import java.util.Arrays;
import static junit.framework.Assert.assertTrue;
import static org.mockito.Mockito.when;


/**
 * Created by roman on 11/11/2015.
 */

public class ThreeButtonKeyboardTest {
    ThreeButtonKeyboard $;
    ArrayList<Integer> currentGroup;
    GroupTree<Integer> groupTreeMock;
    private CustomKeyboardView ckvMock;
    Resources resourcesMock;



    private ArrayList<Integer> setupGroup() {
        return new ArrayList<>(Arrays.asList(1,2,3,4,5));
    }


    @BeforeClass
    public static void setupAll(){

    }

    @Before
    public void setup() {

        Injector injector=Guice.createInjector(new AbstractModule() {

            @Override
            protected void configure() {
                bind(String.class).toInstance("testServer");
                currentGroup = setupGroup();
                groupTreeMock = Mockito.mock(GroupTree.class);
                ckvMock = Mockito.mock(CustomKeyboardView.class);
                resourcesMock= Mockito.mock(Resources.class);
                bind(new TypeLiteral<ArrayList<Integer>>() {
                }).toInstance(currentGroup);
                bind(new TypeLiteral<GroupTree<Integer>>() {
                }).toInstance(groupTreeMock);
                bind(CustomKeyboardView.class).toInstance(ckvMock);
                bind(Resources.class).toInstance(resourcesMock);
            }
        });
        $=injector.getInstance(ThreeButtonKeyboard.class);
    }


    @Test
    public void testGroupTreeSelectChild_CalleAfter_SelectClicked() {
        when(resourcesMock.getInteger(R.integer.selectCode)).thenReturn(-200);
        $.onKey(-200, null);
        Mockito.verify(groupTreeMock,Mockito.times(1)).selectChild();
        Mockito.verify(groupTreeMock,Mockito.times(0)).selectParent();
        Mockito.verify(groupTreeMock,Mockito.times(0)).selectNextSibling();
    }

    @Test
    public void testGroupTreeSelectNext_CalleAfter_SelectNextClicked() {
        when(resourcesMock.getInteger(R.integer.nextCode)).thenReturn(-100);
        $.onKey(-100, null);
        Mockito.verify(groupTreeMock,Mockito.times(1)).selectNextSibling();
        Mockito.verify(groupTreeMock,Mockito.times(0)).selectChild();
        Mockito.verify(groupTreeMock,Mockito.times(0)).selectParent();
    }

    @Test
    public void testGroupTreeSelectParent_CalleAfter_backClicked() {
        when(resourcesMock.getInteger(R.integer.backCode)).thenReturn(-300);
        $.onKey(-300, null);
        Mockito.verify(groupTreeMock,Mockito.times(1)).selectParent();
        Mockito.verify(groupTreeMock,Mockito.times(0)).selectNextSibling();
        Mockito.verify(groupTreeMock,Mockito.times(0)).selectChild();
    }

    @Test
      public void testCurrentGroupChanges_IfGroupTreeChanges() {
        ArrayList<Integer> newGroup = new ArrayList<>(Arrays.asList(1, 2, 3));
        when(groupTreeMock.selectNextSibling()).thenReturn(newGroup);
        when(resourcesMock.getInteger(R.integer.nextCode)).thenReturn(-200);
        $.onKey(-200, null);
        assertTrue(currentGroup.equals(newGroup));
    }

}
