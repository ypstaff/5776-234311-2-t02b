package autocomplete;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.Arrays;

import yearlyproject.autocomplete.SortedStringTrie;

public class Main {
	public static void main(String[] args) {
		if(args.length<2) {
			System.out.println("error");
		}
		String in=args[0];
		String out=args[1];
		
		SortedStringTrieInitializer init=new SortedStringTrieInitializer();
		SortedStringTrie $=init.initializeNewSST(Arrays.asList(new String[] {in}));
		
		try {
			FileOutputStream fos=new FileOutputStream(out);
			SortedStringTrie.serealize(fos,$);
			fos.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
