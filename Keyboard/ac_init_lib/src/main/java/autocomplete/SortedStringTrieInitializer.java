package autocomplete;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import yearlyproject.autocomplete.SortedStringTrie;

public class SortedStringTrieInitializer {
	
	
	public SortedStringTrieInitializer() {}
	
	public SortedStringTrie initializeNewSST(List<String> textFiles) {
		SortedStringTrie $=new SortedStringTrie();
		for(String textFile : textFiles) {
			try {
				List<String> lines = Files.readAllLines(Paths.get(textFile),
						Charset.forName("UTF-8"));
				for(String line : lines) {
					for(String word : line.split(" ")) {
						if(word.length()>1)
							$.insert(word);
					}
				}
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return $;
	}
	
	
}
